<?php
namespace common\models;

use yii\helpers\VarDumper;

class User extends \dektrium\user\models\User
{
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][] = 'user_type';
        $scenarios['update'][] = 'user_type';
        $scenarios['updateType'] = ['user_type'];
        $scenarios['register'][] = 'user_type';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['fieldRequired'] = ['user_type', 'required'];
        $rules['fieldLength'] = ['user_type', 'string', 'max' => 50];

        return $rules;
    }

    public  function getLatestUsername(){

        \Yii::trace(VarDumper::dumpAsString($this->user_type),'vardump');

        if($this->user_type=='Executive')
            $usertypecode='EMP-';
        else if($this->user_type=='Individual'){
            //$usertypecode="MEM-$this->user_type";
            $usertypecode="IND-";
        }
        else if($this->user_type=='Company'){
            //$usertypecode="MEM-$this->user_type";
            $usertypecode="COM-";
        }else if($this->user_type=='Tenant'){
            //$usertypecode="MEM-$this->user_type";
            $usertypecode="TEN-";
        }else if($this->user_type=='Data Entry'){
            //$usertypecode="MEM-$this->user_type";
            $usertypecode="DEN-";
        }else if($this->user_type=='Admin'){
            //$usertypecode="MEM-$this->user_type";
            $usertypecode="ADM-";
        }

        //$pref="VHP-$this->user_type";
        $pref=$usertypecode;

        $lbn=001;

        $sql="select  max(username) as username from user where username like '$pref%'  ";
        $cmd = \Yii::$app->db->createCommand($sql);

        $result = $cmd->queryAll();
        //$lbn=6000;
        foreach ($result as $row){
            /*preg_match('/\d+$/',$row["username"],$match);
            if($match[0]) {
                $lbn = $match[0];
                $lbn++;
            }*/
            if(!empty($row["username"])) {
                $lbn = substr($row["username"], -3);
                $lbn++;
            }

        }

       /* $sql="select * from auto_no_init where name like 'batch' ";
        $cmd = Yii::app()->db->createCommand($sql);
        $res=$cmd->queryRow();
        if($res&&$res["reset"]==1){
            preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
            // var_dump($match);
            if(isset($match[1]))
                $pref=$match[1];
            if(isset($match[2]))
                $lbn=$match[2];

        }elseif($res){
            // var_dump($res["slno"]);
            preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
            //  var_dump($match);
            if(isset($match[1]))
                $pref=$match[1];
        }*/
        //$billno="MED/". date("Y")."/"."INVOICE/".sprintf("%05d", $lbn+1);
        //$billno=date("Y")."/".$pref.sprintf("%05d", $lbn);
        $pref=$pref.date('ym')."/";
        $billno=$pref.sprintf("%03d", $lbn);
        return $billno;

    }

    public function beforeSave($insert)
    {
        \Yii::trace(VarDumper::dumpAsString($this),'vardump');
        if (parent::beforeSave($insert)) {
                $this->username=$this->getLatestUsername();
            return true;
        }else{
            return false;
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
        return true;
    }


    public static function findByUsername($username)
    {
        //return static::findBySql("username=:username or email=:username",[":username"=>$username]);
        //" select * from user where username=:username or email=:username",[":username"=>$username]

        return static::find()->where("username=:username or email=:username",[":username"=>$username])->one();
        return static::findOne(['email' => $username]);

    }

    public function validatePassword($password) {
        return  \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

}