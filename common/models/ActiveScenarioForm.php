<?php
namespace common\models;

use yii\base\Object;
use yii\widgets\ActiveForm;

class ActiveScenarioForm extends ActiveForm{

    public function  field($model, $attribute, $options = [] )
    {
        $scenarios[]=$model->scenario;
        $checkres=ActiveScenarioForm::checkAttributeScenaioAccess($scenarios,'write',$model,$attribute);
        if($checkres->suc==false){
            return new NullActiveField();
        }else{

            return  parent::field($model, $attribute, $options);
        }

    }


    public static function checkAttributeScenaioAccess($scenarios, $oper, $model, $attribute)
    {
        $ret = (object)array();
        $scenarios[] = "*";
        $ret->suc = true;
        $ara=array();
        //var_dump($model->attributeRoleAssignment());
        if(method_exists($model,'attributeScenarioAcess'))
            $ara = $model->attributeScenarioAcess();

        if ($oper == 'write') {


            foreach ($scenarios as $scenario) {


                if (isset($ara[$attribute]) && array_key_exists($scenario, $ara[$attribute])) {

                    if (in_array('read', $ara[$attribute][$scenario]))
                        $ret->read = false;
                }

            }
        }
        foreach ($scenarios as $scenario){
            //var_dump($ara[$attribute]);
            if (isset($ara[$attribute]) && array_key_exists($scenario, $ara[$attribute])) {

                if (in_array($oper, $ara[$attribute][$scenario]))
                    $ret->suc = false;
            }

        }


        return $ret;

    }



}

class NullActiveField {
    public function textInput(){
        return null;
    }
    public function textArea(){
        return null;
    }
    public function dropDownList(){
        return null;
    }
    public function widget(){
        return null;
    }


}
