
var myApp = angular.module('ctodo', []);

myApp.directive('ctodo', function ($compile) {

    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            video: '@'
        },
        templateUrl: './directivetemplates/ctodo.html',
        controller:function($scope,$element,$http){
            $scope.src=serverAPIAdrs+'executive/todolist';
            $scope.addUrl=serverAPIAdrs+'executive/addtodo';
            $scope.tickUrl=serverAPIAdrs+'executive/ticktodo';

            $http({method: 'GET', url:$scope.src}).success(function (result) {
                for(var i=0;i<result.length;i++){
                    var li = document.querySelector('#liproto').cloneNode(true);
                    var span = li.querySelector('#liproto span');
                    var inp = li.querySelector('#liproto input');
                    inp.setAttribute("id",result[i].id);
                    li.style.display = "inherit";
                    li.id = "";
                    span.innerHTML = result[i].todo;
                    li=$compile(li)($scope);
                    angular.element(document.querySelector('#todo-list')).append(li);
                }
            }, function (result) {
                alert("Error: No data returned");
            });
            //begin: tickToDo
            $scope.tickToDo=function( $event){
                //alert($event.currentTarget.id);
                var id=$event.currentTarget.id;
                $http({method: 'POSt', url:$scope.tickUrl,data: {id: id}})
                .success(function (result) {
                        var chbox=angular.element(document.getElementById(id));
                        document.getElementById(id).checked=false;
                        if(result.status==1)
                            chbox.parent().children('span').addClass("strike-through")
                        else
                            chbox.parent().children('span').removeClass("strike-through")

                    })
                .error(function(){
                        var chbox=(document.getElementById(id));
                        chbox.checked=false;//("checked");
                        alert("Invalid Data");
                    });
            }

            $scope.addToDo=function(){
                var todotext=document.querySelector('#todo-input-text').value;
                if(todotext!='') {
                    $http({method: 'POST', url: $scope.addUrl, data: {todo: todotext}})
                    .success(function (result) {
                        console.log(result);
                        var li = document.querySelector('#liproto').cloneNode(true);
                        var span = li.querySelector('#liproto span');
                        var inp = li.querySelector('#liproto input');
                        inp.setAttribute("id", result.id);
                        li.style.display = "inherit";
                        li.id = "";
                        span.innerHTML = result.todo;
                        li = $compile(li)($scope);
                        angular.element(document.querySelector('#todo-list')).append(li);
                        })
                    .error( function (result) {
                        alert("Error: Bad input");
                    });
                }

               /*if(todotext!='') {
                    var li = document.querySelector('#liproto').cloneNode(true);
                    //var liproto=angular.element(document.querySelector('#liproto'));
                    //li=liproto.cloneNode(true);
                    //li=angular.copy(liproto);
                    var span = li.querySelector('#liproto span');
                    li.style.display = "inherit";
                    li.id = "";
                    span.innerHTML = todotext;
                    li=$compile(li)($scope);
                    // li.appendChild(input)(scope);
                    $("#todo-list").append(li);
                }*/
            }

        },
        link: function (scope, element, attrs) {
            console.log(element);
            /*var ratio = (attrs.height / attrs.width) * 100;
            element[0].style.paddingTop = ratio + '%';*/



            //end addtodo
            //begin: tickToDo
            /*scope.tickToDo=function( $event){
                alert("link");
                //alert($event.currentTarget.checked);
            }*/
        }
    };
});