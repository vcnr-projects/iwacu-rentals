'use strict';
var serverAPIAdrs="/vhp1/vhp/frontend/web/";
var chatServerAdrs="ws://192.168.137.121:9000/echobot";
var controllers = angular.module('controllers', []);
controllers.controller('MainController', ['$scope', '$http', '$location', '$window','$rootScope','SessionService',

    function ($scope,$http, $location, $window,$rootScope,SessionService) {
        $scope.main={};
        $scope.mainControllerFinish=false;
        /*$http.get(serverAPIAdrs+'member/login-details').success(
            function (data) {
                console.log(data);
                $scope.username=data.username;
                $scope.photo=data.photo;
                $scope.crsa=data.crsa;
                $scope.rsas=data.rsas;
                $scope.newrsa=data.newrsa;

                $scope.properties=data.properties;
                $scope.property=data.properties[0];
                $scope.mainControllerFinish=true;

            }).error(
            function (data) {
                delete $window.sessionStorage.access_token;
                window.location.assign("login.html")
            }
        );*/
        $scope.initDone = false;
        $scope.sessionObj={};
        SessionService.init().then(function(){
            //alert("sessionservice");
            $scope.initDone=SessionService.getInitDone();
            $scope.sessionObj["username"]=SessionService.session.username;
            $scope.sessionObj["photo"]=SessionService.session.photo;
            $scope.sessionObj["crsa"]=SessionService.session.crsa;
            $scope.sessionObj["rsas"]=SessionService.session.rsas;
            $scope.sessionObj["newrsa"]=SessionService.session.newrsa;
            $scope.sessionObj["properties"]=SessionService.session.properties;
            $scope.sessionObj["property"]=SessionService.session.properties[0];
            $scope.mainControllerFinish=true;
        });


        $scope.loggedIn = function() {
            return Boolean($window.sessionStorage.access_token);
        };

        $scope.logout = function () {
            delete $window.sessionStorage.access_token;
           // $location.path('/login').replace();

            window.location.assign("login.html")
        };

        $scope.propertyChange=function(){
            console.log($scope.property);
        }

        $scope.sessionObj.rsaNotifiationClick=0;
        $scope.viewRequest=function(){


            $http.get(serverAPIAdrs+'member/set-request-services-as-viewed').success(function(){
                if($scope.sessionObj.rsaNotifiationClick>0){

                    $scope.sessionObj.newrsa=0;
                    $scope.sessionObj.rsaNotifiationClick=0;
                }else
                $scope.sessionObj.rsaNotifiationClick++;


            }).error(function(){
                //alert("Error try again");
            });

        }

        $rootScope.currentProperty=function(){

            return $scope.sessionObj.property;
        }
        $rootScope.getMainControllerFinish=function(){

            return $scope.mainControllerFinish;
        }
    }
]);


controllers.controller('requestServiceController', ['$scope', '$http','$window','$rootScope','$location',
    function ($scope, $http,$window,$rootScope,$location) {
        $scope.submitted = true;
        $scope.error = {};
        $scope.rst=[];


            console.log($rootScope.currentProperty());
            var payload={'property':$rootScope.currentProperty().id};
            console.log(payload);

            $http.post(serverAPIAdrs+'member/requestservicetypes',payload)
                .success(function (data) {
                    $scope.rst = data;
                })




        $scope.sendRequest=function(){
           // console.log($scope.rsa);
            $scope.property=$rootScope.currentProperty();
            $scope.rsa.member_id=$scope.property.id;
            $http.post(serverAPIAdrs+'member/sendrequest',$scope.rsa).success(function (data) {
                if(data.success){
                    $scope.sessionObj.rsas=data.rsas;
                    $scope.sessionObj.crsa=data.crsa;
                    $scope.sessionObj.newrsa=data.newrsa;
                    $scope.sessionObj.rsaNotifiationClick=0;
                    $location.search({id:data.id});
                    //alert($scope.$parent.newrsa);
                    $location.path( "/view-rsa" );
                }
            }).error(
                function (data) {
                    angular.forEach(data, function (error) {
                        $scope.error[error.field] = error.message;
                    });
                }
            );
        }

    }
]);

controllers.controller('viewRSAController', ['$scope', '$http','$window','$rootScope','$location',
    function ($scope, $http,$window,$rootScope,$location) {

        var $queryString=$location.search();
        console.log($queryString);



        $http.post(serverAPIAdrs+'member/view-rsa',$queryString).success(function (data) {
            $scope.rsa = data;
            switch ($scope.rsa.status*1){
                case 0 :
                    $scope.rsa.fstatus="Pending Admin Approval";
                    break;
                case 1:
                    $scope.rsa.fstatus="Pending Member/Authorized Approval";
                    break;
                case 2:
                    $scope.rsa.fstatus="Pending for Work-Start";
                    break;
                case 3:
                    $scope.rsa.fstatus="Work Started";
                    break;
                case 4:
                    $scope.rsa.fstatus="Work Finished";
                    break;
                case -1:
                    $scope.rsa.fstatus="Cancelled";
                    break;
                default:
                    $scope.rsa.fstatus=$scope.rsa.status*1;
            }


        });

        $scope.approve=function(){
            $http.post(serverAPIAdrs+'member/approve-rsa',$queryString).success(function (data) {
                $scope.rsa = data;
                switch ($scope.rsa.status*1){
                    case 0 :
                        $scope.rsa.fstatus="Pending Admin Approval";
                        break;
                    case 1:
                        $scope.rsa.fstatus="Pending Member/Authorized Approval";
                        break;
                    case 2:
                        $scope.rsa.fstatus="Pending for Work-Start";
                        break;
                    case 3:
                        $scope.rsa.fstatus="Work Started";
                        break;
                    case 4:
                        $scope.rsa.fstatus="Work Finished";
                        break;
                    case -1:
                        $scope.rsa.fstatus="Cancelled";
                        break;
                    default:
                        $scope.rsa.fstatus=$scope.rsa.status*1;
                }


            })
        }


    }
]);

controllers.controller('RequestDetailController', ['$scope','$route', '$http', '$window','$location',
    function($scope,$route, $http, $window,$location) {
        //console.log($route.current.params);
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.user_type='Individual';

        $scope.fillDetails=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $scope.myInterval = 3000;

            $http.post(serverAPIAdrs + 'member/request-detail',payload).success(function (data) {
                $scope.mstr_detail=data.mstr_detail;
                $scope.accepted_cost=$scope.mstr_detail.accepted_cost;
                $scope.accepted_date=$scope.mstr_detail.formatedStartDate;
                $scope.acceptedStatus=$scope.mstr_detail.member_approval;

                //$scope.conversation=data.conversation;

                //$scope.propertyVisitList = data.pVisitL;
                //$scope.propertyVisitedList = data.pVisitedL;
                setTimeout(function(){
                    $('a[data-rel^=lightcase]').lightcase();
                    /*$('#homeCarousel').carousel({
                     interval:3000,
                     pause: "false"
                     });*/
                },1000);
                //alert($('a[data-rel^=lightcase]').size());
                //$('a[data-rel^=lightcase]').lightcase();

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillDetails();

        $scope.memberApprove=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $http.post(serverAPIAdrs + 'member/rsa-approve',payload)
                .success(function (data) {
                    if(data.success){
                        $route.reload();
                    }else{
                        alert(data.errmsg);
                    }

                }).error(function () {
                    alert("Error try again");
                });
        }

        //websocket start
        var socket;

        $scope.init=function() {
            var host = chatServerAdrs; // SET THIS TO YOUR SERVER
            try {
                socket = new WebSocket(host);
                console.log('WebSocket - status '+socket.readyState);
                socket.onopen    = function(msg) {
                    var token={"token":$window.sessionStorage.access_token,"rsa_id":$route.current.params.id};
                    socket.send(JSON.stringify(token));
                    console.log("Welcome - status "+this.readyState);
                };
                socket.onmessage = function(msg) {

                    console.log("Received: "+msg.data);
                    var data=JSON.parse(msg.data);
                    console.log(data);
                    $scope.$apply(function () {
                        $scope.conversation=data.data;
                    });
                    //$scope.displayChat(data);
                    goToByScroll("typeHere");
                };
                socket.onclose   = function(msg) {
                    console.log("Disconnected - status "+this.readyState);
                };
            }
            catch(ex){
                console.log(ex);
            }
            //$("msg").focus();
        }


        $scope.send=function(msg){
            //var txt,msg;
            //txt = $("msg");
            //msg = txt.value;
            if(!msg){
                alert("Message can not be empty");
                return;
            }
            //txt.value="";
            //txt.focus();
            try{
                socket.send(msg);
                console.log('Sent: '+msg);
            }catch(ex){
                console.log(ex);
            }
        }

        $scope.quit=function(){
            if (socket != null) {
                console.log("Goodbye!");
                socket.close();
                socket=null;
            }
        }

        $scope.reconnect=function() {
            $scope.quit();
            $scope.init();
        }

        $scope.init();
        //websocket end

        $scope.sendMessage=function(){
            //alert($scope.message);

            var payload={};
            payload["newMessage"]=true;
            payload["rsa_id"]=$route.current.params.id;
            payload["message"]={};
            payload["message"]["text"]=$scope.message;
            payload=JSON.stringify(payload);
            $scope.message='';
            $scope.send(payload);
        }


        $scope.$on("$destroy", function() {
            $scope.quit();
        });

        $scope.displayChat=function(data){
            $scope.conversation=data.data;
        }

        function goToByScroll(id){

            // Scroll
            $('html,body').animate({
                    scrollTop: $("#"+id).offset().top},
                'slow');
        }

    }
]);



