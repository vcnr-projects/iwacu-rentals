'use strict';
var app = angular.module('app', [
    'ngRoute',          //$routeProvider
    'controllers',       //Our module frontend/web/js/controllers.js

]);

app.config(['$routeProvider', '$httpProvider',
    function($routeProvider, $httpProvider) {
        $routeProvider.
            when('/', {

            }).
            when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'LoginController'
            }).
            when('/dashboard', {
                templateUrl: 'partials/dashboard.html',
                controller: 'DashboardController'
            }).
            when('/view-rsa', {
                templateUrl: 'partials/member/view-rsa.html',
                controller: 'viewRSAController'
            }).
            when('/create-rsa', {
                templateUrl: 'partials/member/request_service.html',
                controller: 'requestServiceController'
            }).
            when('/requestDetail/:id', {
                templateUrl: 'partials/executive/requestDetail.html',
                controller: 'RequestDetailController'
            }).
            otherwise({
                templateUrl: 'partials/404.html'
            });
        $httpProvider.interceptors.push('authInterceptor');
    }
]);

app.factory('authInterceptor', function ($q, $window, $location) {
    return {
        request: function (config) {
            if ($window.sessionStorage.access_token) {
                //HttpBearerAuth
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
            }
            return config;
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                window.location.assign("login.html")
                //$location.path('/login').replace();
            }
            return $q.reject(rejection);
        }
    };
});


app.run(function($rootScope, $location, $anchorScroll, $routeParams) {
    //when the route is changed scroll to the proper element.
    $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
        $location.hash($routeParams.scrollTo);
        $anchorScroll();
    });
});


app.directive('calendar', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {

            $(el).datepicker({
                format: 'dd-mm-yyyy',
                onSelect: function (dateText) {
                    alert(dateText);
                    console.log(ngModel);
                    console.log(dateText);
                    scope.$apply(function () {

                        ngModel.$setViewValue(dateText);
                    });
                }
            }).on('changeDate', function(ev) {
                var date=new Date(ev.date);
                $(this).datepicker('hide');
                scope.$apply(function () {
                    var fdate=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
                    ngModel.$setViewValue(fdate);
                });
            });
        }
    };
});


app.service('SessionService', function ($http, $q, $rootScope) {

    this.initDone = false,
    this.session = {},
        this.init = function(){
            if (!this.initDone){
                var s = this;
                var deferred = $q.defer();

                $http.get(serverAPIAdrs + 'member/login-details').success(
                    function (data) {
                        console.log(data);
                        s.session["username"] = data.username;
                        s.session["photo"] = data.photo;
                        s.session["crsa"] = data.crsa;
                        s.session["rsas"] = data.rsas;
                        s.session["newrsa"] = data.newrsa;
                        s.session["properties"] = data.properties;
                        s.session["property"] = data.properties[0];
                        deferred.resolve(data);
                        s.initDone = true;
                    }).error(
                    function (data) {
                        //delete $window.sessionStorage.access_token;
                        window.location.assign("login.html")
                    }
                );
                return deferred.promise;
            }
        };


    this.getInitDone = function () {
        return this.initDone;
    }
});