'use strict';
var serverAPIAdrs="/vhp1/app/";
var chatServerAdrs="ws://192.168.137.121:9000/echobot";
var backendUploadDir="/vhp/backend/web/uploads/";
var controllers = angular.module('controllers', []);
controllers.controller('MainController', ['$scope', '$http', '$location', '$window','SessionService',

    function ($scope,$http, $location, $window,SessionService) {
        /*$http.get(serverAPIAdrs+'executive/login-details').success(
            function (data) {
                console.log(data);
                $scope.username=data.username;
                $scope.photo=data.photo;
                $scope.sessionObj["crsa"]=SessionService.session.crsa;
                $scope.sessionObj["rsas"]=SessionService.session.rsas;
                $scope.sessionObj["newrsa"]=SessionService.session.newrsa;
            }).error(
            function (data) {
                delete $window.sessionStorage.access_token;
                window.location.assign("login.html")
            }
        );*/

        $scope.resetActive=function(activeUrl){

            $scope.activeDashboard=false;
            $scope.activeMembers=false;
            $scope.activeTenants=false;
            $scope.activeRequests=false;
            $scope.activeRequests=false;
            $scope.activeAllCollections=false;
            $scope.activeDailyReporting=false;
            $scope.activeDailyAccounting=false;
            $scope.activeTravelDetails=false;
            $scope.activePropertyVisit=false;
            $scope.activeAssociationMeeting=false;

            $scope[activeUrl]=true;



        }

        $scope.mainControllerFinish=false;
        $scope.initDone = false;
        $scope.sessionObj={};
        SessionService.init().then(function(){
            $scope.initDone=SessionService.getInitDone();
            $scope.sessionObj["username"]=SessionService.session.username;
            $scope.sessionObj["photo"]=SessionService.session.photo;
            $scope.sessionObj["crsa"]=SessionService.session.crsa;
            $scope.sessionObj["rsas"]=SessionService.session.rsas;
            $scope.sessionObj["newrsa"]=SessionService.session.newrsa;
            //alert(SessionService.session.rsas);

            $scope.mainControllerFinish=true;
        });

        $scope.loggedIn = function() {
            return Boolean($window.sessionStorage.access_token);
        };

        $scope.logout = function () {
            delete $window.sessionStorage.access_token;
           // $location.path('/login').replace();

            window.location.assign("login.html")
        };

        $scope.sessionObj.rsaNotifiationClick=0;
        $scope.viewRequest=function(){


            $http.get(serverAPIAdrs+'executive/set-request-services-as-viewed').success(function(){
                if($scope.sessionObj.rsaNotifiationClick>0){

                    $scope.sessionObj.newrsa=0;
                    $scope.sessionObj.rsaNotifiationClick=0;
                }else
                    $scope.sessionObj.rsaNotifiationClick++;


            }).error(function(){
                //alert("Error try again");
            });

        }

    }
]);

controllers.controller('DashboardController', ['$scope', '$http','$window',
    function ($scope, $http,$window) {
        $scope.resetActive("activeDashboard");

        var options={
            lineWidth: 25,
            size: 165,
            barColor: '#fc8675',
            trackColor: '#fee7e3',
            lineCap: 'square',
            onStep: function(from, to, percent) {

                $(this.el).find('.percent').text(Math.round(percent));
            }
        };
        $('.chart-collection').easyPieChart(options);
        options.barColor="#46bbd3";
        options.trackColor="#daf1f6";
        $('.chart-payment').easyPieChart(options);
        options.barColor="#e8bd6a";
        options.trackColor="#faf2e1";
        $('.chart-visit').easyPieChart(options);
        options.barColor= '#85969a',
        options.trackColor= '#f5f5f5',
        $('.chart-meeting').easyPieChart(options);

        $http.get(serverAPIAdrs+'executive/dashboard').success(function (data) {
            $scope.overdueTasks = data.overdueTasks;
            $scope.overdueTasksCount = data.overdueTasks.length;
            $scope.collections = data.collections;
            $scope.collectionsCount = data.collections.length;
            $scope.nocPerc = data.nocperc;

            $scope.pendingCollection = data.pendingCollection;
            $scope.pendingPayment = data.pendingPayment;
            $scope.pendingVisit = data.pendingVisit;
            $scope.pendingMeeting = data.pendingMeeting;

            $('.chart-collection').data('easyPieChart').update(data.nocperc);
            $('.chart-payment').data('easyPieChart').update(data.nopperc);
            $('.chart-visit').data('easyPieChart').update(data.pvperc);
            $('.chart-meeting').data('easyPieChart').update(data.amperc);

            //$scope.memberContractExpiry = data.collections;
            //$scope.st=$window.sessionStorage.access_token;
        })
    }
]);

controllers.controller('LoginController', ['$scope', '$http', '$window', '$location',
    function($scope, $http, $window, $location) {
        $scope.login = function () {
            $scope.submitted = true;
            $scope.error = {};
            console.log($scope.userModel);
            $http.post(serverAPIAdrs+'executive/login', $scope.userModel).success(
                function (data) {
                    $window.sessionStorage.access_token = data.access_token;
                    $location.path('/dashboard').replace();
                }).error(
                function (data) {
                    angular.forEach(data, function (error) {
                        $scope.error[error.field] = error.message;
                    });
                }
            );
        };
    }
]);

controllers.controller('AllCollectionsController', ['$scope','$route', '$http','$window','$location','GeneralSharedObject',
    function ($scope,$route, $http,$window,$location,GeneralSharedObject) {
        $scope.resetActive("activeAllCollections");

        $scope.from_date=localeFormatTodayDate();
        $scope.to_date=localeFormatTodayDate();
        $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");

        $scope.statusSelect="All";
        $scope.typeSelect="All";
        $scope.typeSelect=$route.current.params.type;

        $scope.paidStatusSelect="All";
        $scope.paidTypeSelect="All";
        $scope.paidTypeSelect=$route.current.params.type;
        $scope.fillAllCollections=function(){
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;
            payload["statusSelect"]=$scope.statusSelect;

            payload["typeSelect"]=$scope.typeSelect;
            payload["paidStatusSelect"]=$scope.paidStatusSelect;
            payload["paidTypeSelect"]=$scope.paidTypeSelect;

            $http.post(serverAPIAdrs+'executive/all-collections',payload).success(function (data) {
                $scope.collections = data.allCollections;
                $scope.paidCollections = data.paidCollections;
                $scope.pdfSessionToken = data.pdfSessionToken;

            });
        }
        $scope.fillAllCollections();
        $scope.dateRangeChange=function(){
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillAllCollections();
        }

        $scope.dateRangeCancel=function(){
            $scope.from_date=localeFormatTodayDate();
            $scope.to_date=localeFormatTodayDate();
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillAllCollections();
        }

        $scope.statusSelectChange=function(status){
            $scope.statusSelect=status;
            $scope.fillAllCollections();
        }
        $scope.typeSelectChange=function(status){
            $scope.typeSelect=status;
            $scope.fillAllCollections();
        }


        $scope.fillPaidCollections=function(){
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;

            payload["paidStatusSelect"]=$scope.paidStatusSelect;
            payload["paidTypeSelect"]=$scope.paidTypeSelect;

            $http.post(serverAPIAdrs+'executive/paid-collections',payload).success(function (data) {
                //$scope.collections = data.allCollections;
                $scope.paidCollections = data;
            });
        }

        $scope.paidStatusSelectChange=function(status){
            $scope.paidStatusSelect=status;
            $scope.fillPaidCollections();
        }
        $scope.paidTypeSelectChange=function(status){
            //alert();
            $scope.paidTypeSelect=status;
            $scope.fillPaidCollections();
        }



        $scope.hideAllPopups=function(){
            $scope.xaxnModeSelect = false;
            $scope.depositChallanSelect = false;
            $scope.cashPaymentForm=false;
            $scope.checkPaymentForm=false;
            $scope.onlinePaymentForm=false;
            $scope.cardPaymentForm=false;
            $scope.depositUploadForm=false;
            $scope.billUploadForm=false;

        }

        $scope.collectionId=0;
        $scope.offset=0;
        $scope.collectNowClick=function(id,e,actstat){
            $scope.collectionId=id;
            $scope.offset = $(e.target).offset();

                $scope.hideAllPopups();
                $scope.xaxnModeSelect = true;
                 //offset = $(e.target).offset();
                var form= angular.element( document.querySelector( '.form-select' ) );
                form.css("top", $scope.offset.top + "px");
                form.css("display", "block");



                //alert($scope.xaxnModeSelect);

        }

        $scope.payBillSlipClick=function(id,e,actstat,collectionAmount){
            $scope.collectionId=id;
            $scope.offset = $(e.target).offset();
            $scope.hideAllPopups();
            $scope.billUploadForm = true;
            $scope.uploadProgressValue="0";
            $scope.billAmount='';
            $scope.paidAmount='';
            $scope.paidPerson='';

            if(collectionAmount!=0)
                $scope.paidAmount=collectionAmount;
            var form= angular.element( document.querySelector( '#billUploadForm' ) );
            form.css("top", $scope.offset.top + "px");
            form.css("display", "block");
        }

        $scope.xaxnMode=function(mode,e){
            //var offset = $(e.target).offset();
            $scope.xaxnModeSelect=false;
            if(mode=="cash"){
                $scope.cashPaymentForm=true;

                var form= angular.element( document.querySelector( '#cashFormReceipt' ) );
                form.css("top", $scope.offset.top + "px");
                form.css("display", "block");
            };
            if(mode=="check"){
                $scope.checkPaymentForm=true;
                var form= angular.element( document.querySelector( '#checkFormReceipt' ) );
                form.css("top", $scope.offset.top + "px");
                form.css("display", "block");
            }
            if(mode=="online"){
                $scope.onlinePaymentForm=true;
                var form= angular.element( document.querySelector( '#onlineFormReceipt' ) );
                form.css("top", $scope.offset.top + "px");
                form.css("display", "block");
            }
            if(mode=="card"){
                $scope.cardPaymentForm=true;
                var form= angular.element( document.querySelector( '#cardFormReceipt' ) );
                form.css("top", $scope.offset.top + "px");
                form.css("display", "block");
            }
        }

        $scope.collectCash=function(){

            /*$http.post(serverAPIAdrs+'executive/collect-cash',{
                "amount":$scope.cashAmount
                ,"date":$scope.cashDate
                ,"id":$scope.collectionId
            }).success(function (data) {
                alert(data.success);
                $scope.fillAllCollections();
                $(".rent").toggle();
                //$scope.collections = data;
            });*/

            GeneralSharedObject.mutableObject={};

            GeneralSharedObject.mutableObject['receiptMode']="Cash";
            GeneralSharedObject.mutableObject['receiptCashObj']={};
            GeneralSharedObject.mutableObject['receiptCashObj']['amount']=$scope.cashAmount;
            GeneralSharedObject.mutableObject['receiptCashObj']['date']=$scope.cashDate;
            GeneralSharedObject.mutableObject['receiptCashObj']['id']=$scope.collectionId;
            $location.path('recieptCheque');


        }

        $scope.collectCheque=function(){
            GeneralSharedObject.mutableObject={};

            GeneralSharedObject.mutableObject['receiptMode']="Cheque";
            GeneralSharedObject.mutableObject['receiptChequeObj']={};
            GeneralSharedObject.mutableObject['receiptChequeObj']['cheque_no']=$scope.cheque_no;
            GeneralSharedObject.mutableObject['receiptChequeObj']['cheque_date']=$scope.cheque_date;
            GeneralSharedObject.mutableObject['receiptChequeObj']['cheque_amount']=$scope.cheque_amount;
            GeneralSharedObject.mutableObject['receiptChequeObj']['cheque_branch']=$scope.cheque_branch;
            GeneralSharedObject.mutableObject['receiptChequeObj']['cheque_bank']=$scope.cheque_bank;
            GeneralSharedObject.mutableObject['receiptChequeObj']['id']=$scope.collectionId;
            console.log(GeneralSharedObject.mutableObject);
            $location.path('recieptCheque');
        }

        $scope.onlineXanxSubmit=function(){
            GeneralSharedObject.mutableObject={};
            GeneralSharedObject.mutableObject['receiptMode']="Online";
            GeneralSharedObject.mutableObject['receiptOnlineObj']={};
            GeneralSharedObject.mutableObject['receiptOnlineObj']['onlineXaxnNo']=$scope.onlineXaxnNo;
            GeneralSharedObject.mutableObject['receiptOnlineObj']['onlineXaxndate']=$scope.onlineXaxndate;
            GeneralSharedObject.mutableObject['receiptOnlineObj']['onlineXaxnAmount']=$scope.onlineXaxnAmount;
            GeneralSharedObject.mutableObject['receiptOnlineObj']['onlineXaxnBank']=$scope.onlineXaxnBank;
            GeneralSharedObject.mutableObject['receiptOnlineObj']['id']=$scope.collectionId;
            $location.path('recieptCheque');
        }
        $scope.cardCredit=true;

        $scope.cardXanxSubmit=function(){
            GeneralSharedObject.mutableObject={};
            GeneralSharedObject.mutableObject['receiptMode']="Card";
            GeneralSharedObject.mutableObject['receiptCardObj']={};
            GeneralSharedObject.mutableObject['receiptCardObj']['cardXaxnNo']=$scope.txn_no;
            GeneralSharedObject.mutableObject['receiptCardObj']['cardBank']=$scope.cardBank;
            GeneralSharedObject.mutableObject['receiptCardObj']['cardDate']=$scope.cardDate;
            GeneralSharedObject.mutableObject['receiptCardObj']['cardAmount']=$scope.cardAmount;
            GeneralSharedObject.mutableObject['receiptCardObj']['cardType']=($scope.cardDebit)?"Debit Card":"Credit Card";
            GeneralSharedObject.mutableObject['receiptCardObj']['id']=$scope.collectionId;
            $location.path('recieptCheque');
        }


        $scope.billUploadProgressValue=0;
        $scope.billSlipSubmit=function(){
                    alert("billslipsubmit");
            $scope.billUploadProgressValue=0;
            console.log($scope.billUploadFile);
            console.log($scope.slipUploadFile);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", serverAPIAdrs+'executive/pay-bill-slip-upload', true);
            xhr.setRequestHeader('Authorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.setRequestHeader('aAuthorization','Bearer ' + $window.sessionStorage.access_token);

            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    $scope.$apply(function () {
                            $scope.billUploadProgressValue = Math.ceil((e.loaded/e.total)*100);
                    });
                }
            }

            var formData = new FormData();
            formData.append("id", ($scope.collectionId));
            formData.append("billAmount", ($scope.billAmount));
            formData.append("paidAmount", ($scope.paidAmount));
            formData.append("paidPerson", ($scope.paidPerson));


            /*formData.append("billUploadFile", $scope.billUploadFile);
            formData.append("slipUploadFile", $scope.slipUploadFile);*/

            if($scope.billUploadFiles)
                for(var i=0;i<$scope.billUploadFiles.length;i++){
                    console.log($scope.billUploadFiles[i]);
                    formData.append("billUploadFile", $scope.billUploadFiles[i]);
                }

            if($scope.slipUploadFiles)
                for(var i=0;i<$scope.slipUploadFiles.length;i++){
                    formData.append("slipUploadFile", $scope.slipUploadFiles[i]);
                }

            xhr.onreadystatechange=function(){
                if (xhr.readyState==4 && xhr.status==200){
                    var data=JSON.parse(xhr.responseText);
                    console.log(data);
                    if(data.success){
                        alert("sucess");
                    }
                    else{
                        alert(data.errmsg);
                    }
                    $scope.hideAllPopups();
                    $scope.fillAllCollections();
                }
            }
            xhr.send(formData);

        }



        $scope.depositChallanUpload=function(){
            console.log($scope.depositChallanFile);
            if(typeof $scope.depositChallanFile.name == 'string')
            {
                alert("Please select a file");
                return false;
            }
            $scope.uploadProgressValue=0;

            var xhr = new XMLHttpRequest();
            xhr.open("POST", serverAPIAdrs+'executive/deposit-cash-payment', true);
            xhr.setRequestHeader('Authorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.setRequestHeader('aAuthorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    $scope.$apply(function () {
                        $scope.uploadProgressValue = Math.ceil((e.loaded/e.total)*100);
                    });
                }
            }

            var formData = new FormData();
            formData.append("id", ($scope.collectionId));
            formData.append("file", $scope.depositChallanFile[0]);

            xhr.onreadystatechange=function(){
                if (xhr.readyState==4 && xhr.status==200){
                    var data=JSON.parse(xhr.responseText);
                    console.log(data);
                    if(data.success){
                        alert("sucess");

                    }
                    else{
                        alert(data.errmsg);
                    }
                    $scope.hideAllPopups();
                    $scope.fillAllCollections();
                }
            }
            xhr.send(formData);
        }


        //an array of files selected
        //$scope.files = [];


        //listen for the file selected event
        $scope.$on("fileSelected", function (event, args) {
           // console.log(event.target);
            console.log(event);
            $scope.$apply(function () {
                //add the file object to the scope's files collection
                $scope.files.push(args.file);


                var xhr = new XMLHttpRequest();
               // console.log($window.sessionStorage);

                xhr.open("POST", serverAPIAdrs+'executive/deposit-cash-payment', true);
                //alert($window.sessionStorage.access_token);
                xhr.setRequestHeader('Authorization','Bearer ' + $window.sessionStorage.access_token);
                xhr.setRequestHeader('aAuthorization','Bearer ' + $window.sessionStorage.access_token);

                xhr.upload.onprogress = function (e) {
                    //alert("onprogress");
                    if (e.lengthComputable) {
                        /*$scope.$apply(function () {
                            $scope.uploadProgressValue=$scope.uploadProgressValue*1+(10*1);
                        });*/

                        $scope.$apply(function () {
                         //   alert(Math.ceil((( e.total) / $scope.files[0].size) * 100));
                            //$scope.uploadProgressValue =(Math.ceil((( e.total) / $scope.files[0].size) * 100));
                            $scope.uploadProgressValue = Math.ceil((e.loaded/e.total)*100);
                        });

                    }
                }
                xhr.upload.onloadstart = function (e) {
                    //alert("onloadstart");
                    //$scope.uploadProgressValue = 0;
                }
                xhr.upload.onloadend = function (e) {
                    //$scope.uploadProgressValue = 100;
                }
                var formData = new FormData();
                //need to convert our json object to a string version of json otherwise
                // the browser will do a 'toString()' on the object which will result
                // in the value '[Object object]' on the server.
                //formData.append("id", angular.toJson(data.id));
                formData.append("id", ($scope.collectionId));
                //now add all of the assigned files
                console.log($scope.files);
                for (var i = 0; i < $scope.files.length; i++) {
                    //add each file to the form data and iteratively name them
                    //console.log(data.files[i].name);
                    formData.append("file", $scope.files[i]);

                }
                xhr.onreadystatechange=function(){
                    if (xhr.readyState==4 && xhr.status==200){
                        console.log('xhr.readyState=',xhr.readyState);
                        console.log('xhr.status=',xhr.status);
                        console.log('response=',xhr.responseText);

                       // var data = $.parseJSON(xhr.responseText);
                        var data=JSON.parse(xhr.responseText);
                        console.log(data);
                        //var data=xhr.responseText;
                        if(data.success){
                        alert("sucess");

                        }
                        else{
                            alert(data.errmsg);
                        }
                        $scope.hideAllPopups();
                        $scope.fillAllCollections();
                    }
                }
                xhr.send(formData);

                /*$http({
                    method: 'POST',
                    url: serverAPIAdrs+'executive/deposit-cash-payment',
                    //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
                    // but this is not true because when we are sending up files the request
                    // needs to include a 'boundary' parameter which identifies the boundary
                    // name between parts in this multi-part request and setting the Content-type
                    // manually will not set this boundary parameter. For whatever reason,
                    // setting the Content-type to 'false' will force the request to automatically
                    // populate the headers properly including the boundary parameter.
                    headers: { 'Content-Type': undefined },
                    //This method will allow us to change how the data is sent up to the server
                    // for which we'll need to encapsulate the model data in 'FormData'
                    transformRequest: function (data) {
                        var formData = new FormData();
                        //need to convert our json object to a string version of json otherwise
                        // the browser will do a 'toString()' on the object which will result
                        // in the value '[Object object]' on the server.
                        //formData.append("id", angular.toJson(data.id));
                        formData.append("id", (data.id));
                        //now add all of the assigned files
                        console.log(data.files);
                        for (var i = 0; i < data.files.length; i++) {
                            //add each file to the form data and iteratively name them
                            //console.log(data.files[i].name);
                            formData.append("file", data.files[i]);

                        }
                        return formData;
                    },
                    //Create an object that contains the model and files which will be transformed
                    // in the above transformRequest method
                    data: { id: $scope.collectionId, files: $scope.files }
                }).
                    success(function (data, status, headers, config) {
                        //data=JSON.parse(data);
                        //alert(data);
                        if(data.success)
                            alert("success!");
                        else alert(data.errmsg);
                    }).
                    error(function (data, status, headers, config) {
                        alert("failed!");
                    });*/

            });

        });


    }
]);

controllers.controller('RecieptChequeController', ['$scope', '$http', '$window','$location','GeneralSharedObject',
    function($scope, $http, $window,$location,GeneralSharedObject) {
        $scope.cheque_no ="0000000";
        $scope.type="Type";
        $scope.receiptMode=GeneralSharedObject.mutableObject.receiptMode;
        $scope.amount='';
        $scope.date='';
        $scope.bank='';
        $scope.branch='';

        if($scope.receiptMode=="Cash") {

            $scope.amount=GeneralSharedObject.mutableObject.receiptCashObj.amount+"/-";
            $scope.words=number2text(GeneralSharedObject.mutableObject.receiptCashObj.amount);
            $scope.mode="Cash*";
            $scope.date= GeneralSharedObject.mutableObject.receiptCashObj.date;

            $http.post(serverAPIAdrs+'executive/eal-full-details',{id:GeneralSharedObject.mutableObject.receiptCashObj.id}).success(function (data) {
                $scope.tenant_name = data.tname;
                $scope.member_name = data.mname;
                $scope.type = data.type;
                $scope.txn_no=data.nextTxn_no;
            });
        }

        if($scope.receiptMode=="Cheque") {
            $scope.xmode='Cheque';
            $scope.xanxLabel="No.";
            $scope.bankLabel="Drawn On";
            $scope.branchLabel="Branch";
            $scope.cheque_no = GeneralSharedObject.mutableObject.receiptChequeObj.cheque_no;
            $scope.cheque_date = GeneralSharedObject.mutableObject.receiptChequeObj.cheque_date;
            $scope.amount =GeneralSharedObject.mutableObject.receiptChequeObj.cheque_amount+"/-";
            $scope.words=number2text(GeneralSharedObject.mutableObject.receiptChequeObj.cheque_amount);
            $scope.mode="Cheque/DD*";


            $scope.date= localeFormatTodayDate();
            $scope.bank=GeneralSharedObject.mutableObject.receiptChequeObj.cheque_bank;
            $scope.branch=GeneralSharedObject.mutableObject.receiptChequeObj.cheque_branch;
            $http.post(serverAPIAdrs+'executive/eal-full-details',{id:GeneralSharedObject.mutableObject.receiptChequeObj.id}).success(function (data) {
                $scope.tenant_name = data.tname;
                $scope.member_name = data.mname;
                $scope.type = data.type;
                $scope.txn_no=data.nextTxn_no;


            });
        }

        if($scope.receiptMode=="Online"){

            $scope.xmode='Online';
            $scope.xanxLabel='Transaction ID';
            $scope.bankLabel='Bank';
            $scope.cheque_no=GeneralSharedObject.mutableObject.receiptOnlineObj.onlineXaxnNo;
            $scope.cheque_date = GeneralSharedObject.mutableObject.receiptOnlineObj.onlineXaxndate;
            $scope.bank=GeneralSharedObject.mutableObject.receiptOnlineObj.onlineXaxnBank;
            $scope.amount=GeneralSharedObject.mutableObject.receiptOnlineObj.onlineXaxnAmount+"/-";
            $scope.words=number2text(GeneralSharedObject.mutableObject.receiptOnlineObj.onlineXaxnAmount);

            $scope.date=localeFormatTodayDate();
            $scope.mode="Online Transfer*";
            $http.post(serverAPIAdrs+'executive/eal-full-details',{id:GeneralSharedObject.mutableObject.receiptOnlineObj.id}).success(function (data) {
                $scope.tenant_name = data.tname;
                $scope.member_name = data.mname;
                $scope.type = data.type;
                $scope.txn_no=data.nextTxn_no;
            });
        }

        if($scope.receiptMode=="Card"){

            $scope.xmode='Card';
            $scope.xanxLabel='Transaction ID';
            $scope.bankLabel='Bank';
            $scope.branchLabel='Card Type';
            $scope.cheque_no=GeneralSharedObject.mutableObject.receiptCardObj.cardXaxnNo;
            $scope.branch=GeneralSharedObject.mutableObject.receiptCardObj.cardType;
            $scope.bank=GeneralSharedObject.mutableObject.receiptCardObj.cardBank;
            $scope.cheque_date = GeneralSharedObject.mutableObject.receiptCardObj.cardDate;
            $scope.amount=GeneralSharedObject.mutableObject.receiptCardObj.cardAmount+"/-";
            $scope.words=number2text(GeneralSharedObject.mutableObject.receiptCardObj.cardAmount);

            $scope.date=localeFormatTodayDate();
            $scope.mode="Card*";
            $http.post(serverAPIAdrs+'executive/eal-full-details',{id:GeneralSharedObject.mutableObject.receiptCardObj.id}).success(function (data) {
                $scope.tenant_name = data.tname;
                $scope.member_name = data.mname;
                $scope.type = data.type;
                $scope.txn_no=data.nextTxn_no;
            });
        }

        $scope.submit=function(){
            var payload={};
            if($scope.receiptMode=="Cash") {
                payload['id'] = GeneralSharedObject.mutableObject.receiptCashObj.id;
            }
            if($scope.receiptMode=="Cheque") {
                payload['id'] = GeneralSharedObject.mutableObject.receiptChequeObj.id;
                payload['cheque_no'] = $scope.cheque_no;
                payload['branch']=$scope.branch;
                payload['cheque_date']=$scope.cheque_date;
                payload['bank']=$scope.bank;
            }
            if($scope.receiptMode=="Online") {
                payload['id'] = GeneralSharedObject.mutableObject.receiptOnlineObj.id;
                payload['transaction_no'] = $scope.cheque_no;
                payload['cheque_date']=$scope.cheque_date;
                payload['bank']=$scope.bank;
            }

            if($scope.receiptMode=="Card") {
                payload['id'] = GeneralSharedObject.mutableObject.receiptCardObj.id;
                payload['transaction_no'] = $scope.cheque_no;
                payload['cheque_date']=$scope.cheque_date;
                payload['bank']=$scope.bank;
                payload['branch']=$scope.branch;
            }

            payload['amount']=$scope.amount;
            payload['mode']=$scope.mode;


            payload['date']=$scope.date;
            $http.post(serverAPIAdrs+'executive/collect-cheque',payload).success(function (data) {

                if(data.success){
                    $location.path('allCollections');
                }

            });
        }

        $scope.back=function(){
            $location.path( 'allCollections' );
        }

    }]);

controllers.controller('TravelDetailsController', ['$scope', '$http', '$window','$location','GeneralSharedObject',
    function($scope, $http, $window,$location,GeneralSharedObject) {
        $scope.resetActive("activeTravelDetails");
        $scope.nowDate=new Date();
        $scope.emptyTravel={};

        $scope.clearEmptyTravel=function(){
            $scope.emptyTravel["from_location"]=null;
            $scope.emptyTravel["to_location"]=null;
            $scope.emptyTravel["purpose"]=null;
            $scope.emptyTravel["distance"]=null;
            $scope.emptyTravel["newRecord"]=true;
        }

        $scope.travelDetailsList=[];
        $scope.clearEmptyTravel();

        $scope.focusedDetail=null;
        $scope.unsavedDetail=false;
        $scope.enableSaveButton=false;
        $scope.enableModifyButton=false;

        $scope.fillTravelDetailsList=function() {
            $http.get(serverAPIAdrs + 'executive/travel-details-list').success(function (data) {
                $scope.travelDetailsList = data;
                for (var i = 0; i < $scope.travelDetailsList.length; i++) {
                    $scope.travelDetailsList[i]["newRecord"] = false;
                }
                $scope.travelDetailsList.push($scope.emptyTravel);
                $scope.unsavedDetail = true;


            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillTravelDetailsList();

        $scope.inputClick=function(index){
            //alert(index);
            $scope.focusedDetail=index;
            //alert($scope.travelDetailsList[index].newRecord);
            if($scope.travelDetailsList[index].newRecord)$scope.enableSaveButton=true;
            else $scope.enableSaveButton=false;

            if(!$scope.travelDetailsList[index].newRecord)$scope.enableModifyButton=true;
            else $scope.enableModifyButton=false;
        }

        $scope.addRow=function(){
            if(!$scope.unsavedDetail)
                $scope.travelDetailsList.push(emptyTravel);
        }

        $scope.save=function(){
            //alert($scope.focusedDetail);
            var payload=$scope.travelDetailsList[$scope.focusedDetail];
            $http.post(serverAPIAdrs+'executive/save-travel-details',payload).success(function(data){
                if(data.success) {
                    alert('succcess');
                    $scope.clearEmptyTravel();
                    $scope.fillTravelDetailsList();
                }
                else{
                    alert(data.errmsg);
                }
            }).error(function(){
                alert("Error try again");
            });
        }

    }]);

controllers.controller('PropertyVisitController', ['$scope', '$http', '$window','$location','GeneralSharedObject',
    function($scope, $http, $window,$location,GeneralSharedObject) {
        $scope.resetActive("activePropertyVisit");

        $scope.vidUrls=[""];

        $scope.addVidUrls=function(){
            //alert($scope.vidUrls.length);
            $scope.vidUrls.push("");
            console.log($scope.vidUrls);
        }

        $scope.propertyVisitList=[];

        $scope.showUploadForm=false;

        $scope.from_date=localeFormatTodayDate();
        $scope.to_date=localeFormatTodayDate();
        $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");

        $scope.fillPropertyVisit=function() {
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;
            $http.post(serverAPIAdrs + 'executive/property-visit-list',payload).success(function (data) {
                $scope.propertyVisitList = data.pVisitL;
                $scope.propertyVisitedList = data.pVisitedL;
            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillPropertyVisit();



        $scope.dateRangeChange=function(){
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillPropertyVisit();
        }

        $scope.dateRangeCancel=function(){
            $scope.from_date=localeFormatTodayDate();
            $scope.to_date=localeFormatTodayDate();
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillPropertyVisit();
        }

        $scope.collectionId=0;
        $scope.offset=0;
        $scope.uploadProgressValue=0;
        $scope.collectNowClick=function(id,e,actstat){

            $scope.vidUrls=[""];
            $scope.collectionId=id;
            $scope.offset = $(e.target).offset();

            $scope.showUploadForm=true;
            //offset = $(e.target).offset();
            var form= angular.element( document.querySelector( '.property-style-from' ) );
            form.css("top", $scope.offset.top + "px");
            form.css("display", "block");


        }

        $scope.save=function(){
            var xhr = new XMLHttpRequest();
            xhr.open("POST", serverAPIAdrs+'executive/add-property-visit', true);
            xhr.setRequestHeader('Authorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.setRequestHeader('aAuthorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    $scope.$apply(function () {
                        $scope.uploadProgressValue = Math.ceil((e.loaded/e.total)*100);
                    });
                }
            }

            var formData = new FormData();
            formData.append("id", ($scope.collectionId));
            //alert($scope.comments);
            formData.append("comment", ($scope.comments)?$scope.comments:"");
            formData.append("visitStatus", ($scope.visitStatus));
            //alert($scope.vidurl);
            /*
            $scope.vidurl=($scope.vidurl)?$scope.vidurl:'';
            formData.append("videoUrl", ($scope.vidurl));
            */

            for(var i=0;i<$scope.vidUrls.length;i++){
                formData.append("videoUrls[]", ($scope.vidUrls[i]));
            }

            console.log($scope.billUploadFiles);

            if($scope.billUploadFiles)
            for(var i=0;i<$scope.billUploadFiles.length;i++){
                console.log($scope.billUploadFiles[i]);
                formData.append("billUploadFiles[]", $scope.billUploadFiles[i]);
            }

            if($scope.slipUploadFiles)
            for(var i=0;i<$scope.slipUploadFiles.length;i++){
                formData.append("slipUploadFiles[]", $scope.slipUploadFiles[i]);
            }

            //formData.append("slipUploadFiles[]", $scope.slipUploadFiles);

            xhr.onreadystatechange=function(){
                if (xhr.readyState==4 && xhr.status==200){
                    var data=JSON.parse(xhr.responseText);
                    console.log(data);
                    if(data.success){
                        alert("sucess");
                    }
                    else{
                        alert(data.errmsg);
                    }
                    $scope.showUploadForm=false;
                    $scope.fillPropertyVisit();
                }
            }
            xhr.send(formData);
        }

    }]);

controllers.controller('AssociationMeetingController', ['$scope', '$http', '$window','$location','GeneralSharedObject',
    function($scope, $http, $window,$location,GeneralSharedObject) {

        $scope.resetActive("activeAssociationMeeting");
        $scope.associationMeetingList=[];

        $scope.showUploadForm=false;

        $scope.from_date=localeFormatTodayDate();
        $scope.to_date=localeFormatTodayDate();
        $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");

        $scope.fillAssociationMeeting=function() {
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;
            $http.post(serverAPIAdrs + 'executive/association-meeting-list',payload).success(function (data) {
                $scope.associationMeetingList = data;
            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillAssociationMeeting();




        $scope.dateRangeChange=function(){
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillAssociationMeeting();
        }

        $scope.dateRangeCancel=function(){
            $scope.from_date=localeFormatTodayDate();
            $scope.to_date=localeFormatTodayDate();
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillAssociationMeeting();
        }

        $scope.collectionId=0;
        $scope.offset=0;
        $scope.uploadProgressValue=0;
        $scope.collectNowClick=function(id,e,actstat){
            $scope.collectionId=id;
            $scope.offset = $(e.target).offset();
            $scope.showUploadForm=true;
            //offset = $(e.target).offset();
            var form= angular.element( document.querySelector( '.association-form' ) );
            form.css("top", $scope.offset.top + "px");
            form.css("display", "block");
        }

        $scope.setDayFromDate=function(){
            //alert($scope.date);
            var dts=$scope.date.split("/");
            var date=new Date(dts[2],dts[1]-1,dts[0]);
            var dayName=date.getDay();
            var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
            //alert(days[dayName]);
            $scope.day=days[dayName];
        }

        $scope.save=function(){
            var xhr = new XMLHttpRequest();

            xhr.open("POST", serverAPIAdrs+'executive/add-association-meeting', true);
            xhr.setRequestHeader('Authorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.setRequestHeader('aAuthorization','Bearer ' + $window.sessionStorage.access_token);
            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    $scope.$apply(function () {
                        $scope.uploadProgressValue = Math.ceil((e.loaded/e.total)*100);
                    });
                }
            }

            var formData = new FormData();
            formData.append("id", ($scope.collectionId));
            formData.append("date", ($scope.date));
            formData.append("day", ($scope.day));
            formData.append("time", ($scope.time));
            formData.append("venue", ($scope.venue));
            formData.append("summary", ($scope.summary));
            formData.append("billUploadFiles", $scope.billUploadFiles);
            formData.append("slipUploadFiles", $scope.slipUploadFiles);

            xhr.onreadystatechange=function(){
                if (xhr.readyState==4 && xhr.status==200){
                    var data=JSON.parse(xhr.responseText);
                    console.log(data);
                    if(data.success){
                        alert("sucess");
                    }
                    else{
                        alert(data.errmsg);
                    }
                    $scope.showUploadForm=false;
                    $scope.fillAssociationMeeting();
                }
            }
            xhr.send(formData);
        }

    }]);

controllers.controller('PropertyVisitedDetailsController', ['$scope','$sce','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$sce,$route, $http, $window,$location,GeneralSharedObject) {
       //console.log($route.current.params);
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.photos=[];
        $scope.property={};
        $scope.fillGallery=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $scope.myInterval = 3000;
            $http.post(serverAPIAdrs + 'executive/property-visited-details',payload).success(function (data) {

                if(data.photo[0]){
                    data.photo[0]["active"]=true;
                }
                $scope.photos=data.photo;
                $scope.property=data.property;
                $scope.log=data.log;
                //$scope.log.date=moment($scope.log.date).format('MMMM Do YYYY');
                $scope.month=moment($scope.log.date).format('MMMM');
                $scope.day=moment($scope.log.date).format('Do, YYYY');
                $scope.comments=$scope.log.remarks;

                $scope.videos=data.video;
                for(var i=0;i<data.video.length;i++){
                    $scope.videos[i].file_path= $sce.trustAsHtml(data.video[i].file_path)
                }

                console.log(data.property);
                //$scope.propertyVisitList = data.pVisitL;
                //$scope.propertyVisitedList = data.pVisitedL;
                setTimeout(function(){
                    $('a[data-rel^=lightcase]').lightcase();
                    /*$('#homeCarousel').carousel({
                        interval:3000,
                        pause: "false"
                    });*/
                },1000);
                //alert($('a[data-rel^=lightcase]').size());
                //$('a[data-rel^=lightcase]').lightcase();

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillGallery();

    }
]);

controllers.controller('DailyAccountingController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.resetActive("activeDailyAccounting");
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.entries=[];

        $scope.from_date=localeFormatTodayDate();
        $scope.to_date=localeFormatTodayDate();
        $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");


        $scope.fillEntries=function(){
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;
            $http.post(serverAPIAdrs + 'executive/daily-accounting-list',payload).success(function (data) {
                $scope.entries=data.entries;
                $scope.totCollection=data.totCollection;
                $scope.totPayment=data.totPayment;
            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();


        $scope.dateRangeChange=function(){
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillEntries();
        }

        $scope.dateRangeCancel=function(){
            $scope.from_date=localeFormatTodayDate();
            $scope.to_date=localeFormatTodayDate();
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillEntries();
        }

    }
]);

controllers.controller('RequestListController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.resetActive("activeRequests");
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.entries=[];

        $scope.from_date=localeFormatTodayDate();
        $scope.to_date=localeFormatTodayDate();
        $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");


        $scope.fillEntries=function(){
            var payload={};
            payload["from_date"]=$scope.from_date;
            payload["to_date"]=$scope.to_date;
            $http.post(serverAPIAdrs + 'executive/request-list',payload).success(function (data) {
                $scope.entries=data.entries;

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();


        $scope.dateRangeChange=function(){
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillEntries();
        }

        $scope.dateRangeCancel=function(){
            $scope.from_date=localeFormatTodayDate();
            $scope.to_date=localeFormatTodayDate();
            $scope.dateRangeDisplay=moment($scope.from_date,'DD/MM/YYYY').format("MMM Do, YYYY")+" - "+moment($scope.to_date,'DD/MM/YYYY').format("MMM Do, YYYY");
            $scope.fillEntries();
        }

    }
]);

controllers.controller('RequestDetailController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.user_type='Executive';
        $scope.costEntryBool=false;



        $scope.fillDetails=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $scope.myInterval = 3000;

            $http.post(serverAPIAdrs + 'executive/request-detail',payload).success(function (data) {
                $scope.mstr_detail=data.mstr_detail;
                $scope.accepted_cost=$scope.mstr_detail.accepted_cost;
                $scope.accepted_date=$scope.mstr_detail.formatedStartDate;
                //alert($scope.mstr_detail.member_approval);
                $scope.acceptedStatus=$scope.mstr_detail.member_approval;

                //$scope.conversation=data.conversation;

                //$scope.propertyVisitList = data.pVisitL;
                //$scope.propertyVisitedList = data.pVisitedL;
                setTimeout(function(){
                    $('a[data-rel^=lightcase]').lightcase();
                    /*$('#homeCarousel').carousel({
                     interval:3000,
                     pause: "false"
                     });*/
                },1000);
                //alert($('a[data-rel^=lightcase]').size());
                //$('a[data-rel^=lightcase]').lightcase();

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillDetails();

        $scope.costSubmit=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            /*alert($scope.accepted_cost);
            alert($scope.accepted_date);*/
            payload["accepted_cost"]=$scope.accepted_cost;
            payload["date"]=$scope.accepted_date;
            $http.post(serverAPIAdrs + 'executive/rsa-approve',payload)
            .success(function (data) {
                if(data.success){
                    $route.reload();
                }else{
                    alert(data.errmsg);
                }

            }).error(function () {
                alert("Error try again");
            });
        }

        $scope.completeSubmit=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            /*alert($scope.accepted_cost);
             alert($scope.accepted_date);*/
            payload["date"]=$scope.completed_date;
            payload["final_cost"]=$scope.final_cost;
            $http.post(serverAPIAdrs + 'executive/rsa-complete',payload)
                .success(function (data) {
                    if(data.success){
                        $route.reload();
                    }else{
                        alert(data.errmsg);
                    }

                }).error(function () {
                    alert("Error try again");
                });
        }

        //websocket start
        var socket;

        $scope.init=function() {
            var host = chatServerAdrs // SET THIS TO YOUR SERVER
            try {
                socket = new WebSocket(host);
                console.log('WebSocket - status '+socket.readyState);
                socket.onopen    = function(msg){
                    var token={"token":$window.sessionStorage.access_token,"rsa_id":$route.current.params.id};
                    socket.send(JSON.stringify(token));
                    console.log("Welcome - status "+this.readyState);
                };
                socket.onmessage = function(msg){
                    console.log("Received: "+msg.data);
                    var data=JSON.parse(msg.data);
                    console.log(data);
                    $scope.$apply(function () {
                        $scope.conversation=data.data;
                        $scope.acceptedStatus=data.data[0].member_approval;
                        console.log(data.data);
                    });
                    goToByScroll("typeHere");
                    //$scope.displayChat(data);
                };
                socket.onclose   = function(msg){
                    console.log("Disconnected - status "+this.readyState);
                };
            }
            catch(ex){
                console.log(ex);
            }
            //$("msg").focus();
        }

        $scope.send=function(msg){
            //var txt,msg;
            //txt = $("msg");
            //msg = txt.value;
            if(!msg) {
                alert("Message can not be empty");
                return;
            }
            //txt.value="";
            //txt.focus();
            try {
                socket.send(msg);
                console.log('Sent: '+msg);
            } catch(ex) {
                console.log(ex);
            }
        }

        $scope.quit=function(){
            if (socket != null) {
                console.log("Goodbye!");
                socket.close();
                socket=null;
            }
        }

        $scope.reconnect=function() {
            $scope.quit();
            $scope.init();
        }

        $scope.init();
        //websocket end

        $scope.sendMessage=function(){
            //alert($scope.message);

            var payload={};
            payload["newMessage"]=true;
            payload["rsa_id"]=$route.current.params.id;
            payload["message"]={};
            payload["message"]["text"]=$scope.message;
            payload=JSON.stringify(payload);
            $scope.message='';
            $scope.send(payload);

        }


        $scope.$on("$destroy", function() {
            $scope.quit();
        });

        $scope.displayChat=function(data){
            $scope.conversation=data.data;
        }

        $scope.replyTimeFormat=function(time){
           return moment(time,'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY hh:mm a');
        }

        function goToByScroll(id){

            // Scroll
            $('html,body').animate({
                    scrollTop: $("#"+id).offset().top},
                'slow');
        }




    }
]);

controllers.controller('AssociationMeetingDetailsController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);
        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.photos=[];
        $scope.property={};
        $scope.fillGallery=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $scope.myInterval = 3000;
            $http.post(serverAPIAdrs + 'executive/property-visited-details',payload).success(function (data) {

                if(data.photo[0]){
                    data.photo[0]["active"]=true;
                }
                $scope.photos=data.photo;
                $scope.property=data.property;
                $scope.log=data.log;
                //$scope.log.date=moment($scope.log.date).format('MMMM Do YYYY');
                $scope.month=moment($scope.log.date).format('MMMM');
                $scope.day=moment($scope.log.date).format('Do, YYYY');
                $scope.comments=$scope.log.remarks;


                console.log(data.property);
                //$scope.propertyVisitList = data.pVisitL;
                //$scope.propertyVisitedList = data.pVisitedL;
                setTimeout(function(){
                    $('a[data-rel^=lightcase]').lightcase();
                    /*$('#homeCarousel').carousel({
                     interval:3000,
                     pause: "false"
                     });*/
                },1000);
                //alert($('a[data-rel^=lightcase]').size());
                //$('a[data-rel^=lightcase]').lightcase();

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillGallery();

    }
]);

function localeFormatTodayDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    return today = dd+'/'+mm+'/'+yyyy;
}

//no to words rupees
function number2text(value) {
    var fraction = Math.round(frac(value)*100);
    var f_text  = "";

    if(fraction > 0) {
        f_text = "AND "+convert_number(fraction)+" PAISE";
    }

    return convert_number(value)+" RUPEE "+f_text+" ONLY";
}

function frac(f) {
    return f % 1;
}

function convert_number(number){
    if ((number < 0) || (number > 999999999))
    {
        return "NUMBER OUT OF RANGE!";
    }
    var Gn = Math.floor(number / 10000000);  /* Crore */
    number -= Gn * 10000000;
    var kn = Math.floor(number / 100000);     /* lakhs */
    number -= kn * 100000;
    var Hn = Math.floor(number / 1000);      /* thousand */
    number -= Hn * 1000;
    var Dn = Math.floor(number / 100);       /* Tens (deca) */
    number = number % 100;               /* Ones */
    var tn= Math.floor(number / 10);
    var one=Math.floor(number % 10);
    var res = "";

    if (Gn>0)
    {
        res += (convert_number(Gn) + " CRORE");
    }
    if (kn>0)
    {
        res += (((res=="") ? "" : " ") +
        convert_number(kn) + " LAKH");
    }
    if (Hn>0)
    {
        res += (((res=="") ? "" : " ") +
        convert_number(Hn) + " THOUSAND");
    }

    if (Dn)
    {
        res += (((res=="") ? "" : " ") +
        convert_number(Dn) + " HUNDRED");
    }


    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN");
    var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY");

    if (tn>0 || one>0)
    {
        if (!(res==""))
        {
            res += " AND ";
        }
        if (tn < 2)
        {
            res += ones[tn * 10 + one];
        }
        else
        {

            res += tens[tn];
            if (one>0)
            {
                res += ("-" + ones[one]);
            }
        }
    }

    if (res=="")
    {
        res = "zero";
    }
    return res;
}

//added by pooja
controllers.controller('MembersController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.entries=[];



        $scope.fillEntries=function(){
            var payload={};

            $http.post(serverAPIAdrs + 'executive/members-list',payload).success(function (data) {
                $scope.entries=data.entries;

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();




    }
]);

controllers.controller('memberDetailsController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.backendUploadDir=backendUploadDir;
        $scope.entries=[];



        $scope.fillEntries=function(){
            var payload={};
            payload["id"]=$route.current.params.id;
            $http.post(serverAPIAdrs + 'executive/member-details',payload).success(function (data) {
                $scope.member_details=data.member_details;
                $scope.present_address=data.present_address;
                $scope.permanent_address=data.permanent_address;
                $scope.work_address=data.work_address;
                $scope.property_address=data.property_address;
                setTimeout(function(){
                    $('a[data-rel^=lightcase]').lightcase();
                    /*$('#homeCarousel').carousel({
                     interval:3000,
                     pause: "false"
                     });*/
                },1000);

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();

    }
]);

controllers.controller('memberPropertyDetailsController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.entries=[];

        $(".members-property .drop-starts p #year-range").datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years",
            endDate: "y"
        });

        $(".dropme-down-1").click(function() {
            $('.dropme-down-1 .plus').toggleClass('minus');
        });
        $(".dropme-down-2").click(function() {
            $('.dropme-down-2 .plus').toggleClass('minus');
        });


        $scope.fillEntries=function(){
            var payload={};
            payload["id"]=$route.current.params.id;

            $http.post(serverAPIAdrs + 'executive/member-property-details',payload).success(function (data) {
                $scope.member_property_detail=data.member_property_detail;
                $scope.member_property_detail1=data.member_property_detail1;
                $scope.member_de=data.member_de;
                $scope.collectedRents=data.collectedRents;
                $scope.collectedRMF=data.collectedRMF;
                $scope.collectedEB=data.collectedEB;
                $scope.propetyVisits=data.propetyVisits;
                $scope.assocMeeting=data.assocMeeting;
                $scope.popertyTax=data.popertyTax;
                $scope.pdfSessionToken=data.pdfSessionToken;
                console.log(data.member_de.parking);

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();
    }
]);

controllers.controller('tenantDetailsController', ['$scope','$route', '$http', '$window','$location','GeneralSharedObject',
    function($scope,$route, $http, $window,$location,GeneralSharedObject) {
        //console.log($route.current.params);

        $scope.serverAPIAdrs=serverAPIAdrs;
        $scope.entries=[];



        $scope.fillEntries=function(){
            var payload={};
            payload["id"]=$route.current.params.id;

            $http.post(serverAPIAdrs + 'executive/tenant-details',payload).success(function (data) {
                $scope.tenant_details=data.tenant_details;
                $scope.tenant_rent_details=data.tenant_rent_details;

            }).error(function () {
                alert("Error try again");
            });
        }
        $scope.fillEntries();

    }
]);
