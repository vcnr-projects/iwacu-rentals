'use strict';
var app = angular.module('app', [
    'ngRoute',          //$routeProvider
    'controllers',       //Our module frontend/web/js/controllers.js
    //'easypiechart',
    'ctodo'
    // ,'ui.bootstrap'
]);

app.config(['$routeProvider', '$httpProvider',
    function($routeProvider, $httpProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/executive/dashboard.html',
                controller: 'DashboardController'
            }).
            when('/allCollections/:type', {
                templateUrl: 'partials/executive/allCollections.html',
                controller: 'AllCollectionsController'
            }).
            when('/recieptCheque', {
                templateUrl: 'partials/executive/recieptCheque.html',
                controller: 'RecieptChequeController'
            }).
            when('/travelDetails', {
                templateUrl: 'partials/executive/travelDetails.html',
                controller: 'TravelDetailsController'
            }).
            when('/propertyVisit', {
                templateUrl: 'partials/executive/propertyVisit.html',
                controller: 'PropertyVisitController'
            }).
            when('/propertyVisitedDetails/:id', {
                templateUrl: 'partials/executive/propertyVisitedDetails.html',
                controller: 'PropertyVisitedDetailsController'
            }).
            when('/associationMeeting', {
                templateUrl: 'partials/executive/associationMeeting.html',
                controller: 'AssociationMeetingController'
            }).
            when('/dailyAccounting', {
                templateUrl: 'partials/executive/dailyAccounting.html',
                controller: 'DailyAccountingController'
            }).
            when('/requestList', {
                templateUrl: 'partials/executive/requestList.html',
                controller: 'RequestListController'
            }).
            when('/requestDetail/:id', {
                templateUrl: 'partials/executive/requestDetail.html',
                controller: 'RequestDetailController'
            }).
            when('/contact', {
                templateUrl: 'partials/contact.html',
                controller: 'ContactController'
            }).
            when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'LoginController'
            }).
            when('/dashboard', {
                templateUrl: 'partials/dashboard.html',
                controller: 'DashboardController'
            }).
            /*when('/requestDetail/:id', {
                templateUrl: 'partials/executive/association_meeting_details.html',
                controller: 'AssociationMeetingDetailsController'
            }).*/

            //begin:added by pooja
            when('/members', {
                templateUrl: 'partials/executive/members.html',
                controller: 'MembersController'
            }).
            when('/members-details/:id', {
                templateUrl: 'partials/executive/members-details.html',
                controller: 'memberDetailsController'
            })
            .when('/members-property-details/:id', {
                templateUrl: 'partials/executive/members-property-details.html',
                controller: 'memberPropertyDetailsController'
            })
            .when('/tenant-details/:id', {
                templateUrl: 'partials/executive/tenant_details.html',
                controller: 'tenantDetailsController'
            }).

            //end:added by pooja
            otherwise({
                templateUrl: 'partials/404.html'
            });
        $httpProvider.interceptors.push('authInterceptor');
    }
]);

app.factory('authInterceptor', function ($q, $window, $location) {
    return {
        request: function (config) {
            if ($window.sessionStorage.access_token) {
                //HttpBearerAuth
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
                config.headers["aAuthorization"]= 'Bearer ' + $window.sessionStorage.access_token;
            }
            return config;
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                window.location.assign("login.html")
                //$location.path('/login').replace();
            }
            return $q.reject(rejection);
        }
    };
});


app.run(function($rootScope, $location, $anchorScroll, $routeParams) {
    //when the route is changed scroll to the proper element.
    $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
        $location.hash($routeParams.scrollTo);
        $anchorScroll();
    });
});


app.service('SessionService', function ($http, $q, $rootScope) {

    this.initDone = false,
        this.session = {},
        this.init = function(){
            if (!this.initDone){
                var s = this;
                var deferred = $q.defer();

                $http.get(serverAPIAdrs + 'executive/login-details').success(
                    function (data) {
                        //alert(data);
                        console.log(data);
                        s.session["username"] = data.username;
                        s.session["photo"] = data.photo;
                        s.session["crsa"] = data.crsa;
                        s.session["rsas"] = data.rsas;
                        s.session["newrsa"] = data.newrsa;
                        deferred.resolve(data);
                        s.initDone = true;
                    }).error(
                    function (data) {
                        //delete $window.sessionStorage.access_token;
                        window.location.assign("login.html")
                    }
                );
                return deferred.promise;
            }
        };


    this.getInitDone = function () {
        return this.initDone;
    }
});

app.service('GeneralSharedObject', function () {
    var mutableObject = {};

    return {
        mutableObject:mutableObject,
        getProperty: function () {
            return mutableObject;
        },
        setProperty: function(value) {
            mutableObject = value;
        }
    };
});

app.directive('script', function($parse, $rootScope, $compile) {
    return {
        restrict: 'E',
        terminal: true,
        link: function(scope, element, attr) {
            if (attr.ngSrc) {
                var domElem = '<script src="'+attr.ngSrc+'" async defer></script>';
                $(element).append($compile(domElem)(scope));


            }
        }
    };
});

app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);

            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files);
                });
            });
        }
    };
}]);


app.directive("datepickerjui", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: {
            method:'&onSelect'
        },
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                    //console.log(scope.method);
                        if(methodHandler)
                            methodHandler();

                });
            };

            if(scope.method()) {
                var methodHandler = scope.method();
            }
            var options = {
                dateFormat: "dd/mm/yy",
                onSelect: function (dateText) {
                    updateModel(dateText);

                }
            };
            $(elem).datepicker(options);
        }
    }
});


