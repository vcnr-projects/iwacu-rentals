/**
 *  Custom functions
 *
 **/


! function($) {
    "use strict";

    /**
    Sidebar Module
    */
    var SideBar = function() {
        this.$body = $("body"),
        this.$sideBar = $('aside.left-panel'),
        this.$navbarToggle = $(".navbar-toggle"),
        this.$navbarItem = $("aside.left-panel nav.navigation > ul > li:has(ul) > a")
    };

    //initilizing 
    SideBar.prototype.init = function() {
        //on toggle side menu bar
        var $this = this;
        $(document).on('click', '.navbar-toggle', function () {
            $this.$sideBar.toggleClass('collapsed');
        }); 

        //on menu item clicking
        this.$navbarItem.click(function () {
            if ($this.$sideBar.hasClass('collapsed') == false || $(window).width() < 768) {
                $("aside.left-panel nav.navigation > ul > li > ul").slideUp(300);
                $("aside.left-panel nav.navigation > ul > li").removeClass('active');
                if (!$(this).next().is(":visible")) {
                    $(this).next().slideToggle(300, function () {
                        $("aside.left-panel:not(.collapsed)").getNiceScroll().resize();
                    });
                    $(this).closest('li').addClass('active');
                }
                return false;
            }
        });

        //adding nicescroll to sidebar
        if ($.isFunction($.fn.niceScroll)) {
            $("aside.left-panel:not(.collapsed)").niceScroll({
                cursorcolor: '#8e909a',
                cursorborder: '0px solid #fff',
                cursoropacitymax: '0.5',
                cursorborderradius: '0px'
            });
        }
    },

    //exposing the sidebar module
    $.SideBar = new SideBar, $.SideBar.Constructor = SideBar
    
}(window.jQuery),

!function($) {
    "use strict";

    var TodoApp = function() {
        this.$body = $("body"),
            this.$todoContainer = $('#todo-container'),
            this.$todoMessage = $("#todo-message"),
            this.$todoRemaining = $("#todo-remaining"),
            this.$todoTotal = $("#todo-total"),
            this.$archiveBtn = $("#btn-archive"),
            this.$todoList = $("#todo-list"),
            this.$todoDonechk = ".todo-done",
            this.$todoForm = $("#todo-form"),
            this.$todoInput = $("#todo-input-text"),
            this.$todoBtn = $("#todo-btn-submit"),

            this.$todoData = [];

        this.$todoCompletedData = [];
        this.$todoUnCompletedData = [];
    };

    //mark/unmark - you can use ajax to save data on server side
    TodoApp.prototype.markTodo = function(todoId, complete) {
        for(var count=0; count<this.$todoData.length;count++) {
            if(this.$todoData[count].id == todoId) {
                this.$todoData[count].done = complete;
            }
        }
    },
        //adds new todo
        TodoApp.prototype.addTodo = function(todoText) {
            this.$todoData.push({'id': this.$todoData.length, 'text': todoText, 'done': false});
            //regenerate list
            this.generate();
        },
        //Archives the completed todos
        TodoApp.prototype.archives = function() {
            this.$todoUnCompletedData = [];
            for(var count=0; count<this.$todoData.length;count++) {
                //geretaing html
                var todoItem = this.$todoData[count];
                if(todoItem.done == true) {
                    this.$todoCompletedData.push(todoItem);
                } else {
                    this.$todoUnCompletedData.push(todoItem);
                }
            }
            this.$todoData = [];
            this.$todoData = [].concat(this.$todoUnCompletedData);
            //regenerate todo list
            this.generate();
        },
        //Generates todos
        TodoApp.prototype.generate = function() {
            //clear list
            this.$todoList.html("");
            var remaining = 0;
            for(var count=0; count<this.$todoData.length;count++) {
                //geretaing html
                var todoItem = this.$todoData[count];
                if(todoItem.done == true)
                    this.$todoList.prepend('<li class="list-group-item"><input checked type="checkbox" class="todo-done" id="' + todoItem.id + '"><span class="todo-text">' + todoItem.text + '</span></li>');
                else {
                    remaining = remaining + 1;
                    this.$todoList.prepend('<li class="list-group-item"><input type="checkbox" class="todo-done" id="' + todoItem.id + '"><span class="todo-text">' + todoItem.text + '</span></li>');
                }
            }

            //set total in ui
            this.$todoTotal.text(this.$todoData.length);
            //set remaining
            this.$todoRemaining.text(remaining);
        },
        //init todo app
        TodoApp.prototype.init = function () {
            var $this = this;
            //generating todo list
            this.generate();

            //binding archive
            this.$archiveBtn.on("click", function(e) {
                e.preventDefault();
                $this.archives();
                return false;
            });

            //binding todo done chk
            $(document).on("change", this.$todoDonechk, function() {
                if(this.checked)
                    $this.markTodo($(this).attr('id'), true);

                else
                    $this.markTodo($(this).attr('id'), false);
                //regenerate list
                $this.generate();
            });

            //binding the new todo button
            this.$todoBtn.on("click", function() {
                if ($this.$todoInput.val() == "" || typeof($this.$todoInput.val()) == 'undefined' || $this.$todoInput.val() == null) {
                    //sweetAlert("Oops...", "You forgot to enter todo text", "error");
                    $this.$todoInput.focus();
                } else {
                    $this.addTodo($this.$todoInput.val());
                }
            });
        },
        //init TodoApp
        $.TodoApp = new TodoApp, $.TodoApp.Constructor = TodoApp

}(window.jQuery),


//main app module
function($) {
    //creating side bar
    $.SideBar.init();

   // $.TodoApp.init();

    //accordion

    $('.collapse').on('shown.bs.collapse', function(){
        $(this).prev().find(".ion-plus-round").removeClass("ion-plus-round").addClass("ion-minus-round");
    }).on('hidden.bs.collapse', function(){
        $(this).prev().find(".ion-minus-round").removeClass("ion-minus-round").addClass("ion-plus-round");
    });

    $('.collapse').each(function(){
        if(!$(this).hasClass('in')){
            $(this).prev().find(".ion-minus-round").addClass("ion-plus-round");
        }

    });
    //accordion end


    $('.chart-collection').easyPieChart({
        lineWidth: 25,
        size: 165,
        barColor: '#fc8675',
        trackColor: '#fee7e3',
        lineCap: 'square',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
    $('.chart-payment').easyPieChart({
        barColor: '#46bbd3',
        trackColor: '#daf1f6',
        lineWidth: 25,
        size: 165,
        lineCap: 'square',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
    $('.chart-visit').easyPieChart({
        barColor: '#e8bd6a',
        trackColor: '#faf2e1',
        lineWidth: 25,
        size: 165,
        lineCap: 'square',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
    $('.chart-meeting').easyPieChart({
        barColor: '#85969a',
        trackColor: '#f5f5f5',
        lineWidth: 25,
        size: 165,
        lineCap: 'square',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
    $('.chart-service').easyPieChart({
        barColor: '#8dc63f',
        trackColor: '#e8f4d9',
        lineWidth: 25,
        size: 165,
        lineCap: 'square',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });


}(window.jQuery);
