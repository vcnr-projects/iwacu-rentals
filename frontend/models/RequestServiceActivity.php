<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "request_service_activity".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $property_id
 * @property string $tenant_id
 * @property string $member_id
 * @property integer $user_id
 * @property integer $exec_id
 * @property string $priority
 * @property string $proposed_cost
 * @property string $accepted_cost
 * @property integer $admin_approval
 * @property integer $member_approval
 * @property integer $is_payable
 * @property integer $status
 * @property integer $owner
 *
 * @property User $owner0
 * @property PropertyUnits $property
 * @property TenantPropertyAllocation $tenant
 * @property Service $member
 * @property User $user
 * @property User $exec
 */
class RequestServiceActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_service_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'end_date'], 'required'],
            [['description'], 'string'],
            [['start_date', 'end_date','completed_date'], 'safe'],
            [['property_id', 'tenant_id', 'member_id', 'user_id', 'exec_id', 'admin_approval', 'member_approval', 'is_payable', 'status', 'owner'], 'integer'],
            [['proposed_cost', 'accepted_cost','final_cost'], 'number'],
            [['name','request_id'], 'string', 'max' => 200],
            [['priority'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Service Name',
            'description' => 'Description',
            'start_date' => 'Probable Start Date',
            'end_date' => 'Finish Date',
            'property_id' => 'Property',
            'tenant_id' => 'Tenant',
            'member_id' => 'Member',
            'user_id' => 'User',
            'exec_id' => 'Executive',
            'priority' => 'Priority',
            'proposed_cost' => 'Proposed Cost',
            'accepted_cost' => 'Accepted Cost',
            'admin_approval' => 'Admin Approval',
            'member_approval' => 'Member Approval',
            'is_payable' => 'Is Payable',
            'status' => 'Status',
            'owner' => 'Owner',
            'request_id' => 'Request ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(TenantPropertyAllocation::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Service::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(!$this->request_id){
                $this->request_id=$this->getLatestRequestID();
            }




            return true;
        } else {
            return false;
        }
    }

    public  function getLatestRequestID(){



        /*if($this->user_type=='Executive')
            $usertypecode='VHP-EMPID';
        else{
            $usertypecode="VHP-$this->user_type";
        }*/

        $pref="VHP-SERVREQ";
        //$pref=$usertypecode;

        $lbn=0001;

        $sql="select max(request_id) as mname from request_service_activity ";
        $cmd = \Yii::$app->db->createCommand($sql);

        $result = $cmd->queryAll();
        //$lbn=6000;
        foreach ($result as $row){
            preg_match('/\d+$/',$row["mname"],$match);
            if(isset($match[0])&&$match[0]) {
                Yii::trace(VarDumper::dumpAsString($match),'vardump');
                $lbn = $match[0];
                $lbn++;
            }

        }

        /* $sql="select * from auto_no_init where name like 'batch' ";
         $cmd = Yii::app()->db->createCommand($sql);
         $res=$cmd->queryRow();
         if($res&&$res["reset"]==1){
             preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
             // var_dump($match);
             if(isset($match[1]))
                 $pref=$match[1];
             if(isset($match[2]))
                 $lbn=$match[2];

         }elseif($res){
             // var_dump($res["slno"]);
             preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
             //  var_dump($match);
             if(isset($match[1]))
                 $pref=$match[1];
         }*/
        //$billno="MED/". date("Y")."/"."INVOICE/".sprintf("%05d", $lbn+1);
        //$billno=date("Y")."/".$pref.sprintf("%05d", $lbn);
        $billno=$pref.sprintf("%05d", $lbn);
        return $billno;

    }

    public function saveMemberApproval(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if($this->save()){
               //return ["success"=>true];
               $eal=new ExecutiveActivityLog();
               $eal->exec_id=$this->exec_id;
               $eal->property_id=$this->property_id;
               $eal->member_id=$this->owner;
               $eal->tenant_id=null;
               $eal->collection=1;
               $eal->payment=1;
               $eal->is_payable=1;
               $eal->type=$this->name;
               $eal->date=$this->start_date;
               $eal->status=0;
               $eal->category=0;
               $eal->rsa_id=$this->id;
                if(!$eal->save()){
                    foreach($eal->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                    throw new \Exception ( $errmsg);
                }
            }else{

                foreach($this->getErrors() as $msg)
                    foreach($msg as $m)
                        $errmsg.="$m";
                throw new \Exception ( $errmsg);
            }


            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;

    }
}
