<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "executive_activity_log".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $property_id
 * @property integer $member_id
 * @property integer $tenant_id
 * @property integer $collection
 * @property integer $payment
 * @property integer $is_payable
 * @property string $remarks
 * @property string $type
 * @property integer $activity_id
 * @property string $date
 * @property integer $status
 * @property integer $category
 *
 * @property User $exec
 * @property User $member
 * @property User $tenant
 * @property ExecutiveActivityProperty $activity
 * @property PropertyUnits $property
 */
class ExecutiveActivityLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'property_id', 'collection', 'payment', 'is_payable', 'type', 'date'], 'required'],
            [['exec_id', 'property_id', 'member_id', 'tenant_id', 'collection', 'payment', 'is_payable', 'activity_id', 'status','category'], 'integer'],
            [['remarks'], 'string'],
            [['date'], 'safe'],
            [['type'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive ID',
            'property_id' => 'Property ID',
            'member_id' => 'Member ID',
            'tenant_id' => 'Tenant ID',
            'collection' => 'Collection status',
            'payment' => 'Payment Status',
            'is_payable' => 'Is Payable?',
            'remarks' => 'Remarks',
            'type' => 'Activity Type',
            'activity_id' => 'Activity',
            'date' => 'Date',
            'status' => 'Status',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(User::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(ExecutiveActivityProperty::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property_id']);
    }


    public function getLatestTransactionNo(){
        return "ABDRE345345";
    }

    public function collectCash($amount,$date){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if(ExecutiveActivityTxnDetails::findOne(["activity_log_id"=>$this->id])){
                throw new \Exception("Invalid input transaction already done");
            };

            $sql="update executive_activity_log set collection=2 where   id=:id and exec_id=". Yii::$app->user->id;
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$this->id);
            if($cmd->execute()){

                $txn=new ExecutiveActivityTxnDetails();
                $txn->activity_log_id=$this->id;
                $txn->amount=$amount;
                $txn->date=$date;
                $txn->mode="Cash";
                $txn->txn_no="ABCD4534";
                if(!$txn->save()){
                    foreach($txn->getErrors() as $msg)
                        foreach($msg as $m)
                        $errmsg.="$m";
                }
            }






            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;

    }

    public function depositCashPayment($file,$date){
        Yii::trace(VarDumper::dumpAsString('insidedpeositcashpayment'),'vardump');
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {

            $collect=ExecutiveActivityTxnDetails::findOne([
                "activity_log_id"=>$this->id
                ,"type"=>"Collection"
            ]);
            if(!$collect){
                $errmsg.="Collection didn't happen for this activity";
                throw new \yii\base\Exception($errmsg);
            }

            $sql="update executive_activity_log
set payment=2 ,status=1 where   id=:id and exec_id=". Yii::$app->user->id;
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$this->id);
            if($cmd->execute()){

                $txn=new ExecutiveActivityTxnDetails();
                $txn->activity_log_id=$this->id;
                $txn->amount=$collect->amount;
                $txn->date=$date;
                $txn->mode="Cash";
                $txn->type="Payment";
                $txn->txn_no="ABCD4534";
                if(!$txn->save()){
                    foreach($txn->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }

                $txn_files=new ExecutiveActivityTxnFiles();
                $txn_files->txn_id=$txn->id;
                $txn_files->file_path=$file->name;
                if(!$txn_files->save()){
                    foreach($txn_files->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }
            }


            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;

    }


    public function collectCheque($data){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if(ExecutiveActivityTxnDetails::findOne(["activity_log_id"=>$this->id])){
                throw new \Exception("Invalid input transaction already done");
            };

            $sql="update executive_activity_log set collection=2 where   id=:id and exec_id=". Yii::$app->user->id;
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$this->id);
            if($cmd->execute()){

                $txn=new ExecutiveActivityTxnDetails();
                $txn->activity_log_id=$this->id;

                $txn->amount=isset($data['amount'])?$data['amount']:null;
                $txn->amount=str_replace("/-","",$txn->amount);
                $txn->date=(isset($data['date'])&&strtotime(str_replace('/', '-', $data['date']))!==false)?date('Y-m-d',strtotime(str_replace('/', '-', $data['date']))):null;;
                $txn->cheque_date=(isset($data['cheque_date'])&&strtotime(str_replace('/', '-', $data['cheque_date']))!==false)?date('Y-m-d',strtotime(str_replace('/', '-', $data['cheque_date']))):null;;
                $txn->mode=isset($data['mode'])?$data['mode']:null;
                $txn->txn_no="ABCD4534";
                $txn->type="Collection";
                $txn->cheque_no=isset($data['cheque_no'])?$data['cheque_no']:null;
                $txn->branch=isset($data['branch'])?$data['branch']:null;
                $txn->bank=isset($data['bank'])?$data['bank']:null;
                $txn->transaction_no=isset($data['transaction_no'])?$data['transaction_no']:null;
                Yii::trace(VarDumper::dumpAsString($txn),'vardump');
                if(!$txn->save()){
                    foreach($txn->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }
            }






            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;

    }


    public function saveBillSlipUploads($billUploadFile,$slipUploadFile,$billAmount,$paidAmount,$paidPerson){
        Yii::trace(VarDumper::dumpAsString('insidesaveBillSlipUploads'),'vardump');
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';

        try {

            if($this->status==2){
                $errmsg.="Activity already done.";
                throw new \yii\base\Exception($errmsg);
            }

            if($billAmount<=0){
                $errmsg.="Bill Amount cannot be less than zero.";
                  throw new \yii\base\Exception($errmsg);
            }
            if($paidAmount<=0){
                $errmsg.="Paid Amount cannot be less than zero.";
                throw new \yii\base\Exception($errmsg);
            }
            if(empty($paidPerson)){
                $errmsg.="Paid Person cannot be emtpy";
                throw new \yii\base\Exception($errmsg);
            }

            $collect=new ExecutiveActivityTxnDetails();
            $collect->activity_log_id=$this->id;
            $collect->amount=$billAmount;
            $collect->date=date('Y-m-d');
            $collect->mode="Bill Uploads";
            $collect->txn_no=$this->getLatestTransactionNo();
            $collect->type="Bill Collection";
            if(!$collect->save()){
                foreach($collect->getErrors() as $msg)
                    foreach($msg as $m)
                        $errmsg.="$m";
            }else{
                if($billUploadFile) {
                    $txn_files = new ExecutiveActivityTxnFiles();
                    $txn_files->txn_id = $collect->id;
                    $txn_files->file_path = $billUploadFile->name;
                    if (!$txn_files->save()) {
                        foreach ($txn_files->getErrors() as $msg)
                            foreach ($msg as $m)
                                $errmsg .= "$m";
                    }
                }else{
                    $errmsg.="Bill file is required";
                    throw new \yii\base\Exception($errmsg);
                }
            }

            $payment=new ExecutiveActivityTxnDetails();
            $payment->activity_log_id=$this->id;
            $payment->amount=$paidAmount;
            $payment->date=date('Y-m-d');
            $payment->mode="Slip Uploads";
            $payment->txn_no=$this->getLatestTransactionNo();
            $payment->type="Slip Deposit";
            $payment->transaction_no=$paidPerson;
            if(!$payment->save()){
                foreach($collect->getErrors() as $msg)
                    foreach($msg as $m)
                        $errmsg.="$m";
            }else{
                if($slipUploadFile) {
                    $txn_files = new ExecutiveActivityTxnFiles();
                    $txn_files->txn_id = $payment->id;
                    $txn_files->file_path = $slipUploadFile->name;
                    if (!$txn_files->save()) {
                        foreach ($txn_files->getErrors() as $msg)
                            foreach ($msg as $m)
                                $errmsg .= "$m";
                    }
                }else{
                    $errmsg.="Slip file is required";
                    throw new \yii\base\Exception($errmsg);
                }
            }

            if($paidAmount>0)
            $sql="update executive_activity_log set  status=2 , payment=2,collection=2 where   id=:id and exec_id=". Yii::$app->user->id;
            else
            $sql="update executive_activity_log set  status=2 where   id=:id and exec_id=". Yii::$app->user->id;
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$this->id);
            if(!$cmd->execute()){
                $errmsg.="Update log error";
            }


           /* if($billUploadFile){
                $collect=ExecutiveActivityTxnDetails::findOne([
                    "activity_log_id"=>$this->id
                    ,"type"=>"Collection"
                ]);
                if($collect){
                    $txn_files=ExecutiveActivityTxnFiles::findOne(
                      ["txn_id"=>$collect->id]
                    );
                    if($txn_files){
                        $txn_files->file_path=$billUploadFile->name;
                        if(!$txn_files->save()){
                            foreach($txn_files->getErrors() as $msg)
                                foreach($msg as $m)
                                    $errmsg.="$m";
                        }else{

                        }
                    }
                }else{
                    $collect=new ExecutiveActivityTxnDetails();
                    $collect->activity_log_id=$this->id;
                    $collect->amount=0;
                    $collect->date=date('Y-m-d');
                    $collect->mode="Bill Uploads";
                    $collect->txn_no=$this->getLatestTransactionNo();
                    $collect->type="Collection";
                    if(!$collect->save()){
                        foreach($collect->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                    }else{
                        $txn_files=new ExecutiveActivityTxnFiles();
                        $txn_files->txn_id=$collect->id;
                        $txn_files->file_path=$billUploadFile->name;
                        if(!$txn_files->save()){
                            foreach($txn_files->getErrors() as $msg)
                                foreach($msg as $m)
                                    $errmsg.="$m";
                        }
                    }
                }
                $sql="update executive_activity_log set  status=1 where   id=:id and exec_id=". Yii::$app->user->id;
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$this->id);
                if(!$cmd->execute()){
                    $errmsg.="Update log error";
                }
            }*/

            /*if($slipUploadFile){

                $collect=ExecutiveActivityTxnDetails::findOne([
                    "activity_log_id"=>$this->id
                    ,"type"=>"Collection"
                ]);
                if(!$collect){
                    $errmsg.="Bill not uploaded";
                    throw new \yii\base\Exception($errmsg);
                }

                $collect=ExecutiveActivityTxnDetails::findOne([
                    "activity_log_id"=>$this->id
                    ,"type"=>"Payment"
                ]);
                if($collect){
                    $txn_files=ExecutiveActivityTxnFiles::findOne(
                        ["txn_id"=>$collect->id]
                    );
                    if($txn_files){
                        $txn_files->file_path=$billUploadFile->name;
                        if(!$txn_files->save()){
                            foreach($txn_files->getErrors() as $msg)
                                foreach($msg as $m)
                                    $errmsg.="$m";
                        }else{

                        }
                    }
                }else{
                    $collect=new ExecutiveActivityTxnDetails();
                    $collect->activity_log_id=$this->id;
                    $collect->amount=0;
                    $collect->date=date('Y-m-d');
                    $collect->mode="Slip Uploads";
                    $collect->txn_no=$this->getLatestTransactionNo();
                    $collect->type="Payment";
                    if(!$collect->save()){
                        foreach($collect->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                    }else{
                        $txn_files=new ExecutiveActivityTxnFiles();
                        $txn_files->txn_id=$collect->id;
                        $txn_files->file_path=$slipUploadFile->name;
                        if(!$txn_files->save()){
                            foreach($txn_files->getErrors() as $msg)
                                foreach($msg as $m)
                                    $errmsg.="$m";
                        }
                    }
                }
                $sql="update executive_activity_log set  status=2 where   id=:id and exec_id=". Yii::$app->user->id;
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$this->id);
                if(!$cmd->execute()){
                    $errmsg.="Update log error";
                }
            }*/



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                //$transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            $transaction->rollBack();
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;

    }


    public function addPropertyVisit($comment,$billUploadFiles,$slipUploadFiles,$videoUrl,$videoUrls,$visitStatus){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
                $collect=new ExecutiveActivityTxnDetails();
                $collect->activity_log_id=$this->id;
                $collect->amount=0;
                $collect->date=date('Y-m-d');
                $collect->mode="Photo Uploads";
                $collect->txn_no=$this->getLatestTransactionNo();
                $collect->transaction_no=$visitStatus ;
                $collect->type="Property Visit Uploads";
                if(!$collect->save()){
                    foreach($collect->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }else{
                    foreach($billUploadFiles as $billUploadFile) {
                        $txn_files = new ExecutiveActivityTxnFiles();
                        $txn_files->txn_id = $collect->id;
                        $txn_files->file_path = $billUploadFile->name;
                        if (!$txn_files->save()) {
                            foreach ($txn_files->getErrors() as $msg)
                                foreach ($msg as $m)
                                    $errmsg .= "$m";
                        }
                    }
                }



                $collect=new ExecutiveActivityTxnDetails();
                $collect->activity_log_id=$this->id;
                $collect->amount=0;
                $collect->date=date('Y-m-d');
                $collect->mode="Video Uploads";
                $collect->txn_no=$this->getLatestTransactionNo();
                $collect->type="Property Visit Uploads";
                if(!$collect->save()){
                    foreach($collect->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }else{


                    if (empty($videoUrl)||!filter_var($videoUrl, FILTER_VALIDATE_URL) === false) {

                    } else {
                        $errmsg.="$videoUrl is not a valid URL. ";
                    }

                    if(!empty($videoUrl)) {
                        $txn_files = new ExecutiveActivityTxnFiles();
                        $txn_files->txn_id = $collect->id;
                        $txn_files->file_path = $videoUrl;
                        if (!$txn_files->save()) {
                            foreach ($txn_files->getErrors() as $msg)
                                foreach ($msg as $m)
                                    $errmsg .= "$m";
                        }
                    }


                    foreach($videoUrls as $vidU){
                        /*if (empty($vidU)||!filter_var($vidU, FILTER_VALIDATE_URL) === false) {

                        } else {
                            $errmsg.="$vidU is not a valid URL. ";
                        }*/

                        if(!empty($vidU)) {
                            $txn_files = new ExecutiveActivityTxnFiles();
                            $txn_files->txn_id = $collect->id;
                            $txn_files->file_path = $vidU;
                            if (!$txn_files->save()) {
                                foreach ($txn_files->getErrors() as $msg)
                                    foreach ($msg as $m)
                                        $errmsg .= "$m";
                            }
                        }
                    }

                    /*foreach($slipUploadFiles as $billUploadFile) {
                        $txn_files = new ExecutiveActivityTxnFiles();
                        $txn_files->txn_id = $collect->id;
                        $txn_files->file_path = $billUploadFile->name;
                        if (!$txn_files->save()) {
                            foreach ($txn_files->getErrors() as $msg)
                                foreach ($msg as $m)
                                    $errmsg .= "$m";
                        }
                    }*/
                }


            $this->remarks=$comment;
            $this->status=2;
            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    foreach($msg as $m)
                        $errmsg.="$m";
            }



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                //$transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            $transaction->rollBack();
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;
    }

    public function addAssociationMeeting($data,$billUploadFiles,$slipUploadFiles){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';

        try {

            if(empty($data["date"])&&strtotime(str_replace("/","-",$data["date"]))!==false){
                $errmsg.="Date not valid";
            }else{
                $collect=new ExecutiveActivityTxnDetails();
                $collect->activity_log_id=$this->id;
                $collect->amount=0;
                $collect->date=date('Y-m-d',strtotime($data["date"]));
                $collect->mode="Meeting Details";
                $collect->txn_no=$this->getLatestTransactionNo();
                $collect->type="Association Meeting Uploads";
                $collect->branch=$data["venue"];
                $collect->transaction_no=$data["time"];
                if(!$collect->save()){
                    foreach($collect->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }
            }

            foreach($billUploadFiles as $billUploadFile){
                $collect=new ExecutiveActivityTxnDetails();
                $collect->activity_log_id=$this->id;
                $collect->amount=0;
                $collect->date=date('Y-m-d');
                $collect->mode="Photo Uploads";
                $collect->txn_no=$this->getLatestTransactionNo();
                $collect->type="Property Visit Uploads";
                if(!$collect->save()){
                    foreach($collect->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }else{
                    $txn_files=new ExecutiveActivityTxnFiles();
                    $txn_files->txn_id=$collect->id;
                    $txn_files->file_path=$billUploadFile->name;
                    if(!$txn_files->save()){
                        foreach($txn_files->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                    }
                }
            }

            foreach($slipUploadFiles as $billUploadFile){
                $collect=new ExecutiveActivityTxnDetails();
                $collect->activity_log_id=$this->id;
                $collect->amount=0;
                $collect->date=date('Y-m-d');
                $collect->mode="Video Uploads";
                $collect->txn_no=$this->getLatestTransactionNo();
                $collect->type="Property Visit Uploads";
                if(!$collect->save()){
                    foreach($collect->getErrors() as $msg)
                        foreach($msg as $m)
                            $errmsg.="$m";
                }else{
                    $txn_files=new ExecutiveActivityTxnFiles();
                    $txn_files->txn_id=$collect->id;
                    $txn_files->file_path=$billUploadFile->name;
                    if(!$txn_files->save()){
                        foreach($txn_files->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                    }
                }
            }

            $this->remarks=$data["summary"];
            $this->status=2;
            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    foreach($msg as $m)
                        $errmsg.="$m";
            }



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                //$transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            $transaction->rollBack();
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);

            return false;
        }
        return true;
    }


}
