<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "todo_list".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $todo
 * @property integer $status
 * @property string $date
 *
 * @property User $exec
 */
class TodoList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'todo_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'todo', 'date'], 'required'],
            [['exec_id', 'status'], 'integer'],
            [['date'], 'safe'],
            [['todo'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive ID',
            'todo' => 'To Do',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            Yii::trace(VarDumper::dumpAsString(!strtotime($this->date)),'vardump');
            if(!strtotime($this->date)){
                $this->date=date('Y-m-d',strtotime($this->date));
            }else{
                $this->date=date('Y-m-d H:i:s');
            }

            return true;
        } else {
            return false;
        }
    }
}
