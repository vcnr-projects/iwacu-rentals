<?php
namespace app\models;
use Yii;
use yii\helpers\VarDumper;

class ExecutivePendingActivity {

    public static function  insertPendingActivity(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $success=true;
                $sql="SELECT eap.*,s.property_id as sproperty_id FROM `executive_activity_property` eap
 left join service s on s.id=eap.owner_service and s.is_active=1
 left join tenant_property_allocation tap on tap.id=eap.tenant_service and tap.is_active=1
          WHERE date(now()) between
          DATE_sub(eap.next_collection_date,INTERVAL eap.remainder_days DAY) and eap.next_collection_date
          and ( select if(count(*)>0,false,true) from executive_activity_log eal where eal.activity_id=eap.id and  eal.date >= eap.next_collection_date )
            and eap.is_active=1
            and if(eap.owner_service is not null ,s.is_active is not null,true )
            and if(eap.tenant_service is not null ,tap.is_active is not null,true )
      ";
            $cmd=Yii::$app->db->createCommand($sql);
            $res=$cmd->queryAll();
            foreach($res as  $r){

                if($r["owner_service"]){
                    $sql="
                    select s.property_id,ep.exec_id,s.member_id,null as tenant_id ,eat.category
                    from executive_activity_property eap,service s,executive_properties ep
                    ,executive_activity_type eat
                     where eap.owner_service=s.id and ep.property=s.property_id and ep.is_active=1
                   and s.is_active=1
                   and eat.type=eap.type
                   and eap.id=:id
                    ";
                }else{

                    $sql="
                    select tap.prop_id as property_id ,ep.exec_id,s.member_id,tap.tenant_id,eat.category
                    from  executive_activity_property eap,tenant_property_allocation tap
                    ,executive_properties ep,service s
                    ,executive_activity_type eat
                    where eap.tenant_service=tap.id and tap.is_active=1
                    and tap.prop_id=ep.property and ep.is_active=1
                    and tap.service_id=s.id and s.is_active=1
                    and eat.type=eap.type
                     and eap.id=:id

                    ";

                }
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$r["id"]);
                $res1=$cmd->queryOne();
                if(!$res1)continue;


               /* $sql="select u.id as exec_id,ep.property as property_id ,tap.tenant_id
 from
executive_activity_property eap
left join tenant_property_allocation tap
on eap.tenant_service=tap.id and tap.is_active=1
left join executive_properties ep on  tap.prop_id=ep.property and ep.is_active=1
left join user u on ep.exec_id=u.id
                    where     eap.id={$r['id']}
                    ";
                $cmd=Yii::$app->db->createCommand($sql);
                $res1=$cmd->queryOne();

                $sql="select
  s.member_id
 from service s
 where s.property_id=:prop_id and s.is_active=1

                  ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":prop_id",$res1["property_id"]);
                $member=$cmd->queryScalar();*/

                $eal=new \app\models\ExecutiveActivityLog();
                $eal->exec_id=$res1["exec_id"];

                $eal->property_id = $res1["property_id"];

                $eal->member_id=$res1["member_id"];
                $eal->category=$res1["category"];
                //$eal->member_id=
                $eal->tenant_id=$res1["tenant_id"];
                $eal->collection=$r["collection"];
                //$eal->collection=0;
                $eal->payment=$r["payment"];
                //$eal->payment=0;
                $eal->is_payable=$r["payment"];
                //$eal->remarks=
                $eal->type=$r["type"];
                $eal->date=$r["next_collection_date"];
                $eal->activity_id=$r["id"];
                /*if($eal->collection==0&&$eal->payment==0){
                    $eal->is_payable=-1;
                }*/
                /*if(empty($eal->tenant_id)){
                    $success=false;
                }*/
                if($eal->save()&&$success) {
                    $sql = "update  executive_activity_property set next_collection_date=date_add(next_collection_date, INTERVAL recursion month) where id= " . $r["id"];
                    $cmd = Yii::$app->db->createCommand($sql);
                    $cmd->execute();
                }else{
                    $success=false;
                    Yii::trace(VarDumper::dumpAsString($eal->getErrors()),'vardump');
                }
            }

            if($success)
            $transaction->commit();
            else{
                $transaction->rollBack();
                return false;
            }


        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
                $transaction->rollBack();

            return false;
        }
        return true;
    }


    public static function dateRangeQuery($table,$dateColumn,$fromDate,$toDate){
        $sql="select * from $table where date($dateColumn) between date($fromDate) and date($toDate) ";
        $cmd=Yii::$app->db->createCommand($sql);

        return $cmd->queryAll();

    }
}