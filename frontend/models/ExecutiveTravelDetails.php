<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_travel_details".
 *
 * @property string $id
 * @property string $from_location
 * @property string $to_location
 * @property string $purpose
 * @property string $distance
 * @property integer $exec_id
 * @property string $date
 *
 * @property User $exec
 */
class ExecutiveTravelDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_travel_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_location', 'to_location', 'purpose', 'distance', 'exec_id', 'date'], 'required'],
            [['purpose'], 'string'],
            [['distance'], 'number'],
            [['exec_id'], 'integer'],
            [['date'], 'safe'],
            [['from_location', 'to_location'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_location' => 'From Location',
            'to_location' => 'To Location',
            'purpose' => 'Purpose',
            'distance' => 'Distance',
            'exec_id' => 'Executive ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }
}
