<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_activity_txn_details".
 *
 * @property string $id
 * @property string $txn_no
 * @property string $date
 * @property string $activity_log_id
 * @property string $amount
 * @property string $mode
 * @property string $type
 * @property string $cheque_no
 * @property string $cheque_date
 * @property string $branch
 * @property string $bank
 * @property string $transaction_no
 *
 * @property ExecutiveActivityLog $activityLog
 * @property ExecutiveActivityTxnFiles[] $executiveActivityTxnFiles
 */
class ExecutiveActivityTxnDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_txn_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['txn_no', 'date', 'activity_log_id', 'amount', 'mode'], 'required'],
            [['date', 'cheque_date'], 'safe'],
            [['activity_log_id'], 'integer'],
            [['amount'], 'number'],
            [['txn_no', 'mode', 'type', 'cheque_no', 'transaction_no'], 'string', 'max' => 100],
            [['branch', 'bank'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'txn_no' => 'Transaction ID',
            'date' => 'Date',
            'activity_log_id' => 'Log ID',
            'amount' => 'Amount',
            'mode' => 'Mode of Payment',
            'type' => 'Type of Transaction',
            'cheque_no' => 'Cheque No',
            'cheque_date' => 'Cheque Date',
            'branch' => 'Branch',
            'bank' => 'Bank',
            'transaction_no' => 'Transaction No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityLog()
    {
        return $this->hasOne(ExecutiveActivityLog::className(), ['id' => 'activity_log_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveActivityTxnFiles()
    {
        return $this->hasMany(ExecutiveActivityTxnFiles::className(), ['txn_id' => 'id']);
    }
}
