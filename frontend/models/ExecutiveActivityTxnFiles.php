<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_activity_txn_files".
 *
 * @property string $id
 * @property string $file_path
 * @property string $txn_id
 *
 * @property ExecutiveActivityTxnDetails $txn
 */
class ExecutiveActivityTxnFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_txn_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_path', 'txn_id'], 'required'],
            [['txn_id'], 'integer'],
            //[['file_path'], 'file'],
            [['file_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_path' => 'File',
            'txn_id' => 'Transaction ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTxn()
    {
        return $this->hasOne(ExecutiveActivityTxnDetails::className(), ['id' => 'txn_id']);
    }
}
