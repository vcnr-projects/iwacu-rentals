<?php

namespace frontend\controllers;

use app\models\ExecutiveActivityLog;
use app\models\ExecutivePendingActivity;
use app\models\RequestServiceActivity;
use app\models\TodoList;
use app\modules\service\models\ExecutiveActivityProperty;
use common\models\User;
use frontend\components\AccessRule;
use Yii;
use common\models\LoginForm;
use yii\filters\ContentNegotiator;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;

class MemberController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['dashboard','login-details'
                ,'sendrequest'
                ,'request-detail'
                ,'rsa-approve'
                ,'view-rsa'
                ,'approve-rsa'
                ,'set-request-services-as-viewed'
                ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            // We will override the default rule config with the new AccessRule class
            'ruleConfig' => [
                'class' => AccessRule::className(),
            ],
            'only' => ['dashboard','loginDetails','sendrequest'
                ,'viewRsa'
                ,'approveRsa'
                ,'setRequestServicesAsViewed'
            ],
            'rules' => [
                [
                    'actions' => ['dashboard','loginDetails','sendrequest','ViewRsa','setRequestServicesAsViewed'],
                    'allow' => true,
                    'roles' => ['Individual'],
                ],
            ],
        ];
        return $behaviors;
    }


    public function actionLoginDetails(){
        $sql="select profile_pic_path from tenant t where t.member_id= ". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $file=$cmd->queryOne();
        $file_path='';
        if($file){
            $file_path=$file["profile_pic_path"];
        }

        $sql="select pu.name ,s.id from service s,property_units pu
where s.is_active=1 and pu.id=s.property_id and s.member_id=". Yii::$app->user->id ;
        $cmd=Yii::$app->db->createCommand($sql);
        $prop=$cmd->queryAll();



        $response = array_merge([
            'username' => Yii::$app->user->identity->username,
            'photo' => $file_path,
            'properties'=>$prop,

            //'access_token' => Yii::$app->user->identity->getAuthKey(),
        ],$this->getOwnerRequestNotification());
        return $response;
    }


    public function actionRequestservicetypes(){
        $data=Yii::$app->getRequest()->getBodyParams();
        $sql="select rs.service_name as rs, concat(service_name,if(gs.request_service is  null,' [ Paid Service ]','' )) as name
from request_services rs
left join  (select gs.request_service from  gratuity_services gs  ,service s
where s.id=:id and s.tos=gs.tos
) as gs on rs.service_name=gs.request_service
";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$data["property"]);
        $response=$cmd->queryAll();
        return $response;
    }

    public function actionSendrequest(){
        $data=Yii::$app->getRequest()->getBodyParams();

        $rsa=new RequestServiceActivity();
        if(!empty($data)){
            $rsa->setAttributes($data);
            //$rsa->member_id=Yii::$app->user->id;

            if(!empty($rsa->member_id)){
                $sql="select member_id,property_id from service where id=:id";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$rsa->member_id);
                $res=$cmd->queryOne();
                if($res){
                    $rsa->owner=$res["member_id"];
                    $rsa->property_id=$res["property_id"];
                }

                //begin:ispayable
                $sql="
select gs.id from
 service_type st,gratuity_services gs,service s
where st.name=gs.tos and s.tos=st.name and s.id=:property_id
and gs.request_service=:request_service
";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":property_id",$rsa->member_id);
                $cmd->bindValue(":request_service",$rsa->name);
                $res=$cmd->queryOne();

                if($res){
                    $rsa->is_payable=0;
                }
                //end:ispayable

                //begin:executive
                $sql="
select ep.exec_id from
 executive_properties ep,service s
where ep.service_id=s.id and  s.id=:property_id and s.is_active=1 and ep.is_active=1
";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":property_id",$rsa->member_id);
                $res=$cmd->queryOne();

                if($res){
                    $rsa->exec_id=$res["exec_id"];
                }
                //end:executive

            }







            if($rsa->save()){
                return array_merge(["success"=>true
                    ,"id"=>$rsa->id

                ],$this->getOwnerRequestNotification());
            }else{
                return $rsa;
            }
        }
        throw new \yii\web\HttpException(400, 'Bad Input', 400);
        return $response;
    }


    public function actionViewRsa(){
        $data=Yii::$app->getRequest()->getBodyParams();
        if(isset($data["id"])){

            $res=$this->getRsaDetails($data["id"]);



            if($res){
                $res["owner"]=true;
                return $res;
            }

        }

        throw new \yii\web\HttpException(400, 'Bad Input', 400);
        return ;
    }


    function getOwnerRequestNotification(){
        $sql="select rsa.name,rsa.description,u.username,u.user_type,rsa.id from request_service_activity rsa
          left join service s on s.id=rsa.member_id
          left join tenant_property_allocation tpa on rsa.tenant_id=tpa.id
          left join user u on s.member_id=u.id or tpa.tenant_id=u.id
 where status>=0 and status<3 and owner=". Yii::$app->user->id." order by rsa.id desc limit 5 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $rsas=$cmd->queryAll();

        $sql="select count(*) as c from request_service_activity where status>=0 and status<3 and  owner=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $crsa=$cmd->queryOne()["c"];

        $sql="select count(*) as c from request_service_activity where status>=0 and status<3 and owner_view=0 and  owner=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $new=$cmd->queryOne()["c"];

        return ['rsas'=>$rsas,'crsa'=>$crsa,'newrsa'=>$new];
    }

    public function actionSetRequestServicesAsViewed(){
        $sql="update request_service_activity set owner_view=1 where   owner=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->execute();
        return ["success"=>true];
    }


    public function actionApproveRsa(){
        $data=Yii::$app->getRequest()->getBodyParams();


        if(isset($data["id"])){
            $rsa=RequestServiceActivity::findOne($data["id"]);
            $rsa->status=2;
            $rsa->member_approval=Yii::$app->user->id;
            $rsa->save();


            $res=$this->getRsaDetails($data["id"]);

            if($res){
                $res["owner"]=true;
                return $res;
            }

        }

        throw new \yii\web\HttpException(400, 'Bad Input', 400);
        return ;
    }

    function getRsaDetails($id){
        $sql="select rsa.*,pu.name as property
,date_format(rsa.end_date,'%d-%m-%Y') as fdate
,if(admin_approval is null,'Pending','Approved') as adminaprv
,if(member_approval is null,'Pending','Approved') as finalaprv
,u.username as executive
from request_service_activity rsa
left join  service s on rsa.member_id=s.id
left JOIN  tenant_property_allocation tpa on rsa.tenant_id=tpa.id
left join  property_units pu on (tpa.prop_id=pu.id or s.property_id=pu.id)
left join user u on rsa.exec_id=rsa.id
where rsa.id=:id
";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        return $res=$cmd->queryOne();
    }


    public function actionRequestDetail(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {

                $sql="

select
pu.name as puname
,m.name as mname
,ifnull(t.primary_mobile,t.primary_landline) as contact
,DATE_FORMAT(rsa.end_date,'%d/%m/%Y') as formatedDate
,DATE_FORMAT(rsa.start_date,'%d/%m/%Y') as formatedStartDate
,DATE_FORMAT(rsa.created_on,'%d/%m/%Y %H:%i:%s') as created_date
,rsa.request_id
,rsa.name
,
case
when rsa.status=0 then 'Negotiating'
when rsa.status=1 then 'Rejected'
when rsa.status=2 then 'Accepted'
when rsa.status=3 then 'In Progress'
when rsa.status=4 then 'Finished'
end progress
,rsa.id
,rsa.description
,rsa.proposed_cost
,rsa.accepted_cost
,rsa.executive_approval
,rsa.member_approval
from
request_service_activity rsa
left join property_units pu on rsa.property_id=pu.id

left join
(
select concat(fname,' ',lname) name,member_id,permanent_address_id adress from individual
UNION
select name,member_id,reg_off_address_id  from company
) m on rsa.owner=m.member_id
left join
address a on m.adress=a.id
left join
telephone t on a.telephone_id=t.id
where rsa.owner=:eid and rsa.id=:id";

                Yii::trace(VarDumper::dumpAsString($sql),'vardump');
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$data["id"]);
                $cmd->bindValue(":eid",Yii::$app->user->id);
                $eal=$cmd->queryOne();
                if ($eal) {



                    /*$sql="
                        select pu.name puname,p.locality,pu.block,pu.flat_no from


                        property p,property_units pu
                        where
                         pu.property_id=p.id
                        and pu.id=:id
                     ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$eal->property_id);
                    $resProperty=$cmd->queryOne();*/

                    return ["mstr_detail"=>$eal
                        ,"conversation"=>[]
                    ];

                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }


    public function actionRsaApprove(){
        $data= \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = RequestServiceActivity::findOne($data["id"]);
                if ($eal) {

                    if(!$eal->executive_approval){
                        throw new \Exception ("Executive not approved");
                    }


                    $eal->member_approval=Yii::$app->user->id;

                    if($eal->member_approval!=$eal->owner){
                        throw new \Exception ("Un authorized access");
                    }

                    if($eal->saveMemberApproval()){
                        return ["sucess"=>true];
                    }else{
                        $errmsg='';
                        foreach($eal->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                        throw new \Exception ( $errmsg);
                    }


                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

}
