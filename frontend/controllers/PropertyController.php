<?php

namespace frontend\controllers;

use app\models\ExecutiveActivityLog;
use app\models\ExecutivePendingActivity;
use app\models\TodoList;
use app\modules\service\models\ExecutiveActivityProperty;
use common\models\User;
use frontend\components\AccessRule;
use Yii;
use common\models\LoginForm;
use yii\filters\ContentNegotiator;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;

class PropertyController extends Controller
{

    //public $uploadsUrl="http://localhost/vhp1/vhp/backend/web/uploads/";
    public $uploadsUrl="http://snowfox.valuehomeproperties.com/app/uploads/";

    public function  beforeAction($action){
        $baseUrl=Yii::$app->urlManager->getHostInfo()."/admin";
        $this->uploadsUrl=$baseUrl."/uploads/";
        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['dashboard','login-details'
                ,'pending-collections','todolist','addtodo','ticktodo'],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            // We will override the default rule config with the new AccessRule class
            'ruleConfig' => [
                'class' => AccessRule::className(),
            ],
            'only' => ['dashboard','loginDetails','pendingCollections','todolist','addtodo','ticktodo'],
            'rules' => [
                [
                    'actions' => ['dashboard','loginDetails','pendingCollections','todolist','addtodo','ticktodo'],
                    'allow' => true,
                    'roles' => ['Executive'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionPropertylistOld()
    {
        $data=\Yii::$app->getRequest()->getBodyParams();

        if(
        !isset($data["name"])
        ||!isset($data["type"])
        ||!isset($data["builder"])
        ||!isset($data["address"])
        ||!isset($data["rent"])
        ||!isset($data["lease"])
        ||!isset($data["sale"])
        ||!isset($data["advertised"])
        ||!isset($data["featured"])
        ){
            return ["success"=>false,"message"=>"Invalid input data"];
        }

        // We define our address
        $address = 'Caen, Basse-Normandie';
// We get the JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
// We convert the JSON to an array
        $geo = json_decode($geo, true);
// If everything is cool
        if ($geo['status'] = 'OK') {
            // We set our values
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }

$rentorsale="";
        if($data["rent"]){
           $rentorsale.=" and puf.is_rent=1";
        }
        if($data["lease"]){
            $rentorsale.=" and puf.is_lease=1";
        }
        if($data["sale"]){
            $rentorsale.=" and puf.for_sale=1";
        }

        $price="";
        $advertisedSql="";
        if($data["advertised"]){
            $advertisedSql.=" and p.advertisement=1";
        }
        $featuredSql="";
        if($data["featured"]){
            $advertisedSql.=" and p.featured=1";
        }

        $sql="select p.name,pt.name as type,p.builder_name as builder,
 case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale,
 if(p.advertisement=1,'Yes','No') as advertised,if(p.featured=1,'Yes','No') featured
 from
property p,property_units pu,prop_unit_finance puf,property_type pt
where
pu.property_id=p.id
and puf.prop_unit_id=pu.id
and p.prop_type=pt.id
and p.name like :name
and pt.name like :type
and p.builder_name like :builder
$advertisedSql
$featuredSql
$rentorsale
$price
";
        $name="%".$data["name"]."%";
        $type="%".$data["type"]."%";
        $builder="%".$data["builder"]."%";
       // $advertised="%".$data["advertised"]."%";
       // $featured="%".$data["featured"]."%";


        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":name",$name);
        $cmd->bindValue(":type",$type);
        $cmd->bindValue(":builder",$builder);
       // $cmd->bindValue(":advertised",$advertised);
       // $cmd->bindValue(":featured",$featured);
        return ($cmd->queryAll());
    }

    public function actionPropertylist()
    {
        //$data=\Yii::$app->getRequest()->getBodyParams();
        $data=$_GET;

        /*if(
            !isset($data["name"])
            ||!isset($data["type"])
            ||!isset($data["builder"])
            ||!isset($data["address"])
            ||!isset($data["rent"])
            ||!isset($data["lease"])
            ||!isset($data["sale"])
            ||!isset($data["advertised"])
            ||!isset($data["featured"])
        ){
            return ["success"=>false,"message"=>"Invalid input data"];
        }

        // We define our address
        $address = 'Caen, Basse-Normandie';
// We get the JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
// We convert the JSON to an array
        $geo = json_decode($geo, true);
// If everything is cool
        if ($geo['status'] = 'OK') {
            // We set our values
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }*/


        $locality='';
        if(!empty($data["locality"])){
            $locality.=" and p.locality like :locality ";
        }

        $rentorsale="";
        if(!empty($data["rent"])){

            $rentorsale.=" and ( puf.is_rent=1)";
        }
        if(!empty($data["lease"])){
            if(!empty($rentorsale)){
                $rentorsale=substr($rentorsale,0,strlen($rentorsale)-1);

                $rentorsale.=" or puf.is_lease=1)";
            }else
            $rentorsale.=" and (puf.is_lease=1)";
        }
        if(!empty($data["sale"])){
            if(!empty($rentorsale)){
                $rentorsale=substr($rentorsale,0,strlen($rentorsale)-1);
                $rentorsale.=" or puf.for_sale=1)";
            }else
            $rentorsale.=" and (puf.for_sale=1)";
        }

        $price="";
        $advertisedSql="";
        if(!empty($data["advertised"])){
            $advertisedSql.=" and p.advertisement=1";
        }
        $featuredSql="";
        if(!empty($data["featured"])){
            $advertisedSql.=" and p.featured=1";
        }


        $advertisedSql="";
        if(!empty($data["advertised"])){
            $advertisedSql.=" and p.advertisement=1";
        }
        $featuredSql="";
        if(!empty($data["featured"])){
            $advertisedSql.=" and p.featured=1";
        }






        $bhk='';
        if(
            !empty($data["bhk1"])
            || !empty($data["bhk2"])
            || !empty($data["bhk3"])
            || !empty($data["bhk4"])
        ){

            $bhkSql='';
            if(!empty($data["bhk1"])){
                if(empty($bhkSql)){
                    $bhkSql.='  pu.no_bedrooms=1 ';
                }else
                $bhkSql.=' or pu.no_bedrooms=1 ';
            }
            if(!empty($data["bhk2"])){
                if(empty($bhkSql)){
                    $bhkSql.='  pu.no_bedrooms=2 ';
                }else
                $bhkSql.=' or pu.no_bedrooms=2 ';
            }
            if(!empty($data["bhk3"])){
                if(empty($bhkSql)){
                    $bhkSql.='  pu.no_bedrooms=3 ';
                }else
                $bhkSql.=' or pu.no_bedrooms=3 ';
            }
            if(!empty($data["bhk4"])){
                if(empty($bhkSql)){
                    $bhkSql.='  pu.no_bedrooms=4 ';
                }else
                $bhkSql.=' or pu.no_bedrooms=4 ';
            }
            if(!empty($data["bhkg4"])){
                if(empty($bhkSql)){
                    $bhkSql.='  pu.no_bedrooms>4 ';
                }else
                $bhkSql.=' or pu.no_bedrooms>4 ';
            }

            $bhk.=' and (  '.$bhkSql;
            $bhk.=" ) ";

        }

        /*$bhk1='';
        if(!empty($data["bhk1"])){
            $bhk1 =' and pu.no_bedrooms=1 ';
        }

        $bhk2='';
        if(!empty($data["bhk2"])){
            $bhk2 =' and pu.no_bedrooms=2 ';
        }

        $bhk3='';
        if(!empty($data["bhk3"])){
            $bhk3 =' and pu.no_bedrooms=3 ';
        }

        $bhk4='';
        if(!empty($data["bhk3"])){
            $bhk4 =' and pu.no_bedrooms=4 ';
        }

        $bhkg4='';
        if(!empty($data["bhkg4"])){
            $bhkg4 =' and pu.no_bedrooms>4 ';
        }*/



        $propertyType='';
        if(
            !empty($data["flat"])
            || !empty($data["villa"])
            || !empty($data["independent_house"])
            || !empty($data["row_house"])
            || !empty($data["guest_house"])

            || !empty($data["office_space"])
            || !empty($data["shop_showroom"])
            || !empty($data["warehouse_godown"])
            || !empty($data["hotel_resturant"])
            || !empty($data["commercial_plot_land"])
            || !empty($data["industrial_building_shed"])

            || !empty($data["agriculture_land"])
            || !empty($data["farm_house"])
            || !empty($data["resort"])
        ){

            $ptypeSql='';
            if(!empty($data["flat"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Apartment" or pt.name="Flat" ';
                }else
                    $ptypeSql.=' or pt.name="Apartment" or pt.name="Flat" ';
            }

            if(!empty($data["villa"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Villa" ';
                }else
                    $ptypeSql.=' or pt.name="Villa"  ';
            }

            if(!empty($data["independent_house"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Independent House" ';
                }else
                    $ptypeSql.=' or pt.name="Independent House"  ';
            }

            if(!empty($data["row_house"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Row House" ';
                }else
                    $ptypeSql.=' or pt.name="Row House"  ';
            }

            if(!empty($data["guest_house"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Guest House" ';
                }else
                    $ptypeSql.=' or pt.name="Guest House"  ';
            }

            if(!empty($data["office_space"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="Office Space" ';
                }else
                    $ptypeSql.=' or pt.name="office space"  ';
            }

            if(!empty($data["shop_showroom"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="shop showroom" ';
                }else
                    $ptypeSql.=' or pt.name="shop showroom"  ';
            }

            if(!empty($data["warehouse_godown"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="warehouse godown" ';
                }else
                    $ptypeSql.=' or pt.name="warehouse godown"  ';
            }


            if(!empty($data["hotel_resturant"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="hotel resturant" ';
                }else
                    $ptypeSql.=' or pt.name="hotel resturant"  ';
            }

             if(!empty($data["commercial_plot_land"])){
                 if(empty($ptypeSql)){
                     $ptypeSql.='  pt.name="commercial plot land" ';
                 }else
                     $ptypeSql.=' or pt.name="commercial plot land"  ';
             }


            if(!empty($data["industrial_building_shed"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="industrial building shed" ';
                }else
                    $ptypeSql.=' or pt.name="industrial building shed"  ';
            }

            if(!empty($data["agriculture_land"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="agriculture land" ';
                }else
                    $ptypeSql.=' or pt.name="agriculture land"  ';
            }

            if(!empty($data["farm_house"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="farm house" ';
                }else
                    $ptypeSql.=' or pt.name="farm house"  ';
            }

            if(!empty($data["resort"])){
                if(empty($ptypeSql)){
                    $ptypeSql.='  pt.name="resort" ';
                }else
                    $ptypeSql.=' or pt.name="resort"  ';
            }

            $propertyType.=' and (  '.$ptypeSql;
            $propertyType.=" ) ";
        }

        /*$flat='';
        if(!empty($data["flat"])){
            $flat =' and pt.name="Apartment" ';
        }

        $villa='';
        if(!empty($data["villa"])){
            $villa =' and pt.name="Villa" ';
        }

        $independent_house='';
        if(!empty($data["independent_house"])){
            $independent_house =' and pt.name="Independent House" ';
        }

        $row_house='';
        if(!empty($data["row_house"])){
            $row_house =' and pt.name="Row House" ';
        }

        $guest_house='';
        if(!empty($data["guest_house"])){
            $guest_house =' and pt.name="Guest House" ';
        }

        $office_space='';
        if(!empty($data["office_space"])){
            $office_space =' and pt.name="Office space" ';
        }

        $shop_showroom='';
        if(!empty($data["shop_showroom"])){
            $shop_showroom =' and pt.name="Shop showroom" ';
        }

        $warehouse_godown='';
        if(!empty($data["warehouse_godown"])){
            $warehouse_godown =' and pt.name="Warehouse godown" ';
        }

        $hotel_resturant='';
        if(!empty($data["hotel_resturant"])){
            $hotel_resturant =' and pt.name="Hotel/Restaurant" ';
        }

        $commercial_plot_land='';
        if(!empty($data["hotel_resturant"])){
            $commercial_plot_land =' and pt.name="Commercial Plot/Land" ';
        }

        $industrial_building_shed='';
        if(!empty($data["hotel_resturant"])){
            $industrial_building_shed =' and pt.name="Industrial Building/Shed" ';
        }



        $agriculture_land='';
        if(!empty($data["agriculture_land"])){
            $agriculture_land =' and pt.name="Agriculture Land" ';
        }

        $farm_house='';
        if(!empty($data["farm_house"])){
            $farm_house =' and pt.name="Farm house" ';
        }

        $resort='';
        if(!empty($data["farm_house"])){
            $resort =' and pt.name="Resort" ';
        }*/

        //$budget_min=' and pu.starting_price >= 0 ';

        $bmin=0;
        $budget_min = '';
        if(!empty($data["budget_min"])&&(int)$data["budget_min"]) {
            $bmin = $data["budget_min"];


            $budget_min = ' and (
            (puf.is_rent=1 and puf.min_rent>= ' . ((int)$data["budget_min"]) . ' )or
            (puf.is_lease=1 and puf.min_lease>= ' . ((int)$data["budget_min"]) . ' ) or
            (puf.for_sale=1 and puf.starting_price>= ' . ((int)$data["budget_min"]) . ' )

             )';
        }

        $bmax=99999999999;
        $budget_max='';
        if(!empty($data["budget_max"])&&(int)$data["budget_max"]) {
            $bmax = $data["budget_max"];


            $budget_max = ' and (
            (puf.is_rent=1 and puf.min_rent<= ' . ((int)$data["budget_max"]) . ' )or
            (puf.is_lease=1 and puf.min_lease<= ' . ((int)$data["budget_max"]) . ' ) or
            (puf.for_sale=1 and puf.starting_price<= ' . ((int)$data["budget_max"]) . ' )

             )';

        }




        $door_facing='';
        if(
            !empty($data["door_facing_east"])
            || !empty($data["door_facing_west"])
            || !empty($data["door_facing_north"])
            || !empty($data["door_facing_south"])
        ){

            $dfSql='';
            if(!empty($data["door_facing_east"])){
                if(empty($dfSql)){
                    $dfSql.='  pu.door_facing like "East"  ';
                }else
                    $dfSql.=' or pu.door_facing like "East"  ';
            }
            if(!empty($data["door_facing_west"])){
                if(empty($dfSql)){
                    $dfSql.='  pu.door_facing like "West"  ';
                }else
                    $dfSql.=' or pu.door_facing like "West"  ';
            }
            if(!empty($data["door_facing_north"])){
                if(empty($dfSql)){
                    $dfSql.='  pu.door_facing like "North"  ';
                }else
                    $dfSql.=' or pu.door_facing like "North"  ';
            }
            if(!empty($data["door_facing_south"])){
                if(empty($dfSql)){
                    $dfSql.='  pu.door_facing like "South" ';
                }else
                    $dfSql.=' or pu.door_facing like "South" ';
            }

            $door_facing.=' and (  '.$dfSql;
            $door_facing.=" ) ";

        }


        /*$door_facing_east='';
        if(!empty($data["door_facing_east"])){
            $door_facing_east=' and pu.door_facing like "East" ';
        }

        $door_facing_west='';
        if(!empty($data["door_facing_west"])){
            $door_facing_west=' and pu.door_facing like "West" ';
        }

        $door_facing_north='';
        if(!empty($data["door_facing_north"])){
            $door_facing_north=' and pu.door_facing like "North" ';
        }

        $door_facing_south='';
        if(!empty($data["door_facing_south"])){
            $door_facing_south=' and pu.door_facing like "South" ';
        }*/




        $parking='';
        if(
            !empty($data["parking4"])
            || !empty($data["parking41"])
            || !empty($data["parking42"])
            || !empty($data["parking43"])
            || !empty($data["parking44"])
            || !empty($data["parking2"])
            || !empty($data["parking21"])
            || !empty($data["parking22"])
            || !empty($data["parking23"])
            || !empty($data["parking24"])
        ){
            $parkSql='';
            if(!empty($data["parking4"])){
                if(empty($parkSql)){
                    $parkSql.='   pup.vehicle_type like "Four Wheeler"   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Four Wheeler"   ';
            }

            if(!empty($data["parking41"])){
                if(empty($parkSql)){
                    $parkSql.='   pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=1   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=1   ';
            }

            if(!empty($data["parking42"])){
                if(empty($parkSql)){
                    $parkSql.='   pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=2   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=2   ';
            }

            if(!empty($data["parking43"])){
                if(empty($parkSql)){
                    $parkSql.='   pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=3   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles=3   ';
            }

            if(!empty($data["parking44"])){
                if(empty($parkSql)){
                    $parkSql.='   pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles>=4   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Four Wheeler"  and pup.no_of_vehicles>=4   ';
            }

            if(!empty($data["parking2"])){
                if(empty($parkSql)){
                    $parkSql.='  pup.vehicle_type like "Two Wheeler"   ';
                }else
                    $parkSql.=' or  pup.vehicle_type like "Two Wheeler"   ';
            }



            if(!empty($data["parking21"])){
                if(empty($parkSql)){
                    $parkSql.='  pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=1   ';
                }else
                    $parkSql.=' or pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=1   ';
            }


            if(!empty($data["parking22"])){
                if(empty($parkSql)){
                    $parkSql.='  pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=2   ';
                }else
                    $parkSql.=' or pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=2   ';
            }

            if(!empty($data["parking23"])){
                if(empty($parkSql)){
                    $parkSql.='  pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=3   ';
                }else
                    $parkSql.=' or pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=3   ';
            }

            if(!empty($data["parking24"])){
                if(empty($parkSql)){
                    $parkSql.='  pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles>=4   ';
                }else
                    $parkSql.=' or pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles>=4   ';
            }

            $parking.=' and (  '.$parkSql;
            $parking.=" ) ";
        }

        /*$parking4='';
        if(!empty($data["parking4"])){
            $parking4=' and pup.vehicle_type like "Four Wheeler" ';
        }

        $parking41='';
        if(!empty($data["parking41"])){
            $parking4=' and pup.vehicle_type like "Four Wheeler" and pup.no_of_vehicles=1 ';
        }

        $parking42='';
        if(!empty($data["parking42"])){
            $parking4=' and pup.vehicle_type like "Four Wheeler" and pup.no_of_vehicles=2 ';
        }

        $parking43='';
        if(!empty($data["parking43"])){
            $parking4=' and pup.vehicle_type like "Four Wheeler" and pup.no_of_vehicles=3 ';
        }

        $parking44='';
        if(!empty($data["parking44"])){
            $parking4=' and pup.vehicle_type like "Four Wheeler" and pup.no_of_vehicles>=4 ';
        }

        $parking2='';
        if(!empty($data["parking2"])){
            $parking4=' and pup.vehicle_type like "Two Wheeler" ';
        }

        $parking21='';
        if(!empty($data["parking21"])){
            $parking4=' and pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=1 ';
        }

        $parking22='';
        if(!empty($data["parking22"])){
            $parking4=' and pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=2 ';
        }

        $parking23='';
        if(!empty($data["parking23"])){
            $parking4=' and pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles=3 ';
        }

        $parking24='';
        if(!empty($data["parking24"])){
            $parking4=' and pup.vehicle_type like "Two Wheeler" and pup.no_of_vehicles>=4 ';
        }*/






        $bamin=0;
        if(!empty($data["builtuparea_min"])&&(int)$data["builtuparea_min"]){
            $bamin=$data["builtuparea_min"];
        }
        $builtuparea_min='';
        if($bamin==0){
            $builtuparea_min.=" and (pu.builtup_area>=0 or pu.builtup_area is null )";
        }else{
            $builtuparea_min.=" and pu.builtup_area>=".$bamin." or pu.builtup_area is null )";

        }


        $bamax=99999999999;
        if(!empty($data["builtuparea_max"])&&(int)$data["builtuparea_max"]){
            $bamax=$data["builtuparea_max"];
        }
        $builtuparea_max='';
        if($bamax==99999999999){
            $builtuparea_max.=" and (pu.builtup_area<=99999999999 or pu.builtup_area is null )";
        }else{
            $builtuparea_max.=" and (pu.builtup_area<=".$bamax." or pu.builtup_area is null )" ;

        }


        $furnished='';
        if(
            !empty($data["fully_furnished"])
            || !empty($data["semi_furnished"])
            || !empty($data["un_furnished"])
        ){
            $furSql='';
            if(!empty($data["fully_furnished"])){
                if(empty($furSql)){
                    $furSql.='  pu.furnished_status like "Furnished"  ';
                }else
                    $furSql.=' or pu.furnished_status like "Furnished"  ';
            }

            if(!empty($data["semi_furnished"])){
                if(empty($furSql)){
                    $furSql.='  pu.furnished_status like "Semi-Furnished"  ';
                }else
                    $furSql.=' or pu.furnished_status like "Semi-Furnished"  ';
            }

            if(!empty($data["un_furnished"])){
                if(empty($furSql)){
                    $furSql.='  pu.furnished_status like "Un-Furnished"  ';
                }else
                    $furSql.=' or pu.furnished_status like "Un-Furnished"  ';
            }

            $furnished.=' and (  '.$furSql;
            $furnished.=" ) ";

        }

        /*$furnished='';
        if(!empty($data["fully_furnished"])){
            $furnished=' and pu.furnished_status like "Furnished"';
        }

        $semi_furnished='';
        if(!empty($data["semi_furnished"])){
            $semi_furnished=' and pu.furnished_status like "Semi-Furnished"';
        }

        $un_furnished='';
        if(!empty($data["un_furnished"])){
            $un_furnished=' and pu.furnished_status like "Un-Furnished"';
        }*/

        $availabilityDays=999;

        $availabilitySql='';

        if(!empty($data["ready_to_move"])){
            $availabilitySql="
 and ifnull((
select DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),0 )
    =0

            ";
        }

        if(!empty($data["within_a_week"])){
            $availabilitySql="
            and ifnull((
select DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),0 )
    <= 7
             ";
        }

        if(!empty($data["within_2_weeks"])){
            $availabilitySql="
            and ifnull((
select DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),0 )
    <= 14
             ";        }

        if(!empty($data["within_3_weeks"])){
            $availabilitySql="
            and ifnull((
select DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),0 )
    <= 21
             ";        }

        if(!empty($data["within_a_month"])){
            $availabilitySql="
            and ifnull((
select DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),0 )
    <= 31
             ";        }



        $pets='';
        if(!empty($data["pets_yes"])){
            $pets=" and pu.are_pets_allowed=1 ";
        }

        if(!empty($data["pets_no"])){
            $pets=" and pu.are_pets_allowed=0 ";
        }

        $veg='';
        if(!empty($data["non_vegetarian"])){
            $pets=" and pu.prefered_food_habits like 'Non-Vegetarian' ";
        }

        if(!empty($data["vegetarian"])){
            $pets=" and pu.prefered_food_habits like 'Vegetarian' ";
        }


        $amenitiesArray=[];
        $amenitiesSql='';
        /*if(!empty($data['amenities'])){
            $amenitiesArray=$data['amenities'];
            $amenitiesSql=' and pa.amenity in ( ';
            for($i=0;$i<count($amenitiesArray) ;$i++){
                $amenitiesSql.=':aa'.$i.',';
            }
            $amenitiesSql=rtrim($amenitiesSql,',');
            $amenitiesSql.=" ) ";
        }*/
        if(!empty($data['amenities'])){
            $amenitiesArray=$data['amenities'];
            $amenitiesSql='   ';
            for($i=0;$i<count($amenitiesArray) ;$i++){
                $amenitiesSql.=' and pa.amenity like :aa'.$i;
            }

        }




       $pageSize=10;
        if(!empty($data["page_size"])&&(int)$data["page_size"]){
            $pageSize=(int)$data["page_size"];

        }

        $offset=0;
        if(!empty($data["offset"])&&(int)$data["offset"]){
            $offset=(int)$data["offset"];
        }

        $limit =" limit $offset,$pageSize";



        $sql="select
pu.id
,p.id as projectId
,pu.no_bedrooms,pu.furnished_status,p.locality
,puf.starting_price,puf.min_rent,puf.min_lease,puf.maintenance_fee
,puf.min_rent_deposit
,(select
   DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate())

    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1) as availabilityindays
,ifnull((select
  case
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) is null  then 'Any Time'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) = 0   then 'Ready to move'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) < 0  then 'Any Time'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 0 and DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) <= 7   then 'Within a week'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 7 and DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) <= 14   then 'Within 2 weeks'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 14 and DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) <= 21   then 'Within 3 weeks'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 21 and DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) <= 31   then 'Within a month'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 31 and DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) <= 90   then 'Within 3 months'
    when DATEDIFF(STR_TO_DATE(description,'%d-%m-%Y'),curdate()) > 90    then 'More than 3 months'
    end
    from prop_unit_interior where prop_unit_id=pu.id and category like 'Availability'  limit 1),'Any Time1' )as availability
,pu.door_facing
,pu.builtup_area
,(select group_concat(concat(no_of_vehicles,' ',if(vehicle_type='Four Wheeler','Car','Bike'))) from prop_unit_parking where prop_unit_id=pu.id group by pu.id ) as parking
,(select group_concat(amenity separator '|||') from property_amenities where property_id=p.id ) as amenitiesText
,(select url_replace( concat('{$this->uploadsUrl}',pp.pic))  as img_path from  properties_pics pp
    where pp.type='Banner Image' and pp.prop_id=p.id limit 1) as banner_img

,p.name,pt.name as type,p.builder_name as builder,
 case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.is_rent=1 and puf.is_lease=0 and puf.for_sale=1 then 'For sale and rent'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale,
 if(p.advertisement=1,'Yes','No') as advertised,if(p.featured=1,'Yes','No') featured
 ,ifnull((select count(*) c from properties_pics where prop_id=p.id ),0) as galleryCount
 from
property p left join property_amenities pa on pa.property_id=p.id
left join address a on p.address_id=a.id
,property_type pt
,property_units pu
left join
prop_unit_parking pup on pup.prop_unit_id=pu.id
left join prop_unit_finance puf on puf.prop_unit_id=pu.id
where
pu.property_id=p.id
and p.prop_type=pt.id

$rentorsale
$advertisedSql
$featuredSql

$propertyType
$bhk


$budget_min
$budget_max

$parking

$builtuparea_min
$builtuparea_max

$door_facing

$furnished



$availabilitySql
$pets
$veg
$amenitiesSql
$locality

group by pa.property_id
$limit
";

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        /*$name="%".$data["name"]."%";
        $type="%".$data["type"]."%";
        $builder="%".$data["builder"]."%";*/
        // $advertised="%".$data["advertised"]."%";
        // $featured="%".$data["featured"]."%";


        $cmd=Yii::$app->db->createCommand($sql);
       /* $cmd->bindValue(":name",$name);
        $cmd->bindValue(":type",$type);
        $cmd->bindValue(":builder",$builder);*/
        if(!empty($locality)){
            $cmd->bindValue(':locality',"%".$data["locality"]."%");
        }
        for($i=0;$i<count($amenitiesArray) ;$i++){
            Yii::trace(VarDumper::dumpAsString(':aa'.$i),'vardump');
            Yii::trace(VarDumper::dumpAsString($amenitiesArray[$i]),'vardump');
            $cmd->bindValue(':aa'.$i,$amenitiesArray[$i]);
        }
        // $cmd->bindValue(":advertised",$advertised);
        // $cmd->bindValue(":featured",$featured);
        $res1=$cmd->queryAll();
        $res=$res1;
        foreach($res1 as $i => $r){
            $res[$i]["amenities"]=[];
            if(!empty($r["amenitiesText"]))
            $res[$i]["amenities"]=explode('|||',$r["amenitiesText"]);
        }

        return ($res);
    }

    public function actionListFeaturedProperties()
    {
        //$data = \Yii::$app->getRequest()->getBodyParams();
        $sql="
        select pu.id,
 case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale
 ,url_replace( concat('{$this->uploadsUrl}',pp.pic)) as img_path
 ,p.name as proj_name
 ,concat(pu.no_bedrooms,' BHK') as bedroom_type
 ,pt.name as prop_type
 ,p.locality
 ,a.city
 ,
 case
 when puf.starting_price>=10000000 then concat(ROUND(puf.starting_price/10000000,2) ,' Cr')
 when puf.starting_price>=100000 then concat(ROUND(puf.starting_price/100000,2) ,' Lacs')
 when puf.starting_price<100000 then puf.starting_price
 end as starting_price


 from
property p
left join properties_pics pp on p.id=pp.prop_id and type='Banner Image'
,address a,
(
select
case
when for_sale=1 then starting_price
when is_lease=1 then min_lease

when is_rent=1 then min_rent


end as starting_price
,prop_unit_id,is_rent,is_lease,for_sale
from prop_unit_finance
) puf
,property_type pt
,property_units pu
where
pu.property_id=p.id
and a.id=address_id
and puf.prop_unit_id=pu.id
and p.prop_type=pt.id
and p.featured=1
and pu.is_active=1

        ";

        $sql1="
        select pu.id,
 case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale
 ,url_replace( concat('{$this->uploadsUrl}',pp.pic)) as img_path
 ,p.name as proj_name
 ,concat(pu.no_bedrooms,' BHK') as bedroom_type
 ,pt.name as prop_type
 ,p.locality
 ,a.city
 ,
 case
 when puf.starting_price>=10000000 then concat(ROUND(puf.starting_price/10000000,2) ,' Cr')
 when puf.starting_price>=100000 then concat(ROUND(puf.starting_price/100000,2) ,' Lacs')
 when puf.starting_price<100000 then puf.starting_price
 end as starting_price


 from
property p
left join properties_pics pp on p.id=pp.prop_id and type='Gallery'
,address a,
(
select
case
when for_sale=1 then starting_price
when is_lease=1 then min_lease

when is_rent=1 then min_rent


end as starting_price
,prop_unit_id,is_rent,is_lease,for_sale
from prop_unit_finance
) puf
,property_type pt
,property_units pu
where
pu.property_id=p.id
and a.id=address_id
and puf.prop_unit_id=pu.id
and p.prop_type=pt.id
and p.featured=1
and pu.is_active=1

        ";


        $sql="
        select pu.id,
 case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale
 ,url_replace( concat('{$this->uploadsUrl}',pp.pic)) as img_path
 ,p.name as proj_name
 ,concat((select group_concat(no_bedrooms order by no_bedrooms) from property_units where property_id=p.id group by property_id  ),' BHK') as bedroom_type
 ,pt.name as prop_type
 ,p.locality
 ,a.city
 ,
 case
 when puf.starting_price>=10000000 then concat(ROUND(puf.starting_price/10000000,2) ,' Cr')
 when puf.starting_price>=100000 then concat(ROUND(puf.starting_price/100000,2) ,' Lacs')
 when puf.starting_price<100000 then puf.starting_price
 end as starting_price


 from
property p
left join (select * from properties_pics pp where type='Gallery' group by pp.prop_id) pp  on p.id=pp.prop_id
,address a

, ( select * from  property_units order by no_bedrooms asc)  pu
left join
(
select
case
when for_sale=1 then starting_price
when is_lease=1 then min_lease

when is_rent=1 then min_rent


end as starting_price
,prop_unit_id,is_rent,is_lease,for_sale
from prop_unit_finance
) puf on puf.prop_unit_id=pu.id
,property_type pt

where
pu.property_id=p.id
and a.id=address_id
and p.prop_type=pt.id
and p.featured=1
and pu.is_active=1
group by p.id



        ";


        $cmd=Yii::$app->db->createCommand($sql);
        return ($cmd->queryAll());

    }

    public function actionPropertyDetails($id)
    {
        //$data = \Yii::$app->getRequest()->getBodyParams();
        $sql="
       select

        pu.id
        ,if(p.member_id is null,'Developer','Member') as memberordeveloper
        ,p.lat as lat
        ,p.long as longitude
    ,p.name as proj_name
    ,p.builder_name as builder_name
    ,a.adrs_line1 as address
    ,p.locality as locality
    ,a.city as city
     ,concat(pu.no_bedrooms,' BHK') as bedroom_type
 ,pt.name as prop_type
,pu.furnished_status
,pu.builtup_area
,p.proj_status
,case
 when p.proj_start_date='1970-01-01' then null
 when p.proj_start_date!='1970-01-01' then DATE_FORMAT(proj_start_date,'%d %b %Y')
 end as launched_date
,case
 when p.poss_date='1970-01-01' then null
 when p.poss_date!='1970-01-01' then DATE_FORMAT(poss_date,'%b %Y')
 end as poss_date
,case
 when p.poss_date='1970-01-01' then null
 when p.poss_date!='1970-01-01' and date(poss_date)>=date(now()) then DATE_FORMAT(now(),'%d %b %Y')
 when p.poss_date!='1970-01-01' and date(poss_date)<date(now()) then null
 end as ongoing_date
,p.overview
 ,case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale
 ,url_replace( concat('{$this->uploadsUrl}',pp.pic)) as banner_image
 ,p.name as proj_name
 ,concat(pu.no_bedrooms,' BHK') as bedroom_type
 ,pt.name as prop_type
 ,p.locality
 ,a.city
 ,url_replace( concat('{$this->uploadsUrl}',pu.image)) as floor_plan
 ,url_replace( concat('{$this->uploadsUrl}',ppm.pic)) as master_plan
,p.locality_desc
 ,case
 when puf.starting_price>=10000000 then concat(ROUND(puf.starting_price/10000000,2) ,' Cr')
 when puf.starting_price>=100000 then concat(ROUND(puf.starting_price/100000,2) ,' Lacs')
 when puf.starting_price<100000 then puf.starting_price
 end as starting_price
,pu.door_facing
,pu.carepet_area as carpet_area
,pu.super_builtup_area
,p.approvals
,p.loan
 from
property p
left join properties_pics pp on p.id=pp.prop_id and pp.type='Banner Image'
left join properties_pics ppm on p.id=ppm.prop_id and ppm.type='Master Plan'

,address a,property_type pt
,property_units pu
,(
select
case
when for_sale=1 then starting_price
when is_lease=1 then min_lease

when is_rent=1 then min_rent


end as starting_price
,prop_unit_id,is_rent,is_lease,for_sale
from prop_unit_finance
) puf

where
pu.property_id=p.id
and a.id=address_id
and puf.prop_unit_id=pu.id
and p.prop_type=pt.id

and pu.id=:id

        ";

        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res=$cmd->queryOne();

        $sql="
            select area_partition_type type ,count(area_partition_type) as no,url_replace( concat('{$this->uploadsUrl}',icon)) as icon from prop_area_partition,property_partition_type where prop_area_partition.area_partition_type=property_partition_type.partition_type and prop_area_partition.punit_id=:id group by area_partition_type;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["area_partitions"]=$cmd->queryAll();

        $sql="
            select vehicle_type  ,no_of_vehicles as no
            from prop_unit_parking where prop_unit_id=:id ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["parking"]=$cmd->queryAll();


        $sql="
            select heading,description,(select url_replace( concat('{$this->uploadsUrl}',icon)) from amenity_type WHERE type='Facility' and amenity like heading) icon
            from prop_unit_interior where prop_unit_id =:id  and category='Additional Facilities' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["additional_facilities"]=$cmd->queryAll();


        $sql="
            select pa.amenity,url_replace( concat('{$this->uploadsUrl}',icon)) as icon
            from property_amenities pa,amenity_type at where at.amenity=pa.amenity and property_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["amenity"]=$cmd->queryAll();



        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Flooring' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Flooring"]=$cmd->queryAll();

        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Fittings' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Fittings"]=$cmd->queryAll();

        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Walls' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Walls"]=$cmd->queryAll();



        $sql="
            select
             url_replace( concat('{$this->uploadsUrl}',pic)) as pic

            from properties_pics pp  where pp.type='Gallery' and prop_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["gallery"]=$cmd->queryColumn();


        $sql="
            select
             url_replace( concat('{$this->uploadsUrl}',pic)) as pic

            from properties_pics pp  where pp.type='Construction Status Photos' and prop_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["construction_photos"]=$cmd->queryColumn();


        /*$sql="
            select
            bk.loan
             ,url_replace( concat('{$this->uploadsUrl}',bk.icon)) as icon

            from property p,bank_loans  bk
            where
            bk.loan in (p.loan) and
            p.id =( select property_id from property_units where id=:id ) ;
        ";*/

        if($res){
            Yii::trace(VarDumper::dumpAsString($res),'vardump');
            $loan=explode(",",$res["loan"]);
            $loans='';
            Yii::trace(VarDumper::dumpAsString($loan),'vardump');

            foreach($loan as $l){

                $loans.="'{$l}',";
            }
            Yii::trace(VarDumper::dumpAsString($loans),'vardump');
            Yii::trace(VarDumper::dumpAsString(strlen($loans)-1),'vardump');
            $loans1=substr($loans,0,count($loans)-2);
            Yii::trace(VarDumper::dumpAsString($loans1),'vardump');

            $sql="select
            bk.loan
             ,url_replace( concat('{$this->uploadsUrl}',bk.icon)) as icon

            from bank_loans  bk
            where
            bk.loan in ($loans1)";
            $cmd=Yii::$app->db->createCommand($sql);


            //$cmd->bindValue(":loan",$loans1);
            $res["loan"]=$cmd->queryAll();
        }else{
            $res["loan"]=[];
        }


        return ($res);

    }

    public function actionPropertyGallery(){
        $data=$_GET;

        if(!empty($data["id"])) {
            $sql = "
        select
         url_replace( concat('{$this->uploadsUrl}',pic)) as img_path
         from properties_pics
         where prop_id=:id and type='Gallery'
        ";
            $cmd = Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$data["id"]);
            $res=$cmd->queryColumn();

            return $res;

        }

    }

    public function actionBuilderDetails(){
        $data=$_GET;
        if(!empty($data["name"])){
            $offset=(!empty($data["offset"])&&filter_var($data["offset"],FILTER_VALIDATE_INT)!==false)?filter_var($data["offset"],FILTER_VALIDATE_INT):0;
            $pageSize=(!empty($data["pageSize"])&&filter_var($data["pageSize"],FILTER_VALIDATE_INT)!==false)?filter_var($data["pageSize"],FILTER_VALIDATE_INT):10;

            $sql="select *
,url_replace( concat('{$this->uploadsUrl}',logo)) urlicon
,url_replace( concat('{$this->uploadsUrl}',banner_img)) urlbanner
 from builder where id=:id ";
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$data["name"]);
            $bld=$cmd->queryOne();
            $bld["properties"]=[];
            if($bld){
                $sql="select name,locality,tag_line,proj_status
, start_price
 ,a.city

from property p left join address a on p.address_id=a.id where builder_name=:name
 limit :offset,:pageSize
 ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":name",$data["name"]);
                $cmd->bindValue(":pageSize",$pageSize);
                $cmd->bindValue(":offset",$offset);
                $bld["properties"]=$cmd->queryAll();
            }
            return $bld;
        }
    }



}