<?php

namespace frontend\controllers;

use app\models\ExecutiveActivityLog;
use app\models\ExecutiveActivityTxnDetails;
use app\models\ExecutivePendingActivity;
use app\models\ExecutiveTravelDetails;
use app\models\RequestServiceActivity;
use app\models\TodoList;
use app\modules\service\models\ExecutiveActivityProperty;
use common\models\User;
use frontend\components\AccessRule;
use Yii;
use common\models\LoginForm;
use yii\base\Exception;
use yii\filters\ContentNegotiator;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;
use yii\web\UploadedFile;
use mPDF;

class ExecutiveController extends Controller
{
    //public $uploadsUrl="http://snowfox.valuehomeproperties.com/app/uploads/";
    public $uploadsUrl="http://localhost/vhp1/vhp/backend/web/uploads/";
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['dashboard','login-details'
                ,'pending-collections','todolist'
                ,'addtodo'
                ,'ticktodo'
                ,'rsa-approve'
                ,'rsa-complete'

                ,'request-detail'
                ,'request-list'
                ,'daily-accounting-list'
                ,'property-visited-details'
                ,'add-association-meeting'
                ,'association-meeting-list'
                ,'add-property-visit'
                ,'property-visit-list'
                ,'save-travel-details'
                ,'travel-details-list'
                ,'pay-bill-slip-upload'
                ,'paid-collections'
                ,'collect-cheque'
                ,'eal-full-details'
                ,'deposit-cash-payment'
                ,'collect-cash'
                ,'all-collections'
                ,'set-request-services-as-viewed'
                //added by pooja
                ,'members-list'
                ,'member-details'
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            // We will override the default rule config with the new AccessRule class
            'ruleConfig' => [
                'class' => AccessRule::className(),
            ],
            'only' => ['dashboard','loginDetails','pendingCollections','todolist','addtodo','ticktodo'],
            'rules' => [
                [
                    'actions' => ['dashboard','loginDetails','pendingCollections','todolist','addtodo','ticktodo'],
                    'allow' => true,
                    'roles' => ['Executive'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            $user=User::findOne(Yii::$app->user->id);
            Yii::trace(VarDumper::dumpAsString($user),'vardump');
            if($user->user_type=="Executive"){
                $home="home.html";
            }else{
                $home="member.html";
                //$home="home.html";
            }
            return ['access_token' => Yii::$app->user->identity->getAuthKey()
                ,"home"=>$home];
        } else {
            $model->validate();
            return $model;
        }
    }

    public function actionDashboard()
    {

        ExecutivePendingActivity::insertPendingActivity();

        $sql="select eal.*,p.lat,p.locality as `long`,pu.name ,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,DATEDIFF(eal.date,curdate()) as datediff
from executive_activity_log eal ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id and date(date) >=CURDATE()
and ((eal.collection=0 and is_payable!=-1) or if(eal.is_payable=1 and eal.payment=0,true ,false))
and eal.status=0
and eal.exec_id=". Yii::$app->user->id;
        $sql="select eal.*,p.lat,p.long as `long`,p.locality,pu.name ,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,DATEDIFF(eal.date,curdate()) as datediff
from executive_activity_log eal ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id and date(date) >=CURDATE()
and (eal.collection=1 )
and eal.status=0
and eal.exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $collections=$cmd->queryAll();

        $sql="select eal.*,p.lat,p.locality as `long`,pu.name ,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,DATEDIFF(eal.date,curdate()) as datediff
from executive_activity_log eal ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id and date(date) <CURDATE()
and ((eal.collection=0 and is_payable!=-1) or if(eal.is_payable=1 and eal.payment=0,true ,false))
and eal.status=0
and eal.exec_id=". Yii::$app->user->id;
        $sql="select eal.*,p.lat,p.long as `long`,p.locality,pu.name ,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,DATEDIFF(eal.date,curdate()) as datediff
from executive_activity_log eal ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id and date(date) <CURDATE()
and (eal.collection=0 or eal.payment=0)
and eal.status=0
and eal.exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $overdueTasks=$cmd->queryAll();

       /* $sql="select * from executive_activity_log eal,service s
where eal.property_id=s.property_id and  s.is_active=1

and date(date) > CURDATE() ";
        $cmd=Yii::$app->db->createCommand($sql);
        $memberContractExpiry=$cmd->queryAll();*/

        $sql="select count(*) as noc from executive_activity_log eal where ((eal.collection=1)) and status=0 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $noc=$cmd->queryOne()["noc"];

        $sql="select count(*) as noc
        from executive_activity_log
        where date between date_sub(date, interval 1 month) and  date and collection>=1
 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $toc=$cmd->queryOne()["noc"];
        if(!$toc)$nocperc=100;else
        $nocperc=(round($toc-$noc,4)/$toc)*100;

        $pendingCollection=$noc;


        //payment begin

        $sql="select count(*) as noc from executive_activity_log where payment=1 and status=0";
        $cmd=Yii::$app->db->createCommand($sql);
        $noc=$cmd->queryOne()["noc"];

        $sql="select count(*) as noc
        from executive_activity_log
        where date between date_sub(date, interval 1 month) and  date and payment>=1
 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $toc=$cmd->queryOne()["noc"];
        if(!$toc)$nopperc=100;else
        $nopperc=(round($toc-$noc,4)/$toc)*100;

        $pendingPayment=$noc;

        //payment end


        //begin: visit

        $sql="select count(*) as noc from executive_activity_log where  type='Periodic Visits' and status=0";
        $cmd=Yii::$app->db->createCommand($sql);
        $noc=$cmd->queryOne()["noc"];

        $sql="select count(*) as noc
        from executive_activity_log
        where date between date_sub(date, interval 1 month) and  date and type='Periodic Visits'
 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $toc=$cmd->queryOne()["noc"];
        if(!$toc)$pvperc=100;else
        $pvperc=(round($toc-$noc,4)/$toc)*100;

        $pendingVisit=$noc;

        //end visit

        //begin: assoc meetings

        $sql="select count(*) as noc from executive_activity_log where  type='Association Meetings' and status=0";
        $cmd=Yii::$app->db->createCommand($sql);
        $noc=$cmd->queryOne()["noc"];

        $sql="select count(*) as noc
        from executive_activity_log
        where date between date_sub(date, interval 1 month) and  date and type='Association Meetings'
 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $toc=$cmd->queryOne()["noc"];
        if(!$toc)$amperc=100;else
        $amperc=(round($toc-$noc,4)/$toc)*100;

        $pendingMeeting=$noc;

        //end assoc meetings



        $response=[
            "collections"=>$collections,
            "overdueTasks"=>$overdueTasks,
            "nocperc"=>$nocperc,
            "nopperc"=>$nopperc,
            "pvperc"=>$pvperc,
            "amperc"=>$amperc,

            "pendingCollection"=>$pendingCollection,
            "pendingPayment"=>$pendingPayment,
            "pendingVisit"=>$pendingVisit,
            "pendingMeeting"=>$pendingMeeting,
        ];

        return $response;
    }

    public function actionLoginDetails(){
        $sql="select ea.file_path from executive e, executive_attachments ea where ea.exec_id=e.id
          and ea.attachment_type='Photo' and e.user_id= ". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $file=$cmd->queryOne();
        $file_path='';
        if($file){
            $file_path=$file["file_path"];
        }
        /*$response = [
            'username' => Yii::$app->user->identity->username,
            'photo' => $file_path,
            //'access_token' => Yii::$app->user->identity->getAuthKey(),
        ];*/
        $response = array_merge([
            'username' => Yii::$app->user->identity->username,
            'photo' => $file_path,

            //'access_token' => Yii::$app->user->identity->getAuthKey(),
        ],$this->getExecutiveRequestNotification());
        return $response;
    }

    public function actionPendingCollections(){
        $sql="SELECT eal . * , p.lat, p.long
, pu.name AS propertyId, p.name AS propertyName
, DATE_FORMAT( eal.date,  '%d/%m/%Y' ) AS formatedDate
, IF( tel.primary_mobile =  '', tel.primary_landline, tel.primary_mobile ) AS contact
,a.adrs_line1 as adrs
,concat(t.fname,' ',t.lname) as tname
,if(eal.collection=0,'Collect Now','') as status

FROM executive_activity_log eal, property_units pu, property p, tenant t
LEFT JOIN address a ON t.present_address_id = a.id
LEFT JOIN telephone tel ON a.telephone_id = tel.id
WHERE pu.property_id = p.id
AND t.member_id =eal.tenant_id
 and (eal.collection=0 or if(eal.is_payable=1 and eal.payment=0,true ,false))

 and eal.exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        return $collections=$cmd->queryAll();
    }


    public function actionTodolist(){
        $sql="select * from todo_list where status=0 and exec_id= ".Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $res=$cmd->queryAll();
        return ($res);
    }

    public function actionAddtodo(){
        $data=Yii::$app->getRequest()->getBodyParams();

        if(!empty($data["todo"])){
            $newtodo=new TodoList();
            $newtodo->todo=$data["todo"];
            $newtodo->exec_id=Yii::$app->user->id;
            $newtodo->date=date('Y-m-d H:i:s');
            if($newtodo->save()){
                return $newtodo;
            }
        }
        throw new \yii\web\HttpException(400, 'Bad Input', 400);
    }

    public function actionTicktodo(){
        $data=Yii::$app->getRequest()->getBodyParams();

        if(!empty($data["id"])){
            $newtodo=TodoList::findOne($data["id"]);
            if($newtodo->status==0)$newtodo->status=1;
            else $newtodo->status=0;

            if($newtodo->save()){
                return $newtodo;
            }
        }
        throw new \yii\web\HttpException(400, 'Bad Input', 400);
    }


    function getExecutiveRequestNotification(){

        $sql="select rsa.name,rsa.description,u.username,u.user_type,rsa.id from request_service_activity rsa
          left join service s on s.id=rsa.member_id
          left join tenant_property_allocation tpa on rsa.tenant_id=tpa.id
          left join user u on s.member_id=u.id or tpa.tenant_id=u.id
 where status>=0 and status<3 and rsa.exec_id=". Yii::$app->user->id." order by rsa.id desc limit 5 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $rsas=$cmd->queryAll();

        $sql="select count(*) as c from request_service_activity where status>=0 and status<3 and  exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $crsa=$cmd->queryOne()["c"];

        $sql="select count(*) as c from request_service_activity where status>=0 and status<3 and executive_view=0 and  exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $new=$cmd->queryOne()["c"];

        return ['rsas'=>$rsas,'crsa'=>$crsa,'newrsa'=>$new];
    }


    public function actionSetRequestServicesAsViewed(){
        $sql="update request_service_activity set executive_view=1 where   exec_id=". Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->execute();
        return ["success"=>true];
    }


    public function actionAllCollections(){
      $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $from_date=$dateA->format('Y-m-d');

        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(date('Y-m-d'),date_interval_create_from_date_string("7 days"));
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');

        $statusSql="";
        if(!empty($data["statusSelect"])){
            if($data["statusSelect"]=="Collection"){
                $statusSql.=" and (eal.collection=1 )";
            }
            if($data["statusSelect"]=="Payment"){
                $statusSql.=" and (eal.collection=2 and eal.payment=1 )";
            }
        }

        $typeSql='';
        if(!empty($data["typeSelect"])){
            if($data["typeSelect"]!="All")
            $typeSql.=" and eal.type like :type ";

        }



        $sql="
select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,CASE
when  eal.collection=1 and eal.category=0 then 'Collect Now'
when  eal.collection=2 and eal.payment=1 and eal.category=0
     then 'Deposit Challan'
when eal.category=1 then 'Bill & Payslip'
end as actstat
,eal.id
,eal.category
,ifnull(eatd.amount,0) as collectionAmount
from executive_activity_log eal left join tenant t on t.member_id=eal.tenant_id
left join executive_activity_txn_details eatd on eatd.activity_log_id=eal.id and eatd.type='Collection'
,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and (eal.collection=1 or eal.payment=1 )
and eal.status=0
and eal.date between date(:from_date) and date(:to_date)
$statusSql
$typeSql
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        if(!empty($typeSql)){
            $cmd->bindValue(":type","%".$data["typeSelect"]."%");
        }
        $res["allCollections"]=$cmd->queryAll();
        $res["paidCollections"]=$this->actionPaidCollections();

        $rand = rand(11111111111111,99999999999999);
        Yii::trace(VarDumper::dumpAsString($rand),'vardump');
        $pdfSessionToken= base_convert($rand, 10, 36);
        Yii::$app->session['pdfSessionToken']=$pdfSessionToken;

        $res["pdfSessionToken"]=$pdfSessionToken;


        return ($res);
    }





    public function actionCollectCash(){
        $data=  \Yii::$app->getRequest()->getBodyParams();

        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                    $data["date"]=date('Y-m-d',strtotime($data["date"]));
                    if($eal->collectCash($data["amount"],$data["date"]))
                     return ["success"=>true];
                    else{
                        throw new \Exception ( join(";",$eal->getErrors('errors')));
                    }
                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionDepositCashPayment(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        Yii::trace(VarDumper::dumpAsString($data),'vardump');
        //return;

        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                    if(!empty($_FILES['file'])&&!empty($_FILES['file']['name'])){
                        //$found=false;

                        $file = UploadedFile::getInstanceByName('file');
                        Yii::trace(VarDumper::dumpAsString($file),'vardump');
                        $file->name=$file->baseName .rand(10000000,999999999) .'.' . $file->extension;

                        //Yii::trace(VarDumper::dumpAsString($file->saveAs('uploads/' . $file->name )),'vardump');
                        if($file->saveAs('uploads/' . $file->name )){
                            if(!empty($data["date"])&&strtotime($data["date"])!==false)
                                $data["date"]=date('Y-m-d',strtotime($data["date"]));
                            else{
                                $data["date"]=date('Y-m-d');
                            }
                            if($eal->depositCashPayment($file,$data["date"]))
                                return ["success"=>true];
                            else{
                                throw new \Exception ( join(";",$eal->getErrors('errors')));
                            }
                        }else{
                            throw new \Exception ("Invalid input");
                        }

                    }else{
                        throw new \Exception ("Invalid input");
                    }


                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionEalFullDetails(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $id=isset($data["id"])?$data["id"]:0;
        $sql="select
(select concat(fname,' ',lname) as name from tenant where member_id=eal.tenant_id) as tname
,(select concat(fname,' ',lname) as name from individual where member_id=eal.member_id limit 1
  UNION
  select name from company where member_id=eal.member_id  limit 1
) as mname
,type
from executive_activity_log eal where id=:id  and exec_id= ".Yii::$app->user->id;
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res=$cmd->queryOne();
        $res["nextTxn_no"]="Abdfsd435354";
        return ($res);
    }

    public function actionCollectCheque(){
        $data=  \Yii::$app->getRequest()->getBodyParams();

        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                    //$data["date"]=date('Y-m-d',strtotime($data["date"]));
                    if($eal->collectCheque($data))
                        return ["success"=>true];
                    else{
                        throw new \Exception ( join(";",$eal->getErrors('errors')));
                    }
                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionPaidCollections(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $from_date=$dateA->format('Y-m-d');

        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(date('Y-m-d'),date_interval_create_from_date_string("7 days"));
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');

        $statusSql="";
        if(!empty($data["paidStatusSelect"])){
            if($data["paidStatusSelect"]=="Collection"){
                $statusSql.=" and (eal.collection=1 )";
            }
            if($data["paidStatusSelect"]=="Payment"){
                $statusSql.=" and (eal.collection=2 and eal.payment=1 )";
            }
        }

        $typeSql='';
        if(!empty($data["paidTypeSelect"])){
            if($data["paidTypeSelect"]!="All")
                $typeSql.=" and type like :type ";

        }



        $sql="select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,CASE
when  eal.collection=1 then 'Collect Now'
when  eal.collection=2 and eal.payment=1
    and (activity_id is not null ) then 'Deposit Challan'
end as actstat
,eal.id
from executive_activity_log eal left join tenant t on t.member_id=eal.tenant_id  ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and (eal.collection=2 and ( eal.payment=2 or eal.payment=0 ) )
and eal.status>=0
and eal.date between date(:from_date) and date(:to_date)
$statusSql
$typeSql
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        if(!empty($typeSql)){
            $cmd->bindValue(":type","%".$data["paidTypeSelect"]."%");
        }
        $res=$cmd->queryAll();
        return ($res);
    }

    public function actionPayBillSlipUpload(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        Yii::trace(VarDumper::dumpAsString($data),'vardump');
        //return;

        try {
            if (!empty($data["id"])&&isset($data["billAmount"])&&isset($data["paidAmount"])&&isset($data["paidPerson"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                        //$found=false;

                        $billUploaded=false;
                        $slipUploaded=false;

                        $billUploadFile = UploadedFile::getInstanceByName('billUploadFile');
                        $slipUploadFile = UploadedFile::getInstanceByName('slipUploadFile');
                        Yii::trace(VarDumper::dumpAsString(($billUploadFile)?"true":"false"),'vardump');
                        Yii::trace(VarDumper::dumpAsString(($slipUploadFile)?"true":"false"),'vardump');
                        if($billUploadFile){
                            $billUploadFile->name=$billUploadFile->baseName .rand(10000000,999999999) .'.' . $billUploadFile->extension;
                            if($billUploadFile->saveAs('uploads/' . $billUploadFile->name )){
                                $billUploaded=true;
                            }
                        }
                        if($slipUploadFile){
                            Yii::trace(VarDumper::dumpAsString($slipUploaded),'vardump');

                            $slipUploadFile->name=$slipUploadFile->baseName .rand(10000000,999999999) .'.' . $slipUploadFile->extension;
                            if($slipUploadFile->saveAs('uploads/' . $slipUploadFile->name )){
                                $slipUploaded=true;
                            }
                        }
                    Yii::trace(VarDumper::dumpAsString($billUploaded),'vardump');


                    //Yii::trace(VarDumper::dumpAsString($file->saveAs('uploads/' . $file->name )),'vardump');
                        if($billUploaded || $slipUploaded){

                            if($eal->saveBillSlipUploads($billUploadFile,$slipUploadFile,$data["billAmount"],$data["paidAmount"],$data["paidPerson"])){
                                return ["success"=>true];
                            }else{
                                return ["success"=>false,"errmsg"=>join(";",$eal->getErrors("errors"))];
                            }
                        }else{
                            throw new \Exception ("Invalid input");
                        }




                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionTravelDetailsList(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(date('Y-m-d'),date_interval_create_from_date_string("7 days"));
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');





        $sql="select
        * from executive_travel_details
    where     date between date(:from_date) and date(:to_date)
and
exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();
        return ($res);
    }

    public function actionSaveTravelDetails(){

        $data= \Yii::$app->getRequest()->getBodyParams();
        if(!empty($data["id"])) {
            $etd = ExecutiveTravelDetails::findOne($data["id"]);
        }
        if(empty($etd)){
            $etd=new ExecutiveTravelDetails();
        }
        $etd->attributes=$data;
        $etd->exec_id=Yii::$app->user->id;
        $etd->date=date('Y-m-d');
        if(!$etd->save()){
            $errmsg="";
            foreach($etd->getErrors() as $msg)
                foreach($msg as $m)
                    $errmsg.="$m;";
            return ["success"=>false,"errmsg"=>$errmsg];
        }else return ["success"=>true];

    }

    public function actionPropertyVisitList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(new \DateTime(),date_interval_create_from_date_string("7 days"))->format("YYYY-mm-dd");
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');


        $sql="
select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,CASE
when  eal.collection=1 and eal.category=0 then 'Collect Now'
when  eal.collection=2 and eal.payment=1 and eal.category=0
    and (activity_id is not null ) then 'Deposit Challan'
when eal.category=1 then 'Bill & Payslip'
end as actstat
,eal.id
,eal.category
from executive_activity_log eal left join tenant t on t.member_id=eal.tenant_id  ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and ( eal.collection=0 or eal.payment=0 )
and eal.status=0
and eal.category=3
and eal.date between date(:from_date) and date(:to_date)
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();
        return (["pVisitL"=>$res,"pVisitedL"=>$this->actionPropertyVisitedList()]);
    }

    public function actionAddPropertyVisit(){
        $data= \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                    //$data["date"]=date('Y-m-d',strtotime($data["date"]));
                    $billUploadFiles = UploadedFile::getInstancesByName('billUploadFiles');
                    $slipUploadFiles = UploadedFile::getInstancesByName('slipUploadFiles');

                    if(!empty($billUploadFiles)){
                        foreach($billUploadFiles as $billUploadFile){
                            Yii::trace(VarDumper::dumpAsString($billUploadFile),'vardump');
                            if(!exif_imagetype($billUploadFile->tempName)){
                                throw new Exception("Upload file is not an image");
                            }
                            $billUploadFile->name=$billUploadFile->baseName .rand(10000000,999999999) .'.' . $billUploadFile->extension;
                            if($billUploadFile->saveAs('uploads/' . $billUploadFile->name )){
                                $billUploaded=true;
                            }
                        }
                    }

                    if(!empty($slipUploadFiles)){
                        foreach($slipUploadFiles as $slipUploadFile) {

                            $file_info = new \finfo(FILEINFO_MIME); // object oriented approach!
                            $mime_type = $file_info->buffer(file_get_contents($slipUploadFile->tempName));  // e.g. gives "image/jpeg"
                            Yii::trace(VarDumper::dumpAsString(strpos($mime_type,"video")),'vardump');
                            if(strpos($mime_type,"video")===false){
                                throw new Exception("Upload file is not a video");
                            }
                            Yii::trace(VarDumper::dumpAsString($mime_type),'vardump');
                            $slipUploadFile->name = $slipUploadFile->baseName . rand(10000000, 999999999) . '.' . $slipUploadFile->extension;
                            if ($slipUploadFile->saveAs('uploads/' . $slipUploadFile->name)) {
                                $slipUploaded = true;
                            }
                        }
                    }

                    $data["comment"]=(!empty($data["comment"]))?$data["comment"]:null;
                    $data["videoUrl"]=(!empty($data["videoUrl"]))?$data["videoUrl"]:null;
                    $data["videoUrls"]=(!empty($data["videoUrls"]))?$data["videoUrls"]:[];
                    $data["visitStatus"]=(!empty($data["visitStatus"]))?$data["visitStatus"]:null;

                    if($eal->addPropertyVisit($data["comment"],$billUploadFiles,$slipUploadFiles,$data["videoUrl"],$data["videoUrls"],$data["visitStatus"]))
                        return ["success"=>true];
                    else{
                        throw new \Exception ( join(";",$eal->getErrors('errors')));
                    }
                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionAssociationMeetingList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(date('Y-m-d'),date_interval_create_from_date_string("7 days"));
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');


        $sql="
select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,CASE
when  eal.collection=1 and eal.category=0 then 'Collect Now'
when  eal.collection=2 and eal.payment=1 and eal.category=0
    and (activity_id is not null ) then 'Deposit Challan'
when eal.category=1 then 'Bill & Payslip'
end as actstat
,eal.id
,eal.category
from executive_activity_log eal left join tenant t on t.member_id=eal.tenant_id  ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and ( eal.collection=0 or eal.payment=0 )
and eal.status=0
and eal.category=2
and eal.date between date(:from_date) and date(:to_date)
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();
        return ($res);
    }

    public function actionAddAssociationMeeting(){
        $data= \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {

                    //$data["date"]=date('Y-m-d',strtotime($data["date"]));
                    $billUploadFiles = UploadedFile::getInstancesByName('billUploadFiles');
                    $slipUploadFiles = UploadedFile::getInstancesByName('slipUploadFiles');
                    if(!empty($billUploadFiles)){
                        foreach($billUploadFiles as $billUploadFile){
                            $billUploadFile->name=$billUploadFile->baseName .rand(10000000,999999999) .'.' . $billUploadFile->extension;
                            if(!exif_imagetype($billUploadFile->tm_name)){
                                throw new Exception("Upload file is not an image");
                            }
                            if($billUploadFile->saveAs('uploads/' . $billUploadFile->name )){
                                $billUploaded=true;
                            }
                        }

                    }
                    if(!empty($slipUploadFiles)){
                        foreach($slipUploadFiles as $slipUploadFile) {
                            $slipUploadFile->name = $slipUploadFile->baseName . rand(10000000, 999999999) . '.' . $slipUploadFile->extension;
                            if ($slipUploadFile->saveAs('uploads/' . $slipUploadFile->name)) {
                                $slipUploaded = true;
                            }
                        }
                    }
                    $data["time"]=(!empty($data["time"]))?$data["time"]:null;
                    $data["date"]=(!empty($data["date"]))?$data["date"]:null;
                    $data["venue"]=(!empty($data["venue"]))?$data["venue"]:null;
                    $data["summary"]=(!empty($data["summary"]))?$data["summary"]:null;

                    if($eal->addAssociationMeeting($data,$billUploadFiles,$slipUploadFiles))
                        return ["success"=>true];
                    else{
                        throw new \Exception ( join(";",$eal->getErrors('errors')));
                    }
                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionPropertyVisitedList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(new \DateTime(),date_interval_create_from_date_string("7 days"))->format("YYYY-mm-dd");
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');


        $sql="
select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,CASE
when  eal.collection=1 and eal.category=0 then 'Collect Now'
when  eal.collection=2 and eal.payment=1 and eal.category=0
    and (activity_id is not null ) then 'Deposit Challan'
when eal.category=1 then 'Bill & Payslip'
end as actstat
,eal.id
,eal.category
from executive_activity_log eal left join tenant t on t.member_id=eal.tenant_id  ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and ( eal.collection=0 or eal.payment=0 )
and eal.status=2
and eal.category=3
and eal.date between date(:from_date) and date(:to_date)
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();
        return ($res);
    }

    public function actionPropertyVisitedDetails(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = ExecutiveActivityLog::findOne($data["id"]);
                if ($eal) {
                    $sql="
                        select eatf.*,eatd.transaction_no from
                        executive_activity_txn_details eatd,
                        executive_activity_txn_files   eatf
                        where
                        eatf.txn_id=eatd.id
                        and eatd.mode='Photo Uploads'
                        and eatd.activity_log_id=:id
                     ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$eal->id);
                    $resPhoto=$cmd->queryAll();

                    $sql="
                        select eatf.* from
                        executive_activity_txn_details eatd,
                        executive_activity_txn_files   eatf
                        where
                        eatf.txn_id=eatd.id
                        and eatd.mode='Video Uploads'
                        and eatd.activity_log_id=:id
                     ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$eal->id);
                    $resVideo=$cmd->queryAll();


                    $sql="
                        select pu.name puname,p.locality,pu.block,pu.flat_no from


                        property p,property_units pu
                        where
                         pu.property_id=p.id
                        and pu.id=:id
                     ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$eal->property_id);
                    $resProperty=$cmd->queryOne();

                    return ["log"=>$eal
                        ,"photo"=>$resPhoto
                        ,"video"=>$resVideo
                        ,"property"=>$resProperty
                    ];

                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionDailyAccountingList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(new \DateTime(),date_interval_create_from_date_string("7 days"))->format("YYYY-mm-dd");
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');


        $sql="

select
pu.name as puname,p.name as pname,p.locality
,concat(t.fname,' ',t.lname) as tname
,DATE_FORMAT(eal.date,'%d/%m/%Y') as formatedDate
,eal.type
,eal.collection
,eal.payment
,eal.id
,eal.category
,eatd.mode
,eatd.type as eatdtype
,(if(eatd.type like '%Collection%', eatd.amount,0)) as camount
,(select amount from executive_activity_txn_details td where activity_log_id=eal.id and td.type like '%Deposit%'  ) as pamount
from
executive_activity_log eal
left join ( select * from executive_activity_txn_details where  type like '%Collection%' group by  activity_log_id  ) eatd on eatd.activity_log_id=eal.id

left join tenant t on t.member_id=eal.tenant_id  ,property_units pu,property p
where pu.property_id=p.id and eal.property_id=pu.id
and eal.category=0
and eatd.date between date(:from_date) and date(:to_date)
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();


        $sql="
        select
sum(if(eatd.type like '%Collection%',amount,0)) as totcollection
,sum(ifnull((select amount from executive_activity_txn_details td where activity_log_id=eal.id and td.type like '%Deposit%'  ),0)) as totpayment
from
executive_activity_log eal
left join ( select * from executive_activity_txn_details where  type like '%Collection%' group by  activity_log_id  ) eatd on eatd.activity_log_id=eal.id

where
 eal.category=0
and eatd.date between date(:from_date) and date(:to_date)
and eal.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res1=$cmd->queryOne();

        return ["entries"=>$res,"totCollection"=>$res1["totcollection"],"totPayment"=>$res1["totpayment"]];



    }

    public function actionRequestList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $dateB = new \DateTime();
        //$dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
        $dateA = $dateB->sub(\DateInterval::createFromDateString('0 year'));
        $from_date=$dateA->format('Y-m-d');

        //$to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("7 days")),"Y-m-d");
        $to_date=date_format(date_add(new \DateTime(),date_interval_create_from_date_string("1 days")),"Y-m-d");

        if(!empty($data)&&!empty($data["from_date"])&&strtotime(str_replace('/','-',$data["from_date"]))!==false){
            $from_date=date('Y-m-d',strtotime(str_replace('/','-',$data["from_date"])));
        }
        if(!empty($data)&&!empty($data["to_date"])&&strtotime(str_replace('/','-',$data["to_date"]))!==false){
            $to_date=date('Y-m-d',strtotime(str_replace('/','-',$data["to_date"])));

        }
        Yii::trace(VarDumper::dumpAsString(strtotime($from_date)>strtotime($to_date)),'vardump');
        if(strtotime($from_date)>strtotime($to_date)){
            $dateB = new \DateTime();
            $dateA = $dateB->sub(\DateInterval::createFromDateString('1 year'));
            $from_date=$dateA->format('Y-m-d');

            $to_date=date_add(new \DateTime(),date_interval_create_from_date_string("7 days"))->format("YYYY-mm-dd");
        }
        Yii::trace(VarDumper::dumpAsString($to_date),'vardump');


        $sql="

select
pu.name as puname
,m.name as mname
,ifnull(t.primary_mobile,t.primary_landline) as contact
,DATE_FORMAT(rsa.end_date,'%d/%m/%Y') as formatedDate
,DATE_FORMAT(rsa.start_date,'%d/%m/%Y') as formatedStartDate
,rsa.request_id
,rsa.name
,case
when rsa.member_approval is null and rsa.status=0 then 'Negotiating'
when rsa.status=2 then 'Rejected'
when rsa.member_approval is  not null and rsa.completed_date is null then 'In Progress'
when rsa.completed_date is not null  then 'Completed'
end progress
,rsa.id
,rsa.accepted_cost
,rsa.member_approval
from
request_service_activity rsa
left join property_units pu on rsa.property_id=pu.id

left join
(
select concat(fname,' ',lname) name,member_id,permanent_address_id adress from individual
UNION
select name,member_id,reg_off_address_id  from company
) m on rsa.owner=m.member_id
left join
address a on m.adress=a.id
left join
telephone t on a.telephone_id=t.id
where
 date(created_on) between date(:from_date) and date(:to_date)

and rsa.exec_id=". Yii::$app->user->id."

";

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":from_date",$from_date);
        $cmd->bindValue(":to_date",$to_date);
        $res=$cmd->queryAll();



        return ["entries"=>$res];



    }

    public function actionRequestDetail(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {

                $sql="
select
pu.name as puname
,m.name as mname
,ifnull(t.primary_mobile,t.primary_landline) as contact
,DATE_FORMAT(rsa.end_date,'%d/%m/%Y') as formatedDate
,DATE_FORMAT(rsa.start_date,'%d/%m/%Y') as formatedStartDate
,DATE_FORMAT(rsa.created_on,'%d/%m/%Y %H:%i:%s') as created_date
,rsa.request_id
,rsa.name
,case
when rsa.member_approval is null and rsa.status=0 then 'Negotiating'
when rsa.status=2 then 'Rejected'
when rsa.member_approval is  not null and rsa.completed_date is null then 'In Progress'
when rsa.completed_date is not  null  then 'Completed'
end progress
,rsa.id
,rsa.description
,rsa.proposed_cost
,rsa.accepted_cost
,rsa.executive_approval
,rsa.member_approval
from
request_service_activity rsa
left join property_units pu on rsa.property_id=pu.id
left join
(
select concat(fname,' ',lname) name,member_id,permanent_address_id adress from individual
UNION
select name,member_id,reg_off_address_id  from company
) m on rsa.owner=m.member_id
left join
address a on m.adress=a.id
left join
telephone t on a.telephone_id=t.id
where rsa.exec_id=:eid and rsa.id=:id
";

                Yii::trace(VarDumper::dumpAsString($sql),'vardump');
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$data["id"]);
                $cmd->bindValue(":eid",Yii::$app->user->id);
                $eal=$cmd->queryOne();
                if ($eal) {
                    /*$sql="
                        select pu.name puname,p.locality,pu.block,pu.flat_no from


                        property p,property_units pu
                        where
                         pu.property_id=p.id
                        and pu.id=:id
                     ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$eal->property_id);
                    $resProperty=$cmd->queryOne();*/
                    return [
                        "mstr_detail"=>$eal
                        ,"conversation"=>[]
                    ];

                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    public function actionRsaApprove(){
        $data= \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = RequestServiceActivity::findOne($data["id"]);
                if ($eal) {

                    $eal->end_date=isset($data["date"])?str_replace("/","-",$data["date"]):null;
                    $eal->start_date=(strtotime($eal->end_date)!==false)?date('Y-m-d',strtotime($eal->end_date)):null;
                    $eal->accepted_cost=isset($data["accepted_cost"])?$data["accepted_cost"]:null;

                    if($eal->exec_id!=Yii::$app->user->id){
                        throw new \Exception ("Un-authorized access");
                    }
                    if($eal->member_approval||$eal->admin_approval){
                        throw new \Exception ("Member or admin already approved");
                    }

                    $eal->executive_approval=Yii::$app->user->id;
                    if($eal->save()){
                        return ["success"=>true];

                    }else{
                        $errmsg='';
                        foreach($eal->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";

                        throw new \Exception ( $errmsg);
                    }


                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }


    public function actionRsaComplete(){
        $data= \Yii::$app->getRequest()->getBodyParams();
        try {
            if (!empty($data["id"])) {
                $eal = RequestServiceActivity::findOne($data["id"]);
                if ($eal) {

                    //$eal->completed_date=$data["date"];
                    $eal->completed_date=isset($data["date"])?str_replace("/","-",$data["date"]):null;
                    //Yii::trace(VarDumper::dumpAsString($eal->completed_date),'vardump');
                    $eal->completed_date=(strtotime($eal->completed_date)!==false)?date('Y-m-d',strtotime($eal->completed_date)):null;
                    //Yii::trace(VarDumper::dumpAsString($eal->completed_date),'vardump');
                    //Yii::trace(VarDumper::dumpAsString((!$eal->completed_date)?"true":"false"),'vardump');
                    $eal->final_cost=(isset($data["final_cost"])&&is_numeric($data["final_cost"]))?$data["final_cost"]:0;

                    if(!$eal->completed_date){
                        throw new \Exception ("Completed Date is invalid");
                    }

                    if(strtotime($eal->completed_date)<strtotime($eal->start_date)){
                        throw new \Exception ("Completed Date is greater Accepted Date");
                    }

                    if($eal->exec_id!=Yii::$app->user->id){
                        throw new \Exception ("Un-authorized access");
                    }

                    if(!$eal->member_approval){
                        throw new \Exception ("Member or admin has not given approval");
                    }


                    //$eal->executive_approval=Yii::$app->user->id;
                    if($eal->save()){
                        return ["success"=>true];
                    }else{
                        $errmsg='';
                        foreach($eal->getErrors() as $msg)
                            foreach($msg as $m)
                                $errmsg.="$m";
                        throw new \Exception ( $errmsg);
                    }
                }else{
                    throw new \Exception ("Invalid input");
                }
            }else{
                throw new \Exception ("Invalid input");
            }
        }catch (\Exception $e){
            return ["success"=>false,"errmsg"=> $e->getMessage()];
        }

    }

    //added by pooja
    public function actionMembersList(){

        $data=  \Yii::$app->getRequest()->getBodyParams();

        $sql="
select
pu.name pid
,p.name pname
,p.locality
,s.tos
,date_format(s.expiry_date,'%d/%m/%Y')as date
,m.name
,ifnull(t.primary_mobile,t.primary_landline) contactno
,u.id
from
executive_properties ep,property_units pu,property p,service s,user u
,(
select concat(fname,' ',lname) as name,permanent_address_id as address_id,member_id
from individual
 UNION
 select name ,	reg_off_address_id,member_id  from company
 ) as m
 ,address a,telephone t
where ep.property=pu.id and pu.property_id=p.id and ep.service_id=s.id and s.member_id=u.id
and u.id=m.member_id and m.address_id=a.id and a.telephone_id=t.id
and ep.exec_id=". Yii::$app->user->id;

        Yii::trace(VarDumper::dumpAsString($sql),'vardump');
        $cmd=Yii::$app->db->createCommand($sql);

        $res=$cmd->queryAll();

        return ["entries"=>$res];



    }

    public function actionMemberDetails(){

        $data=  \Yii::$app->getRequest()->getBodyParams();
        $sql="select i.fname,i.lname,i.relation,i.relative,i.email,date_format(i.dob,'%d-%m-%Y')as date,
              i.spouse,date_format(i.wed_ann,'%d-%m-%Y')as date1,
              i.residential_status,i.pan,i.pan_filepath,
              CASE WHEN t.primary_mobile is not null
              THEN t.primary_mobile WHEN t.secondary_mobile is not null THEN t.secondary_mobile
              WHEN t.primary_landline is not null THEN t.primary_landline WHEN t.secondary_landline
              is not null THEN t.secondary_landline
              END as contact
             ,url_replace( concat('{$this->uploadsUrl}',i.profile_image))  as profile_image
             ,url_replace( concat('{$this->uploadsUrl}',i.pan_filepath))  as pan_filepath
             ,url_replace( concat('{$this->uploadsUrl}',i.present_address_proof))  as present_address_proof
             ,url_replace( concat('{$this->uploadsUrl}',i.permanent_address_proof))  as permanent_address_proof

              from individual i,address a,
              telephone t where i.permanent_address_id=a.id and a.telephone_id=t.id and
              member_id=:id ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result=$stat->queryOne();

        /*Present address*/
        $sql="select a.adrs_line1,a.adrs_line2,a.postal_code,a.city,a.state,a.country,t.primary_landline,
              t.secondary_landline,t.primary_mobile,t.secondary_mobile,i.id_proofpath,
              i.present_address_proof from individual i,address a,telephone t WHERE
              a.id=i.present_address_id and a.telephone_id=t.id and member_id=:id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result1=$stat->queryOne();

        /*Permanent address*/
        $sql="select a.adrs_line1,a.adrs_line2,a.postal_code,a.city,a.state,a.country,t.primary_landline,
              t.secondary_landline,t.primary_mobile,t.secondary_mobile,i.id_proofpath,
              i.permanent_address_proof from individual i,address a,telephone t WHERE
              a.id=i.permanent_address_id and a.telephone_id=t.id and member_id=:id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result2=$stat->queryOne();

        /*work address*/
        $sql="select a.adrs_line1,a.adrs_line2,a.postal_code,a.city,a.state,a.country,
              t.primary_landline,t.secondary_landline, t.primary_mobile,i.company_name,i.designation from
              individual i,address a,telephone t WHERE a.id=i.office_address_id and
              a.telephone_id=t.id and member_id=:id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result3=$stat->queryOne();

        $sql="SELECT pu.id,pp.pic,a.adrs_line1,tpa.rent_lease,pu.property_id,pu.name,pu.flat_no,
              pu.block,pu.door_facing,pu.furnished_status,pu.no_bedrooms,p.name as property_name,
              s.tos,s.expiry_date
               , url_replace( concat('{$this->uploadsUrl}',pp.pic))  as banner_image

              FROM tenant_property_allocation tpa,address a,
              `property_units` pu left join service s on pu.id=s.property_id and s.is_active=1
              ,property p left join  properties_pics pp on  p.id=pp.prop_id and pp.type='Banner Image'
 WHERE
              pu.property_id=p.id and p.member_id=:id and p.address_id=a.id
              and s.id=tpa.service_id and p.is_active=1  and tpa.is_active=1
              ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result4=$stat->queryAll();
        return [
            "member_details"=>$result,
            "present_address"=>$result1,
            "permanent_address"=>$result2,
            "work_address"=>$result3,
            "property_address"=>$result4
        ];

    }

    public function actionMemberPropertyDetails(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        $sql="SELECT e.collection_day,tpa.advance as deposit,tpa.maintenance,tpa.rent_amt,tpa.maintenance as maintain,
              ten.member_id,ten.fname as tenent_fname,ten.lname as tenent_lname,ten.email_id as email,t2.primary_landline as
              tenent_phone,t2.primary_mobile as tenent_mobile,t1.primary_mobile as
              local_contact,per.fname as local_fname,per.lname as local_lname ,i.fname,
              i.lname,t.primary_landline,t.primary_mobile,p.incharge_name,a.adrs_line1,
              pu.door_facing,pu.name,pu.furnished_status,pu.no_bedrooms,pu.flat_no,pu.block,
              p.name as property_name
              FROM `property_units` pu
              left join service s on s.property_id=pu.id and s.is_active=1
              left join tenant_property_allocation tpa on tpa.service_id=s.id and tpa.is_active=1 left join
              executive_activity_property e on e.tenant_service=tpa.id and
              e.type='Rent Collection' and e.is_active=1
              left join tenant ten on ten.member_id=tpa.tenant_id
              left join address a2 on a2.id=ten.permanent_address_id
              left join telephone t2 on t2.id=a2.telephone_id,property p
              left join individual i on p.member_id=i.member_id
              left join person per
              on per.id=i.local_person
              left join address a1 on per.address=a1.id
              left join telephone t1 on t1.id=a1.telephone_id,address a,telephone t
              WHERE pu.id=:id
              and pu.property_id=p.id and a.id=p.address_id and a.telephone_id=t.id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result5=$stat->queryOne();

        $sql="SELECT t3.primary_mobile as owner_phone, e.collection_day,tpa.advance as deposit,
              date_format(tpa.start_date,'%b-%Y') as start_date,date_format(tpa.expiry_date,'%b-%Y')as expiry_date,
              tpa.increment_perc,tpa.maintenance,tpa.rent_amt,tpa.maintenance as maintain,
              ten.fname as tenent_fname,ten.lname as tenent_lname,ten.email_id as email,t2.primary_landline as
              tenent_phone,t2.primary_mobile as tenent_mobile,t1.primary_mobile as
              local_contact,per.fname as local_fname,per.lname as local_lname ,i.fname,
              i.lname,t.primary_landline,t.primary_mobile,p.incharge_name,a.adrs_line1,
              pu.door_facing,pu.name,pu.furnished_status,pu.no_bedrooms,pu.flat_no,pu.block,
              p.name as property_name FROM `property_units` pu left join service s
              on s.property_id=pu.id and s.is_active=1 left join tenant_property_allocation
              tpa on tpa.service_id=s.id and tpa.is_active=1 left join
              executive_activity_property e on e.tenant_service=tpa.id and
              e.type='Regular Maintenance Fee' and e.is_active=1 left join tenant ten on
              ten.member_id=tpa.tenant_id left join address a2 on a2.id=ten.permanent_address_id
              left join telephone t2 on t2.id=a2.telephone_id,property p
              left join individual i on p.member_id=i.member_id left join address a3 on i.permanent_address_id=a3.id left join telephone t3 on a3.telephone_id=t3.id left join person per
              on per.id=i.local_person left join address a1 on per.address=a1.id left join
              telephone t1 on t1.id=a1.telephone_id,address a,telephone t WHERE pu.id=:id
              and pu.property_id=p.id and a.id=p.address_id and a.telephone_id=t.id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result6=$stat->queryOne();


        $id=$data["id"];
        $sql="
       select

        pu.id
        ,if(p.member_id is null,'Developer','Member') as memberordeveloper
        ,p.lat as lat
        ,p.long as longitude
    ,p.name as proj_name
    ,p.builder_name as builder_name
    ,a.adrs_line1 as address
    ,p.locality as locality
    ,a.city as city
     ,pu.no_bedrooms
     ,pu.no_bathrooms
     ,pu.no_servant_room
     ,pu.no_balconies
 ,pt.name as prop_type
,pu.furnished_status
,pu.builtup_area
,p.proj_status
,case
 when p.proj_start_date='1970-01-01' then null
 when p.proj_start_date!='1970-01-01' then DATE_FORMAT(proj_start_date,'%d %b %Y')
 end as launched_date
,case
 when p.poss_date='1970-01-01' then null
 when p.poss_date!='1970-01-01' then DATE_FORMAT(poss_date,'%b %Y')
 end as poss_date
,case
 when p.poss_date='1970-01-01' then null
 when p.poss_date!='1970-01-01' and date(poss_date)>=date(now()) then DATE_FORMAT(now(),'%d %b %Y')
 when p.poss_date!='1970-01-01' and date(poss_date)<date(now()) then null
 end as ongoing_date
,p.overview
 ,case
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=1 then 'For rent,lease and sale'
 when puf.is_rent=1 and puf.is_lease=1 and puf.for_sale=0 then 'For rent and lease'
 when puf.is_rent=0 and puf.is_lease=1 and puf.for_sale=1 then 'For sale and lease'
 when puf.for_sale=1  then 'For sale'
 when puf.is_rent=1  then 'For rent'
 when puf.is_lease=1  then 'For lease'
 end as rentorsale
 ,url_replace( concat('{$this->uploadsUrl}',pp.pic)) as banner_image
 ,p.name as proj_name
 ,concat(pu.no_bedrooms,' BHK') as bedroom_type
 ,pt.name as prop_type
 ,p.locality
 ,a.city
 ,url_replace( concat('{$this->uploadsUrl}',pu.image)) as floor_plan
 ,url_replace( concat('{$this->uploadsUrl}',ppm.pic)) as master_plan
,p.locality_desc
 ,case
 when puf.starting_price>=10000000 then concat(ROUND(puf.starting_price/10000000,2) ,' Cr')
 when puf.starting_price>=100000 then concat(ROUND(puf.starting_price/100000,2) ,' Lacs')
 when puf.starting_price<100000 then puf.starting_price
 end as starting_price
,pu.door_facing
,pu.carepet_area as carpet_area
,pu.super_builtup_area
,p.approvals
,p.loan
 from
property p
left join properties_pics pp on p.id=pp.prop_id and pp.type='Banner Image'
left join properties_pics ppm on p.id=ppm.prop_id and ppm.type='Master Plan'

,address a,property_type pt
,property_units pu
,(
select
case
when for_sale=1 then starting_price
when is_lease=1 then min_lease

when is_rent=1 then min_rent


end as starting_price
,prop_unit_id,is_rent,is_lease,for_sale
from prop_unit_finance
) puf

where
pu.property_id=p.id
and a.id=address_id
and puf.prop_unit_id=pu.id
and p.prop_type=pt.id

and pu.id=:id

        ";

        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res=$cmd->queryOne();

        $sql="
            select area_partition_type type ,count(area_partition_type) as no,
            url_replace( concat('{$this->uploadsUrl}',icon)) as icon from prop_area_partition,
            property_partition_type
            where prop_area_partition.area_partition_type=property_partition_type.partition_type
            and prop_area_partition.punit_id=:id group by area_partition_type;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["area_partitions"]=$cmd->queryAll();

        $sql="
            select vehicle_type ,no_of_vehicles as no
            from prop_unit_parking where prop_unit_id=:id ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["parking"]=$cmd->queryAll();

        /*added by pooja*/
        $sql="select t.tenant_family_type,t.food_habits,pu.are_pets_allowed
              from property_units pu,tenant_property_allocation tpa,tenant t
              where pu.id=19 and pu.id=tpa.prop_id and tpa.tenant_id=t.member_id
              and tpa.is_active=1 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["tenant"]=$cmd->queryOne();


        $sql="
            select heading,description,(select url_replace( concat('{$this->uploadsUrl}',icon)) from amenity_type WHERE type='Facility' and amenity like heading) icon
            from prop_unit_interior where prop_unit_id =:id  and category='Additional Facilities' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["additional_facilities"]=$cmd->queryAll();


        $sql="
            select pa.amenity,url_replace( concat('{$this->uploadsUrl}',icon)) as icon
            from property_amenities pa,amenity_type at where at.amenity=pa.amenity and property_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["amenity"]=$cmd->queryAll();



        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Flooring' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Flooring"]=$cmd->queryAll();

        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Fittings' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Fittings"]=$cmd->queryAll();

        $sql="
            select heading,description
            from prop_unit_interior where prop_unit_id =:id  and category='Walls' ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["specs"]["Walls"]=$cmd->queryAll();



        $sql="
            select
             url_replace( concat('{$this->uploadsUrl}',pic)) as pic

            from properties_pics pp  where pp.type='Gallery' and prop_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["gallery"]=$cmd->queryColumn();


        $sql="
            select
             url_replace( concat('{$this->uploadsUrl}',pic)) as pic

            from properties_pics pp  where pp.type='Construction Status Photos' and prop_id =( select property_id from property_units where id=:id ) ;
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$id);
        $res["construction_photos"]=$cmd->queryColumn();


        /*$sql="
            select
            bk.loan
             ,url_replace( concat('{$this->uploadsUrl}',bk.icon)) as icon

            from property p,bank_loans  bk
            where
            bk.loan in (p.loan) and
            p.id =( select property_id from property_units where id=:id ) ;
        ";*/

        if($res){
            Yii::trace(VarDumper::dumpAsString($res),'vardump');
            $loan=explode(",",$res["loan"]);
            $loans='';
            Yii::trace(VarDumper::dumpAsString($loan),'vardump');

            foreach($loan as $l){

                $loans.="'{$l}',";
            }
            Yii::trace(VarDumper::dumpAsString($loans),'vardump');
            Yii::trace(VarDumper::dumpAsString(strlen($loans)-1),'vardump');
            $loans1=substr($loans,0,count($loans)-2);
            Yii::trace(VarDumper::dumpAsString($loans1),'vardump');

            $sql="select
            bk.loan
             ,url_replace( concat('{$this->uploadsUrl}',bk.icon)) as icon

            from bank_loans  bk
            where
            bk.loan in ($loans1)";
            $cmd=Yii::$app->db->createCommand($sql);


            //$cmd->bindValue(":loan",$loans1);
            $res["loan"]=$cmd->queryAll();
        }else{
            $res["loan"]=[];
        }


        //  return ($res);

        /*Rent details (added by pooja)*/
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="
select * from (
select 1 as mno ,'Jan' as mname
union
select 2 ,'Feb' as mname
union
select 3 ,'Mar' as mname
union
select 4 ,'Apr' as mname
union
select 5 ,'May' as mname
union
select 6 ,'Jun' as mname
union
select 7 ,'Jul' as mname
union
select 8 ,'Aug' as mname
union
select 9 ,'Sep' as mname
union
select 10 ,'Oct' as mname
union
select 11 ,'Nov' as mname
union
select 12 ,'Dec' as mname
) as month_no
left JOIN
(
select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
               e_act_log.type='Rent Collection'
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
 ) t on month_no.mno= t.mno              ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $collectedRents=$stat->queryAll();
        /*Rent details (added by pooja)*/


        // begin regular maintenance fee
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="
select * from (
select 1 as mno ,'Jan' as mname
union
select 2 ,'Feb' as mname
union
select 3 ,'Mar' as mname
union
select 4 ,'Apr' as mname
union
select 5 ,'May' as mname
union
select 6 ,'Jun' as mname
union
select 7 ,'Jul' as mname
union
select 8 ,'Aug' as mname
union
select 9 ,'Sep' as mname
union
select 10 ,'Oct' as mname
union
select 11 ,'Nov' as mname
union
select 12 ,'Dec' as mname
) as month_no
left JOIN
(
select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
               e_act_log.type='Regular Maintenance Fee'
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
 ) t on month_no.mno= t.mno              ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $collectedRMF=$stat->queryAll();

        // end regular maintenance fee


        // begin Electricity Bill
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="
select * from (
select 1 as mno ,'Jan' as mname
union
select 2 ,'Feb' as mname
union
select 3 ,'Mar' as mname
union
select 4 ,'Apr' as mname
union
select 5 ,'May' as mname
union
select 6 ,'Jun' as mname
union
select 7 ,'Jul' as mname
union
select 8 ,'Aug' as mname
union
select 9 ,'Sep' as mname
union
select 10 ,'Oct' as mname
union
select 11 ,'Nov' as mname
union
select 12 ,'Dec' as mname
) as month_no
left JOIN
(
select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
               e_act_log.type like 'Electricity Bill%'
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
 ) t on month_no.mno= t.mno              ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $collectedEB=$stat->queryAll();
        // end Electricity Bill


        // begin Property Visit
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="
select * from (
select 1 as mno ,'Jan' as mname
union
select 2 ,'Feb' as mname
union
select 3 ,'Mar' as mname
union
select 4 ,'Apr' as mname
union
select 5 ,'May' as mname
union
select 6 ,'Jun' as mname
union
select 7 ,'Jul' as mname
union
select 8 ,'Aug' as mname
union
select 9 ,'Sep' as mname
union
select 10 ,'Oct' as mname
union
select 11 ,'Nov' as mname
union
select 12 ,'Dec' as mname
) as month_no
left JOIN
(
select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
                ( e_act_log.collection=0 or e_act_log.payment=0 )
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
 ) t on month_no.mno= t.mno              ";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $propetyVisits=$stat->queryAll();
        // end Property Visit


        // begin association meeting
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="
select * from (
select 1 as mno ,'Jan' as mname
union
select 2 ,'Feb' as mname
union
select 3 ,'Mar' as mname
union
select 4 ,'Apr' as mname
union
select 5 ,'May' as mname
union
select 6 ,'Jun' as mname
union
select 7 ,'Jul' as mname
union
select 8 ,'Aug' as mname
union
select 9 ,'Sep' as mname
union
select 10 ,'Oct' as mname
union
select 11 ,'Nov' as mname
union
select 12 ,'Dec' as mname
) as month_no
left JOIN
(
select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
                 e_act_log.type like 'Association Meetings%'
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
 ) t on month_no.mno= t.mno              ";

        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $assocMeeting=$stat->queryAll();
        // end assoiation meeting


        // begin property tax
        $data["year"]=isset($data["year"])?$data["year"]:date('Y');
        $sql="

select e_act_log.id as activity_id ,e_txn_details.id, date_format(e_txn_details.date,'%b %d,%Y') as formattedDate
,if(month(e_txn_details.date)<=3,concat(year(e_txn_details.date)-1,'-',year(e_txn_details.date)),concat(year(e_txn_details.date),'-',year(e_txn_details.date)+1)) as years
,month(e_txn_details.date) as mno
 ,(select id from executive_activity_txn_details where activity_log_id=e_act_log.id and type='Slip Deposit' ) as sid
 from executive_activity_log e_act_log,
              ( select * from  executive_activity_txn_details where type='Collection' or type='Bill Collection' group by activity_log_id ) e_txn_details
               where
                 e_act_log.type like 'Property Tax%'
              and e_act_log.property_id=:id and
              e_txn_details.activity_log_id=e_act_log.id
               and year(e_act_log.date)=:year
            ";

        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $stat->bindValue(":year",$data["year"]);
        $popertyTax=$stat->queryAll();
        // end property tax

        $rand = rand(11111111111111,99999999999999);
        Yii::trace(VarDumper::dumpAsString($rand),'vardump');
        $pdfSessionToken= base_convert($rand, 10, 36);
        Yii::$app->session['pdfSessionToken']=$pdfSessionToken;

        return [
            "member_property_detail"=>$result5,
            "member_property_detail1"=>$result6,
            "member_de"=>$res,
            "collectedRents"=>$collectedRents,
            "collectedRMF"=>$collectedRMF,
            "collectedEB"=>$collectedEB,
            "assocMeeting"=>$assocMeeting,
            "popertyTax"=>$popertyTax,
            "pdfSessionToken"=>$pdfSessionToken,
        ];
    }

    public function actionTenantDetails(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        $sql="select t.email_id,t.dob,t.company,t.designation,t.marital_status,t.spouse,t.wed_ann,t.relation,t.relative,
              a2.adrs_line1 as off_address,a2.city as off_city,a2.state as off_state,
              a2.country as off_country,a2.postal_code as off_postal_code,a1.postal_code
              as perm_postal_code,t2.primary_landline as off_pri_landline,
              t2.secondary_landline as off_sec_landline,t2.primary_mobile as
              off_pri_mobile,t2.secondary_mobile as off_sec_mobile,t.id_proof_path,
              a1.adrs_line1 as perm_address,a1.city as perm_city,a1.state as perm_state,
              a1.country as perm_country,t1.primary_landline as perm_pri_landline,
              t1.secondary_landline as perm_sec_landline,t1.primary_mobile as
              perm_pri_mobile,t1.secondary_mobile as perm_sec_mobile,t.address_proof_path,
              t.fname,t.lname,a.adrs_line1 as present_address,a.city as present_address_city,
              a.state as present_address_state,a.country as present_address_country,
              a.postal_code as present_address_postal_code,tel.primary_landline as
              present_address_primary_landline,tel.secondary_landline
              as present_address_sec_landline,tel.primary_mobile as
              present_address_pri_mobile,tel.secondary_mobile as present_address_sec_mobile
              from tenant t left join address a2 on a2.id=t.office_address_id left join
              telephone t2 on a2.telephone_id=t2.id  left join address a1 on
              a1.id=t.permanent_address_id left join telephone t1 on t1.id=a1.telephone_id
              left join address a on t.present_address_id=a.id left join telephone tel on
              tel.id=a.telephone_id where member_id=:id";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result7=$stat->queryOne();

        $sql="select date_format(tpa.expiry_date,'%b-%Y')as exp_date,
              date_format(tpa.start_date,'%b-%Y')as st_date,tpa.agreement,
              e.collection_day,tpa.rent_amt,tpa.maintenance,tpa.increment_perc,
              tpa.advance,a.adrs_line1,a.city,a.state,a.country,a.postal_code,pu.flat_no,
              pu.door_facing,pu.	block,pu.furnished_status,pu.no_bedrooms,p.name,
              t.food_habits,t.tenant_family_type,t.no_people from
              executive_activity_property e,tenant_property_allocation tpa,tenant t,
              property_units pu,property p left join address a on a.id=p.address_id
              where pu.id=tpa.prop_id and pu.is_active=1 and pu.property_id=p.id
              and t.member_id=tpa.tenant_id and tpa.id=e.tenant_service and e.is_active=1
              and e.type='Rent Collection' and t.member_id=104 and tpa.is_active=1";
        $stat=Yii::$app->db->createCommand($sql);
        $stat->bindValue(":id",$data["id"]);
        $result8=$stat->queryAll();

        return [
            "tenant_details"=>$result7,
            "tenant_rent_details"=>$result8
        ];
    }

    public function actionViewPaymentCollection(){
        $data=  \Yii::$app->getRequest()->getBodyParams();
        $sql="
            select *
            from executive_activity_log eal
            ,executive_activity_txn_details eatd
            ,executive_activity_txn_files eatf
            where
            eal.id=eatd.activity_log_id
            and eatf.txn_id=eatd.id
            and eal.id=:id
        ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$data["id"]);
        $res=$cmd->queryAll();

    }

    public function actionCreatempdf(){

	        $mpdf=new mPDF();
	        $mpdf->WriteHTML($this->renderPartial('viewBillPaySlipPdf'));
	        $mpdf->Output();
	        exit;
    }

    public function actionExecutiveActivityDetailsPdf($id,$pTkn){
        if(!isset(Yii::$app->session)&&!isset(Yii::$app->session['pdfSessionToken'])){
            return "Access Denied; User should log in";
        }
        if(Yii::$app->session['pdfSessionToken']!=$pTkn){
            return "Access Denied; Invalid Data";
        }

        $mpdf=new mPDF();
        $mpdf->WriteHTML($this->renderPartial('viewBillPaySlipPdf',['id'=>$id]));
        $mpdf->Output("vhp.pdf", "I");
        exit;
    }


}