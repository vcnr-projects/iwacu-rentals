<?php

namespace app\modules\dashboard\controllers;

use common\models\LoginForm;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        /*$behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];*/
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            /*'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],*/
            'auth' => [$this, 'auth']
        ];
       /* $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            //'only' => ['dashboard'],
        ];*/

        /*$behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];*/

        $behaviors['access'] = [
        'class' => AccessControl::className(),
        'only' => ['test', 'signup'],
        'rules' => [
                [
                    'actions' => ['test'],
                    'allow' => true,
                    'roles' => ['@'],
                ],

            ],
        ]   ;

        return $behaviors;
    }

    public function Auth($username, $password) {
        // username, password are mandatory fields
        if(empty($username) || empty($password))
            return null;

        // get user using requested email
        $user = User::findOne([
            'username' => $username,
        ]);

        // if no record matching the requested user
        if(empty($user))
            return null;

        // hashed password from user record
        // $this->user_password = $user->password_hash;

        // validate password

        $isPass = \Yii::$app->security->validatePassword($password, $user->password_hash);
        \Yii::trace(VarDumper::dumpAsString($isPass),'vardump');
        // if password validation fails
        if(!$isPass)
            return null;

        // if user validates (both user_email, user_password are valid)
        return $user;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest(){

        \Yii::$app->response->format=Response::FORMAT_JSON;
        //var_dump(\Yii::$app->user->getId());
        return (["success"=>true]);
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            return ['access_token' => Yii::$app->user->identity->getAuthKey()];
        } else {
            $model->validate();
            return $model;
        }
    }

    public function actionDashboard()
    {
        $response = [
            'username' => Yii::$app->user->identity->username,
            'access_token' => Yii::$app->user->identity->getAuthKey(),
        ];

        return $response;
    }

}
