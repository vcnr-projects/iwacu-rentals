<?php
$sql="
            select
            eal.*,pu.name puname,p.locality,pu.block,pu.flat_no
            ,m.name as member_name
            ,concat(t.fname,' ',t.lname) as tenant_name
            from executive_activity_log eal
            left join property_units pu on eal.property_id=pu.id
            left join property p on  pu.property_id=p.id
            left join executive_activity_txn_details eatd on  eal.id=eatd.activity_log_id
            left join  executive_activity_txn_files eatf on eatf.txn_id=eatd.id
            left join
(
select concat(fname,' ',lname) name,member_id,permanent_address_id adress from individual
UNION
select name,member_id,reg_off_address_id  from company
) m on eal.member_id=m.member_id
            left join tenant t on t.member_id=eal.tenant_id
            where
            eal.id=:id
        ";
$sql="
            select
            eal.*,pu.name puname,p.locality,pu.block,pu.flat_no
            ,m.name as member_name
            ,concat(t.fname,' ',t.lname) as tenant_name
            from executive_activity_log eal
            left join property_units pu on eal.property_id=pu.id
            left join property p on  pu.property_id=p.id
                       left join
(
select concat(fname,' ',lname) name,member_id,permanent_address_id adress from individual
UNION
select name,member_id,reg_off_address_id  from company
) m on eal.member_id=m.member_id
            left join tenant t on t.member_id=eal.tenant_id
            where
            eal.id=:id
        ";
$cmd=Yii::$app->db->createCommand($sql);
$cmd->bindValue(":id",$id);
$eal=$cmd->queryOne();


if($eal){
    ?>
    <style>
        table th{
            text-align: left;
        }
    </style>
    <div style="text-align: center">
    <h3>Activity Type : <?=$eal["type"]  ?>  </h3>
     <table width="100%">
         <tbody>
         <tr>
             <th>Property Name</th><td>: <?=$eal["puname"] ?></td>
             <th>Member Name</th><td>: <?=$eal["member_name"] ?></td>

         </tr>
         <tr>
             <th>Tenant Name</th><td>: <?=$eal["tenant_name"] ?></td>
             <th>Locality</th><td>: <?=$eal["locality"] ?></td>
         </tr>
         </tbody>
     </table>

        <hr/>
        <?php
        $sql="select eatd.*
,date_format(date,'%d-%m-%Y') as fdate
,date_format(cheque_date,'%d-%m-%Y') as chqdate from
executive_activity_txn_details eatd
left join executive_activity_txn_files eatf on eatf.txn_id=eatd.id
where  eatd.activity_log_id=:id and eatd.type='Collection' ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$eal["id"]);
        $bcollection=$cmd->queryOne();
        if(!empty($bcollection)){
            ?>
            <h3>Receipt  Type: <?= $bcollection["mode"] ?></h3>
            <table width="100%">
                <tbody>
            <?php if(strpos($bcollection["mode"],"Cash")!==false){?>
                <tr>
                    <th>Cash Amount</th><td>: <?=$bcollection["amount"] ?></td>
                    <th>Date</th><td>: <?=$bcollection["fdate"] ?></td>
                </tr>
            <?php }elseif(strpos($bcollection["mode"],"Cheque")!==false){
                ?>
                <tr>
                    <th>Cheque/DD Amount</th><td>: &#8377;<?=$bcollection["amount"] ?></td>
                    <th>Date</th><td>: <?=$bcollection["fdate"] ?></td>
                </tr>
                <tr>
                    <th>Cheque NO.</th><td>: <?=$bcollection["cheque_no"] ?></td>
                    <th>Cheque Date</th><td>: <?=$bcollection["chqdate"] ?></td>
                </tr>
                <tr>
                    <th>Bank</th><td>: <?=$bcollection["bank"] ?></td>
                    <th>Branch</th><td>: <?=$bcollection["branch"] ?></td>
                </tr>
                <?php
            }  elseif(strpos($bcollection["mode"],"Online")!==false){
            ?>
            <tr>
                <th>Transaction No</th><td>: <?=$bcollection["transaction_no"] ?></td>
                <th>Date</th><td>: <?=$bcollection["fdate"] ?></td>
            </tr>
            <tr>
                <th>Bank</th><td>: <?=$bcollection["bank"] ?></td>
                <th>Transaction Date</th><td>: <?=$bcollection["chqdate"] ?></td>
            </tr>

        <?php
        } elseif(strpos($bcollection["mode"],"Card")!==false){ ?>
            <tr>
                <th>Transaction No</th><td>: <?=$bcollection["transaction_no"] ?></td>
                <th>Date</th><td>: <?=$bcollection["fdate"] ?></td>
            </tr>
            <tr>
                <th>Bank</th><td>: <?=$bcollection["bank"] ?></td>

                <th>Transaction Date</th><td>: <?=$bcollection["chqdate"] ?></td>
            </tr>
                <tr>
                    <th>Card Type</th><td>: <?=$bcollection["branch"] ?></td>
                </tr>

        <?php } ?>
                </tbody>
            </table>
            <?php
            //receipt end
        }
        ?>
        <?php
        $sql="select eatd.amount,eatf.file_path from
executive_activity_txn_details eatd
left join executive_activity_txn_files eatf on eatf.txn_id=eatd.id
where  eatd.activity_log_id=:id and eatd.type='Bill Collection' ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$eal["id"]);
        $bcollection=$cmd->queryAll();

        $sql="select eatd.amount,eatf.file_path from
executive_activity_txn_details eatd
left join executive_activity_txn_files eatf on eatf.txn_id=eatd.id
where  eatd.activity_log_id=:id and eatd.type='Slip Deposit' ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$eal["id"]);
        $slips=$cmd->queryAll();
        ?>
        <hr/>
        <table width="100%">
            <tr>
                <th>Paid Amount</th><td>: &#8377;<?= !empty($bcollection)?$bcollection[0]["amount"] :"" ?></td>
                <th>Bill Amount</th><td>: &#8377;<?= !empty($slips)?$slips[0]["amount"] :"" ?></td>
            </tr>
        </table>
        <hr/>
        <?php

        if(!empty($bcollection)){
            ?>
            <h3>Uploaded Bills </h3>
            <div> Paid Amount: &#8377;<?= $bcollection[0]["amount"]?> </div>
            <?php
                for($i=0;$i<count($bcollection);$i++){
                   echo "<img alt='pay slip' src='/vhp1/app/uploads/{$bcollection[$i]["file_path"]}' />";

                }
            ?>

        <?php
            //bcollect end
        }
        ?>
        <hr/>
        <?php

        if(!empty($slips)){
            ?>
            <h3>Uploaded Slip </h3>
            <div> Bill Amount: &#8377;<?= $slips[0]["amount"]?> </div>
            <?php
            for($i=0;$i<count($slips);$i++){
                echo "<img alt='pay slip' src='/vhp1/app/uploads/{$slips[$i]["file_path"]}' />";

            }
            ?>

            <?php
            //slip end
        }
        ?>

    </div>

<?php
    //eal end
}

?>






