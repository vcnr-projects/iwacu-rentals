<?php

namespace frontend\components;

use common\models\User;
use yii\helpers\VarDumper;

class AccessRule extends \yii\filters\AccessRule {

    /**
     * @inheritdoc
     */
    protected function matchRole($user)
    {
        $cuser=User::findOne($user->id);

        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {

            if ($role == '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role == $cuser->user_type) {
                if (!$user->getIsGuest()) {
                    return true;
                }
                // Check if the user is logged in, and the roles match
            }
            /*elseif (!$user->getIsGuest() && $role == $user->identity->role) {
                return true;
            }*/
        }

        return false;
    }
}