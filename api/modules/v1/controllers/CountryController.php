<?php

namespace api\modules\v1\controllers;

use common\models\User;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Country';
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        /*$behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];*/
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            /*'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],*/
            'auth' => [$this, 'auth']
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function Auth($username, $password) {
        // username, password are mandatory fields
        if(empty($username) || empty($password))
            return null;

        // get user using requested email
        $user = User::findOne([
            'username' => $username,
        ]);

        // if no record matching the requested user
        if(empty($user))
            return null;

        // hashed password from user record
        // $this->user_password = $user->password_hash;

        // validate password
        $isPass = $user->validatePassword($password);

        // if password validation fails
        if(!$isPass)
            return null;

        // if user validates (both user_email, user_password are valid)
        return $user;
    }

    public function actionTest(){
        return json_encode(["user"=>"user1"]);
    }
}


