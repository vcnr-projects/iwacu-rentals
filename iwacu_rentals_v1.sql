-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2020 at 07:51 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iwacu_rentals_v1`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `url_replace` (`url` VARCHAR(1024)) RETURNS VARCHAR(1024) CHARSET latin1 BEGIN RETURN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( url,'%','%25'),'\'','%27'),'|','%7C'),'|','%7c'),'&','%26'),'^','%5E'),'^','%5e'),'-','%2D'),'-','%2d'),'+','%2B'),'+','%2b'),' ','%20');

    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `accessibility_type`
--

CREATE TABLE `accessibility_type` (
  `accessibility_type` varchar(100) NOT NULL COMMENT 'Accessibility Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accessibility_type`
--

INSERT INTO `accessibility_type` (`accessibility_type`, `is_active`) VALUES
('Bus Station', 1),
('Metro', 1);

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `adrs_line1` text NOT NULL COMMENT 'Address Line 1',
  `adrs_line2` text COMMENT 'Address Line 2',
  `city` varchar(200) NOT NULL COMMENT 'City',
  `state` varchar(200) NOT NULL COMMENT 'State',
  `country` varchar(100) NOT NULL COMMENT 'Country',
  `postal_code` varchar(100) NOT NULL COMMENT 'Postal Code',
  `telephone_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Telephone ',
  `proof_doc_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `adrs_line1`, `adrs_line2`, `city`, `state`, `country`, `postal_code`, `telephone_id`, `proof_doc_type`) VALUES
(377, 'Rajajinagar', '', 'Bengaluru', 'Karnataka', 'India', '560012', 244, ''),
(378, 'Rajajinagar', '', 'Bengaluru', 'Karnataka', 'India', '560012', 245, ''),
(379, 'No.2-C Sahajeevan Apartments, 219, Rajmahal\r\nVilas Extension', '', 'Bangalore', 'karnataka', 'India', '560001', 249, ''),
(380, 'No.2-C Sahajeevan Apartments, 219, Rajmahal\r\n\r\nVilas Extension', '', 'Bangalore', 'karnataka', 'India', '560001', 250, ''),
(381, '29th & 30th Floors, World Trade Center\r\n\r\nBrigade Gateway Campus, 26/1, Dr Rajkumar Road \r\n\r\nMalleswaram-Rajajinagar', '', 'Bangalore', 'karnataka', 'India', '56055', 251, ''),
(382, '135/2m, Brigade Tower, Brigade Road\r\n\r\nBrigade Road', '', 'Bangalore', 'Karnataka', 'India', '560025', 252, ''),
(383, '135/2m, Brigade Tower, Brigade Road\r\n\r\nBrigade Road', NULL, 'Bangalore', 'Karnataka', 'India', '560025', NULL, ''),
(384, 'No. 31, Ramakrishna Math Road Mylapore', '', 'Mylapore', 'Chennai', 'India', '600004', 253, ''),
(385, 'No. 31, Ramakrishna Math Road Mylapore', '', 'Mylapore', 'Chennai', 'India', '600004', 254, ''),
(386, 'No. 715, 8A Main Road, 1st Cross,  Basavanagudi,', '', 'Bangalore', 'Karnataka', 'India', '56004', 255, ''),
(390, 'No. 120, 1st Main Road,  5th Cross, Mathikere ', '', 'Bangalore', 'Karnataka', 'India', '560091', 265, ''),
(391, 'No. 120, 1st Main Road,  5th Cross, Mathikere ', '', 'Bangalore', 'Karnataka', 'India', '560091', 266, ''),
(397, 'No.30, 2nd Main Road, 3rd Cross, R T Nagar ', '', 'India', 'Karnataka', 'India', '560032', 274, ''),
(398, 'No.30, 2nd Main Road, 3rd Cross, R T Nagar ', '', 'India', 'Karnataka', 'India', '560032', 275, ''),
(402, 'No. 14, Muni Reddy palya', '', 'Bangalore', 'Karnataka', 'India', '560036', 278, ''),
(403, 'Soladevanahalli, Chikkabanavara post, Hesaraghatta main road', '', 'Bangalore', 'Karnataka', 'India', '560107', 279, ''),
(404, 'Soladevanahalli, Chikkabanavara post, Hesaraghatta main road', '', 'Bangalore', 'Karnataka', 'India', '560107', 280, ''),
(405, 'Sanjaynagar', '', 'Bangalore', 'Karnataka', 'India', '560094', 281, ''),
(416, '#10, 2nd Main Road, 3rd Cross, Ganganagar ', '', 'Bangalore', 'Karnataka', 'India', '560036', 295, ''),
(417, '#10, 2nd Main Road, 3rd Cross, Ganganagar ', '', 'Bangalore', 'Karnataka', 'India', '560036', 296, ''),
(418, '#140, 5th Cross, 8th Main Road, Vasanthnagar', '', 'Bangalore', 'Karnataka', 'India', '560001', 297, ''),
(419, '#10, 2nd Main Road, 3rd Cross, RT Nagar Post', '', 'Bangalore ', 'Karnataka', 'India', '560032', 298, ''),
(420, '#120, 1st Main Road, 6th Cross, Sanjaynagar', '', 'Bangalore', 'Karnataka', 'India', '560094', 299, ''),
(421, '#14/A, 3rd Main Road, 4th Cross, Ganganagar', '', 'Bangalore', 'Karnataka', 'India', '560032', 300, ''),
(422, '#14/A, 3rd Main Road, 4th Cross, Ganganagar', NULL, 'Bangalore', 'Karnataka', 'India', '560032', NULL, ''),
(426, '#9, 1st Main Road, GM Temple Street, Matadahalli, RT Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560032', 304, ''),
(427, '#9, 1st Main Road, GM Temple Street, Matadahalli, RT Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560032', 305, 'DL'),
(428, '#4, 5th Main, 6th Cross, KEB layout, Geddahalli, Sanajaynagar', '', 'Bangalore', 'Karnataka', 'India', '560094', 306, ''),
(429, '#9, 1st Main Road, GM Temple Street, Matadahalli, RT Nagar Post ', '', 'Bangalore', 'Karnataka', 'India', '560032', 307, ''),
(430, '120, EPIP Zone, Whitefield', '', 'Bangalore', 'Karnataka', 'India', '560012', 308, ''),
(431, '#70, 3rd Cross, 3th Main 5th Block, Jayanagar', '', 'Bangalore', 'Karanakta', 'India', '560098', 309, ''),
(432, '#70, 3rd Cross, 3th Main 5th Block, Jayanagar', NULL, 'Bangalore', 'Karanakta', 'India', '560098', NULL, ''),
(436, 'Soladevanahalli, Chikkabanavara post, Hesaragatta main road', '', 'Bangalore', 'Karnataka', 'India', '560107', 313, 'Aadhar Card'),
(437, 'Soladevanahalli, Chikkabanavara post, Hesaragatta main road', '', 'Bangalore', 'Karnataka', 'India', '560107', 314, 'Aadhar Card'),
(438, 'Geddalahalli, Sanjaynagar', '', 'Bangalore', 'Karnataka', 'India', '560039', 315, ''),
(440, 'Tarabanahalli, Chikkabanavara post, Hesaraghatta main road', '', 'Bangalore', 'Karnataka', 'India', '560090', 318, ''),
(441, 'Yeshwanthpur', '', 'Bangalore', 'Karnataka', 'India', '560045', 319, ''),
(442, '# 15, 8th Main, 7th Cross, Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560015', 320, ''),
(443, '# 15, 8th Main, 7th Cross, Jayanagar', NULL, 'Bangalore', 'Karnataka', 'India', '560015', NULL, ''),
(446, '#15, 2nd Cross, 2nd Main, Matadahalli, RT nagar', '', 'Bangalore', 'Karnataka', 'India', '560032', 324, 'DL'),
(447, '#15, 2nd Cross, 2nd Main, Matadahalli, RT nagar', '', 'Bangalore', 'Karnataka', 'India', '560032', 325, 'DL'),
(448, '#80, 1st Floor, 1st Main, 3rd Cross, 2nd Stage ', '', 'Bangalore', 'Karnataka', 'India', '560022', 326, ''),
(449, '#15, 2nd Main, 2nd Cross, Matadahalli, RT Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560032', 327, ''),
(450, '# 18, 1st Main Road, RT Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560032', 328, ''),
(451, '#19, 2nd Main, 4th Block Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560032', 329, ''),
(452, '#19, 2nd Main, 4th Block Jayanagar', NULL, 'Bangalore', 'Karnataka', 'India', '560032', NULL, ''),
(462, 'test', '', 'test', 'ste', 'test', '56226', 337, ''),
(463, 'teste', '', 'Bangalore', 'Karnataka', 'India', '560036', 338, ''),
(464, '#43 ', '', 'Bangalore', 'Karnataka', 'India', '560023', NULL, ''),
(465, 'Sy No 12/3, Beside Veeranjaneya Swamy temple,\r\n\r\nWhitefield Main Road, Bangalore 5600066\r\n', '', 'Bangalore', 'Karnataka', 'India', '5600066', NULL, ''),
(466, '3rd Cross, Narayanappa Badawane, Behind High School, Varthur\r\n', '', 'Bangalore', 'Karnataka', 'India', '560087', 339, ''),
(468, '#45, google street', '', 'Bangalore', 'Karnataka', 'India', '560012', NULL, ''),
(469, 'No.48 Istar Building, 2nd floor, 100ft road, 4th Block, Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560034', 341, ''),
(470, '3rd Cross, Narayanappa Badawane, Behind High School, Varthur\r\n', '', 'Bangalore', 'Karnataka', 'India', '560087', 342, ''),
(472, 'Above Philps Showroom, 2nd Floor, 296/D 40-1, 38th Cross, 9th Main, 5th Block, Jayanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560041', 344, ''),
(473, '92, Ground Floor, Karthik Nagar, Marathahalli, Outer Ring Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560037', 345, ''),
(474, 'No. 314(old no.29), 33rd Cross, 17th main, 4th \'T\' Block, Jayanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560011', 346, ''),
(475, '# 65/2, 1st Floor, Railway Parallel Road, Kumara Park West\r\n', '', 'Bangalore', 'Karnataka', 'India', '560020', 347, ''),
(476, 'Aakruti Amity, Ananth Nagar, Phase II, Huskur Gate, Hosur Main Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560100', 348, ''),
(477, '#4, 9th Main, 13th Cross, HSR Layout, 6th Sector\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 349, ''),
(478, '#785/10, 2nd Floor, 14th Cross, Jayanagar 7th block, KR Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560082', 350, ''),
(479, 'Regd Off No. 848, 18 Main, No. 37, F Cross, 4th Block, Jayanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560041', 351, ''),
(480, '10, Vittal Mallya Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 352, ''),
(481, '#115, 2ndFloor, 10thMain, 14th Cross,Sector-6,  HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 353, ''),
(482, 'Flat 102, 1st Floor, Anand Enclave, LBS Nagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560017', 354, ''),
(483, 'Sy. No.12, Hagadur Village, K.R.Puram Hobli, White field, Bengaluru East Taluk\r\n', '', 'Bangalore', 'Karnataka', 'India', '560084', 355, ''),
(484, '#67, 11th Main, 17th Cross, Sector 6 HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 356, ''),
(485, '# 325, Sobha Complex, 3rd Cross, Vinayaka, Temple Street, Kammanahalli\r\n', '', 'Bangalore', 'Karnataka', 'India', '560084', 357, ''),
(486, '# 2823, 1st Floor, HAL 2nd Stage, 80 Ft Main Road, Indira Nagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560008', 358, ''),
(487, '# 842, 7th Main, 2nd Cross, H.A.L 2nd Stage, Indiranagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 359, ''),
(488, '#1282, 3rd Floor, 17th cross	, 5th main, 7th Sector, HSR layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 360, ''),
(489, 'MSB 111, Ground Floor, KSCMF, Building Opp., Prestige Feroze, No. 8, Cunningham Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560052', 361, ''),
(490, 'Ajmera Summit\" 3/D, 4th Floor, 7th C Main, 3rd Cross, 3rd Block, Koramangala\r\n', '', 'Bangalore`', 'Karnataka', 'India', '560034', 362, ''),
(491, '17F, 18th Cross, Sector - 3, H.S.R Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 363, ''),
(492, '#44/3, First Floor, Sharadamba Complex, 60 Feet Road, Sahakaranagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560092', 364, ''),
(493, 'Vaibhav Residency, Flat No. 201, Plot No. 2, 16 A Cross, Neeladri Nagar, Phase I, Electronic City\r\n', '', 'Bangalore', 'Karnataka', 'India', '560100', 365, ''),
(494, '#4, 9th Main, 13th Cross, 6th Sector, HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 366, ''),
(495, '# 37/10, Ishwarya Towers, Maenee Avenue Tank Road, Opp., Lake Side Hospital, Ulsoor\r\n', '', 'Bangalore', 'Karnataka', 'India', '560042', 367, ''),
(496, 'No.71 , 3rd Block , 4th Cross, Devasandra main road ,K.R.Puram\r\n', '', 'Bangalore', 'Karnataka', 'India', '560036', 368, ''),
(497, '#296/4,13st C Cross, Kormangala 1St Block, Near Asia Pacific School\r\n', '', 'Bangalore', 'Karnataka', 'India', '560034', 369, ''),
(498, 'Apranje Square, 16, Rest House Crescent Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 370, ''),
(499, 'No.14, 1st Cross, 1st Main,  1st Block, BHCS Layout, Banagiri Nagar, BSK 3rd Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560085', 371, ''),
(500, '402 A, 4th Floor, The Estate, Dickenson Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560042', 372, ''),
(501, 'Corporate Office Artha, 1st Floor, #110/37	Solitaire Building, Outer Ring road, Marathahalli\r\n', '', 'Bangalore', 'Karnataka', 'India', '560037', 373, ''),
(502, 'Survey Number: 3/2, Naryana Ghatta Village, Muthanallur Post, Anekal Taluk\r\n', '', 'Bangalore', 'Karnataka', 'India', '560099', 374, ''),
(503, '# 617, 15th Cross, 100 feet ring road, J. P. Nagar 6th Phase\r\n', '', 'Bangalore', 'Karnataka', 'India', '560078', 375, ''),
(504, '#2633, Foodsdays, 4th Floor,27th Main, 13th Cross, 1st Sector, HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 376, ''),
(505, '411, 4th cross, 6th Block, Jayanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560070', 377, ''),
(506, '#1057/49, Sy No : 2/5, Hagadur Village, Immidahalli Main Road, Whitefield\r\n', '', 'Bangalore', 'Karnataka', 'India', '560065', 378, ''),
(507, '#476, 2nd Floor, 80 Feet Road, 6th Block Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560095', 379, ''),
(508, '#2/1 Embassy Icon Annexe, Infantry Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 380, ''),
(509, '# 911, Shri Kshetra Complex, 28th Main, 39th Cross, Above Karnataka Bank, Jayanagar 9th Block\r\n', '', 'Bangalore', 'Karnataka', 'India', '560069', 381, ''),
(510, '12/1, Plain Street, Infantry Road Cross\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 382, ''),
(511, 'No. 17 / 1, Campbell Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560047', 383, ''),
(512, '#5, Victoria Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560047', 384, ''),
(513, 'No.571, 1st Floor, 1st Cross, 8th Block, Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560095', NULL, ''),
(514, '#No.1007, 18th Cross, 8th Main Road, HSR Layout, 7th sector, Near Indian overseas bank\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 385, ''),
(515, '4th Floor, SBI Building, Centre Point, 26-A	Electronic City, Hosur Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560101', NULL, ''),
(516, '#95, 21st Main, Opp to KIMS (pharmaceutical), Banashankari 2nd stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560070', 386, ''),
(517, '#31, 3rd floor, 21st Main, BSK 2nd stage (Opp to BDA Complex)\r\n', '', 'Bangalore', 'Karnataka', 'India', '560070', 387, ''),
(518, 'No 1, Srihari Towers, 5th \"A\" Main Road, Next to Baptist Hospital, Hebbal\r\n', '', 'Bangalore', 'Karnataka', 'India', '560024', 388, ''),
(519, 'Near Hebbal, Thanisandra Main Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560064', 389, ''),
(520, '#9-10, 3rd Floor, Sir Puttanna Chetty Complex,  Bull Temple Road, Basavanagudi\r\n', '', 'Bangalore', 'Karnataka', 'India', '560064', 390, ''),
(521, 'Bijith Classic, Flat No. 109,K. Narayana Pura Road, Next to Kristhu Jayanthi College, Kothanur P. O\r\n', '', 'Bangalore', 'Karnataka', 'India', '560077', 391, ''),
(522, '#11/2, K.R. Road, Beside Post Office, Basavanagudi\r\n', '', 'Bangalore', 'Karnataka', 'India', '560004', 392, ''),
(523, '#41, Above ICICI Bank, Jayamahal extn., Nandi Durga Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560046', 393, ''),
(524, '#61, Balavana, 5th \'A\' Block, Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560095', 394, ''),
(525, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560055', 395, ''),
(526, 'No. 1157, 5th floor, 20th cross, 5th main, 7th Sector, HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 396, ''),
(527, '#100/37/5, Near Silk Board, Rupena Agrahara, Hosur Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560068', 397, ''),
(528, 'No.5C-501, ?George Court? 3rd Floor, 2nd Block, HRBR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 398, ''),
(529, '#5A, ABI Deepi Nest, Devatha Layout, Hennur Cross\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 399, ''),
(530, '#3131,6th \'C\' Main, Indiranagar, HAL II Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560008', 400, ''),
(531, '#810, 27th Main Road, HSR Layout, Sector 1\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 401, ''),
(532, 'Century Sales Galleria, Opposite Sahakaranagar Cross, Bellary Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560092', 402, ''),
(533, 'No.205 1st Floor, JSA Towers, Double Road, Indiranagar 2nd Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 403, ''),
(534, '#26, Chamundi Mansion, 22 Main Road, JP Nagar II Phase\r\n', '', 'Bangalore', 'Karnataka', 'India', '560078', 404, ''),
(535, 'No.1238, 7th Cross, \"B Sector\", New Town Yelahanka\r\n', '', 'Bangalore', 'Karnataka', 'India', '560106', 405, ''),
(536, '27, Victoria Road	\r\n', '', 'Bangalore', 'Karnataka', 'India', '560047', 406, ''),
(537, 'No,849,12th Cross,Opp.Ashraya, Near BDA Complex Indiranagar, 1st Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 407, ''),
(538, 'No. 1/A, Ulsoor Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560042', 408, ''),
(539, 'No. 600, 1st Floor, Opp. M.K. Retail Shop, C Block, AECS Layout Main Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560037', 409, ''),
(540, '3rd floor, Belaku Complex, Singasandra, Hosur Main Rd\r\n', '', 'Bangalore', 'karnataka', 'India', '560068', 410, ''),
(541, '#2, North Park Road, Kumara Park East\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 411, ''),
(542, 'Shakthi Comfort Towers, #102, 5th Floor, K H Road, Above IDBI Bank\r\n', '', 'Bangalore', 'Karnataka', 'India', '560027', 412, ''),
(543, 'No. 583,9th Main Road, Off CMH Road, Indiranagar 1st Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 413, ''),
(544, 'Flat No. 202, B Block, KKR Ruby Apartment, No:23, H M T Main Road, Jalahalli\r\n', '', 'Bangalore', 'Karnataka', 'India', '560013', 414, ''),
(545, 'First Floor, Achaiah Chetty Arcade, No:19, 1st cross, Achaiah Chetty Layout , RMV Extension, Sadashiv Nagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560080', 415, ''),
(546, '#120/B, EPIP ZONE,Whitefield, Opp. Inorbit Mall, Marriott Hotel\r\n', '', 'Bangalore', 'karnataka', 'India', '560066', 416, ''),
(547, 'DivyaSree Chambers, ?A? Wing, #11, O?Shaugnessy Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560025', 417, ''),
(548, 'No. 304, ?A? Wing, Queen?s Corner, # 3, Queens Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 418, ''),
(549, ' 35/1, 7th Cross, Vasanthnagar', '', 'Bangalore', 'Karnataka', 'India', '560052', 419, ''),
(550, '#1854, 17th Main, 30th B Cross, 5th Block, HBR Layout, Near to Ring Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 420, ''),
(551, 'DSR Diya Arcade, 1st Floor, No.220, 9th Main, Next To Maxwell Public School, HRBR Layout 1st Block Extension, Kalyan Nagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 421, ''),
(552, 'Service Road	Maheshwari Nagar, Yelahanka', '', 'Bangalore', 'Karnataka', 'India', '560064', 422, ''),
(553, 'Site Office: #76, Global Citizen, Doddagubbi Road, Hennur\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 423, ''),
(554, '1st Floor, 150, Infantry Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560001', 424, ''),
(555, 'No.24, 1st Floor, Yeshodha Nagara, Opposite to Jakkur Aerodrum, Jakkur Post, Bellary Road (International Airport Road), Yelahanka\r\n', '', 'Bangalore ', 'Karnataka', 'India', '560064', 425, ''),
(556, '#443, 16th Cross, 5th Main, HSR Layout, 6th Sector\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 426, ''),
(557, '#285, Rama Iyengar Road, V. V. Puram, Basavanagudi\r\n', '', 'Bangalore', 'Karnataka', 'India', '560004', 427, ''),
(558, '#86/1, Mariyappa Layout, Panathur', '', 'Bangalore', 'Karnataka', 'India', '560103', 428, ''),
(559, '#3156, 12th Main, H.A.L 2nd Stage, Indiranagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560008', 429, ''),
(560, 'S-8, Silver Spring Layout, Munnekolala', '', 'Bangalore', 'Karnataka', 'India', '560037', 430, ''),
(561, 'No. 161/A, 7th Cross,1st Stage, Teachers Colony, Kumaraswamy Layout\r\n', '', 'Bangalore', 'karnataka', 'India', '560078', 431, ''),
(562, '# 991, 2nd Floor, KVS Complex, HRBR Layout, Banasawadi Outer Ring Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 432, ''),
(563, '#48/6, Kudlu, Sarjapur Hobli, Anekal Taluk \r\n', '', 'Bangalore', 'Karnataka', 'India', '560068', 433, ''),
(564, '#330, Double Road, Next to Bank of Baroda, 1st Stage, Indiranagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 434, ''),
(565, 'Dusit Towers, 2nd Floor, Plot No 14, 100 Feet Ring Road, BTM Layout 1st Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560058', 435, ''),
(566, 'GM Pearl, No.06 BTM Layout, Ring Road, 1st Stage, 1st Phase\r\n', '', 'Bangalore', 'Karnataka', 'India', '560068', 436, ''),
(567, '#5C, 5th Floor	, Godrej One, Pirojshanagar,  Vikhroli East\r\n', '', 'Vikhorli East', 'Mumbai', 'India', '40009', 437, ''),
(568, '#387, 13th Cross, Aster Chambers, Upper Palace Orchards, Sadashivnagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560080', 438, ''),
(569, 'No. 5, Richmond Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560025', 439, ''),
(570, '#007, Suranjandas Road, Gitanjali Layout,  New Thippasandra\r\n', '', 'Bangalore', 'Karnataka', 'India', '560075', 440, ''),
(571, 'Villas No. 3, Lakeside Heights, Ramagondanahalli,Whitefield\r\n', '', 'Bangalore', 'Karnataka', 'India', '560066', 441, ''),
(572, '#82, K V layout, 30th Cross	#82, K V layout, 30th Cross,  4th block east, Jayanagar', '', 'Bangalore', 'karnataka', 'India', '560011', 442, ''),
(573, 'Greens, #10, 9th Cross, Kagadasapura Main Road, C.V.Raman Nagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560093', 443, ''),
(574, '2nd Floor, S.M. Towers, Sr No. 34, Bellandur Outer Ring Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560103', 444, ''),
(575, '#26, Shankarmutt Road, Basavangudi', '', 'Bangalore', 'Karnataka', 'India', '560004', 445, ''),
(576, '#5AC-712, 4th Floor, 5th A Cross	HRBR Layout, 1st Block, Kalyan Nagar\r\n', '', 'Bangalore', 'Karnataka ', 'India', '560043', 446, ''),
(577, 'No.168, 2nd Floor, 1st Main, Seshadripuram\r\n', '', 'Bangalore', 'karnataka', 'India', '560020', 447, ''),
(578, '757/B, 100 Feet Road, HAL 2nd Stage, Indiranagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560038', 448, ''),
(579, '#104, Ground Floor, Infantry Techno Park,  Opp. to Gem Plaza, Infantry Road\r\n', '', 'Bangalore', 'karnataka', 'India', '560001', 449, ''),
(580, 'HRC Ventures , 207, Mota Chambers	# 9, Cunningham Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560052', 450, ''),
(581, '#47/9, Jai Plaza Symphony, 9th main,  Sector-6, HSR Layout\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 451, ''),
(582, 'L-191, Shop No. 2, HSR Layout, 6th Sector, Near Silk Board Bus Stop\r\n', '', 'Bangalore', 'Karnataka', 'India', '560102', 452, ''),
(583, '02 Leafy Blocks, Owners Court West, Hosa Road \r\n', '', 'Bangalore', 'Karnataka', 'India', '560035', 453, ''),
(584, '#2, S.B complex, M.S.R, Main Road,Mathikere\r\n', '', 'Bangalore', 'Karnataka', 'India', '560054', 454, ''),
(585, '#52, 2 Floor, 100 Ft Road, 4 Block, Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560034', 455, ''),
(586, '#2007 (Survey no 119), New Internationl Airport Road, Vidya nagar cross road, chikajala\r\n', '', 'Bangalore', 'Karnataka', 'India', '562157', 456, ''),
(587, '#16, 97/2, Varthur Village, Behind Indane Gas Godown \r\n', '', 'Bangalore', 'Karnataka', 'India', '560087', 457, ''),
(588, '#17, PWD Main Road, Akash Nagar, Near DRDO Phase II, B Narayanapura\r\n', '', 'Bangalore', 'Karnataka', 'India', '560016', 458, ''),
(589, '# 33/1, 1st Floor, Mallathahalli Main Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560056', 459, ''),
(590, 'Solus Floor 11, No. 2, 1st Cross, J C Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560027', 460, ''),
(591, 'M.S. Square, 34/1-1, Langford Road, Shanthinagar', '', 'Bangalore', 'Karnataka', 'India', '560027', 461, ''),
(592, 'JJ Residency, # 372/90, 18th Cross, 15th main, Vijayanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560040', 462, ''),
(593, 'Sy. No. 21, Belthur Village, Near Kadugodi, Whitefields\r\n', '', 'Bangalore', 'Karnataka', 'India', '560067', 463, ''),
(594, '# 71, 2nd Floor, Outer Ring Road, Marathahalli\r\n', '', 'Bangalore', 'Karnataka', 'India', '560037', 464, ''),
(595, '#34, E-Block Extn, Behind Swathi Restaurant, Sahakarnagar \r\n', '', 'Bangalore', 'karnataka', 'India', '560092', 465, ''),
(596, '# 17/46, 1st Floor, Service Road, Remco Layout, Vijayanagar 1st Phase\r\n', '', 'Bangalore', 'Karnataka', 'India', '560040', 466, ''),
(597, '#S-711, 7th Floor, South Block, Manipal Centre, Dickenson Road, \r\n', '', 'Bangalore', 'Karnataka', 'India', '560042', 467, ''),
(598, '151, Industrial Suburb, Yeshwanthpur, Opp Metro Cash and Carry \r\n', '', 'Bangalore', 'Karnataka', 'India', '560022', 468, ''),
(599, '#16, KMC Arcade, 3rd Floor, B Block,  100 Feet Inner Ring Road, Ejipura, Koramangala\r\n', '', 'Bangalore', 'Karnataka', 'India', '560047', 469, ''),
(600, '#141, Sree Shanthi Tower, Ground Floor, 3rd Main Road, Kasturi Nagar Bus Stop, Outer Ring Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560043', 470, ''),
(601, '# S-1718, ?Swarup Bhavan?, Dr.Rajkumar Rd, 2nd Stage, Opp:Navrang Theatre, Rajajinagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560010', 471, ''),
(602, 'No 232, Rajnigandha, Vittal Mallya Road\r\n', '', 'Bangalore', 'Karnataka', 'India', '560094', 472, ''),
(603, 'No. 1788/C, 9th Cross, 5th Main, RPC Layout, Vijayanagar 2nd Stage\r\n', '', 'Bangalore', 'Karnataka', 'India', '560040', 473, ''),
(604, 'Aishwarya Regency Building, 11th \'B\' Cross, 30th Main, J.P. Nagar 1st Phase\r\n', '', 'Bangalore', 'Karnataka', 'India', '560078', 474, ''),
(605, 'No.23 Sankey Square,Sankey Road, Lower Palace Orchards, Sadashivanagar\r\n', '', 'Bangalore', 'Karnataka', 'India', '560003', 475, ''),
(606, 'Kumar Properties, 33, Crescent Road,High Grounds, Opp Taj West End Hotel', '', 'Bangalore', 'Karnataka', 'India', '560025', 476, ''),
(607, '#1197, 1st Floor, 22nd Cross, 24th Main, Parangipalya, HRS layout, Sector 2', '', 'Bangalore', 'Karnataka', 'India', '560102', 477, ''),
(608, '#9, 14th cross, 14th main, 4th sector, HSR layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 478, ''),
(613, '333, Nova Miller, Thimmaih Road', '', 'Bangalore', 'Karnataka', 'India', '560052', 483, ''),
(614, '#67, 11th Main, 17th Cross, Sector 6 HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 484, ''),
(615, '#53, Doddakammanahalli,', '', 'Bangalore', 'Karnataka', 'India', '560083', 485, ''),
(616, 'Kedia Arcase #92, Infantry Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 486, ''),
(617, '074/G, 11th Main, HAL 2nd Stage, Indiranagar', '', 'Bangalore', 'Karnataka', 'India', '560038', 487, ''),
(618, '#95,1st Floor,4th Cross, Pai Layout,Hulimavu Main Road, Bannerghatta Road', '', 'Bangalore', 'Karnataka', 'India', '560076', 488, ''),
(619, '51,17th Cross, 12th Main, 6th Sector, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 489, ''),
(620, '#37 /?38, 1st Floor, SBI Colony, 80 Feet Road, Koramangala 3rd Block', '', 'Bangalore', 'Karnataka', 'India', '560034', 490, ''),
(621, '#906, Raj Arcade, 4th Floor, 5th A Cross, H.R.B.R 1st Block, Near BWSSB Water Tank, Kalyan Nagar, Outer Ring Road', '', 'Bangalore', 'Karnataka', 'India', '560043', 491, ''),
(622, 'No. 1090/I, 18th Cross, Sector ? 3, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 492, ''),
(623, 'Mantri House, #41, Vittal Mallya Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 493, ''),
(624, 'Sy. No. 213/3, Veeraswamy Reddy Layout, Kadugodi, Near Whitefield Global School, Channasandra Road', '', 'Bangalore', 'Karnataka', 'India', '560067', 494, ''),
(625, '#50 HMT Layout, Ist floor, 7th Cross, R.T. Nagar, Behind Aircel showroom', '', 'Bangalore', 'Karnataka', 'India', '560032', 495, ''),
(626, 'No. B2 , 1st Floor, BTM 1st stage, B.G. Road', '', 'Bangalore', 'Karnataka', 'India', '560029', 496, ''),
(627, 'No.94/3, Kammasandra Hebbagodi, Electronic City Post', '', 'Bangalore', 'Karnataka', 'India', '560100', 497, ''),
(628, 'No. 1569, Outer Ring Road, Opp Agara Lake', '', 'Bangalore', 'Karnataka', 'India', '560102', 498, ''),
(629, 'Beside Adarsh Palm Meadows,Opp.Skylark Greens, Ramagondanahalli Borewell Road, Whitefield', '', 'Bangalore', 'Karnataka', 'India', '560066', 499, ''),
(630, 'MIMS Signature # 20, Coles Road', '', 'Bangalore', 'Karnataka', 'India', '560005', 500, ''),
(631, '# 54, 2nd Floor, 17th Cross,12th Main, Sector ? 6, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 501, ''),
(632, '#54, III Floor, Monarch Plaza, Brigade Road, Opp. Nilgiris', '', 'Bangalore', 'Karnataka', 'India', '560001', 502, ''),
(633, '811, 27th Main Rd, Sector 1, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 503, ''),
(634, '#2, MSK Square, 2nd Floor, 13th Cross, Attimabbe Road, C.T. Bed Extn, BSK II Stage', '', 'Bangalore', 'Karnataka', 'India', '560070', 504, ''),
(635, '# 595 4th Floor, 7th Main, Above State Bank Of Travancore, Mico Layout, BTM 2nd Stage', '', 'Bangalore', 'Karnataka', 'India', '560076', 505, ''),
(636, '#83, Ist floor G.P. Plaza, Outer Ring Road, Anand Nagar, Near Kalamandir, above Karnataka Bank, Marathahalli', '', 'Bangalore', 'Karnataka', 'India', '560037', 506, ''),
(637, 'Muncipal no 50, (Old no 23), 5th Cross, Wilson Garden', '', 'Bangalore', 'Karnataka', 'India', '560027', 507, ''),
(638, 'Address: 46, 36th Main, BTM Dollar Scheme', '', 'Bangalore', 'Karnataka', 'India', '560068', 508, ''),
(639, '# 14/101, 19th Main, 1st ?N? block, Rajajinagar Opp, Vidyavardhaka Sangha', '', 'Bangalore', 'Karnataka', 'India', '560010', 509, ''),
(640, '#1861,1st Floor, East End Main Road, South End ?C? Cross, Near I.O.B, 9th Block East, Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560069', 510, ''),
(641, '1697/36, 1st floor, \"GOLDEN HOUSE\", Near ICICI Bank, Dr. Rajkumar Road, Rajajinagar', '', 'Bangalore', 'karnataka', 'India', '560021', 511, ''),
(642, '#11, A. S Complex, 1st Floor, Somashekarappa Layout, 8th Main, Basaveshwaranagar', '', 'Bangalore', 'Karnataka India', 'India', '560079', 512, ''),
(643, '#71,16th Cross 14th \"A\" Main Road, H.S.R Layout , Sector-4 Opp BDA Complex', '', 'Bangalore', 'Karnataka', 'India', '560102', 513, ''),
(644, '3/19,4th Cross, Kalpataru, Abhayadhama Road, Whitefield, Near Hope Farm', '', 'Bangalore', 'Karnataka', 'India', '560066', 514, ''),
(645, '# 152 & 153, Marathalli Outer ring road, Opposite More Mega Store, Mahadevapura', '', 'Bangalore', 'Karnataka', 'India', '560037', 515, ''),
(646, 'No. 219 50/3, 3rd Main, 6th Block, Jayanagar', '', 'Bangalore', 'karnatka', 'India', '560082', 516, ''),
(647, 'Level 7, Nitesh Timesquare, #8 MG Road ', '', 'Bangalore', 'Karnataka', 'India', '560001', 517, ''),
(648, 'Obel Towers, site No.42, Survey No. 96/3, Adj to Vincent Pollotti School, Babusapalya Kalyan Nagar', '', 'Bangalore', 'Karnataka', 'India', '560043', 518, ''),
(649, '#297, 1st Floor, 1st Cross, 7th Main, BTM Layout 2nd Stage, Bangalore - 560 076. INDIA\r\n', '', 'Bangalore', 'Karnataka', 'India', '560076', 519, ''),
(650, '# 56, 10th Cross, 2nd Main WOC Road, Mahalakshmipuram ', '', 'Bangalore', 'Karnataka', 'India', '560086', 520, ''),
(651, '38, Ulsoor Road', '', 'Bangalore', 'karnataka', 'India', '560042', 521, ''),
(652, '#815, 3rd Floor, 27th Main,Sector-1, HSR Layout ', '', 'Bangalore', 'Karnatak', 'India', '560102', 522, ''),
(653, '#150 Second Floor, 8th Main, Vivek Nagar', '', 'Bangalore', 'Karnataka', 'India', '560047', 523, ''),
(654, '2nd Floor, Doddamane #19/1,Vittal Mallya Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 524, ''),
(655, 'Pavani Royal, First cross, Green garden layout, Munnekolela, Kundanahalli gate', '', 'Bangalore', 'Karnataka', 'India', '560037', 525, ''),
(656, '#1783,2nd &3rd Floor,19th main 1st sector H.S.R layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 526, ''),
(657, '#364, 3rd floor,35th cross, 16th Main, 4th \'T\' block, Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560041', 527, ''),
(658, ' Prabhavathi Castle, NO 13, 2nd Cross, N.S.Palya, Industrial Area, 65, BTM 2nd Stage', '', 'Bangalore', 'Karnataka', 'India', '560076', 528, ''),
(659, 'Sy.No. 92/3, Railway Staion Road, Beside Croma Building Road, Panathur', '', 'Bangalore', 'Karnataka', 'India', '560103', 529, ''),
(660, '#1236, 25th A Main, Sector -2, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', NULL, ''),
(661, '\"Riddhi Siddhi\", #11, 1st Cross, K G Extn, opp. Menaka theatre, off K G Road', '', 'Bangalore', 'Karnataka', 'India', '560009', 530, ''),
(662, 'The Falcon House, No. 1, Main Guard Cross Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 531, ''),
(663, 'Pride Hulkul, 901, 9th Floor, No.116 Lalbagh Road', '', 'Bangalore', 'Karnataka', 'India', '560027', 532, ''),
(664, 'No.14/1, 2nd Cross, Jothi Nagar, Gottigere, Bannergatta Road', '', 'Bangalore', 'Karnataka', 'India', '560083', 533, ''),
(665, '130/1, Ulsoor Road', '', 'Bangalore', 'Karnataka', 'India', '560042', 534, ''),
(666, '#1147, 6th Main, 18th Cross, HSR Layout, 7th Sector', '', 'Bangalore', 'karnataka', 'India', '560102', 535, ''),
(667, '#416, 18th Main, Jayanagar 4th block', '', 'Bangalore', 'Karnataka', 'India', '560041', 536, ''),
(668, 'No. 25, 3rd Cross, KHB Colony, International Airport Road, Gandhi Nagar, Yelahanka', '', 'Bangalore', 'Karnataka', 'India', '560064', 537, ''),
(669, '?Sathyams?, #48/49, 1st Floor, 9th Cross, Sarakki Main Road, Opp. Rajashekar Hospital, J.P. Nagar 1st Phase', '', 'Bangalore', 'Karnataka', 'India', '560078', 538, ''),
(670, '#6, 3rd Floor, Gajanana Towers, 11th main, Jayanagar 4th block', '', 'Bangalore', 'Karnataka', 'India', '560011', 539, ''),
(671, 'Raja Mahalakshmi, F-2, # 12, Basappa Road, Shantinagar', '', 'Bangalore', 'Karnataka', 'India', '560027', 540, ''),
(672, 'Site No. 25-60, 2nd Cross, Raghavendra nagar, Hennur Ring Road, Kalyan Nagar Post', '', 'Bangalore ', 'Karnataka', 'India', '560043', 541, ''),
(673, 'C 205, House Of Lords, St.Mark\'s Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 542, ''),
(674, '#50, Renaissance Landmark, 17th Cross, 8th Main, Malleshwaram', '', 'Bangalore', 'Karnataka', 'India', '560055', 543, ''),
(675, '2226/A,?2nd Floor, PNS Complex, 23rd Cross, 4th Main, K.R. Road, Banashankari II Stage', '', 'Bangalore', 'Karnataka', 'India', '560070', 544, ''),
(676, '21/6, Craig Park Layout, MG Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 545, ''),
(677, '# 284, Ground Floor, 7th Main, 5th Cross, Padmanabhanagar', '', 'Bangalore', 'Karnataka', 'India', '560070', 546, ''),
(678, 'No. 58/1, 2nd Floor, 80 Feet Road, Koramangala 7th Block', '', 'Bangalore', 'Karnataka', 'India', '560095', 547, ''),
(679, '527, Matthuga, 3rd Cross Maruti Layout, Vasanthapura', '', 'Bangalore', 'Karnataka', 'India', '560061', 548, ''),
(680, 'No. 130/131, Basapura Main Road, Near Hosa Road, Basapura', '', 'Bangalore', 'Karnataka', 'India', '560100', 549, ''),
(681, '#19, Surakshaa Fairview Vill, Belathur, Kadugodi P.O Whitefield', '', 'Bangalore', 'Karnataka', 'India', '560067', 550, ''),
(682, '#384,GF,7th Sector,9th Main, HSR layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 551, ''),
(683, '#40, 3rd Floor, 14th Main, 7th Sector, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', 552, ''),
(684, '4th Floor, Salarpuria Windsor, #3 Ulsoor Road', '', 'Bangalore', 'Karnataka', 'India', '560042', 553, ''),
(685, 'No. 21/15, 4th Floor, M.G. Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 554, ''),
(686, 'No.1616 (84), Above Whizz Color Lab, 8th Main Road, 3rd Block Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560011', 555, ''),
(687, '48,23rd main,Marenahalli, JP Nagar, 2nd Phase Behind Mayura Bakery', '', 'Bangalore', 'Karnataka', 'India', '560078', 556, ''),
(688, '# 446, Shivcharan Arcade, 8th Main, Opposite Seva Sadan Institute, 3rd Block, Koramangala', '', 'Bangalore', 'Karnataka', 'India', '560034', 557, ''),
(689, '#1, 2nd Floor, G P Complex, 1st Main, RMV 2nd Stage, New BEL Road', '', 'Bangalore', 'Karnataka', 'India', '560094', 558, ''),
(690, '#80/1, 80 Feet Road, Horamavu, Kalkeri Road', '', 'Bangalore', 'Karnataka', 'India', '560043', 559, ''),
(691, '410, 3rd Floor, Above Just Books, Beside Ramagondanahalli Bus Stop, Whitefield Main Road', '', 'Bangalore', 'Karnataka', 'India', '560066', 560, ''),
(692, 'Old No. 1097, New No. 58, 18th B Main, 5th Block, Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560010', 561, ''),
(693, '# 2254, I Floor, Kodigehalli Main Road, \'D\' Block, Sahakarnagar, Above Vijaya Bank', '', 'Bangalore', 'Karnataka', 'India', '560092', 562, ''),
(694, 'No 40/43, 8th Main, 4th Cross, RMV Extn., Sadhashiva Nagar', '', 'Bangalore', 'Karnataka', 'India', '560080', 563, ''),
(695, '# 824, 20th A Main, 8th cross, Koramangala', '', 'Bangalore', 'Karnataka', 'India', '560095', 564, ''),
(696, '#18, 2nd Cross, 8th Main,Vasanthanagar', '', 'Bangalore', 'Karnataka', 'India', '560052', 565, ''),
(697, '# 290, 6th Cross, Vijaya Bank Colony Main Road, Dodda Banasawadi', '', 'Bangalore', 'Karnataka', 'India', '560043', 566, ''),
(698, 'Skylark Chambers, No 37/21, Yellappachetty Layout, Ulsoor Road', '', 'Bangalore', 'Karnataka', 'India', '560042', 567, ''),
(699, 'No: 2, 5th Cross, Vijaya Bank Colony, Banasawadi', '', 'Bangalore', 'Karnataka', 'India', '560043', 568, ''),
(700, '1 - 4 Cauvery Nilaya, 3rd Main, Vinayakanagar, Bagalur Road, Yelahanka IAF Post, International Air Port Road', '', 'Bangalore', 'Karnataka', 'India', '560063', 569, ''),
(701, 'Sarjapur-Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 570, ''),
(702, 'No. 1, 4th Main, Seshadripuram, Nehru Circle', '', 'Bangalore', 'Karanataka', 'India', '560020', 571, ''),
(703, '750, 1st main road, C-block AECS Layout, Kundalahalli', '', 'Bangalore', 'Karnataka', 'India', '560037', 572, ''),
(704, '#1, Mahaveer Tower Near Nandini Hotel, 24th Main, J.P. Nagar 6th Phase', '', 'Bangalore', 'Karnataka', 'India', '56007 ', 573, ''),
(705, 'G-1, ground floor, Dwaraka apartments, 100ft road, Indiranagar, 1st stage', '', 'Bangalore', 'Karnataka', 'India', '560038', 574, ''),
(706, 'No 154, BenSathya Enclave, Hennur Bande Extension,Kalyan Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560043', 575, ''),
(707, '# 14/101, 19th Main, 1st ?N? block, Rajajinagar, Opp, Vidyavardhaka Sangha', '', 'Bangalore', 'Karnataka', 'India', '560010', 576, ''),
(708, 'Ramaraju Nilayam, No 106, 8th Main, 11th Cross, Malleswaram', '', 'Bangalore', 'Karnataka', 'India', '560003', 577, ''),
(711, 'No. 1, 4th Main, Seshadripuram, Nehru Circle', '', 'Bangalore', 'Karnataka', 'India', '560020', 580, ''),
(712, '402 Sri Krishna Apartments, Rustumbagh Main Road, HAL Airport Road', '', 'Bangalore', 'Karnataka', 'India', '560017', 581, ''),
(713, '# 401, Sri Emerald Park, Opp. VIMS Hospital Road, Beside Vaswani Whispering Palms, Marathalli', '', 'Bangalore', 'Karnataka', 'India', '560037', 582, ''),
(714, 'No 278, 4th Main, AECS lay out, B Block, Singsandra extn.', '', 'Bangalore', 'Karnataka', 'India', '560068', 583, ''),
(715, '#110/2, Harlur off Sarjapur Road', '', 'Bangalore', 'Karnataka', 'India', '560102', 584, ''),
(716, '#229,\"AKASHAYA PLAZA\" , 2nd Cross, Neeladri Nagar, Doddathogur, Electronic City', '', 'Bangalore', 'Karnataka', 'India', '560100', 585, ''),
(717, 'No.8, Cubbon Road, Opp. Income Tax Building', '', 'Bangalore', 'Karnataka', 'India', '560001', 586, ''),
(718, '135, 1st Floor, 3rd Cross, 27th Main, BTM, 1st Stage', '', 'Bangalore', 'Karnataka', 'India', '560068', 587, ''),
(719, 'Buildmann Devikrupa No 27 (New No. 32) 2nd floor, Sathyanarayana Temple Road Ulsoor', '', 'Bangalore', 'Karnataka', 'India', '560008', 588, ''),
(720, '43, CKB Plaza, 2nd Floor, Varthur Main Road, Marathahalli', '', 'Bangalore', 'Karnataka', 'India', '560037', 589, ''),
(721, 'No 134, 7th Main Road, 5th Block, Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560011', 590, ''),
(722, '#204, The Hibiscus, No.11, 1st Main Road, 1st Block Koramangala', '', 'Bangalore', 'Karnataka', 'India', '560034', 591, ''),
(723, 'AMR Plaza, 18th Main, 3rd Sector HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102 ', 592, ''),
(724, '#412, 9th Main, HRBR Layout, 1st Block, Kalyan Nagar', '', 'Bangalore', 'Karnataka', 'India', '560043', 593, ''),
(725, 'No: 159, TGR Corner, Bethel Nagar, Kodigehalli Main Road, K R Puram', '', 'Bangalore', 'Karnataka', 'India', '560036', 594, ''),
(726, ' # 910, 9th Main, 3rd Cross, Kalyan Nagar, 1st Block, HRBR Layout', '', 'Bangalore', 'Karnataka', 'India', '560043', 595, ''),
(727, '#460/20/1, 2nd Floor, 30th Cross, 8th B Main, 4th Block (Next to Jain Temple), Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560011', 596, ''),
(728, '#1215, 14th Main, 22nd Cross, 3rd Sector, Near BDA Complex, HSR Layout', '', 'Bangalore', 'Karnataka', 'India', '560102', NULL, ''),
(729, 'H.No.1-7-142/1, Fort House, Golkonda X Roads', '', 'Hyderabad', 'Arunachal Pradesh', 'India', ' 500020', 597, ''),
(730, 'No. 78, ITPL Main Road, EPIP Zone, Whitefield', '', 'Bangalore', 'Karnataka', 'India', '560066', 598, ''),
(731, '#18, TNR Arcade, Old Airport Road, Domlur', '', 'Bangalore', 'Karnataka', 'India', '560071', NULL, ''),
(732, 'No 13, St. John\'s church road', '', 'Bangalore', 'Karnataka', 'India', '560005', 599, ''),
(733, '39/4 Kishan Arcade, Doddanakundi Ring Road', '', 'Bangalore', 'Karnataka', 'India', '560037', 600, ''),
(734, '10th Floor, Gamma Block, Sigma Soft Tech Park, 7, Whitefield', '', 'Bangalore', 'Karnataka', 'India', '560066', 601, ''),
(735, '29/3, H M Strafford, 2nd floor, 7th Cross Road, Vasanth nagar', '', 'Bangalore', 'Karnataka', 'India', '560052', 602, ''),
(736, 'First Floor, #15, Sankey Main Road, 6A Cross, Lower Palace Orchard, Sadhashiva nagar', '', 'Bangalore', 'Karnataka', 'India', '560003', 603, ''),
(737, '#36, Railway Parallel road, Nehru Nagar Next to Reddy Petrol Bunk, Kumarapark (west)', '', 'Bangalore', 'Karnataka', 'India', '560020', 604, ''),
(738, 'C-1 & 2 Jyothi Complex, 134/1, Infantry Road', '', 'Bangalore', 'Karnataka', 'India', '560001', 605, ''),
(739, 'Sy.#84, Seegehalli, Bidharahalli Hobli, BETQ', '', 'Bangalore', 'Karnataka', 'India', '560067', 606, ''),
(740, '# 189, 2nd Floor, 1st Main, WOC Road, Mahalakshmi layout', '', 'Bangalore', 'Karnataka', 'India', '560086', 607, ''),
(741, 'No.28, 9th Cross, Off R V Road, 2nd Block, Jayanagar,', '', 'Bangalore', 'Karnataka', 'India', '560011', 608, ''),
(742, '#23 , 1st floor, Sankey Square, Sankey Road, Sadashivnagar', '', 'Bangalore', 'Karnataka', 'India', '560080', 609, ''),
(743, '#331, 3rd stage, 4th block, Basaveshwaranagar', '', 'Bangalore', 'Karnataka', 'India', '560079', 610, ''),
(744, 'No. 61,Shri Vijayaraja Estate, Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Devin Paradise Enclave, Tirumanahalli', '', 'Bangalore', 'Karnataka', 'India', '560064', 611, ''),
(745, '#78, Koramangala Industrial Area, Jyothi Nivas College Road', '', 'Bangalore', 'Karnataka', 'India', '560095', 612, ''),
(746, 'The Residency, 10th Floor, #133/1, Residency Road', '', 'Bangalore', 'Karnataka', 'India', '560025 ', 613, ''),
(747, '3rd Phase,  2nd stage Domlur', '', 'Bangalore', 'Karnataka', 'India', '560071', 614, ''),
(748, 'Flat No. 01, VARS All Seasons Apartment, Off Old Airport Road,  Syndicate Bank Cross, Konena Agrahara', '', 'Bangalore', 'Karnataka', 'India', '560017', 615, ''),
(749, '850 / 2, D Block, Sahakar Nagar', '', 'Bangalore', 'Karnataka', 'India', '560092', 616, ''),
(750, '3rd Floor, ?Vaswani Victoria? 30, Victoria Road', '', 'Bangalore', 'Karnataka', 'India', '560047', 617, ''),
(751, 'Opp. Breeze County, 2nd Cross, Celebriry, Paradise Lay-out, Doddathogur Village, Electronic City', '', 'Bangalore', 'Karnataka', 'India', '560100', 618, ''),
(752, '# 1304,2nd floor,20th Main, 9th Cross,J.P. Nagar, 2nd Phase', '', 'Bangalore', 'Karnataka', 'India', '560078', 619, ''),
(753, '#208 17E Main, KHB Colony, 5th Block Koramangala', '', 'Bangalore', 'Karnataka', 'India', '560095', 620, ''),
(754, '302, Oxford Chambers, Rustum Bagh, Behind Manipal Hospital, Airport Road', '', 'Bangalore', 'Karataka', 'India', '560017', 621, ''),
(755, '# 929, C.G.J Complex, 2nd Floor, BTM 4th Stage, Vijaya Bank Layout, Opp. IBP Petrol Station', '', 'Bangalore', 'Karnataka', 'India', '560076', 622, ''),
(756, '#41/1,1st floor, Bull Temple Road,Basavangudi, Karnataka', '', 'Bangalore', 'Karnataka', 'India', '560004', 623, ''),
(757, '#8, Near Golden Park Apartments, Devarachekkanahalli Main Road', '', 'Bangalore', 'Karnataka', 'India', '560076', 624, ''),
(758, 'Chandrodaya Theatre, Vidyapeeta Circle, Katriguppa Main Road, Ashok Nagar', '', 'Bangalore', 'Karnataka', 'India', '560050', 625, ''),
(759, '# 88/1 First Floor, Opp. BEL Corporate office, Hebbal Ring Road, Nagawara', '', 'Banaglore', 'Karnataka', 'India', '560045', 626, ''),
(760, '# 22, 1st Cross, Ashwath Nagar, Marathahalli', '', 'Bangalore', 'Karnataka', 'India', '560037', 627, ''),
(761, '#No: LIG ? 65D, Ground Floor, KHB Colony, Near Police Station, Hoskote', '', 'Bangalore', 'Karnataka', 'India', '562114', 628, ''),
(762, '#122/B, 2nd Floor, VRR Balu Arcade, 5th Block, KHB Colony, Koramangala', '', 'Bangalore', 'Karnataka', 'India', '560095', 629, ''),
(763, 'Ground Floor,Sampoorna Avenue, Kaggadasapura Main Road,  C.V.Raman Nagar Post', '', 'Bangalore', 'Karnataka', 'India', '560093', 630, ''),
(764, ' #219/11, JP Corp, Bellary Road, Sadashivnagar', '', 'Bangalore', 'Karnataka', 'India', '560080', 631, ''),
(765, '#43/2, 2nd floor, above axis bank near hope farm junction, Whitefield Main Road', '', 'Bangalore', 'Karnataka', 'India', '560066', 632, ''),
(766, 'No. 81, 36th Cross, 6th Main,  5th Block, Jayanagar', '', 'Bangalore', 'Karnataka', 'India', '560041', 633, ''),
(767, 'Sterling Heights, \'Penthouse Suite\' 139 Infantry Road, Opp The Hindu', '', 'Bangalore', 'Karnataka', 'India', '560001', 634, ''),
(768, 'S No 2902/1, 2nd Floor, Temple Rd, 	V V Mohalla, Vontikoppal\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 635, ''),
(769, 'Bandipalya (Opp. APMC Yard), Nanjangud Road (NH ? 212)\r\n', '', 'Mysore', 'Karnataka', 'India', '570025', 636, ''),
(774, 'No. 1/B, 1st Floor, Diya Arcade, Near Fire Brigade, Saraswathipuram', '', 'Mysore', 'Karnataka', 'India', '570009', 641, ''),
(775, '#1104, 1st floor, Udayaravi road, Opp. Pramathi School,Kuvempunagar\r\n', '', 'Mysore', 'Karnataka', 'India', '570023', 642, ''),
(776, '# 1064, CH-16, Hotel Airlines Complex, Near Law Courts, Jayalakshmi Vilas Road, Chamaraja Mohalla\r\n', '', 'Mysore', 'Karnataka', 'India', '570009', 643, ''),
(777, 'Structural Engineers & Builders, Venjay Edifice, # 37, JLB Road\r\n', '', 'Mysore', 'Karnataka', 'India', '570005', 644, ''),
(778, '# 2877- G2, 6th cross, V.V. Mohalla\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 645, ''),
(779, '2997/2, Rukma Complex, Kalidasa Road, V V Mohalla\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 646, ''),
(780, '#12/1, 14th Cross, 3 \'A\' Block, V.V Mohalla , Off Kalidasa Road\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 647, ''),
(781, '#36,37, D.D.Urs, Road\r\n', '', 'Mysore', 'Karnataka', 'India', '570001', 648, ''),
(782, 'No.1, S-5, B Ningappa Complex, New Kantharaj Urs Road, BEML HBCS Layout\r\n', '', 'Mysore', 'Karnataka', 'India', '570022', 649, ''),
(783, 'D/6A, Temple Road, V.V.Mohalla\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 650, ''),
(784, 'No 70/1, 12th Cross, Off Kalidasa Road, Jayalakshmipuram, V V Mohalla\r\n', '', 'Mysore', 'Karnataka', 'India', '570002', 651, ''),
(785, 'Bandipalya, Opp. APMC Yard, Nanjangud Road,NH ? 212', '', 'Mysore', 'Karnataka', 'India', '570025', 652, ''),
(794, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 660, ''),
(795, 'test', NULL, 'Bangalore', 'Karnataka', 'India', '560058', NULL, ''),
(796, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560041', 661, ''),
(797, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560041', NULL, ''),
(798, 'Brigade Enterprises Limited 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram ,Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 662, ''),
(799, 'Brigade Enterprises Limited 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram ,Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(800, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 663, ''),
(801, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(804, '#5', '', 'Bangalore', 'Karnataka', 'India', '560039', 666, ''),
(805, '#5', NULL, 'Bangalore', 'Karnataka', 'India', '560039', NULL, ''),
(806, '#54', '', 'Bangalore', 'Karnataka', 'India', '435345', 667, ''),
(807, '#54', NULL, 'Bangalore', 'Karnataka', 'India', '435345', NULL, ''),
(818, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar.', '', 'Bangalore', 'Karnataka', 'India', '560055', 676, ''),
(819, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar.', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(820, '9th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 677, ''),
(821, '9th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(822, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 678, ''),
(823, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(829, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 682, ''),
(830, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(831, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 683, ''),
(832, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(833, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 684, ''),
(834, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(835, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 685, ''),
(836, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(837, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 686, ''),
(838, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, '');
INSERT INTO `address` (`id`, `adrs_line1`, `adrs_line2`, `city`, `state`, `country`, `postal_code`, `telephone_id`, `proof_doc_type`) VALUES
(841, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 688, ''),
(842, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(843, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 689, ''),
(844, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(845, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 690, ''),
(846, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(847, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 691, ''),
(848, ' 29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(849, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 692, ''),
(850, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(851, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Mysore', 'Karnataka', 'India', '560055', 693, ''),
(852, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Mysore', 'Karnataka', 'India', '560055', NULL, ''),
(853, 'Brigade Point, Gokulam Road V.V.Mohalla', '', 'Mysore', 'Karnataka', 'India', '570002', 694, ''),
(854, 'Brigade Point, Gokulam Road V.V.Mohalla', NULL, 'Mysore', 'Karnataka', 'India', '570002', NULL, ''),
(855, ' Brigade Point, Gokulam Road V.V.Mohalla', '', 'Mysore', 'Karnataka', 'India', '570002', 695, ''),
(856, ' Brigade Point, Gokulam Road V.V.Mohalla', NULL, 'Mysore', 'Karnataka', 'India', '570002', NULL, ''),
(857, ' Brigade Point, Gokulam Road V.V.Mohalla', '', 'Mysore', 'Karnataka', 'India', '570002', 696, ''),
(858, ' Brigade Point, Gokulam Road V.V.Mohalla', NULL, 'Mysore', 'Karnataka', 'India', '570002', NULL, ''),
(859, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 697, ''),
(860, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(861, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '56', 698, ''),
(862, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '56', NULL, ''),
(863, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '56', NULL, ''),
(864, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 699, ''),
(865, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(866, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 700, ''),
(867, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(868, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 701, ''),
(869, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(870, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 702, ''),
(871, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(872, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 703, ''),
(873, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(874, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 704, ''),
(875, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(876, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 705, ''),
(877, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(878, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 706, ''),
(879, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(880, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 707, ''),
(881, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(882, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 708, ''),
(883, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(884, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 709, ''),
(885, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(886, 'Brigade Point, Gokulam Road V.V.Mohalla', '', 'Mysore', 'Karnataka', 'India', '570002', 710, ''),
(887, 'Brigade Point, Gokulam Road V.V.Mohalla', NULL, 'Mysore', 'Karnataka', 'India', '570002', NULL, ''),
(888, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 711, ''),
(889, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(890, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 712, ''),
(891, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(892, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', '', 'Bangalore', 'Karnataka', 'India', '560103', 713, ''),
(893, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(894, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', '', 'Bangalore', 'Karnataka', 'India', '560103', 714, ''),
(895, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(896, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', '', 'Bangalore', 'Karnataka', 'India', '560103', 715, ''),
(897, 'Sarjapur - Marthahalli Outer Ring Road (ORR),Devarabisanahalli, Bellandur Post,', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(898, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 716, ''),
(899, 'yghuihui', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(900, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560055', 717, ''),
(901, '29th & 30th Floors, World Trade Center Brigade Gateway Campus, 26/1, Dr Rajkumar Road Malleswaram-Rajajinagar', NULL, 'Bangalore', 'Karnataka', 'India', '560055', NULL, ''),
(902, 'Sarjapur - Marthahalli Outer Ring Road (ORR), Devarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 718, ''),
(903, 'Sarjapur - Marthahalli Outer Ring Road (ORR), Devarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(904, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 719, ''),
(905, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(906, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 720, ''),
(907, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(908, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', '', 'Bangalore', 'Karnataka', 'India', '560103', 721, ''),
(909, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(910, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(911, 'Sarjapur - Marthahalli Outer Ring Road (ORR)\r\nDevarabisanahalli, Bellandur Post', NULL, 'Bangalore', 'Karnataka', 'India', '560103', NULL, ''),
(912, 'No  A2, Daffodils, 25, Church Street', '', 'Bangalore', 'Karnataka', 'India', '560001', 722, ''),
(913, 'No  A2, Daffodils, 25, Church Street', '', 'Bangalore', 'Karnataka', 'India', '560001', 723, ''),
(914, 'abadddf', '', 'Bangalore', 'Karnataka', 'India', '9635', 724, ''),
(915, 'abadddf', NULL, 'Bangalore', 'Karnataka', 'India', '9635', NULL, ''),
(916, '135/2m, Brigade Tower, Brigade Road\r\n\r\nBrigade Road', NULL, 'Bangalore', 'Karnataka', 'India', '560025', NULL, ''),
(917, 'sdf', '', 'Bangalore', 'Karnataka', 'India', '43545', 725, ''),
(918, 'sdf', NULL, 'Bangalore', 'Karnataka', 'India', '43545', NULL, ''),
(919, 'erwesfsf', '', 'Bangalore', 'Karnataka', 'India', '43534534', 726, ''),
(920, 'erwesfsf', NULL, 'Bangalore', 'Karnataka', 'India', '43534534', NULL, ''),
(921, '345', '', '435', '435', '435', '435', 727, ''),
(922, 'sdaf', '', '435', '345', '435', '435345', 728, ''),
(923, 'peenya', '', 'bangalore', 'Karnataka', 'India', '560026', 729, ''),
(924, 'peenya', '', 'bangalore', 'Karnataka', 'India', '560026', 730, 'DL'),
(925, 'peenya', '', 'Bangalore', 'karnataka', 'India', '560058', 731, ''),
(926, '29th & 30th Floors, World Trade Center, Brigade Gateway Campus, 26/1, Dr Rajkumar Road, Malleswaram, Rajajinagar', '', 'Bangalore', 'Karnataka', 'India', '560041', 732, '');

-- --------------------------------------------------------

--
-- Table structure for table `amenity_type`
--

CREATE TABLE `amenity_type` (
  `amenity` varchar(100) NOT NULL COMMENT 'Amenity',
  `is_present` int(11) NOT NULL COMMENT 'Is Present???',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On',
  `type` varchar(100) NOT NULL DEFAULT 'Amenity' COMMENT 'Type',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Icon'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenity_type`
--

INSERT INTO `amenity_type` (`amenity`, `is_present`, `created_on`, `type`, `icon`) VALUES
('Aerobics/Yoga Room', 1, '2016-03-09 00:55:39', 'Amenity', '07.jpg'),
('Amphitheatre', 1, '2016-01-12 23:53:34', 'Amenity', NULL),
('Arts Village', 1, '2016-01-23 05:27:23', 'Amenity', NULL),
('ATM', 1, '2015-12-12 06:25:58', 'Amenity', NULL),
('Badminton Court', 1, '2015-12-12 06:21:49', 'Amenity', NULL),
('Badminton Hall', 1, '2016-01-29 02:59:15', 'Amenity', NULL),
('Barbeque Area', 1, '2016-01-29 02:59:44', 'Amenity', NULL),
('Basket Ball Court', 1, '2015-12-10 00:56:05', 'Amenity', NULL),
('Basketball Court', 1, '2016-01-29 03:01:21', 'Amenity', NULL),
('Billiards Table', 1, '2015-12-12 06:23:18', 'Amenity', NULL),
('Cafeteria / Food Court ', 1, '2015-10-24 10:47:23', 'Amenity', NULL),
('Children\'s Play Area', 1, '2015-12-12 06:23:29', 'Amenity', NULL),
('Club House', 1, '2015-10-24 10:40:52', 'Amenity', NULL),
('Commercial Centre', 1, '2016-01-23 05:31:06', 'Amenity', NULL),
('Convention Center ', 1, '2015-10-24 10:41:28', 'Amenity', NULL),
('Cycling Track', 1, '2015-12-12 06:24:50', 'Amenity', NULL),
('Fitness Center', 1, '2016-02-04 08:19:01', 'Amenity', '01-TM-property-for-rent-listing-V2-filters-1.jpg'),
('Garden', 1, '2015-10-24 12:01:38', 'Amenity', NULL),
('green park ', 1, '2016-02-22 05:01:55', 'Facility', 'bedroom.svg'),
('GYM', 1, '2015-12-10 00:56:34', 'Amenity', NULL),
('Gymnasium', 1, '2015-12-12 06:23:39', 'Amenity', NULL),
('Hospital', 1, '2015-12-12 06:21:12', 'Amenity', NULL),
('Indoor Games Area', 1, '2016-01-23 05:30:00', 'Amenity', NULL),
('Internet / Wi-Fi', 1, '2015-10-24 10:42:36', 'Amenity', NULL),
('IP TV Cable', 1, '2015-12-10 00:57:09', 'Amenity', NULL),
('Jogging Track', 1, '2015-12-12 06:23:50', 'Amenity', NULL),
('Kid\'s Pool', 1, '2016-01-29 03:00:01', 'Amenity', NULL),
('Landscaped Garden', 1, '2016-01-23 05:30:42', 'Amenity', NULL),
('Library', 1, '2015-10-24 10:45:50', 'Amenity', NULL),
('Lift', 1, '2016-02-22 04:59:34', 'Facility', 'bedroom.svg'),
('Lift Passenger', 1, '2015-12-12 06:26:27', 'Amenity', NULL),
('Lift Service', 1, '2015-12-12 06:26:38', 'Amenity', NULL),
('Lounge Bar', 1, '2015-12-12 06:25:03', 'Amenity', NULL),
('Meditation Area', 1, '2015-12-10 01:03:06', 'Amenity', NULL),
('Park', 1, '2015-10-24 10:41:49', 'Amenity', NULL),
('Party Hall', 1, '2015-12-12 06:25:12', 'Amenity', NULL),
('Power Backup', 1, '2015-10-24 10:40:28', 'Amenity', NULL),
('Rain Water Harvesting', 1, '2015-12-10 00:57:40', 'Amenity', NULL),
('Rooftop swimming pool', 1, '2016-01-23 05:41:22', 'Amenity', NULL),
('Security', 1, '2015-10-24 10:43:48', 'Amenity', NULL),
('Senior Citizen\'s  Area', 1, '2016-01-29 03:02:28', 'Amenity', NULL),
('Shopping Arcade', 1, '2016-01-23 05:30:22', 'Amenity', NULL),
('Shopping Mall', 1, '2015-12-12 06:20:57', 'Amenity', NULL),
('Signature Club', 1, '2016-01-23 05:27:07', 'Amenity', NULL),
('Social Infrastructure', 1, '2016-01-23 05:27:41', 'Amenity', NULL),
('SPA', 1, '2015-12-10 01:02:07', 'Amenity', NULL),
('Sports Area', 1, '2016-01-23 05:26:47', 'Amenity', NULL),
('Squash Court', 1, '2015-12-12 06:25:25', 'Amenity', NULL),
('Super Market Space', 1, '2015-12-12 06:25:44', 'Amenity', NULL),
('Swimming Pool', 1, '2015-09-03 08:24:20', 'Amenity', NULL),
('Table Tennis', 1, '2015-12-12 06:20:30', 'Amenity', NULL),
('Tennis court', 1, '2015-10-24 10:44:28', 'Amenity', NULL),
('Terrace Garden', 1, '2015-12-12 06:27:13', 'Amenity', NULL),
('Visitor Parking', 1, '2015-12-12 06:26:48', 'Amenity', NULL),
('Water Storage Tank', 1, '2015-12-12 06:27:02', 'Amenity', NULL),
('Yogo Room', 1, '2016-01-29 03:00:17', 'Amenity', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `apartment`
--

CREATE TABLE `apartment` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `property_id` int(11) NOT NULL COMMENT 'Property Id',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `units` varchar(100) NOT NULL COMMENT 'Units',
  `no_floors` int(11) NOT NULL COMMENT 'Number of Floors',
  `year_of_constructiom` year(4) NOT NULL COMMENT 'Date of Construction',
  `address_id` int(11) NOT NULL COMMENT 'Address Id',
  `main_power` varchar(100) NOT NULL COMMENT 'Main Power',
  `backup_power` varchar(100) NOT NULL COMMENT 'Back Up Power'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `area_partition_type`
--

CREATE TABLE `area_partition_type` (
  `partition_type` varchar(100) NOT NULL COMMENT 'Partition Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `category` varchar(200) NOT NULL COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attachment_type`
--

CREATE TABLE `attachment_type` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `attachment` varchar(200) NOT NULL COMMENT 'Attachment Type',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attachment_type`
--

INSERT INTO `attachment_type` (`id`, `attachment`, `is_active`) VALUES
(3, 'Address Proof', 0),
(4, 'ID Proof', 0),
(5, 'Education', 0),
(6, 'Image', 0);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 2, 'Administrator', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_loans`
--

CREATE TABLE `bank_loans` (
  `loan` varchar(100) NOT NULL COMMENT 'Loan',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Icon',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_loans`
--

INSERT INTO `bank_loans` (`loan`, `icon`, `is_active`) VALUES
('Allahabad Bank', NULL, 1),
('Andhra Bank', NULL, 1),
('Axis Bank', NULL, 1),
('Bank of Baroda', NULL, 1),
('Bank of India', '01-TM-property-for-rent-listing-V2-filters-1.jpg', 1),
('Bank of Maharashtra', NULL, 1),
('Bulls', NULL, 1),
('Canara Bank', NULL, 1),
('Central Bank of India', NULL, 1),
('Citibank', NULL, 1),
('City Union Bank', NULL, 1),
('Corporation Bank', NULL, 1),
('Dena Bank', NULL, 1),
('Dhanlaxmi Bank', NULL, 1),
('Federal Bank', NULL, 1),
('HDFC Ltd', NULL, 1),
('ICICI Bank', NULL, 1),
('IDBI BanK', NULL, 1),
('Indian Bank', NULL, 1),
('Indian Overseas Bank', NULL, 1),
('IndusInd Bank', NULL, 1),
('ING Vysya Bank', NULL, 1),
('J & K Bank ', NULL, 1),
('Jammu and Kashmir Bank', NULL, 1),
('Karnataka Bank', NULL, 1),
('Karur Vysya Bank', NULL, 1),
('Kotak Mahindra Bank', NULL, 1),
('L&T Housing Finance Ltd', NULL, 1),
('Lakshmi Vilas Bank', NULL, 1),
('Oriental Bank of Commerce', NULL, 1),
('PNB Housing Finance', NULL, 1),
('Punjab & Sind Bank', NULL, 1),
('Punjab National Bank', NULL, 1),
('RBL Bank', NULL, 1),
('South Indian Bank', NULL, 1),
('Standard Chartered Bank', NULL, 1),
('State Bank of Hyderabad', NULL, 1),
('State Bank of India', NULL, 1),
('State Bank of Mysore', NULL, 1),
('State Bank of Patiala', NULL, 1),
('State Bank of Travancore', NULL, 1),
('Syndicate Bank', NULL, 1),
('UCO Bank', NULL, 1),
('Union Bank of India', NULL, 1),
('United Bank of India', NULL, 1),
('Vijaya Bank', NULL, 1),
('Yes Bank', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `brand_names`
--

CREATE TABLE `brand_names` (
  `brandname` varchar(200) NOT NULL COMMENT 'Brand Name',
  `category` varchar(100) DEFAULT NULL COMMENT 'Category',
  `id` int(11) NOT NULL COMMENT 'ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand_names`
--

INSERT INTO `brand_names` (`brandname`, `category`, `id`) VALUES
('Usha', 'Electronics & Electrical', 6),
('Wirlpool', 'Electronics & Electrical', 7),
('Philips', 'Electronics & Electrical', 8),
('Samsung', 'Electronics & Electrical', 9),
('Bosch', 'Electronics & Electrical', 10),
('Bosch', 'Electronics & Electrical', 11),
('Blue Star', 'Electronics & Electrical', 12),
('Carrier', 'Electronics & Electrical', 13),
('Daikin', 'Electronics & Electrical', 14),
('Electrolux', 'Electronics & Electrical', 15),
('Fujiaire', 'Electronics & Electrical', 16),
('Godrej', 'Electronics & Electrical', 17),
('GreenDust', 'Electronics & Electrical', 18),
('Hitachi', 'Electronics & Electrical', 19),
('Haier', 'Electronics & Electrical', 20),
('Havells', 'Electronics & Electrical', 21),
('Hyundai', 'Electronics & Electrical', 22),
('IFB', 'Electronics & Electrical', 23),
('Kenstar', 'Electronics & Electrical', 24),
('Lloyd', 'Electronics & Electrical', 25),
('LG', 'Electronics & Electrical', 26),
('Mitsubishi', 'Electronics & Electrical', 27),
('Onida', 'Electronics & Electrical', 28),
('Panasonic', 'Electronics & Electrical', 29),
('Panasonic', 'Electronics & Electrical', 30),
('Sharp', 'Electronics & Electrical', 31),
('Sollatek', 'Electronics & Electrical', 32),
('Sansui', 'Electronics & Electrical', 33),
('Trane', 'Electronics & Electrical', 34),
('Videocon', 'Electronics & Electrical', 35),
('Voltas', 'Electronics & Electrical', 36),
('BPL', 'Electronics & Electrical', 37),
('Vox', 'Electronics & Electrical', 38),
('Siemens', 'Electronics & Electrical', 39),
('GEM', 'Electronics & Electrical', 40),
('Morphy Richard', 'Electronics & Electrical', 41),
('Bajaj', 'Electronics & Electrical', 42),
('Aquaguard', 'Electronics & Electrical', 43),
('Eureka Forbes', 'Electronics & Electrical', 44),
('Pureit', 'Electronics & Electrical', 45),
('Pureit', 'Electronics & Electrical', 46),
('Tata Swach', 'Electronics & Electrical', 47),
('Aquaguard Plus', 'Electronics & Electrical', 48),
('Livepure', 'Electronics & Electrical', 49),
('Aquafresh', 'Electronics & Electrical', 50),
('Blossom', 'Electronics & Electrical', 51),
('Delta', 'Electronics & Electrical', 52),
('Eurotech', 'Electronics & Electrical', 53),
('Hi-Tech', 'Electronics & Electrical', 54),
('Krona', 'Electronics & Electrical', 55),
('Leaupure', 'Electronics & Electrical', 56),
('Pentus', 'Electronics & Electrical', 57),
('Renovator', 'Electronics & Electrical', 58),
('Shudhplus', 'Electronics & Electrical', 59),
('Supermaxx', 'Electronics & Electrical', 60),
('McCoy', 'Electronics & Electrical', 61),
('Crompton Greaves', 'Electronics & Electrical', 62),
('Orpat', 'Electronics & Electrical', 63),
('Oster', 'Electronics & Electrical', 64),
('Ram Coolers', 'Electronics & Electrical', 65),
('Symphony', 'Electronics & Electrical', 66),
('Singer', 'Electronics & Electrical', 67),
('Maharaja Whiteline', 'Electronics & Electrical', 68),
('Osram', 'Electronics & Electrical', 69),
('Eveready', 'Electronics & Electrical', 71),
('Oreva', 'Electronics & Electrical', 72),
('Moser Baer', 'Electronics & Electrical', 74),
('Surya', 'Electronics & Electrical', 75),
('Ujjawal', 'Electronics & Electrical', 76),
('Ajanta', 'Electronics & Electrical', 77),
('Anchor', 'Electronics & Electrical', 78),
('Luminous', 'Electronics & Electrical', 79),
('V-Guard', 'Electronics & Electrical', 80),
('Allora', 'Electronics & Electrical', 81),
('ACS', 'Electronics & Electrical', 82),
('Arise', 'Electronics & Electrical', 83),
('Blue Me', 'Electronics & Electrical', 84),
('Breezelit', 'Electronics & Electrical', 85),
('Brezo', 'Electronics & Electrical', 86),
('Brissk', 'Electronics & Electrical', 87),
('Champion', 'Electronics & Electrical', 88),
('Citron', 'Electronics & Electrical', 89),
('Comforts', 'Electronics & Electrical', 90),
('Desire', 'Electronics & Electrical', 91),
('E-Fab', 'Electronics & Electrical', 92),
('Eplar', 'Electronics & Electrical', 93),
('Fanzart', 'Electronics & Electrical', 94),
('Kailash', 'Electronics & Electrical', 95),
('Karishma', 'Electronics & Electrical', 96),
('Omega', 'Electronics & Electrical', 98),
('Ovastar', 'Electronics & Electrical', 99),
('Polar', 'Electronics & Electrical', 100),
('Fabbiano', 'Electronics & Electrical', 101),
('Bravo', 'Electronics & Electrical', 102),
('Honda', 'Electronics & Electrical', 104),
('Kirloskar', 'Electronics & Electrical', 105),
('Rapower', 'Electronics & Electrical', 106),
('Mahindra Power', 'Electronics & Electrical', 107),
('Padmini', 'Electronics & Electrical', 108),
('Alfa', 'Electronics & Electrical', 109),
('Black And Decker', 'Electronics & Electrical', 110),
('Kanchan', 'Electronics & Electrical', 111),
('Orient', 'Electronics & Electrical', 112),
('Victor TVS', 'Electronics & Electrical', 113),
('Standard', 'Electronics & Electrical', 114),
('Belkin', 'Electronics & Electrical', 115),
('APC', 'Electronics & Electrical', 116),
('Sollatek', 'Electronics & Electrical', 117),
('cascade', 'Electronics & Electrical', 118),
('Eazys', 'Electronics & Electrical', 119),
('Gilma', 'Electronics & Electrical', 120),
('Hindware', 'Electronics & Electrical', 121),
('Hotstar', 'Electronics & Electrical', 122),
('Inalsa', 'Electronics & Electrical', 123),
('Koryo', 'Electronics & Electrical', 124),
('Marc', 'Electronics & Electrical', 125),
('Racold', 'Electronics & Electrical', 126),
('Sammer', 'Electronics & Electrical', 127),
('Remson', 'Electronics & Electrical', 128),
('Sigma', 'Electronics & Electrical', 129),
('Valka', 'Electronics & Electrical', 130),
('Venus', 'Electronics & Electrical', 131),
('O General', 'Electronics & Electrical', 132),
('Khaitan', 'Electronics & Electrical', 134),
('Microtek', 'Electronics & Electrical', 135),
('Kent', 'Electronics & Electrical', 136),
('Gilma', 'Electronics & Electrical', 137),
('Kelvinator', 'Electronics & Electrical', 138),
('Asian Paints', 'Painting', 139),
('Parryware', 'Plumbing', 140),
('Jaguar', 'Plumbing', 141),
('Hindware', 'Plumbing', 142),
('Johnson', 'Plumbing', 143),
('Crea', 'Plumbing', 144),
('Graffiti', 'Plumbing', 145),
('Sheetal', 'Plumbing', 146),
('Auriga', 'Plumbing', 147),
('Aai', 'Plumbing', 148),
('Accedre', 'Plumbing', 149),
('Adson', 'Plumbing', 150),
('Agas', 'Plumbing', 151),
('Alpina', 'Plumbing', 152),
('Alton', 'Plumbing', 153),
('Anox', 'Plumbing', 154),
('Anupam', 'Plumbing', 155),
('Aqua Ocean', 'Plumbing', 156),
('Aquadyne', 'Plumbing', 157),
('Aquarium', 'Plumbing', 158),
('Aquieen', 'Plumbing', 159),
('Aris', 'Plumbing', 160),
('Arkay', 'Plumbing', 161),
('Armamix', 'Plumbing', 162),
('Aviva', 'Plumbing', 163),
('Benelave', 'Plumbing', 164),
('Bharat', 'Plumbing', 165),
('Blue Sea', 'Plumbing', 166),
('Byc', 'Plumbing', 167),
('Cambio', 'Plumbing', 168),
('Casa Decor', 'Plumbing', 169),
('Cavo', 'Plumbing', 170),
('CBM', 'Plumbing', 171),
('Celebrity', 'Plumbing', 172),
('Chilly', 'Plumbing', 173),
('Chrome', 'Plumbing', 174),
('Classic', 'Plumbing', 175),
('Cosrich', 'Plumbing', 176),
('Crayons', 'Plumbing', 177),
('Dazzle', 'Plumbing', 178),
('Decowell', 'Plumbing', 179),
('Dee Max', 'Plumbing', 180),
('Delight', 'Plumbing', 181),
('Delta', 'Plumbing', 182),
('Designo', 'Plumbing', 183),
('Dhawan', 'Plumbing', 184),
('Diore', 'Plumbing', 185),
('Disha', 'Plumbing', 186),
('Dizayn', 'Plumbing', 187),
('Dloop', 'Plumbing', 188),
('Dolin', 'Plumbing', 189),
('Dolphy', 'Plumbing', 190),
('Dooa', 'Plumbing', 191),
('Dorma', 'Plumbing', 192),
('Doyours', 'Plumbing', 193),
('DRC', 'Plumbing', 194),
('Easyflow', 'Plumbing', 195),
('Echo', 'Plumbing', 196),
('Efficia', 'Plumbing', 197),
('Elegance', 'Plumbing', 198),
('Elems', 'Plumbing', 200),
('Elvy', 'Plumbing', 201),
('EmEss', 'Plumbing', 202),
('Enfin Homes', 'Plumbing', 203),
('Eon', 'Plumbing', 204),
('Eselife', 'Plumbing', 205),
('Ess Ess', 'Plumbing', 206),
('Euro Glass', 'Plumbing', 207),
('Euroitaly', 'Plumbing', 208),
('EZBond Power Hook', 'Plumbing', 209),
('Fancy Frost', 'Plumbing', 210),
('Fancy Glass', 'Plumbing', 211),
('Fancy Glass Frosted', 'Plumbing', 212),
('Fancy Glass Shelf', 'Plumbing', 213),
('Ferrokare', 'Plumbing', 214),
('Fixon', 'Plumbing', 215),
('Forlive', 'Plumbing', 216),
('Forte', 'Plumbing', 217),
('Franko', 'Plumbing', 218),
('Fresca', 'Plumbing', 219),
('Fuao', 'Plumbing', 220),
('Fusion', 'Plumbing', 221),
('Futura', 'Plumbing', 222),
('G S Enterprise', 'Plumbing', 223),
('Ganeshaas', 'Plumbing', 224),
('Garden', 'Plumbing', 225),
('Garvila', 'Plumbing', 226),
('Gayatri', 'Plumbing', 227),
('Gesign', 'Plumbing', 228),
('Glitz', 'Plumbing', 229),
('Globex', 'Plumbing', 230),
('Godrej', 'Plumbing', 231),
('Goeka', 'Plumbing', 232),
('Goldmind', 'Plumbing', 233),
('Goonj', 'Plumbing', 234),
('Graffiti', 'Plumbing', 235),
('Greggs', 'Plumbing', 236),
('Gypsy', 'Plumbing', 237),
('Hallmarc', 'Plumbing', 238),
('Handecor', 'Plumbing', 239),
('Handy', 'Plumbing', 240),
('Haneez', 'Plumbing', 241),
('Hart', 'Plumbing', 242),
('Henzer', 'Plumbing', 243),
('Hettich', 'Plumbing', 244),
('Hi Life', 'Plumbing', 245),
('Hira', 'Plumbing', 246),
('Hiran', 'Plumbing', 247),
('HitPlay', 'Plumbing', 248),
('Howards', 'Plumbing', 249),
('Hygenic Plus', 'Plumbing', 251),
('Imax Locks', 'Plumbing', 252),
('Impressive', 'Plumbing', 253),
('Indian Shelf', 'Plumbing', 254),
('Indune Lifestyle', 'Plumbing', 255),
('Jaaz', 'Plumbing', 256),
('JBS', 'Plumbing', 257),
('Jester', 'Plumbing', 258),
('Johnson', 'Plumbing', 259),
('Jolly\'s', 'Plumbing', 260),
('Joy', 'Plumbing', 261),
('JP Hardware', 'Plumbing', 262),
('JVG', 'Plumbing', 263),
('Kamal', 'Plumbing', 264),
('Karmakara', 'Plumbing', 265),
('Kataria', 'Plumbing', 266),
('Kawachi', 'Plumbing', 267),
('KBG', 'Plumbing', 268),
('KCI', 'Plumbing', 269),
('Kea', 'Plumbing', 270),
('Kitsch', 'Plumbing', 271),
('Klassik', 'Plumbing', 272),
('Klaxon', 'Plumbing', 273),
('Krishna', 'Plumbing', 274),
('KRM', 'Plumbing', 275),
('Krystal', 'Plumbing', 276),
('Kywin', 'Plumbing', 277),
('Lacuzini', 'Plumbing', 278),
('Legris', 'Plumbing', 279),
('LMC', 'Plumbing', 280),
('Lucky', 'Plumbing', 281),
('Magnifico', 'Plumbing', 282),
('Mahavira', 'Plumbing', 283),
('Mantissa', 'Plumbing', 284),
('Macrowave', 'Plumbing', 285),
('Marine', 'Plumbing', 286),
('Marvel', 'Plumbing', 287),
('Mavi', 'Plumbing', 288),
('Maxo', 'Plumbing', 289),
('Mayur', 'Plumbing', 290),
('Metro', 'Plumbing', 291),
('Moen', 'Plumbing', 292),
('Mog', 'Plumbing', 293),
('Montero', 'Plumbing', 294),
('Morsel', 'Plumbing', 295),
('MSI', 'Plumbing', 296),
('Muren', 'Plumbing', 297),
('Mygo', 'Plumbing', 298),
('Naman', 'Plumbing', 299),
('Nanson', 'Plumbing', 300),
('Neat', 'Plumbing', 301),
('Neerja', 'Plumbing', 302),
('Neosol', 'Plumbing', 303),
('NIIV', 'Plumbing', 304),
('Nilkamal', 'Plumbing', 305),
('Ocean', 'Plumbing', 306),
('Olive', 'Plumbing', 307),
('Onlinemart', 'Plumbing', 308),
('Orange Tree', 'Plumbing', 309),
('Orca', 'Plumbing', 310),
('Ozone', 'Plumbing', 311),
('Palakz', 'Plumbing', 312),
('Palam', 'Plumbing', 313),
('Panorama', 'Plumbing', 314),
('Parkovic', 'Plumbing', 315),
('Parryware', 'Plumbing', 316),
('Peacock Life', 'Plumbing', 317),
('Pebbleyard', 'Plumbing', 318),
('Penguin', 'Plumbing', 319),
('Phantom', 'Plumbing', 320),
('Pindia', 'Plumbing', 321),
('Platina', 'Plumbing', 322),
('Plato', 'Plumbing', 323),
('Palaroll', 'Plumbing', 324),
('Polytuf', 'Plumbing', 325),
('Poweron', 'Plumbing', 326),
('Prachin', 'Plumbing', 327),
('Premsons', 'Plumbing', 328),
('Purpledip', 'Plumbing', 329),
('Qpro', 'Plumbing', 330),
('Regalia', 'Plumbing', 331),
('Regis', 'Plumbing', 332),
('Renson', 'Plumbing', 333),
('Rimei', 'Plumbing', 334),
('Ripples', 'Plumbing', 335),
('Room Groom', 'Plumbing', 336),
('Royal', 'Plumbing', 337),
('Ruby', 'Plumbing', 338),
('Rust-Oleum', 'Plumbing', 339),
('Safeseed', 'Plumbing', 340),
('Saleh', 'Plumbing', 341),
('Samrat', 'Plumbing', 342),
('Sanimart', 'Plumbing', 343),
('Sanitario', 'Plumbing', 344),
('Sanvi', 'Plumbing', 345),
('SDG', 'Plumbing', 346),
('Sedam', 'Plumbing', 347),
('SEK', 'Plumbing', 348),
('Seven Seas', 'Plumbing', 349),
('SGB', 'Plumbing', 350),
('Sheetal', 'Plumbing', 351),
('Shine Star', 'Plumbing', 352),
('Shriji', 'Plumbing', 353),
('Shruti', 'Plumbing', 354),
('Simoll', 'Plumbing', 355),
('Sipco', 'Plumbing', 356),
('Sk Metal', 'Plumbing', 357),
('Skayline', 'Plumbing', 358),
('Skoot', 'Plumbing', 359),
('Koot', 'Plumbing', 360),
('Mart Shophar', 'Plumbing', 361),
('Miledrive', 'Plumbing', 362),
('Nowbell', 'Plumbing', 363),
('Omani', 'Plumbing', 364),
('Onworld', 'Plumbing', 365),
('Parkle India', 'Plumbing', 366),
('Sparrow', 'Plumbing', 367),
('Spartan', 'Plumbing', 368),
('Spazio', 'Plumbing', 369),
('Spectro', 'Plumbing', 370),
('Speedwav', 'Plumbing', 371),
('Springs', 'Plumbing', 372),
('SR', 'Plumbing', 373),
('Sriman Exims', 'Plumbing', 374),
('SSI', 'Plumbing', 375),
('Stag', 'Plumbing', 376),
('Star Flush', 'Plumbing', 377),
('Strake', 'Plumbing', 378),
('Sterling', 'Plumbing', 379),
('Stonekraft', 'Plumbing', 380),
('Sungold', 'Plumbing', 381),
('Sunrise', 'Plumbing', 382),
('Super IT', 'Plumbing', 383),
('Supreme', 'Plumbing', 384),
('Swift', 'Plumbing', 385),
('Takai', 'Plumbing', 386),
('Taptree', 'Plumbing', 387),
('Tatay', 'Plumbing', 388),
('Tech Fit', 'Plumbing', 389),
('Teeta', 'Plumbing', 390),
('Telebuy', 'Plumbing', 391),
('Tibros', 'Plumbing', 392),
('Tirupati', 'Plumbing', 393),
('TLS', 'Plumbing', 394),
('TMC', 'Plumbing', 395),
('Toto', 'Plumbing', 396),
('Touch me', 'Plumbing', 397),
('Travel Blue', 'Plumbing', 398),
('Treasure Hunt', 'Plumbing', 399),
('Tricon', 'Plumbing', 400),
('Trot', 'Plumbing', 401),
('Truphe', 'Plumbing', 402),
('Turnip', 'Plumbing', 403),
('Tycab', 'Plumbing', 404),
('U Comfort', 'Plumbing', 405),
('UBL', 'Plumbing', 406),
('Umbra', 'Plumbing', 407),
('Uniair', 'Plumbing', 408),
('Unravel India', 'Plumbing', 409),
('Urban Monk Creations', 'Plumbing', 410),
('Uteki', 'Plumbing', 411),
('Vedanta', 'Plumbing', 412),
('Velcro', 'Plumbing', 413),
('Veu', 'Plumbing', 414),
('Victor', 'Plumbing', 415),
('Viking', 'Plumbing', 416),
('Visko', 'Plumbing', 417),
('Vitara', 'Plumbing', 418),
('Vividha', 'Plumbing', 419),
('Vola', 'Plumbing', 420),
('Water Tec', 'Plumbing', 421),
('Wenko', 'Plumbing', 422),
('White Gold', 'Plumbing', 423),
('White Star', 'Plumbing', 424),
('Wink', 'Plumbing', 425),
('Woodsmith', 'Plumbing', 426),
('Xylex', 'Plumbing', 427),
('Yale', 'Plumbing', 428),
('Zahab', 'Plumbing', 429),
('Zain', 'Plumbing', 430),
('Zibo', 'Plumbing', 431),
('Zim', 'Plumbing', 432),
('Zoiks', 'Plumbing', 433),
('Zotalic', 'Plumbing', 434),
('Royal', 'Painting', 435),
('Valspar', 'Painting', 436),
('Jotun', 'Painting', 437),
('Berger', 'Painting', 438),
('Asian', 'Painting', 439),
('Indianpaints', 'Painting', 440),
('Regal', 'Painting', 441),
('Utsarg', 'Painting', 442),
('Dulux', 'Painting', 443),
('Gobal', 'Painting', 444),
('Lifestyle', 'Painting', 445),
('Alba', 'Painting', 446),
('Ace', 'Painting', 447),
('Nerolac', 'Painting', 448),
('Agsar', 'Painting', 449),
('Sheenlac', 'Painting', 450),
('Bruce', 'Flooring', 452),
('Carlisle', 'Flooring', 453),
('Hearne hardwoods', 'Flooring', 454),
('Royal', 'Painting', 455),
('Valspar', 'Painting', 456),
('Jotun', 'Painting', 457),
('Berger', 'Painting', 458),
('Aarsun woods', 'Furnishing', 459),
('Ace', 'Furnishing', 460),
('Adara', 'Furnishing', 461),
('Agromech', 'Furnishing', 462),
('Aristio', 'Furnishing', 463),
('Arsalan', 'Furnishing', 464),
('Artesia', 'Furnishing', 465),
('Atam', 'Furnishing', 466),
('Craft art India', 'Furnishing', 467),
('Dolphy', 'Furnishing', 468),
('Zuniq', 'Furnishing', 469),
('Zoom Ktichenware', 'Furnishing', 470),
('Woodsmith', 'Furnishing', 471),
('Vincy', 'Furnishing', 472),
('TLS', 'Furnishing', 473),
('Sterling', 'Furnishing', 474),
('SK Metal', 'Furnishing', 475),
('Tibros', 'Furnishing', 476),
('Sygo', 'Furnishing', 477),
('Sens', 'Furnishing', 478),
('Nitraa', 'Furnishing', 479),
('Nilkamal', 'Furnishing', 480),
('Mavi', 'Furnishing', 481),
('Plenzo', 'Furnishing', 482),
('Anmol', 'Furnishing', 483),
('Blomus', 'Furnishing', 484),
('Chrome', 'Furnishing', 485),
('Dragon', 'Furnishing', 486),
('Ezzi Deals', 'Furnishing', 487),
('Glitter', 'Furnishing', 488),
('Gran', 'Furnishing', 489),
('JVG', 'Furnishing', 490),
('Kawachi', 'Furnishing', 491),
('Kmwanmol', 'Furnishing', 492),
('Lovato', 'Furnishing', 493),
('Maxxlite', 'Furnishing', 494),
('Mog', 'Furnishing', 495),
('Pindia', 'Furnishing', 496),
('Vasnam', 'Furnishing', 497),
('Vladiva', 'Furnishing', 498),
('Belmun', 'Furnishing', 499),
('Hubberholme', 'Furnishing', 500),
('M&G', 'Furnishing', 501),
('Marwar', 'Furnishing', 502),
('Taaza Garam', 'Furnishing', 503),
('Yingha', 'Furnishing', 504),
('Oceanstar', 'Furnishing', 505),
('Spectrum', 'Furnishing', 506),
('Zenith', 'Furnishing', 507),
('Elan', 'Furnishing', 508),
('Bergner', 'Furnishing', 509),
('BTL', 'Furnishing', 510),
('CBM', 'Furnishing', 511),
('Chef\'n', 'Furnishing', 512),
('Clytinus', 'Furnishing', 513),
('Cocina', 'Furnishing', 514),
('Dazzle', 'Furnishing', 515),
('Disha', 'Furnishing', 516),
('Dolin', 'Furnishing', 517),
('Doyours', 'Furnishing', 518),
('Emerald', 'Furnishing', 519),
('Emsa', 'Furnishing', 520),
('Eoan International', 'Furnishing', 521),
('Erbanize', 'Furnishing', 522),
('Hanbao', 'Furnishing', 523),
('JB', 'Furnishing', 524),
('Hokipo', 'Furnishing', 525),
('Royal Oak', 'Furnishing', 526),
('Jivan', 'Furnishing', 527),
('Evok', 'Furnishing', 528),
('Maharaja', 'Furnishing', 529),
('Saffron', 'Furnishing', 530),
('RBJ', 'Furnishing', 531),
('Picnic', 'Furnishing', 532),
('SGB', 'Furnishing', 533),
('Skys&Ray', 'Furnishing', 534),
('Slivar Bell', 'Furnishing', 535),
('Steels', 'Furnishing', 536),
('Swastik', 'Furnishing', 537),
('Tesco', 'Furnishing', 538),
('Tinny', 'Furnishing', 539),
('TLS', 'Furnishing', 540),
('Umbra', 'Furnishing', 541),
('VCK', 'Furnishing', 542),
('Zecado', 'Furnishing', 543),
('Piyestra', 'Furnishing', 544),
('Zuari', 'Furnishing', 545),
('StyleSpa', 'Furnishing', 546),
('Nesta Furniture', 'Furnishing', 547),
('Durian', 'Furnishing', 548),
('ISC', 'Furnishing', 549),
('Jangir', 'Furnishing', 550),
('D K arts', 'Furnishing', 551),
('Cubit Homes', 'Furnishing', 552),
('Eros', 'Furnishing', 553),
('Furnitech', 'Furnishing', 554),
('Loxia', 'Furnishing', 555),
('Mubeel', 'Furnishing', 556),
('Orange Tree', 'Furnishing', 557),
('SS Modulars', 'Furnishing', 558),
('MCT', 'Furnishing', 559),
('Crystal', 'Furnishing', 560),
('Pigeon', 'Furnishing', 561),
('Shekawati', 'Furnishing', 562),
('Frunstyl', 'Furnishing', 563),
('Kingscrafts', 'Furnishing', 564),
('Purpledip', 'Furnishing', 565),
('Cotton Swings', 'Furnishing', 566),
('D-Swing', 'Furnishing', 567),
('Slack Jack', 'Furnishing', 568),
('Younique', 'Furnishing', 569),
('Spring Fit', 'Furnishing', 570),
('Decorhand', 'Furnishing', 571),
('ACW', 'Furnishing', 572),
('Swayam', 'Furnishing', 573),
('Dekor World', 'Furnishing', 574),
('Azaani', 'Furnishing', 575),
('Bansal Yarn', 'Furnishing', 577),
('Bianca', 'Furnishing', 578),
('Drape', 'Furnishing', 579),
('Eyda', 'Furnishing', 580),
('FabBig', 'Furnishing', 581),
('Desire', 'Furnishing', 582),
('K-Star', 'Furnishing', 583),
('Kabir', 'Furnishing', 584),
('Zyne', 'Furnishing', 585),
('Zikrak Exim', 'Furnishing', 586),
('Vahra', 'Furnishing', 587),
('Kwality', 'Furnishing', 588),
('Mitra', 'Furnishing', 589),
('Novelty', 'Furnishing', 590),
('Presto', 'Furnishing', 591);

-- --------------------------------------------------------

--
-- Table structure for table `builder`
--

CREATE TABLE `builder` (
  `id` varchar(200) NOT NULL COMMENT 'Name',
  `name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `email` varchar(200) DEFAULT NULL COMMENT 'Email ID',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL',
  `about_developer` text COMMENT 'About Developer',
  `logo` varchar(200) DEFAULT NULL COMMENT 'Logo',
  `address` bigint(20) UNSIGNED NOT NULL COMMENT 'Address',
  `associate` int(11) NOT NULL DEFAULT '0' COMMENT 'Associate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `builder`
--

INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('7 Hills Properties', NULL, 'info@7hillsproperty.com', 'info@7hillsproperty.com', 'Established 2007 in 7hills group is well known brand entity in the real estate sector of the country. 7hills has a dream of proffering residential and commercial spaces according to the emerging needs of the real estate buyers. Taking a vision of providing \"QUALITY\" into real estate development comprising with the prime key areas of progression into the residential, commercial and retail sectors. The company has achieved grand success by providing the quality products to all types of clients. 7 hills properties is one of Bangalore\'s premier real estate developers, has been changing the landscape of the city since 2007, it has built a dedicated reputation for quality. For its loyal customer today, this focus on quality represents s far-reaching vision that shines through every 7hills property. \r\n\r\n7hills properties focused on residential and contractual projects. Client satisfaction is the vision and the mission of 7 hills properties and the company strongly believes \"Client is king\". The company has always followed a policy of taking up development only on properties that have clear, litigation-free titles, with every aspect thoroughly vetted by its legal department. The department also ensures that every project has all required approvals from the urban development bodies. A dedicated team of professionals comprising architects, civil and structural engineers, landscape specialist etc.Then take up the responsibility of ensuring ecofriendly spaces, optimization of resources and timely delivery every time. The result has been a reputation for quality that has spread far and wide through word of mouth and the recommendation of satisfied customers. \r\n\r\n7hills properties look into the benefits towards the customers as the investment for them to give a profit in future. Where in the plots which will be purchased by Customers from 7hills properties is profit oriented. We look into the returns to the customers when they are investing in there asset.', 'Builder logo2015-12-11 15:45:28.svg', 475, 1),
('Aakruthi Developers', '', 'sales@aakruthidevelopers.in', 'http://aakruthidevelopers.in/', 'The team behind Aakruthi Developers has been involved in the construction industry for over two decades, catering to customers in Hyderabad, Bangalore and Chennai. With this vast experience and expertise, coupled with the motivation from the fantastic and humbling response to our previous projects, we take utmost care in delivering each project on-time and as promised.\r\n\r\nThe firm foundation of Aakruthi Developers comes from its founder Mr. A. Ashok Reddy, who has been in the field of construction for the past three decades. A man of exceptional experience, a keen sense of aesthetics and attention to detail, he is the driving force behind Aakruthi.\r\n', 'no logo2015-12-11 10:44:18.svg', 465, 1),
('Aakruti Group', NULL, 'info.amity@aakrutigroup.com', 'http://www.aakrutigroup.com/index.html', 'Aakruti Group was started on 26 Jan 1992 as civil contractors to various builders. The founder members of this company were the industrious Mr. Manilal Patel and the enterprising Mr. Vithal Patel.\r\nToday it is a construction company consisting of six determined directors.\r\n\r\nThe millennium year 2000 saw Aakruti in a new light. Their rapid rise to success can only be attributed to the knowledge and wisdom gleaned from their elders, experiences that were involved in a similar line of work.\r\n\r\nIt was the realization of their dream when Aakruti Group started their own real estate developments. And since then these six directors, along with their team, have moved from one milestone to another with the practiced ease of an experienced entrepreneur with strong values and traditions as their foundations to success and achievement.', 'Builder logo2015-12-11 15:48:08.svg', 476, 0),
('Aashrayaa Projects', NULL, 'enquiry@aashrayaaprojects.com', 'http://www.aashrayaaprojects.com/', 'The Aashrayaa Group was set up in 2006 in Bangalore, the Silicon City of Asia, to meet the growing demand for quality housing in Bangalore. Heading this Group by two committed individuals Mr M Padmanabha Reddy and Mr D Raja Gopal. Their vision, their dedication and their forward-looking ideas have shaped the Group and taken it to new heights.\r\nIt has been a glorious journey for the Aashrayaa Group, the name behind many reputed projects in Bangalore and residential layouts with all modern amenities, residential apartments and villas, commercial complexes? we have done it all and done it sucessfully, without any compromises on quality. No wonder then, that the Aashrayaa Group is today synonymous with trustworthiness, superior quality, strategic location and delivering on its promises ? again and again. The projects that come under the Aashrayaa Group are a reflection of the Group?s commitment to provide customers and stakeholders full value for their money.\r\n\r\nAt the Aashrayaa group, we believe that people are our greatest assets. Which is why, we have consistently towards building a strong pool of talent, over the year. Our constantly growing team consist of competent professionals include highly qualified engineers, architects, legal experts, chartered accountants and a strong customer-driven sales and marketing team. We also have dedicated teams for land acquisition, development and administration. Our team work in perfect synergy and share the group?s vision to make quality housing affordable to all.\r\n\r\nSince its inception, the Aashrayaa Group has worked with values that we hold close to our heart. In an industry fraught with ethical challenges, we have worked with integrity. Fair business practices, transparent dealings and clear documentation have meant that every person we partner with, whether in buying land or selling it, are assured of a good and profitable deal.', 'Builder logo2015-12-12 05:51:19.svg', 477, 0),
('Aashrithaa Properties Pvt. Ltd', NULL, 'info@aashrithaa.com', 'https://aashrithaa.com/home.html', 'Aashrithaa Properties is a vision driven organization to offer value at the core of its business. Aashrithaa had its humble beginnings in 2004 under the visionary zeal of Mr. Murali Reddy to redefine the experience of buying sites for all.\r\n\r\nThe organization forayed into the real estate segment with couple of home project which was an instant hit. Highly motivated with the response and better equipped to handle bigger challenges, it has planned its growth from strength to strength. Aashrithaa grew with its sheer dedication to quality delivered. It prides itself on an organic growth plummeted by word of mouth credentials and the unstinting support of its customers in the development of the organization.\r\n\r\nAashrithaa properties have been synonymous with innovation in ideas, integrity in development, dedication to details, execution excellence and prompt deliveries. It has a range of offerings across flourishing geographies in and around Bangalore.\r\n\r\nAashrithaa properties has over 1.5 million sq ft development to its credit and an acquired land base of more than 1.5 million sq ft spread across Bangalore periphery and suburban city areas to ride the next crest of success for all its stakeholders. This vision driven organization thus, in a bid to promote value for all has reinvented itself; Aashrithaa is now driven by its core service of a new focused and unified offering of value plots only. \r\n', 'Builder logo2015-12-12 05:53:39.svg', 478, 0),
('Accent Properties Builders & Developer', NULL, 'contact@ascentbuilders.in', 'http://ascentbuilders.in/', 'Ascent Builders (India) Pvt. Ltd is one of the leading name embarked in construction and the Real Estate operations zone, synonymous in the country and beyond providing the right blend of service quite efficiently, professionally acclaimed for all-round excellence and unparalleled levels of service and its success story spanned across decades and continues to achieve higher goals set in relentlessly for quality performance and service in the diverse fields of construction contracting and the realty business as well, winds of changes are in place as master minded many construction projects and on the Real Estate development fronts too, and it\'s really a force to reckon with all the way. ', 'Builder logo2015-12-12 05:55:57.svg', 479, 0),
('Adarsh Developers', NULL, 'marketing@adarshdevelopers.com', 'http://www.adarshdevelopers.com/', 'Bangalore\'s premier real estate developer for the past 25 years, with expertise in Residential, Commercial, Hospitality and SEZ developments. Bricks build houses but we build homes. Dream homes. Your signature homes. From quality to Return on Investment, lifestyle to customer satisfaction - we are honestly unrivalled. Bought with eagerness, lived in with pride and sold out by word of mouth.', 'Builder logo2015-12-12 05:57:53.svg', 480, 0),
('Adhvaytha Developers ', NULL, 'info@adhvaythadevelopers.com', 'http://www.adhvaythadevelopers.com/', 'In 2012, a group of technocrats in India and overseas together to establish Adhvaytha Developers in Bangalore, the Silicon Valley of Asia. In a short span of time, this forward-looking company has carved a niche for itself in the demanding and dynamic real estate industry.\r\n\r\nPassionate about quality, we at the Adhvaytha Developers believe in giving our customers the kind of living spaces that we would like to live in ourselves. Which is why a great deal of thought goes into planning every single project that we undertake, right from identifying the perfect location to the world-class amenities we offer. Among other things we deal in plot for sale in Bangalore, villa for sale in Bangalore.', 'Builder logo2015-12-12 06:00:04.svg', 481, 0),
('Adithi Builders Pvt. Ltd', NULL, 'sathish@adithibuilders.in', 'http://adithibuilders.in/', 'Adithi Builders Pvt. Ltd. is one of the fastest growing developers in Bangalore with developments spanning across Residential, Commercial. Over a period of a decade we have completed 07 Projects spanning a total developed area of over 4 Lakhs Sq.ft., Adithi has 2 ongoing projects comprising around 2.5 Lakh Sq.ft., & 02 upcoming projects, totaling 3 Lakh Sq.ft., which include Apartment enclaves, Commercial Complexes. \r\n\r\nThe Founder & CMD, Mr. Satish Reddy , An MBA. Graduate from Hyderabad who believes in giving Quality, latest designs, timely delivery, affordable and excellent value for the projects.\r\n\r\nVision\r\nTo provide the highest value to our clients, customers and community with a critical eye towards Quality, Safety, Service and Budget.\r\n\r\nMission\r\nTo deliver exceptional services to the construction industry and the clients, by providing the highest level of professionalism, service response and workmanship.', 'Builder logo2015-12-12 06:02:47.svg', 482, 0),
('Adithya Construction', NULL, '', 'http://www.adithyaconstructions.com/', 'We believe that our customers want a good reliable service and therefore try to ensure that the project is well planned and prepared before commencement of work. When using sub-contractors we make sure that they follow our ethos as far as possible. Our unswerving commitment to quality and time are the hallmark of all our endeavors.\r\n\r\nWe are in this field since 2003 and constructed several projects. Our prime goal is customer satiety, that\'s why we give utmost importance to customers\' rupee. With this motto we are honing our skills day by day with customer feed back and delivering the projects within the due course of time.\r\n\r\nThe company is well established with qualified and experienced architects, structural engineers, civil engineers, marketing executives and all other consultants / professionals.\r\n\r\nWhile excelling in building magnificent structures we also build everlasting relationships with customers. Our expertise and commitment have gained us a goodwill that we cherish to preserve and enhance.', 'Builder logo2015-12-12 06:04:10.svg', 483, 0),
('ADL Elite Housing Projects', NULL, 'sales@livespacess.com', 'http://adlelitehousingprojects.com/', 'ADL Sunshine, headquartered in Bangalore and primarily focused on residential, commercial and contractual projects.\r\n\r\nOur residential projects include extravagantapartments, villas, row houses, and plotted development complete with foremost amenities.\r\n\r\nWe strongly emphasis and promote on environmental management, water harvesting and high safety standards in all our residential endeavors.\r\n\r\nIncepted since 10 years, we have endeavored for excellence in quality, customer centric approach, value for money, pioneering engineering, uncompromising business ethics, timeless values and transparency in all spheres of business conduct transforming ADL Sunshine a preferred real estate trust worthy brand.\r\n\r\nProject Execution and Project Management capability is one of our core strengths. As of March 30th 2014, ADL Sunshine has completed 50 construction projects inclusive of both residential and commercial.\r\n\r\nThe Company currently has 2 ongoing residential projects aggregating to 38 thousand square feet of developable area. ADL Sunshine has made a footprint in multiple parts of the city in Bengaluru.\r\n\r\nWe warmly invite you to explore our world: visit our projects, meet our people, discover how we operate & deliver and what we have achieved. This website, though large, is easy to navigate intuitively. If you need specific direction, you might find it useful to refer to the site map or \'footer\' at the bottom of the page.', 'Builder logo2015-12-12 06:06:09.svg', 484, 0),
('Adonai Shelters Pvt. Ltd', NULL, 'adonaishelterspvtltd@yahoo.com', 'http://www.adonaishelters.com/', 'Adonai Shelters, one man\'s dream was founded by Mr. Balakrishnan, a Civil Engineer and a seasoned professionals with a wealth of experience of over a decade in property development. He has delivered on time varied projects and has carved a niche for his company in the building industry and in customer satisfaction. His company\'s professionalism is reflected in its diverse workforce where divisions are headed by professionals in their own entity. \r\nWe take pride in thanking every clients has bought from us a home to cherish', 'Builder logo2015-12-12 06:08:33.svg', 485, 0),
('Aesthetic Constructions', NULL, 'info.ac@aestheticgroup.in', 'http://www.aestheticgroup.in/', 'esthetic Constructions is a fast developing and proficient company engaged in constructions and development of residential and commercial projects. We have been practicing this business since last ten years and with our innovative business practices and growing experience we have earned enormous respect and trust from our Clients and Business Associates. \r\n\r\nThrough our dedicated team members and committed services we have been successful in bringing smile of satisfaction and happiness to the faces of our clients and shall strive to continue the same. ', 'Builder logo2015-12-12 06:10:05.svg', 486, 0),
('Aisshwarya Group', NULL, 'info@aisshwaryagroup.in', 'http://www.aisshwaryagroup.in/', '', 'Builder logo2015-12-12 06:16:36.svg', 487, 0),
('Aithena Properties', NULL, 'info@aithena.com', 'http://www.aithena.com/', 'Aithena is a premier Enabled Services Company which specializes into Real Estate Marketing, Consultation, and Construction & Interiors. Based in Bangalore, we are one of the fastest growing Real Estate Company in the city.\r\n\r\nAithena started its operation from year 2009 and has undertaken mission critical projects and product development on global and Indian clients to their complete satisfaction. We place great value on relationships, which is an integral part of our work culture and believe in collaborative growth. We have provided extensive services to industry in the Real Estate field since our inception. We act as an service provider and assist organizations to become more efficient through effective deployment of Property solutions, The company has many future planning in which we wish to spread our horizon in other areas.\r\n\r\nAithena advocates industry standards for responsible marketing ? both online and offline, promotes relevance as the key to reaching consumers with desirable offers, and provides cutting-edge research of all best of Real Estate Projects, Such as Villas, Apartments, Commercial, Gated community Projects & Approved Layouts with best of all aspects (Such as Pricing of the property, Legalalities, location, ROI etc., expertise also lies in property advisories with care and we also welcome opportunities to improve results throughout the end-to-end Property services.', 'Builder logo2015-12-12 06:18:10.svg', 488, 0),
('Ajmal Estate & Properties Pvt. Ltd', NULL, 'info@ajmalblr.net', 'http://www.ajmalproperties.com/', 'he name Ajmal in Arabic means most beautiful and this name has transpired and captivated the perfume industry across Middle East and beyond, for the past 60 years.The Group for 60 years was founded by Haji Ajmal Ali, a visionary and an achiever in the truest sense of the word. It was the vision and perseverance of the Late Haji Ajmal Ali, that today the brand stands strong as a multi-million dollar corporation with over 135 signature outlets across 8 countries and a distribution network that spans across 24 countries.\r\n\r\nHaji Ajmal Ali started his humble and yet remarkable journey way back in 1951 from a remote village in Assam. His priority in life was not driven by commercial success but the upliftment of the society, and his philanthropic activities like health care, education reforms, rehabilitation of widows and orphanages. One of the golden moments of his life was establishing a 300 bed hospital which was inaugurated by Mother Teresa.\r\n\r\nAjmal\'s have many business verticals as large corporate do. The group has diversified business activities that include Perfumery, Agriculture, Textile, Leather, Health Care, Education and REAl ESTATE and one of them is Ajmal. Established in 1992 in Dubai, Ajmal\'s Real Estate wing has three running projects in Dubai. These projects have bear the Ajmal signature of finest quality, supreme precision, premium construction and optimal customer satisfaction.\r\n\r\nToday, apart from creating some of the finest fragrances, Ajmal Group is embarking into real estate sector in India. All these projects will bear the Ajmal signature of finest quality, supreme precision, premium construction and optimal customer satisfaction, As is the practice in perfume trade.\r\n\r\nHaving made a mark in the Perfume Industry Ajmal has now decided to spread its goodwill and values into Real estate sector in India. Ajmal initiatives go back to 1995 when it started by acquiring land in the outskirts of Delhi, Mumbai ,Bangalore and our native place Guwahati in Assam.\r\n\r\nApart from acquiring vast knowledge in this field the group also boasts of its association with some of the reputed builders as strategic partners and investors. Ajmal group is also the member of CREDAI and INDIAN GREEN BUILDING COUNCIL.\r\n\r\nAll along we have been strategically involved with some of the top most builders of India & particularly in Bangalore.', 'Builder logo2015-12-12 06:20:23.svg', 489, 0),
('Ajmera Housing Corporation', NULL, 'info@ajmera.com', 'http://www.ajmera.com/', 'Having established itself over 47 beautiful years, Ajmera Group is recognized as one of India\'s leading Real Estate Company. The reach and interest of the Ajmera Group has only grown with time and has extended to other realms, such as solar power, sports, vaults security, cement and social welfare. The company now has a strong presence in and around Mumbai, Pune, Ahmedabad, Surat, Rajkot and Bangalore as well as an international project in Bahrain.\r\n\r\nQuality, innovative construction technology, comfort, aesthetic appeal and maximum value for your money are few attributes that enrich Ajmera with the belief that people have entrusted in them.', 'Builder logo2015-12-12 06:22:02.svg', 490, 0),
('Akshaya Estates', NULL, 'sales@clickhomez.com', 'http://www.akshayaestates.in/', '\r\nBuilt to deliver more ? Akshaya Quietlands\r\n\r\nAt Akshaya, we believe in touching lives and fulfilling dreams. We understand exactly what people want of their urban lifestyle? the location, the outlook and the city convenience. And to this end, we bring to you Akshaya Quietlands.\r\n\r\nAkshaya Quietlands is all that dreams are made of and much more. By including cutting-edge urban renewal processes that promote sustainability and well being, yet keeping the layout as close to nature as possible, that?s Akshaya Quietlands. The project includes 13 parks, which is the first-of-its-kind architectural marvel. The project also includes well laid out avenue roads, tree-lined jogging and walking tracks, an eco-trail, an expansive clubhouse and of course, an enviable lifestyle.\r\n', 'Builder logo2015-12-12 06:23:31.svg', 491, 0),
('Alisha Projects Pvt. Ltd', NULL, 'info@alishaprojects.com', 'http://www.alishaprojects.com/', 'Alisha Projects Pvt Ltd Bangalore has been established by the team of Ex-Defense personnel to provide complete property management services to Defense personnel and community in general. With the tremendous population increase over the last three decades and the continuing growth, there is a high demand for all types of property infrastructure especially for housing. It is Alisha Projects Pvt Ltd\'s interest to professionally respond to this demand by initially customizing affordable housing, setting an excellent role model for all its competitors.\r\n\r\nWe are dedicated to representing the customer\'s best interests in the negotiation of real estate transactions. We want to develop a long term relationship with all the customers and fellow business associates.', 'Builder logo2015-12-12 06:25:47.svg', 492, 0),
('ALPS Prime Spaces Pvt. Ltd', NULL, 'info@alpsprime.com', 'http://www.alpsprime.com/', 'OUR PHILOSOPHY\r\nWe at ALPS believe that Customer satisfaction and loyalty are the primary parameters in measuring our business performance. Stringent adherence to quality systems and processes across every stage has helped us setbenchmark standards over the last 10 years. Fair practices, transparency and uncompromising professionalism have helped us become what we are today. The ALPS corporate ethos embraces practices and principles that ensure high standards of construction and Integrity across all our projects.', 'Builder logo2015-12-12 06:27:09.svg', 493, 0),
('Ambiience Projects ', NULL, 'info@ambiienceprojects.comq', 'http://www.ambiienceprojects.com/', 'Established in 2012, commenced its operations with a group of plot developers. Riding on the growth wave of real-estate, the group made a strategic shift /took deviation into an independently formation of layout', 'Builder logo2015-12-12 06:28:50.svg', 494, 0),
('Amigo Shelters Pvt. Ltd.', NULL, 'info@amigoshelters.com', 'http://www.amigoshelters.com/index.html', 'Amigo Shelters Private Limited is a Bangalore based real estate enterprise, started in 1998 and driven by belief in building aspirations and fulfiling them. This is magnified in the 5 million sq.ft. that has developed and sold till date. Indeed, customers with fulfilled aspirations across thirty one projects is a paradigm in itself.\r\n\r\nAmigo Shelters Private Limited, designs and develops quality living spaces and operates in niche residential segments and apartments in Bangalore.\r\n\r\nAmigo Shelters Private Limited, is run by a young management team. It is on an aggressive growth path with expansion plans in Bangalore. The company holds high quality construction as its mission and vision. The company pride itself on its ability to consistently provide the best in terms of quality, perfection and unique customer relationship management.', 'Builder logo2015-12-12 06:30:55.svg', 495, 0),
('Anjan Citicon Pvt. Ltd ', NULL, 'contact@anjanciticon.com', 'http://anjanciticon.com/', 'Anjan Citcon is a construction company head quartered in Bangalore. Anjan?s main line is residential apartments and large complexes. Having one decade of experience in constructing Housing projects, Anjan CitiCon was founded by group of enthusiast builders. We come across with bunch of tech savvy people looking for their dream home, but couldn?t find one which is affordable. Thus the idea of Anjan Citi Constructions!Our key competitive strength is affordable eco homes which are energy efficient, luxurious and comply 100% with vastu. We ensure that materials of the highest quality are obtained from the most economical source. Our comparative advantage relies precisely on our ability to produce an uncompromising and impeccable finished quality of work. It certainly implies we offer the best quality for money. At the same time we ensure not to sacrifice quality for the sake of cost cutting.Anjan believes in placing the customer first and foremost. In spite of large nature of our projects, our management team maintains a constant contact with our clients. By avoiding the bureaucratic hierarchy we eliminate the delays and ensure each client?s needs and wants are met promptly.We believe that in order to take, you must give and at Anjan we like to give our customers by ensuring that we build safe, clean and environment-friendly homes.Our main focus is on residential section at present and looking forward to building environment friendly hospitals, IT parks and educational institutions across India.', 'Builder logo2015-12-12 06:33:01.svg', 496, 0),
('Annciya Estates', NULL, 'annciyaesates@gmail.com', 'http://www.annciyaestates.com/', 'Annciya Estates is a vision driven organization offering value at the core of its business. with over 40+ years, Annciya Estates had its humble beginnings from plantations to Real Estate Development under the visionary Leadership of Mr. Jose Augustine with a mission to redefine the experience of buying sites for all. \r\n\r\nThe organization forayed into the real estate segment in the year 2006 with a couple of projects which was an instant hit. Highly motivated with the response and better equipped to handle bigger challenges, it has planned its growth from strength to strength. Annciya Estates grew with its sheer dedication to quality delivered. It prides itself on an organic growth plummeted by word of mouth credentials and the unstinting support of its customers in the development of the organization. \r\n\r\nAnnciya Estates have been synonymous with innovation in ideas, integrity in development, dedication to details, execution excellence and prompt deliveries. It has a range of offerings across flourishing geographies in Bangalore periphery and suburban city areas. This vision driven organization thus, in a bid to promote value for all has reinvented itself; Annciya Estates is now driven by its core service of a new focused and unified offering of value plots only.', 'Builder logo2015-12-12 06:34:22.svg', 497, 0),
('Apranje Estates Limited', NULL, 'info@apranje.com', 'http://www.apranjeestates.com/', 'Apranje Estates Ltd. was launched on 29th Apr 2008, as a company with interest in real estate development and affiliated services.\r\n\r\nThe flagship entity of our group is Apranje Jewellers, a provider of premium jewelry in diamonds and precious gems. The firm has been operational since 1961.\r\n\r\nOn 17th Dec 2013, the company got into a partnership with M/s. Elite Properties to develop gated layouts in Bengaluru especially focusing on the newly emerging Northern Suburbs. Elite properties already had five projects completed to their credit.\r\n\r\nThe company has also launched several residential projects in premium residential areas in Bengaluru, viz. Defence Colony, Koramangala, Nandidurg Road, Indiranagar and Varthur.\r\n\r\nAEPL was converted to an unlisted Public Ltd. company on 21st Jan 2015.', 'Builder logo2015-12-12 06:35:45.svg', 498, 0),
('Architha Developers', NULL, 'archithabuilders@gmail.com', 'http://archithadevelopers.com/', ' Architha Developers, Bangalore\r\n\r\nIn Bangalore?s residential developer?s scenario, an upcoming entity, Architha Developers is a convergence of capability in the property development business. The driving force behind Architha Developers is Mr. G.Krishna Kumar, who has founded Architha, having years of experience in the construction activities. Architha mainly focused on residential and commercial projects with quality construction. Company always strived for benchmark quality, it has time and again been acknowledged for the quality of its projects.\r\nMr. G.Krishna Kumar a qualified civil engineer got 25 years of experience in the construction activities. He was rendering his services in Middle East and later he undertook the construction of many residential and commercial projects in Bangalore since 1996. He established a firm in the year 2004 and incorporated a limited company in 2005, since then layed emphasis on values and ethical practice with a firm belief in individual trust worthiness and organisational trust.\r\n', 'Builder logo2015-12-12 06:37:26.svg', 499, 0),
('Ardente Realtors ', NULL, 'sales.pinegrove@ardenterealtors.com', 'http://ardente.in/', 'Young and driven by a rare passion, Kranti Kiran and Ravi Kiran come from a family with a rich Real Estate legacy and experience of over 30 years in building homes. They have delivered 28,000 homes covering more than 100 million sft. With building quality homes in their DNA, they were committed to build a professional and dynamic company. They founded Ardente Realtors to realise this shared dream of theirs.\r\n\r\nBoth the founding brothers completed their higher education in the US and had the opportunity to get a ringside view of frontline Real Estate industry of international repute. With a spirit to develop high-end living communities with quality homes and ensure impeccable customer service to clients in buying their homes, the co-founders have come together with a Rich Residential and Commercial Real Estate experience from their previous endeavors to build Ardente Realtors.\r\n\r\nWhile drawing a rich and invaluable experience from their parent flagship, Janapriya, a brand that is a household name in erstwhile Andhra Pradesh, now Telangana and Seemandhra and a `Gurukul\' where they learnt the basics block by block, Kranti and Ravi have a unique vision of the future where high-end technology gives Ardente an edge over other real estate players.\r\n\r\nArdente as a group believes that everyone is specialized in some unique ways. It is necessary to bring the best in class together to create products that stand apart. To achieve this, Ardente has tied up with renowned international firms like RSP for architecture, CBRE for customer experience & support and RAMBOLL, USA for structural designs. Ardente looks forward to being associated with other top firms to deliver quality projects as well.\r\n\r\nKranti & Ravi envision Ardente as a professionally managed Company partnering with the best firms in the industry to deliver great Customer experience from sale to handover, to ensure high quality standards in construction and timely delivery. Ardente has been formed to emphasize and achieve different objectives than the mother company: Ardente is a Company formed to achieve a set of objectives different from Janapriya, its mother company.\r\n', 'Builder logo2015-12-12 06:39:34.svg', 500, 0),
('Artha', NULL, 'suresh@artha.in', 'http://www.arthaproperty.com/', '', 'Builder logo2015-12-12 06:41:52.svg', 501, 0),
('Arvind Builders', NULL, 'support@arvindconstruction.com', 'http://www.arvindconstruction.com/', 'Arvind Construction is an ISO 9001:2008 certified company, having rich experience as an integrated Service supply of Project consulting, Engineering Design, Construction Management and contracting for Domestic construction projects. Our experts will offer technology and Market operation-oriented solutions and services to Investors, Governmental Organization and multinational companies in Industrial and Civil Project Planning, Investment consulting and whole process of Engineering Construction such as for Urban Planning, Industrial Park Planning and the Engineering and construction of various Industrial and Public Building, Office Building, Hotels, Residential Development, Hospitals, Commercial Development and Schools and Universities. We are committed to thoughtful service of masterful technology and high quality for the Customers.', 'Builder logo2015-12-12 06:43:15.svg', 502, 0),
('Aryan Hometec Pvt. Ltd', NULL, 'info@aryanhometec.co.in', 'http://aryanhometec.co.in/', 'Aryan Hometec are a dedicated, experienced and competent team of construction professionals. We the \"Aryan family\" are strongly bound with a string of trust, commitment and unfailing support.\r\n\r\nWhen we promise Quality, what we are actually assuring you of is world class quality in terms of design and architecture, top notch quality of construction materials, fittings and fixtures for a betted quality of life that you will find irresistible.\r\n\r\nVision\r\nTo establish and mark our existence in the field of construction through our technical expertise, infallible support and consistent delivery.\r\n\r\nMission\r\nOur mission is to absolutely value and assay customer needs and to materialize the same by delivering the most practical, safe and high quality building solutions at affordable prices.', 'Builder logo2015-12-12 06:45:01.svg', 503, 0),
('Ashirwaadh Builders And Developers ', NULL, 'info@ashirwaadhbuilders.com', 'http://www.ashirwaadhbuilders.com/', 'Ashirwaadh builders and developers is an established name among the residential builders in Bangalore. Since our inception in 2011 the company has been steadily completing projects on time and has a strong track record of ensuring high levels of customer. Today we are one of the leading developers in South Bangalore.\r\n\r\nAt Ashirwaadh, every project that comes our way is treated most carefully. We believe that as one of the leading builders it is not only enough to build homes, but fulfill every need of our customer. We have a team that comprises some of the best engineers and architects in the industry who share the same goals as the company and are committed to providing quality homes.', 'Builder logo2015-12-12 06:46:40.svg', 504, 0),
('Ashoka Property Developers', NULL, 'enquiry@ashokaproperties.com', 'http://www.ashokaproperties.com/', 'Ashoka Developers are one among the fastest growing real estate development companies in Bangalore. Ashoka Developers was launched by seasoned entrepreneurial with extensive experience in the Indian reality sector with the vision of facilitating the best in class properties to you. Right from inception, we create a real estate standard that reflects uncompromised quality, functional innovation, transperancy and flexlibility.', 'Builder logo2015-12-12 06:51:01.svg', 505, 0),
('ASN Ventures', NULL, 'marketing@asnventure.com', 'http://www.asnventure.com/', 'ASN Ventures, a company known for developing apartments that displays the best styling and utility features. You can customize your home to your taste in life and living. ASN Ventures gives you the freedom and the right fittings.\r\n\r\nProjects developed by the ASN ventures have in them the best modern amenities, placed splendidly in unpolluted, Secure and surrounded by the all facilities that bring about a perfect balance of luxury and life.\r\n\r\nWe seed quality brick by brick; every element within each apartment is created with utmost care and passion with openings for ample natural light and air.', 'Builder logo2015-12-12 06:53:20.svg', 506, 0),
('Asset Builders', NULL, 'mail@assetbuilders.in', 'http://www.assetbuilders.in/', 'Mission\r\nIt was during Mr. Rahimanas tenure at Sami Labs that he came across a very alarming statistics relating to the real estate sector particularly in Bangalore. The statistics was from pending consumer court cases pertaining to the real estate sector. The major reasons for the cases were (1) Delay in possession (2) Fraud (3) Litigation (4) Alteration in property (unit allotted) (5) Alteration in Plan & (6) Improper documentation.\r\n\r\nHe was aghast at the state of affairs of the sector and the plight of the hapless home buyer who is neither aware of the subtle legal nuances of the field nor of the credibility of the developer. Mr. Rahiman saw a dire need to serve this class of innocent home buyers with credible housing solutions. This deep desire led to the formation of Asset Builders (a unit of Asset Handlers Pvt. Ltd.) The mission being to provide a lifelong hassle-free home ownership experience to all.\r\n\r\nThe journey so far has been slow and steady. Since this is unchartered territory it has its own ups and downs. But we have been successful nevertheless in our endeavour as is evident from the feedback of our esteemed clients, who have in the long run, become our extended family members. As we continue our journey ahead, we remain steadfast in the mission that guides all our endeavours which are Built on Ethics. Built to Last.?\r\n\r\nVision\r\nBangalore is one of the fastest growing cities in Asia, nearly bursting at it seams. This unforeseen and unprecedented growth has led to an inadequacy of infrastructural resources such as water and electricity and damaging environmental effects of pollution. Today the city is grappling with the issues on how to come to terms with these shortfalls and harmful effects, which leads us to the concept of green building.\r\n\r\nOf utmost concern is the need to conserve water and replenish the groundwater table which is depleting at an alarming pace all over the country. Rainwater harvesting, ground water recharge, recycling and re-using of water, generation of power from renewable sources, eco friendly building material and practices have all been religiously incorporated into our projects.\r\n\r\nWe have engaged a highly motivated team of experts to research into the latest developments in the construction technology. As a result we have employed a lot of path breaking initiatives towards sustainable development reaping not only short term benefits for the immediate user, but also long term rewards for generations to come.\r\n\r\nLooking ahead we have realised the need to protect mother earth for our progeny. Our vision is to contribute in a small way towards that by doing our bit in every endeavour we undertake.', 'Builder logo2015-12-12 06:56:05.svg', 507, 0),
('Assetz Property Group', NULL, 'enquiries@assetzproperty.com', 'http://www.assetzproperty.com/', 'At Assetz, we are driven not by the need to create more, but to design better. In an industry where design is often narrowed down to aesthetics, every component of our properties is led by design. Our focus on design in every detail enables us to create properties that are true assets for families and businesses that prosper in them.\r\nFounded in 2006, APG is primarily in the business of real estate development and business portfolio management with a secondary focus on infrastructure development in the Asia region. APG develops projects across commercial, residential and mixed-use asset classes. Besides our history of developing buildings to house many multinational commercial office tenants which has established the business with clear robust systems, transparent processes, and an eye for detail with a belief that conventions should be challenged, our focus has also been to elevate our customers? lifestyles.', 'Builder logo2015-12-12 06:58:53.svg', 508, 0),
('Astrum Value Homes Pvt. Ltd', NULL, 'crm.mysore@astrumhomes.com', 'http://www.astrumhomes.com/', 'Astrum Homes is an FDI funded India focused Real Estate Development Company, engaged in developing world-class residential solutions in emerging cities of the country. The company has been promoted by the management team of FIRE (First Indian Real Estate) Capital Fund in partnership with founders of leading North American real estate investment and development firms. FIRE Capital Fund is a pioneering Indian real estate centric private equity fund that has created several joint venture companies across the country to deliver a total built-up area of approximately 50 million sq. ft. Under the leadership of its Founder & CEO, Om Chaudhry, FIRE Capital Fund makes investments in SUVs ? development ventures of large format ? integrated Satellite Urban Villages, with residential and related verticals of mixed use, which go into creating self-contained communities. One of the leading promoters of Astrum Homes is Mr. Jorge Perez, Chairman of the Related Group, a group which over the last 30 years has created innovative residential developments that have dramatically changed South Florida?s urban landscape. The Related Group is United States? leading builder of luxury condominiums and the leading developer of multi-family residences with successful projects in other countries of North and South America and has to its credit more than 77,000 condominium and apartment residences today.', 'Builder logo2015-12-15 06:56:40.svg', 769, 0),
('Aswani Properties', NULL, 'enquiry@aswaniproperties.com', 'http://www.aswaniproperties.com/', 'The people behind Aswani Properties are first-generation entrepreneurs with a strong background in logistics and resource management. The core team has legacy exposure in real estate and project development backed by capability that ties in experience, expertise and high standards.\r\n\r\nOur main focus is on affordable housing and what makes us different is our approach to the customer. An approach based on transparency, implicit trust and true value. Any project developed by Aswani Properties has the reassurance of a strong financial base, backed by the necessary expertise, resources and project management systems.', 'Builder logo2015-12-12 07:00:29.svg', 509, 0),
('ATZ Properties', NULL, 'info@atzproperties.in', 'http://www.atzproperties.in/', 'ATZ properties are a leading Property Developers based in Bangalore and having its branches in Pune and Mumbai, the team consists a group of professional Engineers and Management graduates, with rich experience in the real estate business of over 20 years. Bangalore being a hub of real estate activity of the country, with the presence of large pool of techies, the company is focusing and are dedicated to developing and constructing the finest homes, offices , retail and hospitality space in all the locations of its presence in India, with breathtaking views of the gardens, environments and top class facilities on par with global standards.\r\n\r\nATZ Properties in a short span of time has made a mark as a well known developers and builders who are well known for quality construction, loyalty and commitment. Our prime goal is to provide you with a quality space in a good selection of prime locations.\r\n\r\nWe provide our clients with high standards of construction and we supply excellent quality of fittings and fixtures.In short, we provide you with a space that will make all of us proud and happy.\r\n\r\nWe continue to offer our services, advice and assistance years after possession of your new home, ATZ properties is a Bangalore real estate company that stands totally on the trust placed on it by its valued patrons\r\n\r\nATZ properties believes in offering its clients answers to most of the Frequently Asked Questions, which helps them in better decision making.\r\n\r\nBuilding a brand name & winning the trust of the urban Bangalorean by delivering \'perfect plots\'..that is what sets ATZ properties apart from other property dealers. ATZ properties is not just about developing land; it is about facilitating the common man realize his dream of an ideal home by providing end-to-end services in real estate.\r\n\r\nWe at ATZ properties realize the value of land & property. We consistently strive to surpass the customer\'s expectations. Each ATZ properties customer is a proud owner of property brilliantly landscaped - prime property, ideal living conditions, all basic amenities, complete ownership rights.\r\n\r\nDelivering what is promised has earned ATZ properties trust, popularity & a brand name in the real estate industry in India\'s Silicon Valley. We design land for homes & not merely houses. ATZ properties pampers its customers by anticipating needs & offering the best.\r\n\r\nBangalore Is booming - Great infrastructure projects are in the works! The Metro Rail project and the Ring Roads will provide the ultimate connectivity in this great city. These infrastructure projects are opening up real estate opportunities like never before. Real-estate prices had peaked and is now stabilizing - Let the Land master of Bangalore show you the way.', 'Builder logo2015-12-12 07:02:21.svg', 510, 0),
('August Ventures Pvt. Ltd', NULL, 'sales@august.in', 'http://www.august.in/', 'August Ventures Pvt. Ltd. is part of a business group with various interests. This company under this group will focus on property development. The company is based at Bangalore. August Park exhibits high level of quality construction, detailing, features and comforts.\r\n\r\nAugust Ventures aspires to carry forward the success of our earlier projects by specializing in quality constructions that are finished to a high standard and are suitable to refined tastes. Good workmanship, commitment to timelines, and a high level of features will be the hallmark of all our Developments.', 'Builder logo2015-12-12 07:07:19.svg', 511, 0),
('Aurelia Estates ', NULL, 'sales@aurelia-estates.com', 'http://pearlgroup.co/', 'The story of Pearl Group draws a parallel to the story of emerging India - an economic powerhouse that is increasingly growing in stature globally; and the soaring ambition of young professional Indians - new-age and globally aware, who seek nothing less than the best; at a price that doesn\'t cost the earth.\r\n\r\nFor more than a decade now, Pearl Group has been providing lifestyles at great value points - to this club of aspiring doers.\r\n\r\nWith the track record of more than half a million square feet of real estate, the Pearl Group also has strategic investments in hospitality and food retail. ', 'Builder logo2015-12-12 07:08:51.svg', 512, 0),
('AV Developers', NULL, 'info@avdevelopers.com', 'http://avdevelopers.com/', 'Vision\r\nWe are a dedicated team striving to bring growth to our community and assist our clients in making their dreams become a reality.\r\n\r\nMission\r\nWe strive to meet and exceed our client?s expectations to foster long term partnerships. We value our staff and create an enjoyable work environment which allows personal fulfillment that leads to a loyal and productive work force. We use value effective construction practices to deliver high-quality, cost-effective projects.', 'Builder logo2015-12-12 07:11:01.svg', 513, 0),
('AVS Villas', NULL, '', 'http://www.avsvillas.com/', 'AVS Group has created a brand by itself through trust, hard work and sincerity maintained for over more than two decades in the field of Construction, Road Contract Work, Infrastructure Developments, Ready mix Concrete to name a few, giving high quality construction and true value for the money, thus utilising our infrastructure and our experience in passing on the same trust to our valued customers through AVS Villas.\r\nAt AVS Villas, we have expertise, who identifies the land very much suitable in terms of Area, Price, Commutation, appreciation and the basic needs required for our customers to invest in our project. Here we take utmost care of the developments undertaken in our project and deliver the developments as committed on time and we have a very professional marketing team who takes care of your requirements and render very good service to our esteemed customers.\r\nTo name a few, we have completed high profile works like TAAL Airport Runway, Hosur Bus Stand, Nilkamal Industries, LUK India Ltd, Volvo, Honda Automobile Industries, Major road and bridge works, major Retail estasblishments , etc. in and around Tamil Nadu and Karnataka\r\nAt AVS Villas, buying a property becomes a reality and your investment is highly valued.', 'Builder logo2015-12-12 07:18:17.svg', 514, 0),
('AXR Properties', NULL, 'info@axrppl.com', 'http://www.axrppl.com/index.html', 'orn out of a pursuit for excellence, AXR Properties is conceived by the young and dynamic third generation entrepreneur, Mr. Nimish Kamdar. Equipped with vast experience in diversified business interests that have taken him around the globe, he brings with him in-depth knowledge in the world of property investment, including both procurement as well as development.\r\nNimish has also been an integral part of a team that played an instrumental role in propelling the Go Green movement in the Middle East. His perseverance and genuine belief in the cause witnessed the active participation of over 200 leading companies there.\r\nWith presence in Mumbai, Bangalore and the Middle East, today AXR Properties is all set to scale ever greater heights and challenges under the relentless passion of Mr. Nimish Kamdar.', 'Builder logo2015-12-12 07:43:34.svg', 515, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('Balaji Properties', NULL, 'balajivajraahomes@gmail.com', 'http://www.balajipropertiesprojects.com/', 'With over 16 years of experience in the construction field, Balaji Properties has collectively accomplished eminence by constructing Residential Apartments. We have striven towards success by completing various residential projects, maintaining infrastructure in its projects by addressing the most basic needs such as Water, Power and feature Amenities for residents.\r\n\r\nWe strictly take care of all the construction activities (start to finish of the project), ensuring to complete in organized and systematic approach ? for reliability, durability and quality by our expert team of architects, engineers and interior designers, business development executives, law and revenue departments.\r\n\r\nOur aim is to provide full-scale facilities, which has a blend of pleasant environs, necessary amenities and conveyance to nearby facilities which give relax and comfortable living space.', 'Builder logo2015-12-12 07:45:06.svg', 516, 0),
('Bhagini Developers', NULL, 'bhaginidevelopers@gmail.com', 'http://bhaginidevelopers.com/', 'Is a Builder and developer in Bangalore. We help people realize their lifetime dream of owning a home. We are in the realty industry since 10 years. We are proud to say able to help innumerable people realize their dreams. We provide property consulting Services. We help people buy industrial and Agricultural land. We provide marketing service for Residential Layout Projects, Apartments, Villas and Villaments. We also help in construction of one?s dream home.', 'Builder logo2015-12-12 07:47:29.svg', 517, 0),
('Bhagyashree Developers', NULL, 'sales@bhagyashreegroup.com', 'http://bhagyashreedevelopers.in/', 'We strive to surpass our customers expectations in various services being offered.\r\nA home is the dream of every family and probably the most important asset created by the people?. We at Bhagyashree Group strive to fulfill the dreams of the people to have an asset for their lifetime.\r\n \r\nBhagyashree Group is a reputed company, running successfully for the past 15 years in Plotted Development Business. The layouts developed by us have appreciated more than 20 to 30 times in last few years .We attribute our success to sincerity and transparency over these years. We strive continuously to build long-lasting relationship with our customers. We have built the foundation on people?s trust that now; they live in homes that were built according to their dreams. Homes that were delivered on or before schedule, with all clear titles and all approvals.\r\n \r\nWe Bhagyashree Developers, attribute our success to sincerity and transparency in all our transactions. All our properties are safe and secure as investment options and completely hassle free in terms of legalities. We realize that an investment today is for the benefit of generations to come and we take all care to ensure that it is a prized asset for ever.', 'Builder logo2015-12-12 07:50:05.svg', 518, 0),
('Bhartiya City Developers Pvt. Ltd', NULL, 'info@bhartiyacity.com', 'http://www.bhartiyacity.com/', 'Unlike most people in realty, Bhartiya comes from a background of design. We started in 1987 designing apparel. Soon, designs from our studios in Milan were being adapted by brands like Hugo Boss, Armani, Zara and Mango. During the course of our evolution, we struck upon a simple truth: good design makes people happy. Be it a well cut, flattering jacket or a thoughtfully built house. To go from one to the other was to take us two decades. During these years, our founder Snehdeep Aggarwal travelled extensively to the finest cities in the world. The contrast between what he saw in Munich, Florence, London, and what he came back home to was tremendous. Even the common man there could afford a good home. Cycling to work was not life-threatening. If such a life was possible in Amsterdam and Barcelona and Milan, why not here? \r\n\r\nFor our founder, this was not a rhetorical question. His answer was to build a whole city, to design a whole way of life. That vision is Bhartiya City. Bhartiya City has brought together architects and urban planners of international repute. They include Perkins Eastman-New York, Cox-Sydney, BDP Khandekar-Amsterdam for master planning, Broadway Malayan-Singapore for residential, retail and hotel and Edifice-Mumbai for the IT Park, amongst many others. \r\n', 'Builder logo2015-12-12 07:57:22.svg', 519, 0),
('Bhoomika Housing Corporation', NULL, '', 'http://www.bhoomikahousing.net/', 'Bhoomika Group has instantly made a distinctive mark on the cityscape of Bangalore: the city in which the group is headquartered, as it understands the city with an instinctive ease sharing with it a glorious past and a promising future.\r\n\r\nThe group has ventured in the real estate world with a humble beginning in the year 2003; with two partners and has successfully completed seven projects in Bangalore with an area of 2- 10 acres each. The third associate, joining the firm, hails from reputed Real Estate Company.\r\n\r\nRiding on the growth wave of real-estate, the group has diversified strategically into the development of land and constructions of independent houses and villas. And from then on it transformed into a new breed of builders and has carved an enviable niche for itself in the real estate arena. Bhoomika has provided shelter for the customers in the form of completed projects and people are enjoying their lifestyle in these beautifully landscaped and superior projects with good appreciation and comfort.\r\n\r\nOur vision and intent now encompass to not just build better homes or offices, but to provide a better quality of life for people who are a part of any facility which the Group has created. The focus is to strive to impart better quality through the development of world-class neighborhoods, which will enrich the lives of anyone living or working within these self-contained environs.\r\n\r\nThe combination of prime location, aesthetic appeal, functional designs, and optimum use of space and excellence of construction at an affordable price is the hallmark of Bhoomika?s ?value for money? proposition. And a highlighting factor that has had a multiplying effect on Bhoomika?s every investment to date.', 'Builder logo2015-12-12 08:00:32.svg', 520, 0),
('Bijith International Builders ', NULL, 'sales@bijith.in', 'http://www.bijith.in/', 'Experience the excitement of realising your dreams. Dreams are but visualisation of the desired result. Realisation of dreams bring joy and excitement in a family, especially, when it is a shared dream! Like, a dream to own a home!\r\n \r\nQuality is our Building Blocks.\r\nBijith Builders is a team of qualified and dedicated professionals who have vast experience and expertise in the area of Architecture and Design, Project planning and Implementation.\r\n \r\nWe Bijith Builders continuous supervision of each project from beginning to the end to ensure that our projects are stamped of reliability, trust and best value for every paise they pay.\r\n \r\nWe know that building a home can be both an exciting and a nerve-wracking experience. It is something that many people do only once in their lifetime. We understand that it is your hard earned money that is being spent and it is our job to make sure that it is spent wisely. We will be with you through every step of the process to make sure that everything goes Well within your budget. We have several banks that we work with and will be glad to assist you in obtaining financing if needed.', 'Builder logo2015-12-12 08:02:51.svg', 521, 0),
('Birla Apple Spire', NULL, 'info@birlaapplespire.com', 'http://birlaapplespire.com/', 'Apple Group had it?s humble beginnings of business in 1985 as a Cement Dealer by the name Manjunatha Agencies and thereon became C&F agents and later sole distributer-ship of Sagar Cement, Suvarna Cement and Nagarjuna Cement for entire Karnataka. The first noticeable milestone for the business was In the year 1998 when the company started its own manufacturing unit by name Lodhi Calcium Products Pvt.Ltd in Kurnool Dist. AP, with a capacity of 100 MT per day and marketing cement with the brand name ?Asian Cement?. Moving forward, the business operations were spread to Chitradurga & Belgaum District during 2004 & 2007 when the company acquired 2 more manufacturing units with a capacity of 250 MT & 750 MT per day respectively, by name DM Cements Pvt.Ltd. and Ratna Cements (Yadwad) Ltd.,\r\n\r\nBeyond Cement sector, the business diversified in to Automobile segment with dealerships for Indian and Global brands like Kinetic, Suzuki and Volkswagen. After successful business growth in the first 2 sectors, the group ventured in to the real estate segment with a few joint venture projects. In 2014, the group launches it?s own brand Apple Spire with a milestone 25 storey project in Nayandahalli, Mysore Road. The Apple group looks forward to recreating it?s magic in the real estate space with the cornerstone of its business initiatives ? ?happy customers?.\r\n', 'Builder logo2015-12-12 08:04:39.svg', 522, 0),
('Blue Valley Properties Pvt. Ltd', NULL, 'customercare@bluevalley.in', 'http://www.bluevalley.in/', 'Vision\r\nTo be the most innovative real estate provider with a reputation for uncompromising ethics, professionalism and competenc\r\nTo address customer needs and sentiments through our deep commitment to a value system grounded in trust and integrity.\r\n\r\nMission\r\nPlacing greater value on people than on profits, we pace our growth with that of client interests. We will provide the best of real estate solutions in order to facilitate all transactions in a smooth and transparent manner.\r\nOur efforts will be aimed at creating properties that will be valued for their aesthetic, strategic and viable aspects, backed by exceptional levels of service.', 'Builder logo2015-12-12 08:07:24.svg', 523, 0),
('Brahma Holdings', NULL, 'info@brahmaholdings.in', 'http://www.brahmaholdings.in/index.html', 'Brahma holdings have been established as a real estate company in south Bangalore. \r\n\r\nPartners of \'Brahma Holdings\' are committed to provide better and customized homes. We built on a wealth of experience, earning excellent reputation for elegantly designed and crafted development of high quality homes. We delight our customers by our PUNCTUALITY, QUALITY, RELEABILITY, SPEED AND TRANSPARENCY. We have a very good team of experienced professionals to plan, work, manage and serve our customers. To build \'forever\' relationships with customers on the solid foundation of trust. To achieve and sustain growth by delivering projects, products and services of consistently great quality through efficient and effective operations within the agreed time.\r\n\r\nBrahma holdings take pride in building apartments that suits exacting demands and high expectorations of their client. Our homes provide much needed respite to the tired mind and body. The Professionalism and commitment that have gone in building each project can be perceived in every corner of the building.\r\nOur mission is to build the finest quality homes bases on modern design concepts our past ventures have been highly rated and have become landmarks in that area.\r\n\r\nBrahma holdings have been established as a real estate company in south Bangalore. Our company purpose is to be a leader in the real estate industry by providing enhanced services, relationship and profitability. Our vision is to provide a quality service that exceeds the expectations of our esteemed customers.\r\nMission statement to build long relationships with our customers and clients and provide exceptions customer services by pursuing business through innovation and advanced technology. Our goal is to build good reputation in the field of real estate.\r\n\r\nBrahma holdings are engaged in constructing residential houses. We completed couple of project in south Bangalore in which people are living happily. We are able to complete all our new projects and sustain in the market through our satisfied customers, their friends and contacts. Quality is primary to \'Brahma Holdings\'. All systems and procedures are formulated to achieve the \"Quality Objective\". We have an obsession for quality. Our vision is to build \'forever\' relationships with customers on the solid foundation of trust.', 'Builder logo2015-12-11 10:48:18.svg', 472, 0),
('Bren Corporation', NULL, 'sales@bren.com', 'http://www.bren.com/', 'Bren is not just about creating homes, workspaces and retail spaces. It is a way of thinking, and creating, always. Our approach, Brennovation, drives our philosophy of development, as we continually innovate to enhance customer experience and quality standards.\r\n\r\nWith expertise in development that stretches back three decades, in just four years we have built homes for over 6000 residents, delivering more than 2 million sq.ft. and have planned developments of 8 million sq.ft.\r\n\r\nWhether it\'s architectural design, construction or finding the best properties, at the heart of our approach is the customer, understanding the needs of people and putting them first in whatever we do.\r\n\r\nBren Retail is in collaboration with Nolte, one of Europe\'s leading manufacturers of kitchens and bedrooms.\r\n\r\nBren Education is dedicated to creating educational infrastructure for schools.', 'Builder logo2015-12-12 08:11:07.svg', 524, 0),
('Brigade Enterprises Limited', NULL, 'enquiry@brigadegroup.com', 'http://www.brigadegroup.com/', 'Brigade Group was established in 1986, with property development as its main focus Today, Brigade Group is one of South India\'s leading property developers. We are headquartered in Bangalore, with branch offices in several cities in South India, a representative office in Dubai and an accredited agent in the USA. We have a uniquely diverse multi-domain portfolio that covers property development, property management services, hospitality and education. Our projects extend across several major cities in South India: Chennai, Chikmagalur, Hyderabad, Kochi, Mangalore and Mysore.', 'Builder logo2015-12-12 08:12:58.svg', 525, 0),
('BRN Land Promoters ', NULL, 'info@brnlandpromoters.com', 'http://www.brnlandpromoters.com/', 'BRN Group is a young, dynamic and vibrant real estate developer from Bangalore formed to fulfill the aspirations of people to have their own house. BRN Group has pledged itself towards fulfilling this aspiration of people and has since 2004 worked in creating residential plots in Hosur, Plots, Villas, Apartments, Houses, Lands, Real Estate.\r\nOne of its main focus areas is developing integrated master planned communities means wherein the developments have one or more community facilities, including hospitals, schools, retail and commercial buildings enabling a ?LIVE, WORK and PLAY? theme within the same development. At BRN, we have expertise, who identifies the land very much suitable in terms of Area, Price, Commutation, appreciation and the basic needs required for our customers to invest in our project.\r\nHere we take utmost care of the developments undertaken in our project and deliver the developments as committed on time. BRN Group has created a brand by itself through trust, hard work and sincerity maintained for over more than two decades in the field of Construction, Road Contract Work, Infrastructure Developments, Ready mix Concrete to name a few, giving high quality construction and true value for the money, thus utilizing our infrastructure and our experience in passing on the same trust to our valued customers through BRN Villas. At BRN Villas, we have expertise, who identifies the land very much suitable in terms of Area, Price, Commutation, appreciation and the basic needs required for our customers to invest in our project. Here we take utmost care of the developments undertaken in our project and deliver the developments as committed on time and we have a very professional marketing team who takes care of your requirements and render very good service to our esteemed customers.', 'Builder logo2015-12-12 08:14:57.svg', 526, 0),
('BSR Constructions', NULL, 'sudhakar@bsrbuilders.in', 'http://bsrconstructions.in/', 'BSR Constructions emerges as one of the finest builders in Bangalore, Our motto ?Quality First? has led us to become one of top budget class apartment builders in the real estate arena today.\r\nOur growth today can be measured with our growing reputation amongst our increasing number of customers as we strongly believe in ?let quality decide our reputation?; we have always thrived to offer dream homes to many and continue to do so with our quality Customer Service, aesthetically designed modern and luxurious apartments. Our infrastructures have been designed with studies made at a micro level on client needs and successfully implementing them with the help of our expert architects and engineers.\r\nBSR Constructions made its debut in 2010 under the leadership of Mr. B SUBBAREDDY (Founder & Managing Director). We feel immense proud in announcing that we are today one of the fastest growing realtors in Bangalore with our primary focused being on Residential Apartments. We have been credited with over 4 completed projects in Bangalore, BSR Residency- I, BSR Residency ?II, Akshya Residency ,Royal Enclave are now given possession to its customers within the promised time frames. We have currently 2 ongoing projects which are due to be completed by the end of 2014 and 2 upcoming projects in Bangalore.\r\nBSR Constructions offers a lifestyle of luxury and elegance with creativity. We are focused on offering High Quality homes at affordable costs. Our structural safety is considered our prime concern; we therefore always remained attentive on the civil engineers team that works with us. With over 10+ years experienced and committed team of engineers and architects we have we can confidently assure the finest ?Dream Homes? to citizens.\r\nOur research team continuously works on innovating and designing solutions to concerns like water scarcity during certain seasons, space management etc.BSR Constructions has taken special care to tackle with such problems, one being the smartly designed Rain Water harvesting system which gives us an edge over others in water supply management. Our focused Customer Service team has always aimed at living upto our ?Motto? that?s ?Quality First?. We have always tried to keep our valued customers satisfied and have always welcomed their feedback and suggestions.', 'Builder logo2015-12-12 08:17:00.svg', 527, 0),
('Carbon Developers Pvt. Ltd', NULL, 'paul@carbondevelopers.com', 'http://www.carbondevelopers.com/', 'Why Choose Carbon Cornerstone?\r\nBangalore has become a magnet for the largest number of expatriates who have made their home temporary, in transit or permanent. As a global investment destination, Bangalore is the ideal place to put your roots down. Cosmopolitan, the IT City offers potential to investors with its developing infrastructure, as it continues to attract a unique talent pool.\r\n\r\nIdeal Location\r\nThe fastest emerging zone, Bangalore North continues to preserve its tranquil and picturesque tree-lined environs, primarily due to the many institutions and convents that lie in the vicinity. What?s more, for comfortable living, Carbon Cornerstone is strategically located with utilities and basic facilities surrounding the project, which include well-stocked super markets, schools, religious institutions, hospitals and clubs. It will not be long before mega malls begin to appear along the 120 feet Hennur Bagalore Main Road.', 'Builder logo2015-12-12 08:49:04.svg', 528, 0),
('Carp Properties', NULL, 'sales@carpproperties.com', 'http://www.carpproperties.com/', 'A name grounding its roots very rapidly in the bangalore real estate market. Emerging as one of leading brand in the market , with a promise to all its customers, that is by providing very excellent apartments in the most happening places in bangalore, with very good quality of construction maintaining all the standards,\r\n\r\nTimely delivery of the projects well within the stipulated time frame, with very good workmanship with its qualified engineers, architects,technicians, and a very well maintained professional marketing staff who understand each and every customer and serve them as per their needs and helping them out in every possible way and above all the best part is the most affordable prices that are being offered with respect to each location the project is in.\r\n\r\nTruly a name which is gaining trust with all its customers who have chosen it, and who are looking forward to have the experience of their lifetime in achieving their dreams of owning a home for themselves. in carp properties they just don?t sell flats they promise a dream home for everybody. a brand name which is going to win not any race but the hearts of all its customers.founder Santosh Mirajkar and Surendar Kumar.', 'Builder logo2015-12-12 08:50:43.svg', 529, 0),
('CBR Constructions ', NULL, 'cbrconstructions@gmail.com', 'http://www.cbrconstructions.in/', 'CBR CONSTRUCTIONS brings you the touch of excellence from experienced builders whose whose business ethics are structured around customers needs.Every requirements of residents at this luxury apartments has been catered to , be it a central location,Superior infrastructure, quality amenities or features conforming to classy lifestyles.\r\nCBR constructions is a premier construction company in Bangalore,It has a singular vision of transforming the urban setting into next generation landscapes based on a solid foundation of innovation .Quality ,Eco -friendliness and value for money ,today CBR Construction projects are known to be the symbols of quality construction ,craftsmanship ,clear titles and value for money,Professional project management services and adherence to strict time schedules bringing out the best in every project .It has over a decade of experience in property development with 5 residential projects development so far many more are coming up in mere future.\r\nDelivering a pre-eminent lifestyle in the lap of comfort is CBR CONSTRUCTIONS top priority .A passion for excellence ,innovation ,futuristic designs ,motivated personnel and strategic locations in a nutshell,defines CBR CONSTRUCTIONS.', 'Builder logo2015-12-12 08:52:39.svg', 530, 0),
('Celebrity Structures India Pvt. Ltd', NULL, 'info@celebritystructuresindia.com', 'http://www.celebritystructuresindia.com/', 'We have over two decades of experience in property development-Celebrity Structures India , a group managed under the able guidance of Flt.Lt. (Retd) P.Karunakar Reddy and a team that is passionate and committed to deliver quality homes to the customers while adding that extra dimension of lifestyle and returns on the investments ,consistently.\r\n\r\nWith several successfully completed and ongoing projects in Bangalore and Hyderabad ,Celebrity Structures India (Pvt) Ltd. is fast emerging as one of the premium developers in south Bangalore.\r\n\r\nOur engineers and supervisors are trained to give our clients the very best. It has over a decade of experience in property development with Fourteen residential projects completed so far in Hyderabad & Bangalore and many more coming up in near future.\r\n', 'Builder logo2015-12-12 08:54:21.svg', 531, 0),
('Century Real Estate Holdings Pvt. Ltd', NULL, 'sales@centuryrealestate.in', 'http://www.centuryrealestate.in/', 'Century Real Estate, with its headquarters in Bangalore, India was founded by Dr.P.Dayananda Pai and Mr.P.Satish Pai in the year 1973 with the primary objective of transacting in the simple buying and selling of land. More than 35 years later, the Company has evolved into an integrated, full service real estate development company that has transformed Bangalore?s urban landscape and is credited with several of Bangalore?s landmark developments such as Manipal Centre, Taj Residency, Vijaya Bank Head Quarters and Diamond District to mention only a few. Century Real Estate has the unique distinction of having purchased land on M.G.Road at INR 5 per square feet way back in 1973 where today the quoted prices are in the range of INR 20,000 per square feet.\r\n\r\nRegarded as one of the oldest and most respected companies in the Real Estate space in India, the growth of Century Real Estate has practically mirrored the growth of Bangalore as one of Asia?s fastest growing cities. Today, we are the largest owners of real estate in Bangalore with a land bank in excess of 3000 acres representing more than USD 2 billion in asset value and a development portfolio of over 10 million square feet comprising hotels, office buildings, residences, educational institutions and integrated townships spread across South India.', 'Builder logo2015-12-12 08:56:48.svg', 532, 0),
('Certainant Infrastructure Pvt. Ltd', NULL, 'sales@certainant.com', 'http://certainant.com/', 'Certainant has evolved as one of the leading Builders and Developers through the dedication of its management and team. Certainant has enjoyed significant growth in a short period of time. The company is managed by group of professionals, who have vast experience in carrying out feasibility studies and project management. The effort has been towards developing projects of superior quality at ideal prices. \r\n\r\nWe specialize in creating value-added, master-planned communities that meet the full spectrum of lifestyle needs, whether it is in design Innovation, space optimization or choice of locations, Certainant proves to be way ahead in terms of quality, standard and innovation. While continuing to actively pursue expansion in its core business of innovative, high quality real estate development, Certainant has diversified into related business lines to further build value for its shareholders and business partners.', 'Builder logo2015-12-12 08:58:38.svg', 533, 0),
('Chamundi Builders ', NULL, 'info@chamundibuilders.in', 'http://www.chamundibuilders.in/', 'Chamundi Builders is among the leading property developers in Bangalore with a reputation of quality construction and commitment to timeless values. Since its inception that is almost three decades, Chamundi Builders has grown from strength to strength carving a niche for itself in the realm of real estate and property development.\r\nThe company perfectly blends customer focus, total transparency, and impeccable ethics to deliver international quality living spaces at affordable rates.\r\nAmber Woods is its latest property offering in one of the most strategic locations in the city?developed to redefine the face of luxury living. Nestled amidst undulating green pastures?bordering the Turahalli forests glade off Kanakapura Road, Chamundi Amber Woods creates a haven that stands in harmony with tranquillity and luxury.\r\nIt is away from the din and bustle of the city yet well connected to top business centres, educational institutions and shopping areas?making it a rational and perfect choice for investment?and living the good life!', 'Builder logo2015-12-12 09:00:12.svg', 534, 0),
('Charigan Group', NULL, 'info@charigangroup.com', 'http://www.charigangroup.com/', 'Our first project ?SRESHTA RESIDENCY? phase I near Chikkaballapur has come to the final stages. ?SRESHTA RESIDENCY? phase II , is approved from authorities.\r\n\r\n   Second project in Mysore  ? JAAJI ENCLAVE? is a sweet start for our 2nd inni ngs. Around 1,40, 000. sq.ft of MUDA approved area has been blocked by 96 esteemed clients in just 15 days. The development work of ?JAAJI ENCLAVE? is goi ng on in a brisk pace.\r\n\r\n   Third project ?CHARIGAN\'s ARAVIND GREEN FIELDS? has around 4,00,000.sq.ft of saleable area near Sarjapur , the place is called as ?Chikka Tirupati? out skirts of Bangalore.\r\n\r\n     Marketing and development of layout is underway. This place is suitable for small scale investors as the place is very close to ITPL, Sarjapur and Kolar-Hosur highway.', 'Builder logo2015-12-12 09:01:56.svg', 535, 0),
('Chartered Housing Pvt. Ltd', NULL, 'info@charteredhousing.com', 'http://www.charteredhousing.com/', 'Since its inception in 1989, Chartered Housing has made its presence felt in the real estate industry by continually producing buildings that are high in quality, and service that is high in values. Having made an instant impression that is to last for decades to come, Chartered Housing constructed Chartered Resorts, a one of its kind project that set a trend in the real estate sector. This was a move which changed the prevalent trend of matchbox flats in the city. \r\n \r\nThis welcome change ignited a spark, after which every project undertaken was claimed to be better than the one preceding it. And as a result of more than 25 years of building homes that delight, the name Chartered instantly strikes one as the house for homes. \r\n \r\nApart from promoting residential apartments in Bangalore since the time it was founded, Chartered also develops commercial spaces, interiors and offers premium construction services in Bangalore. \r\n \r\nWhile special emphasis is on providing unmatched quality, Chartered pursues uncompromising ethical values and transparency which are paramount to all its operations. At Chartered, quality, values and transparency are rolled in a capsule - ensuring happy customers and happy employees.', 'Builder logo2015-12-12 09:03:56.svg', 536, 0),
('Citadil Construction', NULL, 'info@citadilconstruction.com', 'http://www.citadilconstruction.com/', 'Citadil Construction is well known company in the field of who has Builder Developer & Promoters a great amount of trust and loyalty from its patrons over all these years. Our aim is to provide premium quality homes at an affordable price point. We place customer satisfaction above all else, and work with only the best suppliers and materials to ensure that we deliver homes, not just houses. Our services are known for its contemporary design, innovation, clear and legal titles and scheduled completion and have been actively participating in the high growth opportunities offered by Indian Infrastructure Industry, more specifically in the construction and real estate sector\r\n 	\r\nWe are also venturing into development of integrated apartments of world class standards. Managed by well experienced personnel, we offer our clients quality and reliable services in the area of real estate. We are bound to ethical and value based business practices and fosters the highest level of transparency and commitment in all our transactions\r\nOur satisfied customers are our strength to come up with new projects to meet the market requirement of this Silicon Valley, Bangalore. Our commitment is to deliver quality project with affordable cost on the scheduled time.\r\n ', 'Builder logo2015-12-12 09:05:33.svg', 537, 0),
('Citrus Ventures Pvt. Ltd', NULL, 'marketing@citrusventures.in', 'http://www.citrusventures.in/', 'Since our inception in 2011, we have aimed for a customer-centric approach and have strived to deliver your dream home to you through our robust engineering and aesthetic design. Highly regarded for our transparency, reliability and quality, we believe in clear-cut communication with our customers and value the money and time that you invest in us. Our mission is to provide complete customer satisfaction by ensuring that the quality of our products reflect honesty and integrity at every stage of its construction.\r\nIn a few years, we see ourselves achieving transparency in all spheres of business conduct. We want to provide our customers a combination of precision and beauty. For us, our customer is always first and our sole vision is to ensure providing them services which uphold superior quality and reliability.', 'Builder logo2015-12-12 09:07:34.svg', 538, 0),
('CMRS Group', NULL, 'info@cmrsproperties.com', 'http://cmrsproperties.com/', 'One of the leading developers of real estate, the CMRS Group is dedicated to transform the image people have of houses. The CMRS Group aims to help people find homes within the well-constructed structures we build for them. Economical homes are no longer a dream for house hunters with our value homes. The CMRS group also offers a range of luxury homes to people who are looking to spend a little more and live lavishly.', 'Builder logo2015-12-12 09:09:36.svg', 539, 0),
('Columbia Developers Pvt. Ltd', NULL, 'info@columbiagroup.in', 'http://www.columbiagroup.in/', 'In Bangalore\'s fast growing and ever changing construction scenario, Columbia Group stands out like a beacon, symbolizing quality and innovation.\r\nMarketed by our professionalized organization founded in April 2008. At the helm of the company, Mr.Farooq Khan, Chairman and Managing Director of the company. Who possess hand on knowledge. He has an excellent track record with leading developers and builders.\r\n', 'Builder logo2015-12-12 09:11:29.svg', 540, 0),
('Comfort Builders and Developers', NULL, 'info@comfortbuilders.in', 'http://www.comfortbuilders.in/index.html', 'Comfort Builders is a Bangalore based firm engaged in the business of complete real estate and related services, managed by professionals and well experienced personnel. A reality that bears testimony to the passion of Comfort Builders in delivering homes and commercial spaces, which inspire innovations and at the same time presents, its occupants with a season to celebrate the JOY OF LIVING. Started in Bangalore about 16 years ago, we offer our clients quality and reliable services in areas of Housing and commercial complexes in and around Bangalore City. We are bound to ethical and value based business practices and foster the highest level of transparency and commitment in all our transactions. Comfort Builders aims to provide Accessibility Convenience and Value appreciation for our customers. Over the years, we have been associated with the success of several prestigious real estate projects involving the City?s leading property developers and also enjoy a global spread of satisfied clientele.', 'Builder logo2015-12-12 09:12:57.svg', 541, 0),
('Comfort Shelters Pvt. Ltd', NULL, 'shekar@comfortshelters.co.in', 'http://comfortshelters.co.in/', 'Co-Founded by P.N.Chandra Shekar and K.Vijaya Kumar, Comfort Shelters Pvt. Ltd. is poised to take a quantum leap, redefine quality and aesthetics in building solutions. Born out of notions of royalty, class and privilege ?Comfort Shelters? offers you a rare combination of modern infrastructural facilities set in pristine, natural surroundings.\r\n\r\nWe are dedicated to providing the highest level of professional construction and architectural services. Our quality of construction provides value to every project we build. Our goal is to exceed the expectations of our customers and provide the highest levels of professionalism, integrity, and honesty. Comfort Shelters Pvt. Ltd system allows precise management of the budget and schedule associated with your custom project. We work with the customer to maintain budget along with the construction timeline, while allowing for the flexibility associated with custom building.\r\n\r\nDesigning and building around the \"wants and needs\" of the customer is a priority for Comfort Shelters. We, employs the time ? tested concept of Vaastu whilst acquiring and planning layouts and homes. Judiciously combining modern engineering techniques with these traditional values, the company has captured the loyalty of many a family who has invested in its properties.\r\n\r\nThe combination of prime location, aesthetic appeal, functional design, and optimum use of space and excellence of construction with an affordable price is the hallmark of Comfort Shelters Pvt Ltd Blessed with incredible heritage & legacy, we are committed to continue developing Indian landmarks. So when you buy a Comfort unit, you get Value for Your Money. Honesty and a professional outlook, coupled with discipline and a will to please are the benchmarks of this vibrant real estate investment company.\r\n\r\nOur vision and endeavour have always been to create world-class living and working environments?and provide a better quality of life for people who inhabit them. We warmly invite you to explore our world: visit our projects, meet our people, and discover how we work and what we have achieved.', 'Builder logo2015-12-12 09:15:11.svg', 542, 0),
('Cornerstone Properties Pvt. Ltd', NULL, 'sales@cornerstoneindia.in', 'http://www.cornerstoneindia.in/index.html', 'At Cornerstone we focus on developing and investing in a distinctive range of top quality master planned enclaves, residential projects, retail and commercial properties. We precisely tailor property solutions to suit unique needs. Our properties are created through visionary strategies, principle based transactions, perseverance and skilful execution.\r\n \r\nOver the years, Cornerstone has had successful engagements across a broad range of real estate clients. Our strength lies in astuteness in reading the market and providing exemplary project and master planning. Our team works within a well-established client-driven process to ensure unparalleled service for all stakeholders that engage with us.', 'Builder logo2015-12-12 09:17:57.svg', 543, 0),
('Cubatic Shimul Ventures Pvt. Ltd', NULL, 'office@cubatic.net', 'http://www.cubatic-shimul.com/', 'Innovation: No matter the task, we find a way to get the job done. Since our early beginnings, we have always tried for making use of new technologies and evolving building trends. As we expand, we continue to push the boundaries of innovation, finding ways to work faster and smarter, and respond to our clients\' challenges with ingenuity.\r\n\r\nIntegrity: From how we treat our clients to how we engage with our project partners, a deep commitment to ethics and fairness guides how we do business and is paramount to our success. We hold our work and our people to the highest professional standards, because at the end of the day, we believe that doing the right thing all the time and every time is the only thing to do.\r\n\r\nQuality: Every Detail matters. Delivering our clients\' visions means understanding that the details which are just as important as the bigger picture. We believe in doing it right the first time, every time, and place value on diligent planning and stringent quality control. Our projects are built to last and realize our clients\' standards for long-term performance.', 'Builder logo2015-12-12 09:20:17.svg', 544, 0),
('Definer Ventures', NULL, 'rv@definer.in', 'http://definer.in/', 'To build a future wherein DeFINER is a fully integrated  construction / infrastructure & Hospitality company and is not  just a household name across the country but across the Globe for building and marketing fine living environments that are built with the highest quality while promising to be the ultimate value-for-money.DeFINER with a sole mission and vision that every citizen in this country should own their dream home strives to offer quality living spaces at affordable prices.', 'Builder logo2015-12-12 09:22:17.svg', 545, 0),
('Dhruthi Infra Projects Limited', NULL, '', 'http://www.dhruthiinfraprojects.com/', 'Dhruthi Infra Project Ltd. is a organisation involved in developing residential and commercial spaces. We believe in delivering high quality product in order to meet the needs of the customers which in the end results in customer satisfaction. Our flagship project Tranquil Towers is the designed keeping in mind the needs of modern customer. It consists of 270 apartments overlooking more than 35 acres of green belt. It has three way connectivity to ITPL, Hope Farm and will be connected to upcoming Peripheral Ring Road.', 'Builder logo2015-12-12 09:24:52.svg', 546, 0),
('DivyaSree Developers', NULL, '', 'http://www.divyasree.com/', 'DivyaSree Developers, a Bangalore based company is playing a significant role in changing the real estate landscape of South India.\r\n\r\nWhether its an IT Park, a residential development, an Infrastructure project or a built-to-suit campus, our technical expertise, global exposure and project management skills, give us the edge to constantly innovate and deliver the best, in the process helping clients to turn real estate objectives into business assets.', 'Builder logo2015-12-12 09:27:06.svg', 547, 0),
('DNR Corporation Pvt. Ltd', NULL, 'info@dnrgroup.in', 'http://www.dnrgroup.in/', 'ith best in class specifications, concept based designs, international standards & true value.\r\nThe core maxim of any civilization is happiness. Through prosperity of being and becoming. DNR Corp partakes in the millennia old wisdom established by our enlightened ancients.\r\nFluid, Dynamic, Evolving. A venture that creates, regulates and improvises itself constantly, to keep in step with our ever sprawling cities, DNR Group?s presence has become synonymous with being best in the real estate world.\r\nThe key to our work ethic is Innovation and Interaction. Be it information or individual, we are the gardeners, willing and eager to invest loving efforts and energy into the aspiration of the full bloom. Combining a cohesive and consistent Quality Standard with ever vigilant Customer Service, we accomplish the Life Spaces which are breathtaking in attributes.', 'Builder logo2015-12-12 09:29:10.svg', 548, 0),
('Donata Developers', NULL, 'info@donatadevelopers.com', 'http://www.donatadevelopers.com/', 'Donata Group, a new-age construction and development enterprise, prides itself on its ability to consistently provide the best in terms of quality, perfection and unique customer relationship management when it comes to buying homes. The group is led by a team of energetic and experienced professionals dedicated towards creating value brick by brick. The projects offered, suggest a sophistication that is a result of impeccable planning and carefully crafted architecture.', 'Builder logo2015-12-12 09:31:17.svg', 549, 0),
('DS Max Properties Pvt. Ltd', NULL, 'sales@dsmaxproperties.com', 'http://www.dsmaxproperties.com/', 'Vision\r\nOur vision is, \"to provide value homes with best amenities and share the joy of living.\" Driven by a passion to serve; we are inclined towards understanding our customer\'s needs and wants to make their lives sweeter by adding value to their investments on our properties.\r\n\r\nMission\r\nOur mission as an organization is to maintain the standards of our corporate excellence in our delivery and skills and identify avenues to furnish our brand beyond borders. To locate the most exclusive and suitable property, and utilize our creativity, experience and technical expertise to integrate it in an environmentally friendly, ultra-modern, luxury apartments aimed at providing our clients their dream home.\r\n', 'Builder logo2015-12-12 09:35:48.svg', 550, 0),
('DSR Infrastructure Pvt. Ltd', NULL, 'info@dsrinfra.com', 'http://www.dsrinfra.com/index1.html', 'DSR Group was established more than 2 decades ago in 1988 with the vision of transforming the real estate industry and today after the journey of so many years, its projects stand testimony to the shining excellence and brilliance in execution. Commanding an unparalleled expertise, the group has spread its operations across South India across many real estate verticals, catering to various categories of clients. DSR?s repertoire has a wide array of exclusive projects which have been crafted to international standards.\r\n\r\nWith diversified businesses interests across many portfolios DSR today has achieved staggering successes, thanks to its partners, associates and employees. Its success is shared and cherished among its people who work towards the common goal of a promising and prosperous future.\r\n\r\nCustomer satisfaction is the corner stone of our company?s philosophy. We constantly strive for perfection in our service by understanding the customer needs and providing innovative and customized real estate solutions. The locations of our projects are so carefully chosen that they provide immense potential to investment.\r\n\r\nAll through its existence, DSR?s ideals of quality, innovation and attention to the needs of its clients have always been of utmost importance. These ideals remain at the core of every endeavor of DSR. The DSR brand itself stands for quality, commitment and innovation which manifest in all that it does and stands for.\r\n\r\nThe group has several projects underway in Bangalore, Hyderabad and Chennai catering to the customer with varied needs and tastes.\r\n\r\nProfessional to the core and passionate about standards in construction, the company is moving fast forward into the future.\r\n', 'Builder logo2015-12-12 09:38:22.svg', 551, 0),
('Durga Developers Pvt. Ltd', NULL, 'Giddu_blue1@rediffmail.com', 'http://www.ddpl97.com/', 'Durga Developers Pvt. Ltd., is characterized by a uniquely strong combination of knowledge and 30years of experience in different industries. The Group s team members have vast global experience and proven track record. Their skills are constantly updated through a series of intensive programmes and exposure to the most cutting edge technologies and tools. Our industry certified consultants and advisors bring a vast array of skills to our ongoing effort to provide best in class solutions for our clients. Company\'s head office located at Hulimavu, at Bannerghatta Road, Bangalore, Karnataka. The strengths of all our team members are creativity, expertise, individual initiative and responsibility, enabling us to achieve the optimal outcome together.\r\n\r\nWe strive to outgo the dreams of our clients by exceptional quality & exceeding customer delight. Above all a relationship well kept is a home perfectly built. Carrying the trust unto the last, it s our motto to convert the functional design, architectural marvel & strategic location into a grand place of living at a price you love. No word has ever motivated us as \"quality\". We take pride in our workmanship and attention to details when it goes the extra mile with customer expectation. We never rest with creating quality home, instead still be attentive about developing enduring relationships and ensuring absolute peace of mind. We care for the overall welfare of the society because investing with us is a wealth tomorrow.\r\n\r\nCompany\'s strategic planning, the handling of significant or fundamental operative matters is determined by our management team in accordance with the advice from strategic partners and professional advisors. Furthermore, our success stems from our dedicated management team, our long experience in the region and in the business, and our commitment to excellence. We have gone to great lengths to identify and recruit the qualified professionals with the skills and experience needed to ensure that our current destinations and future projects remain effectively managed.', 'Builder logo2015-12-12 09:58:32.svg', 552, 0),
('Eco Land Constructions Pvt. Ltd', NULL, 'info@ecoland.in', 'http://www.ecoland.in/', 'Eco Land Constructions Pvt Limited had its inception as a partnership firm which was later incorporated as a Private Limited Company in 2002. The company believes in providing high end structural engineering and construction support for building residential homes for customers who have saved for years and want to put their money for good use.\r\n\r\nEco Land Constructions envision its projects that bring value for money to our clients. Our projects are located in areas surrounded by I.T Companies, Schools, Hospitals, Shopping Malls, etc. The company has successfully completed projects in Bangalore and Kerala that can boast of being completely sold out and full occupancy.\r\n\r\nThe spirit of the company is sculpted in trust, expertise and genuine commitment to deliver. With a few projects spreading its name and establishing the brand for its innovative design, quality construction and delivery, Eco Land Constructions strives to achieve greater heights with each project.', 'Builder logo2015-12-12 10:01:03.svg', 553, 0),
('Ecstasy Projects Pvt. Ltd', NULL, 'info@trifectaprojects.com', 'http://www.trifectaprojects.com/#', 'Backed by over a decade of experience in the construction sector, we at Trifecta have learned a thing or two. In an imperfect world, we have to try harder. Sure we could make grandiose promises like everyone else, even throw in a luxury car or two, but trivialising one of the most important decisions you will ever make is the last thing we want to do. So what makes Trifecta Projects different? We believe in trying harder. We are earnest and we listen. We try and do our best to deliver what you want. If at all you have to compromise somewhere, you are never the last to know. We take our word seriously, and will not promise you what we cannot deliver. At Trifecta, we are balanced and with balanced people, you know you are in good company. - See more at: http://www.trifectaprojects.com/#sthash.k8wNXTGd.dpuf', 'Builder logo2015-12-14 16:25:17.svg', 733, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('Embassy Property Developments Pvt. Ltd', NULL, '', 'http://www.embassyindia.com/', 'Embassy Property Developments Private Limited commenced operations in 1993. Together with our promoters we have over three decades of experience in real estate development. Embassy has an extensive land bank across the country and has developed close to 37 million square feet of prime commercial, residential and retail space as of March 2015. We also have international operations in Malaysia and Serbia.\r\nOur portfolio of real estate developments spans the commercial, residential, retail and hospitality segments of the real estate industry. Our commercial real estate business includes the development of business parks for the IT/ITES sector, SEZs and corporate office space. The majority of the completed projects in our commercial portfolio are built-to-suit and fit-out developments undertaken for specific clients.\r\nGoing forward, we intend to undertake a combination of built-to-suit projects and those done without pre-commitment. Our clients include Alcatel-Lucent, Atos Origin, ANZ, Cognizant, Computer Science Corporation, Fidelity, Geometric, IBM, LG Soft India, McAfee, Mercedes-Benz, Microsoft, NetApp, Nokia Siemens Networks, Supervalu, Target, Vodafone and Yahoo!\r\nOver the last three decades, Embassy has completed over 6 million sft. of residential spaces. Embassy?s ongoing residential projects include branded residences, luxury apartments, uber luxury villas and villaments and integrated townships. Embassy?s residential projects are also designed to obtain IGBC Green Homes Certifications ratings as part of the efforts for creating sustainable developments.\r\nEmbassy?s vision for the future is to leverage its tenant relationships and expand the portfolio of Information Technology space throughout India and internationally, besides focusing on developing residential and retail spaces. We will be developing hospitality projects in the integrated townships and business parks that we develop.', 'Builder logo2015-12-12 10:07:50.svg', 554, 0),
('Eshanya Projects Pvt. Ltd', NULL, 'info@eshanyaprojects.com', 'http://www.eshanyaprojects.com/', 'Eshanya Projects Private Limited is a Land and infrastructure development company constituted by highly qualified and dedicated professionals, predominantly with Real Estate background. With an ambitious vision, distinct mission and the well-structured principles of the organization, the team works on a precise management philosophy to understand the core competence and to harness its strengths.\r\n\r\nThe team at Eshanya Projects Pvt Ltd has developed standard operating procedures and systems, which are unique in the industry. Exploring and learning in order to share information acquired through research with customers and associates has been a constant and continuous effort. The organization strongly believes in the principles of integrity and professional transparency.\r\n\r\nEshanya Projects Pvt Ltd is empowered by two cornerstone beliefs ? trust & complete professional solutions. We believe that, in order to build lasting housing facilities and maintain excellent standards, an in-depth understanding of the peculiarities of the real estate business along with transparency, honesty and integrity are non-negotiable pre-requisites. We are invested in fulfilling your dreams by establishing lasting spaces, because we at Eshanya Projects Pvt Ltd are in the business of Land Development and Building Communities.', 'Builder logo2015-12-12 10:10:16.svg', 555, 0),
('Estella Projects Pvt. Ltd ', NULL, 'info@estellaprojects.com', 'http://estellaprojects.com/', 'Incepted in the year 2013, Estella Projects Pvt. Ltd. is a professionally managed organization engaged in imparting a wide spectrum of services in the domain of real estate construction. Recognized as one of the most reliable firms, we are a concern that works on the virtues of efficiency, reliability, quality and flexibility.', 'Builder logo2015-12-12 10:12:40.svg', 556, 0),
('Evantha Developers Pvt. Ltd', NULL, 'info@evantha.com', 'http://evantha.com/', 'Evantha is an integrated real estate development company having its time tested foundation set in Bangalore?s construction industry.\r\n\r\nWe strive for superior quality and unique designs that are elegantly blended to create contemporary structures. With the backing of decades of experience in constructions, we focus on timely deliveries and satisfied customers.', 'Builder logo2015-12-12 10:14:38.svg', 557, 0),
('Excel Stone Developers', NULL, 'excelstonedevelopers@yahoo.in', 'http://www.excelstonedevelopers.com/', 'The name Excel Stone Developers has become synonymous with quality, dedication and commitment and has an enviable track record of successfully executing many prestigious projects in Bangalore. We execute construction work under a turnkey basis, which not only assures convenience and comfort to the clients but also give them superior quality at a very reasonable cost.\r\n\r\nExcel Stone Developers is one of the most comprehensive realtors in Bangalore, focused on empowering consumers with the resources they need to make more informed property decisions.\r\n\r\nOur projects are qualified by chief structural engineers and the quality certification is done by certified institutes. We use the top-notch building materials, money and associate with the best minds in the business.\r\n\r\nThe Butterfly has a unique elevation and special theme and thought that goes into every detail inside and outside. The beauty of architect design gives maximum cross-ventilation, natural light for each apartment and privacy from one flat to another.\r\n', 'Builder logo2015-12-12 10:16:28.svg', 558, 0),
('Flamingo Properties', NULL, 'info@flamingoproperties.in', 'http://www.flamingoproperties.in/', 'Flamingo Properties is one Bangalore?s leading boutique property developers, that develops residential layouts & flats. These homes are meticulously designed to work around you. Sustainable living and eco-spaces that facilitate encourage serene, connected living are at the heart of our design philosophy. Known for great location, the Flamingo team works tirelessly to research and identify growth hubs in Bangalore to find the best spot for your home.', 'Builder logo2015-12-11 10:51:49.svg', 473, 0),
('Fortuna Constructions India Pvt. Ltd', NULL, 'fortunaconstructions.sales@gmail.com', 'http://www.fortunaconstructions.com/', 'Fortuna Constructions (I) Pvt. Ltd. is one of the fastest growing developers in Bangalore with developments spanning across Residential, Commercial, Retail, Entertainment sectors. Over a period of a decade we have completed 15 Projects spanning a total developed area of over 2 million Sq.ft., Fortuna has 6 ongoing projects comprising around 3 million Sq.ft., & 10 upcoming projects, totaling 5 million Sq.ft., which include Apartment enclaves, Villas, Shopping malls and Corporate structures and Hospitals.\r\n\r\nThe Founder & CMD, Mr. Padmaiah Vuppu , An Engg. Graduate from NIT Warangal who believes in giving Quality, latest designs, timely delivery and excellent value for the projects.', 'Builder logo2015-12-12 10:18:30.svg', 559, 0),
('Foundations Developers & Promoters', NULL, 'foundationsmysore@gmail.com', 'http://foundationsmysore.com/', 'Bring \"Foundations\" in right at the start. At Foundations the team consists of highly skilled technocrates, who have over two decades of exposure in the construction field & have excelled themselves meeting challenges with grit & determination. \r\n\r\n\"Foundations\" can manage your build schedule from the planning stage right through to a sucessful, quality finish, making the whole process as stress-free as possible for you. Our team includes tried, tested and trusted tradespeople; specialists in every aspects of home extensions and improvements. We give you a single point of contact throughout the job. \r\n\r\nWhen you choose a builder credibility matters a lot. The Builders has a glowing track record to take pride in over the last 20 years, We have completed lot of projects. You can choose a Foundations Home project with 100% confidence. We respect your hard earn money and deserve for a good home.', 'Builder logo2015-12-15 07:02:30.svg', 774, 0),
('Foyer Constructions Pvt. Ltd', NULL, 'manmohan@foyerconstructions.com', 'http://foyergroupindia.com/', 'People don\'t buy 12 feet by 14 feet constructed boxes. They don\'t even desire to be in vertical villages filled with random families. They want to belong to residential clubs with sporting amenities. They don\'t even seek a price that they can afford.\r\n\r\nHome seekers look for places that reflect their personalities. They hope for bright homes that spiritually lift their lives. They want a place where they can unwind, recharge and renew themselves for a challenging tomorrow, every evening.\r\n', 'Builder logo2015-12-12 10:21:18.svg', 560, 0),
('G R Constructions', NULL, 'info@grconstructions.co.in', 'http://www.grconstructions.co.in/', 'GR Constructions, one of the renowned builders and developers of Bangalore, has been in the business of developing both residential and commercial projects over the past two decades. Today it ranks as one of the prominent developers. At GRC, we emphasize on quality, excellence, and an unflinching commitment to on time delivery to our customers. We are known for technical excellence, impeccable quality of constructions and clear titles. GRC is a professionally managed company, backed by an in-house team of talented engineers, designers, executives and administrators. We seek perfection in every project that we undertake and promise that our buildings reflect engineering precision and cater to the customer?s tastes and preferences. We earnestly assure our prospective customers that our projects would definitely surpass their expectations and bring smiles on their faces. In short, GRC is a customer driven professional company, continuously striving towards building projects for our invaluable patrons.', 'Builder logo2015-12-12 10:23:33.svg', 561, 0),
('Garudachala Estates Pvt. Ltd', NULL, 'info@garudachala.com', 'http://www.garudachala.com/', 'Garudachala Estates was established by a group of enthusiastic and visionary engineering experts. With a dedicated team, passion towards excellence and a strong accountability towards our clients. Our group has carved a niche and is already standing tall by providing world-class living spaces. We strive to upkeep our passion towards customer satisfaction all throughout. Compromise is something we toss out of our dictionary, and we strive to succeed and give you world-class passion-driven projects!\r\n\r\nWhen it comes to quality community living, Garudachala offers you spaces with unmatched quality, all while enhancing your lifestyle. We, at Garudachala aim at identifying the right people with the right opportunities for being the pillars of our entity. With this strong foundation of the key people, we focus and determine to ideate and design properties which not only aesthetically appeal to our customers, but are also environment-friendly in nature.', 'Builder logo2015-12-12 10:27:39.svg', 562, 0),
('Geetham Constructions', NULL, 'info@geethambuilders.com', 'http://geethambuilders.com/home/', 'Geetham Constructions is a proficient organization focusing on the constructions and development of residential projects in Bangalore and Hyderabad. Our Projects range from luxurious and spaciously designed homes to affordable 2 and 3 BHK Apartments. Preferred location, uncompromising quality, outstanding amenities have always been the hallmark of our projects. We define integrity with colors. Complete transparency, compliance to all regulations and adherence to our commitments are our USP.', 'Builder logo2015-12-12 10:29:46.svg', 563, 0),
('Gina Developers Pvt. Ltd', NULL, 'sales@ginaengg.com', 'http://www.ginadevelopers.in/', 'Today,if the cityscape of Bangalore can boast of architectural marvels, a great many of them can surely be attributed to the construction expertise of our parent company Gina Engineering Company Pvt. Ltd.\r\n \r\nSince 1964,we have been constantly driven by the vital spark of excellence,helping us reach for the skies.\r\n \r\nProviding comprehensive construction services of the highest quality standards,we satisfy the specific requirements of clients while adhering to the agreed time frame.\r\n \r\nAt Gina we have constructed over millions of sft translating into more than 300 creations for some of the elite builders, industries, institutions, software tech-parks etc.\r\n \r\n Few of the projects by our parent company for various clients include:\r\n \r\nBrigade towers, Manipal Center, Vayudooth Chambers, Prestige EGK, Infant Jesus Church, ITPL Residential Block, Regent Place, Reheja Arbor, Prestige Acropolis, Salarpuria Cambridge Residency, HM Geneva House, Embassy Woods, IIMB, Biocon India & HLL.\r\n \r\nFrom being the company behind the scene, as one of the leading Civil Engineering Contractors of Bangalore, we now offer you premier residential collections under Gina Developers Pvt. Ltd.', 'Builder logo2015-12-12 10:31:51.svg', 564, 0),
('Global Developers', NULL, 'info@globaldevelopers.info', 'http://www.globaldevelopers.info/', 'GLOBAL DEVELOPERS is doing the Residential Townships in Bangalore Since 2005. We have completed many successful projects in Bangalore. We are providing all facilities in the Townships which will feel comfort to our customers.\r\n? We are giving standards of quality in construction of Residential Townships.\r\n? We are using Branded quality of Materials in construction of Villas.\r\n? We are doing the projects in prime locations in Bangalore.\r\n? We are having professionally skilled Engineers, Architectures and Employees. \r\n\r\nOur customers who got the property in our projects, they feel comfort and got higher appreciation of property value. All our Townships having good connectivity by Elevated Flyover, Express Highway and Integrated Master Plan Roads.\r\n\r\nNearest to our projects Software Companies, Hospitals, Star Hotels, International Schools and upcoming Domestic Airport, 300 Feet BMRDA Satellite town ring road which connects to International Airport, upcoming Metro Train.\r\n\r\nWe are having all our customers are Software Engineers, Doctors, NRIS, Engineers, Wing Commanders(Air force & Army). Chartered Accountants Within a short period we are launching higher end apartments in prime locations of Bangalore. ', 'Builder logo2015-12-12 10:34:40.svg', 565, 0),
('GM Infinite Dwelling (India) Pvt. Ltd', NULL, 'info@gminfinite.com', 'http://www.gminfinite.com/', '', 'Builder logo2015-12-12 10:38:35.svg', 566, 0),
('Godrej Properties Limited', NULL, 'aacharya@godrejproperties.com', 'https://www.godrejproperties.com/', 'Godrej Properties brings the Godrej Group philosophy of innovation and excellence to the real estate industry. The company is currently developing landmark projects in 12 cities across India. Established in 1990, Godrej Properties Limited is the first real estate company to have ISO certification. With projects that span across the country, the company\'s upcoming development covers over 8 million square meters. To create landmark structures, Godrej Properties collaborates with outstanding associates and reputed names. The company aims to deliver superior value to all stakeholders through extraordinary and imaginative spaces created out of deep customer focus and insight.', NULL, 567, 0),
('Gokaldas Lifestyle', NULL, 'info@gokaldaslifestyle.com', 'http://glindia.in/', 'At Gokaldas Lifestyle, we celebrate those who live life by their own blueprint. By pushing boundaries of design and innovation, we are the purveyors of boutique homes, custom-made for you and you alone. From humble beginnings in 1979, we have been focused on creating landmarks made of superior design, conveniences and facilities. With over 4 million square feet of Commercial and over 5 lakh square feet of residential space under construction across Mumbai, Bangalore and Hyderabad, we?re building lifestyles beyond compare.\r\n', 'Builder logo2015-12-12 10:43:35.svg', 568, 0),
('Gopalan Enterprises', NULL, 'digital@gopalanenterprises.com', 'http://www.gopalanenterprises.com/', 'Gopalan Enterprises was founded in 1984 by Mr. C. Gopalan, an architect, with the objective of developing and constructing residential apartments in Bangalore. The company is one of the prominent real estate developers in Bangalore and has developed some of the finest apartments in the city. It has the enviable record of having completed all its projects on time. The Group is engaged in the promotion of Residential and Commercial properties along with Shopping Malls, Townships, Special Economic Zones, Software Technology Parks, Biotech Parks, Organic Farms, Educational Institutions, Export of Culinary, Medical Herbs, Star Hotels, Hospitals, etc.\r\n \r\nWe offer 30 years? experience, building for an astute clientele from around the world. We take pride in the superior craftsmanship and attention to detail is given to every project we construct. Our reputation nests on professional management, exceptional product quality, overall affordability and client service throughout the building process. Our commitment, integrity, teamwork and professional excellence has continually seen the growth and success of the business since its inception in 1984.\r\n \r\nGopalan Enterprises bears the stamp of innovative design and superlative quality construction. The Group?s commitment to quality construction, modernity in architectural value, space management, and customer satisfaction has set up new trend among property developers in Bangalore and India. The Group has emerged as one of the competitors for promotion of Special Economic Zone, generating exports and employment. The group enjoys giant presence in the Knowledge City with and boasts of a prestigious global clientele.\r\n \r\nGopalan Enterprises brings you a combination of strong professional experience and a commitment to fresh, creative approaches for project planning and design.\r\nOur talents have been applied to projects throughout Bangalore. Gopalan is committed to providing the highest standard of professional service. We are dedicated to the philosophy ? Inspired by Life, Built on Trust.\r\n \r\nOur goal is to assist you by planning and designing facilities that meet your unique needs. We are supported by talented architects, interior designers, construction administration specialists, technical, marketing, customer support and administrative personnel in our endeavor.', 'Builder logo2015-12-12 10:56:28.svg', 569, 0),
('Greater Bangalore Eco Assets Pvt. Ltd', NULL, '', 'http://www.greaterbangalore.com/', 'Greater Bangalore Eco Assets Pvt Ltd, formerly Greater Bangalore Estates, one of the top builders and developers in Bangalore, established in 1996. We are pioneers in promoting and developing residential properties in Bangalore and its periphery. With a team of expert consultants and associates who possess technical depth, ethical standards and integrated expertise, we have succeeded in fulfilling the customers\' need for high appreciating properties with timely deliverables, while maintaining the finest standards of business practices in all its endeavors.', 'Builder logo2015-12-12 10:59:12.svg', 570, 0),
('Green Environs Projects', NULL, 'greenenvirons9@gmail.com', 'http://www.greenenvironsprojects.com/index.php', 'Green Environs Projects was founded a decade ago in the year 2001, just before the boom of real estate business in Bangalore. Over the last 10 years Green Environs Projects has firmly established itself as one of the leading developers of housing projects & real estate meeting the expectations of property buyers in Bangalore.', 'Builder logo2015-12-12 11:01:10.svg', 571, 0),
('Green Valley Builders', NULL, '', 'http://www.greenvalleybuilders.in/', 'Green Valley Builders the real estate developers who have built their business on solid foundation of trust acquired from decades of enterprise in allied fields, in India and Gulf country.\r\nAfter success full completion of their green view apartments, green orchard apartments and green meadow apartments they have now launched ROYAL GREEN Apartments which offers a lifestyle no other developer can boast of green valley builders have continued their track record of excellence.\r\nWith its adherence to schedule and fulfillment of promises, GREEN VIEW Apartments, Green Orchard and Green Meadow Apartments is a prime example of the promoters commitment.', 'Builder logo2015-12-12 11:03:34.svg', 572, 0),
('Greens Projects', NULL, 'sales@greensprojects.com', 'http://www.greensprojects.com/', 'Greens a privately held construction company founded by people with the expertise, the dedication and the passion to take on projects and complete them in time, on budget and to the highest standard of quality. Primarily focused on residential construction, Greens combines the knowledge, experience and skill of our staff with prequalified contractors to ensue value for money to our customers.\r\n\r\nWe have established a solid track-record with a belief in complete transparency and a friendly, down-to-earth approach to customers. \r\n\r\nWe assure you integrity and reliability.', 'Builder logo2015-12-12 11:08:33.svg', 573, 0),
('Greenshapes Developers (India) Pvt Ltd', NULL, 'investors@greenshapes.com', 'http://www.greenshapes.com/', 'We are Green Shapes Developers ? a cohesive and coherent team of competent and passionate professionals. The team is bound together by the vision of being amongst the top ten real estate companies in India within year 2020 by delivering significant values to customers, partners and people through design and execution excellence.\r\n\r\nCustomer is our world and whatever we do is meant to add value to our customers. It will not be exaggeration to say, we breathe in a culture of value consciousness.\r\n\r\nOur main differentiators are outstanding location of our properties (e.g. all our properties on four lane main roads), green innovations (e.g. low maintenance green design, CLC bricks made from cutting edge German Technology, Innovative Home Automations, no vehicular movement on surface?), transparency (e.g. accurate and timely status reporting, live feed from project site), backward integration (e.g. own CLC Brick Plants, own RMC plants, in-house architecture, design, planning and production teams ?) and consistent over-delivery in terms of quality, price and schedule expectation of the customer.\r\n\r\nCompany have been co-founded by Alok Kumar, an alumni of IIT Kharagpur (E & ECE B-Tech of 1995 batch) and an ex-Microsoft vetran who has been behind about 3.0 million Sqft of constructed residential area and over 75 acres of land bank in Hyderabad.\r\n\r\nWe set foot in Bangalore real estate in year 2003 and delivered our first project within 12 months. Since then we have never looked back. In year 2007, we moved to Hyderabad market. Today, we have 5 villa and 7 apartment projects running in Hyderabad under the brand name of Sankalp (www.sankalpcorp.in) and aakriti (www.aakriticonstructions.com) totaling to about 3.0 million Sqft of constructed residential area. We have come to Bangalore again in 2011 and in a short span of time, we have started two projects with total of about 0.5 million Sqft and another 2 projects worth 1.0 million Sqft in pipeline .', 'Builder logo2015-12-12 11:10:15.svg', 574, 0),
('GSS Project Consultants Pvt Ltd', NULL, 'gss@investinmysore.com', 'http://www.investinmysore.com/', 'GSS Represents Genuine, Safe and Secure and have been part of Business and Socio Community for more than 15 years. We help our customers in buying Legally Approved and quality Residential Plots, Apartments & Villas.', 'Builder logo2015-12-15 07:05:23.svg', 775, 0),
('Habitat Ventures', NULL, 'info@habitatventures.com', 'http://www.habitatventures.com/', 'Habitat Ventures builds world class living spaces for people with a zest for life. A deep understanding of market needs and individual aspirations combined with a breadth of technical knowledge has endowed the Group with the expertise to deliver exactly what the client wants, sometimes even outdoing industry benchmarks.\r\nEvery Habitat project is concluded harmoniously as it follows, from the start, a path of perfection blending quality, creativity, technology, aesthetics, sustainability and thoughtful amenities. Thus, even while enhancing functionality and value for money, these living spaces are a mark of elegance and grace.\r\nAnd thanks to the in-house expertise and the experience that the leadership has gained over the years in the areas of acquisition, conceptualization, architectural and interior design, construction management, marketing & property management, every time surpassing customer expectations has become a habit for Habitat staff. This is the very reason why Habitat has been consistently delivering state-of-the-art apartments and commercial projects in handpicked locations, thus reiterating its firm commitment to both clients and shareholders.\r\nInnovation has been the hallmark of Habitat offerings. This could be attributed to the fact that the Group is promoted by leading architects. Take for instance, the now popular villament concept. First offered by Habitat, this new model of community living gives clients the best of both worlds: an apartment and a villa. Today, many renowned and upcoming builders boast of similar offerings.', 'Builder logo2015-12-12 11:13:14.svg', 575, 0),
('Hebron Properties Pvt Ltd', NULL, 'marketing@hebronproperties.com', 'http://www.hebronproperties.com/', 'Hebron Properties Pvt. Ltd is promoted by Koshys Group, a reputed name with decades of experience in Real Estate, Health Care and Education Verticals in Bangalore. Today, Hebron Properties Private Limited is one of South India\'s leading developer & promoter of Villas, Residential and Commercial Properties. We are headquartered in Bangalore, with agents offices all across the Globe. We have a uniquely diverse multi-domain portfolio that covers Villas, Residential, Commercial, Property development, Developers of Apartments, Architects services, Interior Designing and various services required for residential, commercial and other units. Our projects extend across several major places in Bangalore.', 'Builder logo2015-12-12 12:00:09.svg', 576, 0),
('Hegde Developers', NULL, 'info@hegdedevelopers.com', 'http://www.hegdedevelopers.com/', 'We develop and offer spaces that are extra ordinary in luxury and ensure that it is affordable for our investors.\r\n\r\nIt is the vision of ours to provide up market facilities to our clients and create dwelling environment of superior standards, thus making our enclaves a landmark.', 'Builder logo2015-12-15 07:07:02.svg', 776, 0),
('Hexa Builders', NULL, 'sales@hexabuilders.com', 'http://www.hexabuilders.com/', 'Hexa Builders is a Bangalore based company, its primary focus on Residential projects and Real Estate promotion. The company is the headed by M.Chandrashekar, an architect from BIT (Bangalore Institute of Technology) and P.SivaPriya, a Marketing professional. Brand Hexa Builders is a relatively new name in the market , it is backed by promoters having over a decade of experience in building residential, commercial as well as industrial projects all over Bangalore.', 'Builder logo2015-12-11 10:55:40.svg', 474, 0),
('Hiland Reality Private Limited', NULL, 'info@hilandrealty.com', 'http://www.hilandrealty.com/', 'Close to a decade ago before its incorporation, the founding members of Hiland Realty Private Limited exclusively developed and sold properties for other firms in the industry. Through its history, Hiland has been a respected, real estate company in Bangalore. We have always been committed to provide at most satisfaction and an unwavering dedication toward transparency in dealings and ethical norms of business conduct.\r\n\r\nThis is delivered through the kind of service, value for relation towards customer as well as an Hilander, and business culture followed by us. The management team has enhanced the community and profession by serving in many regional capacities', 'Builder logo2015-12-12 12:14:24.svg', 577, 0),
('House of Hiranandani', NULL, 'bangalore@houseofhiranandani.com', 'http://www.houseofhiranandani.com/', 'Since our inception into India?s urbane terrains, we have upturned the way living spaces are designed. And with that, we have transformed the ethos and aesthetics of real estate in India.\r\n\r\nPillared by a unique approach to designing and planning, we invest heavily in research and development ensuring that each of our developments surpass industry benchmarks and redefine value engineering and design. Our name, since the beginning, has been associated with excellence and we are creating sustainable value for our customers, stakeholders, business associates, employees and society at every step of our development.\r\n\r\nThrough environmentally friendly concepts of New Urbanism, our focus has been on transforming suburban sprawls of land into well-planned urban communities that nourish an outstanding sense of living. And our developments stand as living proof. Apart from residences, we have established numerous schools, colleges, institutions and hospitals through affiliates, trusts, clubhouses and community spaces, all of which have earned unmatched international repute.\r\n\r\nSymbolic to our construction, our developments encompass the ether of the residents? lives entirely, by introducing retail outlets, hospitality centers, healthcare and educational institutions etc. within the realms of House of Hiranandani community. Our aim is to integrate every family into a bigger, more inclusive community.\r\n\r\nWe are cognizant of the fact that we have maneuvered successfully through a turbulent time in this industry?s history. As we move forward, we will carry our legacy forward proudly and will persevere to take it to new heights through a committed adherence to the values that we stand for. These values that manifest in our passion for perfection and endless innovation and advancements.', 'Builder logo2015-12-12 12:16:44.svg', 578, 0),
('Hoysala Projects Pvt.Ltd', NULL, 'salesbangalore@hoysalaprojects.com', 'http://www.hoysalaprojects.com/', 'Hoysala Projects has been changing the way people live, work and play since 1998. Founded by Mr. T.S. Sateesh, Chairman and Managing Director, it has quietly and organically grown into a leading real estate developer in South India. It did so by anticipating and fulfilling the existing market need for premium yet affordable housing, quality construction, open and transparent processes, on-time delivery, and uncompromising customer support. \r\n\r\nHeadquartered in Bangalore, Hoysala Projects is primarily focused on residential projects. Over the years, we have successfully delivered over 25 projects in Bangalore, Kochi and Kottayam. In Bangalore, Hoysala has created a niche for itself as a premier developer in North Bangalore. With the new international airport moving to North Bangalore, this is where real estate development is anticipated to shift in the future. As such, Hoysala Projects is well placed to offer the associated advantages that come with a high growth suburb. \r\n\r\nThoughtful planning and impeccable execution have always been the hallmarks of Hoysala Projects. Our residential projects include luxury and affordable apartments in strategic locations which are well-planned and delivered on time. It is this attention to detail and executional excellence that enable us to create a strong loyal customer base wherever we operate in. \r\n\r\nHoysala Projects has not only transformed the real estate landscape in North Bangalore, Kochi and Kottayam, it has also touched lives through its Corporate Social Responsibility initiatives. In 2011 Mr. T.S. Sateesh, our founder, Chairman and MD, established a charitable trust called the Sri Ranga Seva Trust in Magadi, Tirumala, which provides various social, educational & cultural services. ', 'Builder logo2015-12-12 14:03:42.svg', 579, 0),
('HRC Ventures', NULL, 'info@hrcventures.com', 'http://www.hrcventures.com/', 'HRC Ventures is an ensemble of experts who have made developing and building homes into an art. Established in 2007, the organisation has built a reputation of being a dependable organization that adheres to rules and regulations and keeping to timelines. \r\n\r\nHRC Ventures aims to build sustainable and aesthetically appealing homes that adds value to people?s lives. HRC develops homes that are known for their high quality of construction and world class amenities. HRC strives to maintain a high standard of excellence and professionalism in the real estate industry. HRC?s vision is to develop communities that people are proud to be a part of and homes that meet their every need.\r\n\r\nHRC?s mission is to provide high quality products at reasonable prices. To ensure that customers are always informed about every stage of the project. \r\n\r\nA fulfilling living experience is what you can be assured of at a HRC Ventures home. You can be sure that the property is not just built but sculpted by qualified & skilled professionals who can capture your vision & know exactly how to accomplish them. We are one of the very few developers who believe in integrating natural landscape with architecture. \r\n', 'Builder logo2015-12-12 14:07:24.svg', 580, 0),
('ICON Infra Shelters India Pvt. Ltd', NULL, 'info@iconhomz.com', 'http://iconhomz.com/icon-laurels/', 'ICON homz is a residential real estate developer based in Bangalore.\r\nICON homz focusses on ?value homes? , spread across gated developments, layouts, apartments, villas and town houses.\r\nCurrently, more than 1 million sft is under development, across 5 projects.\r\n\r\nICON homz Group is promoted by Rajasekhar Gowrineni (Chairman and CEO), Dr.Manmohan Reddy P (Director) and Chaitanya Kumar Reddy (Director).\r\nPromoters are experienced real estate professionals, with hands-on working experience in Land acquisition, approvals, construction, marketing and sales.\r\n\r\nICON homz group has a professional team leading key functions such as Marketing, Sales, Finance, Design, QS and Project Management.', 'Builder logo2015-12-12 14:10:31.svg', 581, 0),
('Indburgh Infrastructures', NULL, 'srinivas@indburgh.com', 'http://www.indburgh.com/', 'We are a Construction firm catering to a wide range of requirements within the residential, commercial and industrial sector. Since our inception in 2006 we have been on an ongoing journey catering to quality construction requirements, accomplishing tasks within the given time frame and basing our efforts into making the clients needs come true. Our expertise lies in the fact that we have a sound aesthetic background and qualified professionals who believe in executing every given task with utmost care, giving thought to every minute detail. Having dealt with challenging assignments we have brought to fore an experience that speaks of solidarity and consistency, establishing ourselves as capable Civil engineers and contractors.', 'Builder logo2015-12-12 14:25:47.svg', 589, 0),
('Indwin Developers', NULL, 'indwindevelopers@gmail.com', 'http://www.indwindevelopers.com/', 'INDWIN Developers is passionate and committed to delivering quality homes to customers while adding that extra dimension of lifestyle and returns on investments, consistently. With several successfully completed and ongoing projects in Bangalore, INDWIN Developers is fast emerging as one of the premium developers in Bangalore.\r\n\r\nOur engineers and supervisors are trained to give our clients the very best. It has over a decade of experience in property deveopment with Fourteen residential projects completed so far in Hyderabad & Bangalore and many more coming up in near future.', 'Builder logo2015-12-12 14:12:41.svg', 582, 0),
('Inner Spaces', NULL, 'sales@innerspaces.in', 'http://www.innerspaces.in/', 'Our goal is to build you a home for today as well as an investment that lasts for decades to come. At Inner Spaces, we build unique customer centric and smartly designed boutique homes. Our holistic approach to crafting lifestyles focuses on functionality and the quality of everything that we do. Values of honesty and trust are keystones to building relationships with our clients. We take pride in our passionate drive to create bespoke experiences that are delivered on time.\r\nOur designs are 100% compliant with building codes and a core concern for liveability, which we strive to carry forth in all our projects. We believe that the quality of lifestyle our customers expect go hand in hand with sustainability. Our buildings deliver better results with less energy consumption, lower maintenance cost and less water use.', 'Builder logo2015-12-12 14:14:50.svg', 583, 0),
('Insight Builders & Developers', NULL, 'info@insightdevelopers.com', 'http://www.insightbuilders.in/', 'It was a decade ago when the foundation stone for Insight was laid. Facilities management was the initial area of service which we set our foot into, with all kinds of odd jobs from changing water taps to repairing electrical home appliances were undertaken.\r\nWith passage of time and gain of experience, the name spread through word of mouth, bringing in more work in terms of quality, quantity and value. \r\nToday we are proud to serve clients with varied tastes and requirements. Over the years, we have bagged and executed large scale Civil, Electrical, Plumbing and Interior projects to the satisfaction of our customers.\r\nBecause of our keen interest to develop backward integration for the products and services required to construct GREEN BUILDINGS, Insight diversified into drilling and development of bore wells, manufacturing of energy efficient  elevators, water treatment plants, engineered formwork and automated building maintenance systems etc. \r\nInsight is headed by professionals who have years of experience, and expertise in project management. All these years of exposure gained from executing projects of varied size and nature has given the confidence to enter the domain of property development.', 'Builder logo2015-12-12 14:16:30.svg', 584, 0),
('Intact Developers India Pvt. Ltd', NULL, 'info@intactdevelopers.com', 'http://www.intactdevelopers.com/', 'Intact Developers India Pvt. Ltd. develops Boutique Apartments and Homes in select locations of Bangalore. We believe in delivering comfortable and quality living at affordable prices.\r\nIntact Developer has a team of highly qualified and dedicated professionals. With an ambitious vision, distinct mission and the well-structured principles of the organization, the team works on a precise management philosophy to understand the core competence and to harness its strengths. The team at Intact Developers India Pvt. Ltd. has developed standard operating procedures and systems, which are unique in the industry.\r\nExploring and learning in order to share information acquired through research with customers and associates has been a constant and continuous effort. The organization strongly believes in the principles of quality, integrity and professional transparency.', 'Builder logo2015-12-12 14:18:18.svg', 585, 0),
('Integral Foundations', NULL, 'manish@integralfoundations.com', 'http://www.integralfoundations.com/', 'The seeds of construction were sowed when the buildings were first constructed by hand or simple tools and were called Huts and shelters. Later professional craftsmen like bricklayers, carpenters appeared... ever since then construction industry has transformed at an extremely fast pace. Each step taking the industry way ahead and the modern day construction involves structures which have a mix of beauty, creativity, need & human intellect. Here at Integral foundation we have a complete team of architects, financial advisors, legal advisors & engineers who are capable of making your homing experience a complete one wherein we help you determine the right location for you. We promise that \"We have the guild to build\".\r\nRain or Shine, home is a place that offers us comfort & warmth like no other place in the world.', 'Builder logo2015-12-12 14:20:02.svg', 586, 0),
('ISR Projects', NULL, 'info@isrprojects.com', 'http://isrprojects.com/index.htm', 'ISR Constructions a group of MDVR Projects is a well established company in the real estate field having a vast experience and a proven track record of more than 15 years to its name. Led by its enterprising Managing Director, Indra sena reddy, it started off on its mission in the year 1996 in Hyderabad. With a grand vision in mind, a dedicated, skilled and quality team on hand, the group has developed and built premium properties and quality structures catering to the varying demands and needs of the customers.\r\n\r\nDuring its journey, the group has completed many successful projects thus adding to its ever-growing base and repertoire a huge list of satisfied customers.', 'Builder logo2015-12-12 14:21:46.svg', 587, 0),
('J P Constructions', NULL, 'sales@jpconstructions.co.in', 'http://www.jpconstructions.co.in/', 'JP Constructions is a leading construction companies in the city of Bangalore. The JP Constructions has over twenty years of experience in the building industry, having built several buildings since inception. Given the immense scope in the housing industry in India, the company has been focusing on promoting a series of large residential townships in several areas in the city. All the ventures promoted by the JP Constructions so far have been highly successful.\r\n\r\nThe company is proud of the reputation it has built for honesty and the quality of construction of its projects. Its management has set out a strict code of conduct to abide by in its dealings with customers.', 'Builder logo2015-12-12 14:23:53.svg', 588, 0),
('Jain Heights and Structures Pvt Ltd', NULL, 'sales@jainheights.com', 'http://www.jainheights.com/', 'A home is not just a brick and mortar building; it is a place that serves as your own personal sanctuary. Protecting you from the chaos of the outside world, it brings you the peace and happiness that you covet the most. At Jain Heights, we understand this need of your completely and so for many years we have come up with exquisite luxury homes that have elevated the standards of living here in Bangalore. Each and every project that we have undertaken has been meticulously planned so that they meet up to the global standards of stylish accommodations.', 'Builder logo2015-12-12 14:30:13.svg', 590, 0),
('Janaadhar (India) Pvt. Ltd', NULL, 'info@janaadhar.com', 'http://www.janaadhar.com/', 'OWNING A HOME IS NO LONGER A FAR-FETCHED DREAM \r\nJanaadhar is a complete housing solution company that offers budget homes in a well-planned, secure, self- contained township, where everyone can raise a family and senior citizens can live safely and independently. Janaadhar aims to build, high-quality homes - to create an inclusive society, and best quality life.\r\n\r\nJanaadhar has been floated by Jana Urban Foundation - a Section 25 Not-For-Profit company - for implementing budget housing projects.\r\n\r\nJanaadhar brings together the expertise of Jana Urban Foundation and Sterling Developers Private Limited in the breakthrough venture.', 'Builder logo2015-12-12 14:35:37.svg', 591, 0),
('Jayakumar Constructions', NULL, 'srinivas.jjgroup@gmail.com', 'http://www.jayakumarconstructions.com/', 'The foundation of Jayakumar Constructions extends back to 1980, to the vision and dedication of its founder SRI.S.T.JAYARAM a pioneer who is totally committed to quality both In terms of product as well as service. \r\n\r\nWe at JJ (Jayakumar Constructions), our vision is to create communities that integrate home, work and leisure with open spaces to live a fuller life. Our reputation and commitment is built upon delivering value homes over 35 completed projects in over 31 years. And our commitment is to deliver excellent homes matching you and your dreams which fruitiest your investment.', 'Builder logo2015-12-12 14:38:51.svg', 592, 0),
('Jeevan Builders', NULL, 'info@jeevanprojects.com', 'http://www.jeevanprojects.com/index.html', 'Headquartered in Bengaluru, India, Jeevan Builders strives to place architectural novelty and construction excellence at the heart of its every project. Effectively putting to use time tested building techniques, sublime designs and immaculate attention to detail in each project, the company crafts quality abodes to meet distinctive lifestyle needs of modern families.\r\n\r\nThe dedicated team of real estate professionals take care of the customers\' every requirement - from pre-sale queries and on-site feedback to post-sale follow up. The transparency in dealings and timely completion of projects add to the delight of being the company\'s customer. Simply explore the company\'s ongoing projects to be a part of the growing group of Jeevan Builders\' happy homeowners.', 'Builder logo2015-12-12 15:25:22.svg', 593, 0),
('JR Housing Developers Private Limited', NULL, 'info@jrhousing.com', 'http://www.jrhousing.com/', 'J.R Housing Developers Private Limited was established by Sri. S. Jagadishwara Reddy, who is the Managing Director of the company with over 20 years of experience in the field of real estate, he is also the vice president of Karnataka Land Developer\'s Association.\r\n\r\nThe company has delivered over 14 million square feet of beautifully developed residential properties to the satisfied customers spread all over India and abroad. The company has a huge clientele from different walks of life, while the majority being from the blue chip companies like Infosys, Wipro, Satyam, HCL, HP, IBM, Intel, Cisco etc, to name a few. Needless to say that the quality, maintenance and post sales support of the projects developed by the company has attracted this very high profile customers to invest in their projects. Thousands of happy clients are a testament to the reputation the company has earned today.\r\n\r\nThe company has been instrumental in setting benchmarks in developing residential projects in Bangalore Metropolitan Region by using well designed engineering plans, standard construction material and strictly conforming to the guidelines issued by the approving authorities. In fact we were the first to introduce 80ft wide main roads in the BMRDA approved residential layouts in Bangalore. JR Housing has a well trained team in each sphere of its business operations, be it engineering, architecture, layout development, construction, marketing, sales, accounts, front office and back office operations. In the process of growth and change, the company has created a brand name that stands for reliability, high professional standards and long-lasting customer relationships.', 'Builder logo2015-12-12 15:57:20.svg', 594, 0),
('Jupiter Infrastructure Pvt. Ltd', NULL, 'info@jupiterinfra.com', 'http://jupiterinfra.com/', 'We are an integrated infrastructure development company that builds communities based on two beliefs ? trust and complete professional solutions.\r\n\r\nWe are now growing into a PAN India entity, constituted by highly qualified and dedicated professionals predominantly with Defense background. We bring to the Business of Real Estate, values like transparency, ethical business practices and professional integrity, reflecting the virtues of the Armed Forces.\r\n\r\nWe seamlessly merge human resources and infrastructure together to build world?class communities. From understanding and addressing demands, adding tremendous brand value and legacy in to project execution, to low Operation Expenses, quality management and post sales services, we build a Community that is state-of-the-art and professional using in-house experts.\r\n\r\nOur backbone is a dedicated team of directors with defense background. Working with Discipline, priceless insight, understanding and professionalism is what we do.', 'Builder logo2015-12-12 16:01:12.svg', 595, 0),
('Kadam Group', NULL, 'kadamgroupindia@gmail.com', 'http://www.kadamgroup.com/', 'KADAM GROUP was founded in 2008, by MADHUSUDHAN RAO KADAM a civil engineering Graduate with 15 years of experience in the construction industry with a single-minded ethos, \"thinking spaces\". This intrinsic thought is the foundation of KADAM GROUP, where a skillful team creates functional spaces that speak to people. KADAM GROUP recognizes that space is fundamental, which is why we conceptualize, design and create unique, residential projects across the fastest growing city.\r\n ', 'Builder logo2015-12-12 16:03:12.svg', 596, 0),
('Kaman Holding Pvt. Ltd', NULL, 'info@thekaman.com', 'http://thekaman.com/', 'KHPL is one of the leading property acquisition company in Bangalore. The core business includes buying potential high yield properties, unlocking the value by identifying suitable joint venture partners (corporate developer or through FDI venture or Hotel Industrialists etc.) and real estate development. We have competent expertise and in-depth insight into Bangalore\'s property market. Value addition to the property has been our forte which was made possible through a strong team of Lawyers, Architects and Consultants. Incorporated in the year 2000, KHPL over the years has been successful in delivering quality products thus enhancing shareholder\'s value.', 'Builder logo2015-12-12 16:04:45.svg', 597, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('Karle Properties Builders and Developers', NULL, 'kp@karlegroup.com', 'http://www.karleproperties.co.in/', 'Karle Group, with a track record of above three decades, entered property development through Karle Properties, its property development division. The company?s first landmark project was ?Karle Premium? on Old Airport Road, Bangalore, in 2004. This project has been laid out in an area of over 100,000sq.ft. and has achieved high building efficiency that only a few buildings in the vicinity possess.\r\n\r\nThe second landmark project of the Group was completed in 2006 in Goa. Christened Karle Riverville, this project delivered over 91 sites in a gated community spread over 16 acres with amenities like a swimming pool, a club house and a well-equipped gym. This was the first project in Goa to use underground drainage with STP, and like this Karle introduced many more facilities and amenities as part of Regulatory, Statutory requirements keeping in mind users comfortable living . Even today Karle Riverville remains a benchmark in Goa for quality and facilities.\r\n\r\nNow the Group is all set to launch Karle Tranq?ville, its new project in Goa, offering high-end luxury Mediterranean/ Spanish beach theme Villas, designed by international architects.\r\n\r\nKarle, whose motto is to \"Deliver Superior Value to Customers\", has now made its entry into Mysore with the launch of KARLE HABITAT Residential Layout. The project offers 178 Plots of various sizes, spread over 13 ? acres approved by MUDA with no compromise on specifications.\r\n\r\nYet another landmark project, KARLE SUMMIT Yercaud Salem district Tamilnadu. exquisitely designed residential plots that rises high above not only in altitude but also above the ordinary. strategically located in the heart of the sleepy little Hill Station, close to the Lake and the Killiyur Falls, A heavenly privilege that will be available only to a select few.', 'Builder logo2015-12-12 16:06:07.svg', 598, 0),
('Kataria Builteck Pvt. Ltd', NULL, 'sales@katariabuilteck.com', 'http://www.katariabuilteck.com/', 'M/s Kataria group was founded by Late Sri. Ram Swarup Kataria and has been involved in promotion of flats and building construction for the past two decades. Mr. Ved Prakash Kataria S/o Late. Ram Swarup Kataria, who is a Diploma in Electrical Engineering is presently the Managing Director of M/s KATARIA BUILTECK PVT LTD. having its registered office at no.922 - A , Hemkunt Chamber 89, Nehru Place New Delhi - 110 019. The group has already successfully completed various residential apartments projects in Delhi, Bangalore, Punjab, Haryana, Jaipur and Kolkata.\r\n', 'Builder logo2015-12-12 16:08:26.svg', 599, 0),
('Keerthi Estates Pvt. Ltd', NULL, 'salesblr@keerthiestates.in', 'http://www.keerthiestates.in/', 'Keerthi Estates, established in 1990 was founded by Mr. Anil Kumar Reddy. The guiding principle of Keerthi has been its commitment to excellence, innovation and prime quality in the spaces it creates. Over the last two and half decades, Team Keerthi has grown in manifold ways and has specialized expertise in building homes across categories which provide enhanced value per square feet. The ever changing community needs inspire Keerthi to create endless experiences for myriad clients looking to settle in a new and refreshed residential space in every aspect.\r\n\r\nFrom High-End luxury homes to modest residences , we provide a holistic approach to help you choose a home that fits your needs. Today?s customer desires for a comfortable, luxurious home that one can come back to and spend important moments with his/her family within a secure environment. Our efforts are to primarily re-create this luxury, provide comfort and support to families, in every possible way.\r\n\r\nKeerthi Estates has made a name for itself in constructing innovative residential and commercial spaces in Hyderabad and Bengaluru. With over 6 million square feet constructed, and more than 4500 happy Keerthi families, our efforts are to continue re-creating luxury in Indian spaces.', 'Builder logo2015-12-12 16:10:29.svg', 600, 0),
('Kiran & Kashi Constructions', NULL, 'kirankashi@vsnl.net', 'http://www.kirankashi.com/', 'Kiran & Kashi Constructions  commands more than a decade of Construction Expertise,a part of K.V.Narayan & Group of companies\r\nThe  Managing Partner, S.Kashinath has been associated with K.V. Narayan Group for more than 2 decades.\r\nIn his capacity as Executive Director, K.V.Narayan & Swarup Group, he has successfully completed several Housing & Commercial projects. Being an Engineer, he has delivered the projects on time, every time.\r\nAlong with Partner & Wife, Smt. Kiran Kashi, it has been their Motto to offer high quality Residences at lowest cost without compromise on quality.\r\nUnder his Aegis & inspiring Leadership, many flats have been sold, by his team of dedicated Engineers & Staff.\r\nThe K.V.NARAYAN GROUP is a Bangalore based Company with more than four decades of Business Experience.\r\nUnder the enthusiastic guidance of Sri. K.V.Narayan, the Companies are thriving in their respective fields.', 'Builder logo2015-12-12 16:12:30.svg', 601, 0),
('KMB Estates', NULL, 'info@kmbestates.com', 'http://www.kmbestates.com/#', 'KMB group ? KMB is a diversified group with investments in retail, distribution and real estate. Founded in 1988 as a manufacturing company KMB soon grew to be one of India?s largest watch strap manufacturers. Today KMB has under its wing leading companies in retail, distribution and real estate.\r\n\r\nKMB\'s core values of delivering experiences and products that are deeply innovative, valued by our customers and contributive to the local society have led to an organisation that is committed everyday to delivering iconic products and happiness to our customers through inclusive, design-driven decisions.', 'Builder logo2015-12-12 16:13:43.svg', 602, 0),
('KNS Infrastructure Pvt. Ltd', NULL, 'enquiry@knsgroup.in', 'http://www.knsgroup.in/', 'Was formed in the year 2007 by Mr. K. N. Surendra with his vast knowledge and expertise in plotted development in Bangalore real estate sector with the objective of developing and adopting world-class quality at truly affordable prices with the persistent focus on timely delivery.\r\n\r\nKNS Infrastructure?s vision is to provide world-class real estate experience and quality at truly affordable price in Bangalore.\r\n\r\nOver last ten years, KNS Infrastructure has built a rock solid, credible reputation in Bangalore?s real estate industry with its supremely dynamic and competitive sphere of residential property development. The pioneering force behind the innovation-led, future-focused, connected infrastructure development of KNS Infrastructure Private Limited is K N Surendra, who has an ambitious dream to change the way people live a contemporary home in Bangalore with luxurious amenities.\r\n\r\nKNS Infrastructure is in the forefront of aggregating and developing BDA and BMRDA approved large land holdings into luxurious residential layouts. The company is rapidly changing the skyline of residential plot developments in Bangalore and aim to be the leader in the affordable premium home segment.\r\nIts residential projects comprise presidential plotted development with modern landscaped gardens with modern amenities. At its core, the company works towards enabling greener infrastructure development and strongly emphasize on environmental management, rainwater harvesting and ensure high safety standards across its residential projects.\r\n\r\nKNS Infrastructure?s pace of progress and the sheer volume of its offering reflect its passion, commitment and expertise of advanced global infrastructure development. The company remains committed to delivering world-class projects empowered by a team of highly committed and skilled resources using state-of-the-art technology. An amalgamation of the company?s key strategic initiatives has elevated it to the pinnacle of excellence mirror through its work.\r\n\r\nThe company takes pride of its successful 10 plotted development projects covering around 7.2 million square feet of plotted area and five ongoing projects with around 3 million square feet of plotted area on Mysore road, Electronic city, and Sarjapura. BDA approved project developer KNS Infrastructure is also working on 13 upcoming residential projects with around 5.2 million square feet of plotted area.', 'Builder logo2015-12-12 16:18:18.svg', 603, 0),
('KRK Ventures', NULL, 'sales@krkventures.in', 'http://krkventures.in/about-us/', 'KRK Ventures is a legacy which has been inherited from Aishwarya Properties ? a trail blazing company established in 1996 known for its adherence to quality, integrity and timely delivery. Both KRK Ventures & Aishwarya Properties are a part of a conglomerate Athlur Group. KRK in keeping with the lineage constantly seeks new ways of providing its customers with best solutions through properties which are symbolic of quality and value.\r\n\r\nEvery single customer becomes a part of the KRK family and they speed up the wheels of growth by referrals and recommendations. Only because they are happy with the solution that they have built together with KRK Ventures.\r\n', 'Builder logo2015-12-13 06:18:26.svg', 604, 0),
('KSR Properties Pvt. Ltd', NULL, 'sowmya@ksrproperties.in', 'http://www.ksrproperties.in/index.html', 'Our journey has been one that has been INSPIRED by a zeal to deliver some of the best homes in the country. A journey which began in 1999 that saw some breath-taking designs at Sunny Isles. The TRANSITION into the current projects has been one of the most fulfilling experiences that has seen developments across Bangalore, Chennai and Vizag. Our EVOLUTION into being one of the renowned names in the industry is making us stretch our limits and deliver \'world-class\' communities. With INNOVATIVE approaches in design and construction, we are in a position to challenge the best in the class. With these directions, we are sure that you would be ATTRACTED to us.', 'Builder logo2015-12-13 06:20:29.svg', 605, 0),
('Kumar Properties', NULL, 'bangalore@kumarworld.com', 'http://www.kumarworld.com/index.php', 'The KUMAR PROPERTIES Group is a well diversified, value driven enterprise.\r\nFounded by Mr. K.H. Oswal on 15 August 1966, the group initially catered to the rental segment.\r\nIn the ?70s it progressed to construction of branded apartments. \r\nIn the ?80s it shifted to high-rise buildings and bungalow projects. \r\nIn the ?90s it pioneered luxurious, mega complexes.\r\n \r\nAt the turn of the millennium, Kumar Properties diversified into IT parks and commercial buildings, With a strategic shift to large townships and commercial malls, the journey is far from over. In fact, it has just begun.\r\n \r\nMore than 27,000 satisfied and happy families are our brand ambassadors and our true strength.\r\nOver 27,000 happy families from diverse socio-economic and cultural background, over 1,000 satisfied large, medium and small businesses, all enjoying the comforts and pleasures of living in environment-friendly, well-designed, fully-equipped residential complexes, or working out of swanky commercial premises. That is the Kumar Properties? record of achievement in core service offerings. A record of not mere construction, but creation of complete living, working and business spaces, with the latest amenities and facilities integrated in a holistic manner and backed by lifelong maintenance.', NULL, 606, 0),
('Kumari Builders and Developers', NULL, 'contactus@kumaribuilders.com', 'http://kumaribuilders.com/', 'Kumari Builders & Developers is a construction fully packed with experienced professional and we are establish in Bangalore. At Kumari, we are helping to build to Brighter tomorrow by making a better day we keep things simple yet beautiful. Every project needs strong ideas and clients need people who can help own a dream home in real world it\'s a pleasure for us to share our passion with you. Therefore we are delighted that you found a your way to our portfolio and your road to won a dream home. great design and projects last forever.', 'Builder logo2015-12-13 06:53:17.svg', 607, 0),
('Laasya Projects Pvt. Ltd', NULL, 'Info@laasyaprojects.com', 'http://www.laasyaprojects.com/', 'Laasya Projects - A Premier Name In Real Estate Sector Credited With Numerous Premier-Class Projects \r\nWith years of experience in real estate sector, Laasya Projects is one of most reputed companies carrying out top notch developmental projects in prime locations to bring about remarkable transformation in the real estate scenario. We, at Laasya Projects, based at Bangalore, are consistently engaged in carrying out world-class residential projects for fulfilling the dream of having your own home, packed with luxurious amenities. \r\n\r\nOur primary effort is to conceive and design affordable projects with apt strategies culminating into high class residences. In pursuance of this objective, Laasya projects, utilizing its technical expertise, skill and imagination has successfully completed its residential projects in Bangalore, earning the appreciation of customers for our professionalism and quality services. \r\n\r\nOur accomplished team of experts is engaged in carrying out numerous operations including identification and acquisition of land, strategizing, designing, promoting and executing customer oriented projects. \r\n', 'Builder logo2015-12-13 06:59:51.svg', 608, 0),
('Legacy Global Projects Pvt.Ltd', NULL, 'info@legacy.in', 'http://www.legacy.in/corporate.html', 'Legacy was founded on the cornerstone of quality living. A vision of creating world-class living environments with the promise of a better quality of life for the people who inhabit them. Our expertise allows us to create living spaces that are testaments to meeting your needs and offering you the best in construction. Our trademark style, sense of flair and unforgettable opulence underline the high standards for which we are known. These standards are constantly upgraded by our high-calibre team, making every living environment a source of pride and joy for the owner. Our combined experience guides us in the ideation, design and execution of living spaces that are otherwise unparalleled. Our beautifully crafted doors are always open in welcome, if you would like to explore our world: visit our projects, meet our people and discover how we work.', 'Builder logo2015-12-14 04:22:08.svg', 613, 0),
('Live Spacess', NULL, 'sales@livespacess.com', 'http://livespaces.aspireis.in/index.aspx', 'Live Spacess, headquartered in Bangalore and primarily focused on residential, commercial and contractual projects.\r\n\r\nOur residential projects include extravagant apartments, villas, row houses, and plotted development complete with foremost amenities.\r\n\r\nWe strongly emphasis and promote on environmental management, water harvesting and high safety standards in all our residential endeavors.\r\n\r\nIncepted since 10 years, we have endeavored for excellence in quality, customer centric approach, value for money, pioneering engineering, uncompromising business ethics, timeless values and transparency in all spheres of business conduct transforming LIVE Spacess a preferred real estate trust worthy brand.\r\n\r\nProject Execution and Project Management capability is one of our core strengths. As of March 30th 2014, LIVE Spacess has completed 50 construction projects inclusive of both residential and commercial.\r\n\r\nThe Company currently has 2 ongoing residential projects aggregating to 38 thousand square feet of developable area. LIVE Spacess has made a footprint in multiple parts of the city in Bengaluru.\r\n\r\nWe warmly invite you to explore our world: visit our projects, meet our people, discover how we operate & deliver and what we have achieved. This website, though large, is easy to navigate intuitively. If you need specific direction, you might find it useful to refer to the site map or \'footer\' at the bottom of the page.', 'Builder logo2015-12-14 04:26:37.svg', 614, 0),
('Lotus Developers', NULL, 'lotusecoprojects@gmail.com', 'http://lotusdeveloper.co.in/', 'With a vision of turning dream homes into reality, Lotus developers is currently managed by Two partners, Mr. DEVIPRASAD REDDY and Mr. VENUGOPAL REDDY who bring a combined experience, knowledge and zeal to firmly stamp the LOTUS DEVELOPERS brand in Bangalore.\r\n\r\nLotus Developers, we believe that the pride and passion we invest in building your home is surpassed only by the pride and prestige you enjoy in owning it. Our focus on precise details, innovative design, quality construction and helpful financial options enhances the quality of your life.', 'Builder logo2015-12-14 04:29:24.svg', 615, 0),
('M/s. Pramur Constructions Pvt. Ltd', NULL, 'md@pramur.com', 'http://www.pramurgroup.com/', 'Pramur is heading towards it\'s silver jubilee in 2016. In these past two decades, the company has earned the coveted status of ?Reputed & Dependable?. Several important buildings dotting the landscape of Mysore carry the tag ?Built with Pride by Pramur?. \r\n\r\nP ?  Progressive : Since its inception, Pramur has grown over 25 folds.\r\nR ? Resilient	: Has weathered all odds in its journey to date.\r\nA ?  Achieving : Has recorded good growth and received many awards & applause.\r\nM ? Motivating : Has motivated many young minds to reach higher levels both as professionals and entrepreneurs.\r\nU ? Unique : One of the few construction companies to secure I.S.O. Certification and has received ?Partner in Progress? status from many corporates.\r\nR ?  Reputed : Consistent quality, transparency and honesty have resulted in acquiring the ?Reputed? status in the industry.', 'Builder logo2015-12-15 07:09:18.svg', 777, 0),
('Maangalya Developers', NULL, 'Sales@mangalyagroup.com', 'http://www.maangalyadevelopers.com/', 'Maangalya is a professionally managed property development company that is part of an NRI Group based in Singapore and with extensive operations in Malaysia, Indonesia, Philippines and India. In India, Maangalya is based in Bangalore, with a presence in Chennai and Madurai. Over the years, Maangalya has created a number of prestigious residential and commercial projects across India and abroad.', 'Builder logo2015-12-14 04:34:32.svg', 616, 0),
('Mahabaleshwara Promoters and Builders Pvt. Ltd', NULL, 'sales@mpb.co.in', 'http://www.mpb.co.in/', 'Mr. K. C. Naik, the founder, started the operations of the group in 1961 with an pioneering entry into the transport business. The years ahead saw the company venture into the challenging area of real estate with the launch of Mahabaleshwara Builders and Promoters, now regarded as the Group\'s flagship company. Complementing Mahabaleshwara\'s construction and transportation operations are other ventures like Sarosh Technical Services which is a company involved in the manufacturing of precured tread rubber and the Group\'s two successful retail stores - Kaycien Sales and Classique Supermarkets', 'Builder logo2015-12-14 04:37:57.svg', 617, 0),
('Mahaghar Properties', NULL, 'info@mahaghar.co.in', 'http://www.mahaghar.co.in/', 'Mahaghar Properties Pvt Ltd was started in 2009 by able entrepreneurs Mr. Purushotham Reddy and Ms. Kanya Kumari with an aim to provide ?One Stop Solution for Housing Needs?. Mahaghar Properties Pvt Ltd offers a comprehensive portfolio of apartments and villas in Bangalore and Mysore with finest residential locations. Known for its speedy and quality construction, Mahaghar homes are fast altering the cityscape. Mahaghar has 7 projects in various stages of completion, to cater to a large chunk of the real estate investment crowd.\r\n\r\nMahaghar Properties Pvt Ltd has grown steadily since its inception. The company?s intention is to have a sustained and managed growth by maintaining a low debt-equity ratio. The company?s success lies in its ability to attract and retain experienced personnel with its good HR policies and thus keeping the attrition of employees to the lowest level. Mahaghar Properties Pvt Ltd believes that the prosperity and progress of the organization depends on the well being and happiness of its employees apart from its customers. Also, by being extremely price competitive, highly quality conscious, executing only premium projects packed with life style living amenities and by providing world class Sales and After Sales environment to all, small or large.', 'Builder logo2015-12-14 04:41:57.svg', 618, 0),
('Mahendra Homes Pvt. Ltd', NULL, 'info@mahendrahomes.com', 'http://mahendrahomes.com/', 'We are an infrastructure development company headquartered in Bangalore and we have progressed beyond leaps and bounds under the inspiring leadership of Sri. B. T. Nagaraj Reddy. Our promoters come with vast exposure and experience in the field of construction products and real estate.\r\n\r\nMahendra Homes has a team of committed professionals with vast experience in development of villas and apartments, with hands on experience in Quality Planning, Designing and Construction. We are not only builders but your building partner with our own vertical of infrastructure materials like Ready Mix Concrete; Blue Metals with Brand Bharath Cement Products & Bharath Blue Metals. With most of the requirements fulfilled by our own companies, we are able to maintain the best of quality and timely delivery at astonishing prices, which again goes to say that we value your TRUST over and above everything else. We stand for greater emphasis now on spiritual well being.\r\n\r\nOur work describes the outlook of life aesthetically and spiritually besides being ever so well balanced for a harmonious living. Having the science of Vaastu applied to all our work, we ensure that we combine our ethos and modern requirements in one great symphony of construction.', 'Builder logo2015-12-14 04:45:13.svg', 619, 0),
('Mana Projects Pvt. Ltd', NULL, 'enquiry@manaprojects.com', 'http://www.manaprojects.com/', 'The real estate industry is intensely competitive. Preparedness, foresight and a consumer centric approach are essential if one aims to gain the trust of the customers. These are the qualities that set apart the best in the business from the rest. And at Mana Projects, our well organized market presence, strong and diverse portfolio, streamlined manufacturing capabilities and cost effectiveness gives us the edge to not just compete but rise above the competition.Building quality residences for people is a huge responsibility and we endeavor to fulfill this duty with emphasis on efficiency in operations, retliability and a focus on imaginative engineering. We believe that meeting and exceeding expectations in all phases of the construction process is the true definition of quality. Which is why we personally oversee every detail and establish frequent communication with all parties involved, both on and off the job site, ensuring a quality job, every single time.\r\n\r\nThe success and eminence of Mana Projects began as my dream but has now been transformed into the dreams of many. And every day, we strive to make not just our dreams come true but also the dreams of every person looking to buy a home.', 'Builder logo2015-12-14 04:47:24.svg', 620, 0),
('Manani Projects Pvt. Ltd', NULL, 'info@mananiprojects.com', 'http://mananiprojects.com/', 'Manani Projects Pvt. Ltd is a full service company into Development of residential and commercial projects in Bangalore and across karnataka. We also provide assistance in land valuation, project marketing, investments, corporate solutions, legal services and research services. The company is known for maintaining international quality, construction standards and also for its ethics, transparency, reliability, professionalism and reflexivity.', 'Builder logo2015-12-14 04:49:59.svg', 621, 0),
('Manar Developers Pvt. Ltd', NULL, 'pureearth@manardevelopers.com', 'http://www.manardevelopers.com/', 'Manar Developers is a Private Limited Company known for building and developing quality buildings and flats. The promoters of the Company are in the construction business for the past 20 years. Manar Developers (P) Limited, a name trusted for quality, we have the experience and expertise to turn your desires and dreams in to reality.', 'Builder logo2015-12-14 04:54:04.svg', 622, 0),
('Manasa Developers', NULL, 'sales@manasadevelopers.com', 'http://manasadevelopers.com/', '\"Manasa Developers\" is emerging as leading name in rapidly developing tier 2 city Mysore. It is premier construction group in development of residential apartments, residential layouts, villas and other construction activities. The firm has disciplined work culture and adopted to modern methodology and innovative design and architecture.', 'Builder logo2015-12-15 07:11:07.svg', 778, 0),
('Mantri Developers Pvt. Ltd', NULL, 'Enquiry@mantri.in', 'http://www.mantri.in/', 'Mantri Developers Pvt Ltd In the supremely dynamic and competitive sphere of property development, one name has consistently been a trail blazer, setting the trends for others to follow. The innovation-led, future-focused Mantri Developers Pvt. Ltd.Established in 1999 by Mr. Sushil Mantri, the company has been the pioneering force behind the rapidly changing skyline of south India, with developments that span the residential, retail, commercial, education and hospitality sectors.\r\n\r\nIn just 16 years, the group, with the spirit of innovation at its core, has carved a niche for itself as an industry benchmark for quality, customer focus, robust engineering, in-house research, uncompromising business ethics and the unswerving commitment to timeless values and total transparency in every aspect of its business. These exceptional attributes have made Mantri Developers one of India?s most preferred real estate brands in south India.\r\n\r\nAs part of its diversified portfolio that is cumulatively spread over 20 million sq. ft. under various stages of construction in residential, retail, office, hospitality, and townships in the high-growth urban centres. Under residential segment, Mantri Developers offers villas, row houses, super luxury apartments, luxury apartments and semi-luxury Apartments.The Company has an enviable track record of having delivered 1.4 homes, every single day. Mantri Developers is committed towards developing ecologically sustainable projects, with a strong emphasis on environmental management and safety standards. The company is also in the forefront of using cutting-edge technological innovations like home automation systems to complement the state-of-the-art architecture of its smart home projects.', 'Builder logo2015-12-14 04:56:39.svg', 623, 0),
('Marvel Infrabuild Pvt. Ltd', NULL, 'crm@marvelinfrabuild.com', 'http://marvelinfrabuild.com/', 'Built by Passion. Driven by Values.`This core philosophy lies behind our every action.\r\n\r\nWe are passionate about setting new architectural benchmarks, and transforming the cityscape with our comfortable homes and energized workspaces. Our hunger for new ideas and our willingness to experiment and develop innovative solutions have helped us redefine functionality and expertise.\r\n\r\nOur passion is ruled by a strong value system. Despite being a young company, our uncompromising business ethos, customer-centric operations, absolute transparency, emulative quality standards and robust engineering have earned us a reputation for being reliable.\r\n', 'Builder logo2015-12-14 05:02:26.svg', 624, 0),
('MCB Aassetz', NULL, 'enquiry@mcbaassetz.com', 'http://mcbassetz.com/', 'MCB Aassetz provides service in the field of Properties for over seven years. We make the Real Estate Investments Easy, Effective and Efficient for our Clients. We provide impeccable service to real estate clients and Investors, maintaining and enhancing the asset value for investors while providing world class facilities. Honouring time-bound obligations has been our strength right from the beginning and this legacy will continue. Our approach towards management of the real estate portfolio from an owner?s perspective has made us deliver unparalleled services.\r\n\r\nMCB Aassetz has the skill and knowledge to evaluate a property and create the optimum strategy to achieve highest customer satisfaction. We strongly believe that our success lies in our customers short and long term success; we reflect this in every transaction that we do ? small or big. Our Commitment to our clients has helped us build strong bonds. This impressive growth will no doubt power us to greater achievements in the coming future. We leave no stone unturned in our effort to offer time bound projects. With Offices and associates in Chennai, Bangalore, Trichy, Ooty, Dubai and Singapore aptly managed by a hand picked team of dynamic professionals, its mission is to become the leading Real Estate developer in INDIA.', NULL, 625, 0),
('MDS Projects', NULL, 'mds.dineshtv@gmail.com', 'http://www.mdsprojects.in/', 'The remarkable Portfolio exemplifies style, class and a profound sense of imagimnation. It includes everything from extravagant township, to magnificent residential and commercial complexes.\r\nThe company is driven towards creating benchmarks in real estate by delivering nothing short of pure excellence. Some of their grand residential projects around Bangalore include names like Eden Garden, Eden Greenz, Bliss City, Eden Park, Olive Garden, etc.\r\nMDS Projects is not just about building homes, but creating havens to encompass every desire and dream. It seeks to understand every human emotion and create a luxurious lifestyle around it.', 'Builder logo2015-12-14 05:13:26.svg', 626, 0),
('MDVR Projects Pvt Ltd', NULL, 'mdvrprojects@gmail.com', 'http://www.mdvrprojects.com/', 'MDVR Projects have been developing residential projects and has grown to be one of the prominent developers in Bangalore. Every projects developed by MDVR Projects emphasis on quality, excellent livelihood, environment on time delivery. As a highly customer satisfaction driven company MDVR Projects aims to provide top technical excellence, impeccable quality and clear titles with a concern for surroundings and the society at large. It is a profession-managed company backed by an in house team of talented executives, administrators and dedicated designers and engineers.', 'Builder logo2015-12-14 05:21:23.svg', 627, 0),
('Meda Group', NULL, 'info@medastructures.com', 'http://www.medastructures.com/', 'From our humble beginnings, as a first generation company, we have learnt that the pillars for a strong corporate foundation are transparency, ethics and values. At Meda Structures our guiding light is to deliver best in class tenements on these basic tenets. There cannot be any compromise because owning a house is a matter of pride and a lifelong ambition. We believe that, through our commitment to the customer, we are forging communities of the future.\r\n\r\nOur mission is to become a leading realty house by meeting promises such as timely delivery with emphasis on quality. We are different because we do not claim to hold land parcels and do not claim to have changed the realty industry. As entrepreneurs we can promise you that we want to make it big with the blessing of every home owner?s smile. So come be a part of our story.', 'Builder logo2015-12-14 05:23:49.svg', 628, 0),
('Midtown Structures', NULL, 'info@midtownstructures.com', 'http://www.midtownstructures.com/', 'Midtown Structures is a real estate developer based in India with its initial foray being in the city of Bangalore.\r\n\r\nThe IT hub of India continues to be the leading destination in the world for outsourcing of jobs by large MNCs and consequently there is NO slow down in the demand for \"affordable\" residential apartments in Bangalore.\r\n\r\nThe real estate industry is in a state of transition with a more aware customer base, demanding more for their homes. Our goal is to provide our customers with homes that are designed to make optimal usage of floor space with materials that are durable, in vogue and ensures a high standard of safety.\r\n\r\nIt is said and very rightly so, \"A project gets delayed one day at a time!\" With our past experience with businesses where the difference between success and failure is only linked to operational excellence, we are confident that our focus on costs, operational excellence and timely delivery will provide the keys to happy customers.', 'Builder logo2015-12-14 05:32:18.svg', 629, 0),
('MIMS Builders Pvt. Ltd', NULL, 'info@mimsbuilders.com', 'http://www.mimsbuilders.com/', 'We are a Bangalore based housing brand, well-known for creating quality life-style housing, for over a decade Today, we are a brand synonymous with luxury and design. Having successfully delivered 10 projects, we are currently working on the 11th.\r\n\r\nWe are committed to deliver the best to our home owners. Built on the strong foundation of trust and transparency, we have carved for ourselves a fine niche in Villa and Enclave properties.\r\n\r\nWith an expertise earned after years of Villa delivery, we have now forayed into building extraordinary apartments with MIMS Habitat & MIMS Renaissance. Like our villas, our apartments are multi-faceted, artfully planned homes that range from catering to the needs of a first- time home buyer to indulgent and luxury living.\r\n\r\nWe work hard in giving you a home that is like none other. We believe in keeping promises, be it that of the quality or that of the delivery date.\r\n\r\nOur craftsmanship ensures architectural perfection in each of our projects. Our contemporary designs artfully incorporate traditional aspects, thus bringing you the charms of an old Bangalore packed beautifully in the luxury of a new-age home.\r\n\r\nThe expansive landscapes that our homes are nested in let you bask in a quite haven far from the city?s mayhem. Beautiful gardens, pools, trees and green spaces adorn our homes be it villa projects or apartments.\r\n\r\nWe go beyond building homes. We bring to you communities with living spaces and friendly neighborhoods that grow close to your heart.', 'Builder logo2015-12-14 06:34:53.svg', 630, 0),
('MJR Builders Pvt. Ltd', NULL, 'projectinfo@mjrbuilders.in', 'http://www.mjrbuilders.in/', 'Vision\r\nOur core aim is to continue to strive and provide top quality residential and commercial spaces that delight. And, become the most successful real estate company.\r\nmission\r\n\r\nMission\r\nAs a real estate company, our focus is to provide delightful spaces to our customers, unmatched opportunities to our employees and maximum value to our partners and clients.\r\nvalues\r\n\r\nValues\r\nTo focus on solutions and offer innovative residential and commercial spaces. And, assist clients in property buying, selling and renting decisions.', 'Builder logo2015-12-14 06:39:15.svg', 631, 0),
('Monarch Properties', NULL, 'enquiry@monarchproperties.in', 'http://monarchproperties.in/', 'What started out as a dream in 1988 has, over the last two decades, turned into a gratifying reality. A reality which bears testimony to the passion, dedication and hard work that we invest in everything we do.\r\n\r\nThe name Monarch brings with it a sense of familiarity. Actively revolutionising the industry for the last twenty two years, we have influenced trends in the past and are strategically placed to provide for future needs.\r\n\r\nWe are a multi-segmented, family owned company, based out of Bangalore, and over the years, we?ve expanded our work to diverse fields. Our operations range from Hotels, Properties & Investments, Travel & Tourism to Property Management services, areas in which we now command a reputable market share. Some of our established work consists of Commercial, Residential and Hospitality projects.\r\n\r\nWith many years of experience forming the basis of our work, we are also a member of CREDAI-Bangalore & Indian Green Building Council, and are aggressively positioned to grow in the coming years.', 'Builder logo2015-12-14 06:43:07.svg', 632, 0),
('Motzkin Group', NULL, 'enquiries@motzkingroup.com', 'http://www.motzkingroup.com/', 'The Motzkin Group comprises of Industry experts and entrepreneurs in the Real Estate, IT and Hospitality sectors. Our combined expertise lies in large scale residential projects, Hotel Chains and Global IT Consulting companies making us privy to managing multi-million dollar engagements while exceeding customer expectations. It\'s this ideal mix of experience coupled with proven delivery that marks our common mission.\r\nTo build qualitative living spaces for our customers. By creating ultra-luxury living spaces and communities to reflect their aspirations in the most prime and pristine areas of Bangalore along up-coming IT corridors and new age urban facilities. Packaged with comfort, with a promise of spiraling values.\r\nOur vision is to provide premium and excellent quality homes to the burgeoning upper middle/middle class, starring premium facilities, eclectic architectural styles and smart-home features. To the investor class too these premium properties will prove a gold mine of investment because of assured escalation values. At present our operations are focused in and around Bangalore with long-term objectives of growth in other regions too.', 'Builder logo2015-12-14 06:49:38.svg', 633, 0),
('MSK Group', NULL, 'vittal@mskshelters.com', 'http://www.mskshelters.com/', 'MSK Group have one of the best teams of real estate development professionals working on its various projects from Residential Sites to Villas and Residential Apartments and from facilities planning to interior designing and implementation.MSK Group is into implementing turnkey services to its customers across a very wide spectrum of developing a world class Residential Layouts it aims to provide excellent returns to the customers on their real time investments. Hence, the trust and satisfaction of the customers acts as a motivating factor for the success of the concern.MSK Group?s projects are covering all sections of society, all projects are specific of budget and locations, all under one roof services. Today, the group is into promoting, developing and building residential layouts, residential apartments to cater to the growing need of quality housing for all sections of the society. This new venture would emerge as a successful one, with the efforts of the dedicated workforce using advanced technologies. MSK Shelters have one of the best teams of real estate development professionals working on its various projects from Residential Sites to Villas and Residential Apartments and from facilities planning to interior designing and implementation. MSK Shelters is into implementing turnkey services to its customers across a very wide spectrum of developing a world class Residential Layouts and it aims to provide excellent returns to the customers on their real time investments. Hence, the trust and satisfaction of the customers acts as a motivating factor for the success of the concern.', 'Builder logo2015-12-14 06:53:44.svg', 634, 0),
('MSR Dwellings Pvt. Ltd', NULL, 'sales@msrdwellings.in', 'http://www.msrdwelling.com/', 'Driven by the need to improve the quality of living and work spaces in India and triggered by the vision of being the harbinger of change and innovation in the real estate industry, MSR Dwellings was created. The company\'s transparent, ethical and value-driven style of working is focussed on building long lasting relationships with clients. Possessing the rare obsession of quality, we are a company that is constantly attempting to perfect the art of perfection - to go that extra mile to offer that extra value to our clients and partners.\r\n\r\nContrary to popular belief, affordable luxury is not an oxymoron at MSR Dwellings. Each project, irrespective of its size and nature, is a thoughtful synergy of luxury, quality, comfort and affordability.\r\n\r\nA360 is an architectural and interior design firm based in Bangalore, India. At A360, it all begins with a concept. An idea that gradually comes to life through an integration of thoughts, colours, environment, aesthetics and a sound vision synchronised into a cohesive structure to reflect a client\'s business, attitude and taste. The biggest asset is a group of professionals who understand the need to align themselves with the requirements and resources before commencing the design process.', 'Builder logo2015-12-14 06:56:38.svg', 635, 0),
('Myhna Properties', NULL, 'sales@myhnaproperties.com', 'http://www.myhnaproperties.com/index.html', 'MYHNA Properties takes extreme care to ensure that customer friendly designs and features are incorporated into every project. MYHNA Properties undying commitment to quality sets it apart from the rest. The growing number of satisfied customers is ample testimony of the Company?s attention to quality and aesthetics MYHNA Properties is a reputed construction company in Bangalore with a Very Good name for Quality Construction.\r\n\r\nAt ?MYHNA Properties? we take utmost care to see that the air of friendliness and warmth is an integral part of our construction. That is the reason people who visit our projects, instinctively know that this is where they belong. This is their dream home, their nest at the end of every hectic day, their own space at the start of the day. We have an undying commitment to quality and timely delivery which sets us apart from the rest. We MYHNA Properties are behind some of the prestigious ventures in prime locations in Bangalore, Neyveli & Chennai. Every Venture stands as a testimony to the pristine image of the builders in the field of construction and real estate. With an enviable reputation, and the confidence people response in them has a concrete proof in the form of their highly appreciated ventures.', 'Builder logo2015-12-14 07:06:06.svg', 636, 0),
('Mythreyi Promoters and Developers Pvt. Ltd', NULL, 'sales@mythreyiproperties.com', 'http://www.mythreyiproperties.com/', 'The brand Mythreyi symbolizes the unity of creativity, integrity and versatility, the philosophy which has catapulted Mythreyi Properties as one of the prominent builders in Bangalore.  Mythreyi has triumphantly upheld these ethics since its inception in the year 1996, the decade which witnessed the emergence of real estate domain.  The impetus for the dawn of Mythreyi was to provide superior contemporary living conditions with a traditional approach, bringing you closer to nature, focusing on Vaastu compliance and energy harmonization.\r\n\r\nMythreyi?s inaugural project ?Vithola? off Bannerghatta Road was a true testament to our principles, the project that put Mythreyi on the maps of top Bangalore builders.  Sprawled over an expansive 2,17,025 sqt, the ?Vithola? complex facing a serene lake, comprising 140 homes, has a footprint of just 30% of the land area leaving the rest of the area to be landscaped, offering a natural green feel, enabling more light and air to embrace you.  ?Vithola? was consciously built adhering to all urban luxuries while all homes were built with 100% Vaastu compliance.  More success followed along the same lines of ?Vithola? with ?Aikya,? opposite Meenakshi Temple, off Bannerghatta Road.  Mythreyi has till date, since inception, delivered over 15 prestigious projects in Bangalore that proclaim the proficiency of Mythreyi Group.   Inspired by the success, Mythreyi forayed into commercial ventures and the result being state-of-the art project like 511@Jayanagar to name a few.  Mythreyi is currently involved in many projects, which are in stages of completion with plans drawn up for more in the future.\r\n\r\nWith a host of successful residential and commercial projects, Mythreyi has earned the tag of being a proficient builder in Bangalore and the future looks very promising. With many premium projects being launched currently, the company has charted an ambitious growth for itself by tying up land projects totaling 1 million sqt in the year 2013 alone. Much more is anticipated in the coming years with the company looking at wide range of projects comprising budget houses, plotted developments and villas/independent houses.', NULL, 637, 0),
('Nandi Housing', NULL, 'mail@nandihousing.com', 'http://www.nandihousing.com/home.html', 'We are run by a professional team including graduates from the Indian Institute of Management, Ahmedabad and Columbia University, New York. Inspired by Le Corbusier, our endeavor is to \'infuse a sense of the sacred\' into every home we build.\r\n\r\nWith a belief in complete transparency and a friendly, down-to-earth approach to customers, we have established a solid track record and gained an impeccable reputation in the market.', NULL, 638, 0),
('Nature HP Infras Pvt. Ltd', NULL, 'info@naturehpinfras.com', 'http://www.naturehpinfras.com/', 'NATURE HP INFRAS PVT LTD has entered the Real Estate Industry very recently in the year 2011 with hands on experience of about 11 years in Infrastructure develpment.\r\n\r\nOur Evolution\r\nSince 2002 Mr. R. Harish Kumar, Founder & Managing Director, NHP has been engaged with similar business activities and equipped with quality learning experience about the entire real estate buisness as well infrastructure development and construction, his recent foray into the industry is only a natural progression. He setup NHP Infras with a vision to develop the living standards of the society.', 'Builder logo2015-12-14 08:04:36.svg', 640, 0),
('Nava Nakshatra Homes Pvt. Ltd.', NULL, 'info@nnhomes.in', 'http://www.nnhomes.in/', 'We Stand For Innovation of Quality Homes. \r\nNavanakshatra Homes Pvt. Ltd is a renowned Real Estate/ Residential Construction Company in Bangalore with a very high credential of constructing Real Estate Projects and Residential Flats across Bangalore. NN Homes is acknowledged as the leader in Real Estate business. With a ?dream? to create a splendid and affordable homes, it has established with a clear vision to transform the way people perceive quality in the real estate sector in and around Bangalore. \r\n\r\nSince, its inception on 18th August 2008, as Navanakshatra Homes Pvt. Ltd.started by young and energetic entrepreneurs led by Sri Navaraj .U.L. the honest and hard working leader in real estate field.Our prompt endeavour will always show that we are poised and ready to \"Attain our customer\'s dreams into reality\".', 'Builder logo2015-12-14 08:08:28.svg', 641, 0),
('Navami Builders Pvt. Ltd', NULL, 'sales.marketing@navamibuilders.com', 'http://www.navamibuilders.com/index.html', 'A venture that started 2 decades ago has blossomed into one of Bangalore?s robust infrastructure companies. The name Navami is synonymous with reliable and top quality services in the areas of civil and infrastructure works & Building Construction. Navami with its eye for detail and accuracy has built a niche in the market of construction companies in Bangalore with an impressive clientele throughout the whole region.\r\n\r\nNavami as a company is also known for its management, corporate culture, work-friendly infrastructure, in house design team, board of qualified engineers and architects, panel of legal advisers and chartered accountants, skilled work force and advanced machineries.\r\n\r\nAt Navami, it has always been our tradition to treat each of our projects as a jewel. Our rich experience has allowed us to successfully complete a large number of innovative projects. As of 2013, Navami Builders holds 2 million sqft of construction in the residential segment and 1.5 million sqft of construction in the commercial segment. This quantum of building activity speaks volumes about our strong influential existence. This has been possible due to complete faith and trust reposed by our valued customers.\r\n\r\nWe being proud of our accomplishments will continue to add further value to our capabilities and commitments in the construction industry while emphasizing the topmost international standards of quality and safety.\r\nQUICK CONTACT: 080 23485221 / 23485222 | aditya_b_d@navamibuilders.com', 'Builder logo2015-12-14 08:13:18.svg', 642, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('NBR Group', NULL, 'info@nbrdevelopers.com', 'http://www.nbrdevelopers.com/', 'Quality is never an accident; it is the by-product of determination, hard work and a passion to satisfy customers.\r\n\r\nReal estate is an extremely competitive industry and maintaining a reputation, bettering it and etching a brand name in the heart of customers isn?t an easy task by any standards! NBR Developers has managed to do so since inception and has been a trend setter as regards quality, affordable housing and innovative ideas in construction. Founded in 1998, NBR Developers led by Managing Director Mr. Nagabhushan Reddy has carved a niche for itself in the real estate industry with its unique projects that have attracted buyers from all walks of life. Within a mere 17 years presence in the field, the company has brought in revolutionary thinking in terms of providing a gated community styled living at the most affordable pricing so that owning a dream home is no longer a distant dream for a middle income buyer.\r\n\r\nBoasting of a diverse portfolio, NBR Developers has projects of gargantuan sizes across the most happening places in Bangalore. The NBR Green Valley project was the first of its kind in the Bagalur ? Hoskote stretch and won the coveted Bangalore Real Estate Award for ?Residential Plot Development of the Year? in 2012. The project has been an extraordinary success and its success story has attracted much attention from many other developers and has thus improved the land value and developmental prospects of the region by manifold. Similarly, NBR Developers?? projects in other areas of Bangalore have been well received by customers who place their faith in the company time and again to enjoy great returns.\r\n\r\nWith a vision to provide affordable housing to everyone and provide gated community like amenities even in smaller projects, the company aims to foray into other cities for expanding its real estate activities and will continue to strive to provide the best value for money for its customers.   ', 'Builder logo2015-12-14 08:17:17.svg', 643, 0),
('Nestcon Shelters (Pvt. Ltd', NULL, 'info@nestcon.com', 'http://www.nestcon.com/', 'Vision\r\n? To contribute significantly to the growth of a New and Vibrant India through the construction of quality living spaces within specified time frames.\r\n\r\nMission\r\n? Adopt latest technologies\r\n? Encourage innovations and professional integrity\r\n ? Up gradation of knowledge and skills of employees\r\n\r\nValues\r\n? Implementing professional and ethical practices\r\n? Compliance to legal and environmental issues\r\n? Consistent and concerted effort to enhance customer value and quality\r\n? Integrity and reliability\r\n? Transparency and building trust\r\n? Delivering value for money\r\n', 'Builder logo2015-12-14 08:23:44.svg', 644, 0),
('Nester projects', NULL, 'marketing@nester.in', 'http://www.nestergroup.in/', 'Nester is a niche real estate developer based in Bangalore. We were founded in 2006 and are now into our fourth project. As a company we are deeply committed to delivering high quality, futuristic homes by way of flats, premium apartments, row houses, commercial, villaments and other plotted communities that our customers can thrive in and cherish. Our projects are designed to build communities that are environmentally sustainable.\r\n\r\nNester is known as a premium apartment developer and our strong growth has enabled us to diversify into medium and large sized commercial work spaces and building refurbishments.Over the last decade we have earned a reputation as quality apartment developers.', 'Builder logo2015-12-14 08:29:12.svg', 645, 0),
('Niranjan Group', NULL, 'info@niranjangroup.in', 'http://www.niranjangroup.in/', 'Bangalore based Niranjan Developers is the dynamic leader in real estate services and have great reputation amongst the customers. This is an experienced real estate development organization providing the most exquisite and intricate designer homes through immaculate engineering and construction of quality property that comprises of luxury apartments, independent houses, serviced apartments, club houses, beautiful residential flats, luxury residential and many more\r\n\r\nComprised of a seasoned team of industrial and residential real estate professionals, construction experts and land developers, the company has been instrumental in the development of several residential properties. Our company deals in luxury properties as well as low cost apartments, 2 & 3 bedroom flats suites to the budget of economy class. Our flats are beautiful and spacious with greenery surroundings.\r\n\r\nThe company has embarked on various projects including Amara Canopy, Niranjan Nectar, Niranjan Central, Niranjan Genesis, Niranjan Maxima, Niranjan Legend, Niranjan Sunbeam, Super luxury condominium Niranjan Tridha in India. We are one of the leading builders in this challenging sphere. With sincere efforts and hard work in this business, we have created a niche for ourselves in real estate market. Our services are available in and around Bangalore as well as worldwide.', 'Builder logo2015-12-14 08:34:43.svg', 646, 0),
('Nitesh Estates Limited', NULL, 'sales@niteshestates.com', 'http://www.niteshestates.com/', 'Nitesh Estates is an integrated property development company headquarters in Bangalore, India. Founded in 2004, has grown into a company renowned for its classy developments in Office Buildings, Residential, Hotels and Shopping Malls. Within few years of its inception, Nitesh Estates has brought more than 22 million sq ft of space under development as premium living, working, lifestyle and leisure space- a phenomenal growth fuelled by the resolute and dynamic leadership of its founding Managing Director, Mr. Nitesh Shetty. Today the company is a listed entity on BSE and NSE.\r\n\r\nThe Company is growing even faster with plans to expand its operations in other cities. The Company has now expanded its operations into Goa, Chennai and Cochin.', 'Builder logo2015-12-14 08:50:38.svg', 647, 0),
('Obel Builders', NULL, 'obelbuilders@gmail.com', 'http://www.obelbuilders.com/', 'Come live that dream with us. Come experience a obel home today.\r\n\r\nObel builders is an integrated property development company headquartered in Bangalore, India, renowned for its classy developments in residential apartments, villas and office buildings.\r\n\r\nOur company was founded with the admirable vision of providing affordable, quality homes for the middle and upper-class populace. ,we have achieved tremendous growth and success in this regard. And strive to go even further.\r\n\r\nBased at Bangalore we have spread our wings to embrace the neigbouring cities of Hyderabad and Chennai.\r\n\r\nObel builders is professionally managed partnership company under the dynamic leadership and a team of qualified and dedicated professionals who have vast experience and expertise in the area of architecture ,design,project planning, implementation,legal matters,procurement and marketing.The company is known for quality construction standards,transparency,reliability and professionalisam.\r\n\r\nObel builders is part of obel group companies. Obel group is in the business of computer hard ware marketing,design and development of automatic weather stations and real estate development .', 'Builder logo2015-12-14 08:53:32.svg', 648, 0),
('Oceanus', NULL, 'marketing@oceanus.co.in', 'http://www.oceanus.co.in/', 'A construction Group pulsating with verve and vigour. Versatile- where no assignment is too big to handle, no project too complex; be it Residential, Commercial, Infrastructure, Retail, Entertainment or IT space development. An ISO 9001 : 2008 & ISO 14001 : 2004 Certified company, ranked among the best in the field. Consistent in performance. Delivering on promise, every single time. Professionalism reflected in every venture. The name behind hundreds of glowing smiles.', 'Builder logo2015-12-14 08:56:08.svg', 649, 0),
('Olety Construction Company', NULL, 'info@olety.com', 'http://www.olety.com/index.html', 'We have a firm belief that plans of buildings should be done in harmony with the needs, dreams and aspirations of those who inhabit them & not in isolation. Our earnest endeavour is to gain the trust of our valued customers through reliability, excellency and consistency.Our sole mission has been to make our projects a medium of realization of the cherished dreams of our valued customers & not just building up of concrete structures.', 'Builder logo2015-12-14 08:58:22.svg', 650, 0),
('Ozone Group', NULL, 'enquiry@ozoneurbana.com', 'http://www.urbana.ozonegroup.com/', 'We at Ozonegroup are committed to providing you a better quality of life and redefining the standard of living through innovative real estate products. We distinctively differentiate ourselves through our committed focus on three core values ? Customer Centricity, Quality and Transparency. We have consistently stood at the forefront of design, raising the bar for functionality, infrastructure and eco-friendliness.\r\nOur offerings range from residential condominiums, row houses, villas, serviced apartments, hotels, resorts, spas, business parks, mixed-use development, integrated townships and even retail malls. Our projects are currently being developed in Bangalore, Chennai and Goa.', 'Builder logo2015-12-14 09:29:48.svg', 651, 0),
('Panacea Builders Pvt. Ltd', NULL, 'panaceabuilders@gmail.com', 'http://www.panaceabuilders.com/', 'The company is built around a dynamic team of individuals who are recognized experts in their respective fields. They bring with them their expertise and skills in Project Development, Management, Finance, Marketing and Engineering. Their broad skills, solid competencies and international perspectives have enabled us to quickly establish ourselves in the market..', 'Builder logo2015-12-14 11:02:33.svg', 652, 0),
('Paramount Construction Company', NULL, 'sales@paramountconstructions.in', 'http://paramountconstructions.in/Default.aspx', 'Paramount construction company started in the year 2010 have been working towards establishing and enduring relationship with our customers and stakeholders. We are committed to catering to the needs of our customers of all segments by delivering cost effective and luxury homes.\r\nWith extensive experience and technical competence relating to construction, design and management we have set highest standards for quality, excellence and innovation in the field of real estate. We ensure timely delivery of your homes, ushering best in class apartment facilities, along with aesthetically pleasing surroundings, leisure and recreational infrastructure, adhering to business ethics.\r\nFrom a very modest beginning, Paramount is set to take great stride towards our goal and make incredible mark for itself by creating homes which would redefine the concept of living in ?Mysore.\r\nEvery customer has certain emotions and aspirations that are attached with a decision to choose their dream home. Fulfilling customer aspiration is what inspires our innovation and creativity. We go the extra mile for the customer to see those aspirations getting fulfilled with Paramount.\r\nWe believe in fostering the trust of our customers by offering best homes without compromising on the quality. We understand aspirations of our customers and take utmost care to safeguard customers hard earned Money invested in our projects.', 'Builder logo2015-12-15 06:53:27.svg', 768, 0),
('Paratus Buildcon Pvt. Ltd', NULL, 'info@paratusbuildcon.co.in', 'http://www.paratusbuildcon.com/', 'Paratus BuildCon Private Limited emerged with this thought of how we create smiles. We at Paratus are an Eco-friendly people, hence all our projects are adapted by nature, concentrating on the concept of saving our valuable natural resources of our environment we prepare a perfect recipe which suits our customers. Paratus BuildCon Private Limited is a company with a noble thought of our Dear Chairman/MD Mr. R Ravindra Kumar and the supporting team to strengthen its foundation. Paratus BuildCon Stands for the highest standards of quality and integrity in Indian Property transactional, management and advisory services. Our reputation for uncompromising professionalism in everything we do is earned day in and day out serving our clients and earning their trust. More Than 15 Professionals handle commercial, agricultural and residential projects, advising clients ranging from individual owners and buyers, investors and corporate tenants across the globe.\r\n\r\nParatus concentrates and frames projects which the society wants and these projects are highly concentrated with the quality of work which we do.\r\n\r\nLike a plant has to be taken care of regularly for it to grow with a good health, that?s how Paratus works and takes care of each and every client of it\'s family.', 'Builder logo2015-12-14 11:07:05.svg', 653, 0),
('Pashmina Builders & Developers Pvt. Ltd', NULL, 'salesbangalore@pashminadevelopers.com', 'http://www.pashminadevelopers.com/', 'Pashmina ? the famous Kashmiri wool known for its quality and class signifies the moving spirit behind Pashmina Developers. We conceive, build and deliver residential, commercial and retail real estate properties with a signature touch. Every apartment, row house, villa, commercial and retail property of Pashmina carries a signature touch by combining unique design aesthetics with state-of- the-art construction techniques.\r\nI am confident that you will experience this warmth and passion when you call my team and visit your futurre home here at Pashmina.', 'Builder logo2015-12-14 11:16:16.svg', 654, 0),
('Pathak Developers Pvt. Ltd', NULL, 'marketing@pathakbuilders.in', 'http://pathakdevelopers.in/', 'Pathak is the generation of Lifestyle housing. Started in the year 1996 & became a channel for better living. Home ownership matters to all of us; we make it much simpler & easier. We access innovative tools, leverage advanced methods for constructing and stay on top of the current trends. We offer extremely convenient location with easy access to year-round outdoor markets, and other great conveniences, yet there are many opportunities to commune with nature here. We have apartments with high-efficiency appliances and State-of-the-art architectural elevation. While we work for commercial customers, our projects have helped grow local economies and improve the quality of life for communities and people around the city. Time and again our work has demonstrated that the only limits on human achievement are those that we place on ourselves. Our reputation as an ethical company is one of our most valuable assets. We stand by everything we do.\r\nExtraordinary is a word that epitomises our apartments. No other building in the city can match its many contradictory characteristics, most evident in its wholehearted embrace of both innovation and tradition. Pathak?s offer such diversity in apartments that one will definitely be mystified which one to go for. With Pathak?s you will find the most sophisticated apartments, characterised by its efficiency, promptness & spotlessness. About 95 percent of our projects complete each year without a lost-time accident. Our diverse portfolio encompasses luxury apartments, residential enclaves, modern offices, retail facilities & multi-storey industrial complexes. Pathak Constructions transformed into Pathak Developers PVT LTD in the year 2008 to reach higher milestones as a company.', 'Builder logo2015-12-15 07:13:50.svg', 779, 0),
('Pavani Group', NULL, 'sales@saisravanthi.com', 'http://www.saisravanthi.com/', 'Owning a house is a long nurtured dream for many. To have a home, you have to own one. The goal of any home is to see that all the elements in the home add to a homely feeling and utilization of space.\r\nMr. P.V RAGHAVA RAO founded PAVANI GROUP IN 1995 to promote Apartment construction in Nellore town, at a point of time when Apartment Culture was still new there. He thus become pioneers in Apartment Industry within a short span of time. with all his hard-work, foresight and vision the group of companies has completed 14 projects and five projects are in progress and many more in the anvil, spreading all over south India,Viz. Hyderabad, Chennai, Nellore and Bengaluru.\r\nPAVANI GROUP, backed up by ten years of experience in architectural planning, design and implementation, has come of age, in that, the company has analyzed the demanding choosy requirements of home owners that encompass above parameters and they are determined to a deliver a home tailor made for each owner, snug fitting his or her aesthetics, psychology and ethical Standards. PAVANI GROUP keep in its mind the futuristic requirements. This calls for world-class specifications and high standards of construction practice. Backed up by highly and experienced Civil Project Directors with their team of young field engineers, who have completed a number of projects in and around Bangalore, Chennai, Nellore and Hyderabad covering Residential, Commercial Buildings, PAVANI GROUP is never short of concept for timely completion of high quality buildings. While doing so they ensure that they do not end up functional Architectural cul-de-sac.', 'Builder logo2015-12-14 11:28:48.svg', 655, 0),
('Phoenix infra', NULL, 'johnson@phoenix-infra.com', 'http://www.phoenix-infra.com/', 'At Phoenix-infra, we are structured to provide highly differentiating projects to various urban needs - residential, commercial and hospitality. A front runner among the city?s real estate Marketing Partners, Phoenix-infra is exclusively developing relationship with premium property developers that are defining growth and prosperity in Bangalore city.\r\n\r\nCreating your own brand requires a vision, strategy and trusted partner to build your supporting tools. Pheonix Infra helps you stand out in your market with a core group of promotional products that position you for long-term success', 'Builder logo2015-12-14 11:32:13.svg', 656, 0),
('Pionier Developers', NULL, 'info@pionierdevelopers.com', 'http://pionierdevelopers.com/', 'About Us\r\nPionier Developers has been in engaged in various aspects of the real estate industry like project marketing, liaison, consultancy, financing and so on. Having grasped the nuances of the Bangalore real estate market, the company is well positioned to hand pick projects and promoters and aid them in achieving balanced business solutions. The core values of the company are - Trust, Communication, Service, Courtesy and Knowledge.\r\n\r\nVision\r\nOur Company\'s Vision is to be the most respected and successful enterprise, delighting customers with a wide range of satisfaction - creating a better tomorrow than today.\r\n\r\nOur Mission\r\nTo be the most successful Layout Developers company in India and delivering the best customer experience in markets.', 'Builder logo2015-12-11 10:45:06.svg', 469, 0),
('Platinum City Builders & Developers', NULL, 'contact@platinumcity.co.in', 'http://www.platinumcity.co.in/home.html', 'PLATINUM CITY is one of the fastest growing real estate companies in Bangalore. The team at Platinum is dedicated to providing best deals, results and exceptional service of selling approved plots and sites. We are providing BMRDA & BDA approved sites and plots in Hoskotte which is very cheap and best. Our ongoing projects are Royal City , platinum city etc. platinum properties is run by Young & dynamic people with professionalism and dedication in providing you the best information regarding real estate. The platinum team is uniquely qualified to assist you with all your Bangalore real estate needs with its Boundless energy, superior skills and a talented Human resource ever ready for new conquests which makes great achievements for you a possibility.', 'Builder logo2015-12-14 11:36:04.svg', 657, 0),
('Prabhavathi Builders & Developers Pvt Ltd', NULL, 'sales@prabhavathibuilders.com', 'http://www.prabhavathibuilders.com/index.php', 'In less than 10 years, the company has completed over 35 projects in the city with a strong track record of completing projects on time and ensuring high levels of customer satisfaction and making us one of the leading developers in Bangalore. At Prabhavathi Builders & Developers, we are focused on offering our customers world class homes at affordable prices.  As one of the best builders in Bangalore, we believe that a home must be an amalgamation of luxury, elegance and creativity and to achieve this vision we are constantly working with some of the best teams of committed engineers and architects in the industry. We believe that as leading builders in Bangalore, we are not just building homes, but we are fulfilling dreams of our customers.', 'Builder logo2015-12-14 11:39:29.svg', 658, 0),
('Pranava Group', NULL, 'info@pranavagitaaar.com', 'http://pranavagitaaar.com/', 'Pranava Avenues GROUP has won the trust of its customers primarily due to the fact that it creates top-class construction projects using state of the art technologies. Indeed, trust is of great significance when someone entrusts the responsibility of building a dream house to a real estate company. And thats precisely why the Pranava Avenues pays great attention to detail in each of its construction projects. No wonder, customers continue to have faith in the company.\r\n\r\nPranava Avenues is incorporated in Hyderabad\r\n\r\nA rapidly expanding company pioneered by some of the best professionals in the industry Currently executing projects in Hyderabad. Pranava Avenues aims to be a leading construction company, devoting its human resources to create superior products and services, thereby contributing to a better global society.\r\n\r\nTowards realization of Pranava Avenues Core Values, we follow the Business Principles, which combine a promise to comply with laws and good ethical practices and an express commitment to these values. The Business Principles are the guiding standards for everyone , outlining the conduct expected of all our employees both individually and collectively.', 'Builder logo2015-12-14 11:43:59.svg', 659, 0),
('Prashanthi Group', NULL, 'ravella1972@gmail.com', 'http://www.prasanthigroup.com/', 'Prasanthi Group brings with it over 15 years of experience in the porpoerty development and construction, having completed over 20 projects in Hydrabad, Secundrabad, Vishakapatnam & Bangalore. Right from its inception its aim is to provide the highest level of quality. Whether it is selecting location, providing the better infrastructure or delivering finished projects?\r\nPrasanthi Group ensures that no corners are cut or no compromises made on quality. At the same time we have endeavoured the price remain reasonable and customer friendly, thus establishing its credentials and expanding its customer base which forces us to do more projects.', 'Builder logo2015-12-14 11:47:54.svg', 660, 0),
('Pratham Construction', NULL, 'info@prathamconstructions.com', 'http://prathamconstructions.com/', 'The evolution of the Gadia Group started 70 years ago under the eminent leadership of Mr. Kesarimalji Gadia. The journey began when Mr. Kesarimalji Gadia laid the foundation stone of K.M. Gadia & Sons, a firm dealing with the trading of gold threads and yarn. Mr. Gadia?s vision was widespread, which is identifiable till date with his trading, manufacturing and real estate development firms.\r\n\r\nMr. Gadia exhibited deep affinity to the end consumer and ensured to reflect these feeling in his business practices and principles. His vision was built around the concept of ?economies of scale? that involved the use of technology to bring down operational costs in production. His forward thinking or futuristic approach resulted in K.M. Gadia & Sons enjoying 90% of the market share.\r\n\r\nK.M. Gadia & Sons explored the realm of real estate development with it?s maiden project ?Prathamm Casa serene.? The excitement of turning lands into landmarks was the biggest driving factor for the company. The company?s mission has always been to provide superior products and services by arriving at innovative solutions, which will help improve the quality of life and satisfy consumer needs.', 'Builder logo2015-12-14 11:50:20.svg', 661, 0),
('Prestige Estates Projects Ltd', NULL, 'properties@prestigeconstructions.com', 'http://www.prestigeconstructions.com/', 'The Prestige Group owes its origin to Mr. Razack Sattar, who envisioned a success story waiting to take shape in the Retail Business in 1956 itself. Since its formation in 1986, Prestige Estates Projects has grown swiftly to become one of South India\'s leading Property Developers, helping shape the skyline across the Residential, Commercial, Retail, Leisure & Hospitality sectors.\r\nPrestige Court on K.H. Road in Bangalore set the pace for the Group\'s rapid growth which now stands at over 191 Completed Projects spanning a total developed area of over 64.06 million sqft. It also has another 64 ongoing projects comprising around 65.45 million sqft, which include Apartment Enclaves, Shopping Malls and Corporate Structures, spread across all asset classes.\r\nPrestige Constructions, an ISO 9001:2000 certified company is the only Real Estate Developer in Bangalore to have won the reputed FIABCI Award for its software and residential facilities. Prestige was also recently awarded the Crisil DA1 Developer Rating in recognition of the quality of their projects and the ability to deliver completed projects in a timely manner, making them the ONLY Property Developer across India to have received this distinction.\r\nToday, Prestige stands as a giant and with aggressive growth plans across the Residential, Commercial, Retail and Hospitality Sectors in Bangalore, Goa, Hyderabad, Mangalore, Cochin and Chennai, lies a bright future ahead!', 'Builder logo2015-12-14 11:54:07.svg', 662, 0),
('Pride Group', NULL, 'bangalore@pridegroup.net', 'http://www.pridegroup.net/', 'The Pride Group is a leading property development conglomerate that is changing the cityscapes of Pune, Mumbai and Bangalore with its bold new designs, high engineering and ethical standards and professional outlook. A leading development firm in the residential & commercial space, Pride Group has various Information Technology parks and built-to-suit office space complexes to its credit. Pride Group is also a pioneer in the luxury hospitality segment and is the driving force behind the extensive chain of Pride Hotels across the country. Since the inception of the first Pride Hotel in Pune in 1988, under the able chairmanship of Mr. S.P. Jain and the skilled leadership of his brothers, Mr. D.P Jain and Mr. Arvind Jain, the Group has seen phenomenal growth.', 'Builder logo2015-12-14 11:59:07.svg', 663, 0),
('Primia Constructions', NULL, 'info@primiaconstructions.com', 'http://primiaconstructions.com/Default.aspx', 'Primia Constructions is a Mysore based Construction and Real Estate Development Firm involved in contracting, development of residential layouts, villas, apartments and commercial buildings. Managed by a team of technocrats with more than two decades of experience in the Civil and Mechanical industry, we at Primia strive to set benchmarks in every project by ?Scaling New Standards?.\r\nWe believe that ?Good people create good results? and this belief is reflected in our people who are trained to deliver high quality outputs with careful designing/planning to create extraordinary homes and offer services which reinforces our commitment to give the best to our discerning clients.', 'Builder logo2015-12-15 07:15:28.svg', 780, 0),
('Prithvi Infrastructure', NULL, 'enquiry@prithviinfrastructure.com', 'http://www.prithviinfrastructure.com/', 'Prithvi Infrastructure is one of the most experienced and respected building and civil construction firms in the India. We have been transforming the ideas and visions of our client\'s into award-winning projects. We build with the intention of exceeding our client\'s expectations for safety, quality, functionality, and aesthetics, and deliver finished products that stand the test of time', 'Builder logo2015-12-15 07:17:03.svg', 781, 0),
('Puravankara Projects Limited', NULL, 'sales@puravankara.com', 'http://www.puravankara.com/', 'Since its inception in 1975, Puravankara has believed that there is only one mantra for success: Quality. This credo combined with uncompromising values, customer-centricity, robust engineering, and transparency in business operations, has placed it among the ?most preferred? real estate brands in both residential and commercial segments.\r\n\r\nThe Company has grown from strength to strength, having successfully completed 48 residential projects and 2 commercial projects spanning 23.54 million square feet (PPL?s economic interest ?20.93 msft). Currently, it has 24.87 million square feet / 17,895 units (PPL?s economic interest ? 22.03 msft/ 16,188 units) of projects under development, with an additional 22.73 million square feet(PPL?s economic interest ? 18.98msft) in projected development. An ISO 9001 certification by DNV in 1998 and a DA2+ rating by CRISIL are testament to Puravankara?s reputation as a real estate developer of the highest quality and reliability standards.\r\n\r\nThe Group commenced operations in Mumbai and has established significant presence in the metropolitan cities of Bangalore, Kochi, Chennai, Coimbatore, Hyderabad, Mysore and overseas in Dubai, Colombo and Saudi-Arabia.', 'Builder logo2015-12-14 12:09:10.svg', 665, 0),
('Purnima Build Tech', NULL, 'ldnaidu@yahoo.co.in', 'http://purnimabuildtech.in/', 'Purnima is brought to you by the developers and architect, who came together to create a space that is top notch for the audience who can make a distinction between any home and one that fits their distinguished status.', 'Builder logo2015-12-14 12:12:16.svg', 666, 0),
('Pushpam Group', NULL, 'sales@pushpamgroup.in', 'http://pushpamgroup.in/', 'We are a multinational organization dealing with the diversified areas of Real Estate, Agro business, Floriculture, International Trading, Wellness, Hospitality, Healthcare and Education while creating meaningful living spaces, improving human well-being and empowerment of mankind through power of knowledge, health and happiness.\r\n\r\nThe Pushpam Group is a three (3) decade old conglomerate with interests in Agribusiness, Floriculture , Plantations and International Trading with operations panning India, Africa, Europe, UK and Middle East. The Group diversified into real estate and hospitality in 2010 and implementing high end theme based luxury living spaces for the well deserved. With sufficient land banks in Bangalore, Coorg and Goa, the Group is poised for rapid growth in both the Real Estate and Hospitality segments. It has already made a mark with completion of Pushpalok ? Spa Residencies and Imera ? chain of Spa and Resorts which has generated rare reviews from the customers.', 'Builder logo2015-12-14 12:14:31.svg', 667, 0),
('Pyramid Homes Pvt. Ltd', NULL, 'info@pyramidindiagroup.com', 'http://www.pyramidindiagroup.com/', 'The fast changing city of surprises is what Bangalore. The Garden City is Bangalore. The Silicon City is Bangalore. The Technology Park is Bangalore. The IT Capital of India is Bangalore.\r\n\r\nBangalore, a city of many sobriquets, has grown considerably over the years. Its romantic past is still discernible in the modern city. Its salubrious climate and work culture make it a natural choice for most of the private and public sector industries which sectors include software, telephone, aeronautics, electrical equipment, heavy electrical and now especially, the IT in peak.\r\n\r\nBecause of its colourful and lively antecedents and its developmental history, Bangalore is a city of great diversity offering residents and visitors a range of diversion clubs and resorts, pubs and restaurants, bowling alleys, spiritual places and with no doubt, huge number of shopping centers and multiplexes. Hence, Bangalore is one among few cities in the world, which has power to captivate and motivate casual visitors to stick in the Bangalore, permanently.', 'Builder logo2015-12-14 12:17:18.svg', 668, 0),
('R V Shelters', NULL, 'rvshelters@gmail.com', 'http://www.rvshelters.in/', 'R V Shelters, a Partnership company was formed by 4 right thinking people having a right mix of prime experience and youthfulness on 12th April 2012. Promoters are well versed in the subject they are dealing in having completed nearly 80 projects with a combined experience of 26-27 years.\r\nOur Principled stand is to give the product which is value for money. Going by Our Belief - PRICE THE QUALITY, we reiterate to state that we shall never compromise on the quality.\r\nOur specialty lies in custom homes, custom multi-family, and custom commercial projects with varied experience of being in the business of Construction.\r\nR V Shelters is committed to the community, environment and customers. We want to be prominent providers of superior construction projects and services by consistently improving on the quality; add value for clients through innovative, foresighted, and aggressive performance with all integrity; and to serve with character and purpose that brings honor to our dedicated work.', 'Builder logo2015-12-14 12:47:58.svg', 677, 0),
('Radiant Structures Private Limited', NULL, 'sales@radiantstructures.in', 'http://radiantstructures.in/', 'To deliver quality at all times, to satisfy customers forever, to quote realistic prices always. In short, this is what Radiant Structures is all about.\r\n\r\nA company that?s manned by thorough professionals; people who?ve over two decades of experience in property development. At Radiant, excellence stems from its people ? skilled engineers, architects, workers, marketing professionals and customer care personnel.\r\n\r\nBesides the talented workforce, the company is ably guided by its management team headed by Mr. Raghavendra Reddy. A Civil Engineering graduate from Gulbarga University, he has independently handled huge contracting projects ? both residential and commercial.\r\n\r\nNeedless to say, at Radiant everyone shares a common passion to deliver products that?ll not just exceed customer expectations, but remain as benchmarks for the industry itself.', 'Builder logo2015-12-14 12:19:27.svg', 669, 0),
('Rainbow Properties', NULL, 'contact@rainbowproperties.in', 'http://www.rainbowproperties.in/', 'Rainbow properties was established in the year 2009, at the helm of affairs is Mr. Supreeth Suresh a young entrepreneur with passion to build and transform geographies. With its maiden project spanning 0.25 million square feet completed, commitments fulfilled and promises made,has embarked upon more residential development spanning 0.4 million square feet in the next one year.\r\n\r\nThe projects from Rainbow properties offer sustainability, affordability & excellence. The firm?s synergized teams of young, old and experienced staff provide continual support, knowledge and expertise in achieving highest standards in quality. We at Rainbow properties nurture our products such that it generates positive energy ensuring high customer value and satisfaction.', 'Builder logo2015-12-14 12:21:23.svg', 670, 0),
('Raja Housing Limited', NULL, 'info@rajahousingltd.com', 'http://www.rajahousingltd.com/', 'Vision\r\nOur Vision is to Create a progressive organization matching International Standards maintaining Integrity, High Ethical Standards and Transparency. Provide an environment of professionalism, competence, teamwork, and service excellence.\r\n\r\nMission\r\nOur Mission is to bring quality Residential & Commercial real estate of international standards, comparable with global developers \r\nwithin the reach of all.', 'Builder logo2015-12-14 12:24:15.svg', 671, 0),
('Ramky Estates & Farms Limited', NULL, '', 'http://ramkyestates.com/', 'Welcome to Ramky Group, a pioneer in waste management and infrastructure development. Established in 1994 at Hyderabad, the Ramky Group has grown by leaps and bounds and is today, a specialist multidisciplinary organization focused on Construction, Infrastructure Development, Waste Management, Build-Operate-Transfer (BOO/BOOT/BOT) projects, Real Estate, Finance and Investment.\r\n\r\nInfrastructure Consultancy, Environmental and Laboratory services are the other areas in which Ramky has made a mark for itself. Ramky\'s Waste Management Division has the distinction of establishing the country\'s first integrated Common Hazardous Waste Management and Bio-medical Waste Management Projects at Hyderabad, India.\r\n\r\nOver the past years, Ramky has collectively fostered a number of fast track projects, most of which are today considered as benchmarks. With a philosophy of challenging our own limits, we are incubating a wide range of new ventures and projects.', 'Builder logo2015-12-14 12:30:02.svg', 672, 0),
('Rashi Developers', NULL, 'info@rashidevelopers.com', 'http://www.rashidevelopers.com/', 'Rashi Developers have been in the forefront of the Housing development and construction industry in Bangalore learning from it and contributing to it in the process.\r\n \r\nWe have had the distinction of conceptualizing, creating and delivering several premium Layouts in Bangalore over the past one decade. Our creative and experienced team has been constantly interacting with our customers and has been able to understand their constantly changing needs and have delivered end-to-end solutions to satisfy all their requirements. \r\n\r\nThe success of our service motto ? Total Customer Satisfaction ? is reflected by the fact that over 30 % of our customers are repeat buyers.\r\n \r\nOur ability to complete and deliver the projects well ahead of time has given our customers total confidence in our organization.\r\n\r\nConstruction of multiple Model Houses in our layouts is our USP, which ensures that the development of the layout starts immediately and grows continuously enabling the buyers to shift to the new bungalow/layout without any delay', 'Builder logo2015-12-14 12:33:34.svg', 673, 0),
('Renaissance Holdings & Developers Pvt. Ltd', NULL, 'contact@renaissanceholdings.com', 'http://www.renaissanceholdings.com/', 'Renaissance Holdings is among the leading property developers in Bangalore, with a growing reputation for building innovative and functional spaces while providing high quality experiences and value for its customers. Since its establishment in 1994, Renaissance Holdings has been contributing to the transformation of Bangalore\'s urban landscape, catering across the full spectrum of real estate from residential to hospitality and commercial sectors.\r\n\r\nCatering to the premium luxury segment, Renaissance Holdings is known for its design aesthetics, eco friendly architecture, customer centric approach, uncompromising business ethics, core values that have helped make it a preferred brand in the city\'s real estate environment.', 'Builder logo2015-12-14 12:36:06.svg', 674, 0),
('Richmond Builder Pvt. Ltd', NULL, '', 'http://www.richmondbuilders.in/', '', NULL, 675, 0),
('RJ Group', NULL, 'sales@rjbuilders.in', 'http://www.rjbuilders.in/home.html', 'The RJ Group has been an integral part of the realty sector in Bangalore for more than two decades now. The group, since then has grown from strength to strength, to be a full services organization. We offer building, design & development, interiors, project management & consultation across corporate and residential constructions. We also have a niche in Hospital construction and development. Rich in experience and expertise across the cross section of offerings, we undertake 360 degree turnkey solutions with an underlined thrust on quality. State of art tech savvy homes, custom designed to suit a modern lifestyle with individualized needs is a core focus with us. Click here for Performance Certificate', 'Builder logo2015-12-14 12:44:09.svg', 676, 0),
('S2 Homes India Pvt. Ltd', NULL, 'sales@s2homes.in', 'http://s2homes.in/', 'S2Homes is promoted by a team of entrepreneurs who have the backing of more than 3 decades of dedicated and successful track record in various fields apart from real estate.\r\n', 'Builder logo2015-12-14 12:51:24.svg', 678, 0),
('Sagar Ventures', NULL, 'info@sagarventures.com', 'http://sagarventures.com/', 'Sagar Ventures is proud of its promoters\' experience of more than 25 years in Real Estate development industry. As a new entrant in the real estate industry we are active in different business segments such as property development, real estate marketing and sales along with agriculture and Hospitality. A special feeling for future development, highly creative but customised solutions to the requirements of every person dreaming of a home and an urge to offer more and better services is making us strong and successful.\r\n\r\nRealizing that there is a dearth for good properties with proper legal titles and excellent quality construction \'Sagar Ventures\' was envisaged as a partnership firm to make possible \"Real Estate Development\" enterprises into \"Real Ethics Development\" enterprises by bringing in full transparency in all stages of real estate transactions. Sagar Ventures is a new enterprise jointly floated by Mr. M. S. Narahari and Mrs. Kamakshi Narahari, which commenced operations in the year 2011. With ventures in Bangalore and other cities in Karnataka we are slowly but steadily growing as a successful initiative.', 'Builder logo2015-12-14 12:54:55.svg', 679, 0),
('Sai Lakshmi Shelters', NULL, 'sailakshmishelters@gmail.com', 'http://www.sailakshmishelters.com/', 'With more than a decade of experience in construction field, SAI LAKSHMI SHELTERS has successfully completed several housing ventures. We at, SAI LAKSHMI SHELTERS, are always\r\n\r\n? Committed in building your home with personal care,\r\n? Attentive to every detail including Vaastu,\r\n? Building homes that are both inspiring and comforting for daily living,\r\n? On time in completing our projects,\r\n? Excelled in establishing outstanding reputation with our customers.\r\n\r\nLooking forward to work with you in helping to make your dream home into a reality.\r\n ', 'Builder logo2015-12-14 12:59:24.svg', 680, 0),
('Sai Sannidhi Estates', NULL, 'saisannidhiestates@gmail.com', 'http://www.saisannidhiestates.com/', 'At SAI SANNIDHI ESTATES we are committed to provide you a better quality of life and redefining living standards through world class products. We have set up a team that really works precisely on better Standards to provide required satisfaction, Quality and Transparency, as well. We have projects that consistently showcase advanced designs, raising the bar for functionality, infrastructures with eco-friendly products.\r\n\r\nOur ready to move in projects are Villas & apartments, the under construction projects by us range from residential condominiums, row houses, villas, serviced apartments etc, the projects under stage of obtaining approvals are hotels, integrated townships, retail malls, mixed development etc, Our Projects are currently being located close to Bangalore?s IT SECTOR.\r\nOur focus is to match your mindset to provide a better living, Our team is been dedicated on precisely meeting the engineering standards, the aesthetic design, high-class construction products, which allows for stringent focus on quality control of your dream house.\r\nAll our projects have a fine architecture, with a well designed infrastructure having wide internal roads, adequate amenities to residents as well as their visitors, with large open green spaces with water features, advanced security systems, spacious garden areas, with rain water harvesting, solar heating and lighting, waste management, which in fact is environment friendly.', 'Builder logo2015-12-14 13:03:10.svg', 681, 0),
('Saibya Structures Pvt. Ltd', NULL, 'info@saibya.com', 'http://www.saibya.com/', 'Saibya Structures Pvt. Ltd ? Building and construction industry based in Bangalore, Launched in 2013. We are a builder first, and that makes all the difference. Saibya Structures Pvt. Ltd ? Building and construction industry based in Bangalore. We employ quality business practices, and enjoy an excellent track record of satisfied customers and employees. We choose environmentally-responsible, close-in projects we feel good about participating in, and clients we enjoy working with. Its primary role is to promote the viewpoints and interests of the building and construction industry and to provide services.', 'Builder logo2015-12-14 13:05:56.svg', 682, 0),
('Saiven Developers', NULL, 'saivendevelopers@gmail.com', 'http://www.saivendevelopers.com/', 'For more than a decade, the SAIVEN family has been in the real-estate arena understanding value creation beyond our client\'s expectations. It is our dedication to design excellence and a steadfast focus on quality that has helped us form this organization.\r\n\r\nOur design features are inspired by a tradition of innovative leadership. The resources from award winning teams, our architects, interior designers, project managers and specialists work in flexible teams that are customized specifically for each project we undertake.\r\n\r\nSaiven Developers focus on conceptualizing and executing distinctive projects, keeping in mind all the basic elements of design, space, aesthetics, vastu and most importantly value for money. One such distinct feature is our creation of world class theme-based architectural projects that turn dreams into a reality.', 'Builder logo2015-12-14 13:08:26.svg', 683, 0),
('Salarpuria Sattva Group', NULL, 'enquiry@sattvagroup.in', 'http://www.sattvagroup.in/', 'Built on trust, innovation and knowledge-leadership, the Salarpuria Sattva Group is one of India\'s leading Property Development, Management and Consulting organizations.\r\n\r\nFounded in 1986 with the primary focus of developing high quality constructions, the Group has attained leadership positioning in the field and is one of the most preferred brands in the country today. Having pioneered the early development landscape in Bangalore and literally shaping the city\'s skyline since the mid-80s, the Group has a diverse portfolio of world-class IT Parks, commercial, residential, hospitality and retail properties.\r\n\r\nA course of natural evolution and diversification led to the formation of the Salarpuria Sattva Group, which blends the traditional strengths of the Salarpuria Sattva Group\'s development business and the forward-looking diversification into many verticals such as Aerospace, Hi-tech, Education, Facilities Management and Warehousing.\r\n\r\nFuelled by an intense eagerness to innovate, deliver value and build trust, the expanding business footprint has spread across many major Indian cities. The unwavering focus has always been to attain complete client satisfaction resulting in successful deliveries that are now recognized as the hallmark of the Group.', 'Builder logo2015-12-14 13:14:11.svg', 684, 0),
('Samatha Group', NULL, 'info@samathagroup.com', 'http://samathagroup.com/', '	\r\nA concrete venture brought about by 4 entrepreneurs, Samatha specializes in sourcing ideal locations to develop residential & commercial layouts.\r\n\r\nWe provide total solutions whenever we develop a plot ? right from survey, registration, obtaining necessary legal sanctions, underground drainage & electrical cabling, asphalting of roads etc, All you have to do is build your home .\r\n\r\nNot just developing, Samatha is also into constructing designer houses. Houses that are designed by top architects with sound structures, swank interiors, landscaped gardens and above all highly functional space.\r\n\r\nSamatha Group is a highly resourceful firm, in the sense that it has in its team top notch architects, structural engineers, interior designers, vaastu experts etc all backed by experience & sound administration.\r\n\r\nMysore is slowly emerging out of Bangalore?s shadow as the next happening destination and at Samatha, we are all geared to giving it our best shot in making houses that truly are homes.', 'Builder logo2015-12-15 07:19:54.svg', 782, 0),
('Samaya Structures', NULL, 'info@samruddhirealty.com', 'http://www.samruddhirealty.com/', 'Samruddhi Realty Ltd., a Bangalore based real estate property developer, had its ambitious beginnings in 2004. We started as a group of small, yet experienced and qualified professionals. Our backgrounds were diverse, but we shared the common dream of delivering remarkable living spaces to our customers.\r\n\r\nWe have, since then, been crafting homes that have not just delighted and satisfied buyers but have also surpassed benchmarks in quality, design and planning. We have acquired, developed and marketed landmark residential projects.\r\n\r\nOur team today comprises of a group of skilled, passionate and committed individuals who live by Samruddhi Realty?s core values. Our attention to detail is our strong suit and has helped us understand our customers? needs and dreams. This has enabled us to consistently improve the delightful residential living experience that our customers relish. We take pride in the strong bonds that we have established with our Clients, Associates, Employees and Investors.\r\n\r\nThe goal of Samruddhi Realty Ltd. is to build for you the home that you dream of. Our constellation of residential projects gives you an immense variety of choices.', 'Builder logo2015-12-14 13:25:20.svg', 685, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('Sanjana Build-Tech Pvt. Ltd', NULL, 'info@sanjanabuildtech.com', 'http://www.sanjanabuildtech.com/', 'anjana Build-Tech Private Limited is passionate in making your dreams come true.\r\n\r\nA trusted company focused and dedicated in property development bringing you the essence of life. Our innovative thinking and endless quest have strived for perfection;  years of experience and layers of knowledge have taken us to a greater height of success in property development giving you the best which you have always dreamt of. Our efforts are taking us to a greater height in construction mega structures using sophisticated technology with unmatched facilities adding brilliance to your lifestyle. Customer satisfaction is our main aim providing unflinching service to mankind. ', 'Builder logo2015-12-14 13:28:13.svg', 686, 0),
('Sankalp Group', NULL, 'sales@sankalpgroup.com', 'http://www.sankalpgroup.com/', 'Sankalp The name spells ?Determination and Dedication?. It is our inherent pledge to worship and uphold the heritage of Mysore, and blend it with a tint of modernity, to create a new skyline to the city of Mysore.\r\n\r\nLeading a team, the Chairman and Managing Director of Sankalp Group - Mr V K Jagadish Babu, a technocrat with Masters in Engineering (Structures) set up a corporate entity in the Year 1995, with High Class and Trained Professionals.\r\n\r\nEarlier working as a structural and design consultant in Bangalore, Mr. V K Jagadish Babu, ME structures, transformed himself to a developer and shifted base to Mysore, ?to Create International Quality Homes for the people of Mysore?. \r\n\r\nHe began his first venture on Sayyaji Rao Road, Mysore with ?Sankalp Apartment?. This was a challenging venture, as Sankalp had not only to prove themselves as a property developer but bring to Mysore - the Apartment Culture.\r\n', 'Builder logo2015-12-15 07:21:29.svg', 783, 0),
('Sapthagiri Developers', NULL, 'sapthagiridevelopers@gmail.com', 'http://www.sapthagiridevelopers.com/index1.html', 'Sapthagiri Developers is a bangalore based real estate company saw its genesis in 1996 under \r\nthe able leadership and guidance of its founder and MD.Mr.S.K.Narayana Reddy built on a \r\nwealth of experience earning and excellent reputation for elegantly designed and crafted \r\ndevelopement of high quality homes its amazing, what thoughtful design can achieve enabling \r\nspaces to feel like a whole lot more home is designed to let you make use of every niche \r\nand every look so much that you can exactly visualize whats going where and still experiment \r\nwith the space,the quality of home we build is unparallel.\r\n\r\nSapthagiri Developers emanate from an emenently brilliant architect and civil engg to \r\nprovide high value high quality housing of the finest architectural standards as well \r\nas new age amenities at present we have succesfully completed Residential and commercial Projects.', 'Builder logo2015-12-14 13:33:35.svg', 687, 0),
('Saran Developers & Infrastructure India Pvt. Ltd', NULL, 'sathish@sarandevelopers.com', 'http://www.sarandevelopers.com/', 'Saran Developers is initiated by a bunch of young and accomplished individuals from backgrounds such as Real Estate Development, Agro-farming Technology & Landscape Development, Development of Gated Communities, Information Technology, Management and more.\r\nThe diverse mix brings a unique wholesomeness to the knowledge pool of the organization. The same is instrumental in building all-round value for the customers of Saran Developers.', 'Builder logo2015-12-14 13:37:44.svg', 688, 0),
('Saravana Buildwell Pvt. Ltd', NULL, 'saravanabuildwell@gmail.com', 'http://saravanabuildwell.com/', 'SARAVANA Buildwell Private Limited is a growing name on the real estate landscape in South India. With over twenty lakh square feet constructed area to its credit across residential, commercial, hospital, industrial and institutional projects, Saravana Buildwell Private Limited is a name to reckon with.\r\n\r\nThis Bangalore based venture had its genesis in 1988; it?s this wealth of experience backed by excellent reputation that helped its foray into real estate in 2000. Saravana Buildwell has since grown from strength to strength with comprehensive developments known for its unique blend of comfort, efficiency and understated luxury\r\n\r\nThe group is well led by Chairman and Managing Director Mr. K Nagaraj a civil engineer himself; with a strong focus on high quality homes. The company under his vision has grown from its humble beginning doing small format layouts, independent homes and small apartment complexes? to a front runner in large housing projects. The company also plans to move into the much acclaimed Villa Project ventures soon.\r\n\r\nSaravana Buildwell aims to serve and create self sufficient communities from pockets of land and provide customers with the highest lifestyle standards. The company envisions customer trust and loyalty based on expertise, foresight, skill, and attention to detail in the quality of work delivered. It highly concentrates on developing and committing to employ environmentally friendly processes and ensuring lush and verdant communities that assure a breath of fresh air and natural light in every Saravana home.\r\n\r\nA team of dedicated Architects, Engineers, Contractors, Marketing associates and customer service staff has been responsible for the various landmarks crafted to perfection. They have won critique appreciation and customer delight alike, while building a whole new way of eco friendly stress free living at Saravana homes.\r\n\r\nWe \"Build Well and Build to Last\"..', 'Builder logo2015-12-14 13:41:36.svg', 689, 0),
('Satwi Infra', NULL, 'info@satwiinfra.com', 'http://www.satwiinfra.com/', 'Satwi\'s Clarinet Home translates your dream of fine living into a blissful reality. Enjoy the space, convenience and location; signature style that transforms each room into an enhanced living experience. The project is located in the low density area yet within easy reach to the academic, health care Institutions, shopping malls, commercial establishments, banks, just drive 25 min Bangalore International Airport & Majestic, a very near to main Metro Railway Station (5 to 8min drive) and entertainment avenues offering the residents a picturesque and tranquil surrounding along with the best possible modern amenities and facilities. Designed with creative eloquence by a reputed architect.Satwi\'s Clarinet is attractively laid out with everything you visualize in lavish comfort? an expression of artistical sense of touch. .', 'Builder logo2015-12-14 13:45:14.svg', 690, 0),
('Sekhar Developers', NULL, 'info@sekhardevelopers.com', 'http://sekhardevelopers.com/', 'Sekhar Developers have always been competitive and dynamic when it comes to developing properties that we have set a trend for others to follow. We are a Bangalore headquartered company that believes in innovation and strategies. Our company has been a pioneer when it comes to changing the skyline of Bangalore, Hyderabad and many other cities to follow in the near future.\r\n\r\nSome of the most prominent residential project we have delivered in the recent past in Bangalore includes: Sekhar Ridge (Outer Ring Road), Sekhar Greater Kailash (Old Airport Road), Sekhar Kohinoor (Old Airport Road), Sekhar Yamunotri (Old Airport Road), Sekhar Kaveri (Old Airport Road) and Pleasant (Whitefield). Some of the residential projects we have completed in Hyderabad till date include: Sekhar Enclave (Manikonda Gardens), Sekhar Tirumala Hills (Manikonda Gardens), Sekhar Mansion (Banjara Hills), Sekhar Green Woods (Manikonda Gardens) and Sekhar Anugraha (Manikonda Gardens).\r\n\r\nSekhar Developers have also several ongoing projects in hand. In Bangalore, we have Sekhar Hyde Park (Whitefield), Lake View (Outer Ring Road) and White Palms (Whitefield) projects in progress.\r\n\r\nSekhar Developers? upcoming projects in Bangalore are: Sekhar Green Valley (Whitefield), Sekhar Alturas (Whitefield), Sekhar Belle Vue (Outer Ring Road) and Sekhar Olympus (Outer Ring Road, Horamavu).\r\n\r\nWith all the existing projects in hand and many more to come in the future, Sekhar Developers have carved a niche for itself in the real estate industry and lived up to its spirit that focuses mainly on innovation and delivery. Within a matter of a few years, we have been able to set a benchmark for excellent in-house research, reliable engineering, customer focus, customer focus, quality and uncompromising business ethics. Therefore, it is not surprising to see many clients preferring Sekhar Developers for their projects and hold it in high regards.\r\n\r\nThe technological and architectural innovation that is visible across all projects embarked by Sekhar Developers is not the only thing that separates us from the rest in the pack. Ours is a vision-based company that have core values like Transparency, Speed, Reliability, Quality and Punctuality embedded in its system.', 'Builder logo2015-12-14 13:47:56.svg', 691, 0),
('Shanders Group', NULL, 'shanders@shanders.com', 'http://www.shanders.com/', 'Shanders, is part of a business group that has been in existence since 1918.\r\n\r\nAt the time of the first industrial revolution, the group used to import ?Ruston? internal combustion engines, for application in agricultural processing industries and water pumping. Once the electrification started, the group diversified into the production of import substitutes of ?Lister? and ?Petter? internal combustion engines.\r\n\r\nLater, the group diversified into trading of industrial products. The group also represents major companies in the coastal districts of Andhra Pradesh. Subsequently, the group started manufacturing of rice mill machinery and there on developed several import substitutes.\r\n\r\nIn the 80?s the group entered into Real Estate Development, Manufacturing of essential oils and exported the same to advanced and quality conscious countries like Germany and France.\r\n\r\nIn the late 90?s, the group witnessed immense opportunities and potential for growth and development in the Agricultural sector and ever since has been active in this field.\r\n\r\nIn the 21st Century, the group set foot in the Supply/Cold Chain and Logistics, International Trade, Information Technology, Defence and Core Manufacturing.\r\n\r\nShanders, is a diversified group in the areas of Real Estate, Agriculture, Supply/Cold Chain and Logistics, Retail, International Trade, Information Technology, Defence and Manufacturing.', 'Builder logo2015-12-14 13:50:45.svg', 692, 0),
('Shreedevi Developers', NULL, 'sales@shreedevidevelopers.com', 'http://www.shreedevidevelopers.com/', 'When one puts in a decade of work in any field, you are sure to be an expert at what you specialise in. When we look into the past decade of experience in the construction and marketing field has truly paid its rich dividend. Shreedevi Developers is a professionally managed company that has placed client satisfaction above all else. We have a commitment to excellence that greets our customers at the doors of every Shreedevi Developers home. \r\n\r\nWith \"Built on Trust\" forming the under lying vision of the group, Shreedevi Developers will continue offering luxury at affordable prices to discerning citizens. Shreedevi Developers has added a new lease of life in the Bangalore real estate scenario, with their innovative ideas and distinct architectural executions, their projects are economical, class and epitomises the statement of modern living. \r\n\r\nIn fact investing in a Shreedevi Developers home spells assured appreciation of property year after year, thanks to the efforts we take to hand over CLEAR TITLE homes delivered ON TIME with NO COST ESCALATION to worry about. What more... every building have total customer satisfaction. \r\n\r\nNot surprising then, we enjoy a reputation for quality which you can always bank on. Commitment towards quality that is endorsed by major housing finance companies and institutions helping bring your dream house into the reality of today. \r\n\r\nCome. Discover your beautiful home a range of prices, sizes, amenities and locations.', 'Builder logo2015-12-14 13:54:37.svg', 693, 0),
('Shriram Properties Private Limited', NULL, 'info@shriramproperties.com', 'http://www.shriramproperties.com/', 'At Shriram Properties, we specialize in giving you homes that deliver to your high standards of living. Our living spaces are innovatively designed to\r\nmake the optimal use of space and provide you the best of comfort. Shriram Properties has an extensive presence across South India and is a part of\r\nthe reputed Shriram Group with a diversified portfolio of over 20 million sq. ft. of built-up space, with 12.61 million sq. ft.\r\nto be delivered in the next two years and 45.85 million sq. ft. under development.', 'Builder logo2015-12-14 13:59:39.svg', 694, 0),
('Sipani Properties Pvt. Ltd.', NULL, 'enquiry@sipaniproperties.com', 'http://www.sipaniproperties.com/', 'The Sipani Group is renowned for its pioneering work in the Indian automotive industry during the 1980s. When Indian consumers had a choice of just 2 cars, both using technology of the 1950s, the Sipani Group was the first in India to take the bold step of bringing in contemporary automotive technology to the Indian consumers. The Sipani Group pioneered an initiative through its very own car, The Sipani DOLPHIN, long before the arrival of multinational companies.\r\n\r\nIt was only after this bold move that other companies followed by bringing in new technology. This move literally changed the landscape and structure of the automotive industry. It was a revolutionary step for the transformation of the market; a move, the Sipani group is dignified and distinguished for. In 2004, the group forayed into the realty sector and went on to construct its first residential apartment project in 2009 ? Sipani Grande. It now has earned the distinct image of being considered the best apartment project in Koramangala, and furthermore is a symbol of its uncompromising business ethics.\r\n\r\nSipani Group is now involved in a variety of projects, ranging from Luxury to Economy. Its projects are truly where quality meets innovation, allowing the Sipani Group to bring its elaborate and well-thought plans to reality.', 'Builder logo2015-12-14 14:04:04.svg', 695, 0),
('Siroya Constructions', NULL, '', 'http://www.siroyaconstructions.com/', 'Siroya has since inception endeavored to develop itself into a leadership position in Real estate in India. With over two decades of experience in developing prime residential projects in the Middle East, Siroya is building strategic partnerships and focusing on leveraging its international expertise and quality standards to create and deliver highend facilities and projects in Mumbai and Bangalore. Apart from its interests in property development Siroya?s other business interests include pharmaceuticals, jewelry, mining, garments and trading. Website: www.legendsiroya.com', 'Builder logo2015-12-14 14:06:48.svg', 696, 0),
('Sizzle Properties Pvt Ltd', NULL, 'myhome@sizzleproperties.com', 'http://www.sizzleproperties.com/', 'Let us take this opportunity to introduce you to the family of Sizzle Properties (P) Limited.\r\n\r\nFounded in the year 2009, Sizzle Properties ( ISO 9001:2008 ) started with promoting Independent Houses in the East of Bangalore. Today, we have diversified interests in the field of Real Estate like Developing and Promoting Residential Communities (LAYOUTS) and Construction & Promotion of Independent Houses.\r\n\r\nOur Team is guided by an unwavering pursuit of Excellence. We push the boundaries and surge forward creating happy co-existing communities and Everlasting Relationships. We maintain high standards of  Ethical and Transparent Business Practices.\r\n\r\nWe are proud to develop and Promote Iconic and Landmark residential projects. Our promise to our customers is to provide absolutely dispute free properties so that your hard earned money is invested in perfect place and also not to mention that we provide quality amenities with the best pricing.\r\n\r\nWhen you invest with Sizzle Properties for your property, you get more than just a good property. You get thoughtful planning, convenient amenities and a warm welcoming environment. We understand that you don?t just buy a home or a plot: You are achieving your dreams.\r\n\r\nCome and visit our projects to start your journey of owning your Dream Property?.', 'Builder logo2015-12-14 14:09:32.svg', 697, 0),
('Skylark Mansions Pvt. Ltd', NULL, 'sales@skylarkmansions.com', 'http://www.skylarkmansions.com/', 'Building quality and creating lasting relationships\r\nSince its establishment in 1992, Skylark has built a rock solid reputation founded on trust, and over the years it has earned trust in plenty from customers, associates and vendors. Trust built brick by brick in the past two decades.\r\n\r\nClient satisfaction is the vision and the mission of Skylark and the company strongly believes ?Once a client always a client?. The company has always followed a policy of taking up development only on properties that have clear, litigation-free titles, with every aspect thoroughly vetted by its legal department. The department also ensures that every project has all required approvals from the urban development bodies. A dedicated team of professionals comprising architects, civil and structural engineers, landscape specialists, project managers, etc. then take up the responsibility of ensuring eco friendly spaces, optimization of resources and timely delivery every time. The result has been a reputation for quality that has spread far and wide through word of mouth and the recommendation of satisfied customers.\r\n\r\nSkylark has over 4.8 million square feet of construction under its belt spanning residential and commercial development. Its recent projects have been breaking new ground in the creation of living spaces and this project is yet another expression of Skylark?s desire to reach new heights by offering its customers more.', 'Builder logo2015-12-14 14:14:51.svg', 698, 0),
('SLV Builders', NULL, 'munikumarvatambeti@gmail.com', 'http://www.slvbuilders.net/', 'Bangalore is the capital city and the political, social and economic hub of the state of Karnataka. It is one of the fastest growing metropolitan cities in India today. The population of the city at 8 million in 2011 is expected to exceed 10 million by the year 2013. With less than 0.5% area of the state, it is home to more than 12% of its population. Bangalore is India?s fifth largest and its population has more than doubled over past 10 years.\r\n\r\nThe growth is attributable to economic reforms in India. The city?s growing popularity as a software destination and its ability to attract individuals to live and work has been commendable Historically the Government of India has taken the initiative to establish institution like MAC, HMT, ITC and ISRO etc., in Bangalore. This thrust has helped the city develop to a hub of information technology, electronics, precision engineering, aerospace, readymade textiles and electrical machinery. In fact, 33 of the top 200 software companies in the country are headquartered in Bangalore. It remains the IT hub of India, with over 70% of the revenues of the software industry being generated here.\r\n\r\nToday the Indian and Multinational companies, who are in IT related business occupy close to a million square feet of space and have paved the way for a lot more peer groups to absorb several million square feet of space. On the whole, the upswing in commercial (office and retail) and residential space transaction continues. As predicted by many international real estate consultants like Brooke International, CB Richard, Jones Lang LaSalle, Cushman Wakefield, most facilities which are ready/nearing completion have been leased by end-users.\r\n\r\nBangalore has emerged as one of Asia?s fastest growing cities, with a growth rate of 76% between 1981 to 1991. During 1995-96 the market values had risen by more than 100% in all segments. With increased urban development activity, the city had achieved the distinction of the highest growth rate in the county at more than 40%.', 'Builder logo2015-12-14 14:18:01.svg', 699, 0),
('SLV Constructions', NULL, '', 'http://slvconstructions.net/', 'The group has marked its foray into the infrastructure space by delivering hallmarks that have won the acclaim of competitors, industry and critics alike. Moveing ahead of time, the group takes pride in the hundreds of satisfied customers, who have relished and cherished the executed projects. Optimum resources, an experienced team, excellent infrastructure and strategic planning are integrated to take customer delight ahead of everything else.... ahead of the future as well.', 'Builder logo2015-12-14 14:20:38.svg', 700, 0),
('Sobha Limited', NULL, '', 'http://www.sobha.com/', 'With over 35 years of building monarchical palaces as well as luxurious villas and hotels in the Middle-East, Mr. P.N.C. Menon embarked on his entrepreneurial journey in India in the year 1994. In an era, when the construction industry was in its nascent stage, Mr. Menon revolutionised the industry by introducing world-class quality construction at truly affordable prices with focus on timely delivery. He helped shape the future of the real estate sector in India with his eye for detail and his determination to persistently deliver best-in-class products.', 'Builder logo2015-12-14 14:26:29.svg', 701, 0),
('Sowparnika Projects & Infrastructure Pvt. Ltd', NULL, 'info@sowparnika.com', 'http://www.sowparnika.com/', 'Working in sync with the National Vision of our Government of ?Homes for All by 2020?, Sowparnika has been delivering luxury housing projects at affordable prices. It has redefined the definition of affordable housing. With headquarters in Bangalore, Karnataka, Sowparnika Projects is a leading real estate developer in the Southern part of India.\r\n\r\nSowparnika with its years of experience in residential infrastructure has made a mark for itself in the industry. With projects across Karnataka, Kerala, Tamil Nadu and Maharashtra, Sowparnika Projects have become synonymous to amazingly high quality projects at affordable prices.\r\n\r\nSowparnika, believes in developing competencies in-house and for the same they have a team of experts in the field of design, quality check, plumbing, electrical, block work and fabrication are some of the activities, which are mostly outsourced by most of the real estate organizations.\r\n\r\nThe experts at Sowparnika, monitor and evaluates the quality of the construction in progress from the beginning, i.e. right from design to delivery of the project. Sowparnika Projects is a pioneer in the industry with more than a dozen of on time delivered projects to boast of. Sowparnika Projects aim to successfully incorporate the concept of Lean Six Sigma across processes in the organization.', 'Builder logo2015-12-14 14:33:29.svg', 703, 0),
('Splendid Group', NULL, 'Sales@Splendidgroup.in', 'http://splendidgroup.in/', 'Splendid Group is a Bangalore base real estate development company, focused on providing urban contemporary housing through the development of in - fill loft - soft communities. Splendid Group was founded with the goal of developing the highest quality, innovative urban housing products for consumers.\r\n\r\nSplendid Group is an in-town builder that thinks outside the box. The Company is managed under the able leadership and guidance of its founder and Managing Director Mr. Ram Mohan Vora. He has an extensive hands on experience in the construction industry including Interior design with focus on quality & workmanship for the past thirteen years. The Splendid Group caters to both residential and commercial development.', 'Builder logo2015-12-14 14:37:11.svg', 704, 0),
('Sree Harsha Developers ', NULL, '', 'http://www.sreeharshadevelopers.com/', 'Sree Harsha Developers was established in 2003. Since then, Sree Harsha Developers has grown to be one of the leading real estate developers, serving the needs of a discerning clientele. \r\n\r\nWith each project, we shape the rules that set standards in the real estate market. Buildings that are highly rated, whose value often appreciates faster than others in the same area. \r\n\r\nThe locations of our projects are so carefully chosen that they provide immense potential to investment. After all, every single rupee of yours is as valuable to us as it is to you.', 'Builder logo2015-12-14 14:39:57.svg', 705, 0),
('Sree Maheswara Developers ', NULL, 'sreemaheswaradev@gmail.com', 'http://www.sreemaheswaradevelopers.com/', 'We began our foray into the construction arena almost a decade ago. But the company?s area of expertise effortlessly straddles all the important categories. So far We have completed number of residential projects. The company believes its best contribution has been in the area of residential spaces and has gone on to carve a niche in this segment. \r\nWe bring to the table almost a decade of collective experience of its promoters, for whom quality, reliability and commitment occupy topmost priorities. The company has, to its credit, a wide range of completed projects which bear testimony to its high standards of excellence. ', 'Builder logo2015-12-14 14:42:48.svg', 706, 0),
('Sree Nandi Developers & Construction', NULL, 'ndc.3474@gmail.com', 'http://srinandidevelopers.com/', 'Our Team is a Certified Residential Specialist with over 10 years experience of helping local residents Property Buyers. Has achieved success due to commitment to the enhancement of the real estate profession, client service, and the community.', 'Builder logo2015-12-14 14:47:05.svg', 707, 0),
('Sreeda Homes', NULL, 'sreedahomes@gmail.com', 'http://www.sreedahomes.com/', 'What comes from Sreeda Homes is also laced with unmatched lifestyle & Luxury. We place location of your dream habitat always at a most idyllic location.\r\nSreeda Homes and their associate companies are established by a team of professionals with over two decades of experience in building residential and commercial complexes in Andhra Pradesh and Karnataka. All their constructions stands as testimonial for the quality and commitment. They have presented the next level lifestyle to their residents', 'Builder logo2015-12-14 14:53:30.svg', 708, 0),
('Sri Chowdeshwari Builders and Developers', NULL, '', 'http://prithvihomes.com/', 'We at Sri Chowdeshwari Builders and Developers & Prithvi Homes would like to thank all of our esteemed customers who have been instrumental in making us your preferred choice of real estate developer. It would not have been possible for us to be such a trusted name without your continuous support and encouragement.\r\n\r\nPrithvi Homes is a name now synonymous with Affordable yet Quality Construction and timliness of delivery. We are a young company, began its operations in the year 2005 and within a short span of time has successfully established its position as a thoughtful provider of apartment homes and commercial spaces within the real estate sector.\r\n\r\nWe care about our environment and our constructions are eco-friendly with Rain water harvesting and proper care being taken for garbage disposal. The quality of our constructions will fetch you returns over period of time, when the maintenance cost of the building will be less. We construct as per best practices of civil engineering\r\n\r\nOur latest Ongoing project Thirumala Blossom has got an excellence response and we are Soon Launching Thirumala Anemone, which is with a Lake View. Do not hesistate to contact our friendly team for any enquiry, questions or comments.', 'Builder logo2015-12-14 12:03:04.svg', 664, 0),
('Sri Krishna Constructions India Limited', NULL, 'jignesh@skcipl.in', 'http://www.skcipl.in/', 'Our interests are in Real Estate Development, Management and Investments. We are committed to providing our clients the best of Luxury, Quality, Ambience, Great Architecture and value-for-money products.\r\n\r\nWe are synonymous with the best in Amenities, Architecture, Aesthetics and Design.We have for the past several years provided it by identifying properties with huge appreciation potential.\r\n\r\nAt SKC, we realize the importance of every step in a project?s making. From procurement of raw material to post purchase service. From the amenities we offer to the layout of our projects. With the help and involvement of our qualified engineers and sales team, a system of checks and balance is established that ensures excellence and consistency. A system supervised by the top management of our company, from start to finish.\r\n\r\nOur online booking facilities ensure that you can get your dream SITE at your convenience. You can constantly review the progress of your project online. You?ll also find an online helpdesk that?ll answer your queries within 24 hours. Updates on the progress of plots are e-mailed to you regularly.', 'Builder logo2015-12-14 14:30:51.svg', 702, 0),
('Sri Krishna Constructions India Ltd', NULL, 'srikanth@skcipl.in', 'http://www.skcipl.in/', 'ri Krishna Constructions (India) Ltd. is a Public Limited Company registered under the Companies Act, 1956, formed in the year 2005. We are based in Bangalore and formed with the objective of creating a Corporate Brand in our business line of developing residential Townships in Bangalore. Since our foundation, we have offered our customers excellent returns on their real estate investments. SKC is synonymous with work-ethics, clarity, customer service and a quality home experience.', NULL, 711, 0),
('Sri Nandi Developers & Constructions', NULL, 'ndc.3474@gmail.com', 'http://srinandidevelopers.com/', 'Sri Nandi Developers & Construction Bangalore.\r\nOur Team is a Certified Residential Specialist with over 10 years experience of helping local residents Property Buyers. Has achieved success due to commitment to the enhancement of the real estate profession, client service, and the community.', 'Builder logo2015-12-14 07:58:17.svg', 639, 0),
('Sri Srinivasa Developers', NULL, 'srisrinivasadevelopers@gmail.com', 'http://www.srisrinivasadevelopers.com/', 'Based in Bangalore, SRI SRINIVASA DEVELOPERS is a leading name in real estate development. It is dedicated to delivering distinguished developments cherished by customers, of outstanding value and captivating designs.\r\n\r\nSRI SRINIVASA DEVELOPERS accurately determines all aspects on behalf of its esteemed customers while planning the look and feel of a new project. Committed to supreme quality, only the finest materials are used for construction with professional project management and timely delivery.\r\n\r\nEvery new project by SRI SRINIVASA DEVELOPERS easily surpasses its customer?s aspirations. It?s because the company adds enthusiasm, passion, and vision of creating not just high-rise towers but luxurious havens that enables its customers to relax, refresh and unwind. Another significant aspect is that all its projects provide enormous potential for investments. The value added features such as arranging flexible finance facility for eligible customers and pre-agreement and post occupancy services separate it from other firms.', 'Builder logo2015-12-14 15:00:40.svg', 712, 0),
('Sri Vani shelters', NULL, 'pradeep@srivanishelters.com', 'http://www.srivanishelters.com/', '', 'Builder logo2015-12-14 15:07:26.svg', 714, 0),
('Srinidhi Constructions', NULL, '', 'http://www.srinidhiconstructions.com/', 'SRINIDHI is a premier company in bangalore known to create unique landmarks, build value for many projects and to maintain quality standards to ensure customer satisfaction\r\n\r\nWe always see that our clients are living in comfort and joyfulness. Your experience with SRINIDHI shall be warm and personal like the homes we deliver to you.\r\n\r\nOur engineers and supervisors are trained to give our clients the very best. It has over a decade of experience in property deveopment with 6 residential projects completed so far and many more coming up in near future.', 'Builder logo2015-12-14 15:03:38.svg', 713, 0),
('Srinivasa Developers', NULL, 'info@srinivasadevelopers.com', 'http://www.srinivasadevelopers.com/', 'Srinivasa Developers founded by Mr. Pola Srinivasulu, is a name to reckon with, in the industry.Mr. Pola Srinivasulu has a Rich Experience of 25 years in the construction Industry with B. Tech., in Civil Engineering.\r\n\r\nSrinivasa Developers are Synonymous to Spacious Homes with High-End Quality. We believe in Constructing Aesthetically Designed and Economically Viable Residential Projects. Our Projects reflect sound Vaastu compliance with Ample Ventilation. Our USP is Spacious Quality Homes @ Value for Money Price. We at Srinivasa Developers believe in Integrity, Transparency and Fair Play. We are committed towards redefining Quality and as a team we epitomize Passion at Work.\r\n\r\nAt Srinivasa Developers ? it is just not a building but a lifestyle of enhanced living. We invite you to have a look at our previous projects to believe the spaces we create.\r\n\r\nYour imagination fuels our designs.\r\n\r\nSrinivasa Developers has been executing highly spacious projects in Bangalore for more than 2 decades. An approach towards every aspect of construction from design to execution to a finished flat is looked into with great care and detail.\r\n\r\nWe believe to bring in a project, we research our designs, with strategic location, using quality building material and methods with tests to adhere to quality.\r\n\r\nEvery project we deliver is an opportunity for not only excellent living standards but also as investment point. It is a balance we strike with clear spaces. Infact, we are reputed to provide such unique spaces which will thrill you and excite.', NULL, 715, 0),
('Sriven Estates', NULL, 'info@srivenestates.com', 'http://www.srivenestates.com/projects.php', 'Sriven Estates have been dedicated in our endeavor since 2012 to provide a decent healthy and pleasurable lifestyle in Bangalore with our quality construction.\r\n\r\nOur project with all modern amenities and high quality infrastructure, provide luxurious lifestyle to our customer?s requirements and we take all effort to make our customers comfortable. Our all projects are ideally located in good area and have easy connectivity to various nodes of the city.\r\n\r\nSriven Estates is a trusted Builder and Developer company in Bangalore for delivery project on time with quality construction.', 'Builder logo2015-12-14 15:13:23.svg', 716, 0),
('Sterling Developers Pvt. Ltd', NULL, 'sales@sterlingdevelopers.com', 'http://www.sterlingdevelopers.com/', 'Sterling Developers has been changing the landscape of the city since 1983. It is in the business of building luxury apartments and villas that delight and satisfy customers.This has helped build a reputation for excellence and quality in the industry.\r\n\r\nFor its loyal customer today, this focus on quality represents a long-term vision that shines through every Sterling property.\r\n\r\nSterling Developers was founded by Ramani Sastri ? Chairman & Managing Director, whose far-sightedness combined with the diligence of Shankar Sastri ? Joint Managing Director has shaped the company?s reputation for excellence in creating value for its customers. Under their leadership, Sterling Developers has built some of the best properties in Bangalore that include luxury flats, premium apartments, exquisite villas and commercial complexes.\r\n\r\nIn 2006, the company expanded its operations to Pune, reinforcing their commitment to extend quality homes, luxury apartments and villas, to today?s discerning customers across the country.', 'Builder logo2015-12-14 15:15:56.svg', 717, 0),
('Suavity Projects', NULL, 'info@suavity.in', 'http://www.suavity.in/', 'Suavity Projects is a pioneer in building high quality residential projects across Bangalore. Each and every Suavity home is designed keeping in mind the comfort and convenience for the home buyers. A home has a special place in every individual?s life. We believe in building homes that makes yours living truly memorable.\r\n\r\nCombining the core values of integrity, transparency and value for money, Suavity has gained the trust and confidence of many home buyers. At Suavity, we believe in building relations and nourishing it.\r\n\r\nAll our projects have equal importance to the amenities and the green space. We make sure we provide adequate parking space, landscape area, security, lift and common area.', 'Builder logo2015-12-14 15:18:06.svg', 718, 0),
('Sukritha Buildmann Pvt. Ltd', NULL, 'properties@buildmann.com.test-google-a.com', 'http://www.buildmann.com/', 'ABOUT US\r\nBuildmann is a Bangalore based property development company with over 10 completed projects in prime areas of Bangalore and Mysore. The company?s portfolio across the residential sector includes luxury villas, premium apartments and boutique residences. At Bangalore, Buildmann has developed projects across prime locations such as Whitefield ? KR Puram, Indiranagar, Frazer Town, Benson Town, Basavanagudi and Ulsoor. Buildmann?s management team comprises of professionals from top institutions like the Indian School of Business (ISB) Hyderabad, and have extensive experience in the infrastructure, construction and technology sectors.', 'Builder logo2015-12-14 15:22:15.svg', 719, 0),
('Sumadhura Infracon Pvt Ltd', NULL, 'info@sumadhuragroup.com', 'http://sumadhuragroup.com/', 'Sumadhura Infracon Pvt. Ltd. is a name that resonates among the prominent residential and commercial developers in Bangalore, India. Our ability to deliver on-time luxurious and affordable housing projects in Bangalore has ensured a continual interest from our customers. Our expertise in acquisition of land, appointment of architects and designers, construction, sales to after sales service, has helped us to be counted among Bangalore\'s forerunners in real estate development.', 'Builder logo2015-12-14 15:24:19.svg', 720, 0),
('Sumukha Constructions ', NULL, 'karthikn@sumukhaconstructions.com', 'http://sumukhaconstructions.co.in/', 'Sumukha Constructions is a property development company with a vision for every square feet of land. When the investment in development meets our returns criteria we initiate the development process. Our focus is on building multi functional residential apartments and villas. As on 1st June 2013 we have successfully completed developing 85,000 Sft which includes our apartment namely \"Sumukha Greenville\" off Bannerghatta Road. We are in the process of developing another 2,00,000 Sft of property across various centres in Bangalore. Sumukha Construction strives to take a property to its highest purpose in terms of development.', 'Builder logo2015-12-14 15:27:50.svg', 721, 0),
('Surbacon Development Pvt. Ltd', NULL, 'surbacon@gmail.com', 'http://surbacon.com/', 'GK Ravi Kumar, Director, Surbacon Development Private Limited (SDPL) has designed and constructed several luxury apartments in Bangalore since 1990.\r\n\r\nHe specializes in building spacious, modern, extremely well lit and ventilated homes for upper middle class couples and families. His projects typically have less than 50 apartments , facilitating a sense of privacy, peace and quiet for his buyers.\r\n\r\nHis buyers are varied - young professionals looking for a first home, businessmen looking for investment opportunities and NRIs looking for homes in which they can retire with all the amenities they would find abroad.\r\n\r\nGK Ravi Kumar is an Architect from IIT Kharagpur . Prior to starting his own company in 1991, he has designed and executed projects in Calcutta, Vishakapatnam, Malaysia, Indonesia, Tunisia and Northern Ireland.', 'Builder logo2015-12-14 15:49:45.svg', 722, 0),
('Suvastu Projects Pvt. Ltd', NULL, 'info@suvastuprojects.com', 'http://www.suvastugroup.com/', 'Having 8 years of experience, we offer comprehensive suite of services you can avail in the Real Estate World fulfilling your all kinds of queries and needs. We have invented our corporate structure to best serve our clients by enhancing collaboration across our organization. Our professionals serve the full spectrum of capital market surveys, competitor analysis, the best consultancy, setting selling prices, project site valuation and management services, supported by a comprehensive arrangement of economic and market research facilities.', 'Builder logo2015-12-14 15:52:08.svg', 723, 0),
('SVS Constructions', NULL, 'info@svsconstructions.in', 'http://www.svsconstructions.in/', 'SVS Constructions has, over the past decade, cemented the foundation for success in premier residential offerings to discerning clients.\r\n\r\nThe company\'s reputation as being the Premier Builder of choice cannot be contested, as client speak bears testimony to a lifetime of successful real estate experiences. Svs constructions sets extraordinary standards in prime realty priced exclusively for the quality conscious investor.\r\n\r\nWe extend to you our time-tested tradition of integrity, quality, and reliability. It is this coming together of old values and new innovation that sets SVS Constructions distinctly apart.', 'Builder logo2015-12-14 15:55:17.svg', 724, 0),
('T.G.R Projects India Pvt. Ltd', NULL, 'sales@tgrgroup.in', 'http://www.tgrgroup.in/', 'The company has been found by it\'s Chairman in the millennium year 2000. Well known in Bangalore east as \"Ankshu builder\'s & developer\'s\". Further a step ahead in 2012 the same group incorporated private limited company to provide their customer\'s a upgraded service.\r\n\r\nSince inception the company has always strived for benchmark quality, customer centric approach, robust engineering, in-house research, uncompromising business ethics, timeless values and transparency in all spheres of business conduct.\r\n\r\nThe backward integration model is one of the key competitive strengths of TGR. This literally means that the Company has all the competencies and in-house resources to deliver a project from conceptualization to completion.\r\n\r\nTGR is an organization where quality meets excellence, technology meets aesthetics and passion meets perfection.', 'Builder logo2015-12-14 15:58:47.svg', 725, 0),
('Tetra Grand Constructions Private Limited', NULL, 'Sales@tetragrand.co', 'http://www.tetragrand.com/', 'Tetra Grand Constructions (P) Ltd., is a construction enterprise promoted by Mr. RajKumar as Managing director, and Mr. Elisha, as Executive Directors. Both the promoters bring in their decades of rich experience and expertise from the construction field. They have handled huge government housing projects, construction contracts, and execution of mega structural work relating to the infrastructure projects. Hence their quest for quality, and insight for appetite for quality construction has resulted in starting Ms. Tetra Grand Constructions (P) Ltd\r\n\r\nTetra Grand Constructions (P) Ltd., is one of the successful construction company and launched many of residential and commercial projects in Kakinadata, Andhra Pradesh and Bangalore, Karnataka. It?s an organization where quality meets excellence, technology meets aesthetics and passion meets perfection. Tetra Grand Constructions (P) Ltd., have completed six Projects in Bangalore, Five Projects are ongoing and two are upcoming projects. \r\n', 'Builder logo2015-12-14 16:01:30.svg', 726, 0),
('Thanushree Properties', NULL, 'info@thanushreeproperties.com', 'http://www.thanushreeproperties.com/', 'Thanushree properties the prime Property Development Company based in Bangalore, India, is a highly specialized and established company in the real estate industry. We are dedicated to construction of residential properties, apartments, and commercial complexes. Our expertise in the same can be seen in our completed projects which are ranked among the best for both their design, and quality.\r\n\r\nWith intensive experience as real estate developers and builders in Bangalore, we have evolved as a company and achieved complete customer satisfaction. Our thorough understanding of the home owners? fancies and tastes has led to the development of best apartments and best residential properties in Bangalore.\r\n\r\nThanushree properties are known for both residential properties and commercial properties in Bangalore. Our innovative ideas, customer orientation, modern technology usage, and timely completion of projects have helped us gain client satisfaction. We have completed about ten impressive projects and have a list of upcoming residential and commercial projects in Bangalore.\r\n\r\nThanushree properties have to their credit a list of memorable projects which have achieved great customer satisfaction and won accolades which has landed it among the top builders in Bangalore. With highly experienced, dedicated and qualified work force the company is known for its professionalism, integrity, attention to detail, quality of work, architectural design and construction processes.', 'Builder logo2015-12-14 16:03:38.svg', 727, 0),
('Tharakans Properties Pvt. Ltd', NULL, '', 'http://tharakans-properties-pvt-ltd.zrealty.com/', 'Tharakans Properties Pvt. Ltd. is a young and progressive real estate development company that is built on the core values of ethics, integrity and a passion and commitment to the mother earth. Because we know that Homes are not built by mortars and bricks alone. Passion and Love are the key ingredients to all our real estate development projects. We come with years of experience in the field of real estate development collectively. Silently doing projects over the last few years and gaining valuable knowledge and wisdom in the field, we are taking our growth into the next level.', 'Builder logo2015-12-14 16:06:45.svg', 728, 0),
('Thipparthi Constructions & Housing Pvt. Ltd', NULL, 'sales@thipparthi.com', 'http://www.thipparthi.com/', 'Thipparthi Constructions & Housing Pvt. Ltd. is one of the leading Construction companies in South India with many successful ventures to its credit in Bangalore and Hyderabad.\r\nThe promoters of Thipparthi Constructions & Housing Pvt. Ltd. are a group of youthful technocrats with the zest and commitment to offer value added products, all of them with a proven track record in the construction industry.\r\nIn today?s world of increasing hype and clutter Thipparthi Constructions & Housing Pvt. Ltd. stands out as a refreshing change. All our promoters have relevant experience behind them and are driven by values. Though profits do form an integral part of business and it is the same at Thipparthi too, we take great pride in striving to give you better products which mean more value for money.', 'Builder logo2015-12-14 16:09:52.svg', 729, 0),
('Total Environment Building Systems Pvt. Ltd', NULL, 'discover@total-environment.com', 'http://www.totalenvironment.in/', 'To provide an unsurpassed living experience through the creation of sensitively designed inspiring spaces.', 'Builder logo2015-12-14 16:14:12.svg', 730, 0),
('Total Infra Solutions', NULL, '', 'http://www.totalsolutionsprojects.com/', 'We at \'Total Infra Solutions\' are one of the leading builders and developers engaged in property development, since a long time. We hold rich industrial experience and have in-depth knowledge which has proved to be an aid as we have successfully completed several projects. We have never compromised on our ethical business practices. This has enabled us to establish a trust worthy and transparent relationship with our clientele. \r\n\r\nAll these factors have made us achieve tremendous recognition and have helped us climb the ladder of success in a short span of time. Thanks to our fair business practices, we hope to become one of the leading brands in the real estate industry, in the near future. We have always exceeded our client expectations through our work and this has helped us achieve a reputation in the industry and our esteemed customers, associates and vendors trust and rely on us.', 'Builder logo2015-12-14 16:18:19.svg', 731, 0),
('Triaxis Colonnades', NULL, 'info@triaxis.in', 'http://www.triaxis.in/', 'Triaxis Colonnades is promoted by people with wide experience in the field with some very credible projects to their credit.\r\n\r\nOur team of dynamic professionals are working timelessly to develop dream homes with high standards at affordable pricing. Our projects are unique with no compromise in contemporary standards. We are synonymous with quality, customer satisfaction and dependability.\r\n\r\nWe take at most care in every step from land selection to design finalization till material procurement to ensure value for investments made by our customers. Our aim is to deliver high quality and reliable homes ensuring safety and sustainability. The strength of our company lies in transparent deals, loan guidance and open legal formalities.\r\n\r\nCustomer satisfaction is our ultimate goal. We are a customer oriented company and we believe in putting in our best foot forward in our journey to the pinnacle and we always listen to customers through various channels to deliver the best service.', 'Builder logo2015-12-14 16:22:38.svg', 732, 0);
INSERT INTO `builder` (`id`, `name`, `email`, `url`, `about_developer`, `logo`, `address`, `associate`) VALUES
('UKN Properties Pvt. Ltd', NULL, 'infinity@ukn.co.in', 'http://www.ukn.co.in/', 'Inspired by a philosophy of dynamic forms and strong expressiveness, UKn creates properties that are monumental, yet grounded in an understanding of human contexts and deep engagement with contemporary materials and techniques.\r\n\r\nIn the past decade, we\'ve applied these principles to create Commercial, Retail, Residential and Hospitality Buildings that are aesthetically uplifting as well as supremely functional and, ultimately, humane.\r\n\r\nOur team is experienced, enthusiastic and committed to working with you to create structures that serve human needs, while elevating human experience. To paraphrase the American architect and visionary Louis Kahn, we begin with the immeasurable and use measurable means to create something that, in the end, must be immeasurable - Investing in Infinity.\r\nInvest in Infinity\r\n', 'Builder logo2015-12-14 16:28:15.svg', 734, 0),
('Umiya Group', NULL, 'sales@umiyaindia.com', 'http://www.umiyaindia.com/', 'A vision to develop quality commercial, retail and residential properties inspired the setting up of Umiya in the year 2000 in Bangalore. After establishing itself in Bangalore for the first few years, Umiya expanded its footprint to Goa in 2007. Business is all about relationships & for Umiya, ultimately, our most important relationship is with our customers. We strive to provide a one-stop solution for all our customers? real estate needs, be it a home seeker, a commercial clients= or investor. We provide sales, leasing and property management services of a very high standard.\r\n\r\nUmiya?s portfolio reflects the aspirations of a broad range of home buyers encompassing apartments, luxury apartments, gated row villa communities & super luxurious independent villas. We have gained considerable experience in developing retail mall spaces at prime locations and office spaces for corporate offices, particularly the IT/ITES sector. We have constructed and are constructing various properties that are recognized for their ideal locations and superior build quality.\r\n\r\nWe continue to be one of the emerging players in the real estate development space. A dedicated team that is fuelled by a passion for quality, striving continuously to achieve customer delight stands behind the Umiya brand. We are on a journey and are confident that we will soon achieve our aim ? to be the Builder of Choice in this rapidly growing sector and extremely competitive Industry.', 'Builder logo2015-12-14 16:31:45.svg', 735, 0),
('Unicca', NULL, 'sales@unicca.in', 'http://www.unicca.in/', 'Having established itself over five beautiful years, Unicca Group is recognized as one of India\'s leading Real Estate Company. The company now has a strong presence in Bengaluru city in India. Quality, innovative technology, comfort, aesthetic appeal and maximum value for your money are few attributes that enrich Unicca Emporis with the belief that people have entrusted in them.', 'Builder logo2015-12-14 16:40:52.svg', 736, 0),
('Unishire', NULL, 'info@unishire.com', 'http://www.unishire.com/', 'Right from our inception in 1987, under the guidance of Mr Kirti Mehta, we have always believed in redefining real estate and creating benchmarks for others to follow. From the word \"go\" we have made our customers the centre of our universe and our raison d\'etre. Whatever we do is driven by the needs, dreams and aspirations of our customers.\r\n\r\nUNISHIRE as the name states, stands for Unique Communities (UNI - Unique | SHIRE ? Community). \r\n\r\nThis is the reason why we think beyond four walls and beyond brick and mortar. We understand that when someone buys a house, he or she also lays a foundation for being part of a community. At Unishire, we think of the individuals as well as everything that surrounds their daily lives.This has helped us create and offer international living standards to the global Indians who aspire to provide the best to their loved ones.\r\n\r\nIn just the past 1 year, Unishire has witnessed growth that has exceeded all expectations. From a team of just 20 people, the Unishire family has fast expanded to a strength of over 150 dynamic individuals.The 3 projects announced last year have quickly multiplied to 16 projects, thereby providing a multi-fold growth from 1 million sq.ft. to 10 million sq.ft. under planning and development. The 76 premium homes being developed last year have risen to over a 2000 homes today. From just Residential Projects, Unishire today boasts of projects across multiple verticals such as Commercial Spaces, Malls, Hotels, Hospitals, SEZ\'s, etc\r\n\r\nA deeply organized structure and a technically equipped and committed team of professionals have been the driving force behind Unishire\'s success story. Be it Sales and Marketing team, In-house Customer Care center, CRM Team, Construction team, Planning team, Technical team, Procurement, Legal, Finance or Creatives teams.', 'Builder logo2015-12-14 16:44:31.svg', 737, 0),
('Unitech Limited', NULL, '', 'http://www.unitechgroup.com/', 'Established in 1971 by a group of technocrats, Unitech Limited is one of India\'s leading Real Estate player. It started business as a consultancy firm for soil and foundation engineering and has grown to have the most diversified product mix in real estate comprising of world-class commercial complexes, IT/ITes parks, SEZs, integrated residential developments, schools, hotels, malls, golf courses and amusement parks.\r\nSo far Unitech has built more than 100 residential projects. Nirvana Country in Gurgaon is an integrated development complete with villas, apartments, offices, retail spaces, schools and clubs. Vista Villas, in Greenwood City, is a vast spread of landscaped meadows with villas of magical Mediterranean flair. These spacious homes on varying plot sizes are designed to be elegant as well as functional and blend perfectly into the green surroundings. Unitech continues to build hallmark and luxurious living spaces pan India. Karma Lakelands, nestled amidst pristine surroundings and breathtaking beauty, is spread over approximately 300 acres of nature. Nirvana Country 2, ensconced in the pristine locales of Sohna Road Gurgaon, is a highly sought after 100 acre integrated township. The Unitech Golf and Country Club, which offers 347 acres of ultra-luxury living with unparalleled views of a signature golf course, just off the expressway in Noida, is one of the upcoming prestigious address.\r\nUnitech has experience in developing and leasing IT/ITes and commercial office spaces in its Grade \'A\' complexes in Gurgaon like Cyber Park, Signature Towers, Global Business Parks, Unitech Business Park, Unitech Trade Centre, Millenium Plaza, Unitech Corporate Park, etc. Some recent launches have been Nirvana Courtyards II, Signature Towers II, Uniworld Towers and Infospace in Gurgaon, Bhubaneshwar 1 in Bhubaneshwar etc.\r\nUnitech has also developed world-class malls like Metro Walk in Rohini, The Great India Place in Noida, and Central in Gurgaon have been hugely successful. Currently, 4.5 million sq.ft. of retail space is already under construction in cities like Mumbai, Kolkata, Bengaluru, Hyderabad, Chandigarh, Dehradun, Amritsar, Bhopal, Mysore, Mangalore, Lucknow, Kochi, Trivandrum and Siliguri. Some of the recent launches have been Gardens Galleria in Bengaluru, Noida and Mohali, Great India Place in Bhopal and Dehradun and Downtown in Mohali.', 'Builder logo2015-12-14 16:48:04.svg', 738, 0),
('United Developers', NULL, 'unitedelysium@gmail.com', 'http://www.united-developers.co.in/', 'Sree United Developers have steadily made phenomenal growth with vast expanse of development activity in and around the state. They have good experience goes a long way in flawless, perfect and punctual execution. Excellent infrastructure to execute projects, introduction and application of innovative technologies from time to time, a team of well qualified engineers and technical personnel, managerial staff who put their heart and soul in to the projects. The following few projects - is a standing testimony to quality finish and magnitude of the projects handled, integrity, credibility and performance of the company.', 'Builder logo2015-12-14 16:50:47.svg', 739, 0),
('Unnathi Group India Inc', NULL, 'info@unnathigroupindia.com', 'http://www.unnathigroupindia.com/', 'Unnathi Group India Inc. is a modest but strong, consistent, multi-disciplinary organization that is committed to customer satisfaction. We have always banked on innovation, teamwork, and high degree of technical excellence to maintain the highest standards of business practice. Our goal is to provide plush, comfortable and clear marketable properties at prime locations that give you quick and assured appreciation. With several properties that are just a drive away from the New International Airport, our dream is to leave a distinguished mark on the skyline of North Bangalore. Unnathi aspires to create premium living spaces that are futuristic, comfortable, economical and strategically located at this exponentially growing urban landscape. - See more at: http://www.unnathigroupindia.com/?page_id=7#sthash.HCmU3sto.dpuf', 'Builder logo2015-12-15 04:11:16.svg', 740, 0),
('Upkar Developers', NULL, 'mail@upkar.co.in', 'http://www.upkar.co.in/', 'About four decades ago, more rapidly than ever before, people started moving to urban areas in search of Employment and shelter.A few visionaries like K.H.Khan could forsee the impending real estate demands and that is how Upkar came into being. Upkar group, under the able leadership of its founder Mr. K. H. Khan, developed several residential townships on the outskirts of the city to ease the pressure on the core areas of the city.', NULL, 741, 0),
('UTC India Pvt Ltd', NULL, 'sales@utcgroup.co.in', 'http://www.utcgroup.co.in/index.html', 'UTC Group established in the 2003 by a young & energetic entrepreneur, Mr. B.P.Gangahanumaiah having a decade of experience in the field of infrastructure development, land procurement, legalities & real estates. UTC Group today one of the leading developers in Bangalore having already completed 4 major residential gated communities, has created a niche for itself in the competitive realty market of the garden city.\r\nThe main objective of UTC Group has always been Quality, Transparency & perfection. Customer satisfaction & timely delivery has always been of at most importance.\r\n\r\n\r\nMr. B.P. Gangahanumaiah, CMD\r\nThe company is proud of its strong customer support & their referrals & recommendations have played a major part in speeding up the wheels of growth.\r\n\r\nWhat has given wings to our hard work is our core understanding of the segment, our ability to read the ever changing economic scenario, our culture, our people, our ethics, our belief and last but not the least our understanding of the needs of our customers. Since we know our trade inside out, it?s never been a problem for us to provide our clientele the apt investment solution.\r\n\r\nIt?s our belief in the system, belief in our strong marketing team, enthusiastic back office, our loyal team of vendors, satisfaction of our ever growing family of customers, has inspired us in bringing our upcoming projects & inspired us to step into the field of infrastructure, Commercial, Hospitality, Club & Resort development.\r\n\r\nWe?re opening up to new possibilities and new dreams for ourselves, our investors and customers.', 'Builder logo2015-12-15 04:18:24.svg', 742, 0),
('Vahe Projects Pvt Ltd', NULL, 'enquiry@vaheprojects.com', 'http://www.vaheprojects.com/', 'Welcome To Vahe Projects!\r\nIncepted in 2005, Vahe has been in the business of Construction Projects, Interiors Designing and Consultancy. In a short span of time, the company earned renown for its inclusive business philosophy and value-driven operations.\r\n\r\nIn the area of Construction, be it a Business Park, a residential cluster or even a customized campus, Vahe has an awesomely lined up expertise, economics and engineering abilities to do the project for you.\r\n\r\nWhen it is about Interiors, Vahe surpasses the client expectations in creativity, optimized utility, stunning concepts, and efficient economics. The company will design and execute strikingly imaginative contemporary and futuristic interiors to meet any purpose Consultancy is another strong arm of Vahe. The company provides its clients with an unfailing advice on varied ventures. Here again, it?s Vahe?s competence that gravitates clients. The company?s end-to-end consultancy process invariably aims at achieving results beyond ordinary perception.\r\n\r\nIn brief, with an incredible combination of technical competence, economic acumen, project management proficiency and commitment to eco-friendliness, Vahe stands among the most significant ones in the business.\r\n\r\nThe Bottomline - Vahe?s commitment to its customer goes beyond its business activity and extends to building customer loyalty and ensuring their satisfaction. So, when Vahe says \"we are inspired by you\" it is not just a catchphrase, but it is a true-to-conscience expression.', 'no logo2015-12-11 10:42:57.svg', 470, 0),
('Vaibhav Developers', NULL, 'info@vaibhavdevelopers.com', 'http://www.vaibhavdevelopers.com/', 'Thank you! Thank you for being a part of our 10 years of existence. As we step into the second decade of Vaibhav, I want to thank each one of you personally for making this dream possible.\r\n \r\nEvery company has a story to tell and we have a decade worth of story ingrained with passion, sweat, blood and happiness. We envision ourselves as a product development company focussing on building great products and hope people appreciate the fine attention to details and engineering that we put into the product.\r\n \r\nWe have completed 32 residential and commercial projects till date and want to continue doing our best for you.\r\n \r\nWe are rethinking the living spaces with a well-thought design process, smooth purchase process and stress-free customer service and support. We want to be associated with the likes of Bose and Mercedes. Such companies who do not bother about market cap or market share, but rather desiring to leave a legacy with every product they build. \r\n \r\nThis year, we have a lot in store for you. We are planning two high rise luxury condominiums, introducing Referral Program, Customer Satisfaction Program and so on among other things. We will keep you updated on some exciting things planned for this year.', 'Builder logo2015-12-15 04:25:13.svg', 743, 0),
('Vajram Group', NULL, 'info@vajramgroup.com', 'http://www.vajramgroup.com/', 'The Vajram Group is a new generation, Bangalore-based, real-estate development company transcending conventions in design, quality, construction and customer relationships based on its founding principles of Integrity, Quality, Commitment and Professionalism. Vajram\'s experience in delivering residential, hospitality and commercial projects is built on these four core values that are embedded in us from blueprint to brick. We believe that the key element of all our projects is always the CUSTOMER - a view shared by one of the most dedicated teams in the industry. We\'re about building more than just concrete structures and buildings ? we want to build homes and great working environments that will inspire and last the test of time. The best interests of our clients always come first and we place your concerns ahead of our own in each and every transaction, as we are dedicated to the development of long-term client relationships. Our unified customer-centric philosophy ensures that our clients\' investment and time are of paramount important to each and every member of our organization. We combine regional culture with international experience and our clients value this mix. Vajram\'s clients benefit from the strength of our in-house team of qualified and highly-skilled people. This is a factor that sets us apart in the construction industry today. We work to the highest standards and take personal pride in delivering a job well done based on our committed timelines. India\'s growing economy presents exciting prospects and growth opportunities. Over the last decade, the Vajram Group has used these opportunities to construct its organizational base with diversified interests in the technology, hospitality, aromatics and pharmaceutical sectors.', 'Builder logo2015-12-15 04:29:15.svg', 744, 0),
('Vakil Housing Development Corporation Pvt. Ltd', NULL, '', 'http://www.vakilhousing.com/', 'Vakil Housing Development Corporation is one of the leading developers of Residential Townships in Bangalore, that aims to create international quality living spaces at affordable rates. Commerce and conscience can co-exist. At Vakil, we?ve been proving doubters wrong, and customers right for the past 13 years now. Our success does not come at the cost of our customers, the environment, our\r\nemployees, or the projects itself.', 'Builder logo2015-12-15 04:32:31.svg', 745, 0),
('Valmark', NULL, 'info@valmark.in', 'http://valmark.in/', 'We have over two decades of experience and a proven track-record of excellence in the real estate sector. Our projects include the prestigious Manyata Tech Park & Residency in Hebbal, the gated communities of Classic Orchards on Bannerghatta Road and Classic County in Kengeri.\r\n\r\nWe also have joint ventures with several well-known real estate majors including Unitech, Brigade Group, Vaishnavi, Ashed Properties, Classic Group and Valdel Group for residential apartments, shopping malls, hotels, commercial office spaces and integrated townships. All these projects are located in and around Bangalore.\r\n\r\nWe guarantee top-quality project management, strict adherence to time schedules, excellence in execution and absolute value for your money. After all, Valmark stands for ?valuable landmarks.?', 'Builder logo2015-12-15 04:35:17.svg', 746, 0),
('Value Designbuild Pvt. Ltd', NULL, 'enquiry@vdb.co.in', 'http://www.valuedesignbuild.com/', 'Welcome to Value Designbuild, a Bangalore-based company that specializes in creating residential and commercial spaces that reflect functionality, form and finesse. We have to date developed several projects that are cherished by our clients for their design innovation, space utilization, and quality of construction. We have an impressive track record in identifying ?emerging hot spots? with growth potential translating to excellent ROI for our clients. We have consistently identified locations with immense potential for our projects. Since inception we have been guided by values that we would not neglect or overlook at any cost. These include:', 'Builder logo2015-12-15 04:46:54.svg', 747, 0),
('VARS Builders Pvt Ltd', NULL, 'marketing@varsbuilders.com', 'http://varsbuilders.com/', 'For 25 years, VARS has been at the heart of residential and commercial development in Bangalore. With over 40 projects and more than 1000 satisfied customers to our credit, we stand by our core principles of ethical construction, efficient and utilitarian design, an aesthetic that evolves with our customers, and relationships that continue long after we hand over the keys. Family owned and operated, we prize quality over profit and endeavour to construct homes as if they were our own.\r\n\r\nIn fact, we ensure that each one of our projects is based on something far stronger than concrete: \r\nvalues and ethics.', 'Builder logo2015-12-15 05:10:49.svg', 748, 0),
('Vasathi Housing Limited', NULL, '', 'http://www.vasathihousing.com/home/', 'Vasathi Housing Limited is an ISO 9001:2008 certified organization that is committed to excellence and on time delivery. Established in the year 2009, Vasathi has acquired a prominent position in a short span of time through building energy efficient and environment friendly residential projects. Vasathi Housing offers lasting value with an emphasis on contemporary designs, optimized layouts and cutting-edge technologies.', 'Builder logo2015-12-15 05:13:54.svg', 749, 0),
('Vaswani Group', NULL, 'sales@vaswanigroup.com', 'http://www.vaswanigroup.com/', 'As one of the leading property builders in India, Vaswani Group is a name denoting brilliance, excellence, versatility and quality in the real estate and commercial space. In 1992, Vaswani Group embarked on a corporate journey to offer global-standard residential solutions for the discerning home buyer as well as turnkey solutions for Indian and multinational clients. From resplendent housing to upscale commercial, our equity is strengthened with a proven and credible track record having developed close to 6 million square feet of built area in the verticals of Residential, Commercial, Retail and Hospitali', 'Builder logo2015-12-15 05:17:09.svg', 750, 0),
('VDB Infra & realty Pvt. Ltd', NULL, 'vbuildmore@gmail.com', 'http://vbuildmore.com/', 'VBuild More is a brainchild of group of qualified, talented and experienced entrepreneurs driven by core values of integrity,commitment and loyalty to customers. VBM Builder?s future driven architecture brings quality to life. We believe in creating a home?s based on understanding of consumer needs and preferences.', 'Builder logo2015-12-15 05:22:05.svg', 751, 0),
('Vedant Developers', NULL, 'sales@vedantprojects.com', 'http://www.vedantprojects.com/', 'Although from varied backgrounds, our leaders share a common vision and values; They possess a passion for building, a devotion to clients, a dedication to ensuring the company\'s - and people\'s - continued growth and prosperity.\r\n\r\nTeamwork is an essential part of the Vedant culture. Vedant combines a rigorous focus on outstanding construction with a dedication to superior client service. Our mission demonstrates our clear-cut ability to put our values and mission into action.\r\n\r\nWe at Vedant work as one family with a shared sense of values held in high esteem. Responsibility, dedication, perseverance and innovation are the key words that inspire us and as a culture these permeate our entire work atmosphere.', 'Builder logo2015-12-15 05:33:03.svg', 752, 0),
('Venetion Group', NULL, 'info@venetiongroup.com', 'http://www.venetiongroup.com/', 'Established in 1995, the Venetion Group has always made it a core principle to commit the very best resources towards building the ideal Property.\r\n \r\nThe Group has completed more than 50 projects in Karnataka, Hyderabad and Maharashtra with more than 22000 satisfied customers and has a giant knowledge in Real Estate, Hotel industry, Educational institutions, construction and civil contracts.\r\n\r\nLastly, Our Highly experienced in-house architects and interior designers do their best to ensure every Venetion project has a distinct aesthetic appeal.', 'Builder logo2015-12-15 05:35:53.svg', 753, 0),
('Veracious Builders And Developers Pvt. Ltd', NULL, 'enquiry@veraciousbuilders.com', 'http://veracious.in/index.html', 'Veracious Builders & Developers came to existence two decade ago, the Veracious today is a strong and vibrant organization committed to delighting our customers through innovation. Our architectural and technical innovation set customer a strong vision of the company, based on our enduring values such as Quality, reliability, punctuality and transparency. In the supremely dynamic and competitive sphere of property development, one name has consistently been a trail blazer, setting the trends for others to follow. The company is professionally managed by Board of Directors consisting of specialist from variant fields and walks; it is this trait that has always attracted the most talented people to our company. While we seek to bring innovation and fresh energy into the organization, we\'ve never under-valued our old relationships.\r\n\r\nAll the stakeholders who have been with us through our journey,Veracious has kept the promise to deliver in time; we have completed one million square feet and Member of CREDAI. We have grown to be one of the leading real estate developers in Bangalore, Today Veracious Builders is a household name across the Bangalore.\r\n\r\nVeracious project is a standing for excellent standards in design and construction. Over the years, we have created a distinctive mark on the skyline in Bangalore with our land mark developments. Veracious Builders are popular not only among a large populace residing in Bangalore, but also across India. And also Veracious subsidiary company Legno Door System Pvt. Ltd .into Manufacturers of Solid wooden doors, Modular kitchen, kitchen shutters, word robes & Hotel interiors etc., Our projects are known as some of the best addresses in town.', 'Builder logo2015-12-15 05:43:25.svg', 754, 0),
('Vibrant Structures', NULL, 'rights@vibrantstructures.com', 'http://www.vibrantstructures.com/', 'Vibrant as a group formed by a team who has a rich & excellent past experience of 15 years in Real Estate, Marketing, Customer service and Technology. Ready to take to the future with passion and dedication towards the growth & fulfill the dreams of people in having a home with.', 'Builder logo2015-12-15 05:46:56.svg', 755, 0),
('Vijaya Enterprises', NULL, 'vijaya1973@gmail.com', 'http://www.vijaya.in/', 'Since 1973, Vijaya Enterprises has carved out a gold standard reputation in real estate through landmark properties in the residential, commercial and industrial realms. Vijaya\'s work is driven by a deep sense of integrity, a bearing that reflects in its premium quality, ingenious design, sharp customer focus, on-time completion of projects, a grasp of changing times, and a balanced understanding of construction.', 'Builder logo2015-12-15 05:52:13.svg', 756, 0),
('Vijayas Ventures Pvt. Ltd', NULL, 'vijayaenqs@gmail.com', 'http://vijayasventures.com/', 'Vijayas Ventures Pvt. Ltd. are well established real estate developers in Bangalore, India . As a well-established firm we have a rich experience in land development for both residential and commercial purposes. with property development as its main focus. Vijayas Ventures group managed under the able guidance of Mr. GB Vijay Kumar and a team that is passionate and committed to delivering quality homes to customers while adding that extra dimension of lifestyle and returns on investments, consistently. Vijayas Ventures Pvt. Ltd. is fast emerging as one of the premium developers in South Bangalore. Our engineers and supervisors are trained to give our clients the very best.', 'Builder logo2015-12-15 05:56:50.svg', 757, 0),
('Vinayaka Homes', NULL, 'srivinayaka21@gmail.com', 'http://www.srivinayakaprojects.com/home-page.html', 'ri Vinayaka Projects is one of the well known Land dealers and developers in the city. We are committed to help people get a good piece of land in their preferred location.\r\n\r\nWith our rich experience in the real estate domain, we enjoy helping people to find the right property to invest on. Our core strength has been Utmost Reliability and high transparency while dealing with the clients.\r\n\r\nAt Sri Vinayaka Projects we provide a wide range of options to the buyers, this quality of ours is loved by our clients. We strive to provide a real value for the money invested on land by our clients because, Land is an investment if made wisely will never let them down.\r\n\r\nBangalore! The preferred destination of the Global IT World is also considered as one of the best place on earth to live. With a huge demand for the real estate, Land in Bangalore is getting occupied in a jiffy.\r\n\r\nSri Vinayaka Projects help its clients to find a right piece of land to construct their dream home. It has become our prime objective to add more value to our client?s chosen plot at no extra cost. \r\n\r\nSri Vinayaka Projects helps its clients to get a land which is more strategically located and this will add zing to the place where they live! Our construction business is loved by our clients as Sri Vinayaka Projects construct the residences within the affordability bracket of its clients. One more factor which makes our clients vouch for us is the on time delivery of projects. Yes! No construction delays has become the trademark of Sri Vinayaka Projects!\r\n\r\nConsistent Pro Client thinking has helped us to achieve this status. Sri Vinayaka Projects is also a land promoter, this makes us to be in the overall real estate gamut which helps us to get more knowledge in this field.', 'Builder logo2015-12-15 06:00:27.svg', 758, 0),
('Visalakshi Housing Projects', NULL, 'visalakshihousing@gmail.com', 'http://www.visalakshihousing.com/contact_us.html', 'Visalakshi Housing Projects is a name to reckon with in the real estate field in Bangalore. The company has excelled in the construction of residential apartments, residential layout and independent villas. A number of well executed and completed projects, Maintaining the highest standards in quality without any compromises on the customers\' expectations and delivery of projects within the promised time frame have been the principles at Visalakshi Housing Projects.\r\n\r\nNCC Urban Infrastructure Ltd., and Visalakshi Housing Projects have now come together to offer - Rajatha Greens another masterpiece in residential apartments.', 'Builder logo2015-12-15 06:06:14.svg', 759, 0),
('VKC Developers Pvt. Ltd', NULL, 'enquiry@vkcdevelopers.com', 'https://www.vkcdevelopers.com/', 'VKC Developers is dedicated to provide fashionable and convenient homes provided with all the ultra-modern amenities that match with the best of global standards, at affordable prices. We commit ourselves to impeccably translate the vision of the architect into concrete reality and dazzling homes .We surpass expectations and provide luxury homes, value homes nationwide. We will continue to raise the bar and set new standards in the industry to strengthen our goals .', 'Builder logo2015-12-15 06:10:22.svg', 760, 0),
('VR Residency Pvt Ltd', NULL, 'salesvrresidency@gmail.com', 'http://www.vrresidency.in/index.html', 'VR Group (VRRPL) is established by bright and dynamic entrepreneurs with vision are to construct commercial and residential homes with highest quality, state of the art, modern to the utmost satisfaction of every customer with competitive pricing. VRRPL believes in giving quality due priority with zero maintenance and offers highest value to the investment of every customer. Its solid belief in integrity, reliability, professionalism and commitment fetched great rewards.', 'Builder logo2015-12-15 06:16:05.svg', 761, 0),
('VRR Builders and Developers', NULL, 'info@vrrbuilders.com', 'http://vrrbuilders.com/', 'VRR & Co. has a successful track record of completing over 110 projects since 1984. H. N. Vijaya Raghava Reddy has accomplished many land mark buildings in bangalore, Karnataka for Indian Satellite Research Organisation (I. S. R. O.), Indian Institute of Science (I. I. Sc.), Jawaharlal Nehru Centre for Advanced Scientific Research (J. N. C. A. S. R.), Bharath Sanchar Nigam Limited (B. S. N. L.), Central Public Works Dept. (C. P. W. D.), to name a few.', 'Builder logo2015-12-15 06:20:40.svg', 762, 0),
('VSR Ventures', NULL, 'info@vsrventures.in', 'http://www.vsrventures.in/', 'Our approach to civil planning which shapes our projects is two fold-excellence in technical details and aesthetic interiors. so,there is absolutely no compromise or shortfall in quality. This is the reason why our project work is greater in value appreciation ,holds solid structural stamina and is long-lasting. as developer we consider every aspect in depth ,so that you can avoid the hassles later , Similarly ,we understand and appreciate the value of your hard earned money.Our primary accountability is to give you a trusted construction which commands your respect and goodwill for a lifetime.\r\n', 'Builder logo2015-12-15 06:23:50.svg', 763, 0),
('Wellnest India Projects Pvt. Ltd', NULL, '', 'http://www.wellnestindia.com/', 'At Wellnest, the aim is to provide our clientele, a lifestyle that is high on the luxury quotient as well as excellent opportunities to help them create wealth. We offer wide variety of Real Estate Products and Investment Services to choose from that are straightforward, simple, affordable and maximizing returns on investment.\r\n\r\nWellnest India Projects Pvt Ltd & VR Holdings was established by its Founder, Chairman & Managing Director, Mr. V. Chandrashekar in the year 2009, with a group of dynamic personalities who envisioned developing land properties in a way that respects the earth as well as the needs of its discerning customers.\r\n\r\nOur commitment is to uphold the best practices in the industry and help our valued customers realize their dreams of owning beautiful and unique living spaces. Wellnest has the advantage of having experts in construction, design, architecture, engineering and legal affairs coming to provide holistic living solutions.\r\n\r\nWellnest has strongly expanded its presence in the high growth corridors of North and East of Bangalore focusing on areas around Nandi Hills and Hoskote regions in particular. These regions are rapidly emerging as one of the fastest developing Commercial and Residential Hubs of Bangalore, as the most preferred futuristic Destinations for Smart Investors and home owners.\r\n\r\nWellnest is a professionally managed organization with a dedicated team, driven by passion. Passion backed by an unwavering commitment to collaborate with employees, vendors, partners and customers to ensure that, as an organization, we truly deliver THE BEST.\r\n\r\nAt WELLNEST, We conduct our business with utmost transparency and commitment to customers and our stakeholders. We are customer centric. We like to combine our goals and resources with the objectives and needs of our clients to ensure a strong partnership is built with our clients by focusing on their needs, aspirations and wellbeing through a harmonious experience.', 'Builder logo2015-12-15 06:27:12.svg', 764, 0),
('Whitestone Projects', NULL, 'sales@whitestoneprojects.com', 'http://whitestoneprojects.com/', 'Whitestone Projects was incorporated in 2010 as Whitestone Builders in Bangalore with a vision to offer Affordable Luxury for all classes. The firm was renamed in 2014 as Whitestone Projects in the process of converting into an LLP and then to a Private Limited company.\r\n\r\nWhitestone follows the key and unwritten rules in all its projects as depicted below:', 'Builder logo2015-12-15 06:29:59.svg', 765, 0),
('Windsor Gardens Pvt. Ltd', NULL, 'amulyam@windsordevelopers.com', 'http://www.windsordevelopers.com/', 'Windsor Gardens Pvt. Ltd has over the past decade, cemented the foundation for success in premier residential offerings to discerning clients.Incepted in 1995 Windsor Gardens Pvt. Ltd., Builders and Developers is a real estate company headed by M. Srinivasa Rao Managing Director.With long-term relationship with all our existing clientele who have been our backbone have helped us to successfully complete 17 years of experience in construction industry.We believe in quality, trust, integrity and time management a blend of which can be seen in projects we undertook. We give first priority i.e., customer satisfaction, to achieve that we always strive for better quality and better ambiance. Respond quickly to customer requests and property information.Our apartments are unique for design,interior finishing\'s and timeless contemporary architecture.We look forward to provide eye attracting environment around,Spacious surrounding,Proper lighting ,facility for electricity and water with ease to access to Malls, Schools, Restaurants, Parks and Greenways, Grocers, Cafes, and Bangalore\'s most sought after recreational spots.', 'Builder logo2015-12-15 06:45:14.svg', 766, 0),
('Yesh Developers', NULL, 'info@yeshdevelopers.com', 'http://www.yeshdevelopers.com/', 'YESH DEVELOPERS ? A leading developer of Residential Townships in Karnataka with a sustained focus on Bangalore, Mysore, Hubli and Dharwad. A fast growing, dynamic and professionally managed company, Yesh caters to the specialized housing needs of its various customer groups. With its primary focus on plotted land development coupled with an early mover advantage, Yesh has [?]', 'Builder logo2015-12-15 07:23:41.svg', 784, 0),
('Zaffar\'s Sterling Estates Pvt. Ltd', NULL, 'info@sterlingestatesindia.com', 'http://www.sterlingestatesindia.com/', 'Building Success One Day At a Time\r\nIncorporated in the year 1988, Zaffar\'s Sterling Estates is today one of the Bangalore\'s most respected developers in the market. Over the years under the leadership of its Managing Director Zaffar Fiaz, Sterling Estates has built a reputation that is grounded in its corporate vision, mission and philosophy of life.', 'Builder logo2015-12-15 06:50:11.svg', 767, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `member_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Member',
  `company_type_id` varchar(200) NOT NULL COMMENT 'Company Type',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `reg_off_address_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Registered Office Address',
  `head_off_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Head Office Address',
  `branch_off_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Branch Office Address',
  `pan_number` varchar(50) NOT NULL COMMENT 'PAN No.',
  `authorized_person_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Authorized Person',
  `mou_filepath` varchar(256) DEFAULT NULL COMMENT 'MOU',
  `aou_filepath` varchar(256) DEFAULT NULL COMMENT 'AOU',
  `email` varchar(255) NOT NULL COMMENT 'Email ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_type`
--

CREATE TABLE `company_type` (
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_type`
--

INSERT INTO `company_type` (`name`, `is_active`) VALUES
('LLP', 1),
('Private LTD', 1),
('Proprietorship', 1),
('Public LTD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `name` varchar(200) NOT NULL COMMENT 'Country Name',
  `code` varchar(20) NOT NULL COMMENT 'Country Code'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`name`, `code`) VALUES
('Abkhazia', 'GEO'),
('Afghanistan', 'AFG'),
('Aland', 'ALA'),
('Albania', 'ALB'),
('Algeria', 'DZA'),
('American Samoa', 'ASM'),
('Andorra', 'AND'),
('Angola', 'AGO'),
('Anguilla', 'AIA'),
('Antigua and Barbuda', 'ATG'),
('Argentina', 'ARG'),
('Armenia', 'ARM'),
('Aruba', 'ABW'),
('Ascension', 'ASC'),
('Ashmore and Cartier Islands', 'AUS'),
('Australia', 'AUS'),
('Australian Antarctic Territory', 'ATA'),
('Austria', 'AUT'),
('Azerbaijan', 'AZE'),
('Bahamas  The', 'BHS'),
('Bahrain', 'BHR'),
('Baker Island', 'UMI'),
('Bangladesh', 'BGD'),
('Barbados', 'BRB'),
('Belarus', 'BLR'),
('Belgium', 'BEL'),
('Belize', 'BLZ'),
('Benin', 'BEN'),
('Bermuda', 'BMU'),
('Bhutan', 'BTN'),
('Bolivia', 'BOL'),
('Bosnia and Herzegovina', 'BIH'),
('Botswana', 'BWA'),
('Bouvet Island', 'BVT'),
('Brazil', 'BRA'),
('British Antarctic Territory', 'ATA'),
('British Indian Ocean Territory', 'IOT'),
('British Sovereign Base Areas', ''),
('British Virgin Islands', 'VGB'),
('Brunei', 'BRN'),
('Bulgaria', 'BGR'),
('Burkina Faso', 'BFA'),
('Burundi', 'BDI'),
('Cambodia', 'KHM'),
('Cameroon', 'CMR'),
('Canada', 'CAN'),
('Cape Verde', 'CPV'),
('Cayman Islands', 'CYM'),
('Central African Republic', 'CAF'),
('Chad', 'TCD'),
('Chile', 'CHL'),
('China  People\'s Republic of', 'CHN'),
('China  Republic of (Taiwan)', 'TWN'),
('Christmas Island', 'CXR'),
('Clipperton Island', 'PYF'),
('Cocos (Keeling) Islands', 'CCK'),
('Colombia', 'COL'),
('Comoros', 'COM'),
('Congo  (Congo ? Brazzaville)', 'COG'),
('Congo  (Congo ? Kinshasa)', 'COD'),
('Cook Islands', 'COK'),
('Coral Sea Islands', 'AUS'),
('Costa Rica', 'CRI'),
('Cote d\'Ivoire (Ivory Coast)', 'CIV'),
('Croatia', 'HRV'),
('Cuba', 'CUB'),
('Cyprus', 'CYP'),
('Czech Republic', 'CZE'),
('Denmark', 'DNK'),
('Djibouti', 'DJI'),
('Dominica', 'DMA'),
('Dominican Republic', 'DOM'),
('Ecuador', 'ECU'),
('Egypt', 'EGY'),
('El Salvador', 'SLV'),
('Equatorial Guinea', 'GNQ'),
('Eritrea', 'ERI'),
('Estonia', 'EST'),
('Ethiopia', 'ETH'),
('Falkland Islands (Islas Malvinas)', 'FLK'),
('Faroe Islands', 'FRO'),
('Fiji', 'FJI'),
('Finland', 'FIN'),
('France', 'FRA'),
('French Guiana', 'GUF'),
('French Polynesia', 'PYF'),
('French Southern and Antarctic Lands', 'ATF'),
('Gabon', 'GAB'),
('Gambia  The', 'GMB'),
('Georgia', 'GEO'),
('Germany', 'DEU'),
('Ghana', 'GHA'),
('Gibraltar', 'GIB'),
('Greece', 'GRC'),
('Greenland', 'GRL'),
('Grenada', 'GRD'),
('Guadeloupe', 'GLP'),
('Guam', 'GUM'),
('Guatemala', 'GTM'),
('Guernsey', 'GGY'),
('Guinea', 'GIN'),
('Guinea-Bissau', 'GNB'),
('Guyana', 'GUY'),
('Haiti', 'HTI'),
('Heard Island and McDonald Islands', 'HMD'),
('Honduras', 'HND'),
('Hong Kong', 'HKG'),
('Howland Island', 'UMI'),
('Hungary', 'HUN'),
('Iceland', 'ISL'),
('India', 'IND'),
('Indonesia', 'IDN'),
('Iran', 'IRN'),
('Iraq', 'IRQ'),
('Ireland', 'IRL'),
('Isle of Man', 'IMN'),
('Israel', 'ISR'),
('Italy', 'ITA'),
('Jamaica', 'JAM'),
('Japan', 'JPN'),
('Jarvis Island', 'UMI'),
('Jersey', 'JEY'),
('Johnston Atoll', 'UMI'),
('Jordan', 'JOR'),
('Kazakhstan', 'KAZ'),
('Kenya', 'KEN'),
('Kingman Reef', 'UMI'),
('Kiribati', 'KIR'),
('Korea  North', 'PRK'),
('Korea  South', 'KOR'),
('Kuwait', 'KWT'),
('Kyrgyzstan', 'KGZ'),
('Laos', 'LAO'),
('Latvia', 'LVA'),
('Lebanon', 'LBN'),
('Lesotho', 'LSO'),
('Liberia', 'LBR'),
('Libya', 'LBY'),
('Liechtenstein', 'LIE'),
('Lithuania', 'LTU'),
('Luxembourg', 'LUX'),
('Macau', 'MAC'),
('Macedonia', 'MKD'),
('Madagascar', 'MDG'),
('Malawi', 'MWI'),
('Malaysia', 'MYS'),
('Maldives', 'MDV'),
('Mali', 'MLI'),
('Malta', 'MLT'),
('Marshall Islands', 'MHL'),
('Martinique', 'MTQ'),
('Mauritania', 'MRT'),
('Mauritius', 'MUS'),
('Mayotte', 'MYT'),
('Mexico', 'MEX'),
('Micronesia', 'FSM'),
('Midway Islands', 'UMI'),
('Moldova', 'MDA'),
('Monaco', 'MCO'),
('Mongolia', 'MNG'),
('Montenegro', 'MNE'),
('Montserrat', 'MSR'),
('Morocco', 'MAR'),
('Mozambique', 'MOZ'),
('Myanmar (Burma)', 'MMR'),
('Nagorno-Karabakh', 'AZE'),
('Namibia', 'NAM'),
('Nauru', 'NRU'),
('Navassa Island', 'UMI'),
('Nepal', 'NPL'),
('Netherlands', 'NLD'),
('Netherlands Antilles', 'ANT'),
('New Caledonia', 'NCL'),
('New Zealand', 'NZL'),
('Nicaragua', 'NIC'),
('Niger', 'NER'),
('Nigeria', 'NGA'),
('Niue', 'NIU'),
('Norfolk Island', 'NFK'),
('Northern Cyprus', 'CYP'),
('Northern Mariana Islands', 'MNP'),
('Norway', 'NOR'),
('Oman', 'OMN'),
('Pakistan', 'PAK'),
('Palau', 'PLW'),
('Palmyra Atoll', 'UMI'),
('Panama', 'PAN'),
('Papua New Guinea', 'PNG'),
('Paraguay', 'PRY'),
('Peru', 'PER'),
('Peter I Island', 'ATA'),
('Philippines', 'PHL'),
('Pitcairn Islands', 'PCN'),
('Poland', 'POL'),
('Portugal', 'PRT'),
('Pridnestrovie (Transnistria)', 'MDA'),
('Puerto Rico', 'PRI'),
('Qatar', 'QAT'),
('Queen Maud Land', 'ATA'),
('Reunion', 'REU'),
('Romania', 'ROU'),
('Ross Dependency', 'ATA'),
('Russia', 'RUS'),
('Rwanda', 'RWA'),
('Saint Barthelemy', 'GLP'),
('Saint Helena', 'SHN'),
('Saint Kitts and Nevis', 'KNA'),
('Saint Lucia', 'LCA'),
('Saint Martin', 'GLP'),
('Saint Pierre and Miquelon', 'SPM'),
('Saint Vincent and the Grenadines', 'VCT'),
('Samoa', 'WSM'),
('San Marino', 'SMR'),
('Sao Tome and Principe', 'STP'),
('Saudi Arabia', 'SAU'),
('Senegal', 'SEN'),
('Serbia', 'SRB'),
('Seychelles', 'SYC'),
('Sierra Leone', 'SLE'),
('Singapore', 'SGP'),
('Slovakia', 'SVK'),
('Slovenia', 'SVN'),
('Solomon Islands', 'SLB'),
('Somalia', 'SOM'),
('Somaliland', 'SOM'),
('South Africa', 'ZAF'),
('South Georgia & South Sandwich Islands', 'SGS'),
('South Ossetia', 'GEO'),
('Spain', 'ESP'),
('Sri Lanka', 'LKA'),
('Sudan', 'SDN'),
('Suriname', 'SUR'),
('Svalbard', 'SJM'),
('Swaziland', 'SWZ'),
('Sweden', 'SWE'),
('Switzerland', 'CHE'),
('Syria', 'SYR'),
('Tajikistan', 'TJK'),
('Tanzania', 'TZA'),
('Thailand', 'THA'),
('Timor-Leste (East Timor)', 'TLS'),
('Togo', 'TGO'),
('Tokelau', 'TKL'),
('Tonga', 'TON'),
('Trinidad and Tobago', 'TTO'),
('Tristan da Cunha', 'TAA'),
('Tunisia', 'TUN'),
('Turkey', 'TUR'),
('Turkmenistan', 'TKM'),
('Turks and Caicos Islands', 'TCA'),
('Tuvalu', 'TUV'),
('U.S. Virgin Islands', 'VIR'),
('Uganda', 'UGA'),
('Ukraine', 'UKR'),
('United Arab Emirates', 'ARE'),
('United Kingdom', 'GBR'),
('United States', 'USA'),
('Uruguay', 'URY'),
('Uzbekistan', 'UZB'),
('Vanuatu', 'VUT'),
('Vatican City', 'VAT'),
('Venezuela', 'VEN'),
('Vietnam', 'VNM'),
('Wake Island', 'UMI'),
('Wallis and Futuna', 'WLF'),
('Yemen', 'YEM'),
('Zambia', 'ZMB'),
('Zimbabwe', 'ZWE');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT 'Department Name',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `is_active`) VALUES
(1, 'Admin', 0),
(2, 'HR', 0),
(3, 'Marketing', 0),
(4, 'Support', 0),
(5, 'Canteen', 1);

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `designation` varchar(100) NOT NULL COMMENT 'Designation',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`designation`, `is_active`) VALUES
('Admin', 1),
('Executive-Properties', 1),
('Field-Executive', 1);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'District/City Name',
  `code` varchar(20) NOT NULL COMMENT 'District/City Code',
  `state` int(11) NOT NULL COMMENT 'State'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `code`, `state`) VALUES
(1, 'Port Blair', '45', 1),
(2, 'Hyderabad', '65', 4),
(3, 'Bangalore', 'BLR', 7);

-- --------------------------------------------------------

--
-- Table structure for table `executive`
--

CREATE TABLE `executive` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID',
  `mname` varchar(50) DEFAULT NULL COMMENT 'Middle Name',
  `lname` varchar(50) NOT NULL COMMENT 'Last Name',
  `fname` varchar(50) NOT NULL COMMENT 'First Name',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email ID',
  `address_proof_id` bigint(11) UNSIGNED NOT NULL COMMENT 'Address Proof',
  `created_on` datetime DEFAULT NULL COMMENT 'Created on',
  `last_active` datetime DEFAULT NULL COMMENT 'Last Active Date',
  `present_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Present Address',
  `permanent_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Permanent Address',
  `license_no` varchar(50) DEFAULT NULL COMMENT 'License No.',
  `license_expiry` date DEFAULT NULL COMMENT 'License Expiry Date',
  `department_id` int(11) DEFAULT NULL COMMENT 'Department',
  `designation` varchar(100) DEFAULT NULL COMMENT 'Designation',
  `joining_date` date DEFAULT NULL COMMENT 'Joining Date',
  `salary` float NOT NULL DEFAULT '0' COMMENT 'Salary',
  `applicable_for_incentives` int(11) DEFAULT NULL COMMENT 'Incentive Applicability',
  `applicable_for_food_allowance` int(11) DEFAULT NULL COMMENT 'Food Incentive Applicability',
  `mobile_bill_limit` float NOT NULL DEFAULT '0' COMMENT 'Mobile Bill Limit',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Status',
  `title` varchar(200) NOT NULL COMMENT 'Title',
  `gender` varchar(10) NOT NULL COMMENT 'Gender',
  `fathername` varchar(200) DEFAULT NULL COMMENT 'Father''s Name',
  `marital_status` varchar(20) DEFAULT NULL COMMENT 'Marital Staus',
  `appraisal_perc` decimal(10,2) DEFAULT NULL COMMENT 'Appraisal Percentage',
  `dob` datetime DEFAULT NULL COMMENT 'Date of Birth',
  `branch` varchar(100) DEFAULT NULL COMMENT 'Branch',
  `office_city` varchar(100) DEFAULT NULL COMMENT 'City',
  `licence_issue_place` varchar(100) NOT NULL COMMENT 'Licence Issue Place'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive`
--

INSERT INTO `executive` (`id`, `user_id`, `mname`, `lname`, `fname`, `email`, `address_proof_id`, `created_on`, `last_active`, `present_address_id`, `permanent_address_id`, `license_no`, `license_expiry`, `department_id`, `designation`, `joining_date`, `salary`, `applicable_for_incentives`, `applicable_for_food_allowance`, `mobile_bill_limit`, `status`, `title`, `gender`, `fathername`, `marital_status`, `appraisal_perc`, `dob`, `branch`, `office_city`, `licence_issue_place`) VALUES
(43, 101, 'Nimesha', 'Dravid', 'Rahul', 'Rahul@email.com', 377, '2015-10-13 19:28:33', NULL, 378, NULL, NULL, '1970-01-01', 4, 'Field-Executive', '2015-10-14', 15000, 1, 1, 500, 1, 'Mr', 'Male', 'Venkatesh', 'Single', '30.00', '2015-10-09 00:00:00', 'SanjayNagar', NULL, ''),
(44, 105, 'Papamma', 'Kumar', 'Ravi', 'ravi.m@gmail.com', 390, '2015-10-16 02:59:33', NULL, 391, NULL, 'KA-04/F/3950/1996', '2016-07-12', 3, 'Executive-Properties', '2015-10-20', 10000, 1, 1, 250, 1, 'Mr', 'Male', 'Muni Swamy', 'Married', '3.00', '1965-06-11 00:00:00', 'SanjayNagar', NULL, 'Bangalore'),
(45, 115, '', 'df', '435s', 'sdaf@gmaoicom.com', 921, '2016-02-24 19:06:50', NULL, 922, NULL, NULL, '1970-01-01', 1, 'Admin', '2016-02-22', 0, 0, 0, 0, 1, 'Mr', 'Female', '', 'Single', NULL, '1970-01-01 00:00:00', 'Peenya', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity`
--

CREATE TABLE `executive_activity` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'iD',
  `activity` varchar(200) NOT NULL COMMENT 'Activity',
  `exec_id` int(11) NOT NULL COMMENT 'Executive ID',
  `created_on` datetime NOT NULL COMMENT 'Created On',
  `start_date` datetime NOT NULL COMMENT 'Start Date',
  `end_date` datetime DEFAULT NULL COMMENT 'End Date',
  `status` varchar(50) NOT NULL COMMENT 'Status',
  `prop_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property ID',
  `remarks` text COMMENT 'Remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_attachment`
--

CREATE TABLE `executive_activity_attachment` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `file` varchar(200) NOT NULL COMMENT 'File',
  `remarks` text COMMENT 'Remarks',
  `activity_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Activity'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_log`
--

CREATE TABLE `executive_activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive ID',
  `property_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property ID',
  `member_id` int(11) DEFAULT NULL COMMENT 'Member ID',
  `tenant_id` int(11) DEFAULT NULL COMMENT 'Tenant ID',
  `collection` int(11) NOT NULL COMMENT 'Collection status',
  `payment` int(11) NOT NULL COMMENT 'Payment Status',
  `is_payable` int(11) NOT NULL COMMENT 'Is Payable?',
  `remarks` text COMMENT 'Remarks',
  `type` varchar(200) NOT NULL COMMENT 'Activity Type',
  `activity_id` int(11) NOT NULL COMMENT 'Activity',
  `date` date NOT NULL COMMENT 'Date',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Status',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_activity_log`
--

INSERT INTO `executive_activity_log` (`id`, `exec_id`, `property_id`, `member_id`, `tenant_id`, `collection`, `payment`, `is_payable`, `remarks`, `type`, `activity_id`, `date`, `status`, `category`) VALUES
(87, 105, 92, 116, 108, 0, 0, 0, NULL, 'Association Meetings', 38, '2016-03-05', 0, 2),
(88, 105, 92, 116, 108, 2, 2, 1, NULL, 'Rent Collection', 39, '2016-03-05', 2, 0),
(89, 105, 92, 116, 108, 1, 1, 1, NULL, 'Other activity', 40, '2016-03-05', 0, 1),
(90, 105, 92, 116, 108, 0, 0, 0, NULL, 'Periodic Visits', 41, '2016-03-05', 0, 3),
(91, 105, 92, 116, 108, 0, 0, 0, NULL, 'Photo session', 42, '2016-03-05', 0, 3),
(92, 105, 92, 116, 108, 2, 2, 1, NULL, 'Regular Maintenance Fee', 43, '2016-03-05', 2, 0),
(93, 105, 92, 116, 108, 2, 1, 1, NULL, 'Rent Collection', 44, '2016-03-05', 0, 0),
(94, 105, 92, 116, 108, 1, 1, 1, NULL, 'Water Bill', 45, '2016-03-05', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_property`
--

CREATE TABLE `executive_activity_property` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `tenant_service` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Tenant Service',
  `recursion` int(11) NOT NULL COMMENT 'Recursion',
  `collection` int(11) NOT NULL COMMENT 'Collection',
  `payment` int(11) NOT NULL COMMENT 'Payment',
  `collection_day` int(11) NOT NULL COMMENT 'Collection Day',
  `remainder_days` int(11) NOT NULL COMMENT 'Remainder Days',
  `next_collection_date` datetime NOT NULL COMMENT 'Next Collection Date',
  `owner_service` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Owner Service',
  `type` varchar(200) NOT NULL COMMENT 'Activity',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_activity_property`
--

INSERT INTO `executive_activity_property` (`id`, `tenant_service`, `recursion`, `collection`, `payment`, `collection_day`, `remainder_days`, `next_collection_date`, `owner_service`, `type`, `is_active`) VALUES
(1, 6, 4, 0, 0, 20, 15, '2016-07-01 00:00:00', NULL, 'Association Meetings', 1),
(2, 6, 1, 1, 1, 10, 5, '2016-04-01 00:00:00', NULL, 'Electricity Bill', 1),
(3, 6, 3, 0, 0, 1, 15, '2016-01-01 00:00:00', NULL, 'Periodic Visits', 1),
(4, 6, 2, 0, 0, 5, 5, '2016-04-29 00:00:00', NULL, 'Photo session', 1),
(5, 6, 1, 1, 0, 1, 5, '2016-04-01 00:00:00', NULL, 'Rent Collection', 1),
(6, 6, 1, 1, 1, 10, 6, '2016-03-29 00:00:00', NULL, 'Water Bill', 1),
(7, 6, 1, 1, 1, 10, 5, '2016-04-01 00:00:00', NULL, 'Regular Maintenance Fee', 1),
(8, NULL, 12, 1, 1, 1, 15, '2017-03-02 00:00:00', 22, 'Property Tax', 1),
(9, 7, 4, 0, 0, 20, 15, '2016-07-03 00:00:00', NULL, 'Association Meetings', 1),
(10, 7, 1, 1, 1, 10, 5, '2016-04-03 00:00:00', NULL, 'Electricity Bill', 1),
(11, 7, 1, 1, 1, 1, 10, '2016-04-03 00:00:00', NULL, 'Other activity', 1),
(12, 7, 3, 0, 0, 1, 15, '2016-06-03 00:00:00', NULL, 'Periodic Visits', 1),
(13, 7, 2, 0, 0, 5, 5, '2016-05-03 00:00:00', NULL, 'Photo session', 1),
(14, 7, 1, 1, 1, 10, 5, '2016-04-03 00:00:00', NULL, 'Regular Maintenance Fee', 1),
(15, 7, 1, 1, 1, 1, 5, '2016-04-03 00:00:00', NULL, 'Rent Collection', 1),
(16, 7, 1, 1, 1, 10, 6, '2016-04-03 00:00:00', NULL, 'Water Bill', 1),
(17, 8, 4, 0, 0, 20, 15, '2016-07-04 00:00:00', NULL, 'Association Meetings', 1),
(18, 8, 1, 1, 1, 10, 5, '2016-04-04 00:00:00', NULL, 'Electricity Bill', 1),
(19, 8, 1, 1, 1, 1, 10, '2016-04-04 00:00:00', NULL, 'Other activity', 1),
(20, 8, 3, 0, 0, 1, 15, '2016-06-04 00:00:00', NULL, 'Periodic Visits', 1),
(21, 8, 2, 0, 0, 5, 5, '2016-05-04 00:00:00', NULL, 'Photo session', 1),
(22, 8, 1, 1, 1, 10, 5, '2016-04-04 00:00:00', NULL, 'Regular Maintenance Fee', 1),
(23, 8, 1, 1, 1, 1, 5, '2016-04-04 00:00:00', NULL, 'Rent Collection', 1),
(24, 8, 1, 1, 1, 10, 6, '2016-04-04 00:00:00', NULL, 'Water Bill', 1),
(25, NULL, 12, 1, 1, 1, 15, '2017-03-04 00:00:00', 23, 'Property Tax', 1),
(26, 9, 4, 0, 0, 20, 15, '2016-03-05 00:00:00', NULL, 'Association Meetings', 1),
(27, 9, 1, 1, 1, 10, 5, '2016-03-05 00:00:00', NULL, 'Electricity Bill', 1),
(28, 9, 1, 1, 1, 1, 10, '2016-03-05 00:00:00', NULL, 'Other activity', 1),
(29, 9, 3, 0, 0, 1, 15, '2016-03-05 00:00:00', NULL, 'Periodic Visits', 1),
(30, 9, 2, 0, 0, 5, 5, '2016-03-05 00:00:00', NULL, 'Photo session', 1),
(31, 9, 1, 1, 1, 10, 5, '2016-03-05 00:00:00', NULL, 'Regular Maintenance Fee', 1),
(32, 9, 1, 1, 1, 1, 5, '2016-03-05 00:00:00', NULL, 'Rent Collection', 1),
(33, 9, 1, 1, 1, 10, 6, '2016-03-05 00:00:00', NULL, 'Water Bill', 1),
(34, NULL, 6, 0, 0, 15, 10, '2016-03-05 00:00:00', 24, 'Live video feed', 1),
(35, NULL, 12, 1, 1, 1, 15, '2016-03-05 00:00:00', 24, 'Property Tax', 1),
(36, NULL, 6, 0, 0, 15, 10, '2016-09-05 00:00:00', 25, 'Live video feed', 1),
(37, NULL, 12, 1, 1, 1, 15, '2017-03-05 00:00:00', 25, 'Property Tax', 1),
(38, 10, 4, 0, 0, 20, 15, '2016-07-05 00:00:00', NULL, 'Association Meetings', 1),
(39, 10, 1, 1, 1, 10, 5, '2016-04-05 00:00:00', NULL, 'Electricity Bill', 1),
(40, 10, 1, 1, 1, 1, 10, '2016-04-05 00:00:00', NULL, 'Other activity', 1),
(41, 10, 3, 0, 0, 1, 15, '2016-06-05 00:00:00', NULL, 'Periodic Visits', 1),
(42, 10, 2, 0, 0, 5, 5, '2016-05-05 00:00:00', NULL, 'Photo session', 1),
(43, 10, 1, 1, 1, 10, 5, '2016-04-05 00:00:00', NULL, 'Regular Maintenance Fee', 1),
(44, 10, 1, 1, 1, 1, 5, '2016-04-05 00:00:00', NULL, 'Rent Collection', 1),
(45, 10, 1, 1, 1, 10, 6, '2016-04-05 00:00:00', NULL, 'Water Bill', 1);

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_txn_details`
--

CREATE TABLE `executive_activity_txn_details` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `txn_no` varchar(100) NOT NULL COMMENT 'Transaction ID',
  `date` date NOT NULL COMMENT 'Date',
  `activity_log_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Log ID',
  `amount` decimal(10,2) NOT NULL COMMENT 'Amount',
  `mode` varchar(100) NOT NULL COMMENT 'Mode of Payment',
  `type` varchar(100) NOT NULL DEFAULT 'Collection' COMMENT 'Type of Transaction',
  `cheque_no` varchar(100) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `bank` varchar(200) DEFAULT NULL,
  `transaction_no` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_activity_txn_details`
--

INSERT INTO `executive_activity_txn_details` (`id`, `txn_no`, `date`, `activity_log_id`, `amount`, `mode`, `type`, `cheque_no`, `cheque_date`, `branch`, `bank`, `transaction_no`) VALUES
(107, 'ABCD4534', '2016-03-05', 88, '453.00', 'Cash*', 'Collection', NULL, NULL, NULL, NULL, NULL),
(108, 'ABDRE345345', '2016-03-05', 88, '453.00', 'Bill Uploads', 'Bill Collection', NULL, NULL, NULL, NULL, NULL),
(109, 'ABDRE345345', '2016-03-05', 88, '453.00', 'Slip Uploads', 'Slip Deposit', NULL, NULL, NULL, NULL, 'Owner'),
(110, 'ABDRE345345', '2016-03-05', 92, '500.00', 'Bill Uploads', 'Bill Collection', NULL, NULL, NULL, NULL, NULL),
(111, 'ABDRE345345', '2016-03-05', 92, '500.00', 'Slip Uploads', 'Slip Deposit', NULL, NULL, NULL, NULL, 'Owner'),
(112, 'ABCD4534', '2016-03-09', 93, '45.00', 'Cash*', 'Collection', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_txn_files`
--

CREATE TABLE `executive_activity_txn_files` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `file_path` varchar(255) NOT NULL COMMENT 'File',
  `txn_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Transaction ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_activity_txn_files`
--

INSERT INTO `executive_activity_txn_files` (`id`, `file_path`, `txn_id`) VALUES
(99, 'download (2)100419684.', 108),
(100, 'bill_dc_ref (2)372873397.csv', 109),
(101, 'bill_dc_ref (2)74291800.csv', 110),
(102, 'FabricServlet(2)188811817.', 111);

-- --------------------------------------------------------

--
-- Table structure for table `executive_activity_type`
--

CREATE TABLE `executive_activity_type` (
  `type` varchar(200) NOT NULL COMMENT 'Executive Activity Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'is Active?',
  `recursion` int(11) NOT NULL DEFAULT '1',
  `collection` int(11) NOT NULL DEFAULT '1',
  `payment` int(11) NOT NULL DEFAULT '0',
  `collection_day` int(11) NOT NULL,
  `remainder_days` int(11) NOT NULL,
  `remarks` text,
  `relates_to` int(11) NOT NULL DEFAULT '0' COMMENT 'Related To',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_activity_type`
--

INSERT INTO `executive_activity_type` (`type`, `is_active`, `recursion`, `collection`, `payment`, `collection_day`, `remainder_days`, `remarks`, `relates_to`, `category`) VALUES
('Association Meetings', 1, 4, 0, 0, 20, 15, 'sadf', 0, 2),
('Electricity Bill', 1, 1, 1, 1, 10, 5, '', 0, 0),
('Live video feed', 1, 6, 0, 0, 15, 10, 'k', 1, 3),
('Other activity', 1, 1, 1, 1, 1, 10, 'sdaf', 0, 1),
('Periodic Visits', 1, 3, 0, 0, 1, 15, '', 0, 3),
('Photo session', 1, 2, 0, 0, 5, 5, '', 0, 3),
('Property Tax', 1, 12, 1, 1, 1, 15, 'property tax', 1, 1),
('Regular Maintenance Fee', 1, 1, 1, 1, 10, 5, 'Regular Maintenance Fee', 0, 0),
('Rent Collection', 1, 1, 1, 1, 1, 5, 'Rent collection', 0, 0),
('Water Bill', 1, 1, 1, 1, 10, 6, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `executive_attachments`
--

CREATE TABLE `executive_attachments` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `attachment_type` varchar(100) NOT NULL COMMENT 'Attachment Type',
  `file_path` varchar(255) NOT NULL COMMENT 'File Path',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created Date',
  `remarks` text COMMENT 'Remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `executive_certification`
--

CREATE TABLE `executive_certification` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `certificate` varchar(200) NOT NULL COMMENT 'Certificate',
  `issued_date` date NOT NULL COMMENT 'Date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_certification`
--

INSERT INTO `executive_certification` (`id`, `exec_id`, `certificate`, `issued_date`) VALUES
(1, 44, '3rd year', '2015-10-16');

-- --------------------------------------------------------

--
-- Table structure for table `executive_computer_skill`
--

CREATE TABLE `executive_computer_skill` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `skill` varchar(50) NOT NULL COMMENT 'Skill',
  `expertise` varchar(50) DEFAULT NULL COMMENT 'Expertise'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_computer_skill`
--

INSERT INTO `executive_computer_skill` (`id`, `exec_id`, `skill`, `expertise`) VALUES
(1, 44, 'Basic of computer', 'Intermediate');

-- --------------------------------------------------------

--
-- Table structure for table `executive_education`
--

CREATE TABLE `executive_education` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `qualification` varchar(100) NOT NULL COMMENT 'qualification',
  `college_university` varchar(200) NOT NULL COMMENT 'College / University',
  `yop` int(11) NOT NULL COMMENT 'Year of Passing',
  `percentage` float DEFAULT NULL COMMENT 'Percentage'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_education`
--

INSERT INTO `executive_education` (`id`, `exec_id`, `qualification`, `college_university`, `yop`, `percentage`) VALUES
(1, 44, 'B. com', 'Reva College', 1996, 60);

-- --------------------------------------------------------

--
-- Table structure for table `executive_experience`
--

CREATE TABLE `executive_experience` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `company` varchar(200) NOT NULL COMMENT 'Company',
  `designation` varchar(100) NOT NULL COMMENT 'Designation',
  `from` date NOT NULL COMMENT 'From',
  `to` date NOT NULL COMMENT 'To',
  `remarks` text COMMENT 'Remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_experience`
--

INSERT INTO `executive_experience` (`id`, `exec_id`, `company`, `designation`, `from`, `to`, `remarks`) VALUES
(1, 44, 'Shop and Save departmental store', 'Manager', '2007-07-15', '2014-07-15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `executive_insurance`
--

CREATE TABLE `executive_insurance` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `policy_no` varchar(200) DEFAULT NULL COMMENT 'Policy No.',
  `amt` decimal(10,2) NOT NULL COMMENT 'Amount',
  `expiry_dt` date DEFAULT NULL COMMENT 'Expiry Date',
  `type` varchar(100) DEFAULT NULL COMMENT 'Insurance Type',
  `exec_id` int(11) NOT NULL COMMENT 'Executive ID',
  `duration` int(11) DEFAULT NULL COMMENT 'Duration Months',
  `date` date DEFAULT NULL COMMENT 'Date',
  `company` varchar(100) DEFAULT NULL COMMENT 'Company',
  `ins_amt` decimal(10,2) DEFAULT NULL COMMENT 'Total Insured Amount',
  `certificate` varchar(255) DEFAULT NULL COMMENT 'Insurance Certificate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_insurance`
--

INSERT INTO `executive_insurance` (`id`, `policy_no`, `amt`, `expiry_dt`, `type`, `exec_id`, `duration`, `date`, `company`, `ins_amt`, `certificate`) VALUES
(3, 'UNCL-PLY0000012120', '1500.00', '2015-10-16', NULL, 44, NULL, '2015-10-01', 'Royal Sundaram Insurance company Limited', '100000.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `executive_language`
--

CREATE TABLE `executive_language` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(10) NOT NULL COMMENT 'Executive',
  `language` varchar(50) NOT NULL COMMENT 'Language',
  `proficiency` varchar(100) DEFAULT NULL COMMENT 'Proficiency'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_language`
--

INSERT INTO `executive_language` (`id`, `exec_id`, `language`, `proficiency`) VALUES
(66, 43, 'English', 'Read,Write'),
(67, 43, 'Kannada', 'Read,Write'),
(68, 44, 'English', 'Read,Write'),
(69, 44, 'Kannada', 'Speak'),
(70, 44, 'Hindi', 'Speak'),
(71, 44, 'Tamil', 'Speak'),
(72, 44, 'Telugu', 'Speak'),
(73, 45, 'en', 'Speak');

-- --------------------------------------------------------

--
-- Table structure for table `executive_properties`
--

CREATE TABLE `executive_properties` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `property` bigint(20) UNSIGNED NOT NULL COMMENT 'Property',
  `date` date NOT NULL COMMENT 'Activation Date',
  `expiry_date` date DEFAULT NULL COMMENT 'Expiry Date',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `service_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_properties`
--

INSERT INTO `executive_properties` (`id`, `exec_id`, `property`, `date`, `expiry_date`, `is_active`, `service_id`) VALUES
(4, 105, 19, '2016-02-24', '2016-02-29', 1, 22),
(5, 105, 20, '2016-03-04', NULL, 1, 23),
(6, 105, 92, '2016-03-05', '2016-03-31', 1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `executive_reference`
--

CREATE TABLE `executive_reference` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `company` varchar(200) NOT NULL COMMENT 'Company',
  `exec_id` int(20) NOT NULL COMMENT 'Executive ID',
  `designation` varchar(200) NOT NULL COMMENT 'Designation',
  `phno` varchar(20) NOT NULL COMMENT 'Contact No.',
  `reference_letter` varchar(255) DEFAULT NULL COMMENT 'Reference Letter',
  `remarks` text COMMENT 'Remarks',
  `adrs` text COMMENT 'Address',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_reference`
--

INSERT INTO `executive_reference` (`id`, `name`, `company`, `exec_id`, `designation`, `phno`, `reference_letter`, `remarks`, `adrs`, `email`) VALUES
(1, 'Mohan', 'Resource Communication Pvt Ltd', 44, 'Manager', '99009 90055', NULL, '', 'No. 14, 6th Cross, Sai Baba Temple Road, Anandnagar, Bangalore, 560 033', 'mohan.m@yahoomail.com');

-- --------------------------------------------------------

--
-- Table structure for table `executive_travels`
--

CREATE TABLE `executive_travels` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `km` float NOT NULL COMMENT 'KM',
  `date` date NOT NULL COMMENT 'Date of Travelling',
  `fuel` float NOT NULL COMMENT 'Fuel Consumed',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created Date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `executive_travel_details`
--

CREATE TABLE `executive_travel_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_location` varchar(200) NOT NULL,
  `to_location` varchar(200) NOT NULL,
  `purpose` text NOT NULL,
  `distance` decimal(10,2) NOT NULL,
  `exec_id` int(11) NOT NULL COMMENT 'Executive ID',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_travel_details`
--

INSERT INTO `executive_travel_details` (`id`, `from_location`, `to_location`, `purpose`, `distance`, `exec_id`, `date`) VALUES
(1, 'Bangalore', 'Mysore sre', 'test', '543.00', 105, '2016-03-01'),
(2, 'sdf', 'sdf', '3w453', '43534.00', 105, '2016-03-01'),
(3, 'dfsdf', '435453', '543', '345.00', 105, '2016-03-01'),
(4, 'ds', '45966', '435', '435435.00', 105, '2016-03-01'),
(5, 'sadf', 'sdf', '435', '6545.00', 105, '2016-03-01'),
(6, 'sda', 'tes', 'tes', '3453.00', 105, '2016-03-01'),
(7, 'miou89798', 'ljo;', 'lkhk', '646.00', 105, '2016-03-01'),
(8, 'Banglaore', 'Mysore', 'visit', '200.00', 105, '2016-03-04'),
(9, 'ban', 'drgfsvd', '43543', '5645.00', 105, '2016-03-05');

-- --------------------------------------------------------

--
-- Table structure for table `executive_vehicle`
--

CREATE TABLE `executive_vehicle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exec_id` int(11) NOT NULL COMMENT 'Executive',
  `vehicle_no` varchar(20) NOT NULL COMMENT 'Vehicle No.',
  `vehicle_type` varchar(100) NOT NULL COMMENT 'Vehicle Type',
  `make` varchar(100) DEFAULT NULL COMMENT 'Make',
  `model` varchar(100) DEFAULT NULL COMMENT 'Model',
  `insurance` varchar(100) DEFAULT NULL COMMENT 'Insurance',
  `insurance_expiry_dt` date DEFAULT NULL COMMENT 'Insurance Expiry Date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `executive_vehicle`
--

INSERT INTO `executive_vehicle` (`id`, `exec_id`, `vehicle_no`, `vehicle_type`, `make`, `model`, `insurance`, `insurance_expiry_dt`) VALUES
(2, 44, 'KA-04 HJ 1542', 'Two wheeler', 'Hero', 'Splendor Plus', 'United India Insurance Company Limited', '2016-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `flat_accessibility_type`
--

CREATE TABLE `flat_accessibility_type` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Is Active??'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_accessibility_type`
--

INSERT INTO `flat_accessibility_type` (`id`, `name`, `is_active`) VALUES
(1, 'j', 1);

-- --------------------------------------------------------

--
-- Table structure for table `flat_amenity_type`
--

CREATE TABLE `flat_amenity_type` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Is Active??'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_finance`
--

CREATE TABLE `flat_finance` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `maintenance_charges` decimal(10,3) NOT NULL COMMENT 'Maintenance Charges',
  `agreement_type` int(11) NOT NULL COMMENT 'Agreement Type',
  `agreement_id` int(11) NOT NULL COMMENT 'Agreement Id',
  `preferred_payment_id` int(11) NOT NULL COMMENT 'Preferred Payment Id',
  `preferred_tenant_type` varchar(200) NOT NULL COMMENT 'Preferred Tenant Type ',
  `preferred_food_habits` varchar(200) NOT NULL COMMENT 'Preferred Food Habits'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_inventory`
--

CREATE TABLE `flat_inventory` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `name` int(11) NOT NULL COMMENT 'Name'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_lease`
--

CREATE TABLE `flat_lease` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `min_expected_deposit` decimal(10,2) NOT NULL COMMENT 'Minimum Expected Deposit',
  `max_expected_deposit` decimal(10,2) NOT NULL COMMENT 'Maximum Expected Deposit',
  `fixed_lease` decimal(10,2) NOT NULL COMMENT 'Fixed Lease',
  `lease_years` int(11) NOT NULL COMMENT 'Lease Years'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_media`
--

CREATE TABLE `flat_media` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `file_path` varchar(200) NOT NULL COMMENT 'File Path',
  `media_type` varchar(200) NOT NULL COMMENT 'Media Type',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On',
  `is_active` int(11) NOT NULL COMMENT 'Is Active??'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_rent`
--

CREATE TABLE `flat_rent` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `min_expected_rent` decimal(10,2) NOT NULL COMMENT 'Minimum Expected Rent',
  `max_expected_rent` decimal(10,2) NOT NULL COMMENT 'Maximum Expected Rent',
  `fixed_rent` decimal(10,2) NOT NULL COMMENT 'Fixed Rent',
  `min_deposit` decimal(10,2) NOT NULL COMMENT 'Minimum Deposit',
  `max_deposit` decimal(10,2) NOT NULL COMMENT 'Maximum Deposit',
  `fixed_deposit` decimal(10,2) NOT NULL COMMENT 'Fixed Deposit',
  `yearly_increment` decimal(10,2) NOT NULL COMMENT 'Yearly Increment'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `govt_approvals`
--

CREATE TABLE `govt_approvals` (
  `approvals` varchar(100) NOT NULL COMMENT 'Approvals',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `icon` varchar(200) DEFAULT NULL COMMENT 'Icon'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `govt_approvals`
--

INSERT INTO `govt_approvals` (`approvals`, `is_active`, `icon`) VALUES
('BBMP', 1, NULL),
('BDA', 1, NULL),
('BIAAPA', 1, NULL),
('BMRDA', 1, NULL),
('DTCP', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gratuity_services`
--

CREATE TABLE `gratuity_services` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `tos` varchar(200) NOT NULL COMMENT 'Type of Service',
  `request_service` varchar(200) NOT NULL COMMENT 'Request Service'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gratuity_services`
--

INSERT INTO `gratuity_services` (`id`, `tos`, `request_service`) VALUES
(3, 'Apartment / House Monitoring', 'Electricity Bill payment'),
(4, 'Apartment / House Monitoring', 'Masonry'),
(5, 'Apartment / House Monitoring', 'Plumbing'),
(6, 'Apartment / House Monitoring', 'Water Bill');

-- --------------------------------------------------------

--
-- Table structure for table `individual`
--

CREATE TABLE `individual` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `member_id` int(11) NOT NULL,
  `local_person` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Local Person',
  `residential_status` varchar(50) DEFAULT NULL COMMENT 'Residential Status',
  `permanent_address_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Permanent Address',
  `present_address_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Present Address',
  `company_name` varchar(200) DEFAULT NULL COMMENT 'Company Name',
  `designation` varchar(100) DEFAULT NULL COMMENT 'Designation',
  `office_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Office Address',
  `profile_image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `title` int(11) UNSIGNED NOT NULL COMMENT 'Title',
  `fname` varchar(200) NOT NULL COMMENT 'First Name',
  `lname` varchar(200) NOT NULL COMMENT 'Last Name',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `dob` date NOT NULL COMMENT 'Date of Birth',
  `marital_status` varchar(10) NOT NULL COMMENT 'Marital Status',
  `spouse` varchar(200) DEFAULT NULL COMMENT 'Spouse',
  `wed_ann` date DEFAULT NULL COMMENT 'Wedding Anniversary',
  `gender` varchar(10) NOT NULL COMMENT 'Gender',
  `relation` varchar(50) DEFAULT NULL COMMENT 'Relation',
  `relative` varchar(100) DEFAULT NULL COMMENT 'Relative',
  `pan` varchar(50) DEFAULT NULL COMMENT 'PAN No.',
  `pan_filepath` varchar(255) DEFAULT NULL COMMENT 'PAN Document',
  `id_proofpath` varchar(255) DEFAULT NULL COMMENT 'ID Proof Document',
  `id_proof_doctype` varchar(50) DEFAULT NULL,
  `present_address_proof` varchar(250) DEFAULT NULL,
  `permanent_address_proof` varchar(255) DEFAULT NULL COMMENT 'Permanent Address Proof'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `individual`
--

INSERT INTO `individual` (`id`, `member_id`, `local_person`, `residential_status`, `permanent_address_id`, `present_address_id`, `company_name`, `designation`, `office_address_id`, `profile_image`, `title`, `fname`, `lname`, `email`, `dob`, `marital_status`, `spouse`, `wed_ann`, `gender`, `relation`, `relative`, `pan`, `pan_filepath`, `id_proofpath`, `id_proof_doctype`, `present_address_proof`, `permanent_address_proof`) VALUES
(27, 103, NULL, 'Resident India', 379, 380, 'Mahendra Word City', 'Director', 381, '', 1, 'Vinayk', 'Gowda', 'Vinayak.g@gmail.com', '1951-05-28', 'Married', 'Maya', '1986-06-11', 'Male', 'Son of', 'B M Kuttappa', 'AKRPR122J', NULL, NULL, '', NULL, NULL),
(28, 109, 1, 'Resident India', 416, 417, 'AGES', 'Manager', 418, '', 1, 'Vijay', 'Kumar', 'vijay@gmail.com', '1969-06-15', 'Married', 'Rajeshwari', '1985-07-12', 'Male', 'Son of', 'Muni Swamy', 'AACE3205A', '09202537889.jpg', NULL, '', NULL, NULL),
(29, 110, 2, 'Resident Indian', 426, 427, 'Element-D Digicom Pvt Ltd', 'Project Coordnator', 428, '', 1, 'raghu', 'Dorai', 'raghu0604@gmail.com', '1983-09-11', 'Single', '', '1970-01-01', 'Male', 'Son of', 'Dorai Swamy', 'AACC24092A', NULL, NULL, NULL, NULL, NULL),
(30, 111, 3, '', 436, 437, 'Value Home Properties Pvt Ltd', 'Telecaller', 438, '', 2, 'Malini D', 'Dharmaraya', 'malinidharmaraya@gmail.com', '1997-04-08', 'Single', '', '1970-01-01', 'Female', 'Daughter of', 'Dharmaraya', '0822', NULL, NULL, NULL, NULL, NULL),
(32, 113, 4, 'Resident Indian', 446, 447, 'Arvi Systems Pvt Ltd', 'sales Executive', 448, '', 1, 'Shashi', 'Kumar', 'shashi0605@ymail.com', '1975-10-26', 'Married', 'Barathi', '1997-09-12', 'Male', 'Son of', 'Velu Swamy', 'AACCEE347A', NULL, NULL, NULL, NULL, NULL),
(33, 114, NULL, 'Resident Indian', 912, 913, '', '', NULL, '', 1, 'Vireena', 'Subbaiah', 'bk.subbaiah52@gmail.com', '1964-11-30', 'Married', 'B K Subbaiah', '1970-01-01', 'Female', 'Wife of', 'B K Subbaiah', '', NULL, NULL, NULL, NULL, NULL),
(34, 116, NULL, 'NRI', 923, 924, 'Medies', 'md', 925, '', 1, 'binu', 'v', 'binu@gmail.com', '2016-03-05', 'Single', '', '1970-01-01', 'Male', 'Son of', 'vinu', '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Inventory Name',
  `cat` varchar(200) NOT NULL COMMENT 'Inventory Category',
  `description` text COMMENT 'Description',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `name`, `cat`, `description`, `is_active`) VALUES
(2, 'Bulb', 'Electronics & Electrical', 'Bulb', 1),
(5, 'Refrigerator', 'Electronics & Electrical', 'Refrigerator', 1),
(6, 'Washing Machines', 'Electronics & Electrical', 'Washing Machines', 1),
(7, 'Microwave Ovens', 'Electronics & Electrical', 'Microwave Ovens', 1),
(8, 'Water Purifiers', 'Electronics & Electrical', 'Water Purifiers', 1),
(9, 'Tubelight', 'Electronics & Electrical', 'Tubelight', 1),
(10, 'CFL bulb', 'Electronics & Electrical', 'CFL bulb', 0),
(11, 'Chandelier', 'Electronics & Electrical', 'Chandelier', 1),
(12, 'LED Lights', 'Electronics & Electrical', 'LED Lights', 1),
(13, 'Ceiling Fan', 'Electronics & Electrical', 'Ceiling Fan', 1),
(14, 'Pedestal Fan', 'Electronics & Electrical', 'Pedestal Fan', 1),
(15, 'Wall Mount Fan', 'Electronics & Electrical', 'Wall Mount Fan', 1),
(16, 'Table Fan', 'Electronics & Electrical', 'Table Fan', 1),
(17, 'Exhaust Fan', 'Electronics & Electrical', 'Exhaust Fan', 1),
(18, 'Air Conditioners', 'Electronics & Electrical', 'Air Conditioners', 1),
(20, 'Racks & Shelves ', 'Plumbing', 'Racks & Shelves ', 1),
(21, 'Ceiling Fan', 'Electronics & Electrical', 'Ceiling Fan', 1),
(22, 'Pedestal Fan', 'Electronics & Electrical', '', 1),
(23, 'Pedestal Fan', 'Electronics & Electrical', '', 1),
(24, 'Pedestal Fan', 'Electronics & Electrical', '', 1),
(25, 'Pedestal Fan', 'Electronics & Electrical', '', 1),
(26, 'Wall Mount Fan', 'Electronics & Electrical', '', 1),
(27, 'Generator', 'Electronics & Electrical', '', 1),
(28, 'UPS', 'Electronics & Electrical', '', 1),
(29, 'Geyser', 'Electronics & Electrical', '', 1),
(30, 'Geyser', 'Electronics & Electrical', '', 1),
(31, 'Shower Head', 'Plumbing', '', 1),
(32, 'Shower Head', 'Plumbing', 'Shower Head', 1),
(33, 'Towel Holders ', 'Plumbing', 'Towel Holders ', 1),
(34, 'Toothbrush Holders ', 'Plumbing', 'Toothbrush Holders ', 1),
(35, 'Faucets', 'Plumbing', 'Faucets', 1),
(36, 'Liquid Dispensers ', 'Plumbing', 'Liquid Dispensers ', 1),
(37, 'Toilet Paper Holders ', 'Plumbing', 'Toilet Paper Holders ', 1),
(38, 'Curtain Rings & Hooks ', 'Plumbing', 'Curtain Rings & Hooks ', 1),
(39, 'Soap Dishes ', 'Plumbing', 'Soap Dishes ', 1),
(40, 'Hooks', 'Plumbing', 'Hooks', 1),
(42, 'Wall paints', 'Painting', 'Wall paints', 1),
(43, 'Hardwood', 'Flooring', 'Hardwood', 1),
(44, 'Bamboo', 'Flooring', '', 1),
(45, 'Ceramic Tile', 'Flooring', 'Ceramic Tile', 1),
(46, 'Wall paints', 'Painting', 'Wall paints', 1),
(47, 'Primers', 'Painting', 'Primers', 1),
(48, 'Latex', 'Painting', 'Latex', 1),
(49, 'Acrylic', 'Painting', 'Acrylic', 1),
(50, 'Eggshell or Satin paint', 'Painting', 'Eggshell or Satin paint', 1),
(51, 'Glossy Paints', 'Painting', 'Glossy Paints', 1),
(52, 'Wall Shelves', 'Furnishing', 'Wall Shelves', 1),
(53, 'Shoe Racks', 'Furnishing', 'Shoe Racks', 1),
(54, 'Magazine Holders', 'Furnishing', 'Magazine Holders', 1),
(55, 'Kitchen Racks', 'Furnishing', 'Kitchen Racks', 1),
(56, 'Dressing Table Brands', 'Furnishing', 'Dressing Table Brands', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_brands`
--

CREATE TABLE `inventory_brands` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `inventory` int(11) NOT NULL COMMENT 'Inventory',
  `brand` varchar(200) NOT NULL COMMENT 'Brand Name'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_brands`
--

INSERT INTO `inventory_brands` (`id`, `inventory`, `brand`) VALUES
(4, 18, 'LG'),
(5, 18, 'Whirlpool'),
(6, 18, 'Samsung'),
(7, 18, 'Electrolux'),
(8, 18, 'Videocon'),
(9, 18, 'Voltas'),
(10, 18, 'Hitachi'),
(11, 18, 'Daikin'),
(13, 18, 'Blue Star'),
(14, 18, 'Panasonic'),
(15, 18, 'Onida'),
(16, 18, 'Lloyd'),
(17, 18, 'Godrej'),
(18, 18, 'Carrier'),
(19, 18, 'Sansui'),
(20, 5, 'LG'),
(21, 5, 'Whiirlpool'),
(22, 5, 'Samsung'),
(23, 5, 'Kelvinator'),
(24, 5, 'Haier'),
(25, 5, 'Electrolux'),
(26, 5, 'Videocon'),
(27, 5, 'Hitachi'),
(28, 5, 'Panasonic'),
(29, 5, 'Bosch'),
(30, 5, 'BPL'),
(31, 5, 'GEM'),
(32, 5, 'IFB'),
(33, 5, 'Sansui'),
(34, 5, 'Onida'),
(35, 6, 'Bosch'),
(36, 6, 'BPL'),
(37, 6, 'Godrej'),
(38, 6, 'Electrolux'),
(39, 6, 'LG'),
(40, 6, 'kelvinator'),
(41, 6, 'GEM'),
(43, 6, 'Haier'),
(44, 6, 'Whirpool'),
(45, 6, 'Videocon'),
(46, 6, 'Siemens'),
(47, 6, 'Panasonic'),
(48, 6, 'Samsung'),
(49, 6, 'Hitachi'),
(50, 6, 'IFB'),
(51, 6, 'Bosch'),
(52, 6, 'Videocon'),
(53, 6, 'Electrolux'),
(54, 6, 'Kelvinator'),
(55, 6, 'Onida'),
(56, 6, 'BPL'),
(57, 6, 'Intex'),
(58, 6, 'Lloyd'),
(59, 6, 'Sharp'),
(60, 7, 'Samsung'),
(61, 7, 'IFB'),
(62, 7, 'Whirlpool'),
(63, 7, 'Panasonic'),
(64, 7, 'Morphy Richard'),
(65, 7, 'Electrolux'),
(66, 7, 'Godrej'),
(67, 7, 'Bajaj'),
(68, 7, 'Onida'),
(69, 7, 'Kenstar'),
(70, 7, 'Haier'),
(71, 7, 'Pelonis'),
(72, 7, 'Midea'),
(73, 7, 'LG'),
(74, 8, 'Whirlpool'),
(75, 8, 'Bajai'),
(76, 8, 'Philips'),
(77, 8, 'Godrej'),
(78, 8, 'Crompton Greaves'),
(79, 8, 'Kent'),
(80, 8, 'Aquaguard'),
(81, 8, 'Panasonic'),
(82, 8, 'Black & Decker'),
(83, 8, 'Pureit'),
(84, 8, 'Kelvinator'),
(85, 8, 'Livpure'),
(86, 8, 'Blossom'),
(87, 8, 'Bluemount'),
(88, 8, 'Delta'),
(89, 8, 'Eurotech'),
(90, 8, 'Krona'),
(91, 8, 'Eureka Forbes'),
(92, 8, 'Livepure'),
(93, 18, 'Bajaj'),
(94, 18, 'Crompton Greaves'),
(95, 18, 'Havells'),
(96, 18, 'Kenstar'),
(97, 18, 'Khaitan'),
(98, 18, 'LG'),
(99, 18, 'McCoy'),
(100, 18, 'Mitsoni'),
(101, 18, 'Maharaja Whiteline'),
(102, 18, 'Orient Electric'),
(103, 18, 'Orpat'),
(104, 18, 'Oster'),
(105, 18, 'Ram Coolers'),
(106, 18, 'Samsung'),
(107, 18, 'Whirlpool'),
(108, 12, 'Philips'),
(109, 12, 'Osram'),
(110, 12, 'Havells'),
(111, 12, 'Wipro'),
(112, 12, 'Eveready'),
(113, 12, 'Syska'),
(114, 12, 'Oreva'),
(115, 12, 'Moser Baer'),
(116, 12, 'Surya'),
(117, 12, 'Crompton Greaves'),
(118, 9, 'Philips'),
(119, 9, 'Osram'),
(120, 9, 'Havells'),
(121, 9, 'Wipro'),
(122, 9, 'Bajaj'),
(123, 9, 'Eveready'),
(124, 9, 'Syska'),
(125, 9, 'Oreva'),
(126, 9, 'Moser Baer'),
(127, 9, 'Surya'),
(128, 9, 'Crompton Greaves'),
(129, 9, 'Ujjawal'),
(130, 9, 'Ajanta'),
(131, 9, 'Anchor'),
(132, 12, 'Ujjawal'),
(133, 12, 'Ajanta'),
(134, 12, 'Anchor'),
(135, 10, 'Philips'),
(136, 10, 'Osram'),
(137, 10, 'Havells'),
(138, 10, 'Wipro'),
(139, 10, 'Bajaj'),
(140, 10, 'Eveready'),
(141, 10, 'Oreva'),
(142, 10, 'Moser Baer'),
(143, 10, 'Surya'),
(144, 10, 'Crompton Greaves'),
(145, 10, 'Ujjawal'),
(146, 10, 'Ajanta'),
(147, 10, 'Anchor'),
(148, 11, 'Philips'),
(149, 11, 'Osram'),
(150, 11, 'Havells'),
(151, 11, 'Bajaj'),
(152, 11, 'Eveready'),
(153, 11, 'Syska'),
(154, 11, 'Oreva'),
(155, 11, 'Moser Baer'),
(156, 11, 'Crompton Greaves'),
(157, 11, 'Anchor'),
(158, 11, 'Wipro'),
(159, 11, 'Ajanta'),
(160, 13, 'Havells'),
(161, 13, 'Usha'),
(162, 13, 'Orient'),
(163, 13, 'Bajaj'),
(164, 13, 'Khaitan'),
(165, 13, 'Crompton Greaves'),
(169, 13, 'Ajanta'),
(170, 13, 'Surya'),
(171, 13, 'Panasonic'),
(195, 22, 'Usha'),
(196, 22, 'Crompton Greaves'),
(197, 22, 'V-Guard'),
(198, 22, 'Fabbiano'),
(199, 22, 'Bravo'),
(200, 23, 'Padmini'),
(201, 23, 'Alfa'),
(202, 23, 'Black And Decker'),
(203, 23, 'Kanchan'),
(204, 23, 'Orient'),
(205, 24, 'Havells'),
(206, 24, 'Bajaj'),
(207, 25, 'Khaitan'),
(208, 26, 'Usha'),
(209, 26, 'Havells'),
(210, 26, 'Bajaj'),
(211, 26, 'Crompton Greaves'),
(212, 26, 'Orient'),
(213, 26, 'Victor TVS'),
(214, 26, 'Khaitan'),
(215, 27, 'Siemens'),
(216, 27, 'Honda'),
(217, 27, 'Kirloskar'),
(218, 27, 'Rapower'),
(219, 27, 'Mahindra Power'),
(220, 28, 'Philips'),
(221, 28, 'Sollatek'),
(222, 28, 'Crompton Greaves'),
(223, 28, 'V-Guard'),
(224, 28, 'Belkin'),
(225, 28, 'Microtek'),
(226, 29, 'Bajaj'),
(227, 29, 'Crompton Greaves'),
(228, 29, 'Maharaja Whiteline'),
(229, 29, 'cascade'),
(230, 29, 'Eazys'),
(231, 29, 'Gilma'),
(232, 29, 'Hindware'),
(233, 29, 'Hotstar'),
(234, 29, 'Inalsa'),
(235, 29, 'Koryo'),
(236, 29, 'Marc'),
(237, 29, 'Racold'),
(238, 29, 'Sammer'),
(239, 29, 'Remson'),
(240, 29, 'Sigma'),
(241, 29, 'Valka'),
(242, 29, 'Venus'),
(243, 29, 'O General'),
(244, 30, 'Usha'),
(245, 30, 'Havells'),
(246, 30, 'Kenstar'),
(247, 30, 'Morphy Richard'),
(248, 30, 'Singer'),
(249, 30, 'V-Guard'),
(250, 30, 'Inalsa'),
(251, 30, 'Racold'),
(252, 30, 'Valka'),
(254, 30, 'Kent'),
(311, 31, 'Jaguar'),
(312, 31, 'Hindware'),
(313, 31, 'Aai'),
(314, 31, 'Anox'),
(315, 31, 'Benelave'),
(316, 31, 'Byc'),
(317, 31, 'CBM'),
(318, 31, 'Delta'),
(319, 31, 'Dolphy'),
(320, 31, 'DRC'),
(321, 31, 'Goeka'),
(322, 31, 'Graffiti'),
(323, 31, 'Greggs'),
(324, 31, 'Haneez'),
(325, 31, 'Jaaz'),
(326, 31, 'Jolly\'s'),
(327, 31, 'Kitsch'),
(328, 31, 'Klassik'),
(329, 31, 'MSI'),
(330, 31, 'Parkovic'),
(331, 31, 'Parryware'),
(332, 31, 'Royal'),
(333, 31, 'Sanimart'),
(334, 31, 'Simoll'),
(335, 31, 'Tatay'),
(336, 31, 'TMC'),
(337, 31, 'Truphe'),
(338, 31, 'Turnip'),
(339, 31, 'Zahab'),
(340, 20, 'Johnson'),
(341, 20, 'Aai'),
(342, 20, 'Bharat'),
(343, 20, 'CBM'),
(344, 20, 'Chrome'),
(345, 20, 'Decowell'),
(346, 20, 'Delight'),
(347, 20, 'Designo'),
(348, 20, 'Disha'),
(349, 20, 'Dolin'),
(350, 20, 'Dolphy'),
(351, 20, 'Euro Glass'),
(352, 20, 'Fancy Frost'),
(353, 20, 'Fancy Glass'),
(354, 20, 'Fancy Glass Frosted'),
(355, 20, 'Fancy Glass Shelf'),
(356, 20, 'Ferrokare'),
(357, 20, 'Forlive'),
(358, 20, 'G S Enterprise'),
(359, 20, 'Ganeshaas'),
(360, 20, 'Godrej'),
(361, 20, 'Handy'),
(362, 20, 'Hettich'),
(363, 20, 'Hi Life'),
(364, 20, 'Indian Shelf'),
(365, 20, 'Indune Lifestyle'),
(366, 20, 'Joy'),
(367, 20, 'JVG'),
(368, 20, 'Kawachi'),
(369, 20, 'KCI'),
(370, 20, 'Kea'),
(371, 20, 'Klassik'),
(372, 20, 'Klaxon'),
(373, 20, 'KRM'),
(374, 20, 'Krystal'),
(375, 20, 'Kywin'),
(376, 20, 'Marvel'),
(377, 20, 'Naman'),
(378, 20, 'Olive'),
(379, 20, 'Orange Tree'),
(380, 20, 'Panorama'),
(381, 20, 'Pebbleyard'),
(382, 20, 'Regis'),
(383, 20, 'Renson'),
(384, 20, 'Ripples'),
(385, 20, 'SDG'),
(386, 20, 'SGB'),
(387, 20, 'Skayline'),
(388, 20, 'Skoot'),
(389, 20, 'Sterling'),
(390, 20, 'Sungold'),
(391, 20, 'Teeta'),
(392, 20, 'Telebuy'),
(393, 20, 'Tirupati'),
(394, 20, 'TLS'),
(395, 21, 'Havells'),
(396, 21, 'Maharaja Whiteline'),
(397, 21, 'Allora'),
(398, 21, 'ACS'),
(399, 21, 'Arise'),
(400, 21, 'Blue Me'),
(401, 21, 'Breezelit'),
(402, 21, 'Brissk'),
(403, 21, 'Champion'),
(404, 21, 'Citron'),
(405, 21, 'Comforts'),
(406, 21, 'Desire'),
(407, 21, 'E-Fab'),
(408, 21, 'Eplar'),
(409, 21, 'Fanzart'),
(410, 21, 'Kailash'),
(411, 21, 'Karishma'),
(412, 21, 'Omega'),
(413, 21, 'Ovastar'),
(414, 21, 'Polar'),
(415, 32, 'Parryware'),
(416, 32, 'Jaguar'),
(417, 32, 'Hindware'),
(418, 32, 'Johnson'),
(419, 32, 'Crea'),
(420, 32, 'Aai'),
(421, 32, 'Alpina'),
(422, 32, 'Alton'),
(423, 32, 'Anox'),
(424, 32, 'Anupam'),
(425, 32, 'Benelave'),
(426, 32, 'Byc'),
(427, 32, 'CBM'),
(428, 32, 'Crayons'),
(429, 32, 'Decowell'),
(430, 32, 'Delta'),
(431, 32, 'Dolin'),
(432, 32, 'Dolphy'),
(433, 32, 'DRC'),
(434, 32, 'Ess Ess'),
(435, 32, 'Goeka'),
(436, 32, 'Goonj'),
(437, 32, 'Graffiti'),
(438, 32, 'Greggs'),
(439, 32, 'Haneez'),
(440, 32, 'Jaaz'),
(441, 32, 'Kamal'),
(442, 32, 'Kawachi'),
(443, 32, 'Macrowave'),
(444, 32, 'Moen'),
(445, 32, 'Ozone'),
(446, 32, 'Parkovic'),
(447, 32, 'Royal'),
(448, 32, 'Sanimart'),
(449, 32, 'Simoll'),
(450, 32, 'Supreme'),
(451, 32, 'Swift'),
(452, 32, 'Tatay'),
(453, 32, 'TMC'),
(454, 32, 'Truphe'),
(455, 32, 'Turnip'),
(456, 32, 'Zahab'),
(457, 33, 'Parryware'),
(458, 33, 'Hindware'),
(459, 33, 'Johnson'),
(460, 33, 'Crea'),
(461, 33, 'Graffiti'),
(462, 33, 'Auriga'),
(463, 33, 'Aai'),
(464, 33, 'Agas'),
(465, 33, 'Alton'),
(466, 33, 'Aquarium'),
(467, 33, 'Aviva'),
(468, 33, 'Bharat'),
(469, 33, 'Blue Sea'),
(470, 33, 'Cavo'),
(471, 33, 'CBM'),
(472, 33, 'Celebrity'),
(473, 33, 'Chrome'),
(474, 33, 'Classic'),
(475, 33, 'Crayons'),
(476, 33, 'Dazzle'),
(477, 33, 'Decowell'),
(478, 33, 'Dee Max'),
(479, 33, 'Delta'),
(480, 33, 'Dhawan'),
(481, 33, 'Diore'),
(482, 33, 'Dloop'),
(483, 33, 'Dolin'),
(484, 33, 'Dolphy'),
(485, 33, 'Eon'),
(486, 33, 'Ess Ess'),
(487, 33, 'Garvila'),
(488, 33, 'Glitz'),
(489, 33, 'Graffiti'),
(490, 33, 'Greggs'),
(491, 33, 'Handy'),
(492, 33, 'Haneez'),
(493, 33, 'Hira'),
(494, 33, 'Jolly\'s'),
(495, 33, 'Joy'),
(496, 33, 'JP Hardware'),
(497, 33, 'JVG'),
(498, 33, 'Kamal'),
(499, 33, 'Kawachi'),
(500, 33, 'KCI'),
(501, 33, 'Kitsch'),
(502, 33, 'Klassik'),
(503, 33, 'KRM'),
(504, 33, 'Lacuzini'),
(505, 33, 'Legris'),
(506, 33, 'Magnifico'),
(507, 33, 'Mahavira'),
(508, 33, 'Macrowave'),
(509, 33, 'Marine'),
(510, 33, 'Maxo'),
(511, 33, 'Pebbleyard'),
(512, 33, 'Penguin'),
(513, 33, 'Platina'),
(514, 33, 'Regis'),
(515, 33, 'Renson'),
(516, 33, 'Ripples'),
(517, 33, 'Royal'),
(518, 33, 'Sanimart'),
(519, 33, 'Sanitario'),
(520, 33, 'Sedam'),
(521, 33, 'SGB'),
(522, 33, 'Simoll'),
(523, 33, 'Koot'),
(524, 33, 'Sparrow'),
(525, 33, 'Springs'),
(526, 33, 'SSI'),
(527, 33, 'Sterling'),
(528, 33, 'Stonekraft'),
(529, 33, 'Sungold'),
(530, 33, 'Sunrise'),
(531, 33, 'Supreme'),
(532, 33, 'Swift'),
(533, 33, 'Tatay'),
(534, 33, 'Teeta'),
(535, 33, 'Tibros'),
(536, 33, 'Tricon'),
(537, 33, 'Truphe'),
(538, 33, 'Turnip'),
(539, 33, 'Zahab'),
(540, 33, 'Zoiks'),
(541, 33, 'Zotalic'),
(542, 34, 'Blue Sea'),
(543, 34, 'CBM'),
(544, 34, 'Chrome'),
(545, 34, 'Dazzle'),
(546, 34, 'Decowell'),
(547, 34, 'Dhawan'),
(548, 34, 'Elvy'),
(549, 34, 'Eon'),
(550, 34, 'Glitz'),
(551, 34, 'Handy'),
(552, 34, 'Jaaz'),
(553, 34, 'KRM'),
(554, 34, 'Krystal'),
(555, 34, 'Legris'),
(556, 34, 'Palakz'),
(557, 34, 'Parryware'),
(558, 34, 'Pindia'),
(559, 34, 'Palaroll'),
(560, 34, 'Ruby'),
(561, 34, 'Sipco'),
(562, 34, 'Skayline'),
(563, 34, 'Turnip'),
(564, 34, 'Uteki'),
(565, 34, 'Vedanta'),
(566, 34, 'Veu'),
(567, 34, 'Wenko'),
(568, 35, 'Parryware'),
(569, 35, 'Jaguar'),
(570, 35, 'Hindware'),
(571, 35, 'Johnson'),
(572, 35, 'Crea'),
(573, 35, 'Graffiti'),
(574, 35, 'Sheetal'),
(575, 35, 'Auriga'),
(576, 35, 'Aai'),
(577, 35, 'Accedre'),
(578, 35, 'Alton'),
(579, 35, 'Anox'),
(580, 35, 'Anupam'),
(581, 35, 'Aqua Ocean'),
(582, 35, 'Aquadyne'),
(583, 35, 'Aquarium'),
(584, 35, 'Aquieen'),
(585, 35, 'Aris'),
(586, 35, 'Arkay'),
(587, 35, 'Aviva'),
(588, 35, 'Bharat'),
(589, 35, 'Blue Sea'),
(590, 35, 'Casa Decor'),
(591, 35, 'Cavo'),
(592, 35, 'CBM'),
(593, 35, 'Celebrity'),
(594, 35, 'Chilly'),
(595, 35, 'Chrome'),
(596, 35, 'Classic'),
(597, 35, 'Cosrich'),
(598, 35, 'Crayons'),
(599, 35, 'Dazzle'),
(600, 35, 'Decowell'),
(601, 35, 'Dee Max'),
(602, 35, 'Delta'),
(603, 35, 'Designo'),
(604, 35, 'Dhawan'),
(605, 35, 'Diore'),
(606, 35, 'Disha'),
(607, 35, 'Dizayn'),
(608, 35, 'Dolin'),
(609, 35, 'Dolphy'),
(610, 35, 'Dooa'),
(611, 35, 'DRC'),
(612, 35, 'Easyflow'),
(613, 35, 'Echo'),
(614, 35, 'Efficia'),
(615, 35, 'Elegance'),
(616, 35, 'Elvy'),
(617, 35, 'Eon'),
(618, 35, 'Ess Ess'),
(619, 35, 'Fancy Glass'),
(620, 35, 'Forlive'),
(621, 35, 'Franko'),
(622, 35, 'Fuao'),
(623, 35, 'Fusion'),
(624, 35, 'Futura'),
(625, 35, 'Garvila'),
(626, 35, 'Glitz'),
(627, 35, 'Globex'),
(628, 35, 'Godrej'),
(629, 35, 'Goldmind'),
(630, 35, 'Graffiti'),
(631, 35, 'Greggs'),
(632, 35, 'Gypsy'),
(633, 35, 'Handy'),
(634, 35, 'Haneez'),
(635, 35, 'Henzer'),
(636, 35, 'Hettich'),
(637, 35, 'Jaaz'),
(638, 35, 'JBS'),
(639, 35, 'Jolly\'s'),
(640, 35, 'Joy'),
(641, 35, 'JVG'),
(642, 35, 'Kamal'),
(643, 35, 'Kataria'),
(644, 35, 'Kawachi'),
(645, 35, 'KBG'),
(646, 35, 'KCI'),
(647, 35, 'Kea'),
(648, 35, 'KRM'),
(649, 35, 'Legris'),
(650, 35, 'LMC'),
(651, 35, 'Macrowave'),
(652, 35, 'MSI'),
(653, 35, 'Muren'),
(654, 35, 'Naman'),
(655, 35, 'Ocean'),
(656, 35, 'Palakz'),
(657, 35, 'Palam'),
(658, 35, 'Parkovic'),
(659, 35, 'Pebbleyard'),
(660, 35, 'Penguin'),
(661, 35, 'Phantom'),
(662, 35, 'Pindia'),
(663, 35, 'Platina'),
(664, 35, 'Palaroll'),
(665, 35, 'Renson'),
(666, 35, 'Ripples'),
(667, 35, 'Royal'),
(668, 35, 'Ruby'),
(669, 35, 'Sanitario'),
(670, 35, 'Sanvi'),
(671, 35, 'SEK'),
(672, 35, 'Shruti'),
(673, 35, 'Simoll'),
(674, 35, 'Sipco'),
(675, 35, 'Skayline'),
(676, 35, 'Koot'),
(677, 35, 'Sterling'),
(678, 35, 'Taptree'),
(679, 35, 'Teeta'),
(680, 35, 'Telebuy'),
(681, 35, 'Tibros'),
(682, 35, 'TLS'),
(683, 35, 'Travel Blue'),
(684, 35, 'Tricon'),
(685, 35, 'Trot'),
(686, 35, 'Truphe'),
(687, 35, 'UBL'),
(688, 35, 'Umbra'),
(689, 35, 'Velcro'),
(690, 35, 'Veu'),
(691, 35, 'Visko'),
(692, 35, 'Vola'),
(693, 35, 'Water Tec'),
(694, 35, 'Zahab'),
(695, 35, 'Zibo'),
(696, 35, 'Zoiks'),
(697, 35, 'Zotalic'),
(698, 36, 'Aai'),
(699, 36, 'Accedre'),
(700, 36, 'Aquarium'),
(701, 36, 'Aviva'),
(702, 36, 'Classic'),
(703, 36, 'Dazzle'),
(704, 36, 'Dolphy'),
(705, 36, 'Eon'),
(706, 36, 'Haneez'),
(707, 36, 'Jaaz'),
(708, 36, 'JBS'),
(709, 36, 'KRM'),
(710, 36, 'Marvel'),
(711, 36, 'Ocean'),
(712, 36, 'Regis'),
(713, 36, 'Royal'),
(714, 36, 'Sanimart'),
(715, 36, 'Sipco'),
(716, 36, 'Skayline'),
(717, 36, 'Sterling'),
(718, 36, 'Sungold'),
(719, 36, 'Sunrise'),
(720, 36, 'Taptree'),
(721, 36, 'Viking'),
(722, 36, 'Zahab'),
(723, 37, 'Aai'),
(724, 37, 'Agas'),
(725, 37, 'Blue Sea'),
(726, 37, 'CBM'),
(727, 37, 'Dazzle'),
(728, 37, 'Decowell'),
(729, 37, 'Fusion'),
(730, 37, 'Glitz'),
(731, 37, 'JVG'),
(732, 37, 'KRM'),
(733, 37, 'Magnifico'),
(734, 37, 'Marvel'),
(735, 37, 'Mog'),
(736, 37, 'Ocean'),
(737, 37, 'Palaroll'),
(738, 37, 'Regis'),
(739, 37, 'Sanimart'),
(740, 37, 'Sipco'),
(741, 37, 'Skayline'),
(742, 37, 'Sterling'),
(743, 37, 'Vedanta'),
(744, 37, 'Viking'),
(745, 38, 'Parryware'),
(746, 38, 'Jaguar'),
(747, 38, 'Hindware'),
(748, 38, 'Johnson'),
(749, 38, 'Crea'),
(750, 38, 'Graffiti'),
(751, 38, 'Auriga'),
(752, 38, 'Dolphy'),
(753, 38, 'Godrej'),
(754, 39, 'Parryware'),
(755, 39, 'Jaguar'),
(756, 39, 'Hindware'),
(757, 39, 'Johnson'),
(758, 39, 'Crea'),
(759, 39, 'Graffiti'),
(760, 39, 'Aai'),
(761, 39, 'Accedre'),
(762, 39, 'Agas'),
(763, 39, 'Aviva'),
(764, 39, 'Blue Sea'),
(765, 39, 'Cavo'),
(766, 39, 'CBM'),
(767, 39, 'Chrome'),
(768, 39, 'Classic'),
(769, 39, 'Crayons'),
(770, 39, 'Dazzle'),
(771, 39, 'Decowell'),
(772, 39, 'Dee Max'),
(773, 39, 'Delight'),
(774, 39, 'Delta'),
(775, 39, 'Dhawan'),
(776, 39, 'Disha'),
(777, 39, 'Dloop'),
(778, 39, 'Dolin'),
(779, 39, 'Dolphy'),
(780, 39, 'Eon'),
(781, 39, 'Eselife'),
(782, 39, 'Euro Glass'),
(783, 39, 'Fancy Glass'),
(784, 39, 'Fancy Glass Frosted'),
(785, 39, 'Fancy Glass Shelf'),
(786, 39, 'JBS'),
(787, 39, 'Klassik'),
(788, 39, 'Mayur'),
(789, 39, 'Mog'),
(790, 39, 'Peacock Life'),
(791, 39, 'Regis'),
(792, 39, 'Royal'),
(793, 39, 'Sanitario'),
(794, 39, 'Shriji'),
(795, 39, 'Shruti'),
(796, 39, 'Sterling'),
(797, 39, 'Sunrise'),
(798, 39, 'Swift'),
(799, 39, 'Turnip'),
(800, 39, 'Vedanta'),
(801, 39, 'Viking'),
(802, 39, 'Zahab'),
(803, 39, 'Zotalic'),
(804, 40, 'Parryware'),
(805, 40, 'Jaguar'),
(806, 40, 'Hindware'),
(807, 40, 'Johnson'),
(808, 40, 'Crea'),
(809, 40, 'Graffiti'),
(810, 40, 'Aai'),
(811, 40, 'Accedre'),
(812, 40, 'Adson'),
(813, 40, 'Agas'),
(814, 40, 'Aqua Ocean'),
(815, 40, 'Aquarium'),
(816, 40, 'Chrome'),
(817, 40, 'Classic'),
(818, 40, 'Decowell'),
(819, 40, 'Delta'),
(820, 40, 'Designo'),
(821, 40, 'Dhawan'),
(822, 40, 'Dizayn'),
(823, 40, 'Dolphy'),
(824, 40, 'Dorma'),
(825, 40, 'Handy'),
(826, 40, 'Haneez'),
(827, 40, 'Jolly\'s'),
(828, 40, 'Joy'),
(829, 40, 'JVG'),
(830, 40, 'Kamal'),
(831, 40, 'Kawachi'),
(832, 40, 'KBG'),
(833, 40, 'Peacock Life'),
(834, 40, 'Pebbleyard'),
(835, 40, 'Penguin'),
(836, 40, 'Phantom'),
(837, 40, 'Pindia'),
(838, 40, 'Plato'),
(839, 40, 'Palaroll'),
(840, 40, 'Regis'),
(841, 40, 'Sterling'),
(842, 40, 'Sungold'),
(843, 40, 'Sunrise'),
(844, 40, 'Super IT'),
(845, 40, 'Supreme'),
(846, 40, 'Swift'),
(847, 40, 'Trot'),
(848, 40, 'Victor'),
(849, 40, 'Viking'),
(850, 40, 'Visko'),
(851, 40, 'Wenko'),
(852, 40, 'White Gold'),
(853, 40, 'Zahab'),
(854, 40, 'Zoiks'),
(855, 40, 'Zotalic'),
(874, 42, 'Asian Paints'),
(875, 42, 'Royal'),
(876, 42, 'Valspar'),
(877, 42, 'Jotun'),
(878, 42, 'Berger'),
(879, 42, 'Asian'),
(880, 42, 'Indianpaints'),
(881, 42, 'Regal'),
(882, 42, 'Utsarg'),
(883, 42, 'Dulux'),
(884, 42, 'Gobal'),
(885, 42, 'Lifestyle'),
(886, 42, 'Alba'),
(887, 42, 'Ace'),
(888, 42, 'Nerolac'),
(889, 42, 'Agsar'),
(890, 42, 'Sheenlac'),
(891, 43, 'Bruce'),
(892, 43, 'Carlisle'),
(893, 43, 'Hearne hardwoods'),
(894, 44, 'Hearne hardwoods'),
(895, 45, 'Carlisle'),
(896, 46, 'Asian Paints'),
(897, 46, 'Royal'),
(898, 46, 'Valspar'),
(899, 46, 'Jotun'),
(900, 46, 'Berger'),
(901, 46, 'Asian'),
(902, 46, 'Indianpaints'),
(903, 46, 'Regal'),
(904, 46, 'Utsarg'),
(905, 46, 'Dulux'),
(906, 46, 'Gobal'),
(907, 46, 'Lifestyle'),
(908, 46, 'Alba'),
(909, 46, 'Ace'),
(910, 46, 'Nerolac'),
(911, 46, 'Agsar'),
(912, 46, 'Sheenlac'),
(913, 46, 'Royal'),
(914, 46, 'Valspar'),
(915, 46, 'Jotun'),
(916, 46, 'Berger'),
(917, 47, 'Asian Paints'),
(918, 47, 'Asian'),
(919, 47, 'Indianpaints'),
(920, 47, 'Regal'),
(921, 47, 'Utsarg'),
(922, 47, 'Dulux'),
(923, 47, 'Gobal'),
(924, 47, 'Lifestyle'),
(925, 47, 'Alba'),
(926, 47, 'Ace'),
(927, 47, 'Nerolac'),
(928, 47, 'Agsar'),
(929, 47, 'Sheenlac'),
(930, 47, 'Royal'),
(931, 47, 'Valspar'),
(932, 47, 'Jotun'),
(933, 47, 'Berger'),
(934, 48, 'Royal'),
(935, 48, 'Valspar'),
(936, 48, 'Jotun'),
(937, 48, 'Berger'),
(938, 48, 'Asian'),
(939, 48, 'Indianpaints'),
(940, 48, 'Regal'),
(941, 48, 'Utsarg'),
(942, 48, 'Dulux'),
(943, 48, 'Gobal'),
(944, 48, 'Lifestyle'),
(945, 48, 'Alba'),
(946, 48, 'Ace'),
(947, 48, 'Nerolac'),
(948, 48, 'Agsar'),
(949, 48, 'Sheenlac'),
(950, 49, 'Asian Paints'),
(951, 49, 'Royal'),
(952, 49, 'Valspar'),
(953, 49, 'Jotun'),
(954, 49, 'Berger'),
(955, 49, 'Asian'),
(956, 49, 'Indianpaints'),
(957, 49, 'Regal'),
(958, 49, 'Utsarg'),
(959, 49, 'Dulux'),
(960, 49, 'Gobal'),
(961, 49, 'Lifestyle'),
(962, 49, 'Alba'),
(963, 49, 'Ace'),
(964, 49, 'Nerolac'),
(965, 49, 'Agsar'),
(966, 49, 'Sheenlac'),
(967, 50, 'Asian Paints'),
(968, 50, 'Royal'),
(969, 50, 'Valspar'),
(970, 50, 'Jotun'),
(971, 50, 'Berger'),
(972, 50, 'Asian'),
(973, 50, 'Indianpaints'),
(974, 50, 'Regal'),
(975, 50, 'Utsarg'),
(976, 50, 'Dulux'),
(977, 50, 'Gobal'),
(978, 50, 'Lifestyle'),
(979, 50, 'Alba'),
(980, 50, 'Ace'),
(981, 50, 'Nerolac'),
(982, 50, 'Agsar'),
(983, 50, 'Sheenlac'),
(984, 51, 'Asian Paints'),
(985, 51, 'Royal'),
(986, 51, 'Valspar'),
(987, 51, 'Jotun'),
(988, 51, 'Berger'),
(989, 51, 'Asian'),
(990, 51, 'Indianpaints'),
(991, 51, 'Regal'),
(992, 51, 'Utsarg'),
(993, 51, 'Dulux'),
(994, 51, 'Gobal'),
(995, 51, 'Lifestyle'),
(996, 51, 'Alba'),
(997, 51, 'Ace'),
(998, 51, 'Nerolac'),
(999, 51, 'Agsar'),
(1000, 51, 'Sheenlac'),
(1001, 52, 'Aarsun woods'),
(1002, 52, 'Ace'),
(1003, 52, 'Adara'),
(1004, 52, 'Agromech'),
(1005, 52, 'Aristio'),
(1006, 52, 'Arsalan'),
(1007, 52, 'Artesia'),
(1008, 52, 'Atam'),
(1009, 52, 'Craft art India'),
(1010, 52, 'Dolphy'),
(1011, 52, 'Zuniq'),
(1012, 52, 'Zoom Ktichenware'),
(1013, 52, 'Woodsmith'),
(1014, 52, 'Vincy'),
(1015, 52, 'TLS'),
(1016, 52, 'Sterling'),
(1017, 52, 'SK Metal'),
(1018, 52, 'Tibros'),
(1019, 52, 'Sygo'),
(1020, 52, 'Sens'),
(1021, 52, 'Nitraa'),
(1022, 52, 'Nilkamal'),
(1023, 52, 'Mavi'),
(1024, 52, 'Plenzo'),
(1025, 52, 'Anmol'),
(1026, 52, 'Blomus'),
(1027, 52, 'Chrome'),
(1028, 52, 'Dragon'),
(1029, 52, 'Ezzi Deals'),
(1030, 52, 'Glitter'),
(1031, 52, 'Gran'),
(1032, 52, 'JVG'),
(1033, 52, 'Kawachi'),
(1034, 52, 'Kmwanmol'),
(1035, 52, 'Lovato'),
(1036, 52, 'Maxxlite'),
(1037, 52, 'Mog'),
(1038, 52, 'Pindia'),
(1039, 52, 'Vasnam'),
(1040, 52, 'Vladiva'),
(1041, 52, 'Belmun'),
(1042, 52, 'Hubberholme'),
(1043, 52, 'M&G'),
(1044, 52, 'Marwar'),
(1045, 52, 'Taaza Garam'),
(1046, 52, 'Yingha'),
(1047, 52, 'Oceanstar'),
(1048, 52, 'Spectrum'),
(1049, 52, 'Zenith'),
(1050, 52, 'Elan'),
(1051, 52, 'Bergner'),
(1052, 52, 'BTL'),
(1053, 52, 'CBM'),
(1054, 52, 'Chef\'n'),
(1055, 52, 'Clytinus'),
(1056, 52, 'Cocina'),
(1057, 52, 'Dazzle'),
(1058, 52, 'Disha'),
(1059, 52, 'Dolin'),
(1060, 52, 'Doyours'),
(1061, 52, 'Emerald'),
(1062, 52, 'Emsa'),
(1063, 52, 'Eoan International'),
(1064, 52, 'Erbanize'),
(1065, 52, 'Hanbao'),
(1066, 52, 'JB'),
(1067, 52, 'Hokipo'),
(1068, 52, 'Evok'),
(1069, 52, 'Maharaja'),
(1070, 52, 'Saffron'),
(1071, 52, 'RBJ'),
(1072, 52, 'Picnic'),
(1073, 52, 'SGB'),
(1074, 52, 'Skys&Ray'),
(1075, 52, 'Slivar Bell'),
(1076, 52, 'Steels'),
(1077, 52, 'Swastik'),
(1078, 52, 'Tesco'),
(1079, 52, 'Tinny'),
(1080, 52, 'TLS'),
(1081, 52, 'Umbra'),
(1082, 52, 'VCK'),
(1083, 52, 'Zecado'),
(1084, 52, 'Piyestra'),
(1085, 52, 'Zuari'),
(1086, 52, 'StyleSpa'),
(1087, 52, 'Nesta Furniture'),
(1088, 52, 'Durian'),
(1089, 52, 'ISC'),
(1090, 52, 'Jangir'),
(1091, 52, 'D K arts'),
(1092, 52, 'Cubit Homes'),
(1093, 52, 'Eros'),
(1094, 52, 'Furnitech'),
(1095, 52, 'Loxia'),
(1096, 52, 'Mubeel'),
(1097, 52, 'Orange Tree'),
(1098, 52, 'SS Modulars'),
(1099, 52, 'MCT'),
(1100, 52, 'Crystal'),
(1101, 52, 'Pigeon'),
(1102, 52, 'Shekawati'),
(1103, 52, 'Frunstyl'),
(1104, 52, 'Kingscrafts'),
(1105, 52, 'Purpledip'),
(1106, 52, 'Cotton Swings'),
(1107, 52, 'D-Swing'),
(1108, 52, 'Slack Jack'),
(1109, 52, 'Younique'),
(1110, 52, 'Spring Fit'),
(1111, 52, 'Decorhand'),
(1112, 52, 'ACW'),
(1113, 52, 'Swayam'),
(1114, 52, 'Dekor World'),
(1115, 52, 'Azaani'),
(1116, 52, 'Bansal Yarn'),
(1117, 52, 'Bianca'),
(1118, 52, 'Drape'),
(1119, 52, 'Eyda'),
(1120, 52, 'FabBig'),
(1121, 52, 'K-Star'),
(1122, 52, 'Kabir'),
(1123, 52, 'Zyne'),
(1124, 52, 'Zikrak Exim'),
(1125, 52, 'Vahra'),
(1126, 52, 'Kwality'),
(1127, 52, 'Mitra'),
(1128, 52, 'Novelty'),
(1129, 52, 'Presto'),
(1130, 53, 'Nilkamal'),
(1131, 53, 'Anmol'),
(1132, 53, 'Blomus'),
(1133, 53, 'Chrome'),
(1134, 53, 'Dragon'),
(1135, 53, 'Ezzi Deals'),
(1136, 53, 'Glitter'),
(1137, 53, 'Gran'),
(1138, 53, 'JVG'),
(1139, 53, 'Kawachi'),
(1140, 53, 'Kmwanmol'),
(1141, 53, 'Lovato'),
(1142, 53, 'Maxxlite'),
(1143, 53, 'Mog'),
(1144, 53, 'Pindia'),
(1145, 53, 'Vasnam'),
(1146, 53, 'Vladiva'),
(1147, 53, 'Belmun'),
(1148, 54, 'Nilkamal'),
(1149, 54, 'Anmol'),
(1150, 54, 'Dragon'),
(1151, 54, 'Ezzi Deals'),
(1152, 54, 'Glitter'),
(1153, 54, 'Gran'),
(1154, 54, 'JVG'),
(1155, 54, 'Mog'),
(1156, 54, 'Pindia'),
(1157, 54, 'Vasnam'),
(1158, 54, 'Vladiva'),
(1159, 54, 'Belmun'),
(1160, 54, 'Picnic'),
(1161, 54, 'Kwality'),
(1162, 54, 'Novelty'),
(1163, 55, 'Aarsun woods'),
(1164, 55, 'Ace'),
(1165, 55, 'Adara'),
(1166, 55, 'Aristio'),
(1167, 55, 'Arsalan'),
(1168, 55, 'Artesia'),
(1169, 55, 'Craft art India'),
(1170, 55, 'Dolphy'),
(1171, 55, 'Zuniq'),
(1172, 55, 'Zoom Ktichenware'),
(1173, 55, 'Woodsmith'),
(1174, 55, 'Vincy'),
(1175, 55, 'TLS'),
(1176, 55, 'Sterling'),
(1177, 55, 'SK Metal'),
(1178, 55, 'Tibros'),
(1179, 55, 'Sygo'),
(1180, 55, 'Nitraa'),
(1181, 55, 'Nilkamal'),
(1182, 55, 'Mavi'),
(1183, 55, 'Plenzo'),
(1184, 55, 'Anmol'),
(1185, 55, 'Blomus'),
(1186, 55, 'Chrome'),
(1187, 55, 'Ezzi Deals'),
(1188, 55, 'Glitter'),
(1189, 55, 'Gran'),
(1190, 55, 'JVG'),
(1191, 55, 'Kawachi'),
(1192, 55, 'Kmwanmol'),
(1193, 55, 'Mog'),
(1194, 55, 'Pindia'),
(1195, 55, 'Vasnam'),
(1196, 55, 'Vladiva'),
(1197, 55, 'Belmun'),
(1198, 55, 'Hubberholme'),
(1199, 55, 'M&G'),
(1200, 55, 'Taaza Garam'),
(1201, 55, 'Yingha'),
(1202, 55, 'Oceanstar'),
(1203, 55, 'Spectrum'),
(1204, 55, 'Zenith'),
(1205, 55, 'Elan'),
(1206, 55, 'Bergner'),
(1207, 55, 'BTL'),
(1208, 55, 'CBM'),
(1209, 55, 'Chef\'n'),
(1210, 55, 'Clytinus'),
(1211, 55, 'Cocina'),
(1212, 55, 'Dazzle'),
(1213, 55, 'Disha'),
(1214, 55, 'Dolin'),
(1215, 55, 'Doyours'),
(1216, 55, 'Emerald'),
(1217, 55, 'Emsa'),
(1218, 55, 'Eoan International'),
(1219, 55, 'Erbanize'),
(1220, 55, 'Hanbao'),
(1221, 55, 'JB'),
(1222, 55, 'Hokipo'),
(1223, 55, 'Royal Oak'),
(1224, 55, 'Jivan'),
(1225, 55, 'Evok'),
(1226, 55, 'Maharaja'),
(1227, 55, 'Saffron'),
(1228, 55, 'RBJ'),
(1229, 55, 'Picnic'),
(1230, 55, 'SGB'),
(1231, 55, 'Skys&Ray'),
(1232, 55, 'Steels'),
(1233, 55, 'Swastik'),
(1234, 55, 'Tesco'),
(1235, 55, 'Tinny'),
(1236, 55, 'TLS'),
(1237, 55, 'Umbra'),
(1238, 55, 'VCK'),
(1239, 55, 'Zecado'),
(1240, 55, 'Piyestra'),
(1241, 55, 'Zuari'),
(1242, 55, 'StyleSpa'),
(1243, 55, 'Nesta Furniture'),
(1244, 55, 'Durian'),
(1245, 55, 'ISC'),
(1246, 55, 'Jangir'),
(1247, 55, 'D K arts'),
(1248, 55, 'Cubit Homes'),
(1249, 55, 'Eros'),
(1250, 55, 'Furnitech'),
(1251, 55, 'Loxia'),
(1252, 55, 'Mubeel'),
(1253, 55, 'Orange Tree'),
(1254, 55, 'SS Modulars'),
(1255, 55, 'MCT'),
(1256, 55, 'Crystal'),
(1257, 55, 'Pigeon'),
(1258, 55, 'Shekawati'),
(1259, 55, 'Frunstyl'),
(1260, 55, 'Kingscrafts'),
(1261, 55, 'Purpledip'),
(1262, 55, 'Cotton Swings'),
(1263, 55, 'D-Swing'),
(1264, 55, 'Slack Jack'),
(1265, 55, 'Younique'),
(1266, 55, 'Spring Fit'),
(1267, 55, 'Decorhand'),
(1268, 55, 'ACW'),
(1269, 55, 'Swayam'),
(1270, 55, 'Dekor World'),
(1271, 55, 'Azaani'),
(1272, 55, 'Bansal Yarn'),
(1273, 55, 'Bianca'),
(1274, 55, 'Drape'),
(1275, 55, 'Eyda'),
(1276, 55, 'FabBig'),
(1277, 55, 'Desire'),
(1278, 55, 'K-Star'),
(1279, 55, 'Kabir'),
(1280, 55, 'Zyne'),
(1281, 55, 'Zikrak Exim'),
(1282, 55, 'Vahra'),
(1283, 55, 'Kwality'),
(1284, 55, 'Mitra'),
(1285, 55, 'Novelty'),
(1286, 55, 'Presto'),
(1287, 56, 'Nilkamal'),
(1288, 56, 'Royal Oak'),
(1289, 56, 'Durian');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_category`
--

CREATE TABLE `inventory_category` (
  `cat` varchar(200) NOT NULL COMMENT 'Inventory Category',
  `description` text COMMENT 'Description',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_category`
--

INSERT INTO `inventory_category` (`cat`, `description`, `is_active`) VALUES
('Electronics & Electrical', 'Electronics & Electrical', 1),
('Flooring', 'Flooring', 1),
('Furnishing', 'Furnishing', 1),
('Painting', 'Painting', 1),
('Plumbing', 'Plumbing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `ID` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `email` varchar(255) NOT NULL COMMENT 'Email ID',
  `password` varchar(50) NOT NULL COMMENT 'Password',
  `user_type` varchar(100) NOT NULL COMMENT 'User Type',
  `created_date` datetime NOT NULL COMMENT 'Created Date',
  `last_active_date` datetime DEFAULT NULL COMMENT 'Last Active Date',
  `is_active` int(11) NOT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1438066453),
('m140209_132017_init', 1438066464),
('m140403_174025_create_account_table', 1438066464),
('m140504_113157_update_tables', 1438066466),
('m140504_130429_create_token_table', 1438066467),
('m140830_171933_fix_ip_field', 1438066467),
('m140830_172703_change_account_table_name', 1438066467),
('m141222_110026_update_ip_field', 1438066467);

-- --------------------------------------------------------

--
-- Table structure for table `office_branches`
--

CREATE TABLE `office_branches` (
  `branch` varchar(100) NOT NULL COMMENT 'Branch Name',
  `city` varchar(100) NOT NULL COMMENT 'Branch City',
  `adrs` text COMMENT 'Address',
  `contact_no` varchar(20) DEFAULT NULL COMMENT 'Contact No.',
  `state` varchar(100) DEFAULT NULL COMMENT 'State'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office_branches`
--

INSERT INTO `office_branches` (`branch`, `city`, `adrs`, `contact_no`, `state`) VALUES
('BEL', 'Mysuru', NULL, NULL, NULL),
('Peenya', 'Bangalore', NULL, NULL, NULL),
('SanjayNagar', 'Bangalore', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Is Active??'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `title` int(10) UNSIGNED NOT NULL COMMENT 'Title',
  `fname` varchar(100) NOT NULL COMMENT 'First Name',
  `lname` varchar(100) NOT NULL COMMENT 'Last Name',
  `email` varchar(255) NOT NULL COMMENT 'Email ID',
  `relation` varchar(100) DEFAULT NULL COMMENT 'Relation',
  `relation_person` varchar(200) DEFAULT NULL COMMENT 'Relation Person',
  `dob` date NOT NULL COMMENT 'Date of Birth',
  `marital_status` varchar(20) NOT NULL COMMENT 'Marital Status',
  `spouse` varchar(200) DEFAULT NULL COMMENT 'Spouse Name',
  `wed_ann` date DEFAULT NULL COMMENT 'Wedding Anniversary',
  `address` bigint(20) UNSIGNED NOT NULL COMMENT 'Address',
  `off_address` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Office Address',
  `company` varchar(200) DEFAULT NULL COMMENT 'Company Name',
  `designation` varchar(100) DEFAULT NULL COMMENT 'Designation',
  `gender` varchar(10) NOT NULL,
  `profile_image` varchar(200) DEFAULT NULL COMMENT 'Profile Image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `title`, `fname`, `lname`, `email`, `relation`, `relation_person`, `dob`, `marital_status`, `spouse`, `wed_ann`, `address`, `off_address`, `company`, `designation`, `gender`, `profile_image`) VALUES
(1, 1, 'Selva ', 'Kumar', 'Selva@yahoomail.com', 'Brother', NULL, '1979-06-16', 'Single', '', '1970-01-01', 419, 420, 'Compuage compters ltd', 'Sales Executive', 'Male', NULL),
(2, 1, 'Kiran', 'Kumar', 'Kiran18@gmail.com', 'Brother', NULL, '1990-08-18', 'Single', '', '1970-01-01', 429, 430, 'AGES', 'Team Leader', 'Male', NULL),
(3, 2, 'Tanu', 'Shree', 'tanu344munesh@gmial.com', 'Sister', NULL, '1996-10-22', 'Single', '', '1970-01-01', 440, 441, 'Infosys Infotech Pvt Ltd ', 'Data entry operator', 'Female', NULL),
(4, 1, 'Narayan', 'Swami', 'Narayan-15@gmail.com', 'Brother', NULL, '1971-06-14', 'Married', 'Parmila', '1999-10-05', 449, 450, 'ICICI Bank Limted', 'Accounts', 'Male', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_title`
--

CREATE TABLE `person_title` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_title`
--

INSERT INTO `person_title` (`id`, `name`, `is_active`) VALUES
(1, 'Mr', 1),
(2, 'Miss', 1),
(3, 'Mrs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
(101, NULL, NULL, 'Rahul@email.com', '2d76f6ff2d7aa938edb3dc5f79d19e9d', NULL, NULL, NULL),
(103, NULL, NULL, 'Vinayak.g@gmail.com', '0ac244fb94681e81e7d531fa6d46571b', NULL, NULL, NULL),
(104, NULL, NULL, 'Kiran0604@gmail.com', '90f1a8686c46a3adcc1d820bee90ee19', NULL, NULL, NULL),
(105, NULL, NULL, 'ravi.m@gmail.com', '5efb82a85838ee3464fa4eddd6f7137e', NULL, NULL, NULL),
(106, NULL, NULL, 'narayan.s@gmail.com', 'f348f8ff8accd4919d7304c7889a0183', NULL, NULL, NULL),
(108, NULL, NULL, 'malinidharmaraya@gmail.com', 'cc1769c8e30e8a0df06fcb7155959661', NULL, NULL, NULL),
(109, NULL, NULL, 'vijay@gmail.com', 'bab9c833a990ef25cbe88ea99e6201c4', NULL, NULL, NULL),
(110, NULL, NULL, 'raghu0604@gmail.com', '1ff07ed62f8880c1b1ae3ba83fb03033', NULL, NULL, NULL),
(111, NULL, NULL, 'malini22dharmaraya@gmail.com', '6793af1897819048047d3634a6033b63', NULL, NULL, NULL),
(113, NULL, NULL, 'shashi0605@ymail.com', '4261420fb87fa1e32f475c3fef28dfd1', NULL, NULL, NULL),
(114, NULL, NULL, 'bk.subbaiah52@gmail.com', 'a3b515a2f57f234f65c38f800970412c', NULL, NULL, NULL),
(115, NULL, NULL, 'sdaf@gmaoicom.com', '01bfd06380b77a1958ce22fb315d5ee6', NULL, NULL, NULL),
(116, NULL, NULL, 'binu@gmail.com', '0499d0714b077e5f1198620e56d59e7e', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `prop_type` int(11) DEFAULT NULL COMMENT 'Property Type',
  `no_units` int(11) DEFAULT NULL COMMENT 'No. of Units',
  `no_floors` int(11) DEFAULT NULL COMMENT 'No. of Floors',
  `year_of_construction` int(11) DEFAULT NULL COMMENT 'Year of Construction',
  `address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Address ',
  `tot_units` int(11) DEFAULT NULL COMMENT 'No. of Units',
  `site_contact_email` varchar(255) DEFAULT NULL COMMENT 'On-site Contact Email ID',
  `site_contact_no` varchar(20) DEFAULT NULL COMMENT 'On-site Contact No. ',
  `image` text COMMENT 'Photos',
  `lat` varchar(30) DEFAULT NULL COMMENT 'Latitude',
  `long` varchar(30) DEFAULT NULL COMMENT 'Longitude',
  `incharge_name` varchar(200) DEFAULT NULL COMMENT 'In-charge Name',
  `designation` varchar(100) DEFAULT NULL COMMENT 'In-charge designation',
  `builder_name` varchar(200) DEFAULT NULL COMMENT 'Builder Name',
  `proj_status` varchar(100) DEFAULT NULL COMMENT 'Project Status',
  `tot_proj_area` int(11) DEFAULT NULL COMMENT 'Total Project Area in acres',
  `start_price` decimal(12,2) DEFAULT NULL COMMENT 'Price starting form',
  `area_range` decimal(10,2) DEFAULT NULL COMMENT 'Area range',
  `approvals` text COMMENT 'Approvals',
  `loan` int(11) NOT NULL DEFAULT '1' COMMENT 'Loans',
  `proj_start_date` date DEFAULT NULL COMMENT 'Project Start Date',
  `poss_date` date DEFAULT NULL COMMENT 'Possession handing over date',
  `master_plan` varchar(255) DEFAULT NULL COMMENT 'Master Plan',
  `featured` int(11) NOT NULL DEFAULT '0' COMMENT 'Featured'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `prop_type`, `no_units`, `no_floors`, `year_of_construction`, `address_id`, `tot_units`, `site_contact_email`, `site_contact_no`, `image`, `lat`, `long`, `incharge_name`, `designation`, `builder_name`, `proj_status`, `tot_proj_area`, `start_price`, `area_range`, `approvals`, `loan`, `proj_start_date`, `poss_date`, `master_plan`, `featured`) VALUES
(17, 'test', 1, NULL, 1, NULL, 795, NULL, NULL, NULL, '', '13.036251230364917', '77.5143814086914', NULL, NULL, 'Aakruthi Developers', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(18, 'Brigade Altamont', 1, NULL, 15, NULL, 797, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(19, 'Brigade Caladium', 1, NULL, 12, NULL, 799, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(20, 'Brigade Cosmopolis', 1, NULL, 19, NULL, 801, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(21, 'test proj2', 1, NULL, 4, NULL, 805, NULL, NULL, NULL, '', '13.026384036454747', '77.5140380859375', NULL, NULL, 'Brahma Holdings', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(22, 'test proj3', 1, NULL, 3, NULL, 807, NULL, NULL, NULL, '', '13.028140091255281', '77.50880241394043', NULL, NULL, 'Brahma Holdings', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(23, 'Brigade Exotica', 1, NULL, NULL, NULL, 819, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(24, 'Brigade Golden Triangle', 1, NULL, NULL, NULL, 821, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(25, 'Brigade Northridge', 1, NULL, NULL, NULL, 823, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(26, 'Brigade Omega', 1, NULL, NULL, NULL, 830, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(27, 'Brigade Orchards Luxury Apartments', 1, NULL, NULL, NULL, 832, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(28, 'Brigade Lakefront', 1, NULL, NULL, NULL, 834, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(29, 'Brigade Panorama', 1, NULL, NULL, NULL, 836, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(30, 'Wisteria at Brigade Meadows', 1, NULL, NULL, NULL, 838, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(31, 'Brigade Meadows Value Homes', 1, NULL, NULL, NULL, 842, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(32, 'Brigade Crescent', 1, NULL, NULL, NULL, 844, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(33, 'Brigade Orchards Parkside Retirement Homes', 1, NULL, NULL, NULL, 846, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(34, 'Brigade Orchards Pavilion Villas', 1, NULL, NULL, NULL, 848, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(35, 'Brigade Sonata', 1, NULL, NULL, NULL, 850, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(36, 'Brigade Sparkle', 1, NULL, 4, NULL, 852, NULL, NULL, NULL, '', '12.2976926', '76.60011120000001', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(37, 'Brigade Mountain View', 1, NULL, NULL, NULL, 854, NULL, NULL, NULL, '', '12.3271434', '76.627479', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(38, 'Brigade Palmgrove', 3, NULL, NULL, NULL, 856, NULL, NULL, NULL, '', '12.3271434', '76.627479', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(39, 'Brigade Symphony', 1, NULL, NULL, NULL, 858, NULL, NULL, NULL, '', '12.3271434', '76.627479', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(40, 'Brigade Bhuwalka Icon', 11, NULL, NULL, NULL, 860, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(41, 'BRIGADE IRV CENTRE Whitefield, Bangalore - Ongoing', 11, NULL, NULL, NULL, 862, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(42, 'Brigade IRV Centre', 11, NULL, NULL, NULL, 863, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(43, 'Brigade Magnum', 11, NULL, NULL, NULL, 865, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(44, 'Nalapad Brigade Centre', 11, NULL, 10, NULL, 867, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(45, 'Signature Towers Brigade Golden Triangle', 11, NULL, 22, NULL, 869, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(46, 'Brigade Rubix', 11, NULL, NULL, NULL, 871, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(47, 'World Trade Center', 11, NULL, NULL, NULL, 873, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(48, 'Orion East Mall', 10, NULL, NULL, NULL, 875, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(49, 'The Arcade at Brigade Meadows', 10, NULL, 2, NULL, 877, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(50, 'Nalpad Brigade Broadway', 10, NULL, NULL, NULL, 879, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(51, 'Orion Mall at Brigade Gateway', 10, NULL, NULL, NULL, 881, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(52, 'The Arcade at Brigade Metropolis', 10, NULL, NULL, NULL, 883, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(53, 'Brigade Solitaire', 10, NULL, NULL, NULL, 885, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(54, 'Brigade Point', 10, NULL, NULL, NULL, 887, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(55, 'Sobha Indraprastha', 1, NULL, 36, NULL, 889, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(56, 'Sobha City', 1, NULL, NULL, NULL, 891, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(57, ' Sobha 25 Richmond', 1, NULL, NULL, NULL, 893, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(58, 'Sobha Halcyon', 1, NULL, 21, NULL, 895, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(59, 'Sobha Silicon Oasis', 1, NULL, 20, NULL, 897, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(60, 'test 4', 1, NULL, NULL, NULL, 899, NULL, NULL, NULL, '', '', '', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(61, 'Brigade Opus', 11, NULL, NULL, NULL, 901, NULL, NULL, NULL, '', '12.9651473', '77.57914200000005', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(62, 'Sobha Palladian', 1, NULL, 6, NULL, 903, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(63, 'Sobha Clovelly', 1, NULL, NULL, NULL, 905, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(64, 'The Park & The Plaza', 1, NULL, 18, NULL, 907, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(65, 'Sobha Valley View', 1, NULL, 13, NULL, 909, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(66, 'Sobha Eternia', 1, NULL, 15, NULL, 910, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brigade Enterprises Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(67, 'Sobha Grandeur', 1, NULL, 9, NULL, 911, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Sobha Limited', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(68, 'Brahma villa apartment', 1, NULL, NULL, NULL, 915, NULL, NULL, NULL, '', '13.025556178240322', '77.50760078430176', NULL, NULL, 'Brahma Holdings', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(69, 'Brigade Gateway', 1, NULL, 12, 2008, 916, NULL, NULL, NULL, '', '12.9713063', '77.60699139999997', NULL, NULL, 'Brigade Enterprises Ltd', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(70, 'b1', 2, NULL, NULL, NULL, 918, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Brahma Holdings', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0),
(71, 'hb1', 1, NULL, NULL, NULL, 920, NULL, NULL, NULL, '', '12.9715987', '77.59456269999998', NULL, NULL, 'Hexa Builders', NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01', '1970-01-01', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `properties_pics`
--

CREATE TABLE `properties_pics` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `prop_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property ID',
  `pic` varchar(255) NOT NULL COMMENT 'Picture Path',
  `type` varchar(100) NOT NULL COMMENT 'Type'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `properties_pics`
--

INSERT INTO `properties_pics` (`id`, `prop_id`, `pic`, `type`) VALUES
(86, 25, '03-Home-Page-V2-search-budget-dropdown936350899.jpg', 'Brochure'),
(87, 25, '02-Home-Page-V2-search-prop-type-dropdown542725683.jpg', 'Brochure'),
(88, 25, '03-Home-Page-V2-search-budget-dropdown936350899484671081.jpg', 'Brochure'),
(89, 25, '02-Home-Page-V2-search-prop-type-dropdown542725683315410986.jpg', 'Brochure'),
(90, 26, '03-Home-Page-V2-search-budget-dropdown884508416.jpg', 'Master Plan'),
(91, 26, 'Error report235088101.png', 'Floor Plan'),
(92, 26, '03-Home-Page-V2-search-budget-dropdown410535634.jpg', 'Gallery'),
(93, 26, '03-Home-Page-V2-search-budget-dropdown862088015.jpg', 'Brochure'),
(94, 26, '03-Home-Page-V2-search-budget-dropdown862088015918040534.jpg', 'Brochure'),
(95, 26, '03-Home-Page-V2-search-budget-dropdown386948311.jpg', 'Construction Status Photos'),
(96, 26, '02-Home-Page-V2-search-prop-type-dropdown896992702.jpg', 'Construction Status Photos'),
(453, 44, 'Master Plan372398290.pdf', 'Master Plan'),
(454, 44, 'Floor Plan507227677.pdf', 'Floor Plan'),
(455, 44, 'Brochure896934766.pdf', 'Brochure'),
(456, 44, '04997122705.jpg', 'Construction Status Photos'),
(457, 44, '05823254795.jpg', 'Construction Status Photos'),
(458, 44, '06401824444.jpg', 'Construction Status Photos'),
(473, 45, 'Brochure71141630.pdf', 'Brochure'),
(474, 45, '01170023596.jpg', 'Construction Status Photos'),
(475, 45, '04767338748.jpg', 'Construction Status Photos'),
(476, 45, '05144274783.jpg', 'Construction Status Photos'),
(477, 45, '07149603558.jpg', 'Construction Status Photos'),
(478, 45, '08950630637.jpg', 'Construction Status Photos'),
(479, 45, '09326028360.jpg', 'Construction Status Photos'),
(480, 46, 'Master Plan589470032.pdf', 'Master Plan'),
(481, 46, '1 Floor Plan609049286.pdf', 'Floor Plan'),
(482, 46, '2 Floor Plan159138000.pdf', 'Floor Plan'),
(483, 46, 'Brochure119815023.pdf', 'Brochure'),
(484, 46, '01550444222.jpg', 'Construction Status Photos'),
(485, 46, '03282233892.jpg', 'Construction Status Photos'),
(486, 46, '04371232544.jpg', 'Construction Status Photos'),
(487, 46, '0673528054.jpg', 'Construction Status Photos'),
(488, 46, '07901693409.jpg', 'Construction Status Photos'),
(489, 46, '09212315776.jpg', 'Construction Status Photos'),
(490, 47, 'Master Plan130113770.pdf', 'Master Plan'),
(491, 47, 'Floor Plan811770241.pdf', 'Floor Plan'),
(492, 47, 'Brochure844568560.pdf', 'Brochure'),
(493, 47, '01475508288.jpg', 'Construction Status Photos'),
(494, 47, '02727413074.jpg', 'Construction Status Photos'),
(495, 47, '03350408967.jpg', 'Construction Status Photos'),
(496, 47, '04334382755.jpg', 'Construction Status Photos'),
(497, 47, '05164383925.jpg', 'Construction Status Photos'),
(501, 48, 'Master Plan954843547.pdf', 'Master Plan'),
(502, 48, 'Floor Plan39306328.pdf', 'Floor Plan'),
(503, 48, 'Brochure385292394.pdf', 'Brochure'),
(504, 48, '01520676912.jpg', 'Construction Status Photos'),
(505, 48, '02208419852.jpg', 'Construction Status Photos'),
(506, 48, '0321275498.jpg', 'Construction Status Photos'),
(507, 48, '04875958512.jpg', 'Construction Status Photos'),
(508, 49, 'Brochure257368395.pdf', 'Brochure'),
(509, 50, 'Brochure477222662.pdf', 'Brochure'),
(511, 52, 'Brochure429892857.pdf', 'Brochure'),
(512, 52, '01591353487.jpg', 'Construction Status Photos'),
(513, 52, '02107903982.jpg', 'Construction Status Photos'),
(540, 62, 'Master Plan981130205.jpg', 'Master Plan'),
(541, 62, '1 Floor Plan933957863.jpg', 'Floor Plan'),
(542, 62, '2 Floor Plan686655888.jpg', 'Floor Plan'),
(543, 62, '3 Floor Plan184735237.jpg', 'Floor Plan'),
(544, 62, '4 Floor Plan486678687.jpg', 'Floor Plan'),
(545, 62, '5 Floor Plan750547066.jpg', 'Floor Plan'),
(546, 62, '6 Floor Plan430856221.jpg', 'Floor Plan'),
(547, 62, '01452088240.jpg', 'Gallery'),
(548, 62, '02874567804.jpg', 'Gallery'),
(549, 62, '03260601066.jpg', 'Gallery'),
(550, 62, '04384814331.jpg', 'Gallery'),
(551, 40, 'Master Plan573225167.pdf', 'Master Plan'),
(552, 40, 'Floor Plan487863430.pdf', 'Floor Plan'),
(553, 40, 'Brochure736439333.pdf', 'Brochure'),
(625, 22, 'Master plan888202328.pdf', 'Master Plan'),
(626, 22, 'Floor Plan177382741.pdf', 'Floor Plan'),
(627, 22, '01 Image122488705.jpg', 'Gallery'),
(628, 22, '02 Image185742878.jpg', 'Gallery'),
(629, 22, '03 Image578388488.jpg', 'Gallery'),
(630, 22, '04 Image404842892.jpg', 'Gallery'),
(631, 22, '05 Image335404960.jpg', 'Gallery'),
(632, 22, 'Brochure283509307.pdf', 'Brochure'),
(633, 22, 'Brochure283509307113242662.pdf', 'Brochure'),
(634, 22, 'Brochure283509307.pdf', 'Brochure'),
(635, 22, 'Brochure283509307113242662.pdf', 'Brochure'),
(636, 22, 'https://youtu.be/buNB9cr2yuc', 'Walkthrough Video'),
(637, 22, '05 Image472217652.jpg', 'Construction Status Photos'),
(638, 24, 'Master Plan688106797.pdf', 'Master Plan'),
(639, 24, 'Floor Plan611640473.pdf', 'Floor Plan'),
(640, 24, '01493855682.jpg', 'Gallery'),
(641, 24, 'Brochure480714852.pdf', 'Brochure'),
(642, 24, 'Brochure480714852231328182.pdf', 'Brochure'),
(643, 24, 'https://youtu.be/lzAYm81uBEo', 'Walkthrough Video'),
(644, 24, '02713002835.jpg', 'Construction Status Photos'),
(645, 24, '03715059683.jpg', 'Construction Status Photos'),
(646, 24, '04560271302.jpg', 'Construction Status Photos'),
(657, 27, 'Master Plan48546467.pdf', 'Master Plan'),
(658, 27, 'Floor Plan 1442537979.pdf', 'Floor Plan'),
(659, 27, 'Floor Plan 2906054717.pdf', 'Floor Plan'),
(660, 27, '01279709580.jpg', 'Gallery'),
(661, 27, '0288060704.jpg', 'Gallery'),
(662, 27, '03522254291.jpg', 'Gallery'),
(663, 27, 'Brochure386340006.pdf', 'Brochure'),
(664, 27, 'https://youtu.be/o0I8x7i6Iag', 'Walkthrough Video'),
(665, 27, '09513956360.jpg', 'Construction Status Photos'),
(666, 27, '10399249633.jpg', 'Construction Status Photos'),
(667, 27, '11999911535.jpg', 'Construction Status Photos'),
(668, 28, 'Master Plan176120002.pdf', 'Master Plan'),
(669, 28, 'Floor Plan337737673.pdf', 'Floor Plan'),
(670, 28, '11816362115.jpg', 'Gallery'),
(671, 28, '12367670123.jpg', 'Gallery'),
(672, 28, 'Brochure677936837.pdf', 'Brochure'),
(673, 28, 'Brochure677936837213665619.pdf', 'Brochure'),
(674, 28, 'https://youtu.be/YeAFUOHW4Ao', 'Walkthrough Video'),
(675, 28, '02268404929.jpg', 'Construction Status Photos'),
(676, 28, '03396163128.jpg', 'Construction Status Photos'),
(677, 29, 'Master Plan223418345.pdf', 'Master Plan'),
(678, 29, 'Floor Plan334954536.pdf', 'Floor Plan'),
(679, 29, '06838270644.jpg', 'Gallery'),
(680, 29, '07290716161.jpg', 'Gallery'),
(681, 29, 'Brochure899561449.pdf', 'Brochure'),
(682, 29, 'https://youtu.be/Y8zGCfFjCSs', 'Walkthrough Video'),
(683, 29, '02104528617.jpg', 'Construction Status Photos'),
(684, 29, '03218959126.jpg', 'Construction Status Photos'),
(698, 30, '1 Master Plan579172455.pdf', 'Master Plan'),
(699, 30, '2 Master Plan783714256.pdf', 'Master Plan'),
(700, 30, '3 Master Plan243071460.pdf', 'Master Plan'),
(701, 30, '1 Floor Plan348723711.pdf', 'Floor Plan'),
(702, 30, '2 Floor Plan208708534.pdf', 'Floor Plan'),
(703, 30, '3 Floor Plan134496382.pdf', 'Floor Plan'),
(704, 30, '01332254685.jpg', 'Gallery'),
(705, 30, '02650911448.jpg', 'Gallery'),
(706, 30, '03920002490.jpg', 'Gallery'),
(707, 30, '04252395593.jpg', 'Gallery'),
(708, 30, 'Brochure266500397.pdf', 'Brochure'),
(709, 30, 'Brochure266500397653838358.pdf', 'Brochure'),
(710, 30, 'https://youtu.be/Jzk0RaRfNvc', 'Walkthrough Video'),
(711, 31, '1 Master Plan184635102.pdf', 'Master Plan'),
(712, 31, '2 Master Plan841465890.pdf', 'Master Plan'),
(713, 31, '3 Master Plan464081788.pdf', 'Master Plan'),
(714, 31, '1 Floor Plan888296014.pdf', 'Floor Plan'),
(715, 31, '2 Floor Plan75675602.pdf', 'Floor Plan'),
(716, 31, '3 Floor Plan331696066.pdf', 'Floor Plan'),
(717, 31, '4 Floor Plan866683937.pdf', 'Floor Plan'),
(718, 31, 'Brochure492830259.pdf', 'Brochure'),
(719, 31, 'Brochure492830259752296545.pdf', 'Brochure'),
(720, 31, '0158312547.jpg', 'Construction Status Photos'),
(721, 31, '03988842911.jpg', 'Construction Status Photos'),
(722, 32, 'Master Plan543103647.pdf', 'Master Plan'),
(723, 32, '1 Floor Plan290709230.pdf', 'Floor Plan'),
(724, 32, '2 Floor Plan791873258.pdf', 'Floor Plan'),
(725, 32, '3 Floor Plan513265411.pdf', 'Floor Plan'),
(726, 32, '4 Floor Plan895632643.pdf', 'Floor Plan'),
(727, 32, '01103409941.jpg', 'Gallery'),
(728, 32, '02576995854.jpg', 'Gallery'),
(729, 32, '03261806809.jpg', 'Gallery'),
(730, 32, 'Brochure755951328.pdf', 'Brochure'),
(731, 32, 'Brochure755951328137015178.pdf', 'Brochure'),
(732, 32, 'https://youtu.be/qR70DXvP8j0', 'Walkthrough Video'),
(733, 33, 'Master Plan952882887.pdf', 'Master Plan'),
(734, 33, '1 Floor Plan447582540.pdf', 'Floor Plan'),
(735, 33, '2 Floor Plan676456902.pdf', 'Floor Plan'),
(736, 33, '3 Floor Plan72393178.pdf', 'Floor Plan'),
(737, 33, '4 Floor Plan116448147.pdf', 'Floor Plan'),
(738, 33, '5 Floor Plan584450433.pdf', 'Floor Plan'),
(739, 33, '07992522793.jpg', 'Gallery'),
(740, 33, '08378937874.jpg', 'Gallery'),
(741, 33, '09105381248.jpg', 'Gallery'),
(742, 33, 'Brochure707030779.pdf', 'Brochure'),
(743, 33, 'Brochure707030779632887543.pdf', 'Brochure'),
(744, 33, 'https://youtu.be/VUwi87yKOTY', 'Walkthrough Video'),
(745, 33, '01603251854.jpg', 'Construction Status Photos'),
(746, 33, '02435752150.jpg', 'Construction Status Photos'),
(747, 33, '03846665373.jpg', 'Construction Status Photos'),
(748, 33, '05254819522.jpg', 'Construction Status Photos'),
(749, 34, 'Master Plan913018673.pdf', 'Master Plan'),
(750, 34, 'Floor Plan710666398.pdf', 'Floor Plan'),
(751, 34, '02965765580.jpg', 'Gallery'),
(752, 34, '03972419198.jpg', 'Gallery'),
(753, 34, '08683297902.jpg', 'Gallery'),
(754, 34, '09509398334.jpg', 'Gallery'),
(755, 34, 'Brochure853234011.pdf', 'Brochure'),
(756, 34, 'Brochure853234011639321849.pdf', 'Brochure'),
(757, 34, 'https://youtu.be/oDqjdabIdOc', 'Walkthrough Video'),
(758, 34, '11353638594.jpg', 'Construction Status Photos'),
(759, 34, '12141223558.jpg', 'Construction Status Photos'),
(760, 36, 'Floor Plan464454826.pdf', 'Floor Plan'),
(761, 36, '0113905250.jpg', 'Gallery'),
(762, 36, '02830539038.jpg', 'Gallery'),
(763, 36, '0376812437.jpg', 'Gallery'),
(764, 36, 'Brochure239904646.pdf', 'Brochure'),
(765, 36, 'https://youtu.be/WCGHDXZI3aU', 'Walkthrough Video'),
(766, 35, 'Master Plan98720239.pdf', 'Master Plan'),
(767, 35, 'Floor Plan737942650.pdf', 'Floor Plan'),
(768, 35, '01872035198.jpg', 'Gallery'),
(769, 35, '02270171500.jpg', 'Gallery'),
(770, 35, 'Brochure733439403.pdf', 'Brochure'),
(771, 35, 'Brochure733439403415336063.pdf', 'Brochure'),
(772, 35, 'https://youtu.be/37tr15sDqr0', 'Walkthrough Video'),
(773, 35, '10700452953.jpg', 'Construction Status Photos'),
(774, 35, '11486727778.jpg', 'Construction Status Photos'),
(775, 35, '12374284132.jpg', 'Construction Status Photos'),
(776, 37, 'Master Plan823473954.pdf', 'Master Plan'),
(777, 37, 'Floor Plan519091686.pdf', 'Floor Plan'),
(778, 37, 'Brochure372217711.pdf', 'Brochure'),
(779, 37, 'https://youtu.be/C7GYIZtpnpo', 'Walkthrough Video'),
(780, 37, '01766731384.jpg', 'Construction Status Photos'),
(781, 37, '02131163024.jpg', 'Construction Status Photos'),
(782, 37, '03215592259.jpg', 'Construction Status Photos'),
(783, 37, '04161292529.jpg', 'Construction Status Photos'),
(784, 37, '05.176740986.jpg', 'Construction Status Photos'),
(785, 37, '06339465120.jpg', 'Construction Status Photos'),
(786, 37, '07744861907.jpg', 'Construction Status Photos'),
(787, 38, 'Floor Plan474108725.pdf', 'Floor Plan'),
(788, 38, 'Brochure97874202.pdf', 'Brochure'),
(789, 38, 'https://youtu.be/r1QKjn9hivI', 'Walkthrough Video'),
(790, 38, '01773389855.jpg', 'Construction Status Photos'),
(791, 38, '02565431600.jpg', 'Construction Status Photos'),
(792, 38, '03472292297.jpg', 'Construction Status Photos'),
(793, 39, 'Floor Plan919385306.pdf', 'Floor Plan'),
(794, 39, '01178275405.jpg', 'Gallery'),
(795, 39, 'Brochure906154342.pdf', 'Brochure'),
(796, 39, 'https://youtu.be/n97JfXxVR3U', 'Walkthrough Video'),
(797, 41, 'Master Plan482836476.pdf', 'Master Plan'),
(798, 41, 'Floor Plan125502334.pdf', 'Floor Plan'),
(799, 41, '05122655087.jpg', 'Gallery'),
(800, 41, '06424483990.jpg', 'Gallery'),
(801, 41, '07932254276.jpg', 'Gallery'),
(802, 41, '08684607836.jpg', 'Gallery'),
(803, 41, 'Brochure10814379.pdf', 'Brochure'),
(804, 41, 'https://youtu.be/iT9ioK1W23c', 'Walkthrough Video'),
(805, 41, '01910264834.jpg', 'Construction Status Photos'),
(806, 41, '02717125173.jpg', 'Construction Status Photos'),
(807, 41, '03738646736.jpg', 'Construction Status Photos'),
(808, 41, '04951912735.jpg', 'Construction Status Photos'),
(809, 42, 'Master Plan919802757.pdf', 'Master Plan'),
(810, 42, 'Floor Plan232051814.pdf', 'Floor Plan'),
(811, 42, '01675246702.jpg', 'Gallery'),
(812, 42, '02595218234.jpg', 'Gallery'),
(813, 42, '03262144719.jpg', 'Gallery'),
(814, 42, 'Brochure995696935.pdf', 'Brochure'),
(815, 42, 'https://youtu.be/G3NEb6PZSo0', 'Walkthrough Video'),
(816, 43, 'Master Plan754577459.pdf', 'Master Plan'),
(817, 43, 'Floor Plan531323005.pdf', 'Floor Plan'),
(818, 43, '03209766519.jpg', 'Gallery'),
(819, 43, '04625822817.jpg', 'Gallery'),
(820, 43, '05113068187.jpg', 'Gallery'),
(821, 43, '06121120378.jpg', 'Gallery'),
(822, 43, '07755066400.jpg', 'Gallery'),
(823, 43, 'Brochure739792072.pdf', 'Brochure'),
(824, 43, 'https://youtu.be/0z45rLi-7zg', 'Walkthrough Video'),
(825, 43, '01240521650.jpg', 'Construction Status Photos'),
(826, 43, '02478576718.jpg', 'Construction Status Photos'),
(827, 58, '01794854108.jpg', 'Gallery'),
(828, 58, '02673788950.jpg', 'Gallery'),
(829, 58, '03115861538.jpg', 'Gallery'),
(830, 58, '04595450168.jpg', 'Gallery'),
(831, 58, '05421250057.jpg', 'Gallery'),
(832, 58, 'Brochure429985390.pdf', 'Brochure'),
(833, 58, 'http://talleen.in/sobhaip/', 'Walkthrough Video'),
(834, 59, 'Master Plan170946974.jpg', 'Master Plan'),
(835, 59, '1 Floor Plan486493189.jpg', 'Floor Plan'),
(836, 59, '2 Floor Plan931272892.jpg', 'Floor Plan'),
(837, 59, '3 Floor Plan431228006.jpg', 'Floor Plan'),
(838, 59, '01380956918.jpg', 'Gallery'),
(839, 59, '02967763236.jpg', 'Gallery'),
(840, 59, '03403513158.jpg', 'Gallery'),
(841, 59, '04687214843.jpg', 'Gallery'),
(842, 59, 'Brochure174860911.pdf', 'Brochure'),
(843, 60, 'Master Plan311487852.jpg', 'Master Plan'),
(844, 60, '01150995236.jpg', 'Gallery'),
(845, 60, 'Brochure875643696.pdf', 'Brochure'),
(846, 65, 'Master Plan379094891.jpg', 'Master Plan'),
(847, 65, '1 Floor Plan492477223.jpg', 'Floor Plan'),
(848, 65, '2 Floor Plan544637463.jpg', 'Floor Plan'),
(849, 65, '3 Floor Plan592505825.jpg', 'Floor Plan'),
(850, 65, '0188117431.jpg', 'Gallery'),
(851, 65, '02373258156.jpg', 'Gallery'),
(852, 65, '03681562620.jpg', 'Gallery'),
(853, 65, 'Brochure831350461.pdf', 'Brochure'),
(854, 65, '04900253083.jpg', 'Construction Status Photos'),
(855, 65, '05902498603.jpg', 'Construction Status Photos'),
(856, 66, 'Master Plan122905912.jpg', 'Master Plan'),
(857, 66, '1 Floor Plan614933399.jpg', 'Floor Plan'),
(858, 66, '2 Floor Plan810836566.jpg', 'Floor Plan'),
(859, 66, '3 Floor Plan312288109.jpg', 'Floor Plan'),
(860, 66, '01395437358.jpg', 'Gallery'),
(861, 66, '02443208235.jpg', 'Gallery'),
(862, 67, 'Master Plan729853654.jpg', 'Master Plan'),
(863, 67, '1 Floor Plan44774814.jpg', 'Floor Plan'),
(864, 67, 'Brochure791055206.pdf', 'Brochure'),
(865, 67, '01416340594.jpg', 'Construction Status Photos'),
(866, 68, 'Master Plan316593202.jpg', 'Master Plan'),
(867, 68, '1 Floor Plan708473242.jpg', 'Floor Plan'),
(868, 68, '2 Floor Plan302684169.jpg', 'Floor Plan'),
(869, 68, '3 Floor Plan519025068.jpg', 'Floor Plan'),
(870, 68, '4 Floor Plan847288223.jpg', 'Floor Plan'),
(871, 68, '01196971232.jpg', 'Gallery'),
(872, 68, '02645550615.jpg', 'Gallery'),
(873, 68, '03523285347.jpg', 'Gallery'),
(874, 68, '04277840468.jpg', 'Gallery'),
(875, 68, 'Brochure129871329.pdf', 'Brochure'),
(876, 61, 'Master Plan452589087.jpg', 'Master Plan'),
(887, 63, 'Master Plan696452784.jpg', 'Master Plan'),
(888, 63, '1 Floor Plan60126246.jpg', 'Floor Plan'),
(889, 63, '2 Floor Plan741566383.jpg', 'Floor Plan'),
(890, 63, '3 Floor Plan466416418.jpg', 'Floor Plan'),
(891, 63, '4 Floor Plan192039142.jpg', 'Floor Plan'),
(892, 63, '5 Floor Plan899929769.jpg', 'Floor Plan'),
(893, 63, 'Brochure212565259.pdf', 'Brochure'),
(894, 63, '01742743132.jpg', 'Construction Status Photos'),
(895, 63, '02393372601.jpg', 'Construction Status Photos'),
(896, 63, '03277480141.jpg', 'Construction Status Photos'),
(960, 70, '03-Home-Page-V2-search-budget-dropdown450359686.jpg', 'Banner Image'),
(989, 23, 'Brigade Caladium-Master Plan969582271.pdf', 'Master Plan'),
(990, 23, 'Error image84003570.jpg', 'Floor Plan'),
(991, 23, '01-TM-property-for-  rent-listing-V2-filters-2850896977.jpg', 'Gallery'),
(994, 23, 'Error image807628730.jpg', 'Gallery'),
(999, 23, 'Brigade Caladium-Brochure874893125.pdf', 'Brochure'),
(1000, 23, 'Brigade Caladium-Brochure874893125554759825.pdf', 'Brochure'),
(1001, 23, 'https://youtu.be/y1XEw5uNltA', 'Walkthrough Video'),
(1002, 23, '01-Home-Page-V2546633377.jpg', 'Banner Image'),
(1005, 23, '01-TM-property-for-rent-listing-V2-filters-1646021456.jpg', 'Gallery'),
(1006, 23, 'default-image340755522.jpg', 'Gallery'),
(1007, 23, '01-TM-property-for-  rent-listing-V2-filters-2681126281.jpg', 'Gallery'),
(1008, 23, '01-TM-property-for-rent-listing-V2-filters-163400498.jpg', 'Gallery'),
(1011, 23, '01-TM-property-for-  rent-listing-V2-filters-2474356648.jpg', 'Gallery'),
(1012, 23, '03-Home-Page-V2-search-budget-dropdown186425938.jpg', 'Gallery'),
(1013, 23, '01-Home-Page-V2700956892.jpg', 'Gallery'),
(1021, 71, 'logo2872958750.png', 'Gallery'),
(1022, 71, 'logo (1)439370611.jpg', 'Gallery'),
(1023, 71, 'logo164003283.jpg', 'Gallery'),
(1024, 71, '01-TM-property-for-  rent-listing-V2-filters-2171827188.jpg', 'Gallery'),
(1025, 71, '01-TM-property-for-rent-listing-V2-filters-1512701901.jpg', 'Gallery'),
(1030, 72, 'https://youtu.be/y1XEw5uNltA', 'Walkthrough Video'),
(1031, 21, 'Master Plan466460474.jpg', 'Master Plan'),
(1032, 21, '1 Floor Plan320572351.jpg', 'Floor Plan'),
(1033, 21, '2 Floor Plan939467006.jpg', 'Floor Plan'),
(1034, 21, '3 Floor Plan913165151.jpg', 'Floor Plan'),
(1035, 21, '4 Floor Plan591993408.jpg', 'Floor Plan'),
(1036, 21, '5 Floor Plan225658464.jpg', 'Floor Plan'),
(1037, 21, '6 Floor Plan947839542.jpg', 'Floor Plan'),
(1038, 21, '01820506898.jpg', 'Gallery'),
(1039, 21, '02920882422.jpg', 'Gallery'),
(1040, 21, '03509605995.jpg', 'Gallery'),
(1041, 21, 'Brochure828519610.pdf', 'Brochure'),
(1042, 21, '05676420018.jpg', 'Construction Status Photos'),
(1043, 21, '06348176929.jpg', 'Construction Status Photos'),
(1044, 21, 'mickey894739627.jpeg', 'Banner Image'),
(1045, 15, 'mickey913854423.jpeg', 'Banner Image');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `member_id` int(11) DEFAULT NULL COMMENT 'Member ID',
  `prop_type` int(11) DEFAULT NULL COMMENT 'Property Type',
  `no_units` int(11) DEFAULT NULL COMMENT 'No. of Units',
  `no_floors` int(11) DEFAULT NULL COMMENT 'No. of Floors',
  `year_of_construction` int(11) DEFAULT NULL COMMENT 'Year of Construction',
  `address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Address ',
  `tot_units` int(11) DEFAULT NULL COMMENT 'No. of Units',
  `site_contact_email` varchar(255) DEFAULT NULL COMMENT 'On-site Contact Email ID',
  `site_contact_no` varchar(20) DEFAULT NULL COMMENT 'On-site Contact No. ',
  `image` text COMMENT 'Photos',
  `lat` varchar(30) DEFAULT NULL COMMENT 'Latitude',
  `long` varchar(30) DEFAULT NULL COMMENT 'Longitude',
  `incharge_name` varchar(200) DEFAULT NULL COMMENT 'In-charge Name',
  `designation` varchar(100) DEFAULT NULL COMMENT 'In-charge designation',
  `builder_name` varchar(200) DEFAULT NULL COMMENT 'Builder Name',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `proj_status` varchar(100) DEFAULT NULL COMMENT 'Project Status',
  `tot_proj_area` varchar(100) DEFAULT NULL COMMENT 'Total Project Area in acres',
  `start_price` varchar(100) DEFAULT NULL COMMENT 'Price starting form',
  `area_range` varchar(100) DEFAULT NULL COMMENT 'Area range',
  `approvals` text COMMENT 'Approvals',
  `loan` text COMMENT 'Loans',
  `proj_start_date` date DEFAULT NULL COMMENT 'Project Start Date',
  `poss_date` date DEFAULT NULL COMMENT 'Possession handing over date',
  `master_plan` varchar(255) DEFAULT NULL COMMENT 'Master Plan',
  `featured` int(11) NOT NULL DEFAULT '0' COMMENT 'Featured',
  `advertisement` int(11) NOT NULL DEFAULT '0' COMMENT 'Advertisement',
  `overview` text,
  `facilities` text,
  `budget` varchar(100) DEFAULT NULL,
  `no_block` int(11) DEFAULT NULL,
  `locality` varchar(200) DEFAULT NULL,
  `locality_desc` text,
  `list_of_doc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `name`, `member_id`, `prop_type`, `no_units`, `no_floors`, `year_of_construction`, `address_id`, `tot_units`, `site_contact_email`, `site_contact_no`, `image`, `lat`, `long`, `incharge_name`, `designation`, `builder_name`, `is_active`, `proj_status`, `tot_proj_area`, `start_price`, `area_range`, `approvals`, `loan`, `proj_start_date`, `poss_date`, `master_plan`, `featured`, `advertisement`, `overview`, `facilities`, `budget`, `no_block`, `locality`, `locality_desc`, `list_of_doc`) VALUES
(15, 'Brigade Gateway', 109, 1, 1, 12, 2008, 382, 122, '', NULL, '', '12.9713063', '77.60699139999997', 'Ramdev', '', 'Brigade Enterprises Ltd', 1, '', '', '', '', '', '', NULL, NULL, NULL, 0, 0, '', NULL, NULL, 0, 'Whitefield', '', ''),
(16, 'Shoba Golden Triangle', 109, 1, 1, 15, 2007, 421, 25, 'Selva@gmail.com', NULL, '', '12.9444237', '77.56566850000002', 'Selva Kumar', 'Assistant Manager', '', 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(17, 'Shoba white house', 110, 1, 1, 12, 2009, 431, 12, 'Monaj33@yahoomail.com', NULL, '', '12.9156824', '77.54632079999999', 'Monaj', 'Manager', '', 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(18, 'DLF Golden Traiangle', 111, 1, 1, 13, 2005, 442, 25, 'shashi@gmail.com', NULL, '', '12.9297564', '77.58934169999998', 'Shashi', 'Manager', '', 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(19, 'Shob Pentiuna', 113, 1, 1, 15, 2006, 451, 1, 'Nandu@gmail.com', NULL, '', '12.9298849', '77.59264800000005', 'Nandu Kumar', 'Assistant Manager', 'Shoba Developers', 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(21, 'Sobha Grandeur', 109, 1, NULL, 9, NULL, 794, NULL, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '2.8', '', '3BR 2190 to 3383 Sq.ft / 4BR 3653 to 3754  Sq.ft / 2BR 1541  Sq.ft / 3BR 1855 to 2160 Sq.ft ', 'BBMP', 'Allahabad Bank,Bank of India', '2012-08-18', '2016-08-01', NULL, 1, 0, 'For city lovers, the day doesn?t end once work is done. That?s why Sobha Morzaria Grandeur is perfect for you. It?s central to some of Bangalore?s finest malls, shopping centres, theatres, restaurants, gaming arcades and more. So whether you want to indulge in some fine dining, do some window shopping, catch the late night movie or take the kids out for dessert, all you have to do is step out.', 'faccilites', NULL, 1, 'Near Forum Mall, Koramangala', 'Koramangala is a locality in Bangalore, India. Situated in the south-eastern part of the city, it is one of the largest neighborhoods, and is a highly sought after residential locality with wide tree-lined boulevards and a mix of luxury apartments, commercial structures, and posh bungalows. It has gradually developed into a commercial hub.\r\n\r\nKoramangala is largely cosmopolitan in nature. It is the focal point of many busy main roads, notably the Intermediate Ring Road linking the Airport Road with Hosur Road and Electronics City, Sarjapur Road leading to the Outer Ring Road, the IT corridor and beyond and 80 Feet Road leading to Viveknagar and MG Road at the center of Bangalore. It commands one of the highest property prices in the city and is considered by Bangaloreans to be one of the better places to live, although commercialization in the form of shopping centres, malls and restaurants, increasing traffic and population have posed problems for its residents.\r\n\r\nKoramangala is also home to many educational institutions like the St. John\'s Medical College, Christ University, Indian Institute of Plantation Management, Amity Global Business School, NuSkillz Academy, Jyoti Nivas College, Vemana Institute of Technology, Indian Institute of Astrophysics, NMIMS, and TAPMI.', '1.Aadhar card\r\n2.ration card'),
(22, 'Brigade Altamont', NULL, 1, NULL, NULL, NULL, 796, 189, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '3', '81 Lakh Onwards', '2BHK: 1260 sqft / 3BHK: 1670 sqft', 'BDA', 'Allahabad Bank,Axis Bank,Canara Bank,Citibank,Federal Bank,HDFC Ltd,ICICI Bank,IDBI BanK,L&T Housing Finance Ltd,Punjab National Bank,State Bank of India,State Bank of Mysore,UCO Bank', '1970-01-01', '2015-12-31', NULL, 1, 0, 'Brigade Altamont is the perfect example of how form and function complement each other to accommodate all the necessities of modern living, while ensuring the efficient use of space.\r\n\r\nEvery living space at Brigade Altamont is designed to make you fall in love with a new quality of life. The layout of each home combined with unique structure and attention to detail, natural ventilation and sunlight makes each home a pleasure. \r\n\r\nA home at Brigade Altamont is where you can relax, find comfort and experience the joys of finer living. At Brigade Altamont everyone can enjoy the advantages of finer living. With a range of lifestyle amenities every individual has the option to unwind and rejuvenate.', '', NULL, NULL, 'Off Hennur Road', '', ''),
(23, 'Brigade Caladium', NULL, 1, NULL, NULL, NULL, 798, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9715987', '77.59456269999998', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '2', '3.4 Crore Onwards', '3BHK: 3450 & 3520 sqft / 4BHK: 4000, 4310 & 4450 sqft', 'BBMP,BDA,BIAAPA,BMRDA,DTCP', 'Bank of India,Bank of Maharashtra,Canara Bank,Central Bank of India,Citibank,ICICI Bank,J & K Bank ,L&T Housing Finance Ltd,PNB Housing Finance,State Bank of Travancore', '2015-01-01', '2016-02-04', NULL, 1, 0, 'At Brigade Caladium the sublime luxury that greets you on arrival prevails throughout the property. The smooth curves of the building derive inspiration from a droplet of water ? the source of life. Set amidst exquisite landscaping the Caladium is the definitive answer to a lifestyle quest. These 3 & 4 bedroom residences range from 3450 to 4450 square feet. All residents have access to the rooftop swimming pool, lounge deck and fitness centre.', '', NULL, NULL, 'Hebbal', 'Hebbal is an Assembly Constituency area in Bangalore, Karnataka, India, which was once indicative of the north endpoint of the city. Though originally famous for Hebbal Lake, it is now better known for the serpentine maze of flyovers that network the Outer Ring Road and Bellary Road. The flyover spans a length of 5.23 kilometres (3.2 mi) over all the loops combined. The flyover was built by Gammon India. The lake area is well known for the park, the boating facility, and for the bird watching opportunities.\r\n\r\nHebbal used to be the end of the corporation limits of Bangalore City. One can still see the milestone between the Baptist hospital and checkpost. Bangalore has now grown quite a bit towards north, beyond these marks. The commencement of the Bengaluru International Airport (BIAL) at Devanahalli, which was constructed by L&T, in 2008 has ensured that Hebbal has become a very important location for the passengers flying out of Bangalore. L&T factory, which was established in 70\'s, is also close to Hebbal. Raintree Boulevard project is coming up at same place before GKVK University. All arterial roads and road networks leading to the BIAL intersect at Hebbal. Further, BIAL has its office on the National Highway (NH 7) exit of the Hebbal flyover in Gayathri Lake Front. L&T is doing tree transplantation in Hebbal Area at Raintree Boulevard Project.', ''),
(24, 'Brigade Cosmopolis', NULL, 1, NULL, NULL, NULL, 800, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9715987', '77.59456269999998', 'Brigade Group', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '9', '90 Lakh Onwards', '1250 - 3640 sft', '', 'Axis Bank,Canara Bank,Citibank,HDFC Ltd,ICICI Bank,Standard Chartered Bank,State Bank of India,State Bank of Travancore', '1970-01-01', '2016-06-01', NULL, 1, 0, 'Located strategically in the heart of Whitefield, Brigade Cosmopolis is a short distance away from everything you need, and is also well-connected to the International Airport. \r\n\r\nSpread over 9.3 acres, Cosmopolis features wide open spaces, water bodies and is home to long forgotten pleasures of wandering paths and natural surprises. The vast greens are the signature feature of Brigade Cosmopolis, which is also home to an urban-forest experience.\r\n\r\nDesigned for the discerning global citizen, life here is everything you could ever want. A home with every feature you could expect to see and have in any city around the globe is what Brigade Cosmopolis aims to provide. Intended as a residential area of international importance and standards, the lifestyle here is comfortable as well as luxurious, with a well-appointed clubhouse, landscaped courtyards, plenty of clean-green lung space, all situated in one of the best locations in the city of Bangalore.', '', NULL, NULL, 'Whitefield', 'Whitefield is an integral part of Bangalore in the state of Karnataka, India. Established in the late 1800s as a settlement for the Eurasians and Anglo Indians of Bangalore, Whitefield remained a quaint little settlement to the east of Bangalore city till the late 1990s when the local IT boom turned it as a major suburb. It is now a major part of Greater Bangalore. It is also renowned for Sathya Sai Baba\'s ashram called Brindavan and as a haven for multinational information technology companies', ''),
(25, 'test proj2', NULL, 1, NULL, 4, NULL, 804, 1, '', NULL, '', '13.026384036454747', '77.5140380859375', 'ganesh', '', 'Brahma Holdings', 0, 'Pre-launch project', '30000', '1000-30000 thousand', '10-20', 'BBMP', 'Allahabad Bank', '1970-01-01', '1970-01-01', NULL, 0, 0, 'overview', 'facilites', NULL, 2, 'Whitefield', '?	Neev Preschool ? 100 M\r\n?	Forum Value Mall ? 200 M\r\n?	Columbia Asia Hospital ? 300 M\r\n?	Deen\'s Academy ? 03 Kms\r\n?	Brigade Tech Park ? 04 Kms\r\n?	ITPB ? 04 Kms\r\n?	EPIP ? 05 Kms\r\n?	Vydehi Hospital ? 04 Kms\r\n?	Ascendas Park Square Mall ? 04 Kms\r\n?	Vivanta By Taj Whitefield ? 04 Kms\r\n?	Four Points By Sheraton	? 05 Kms\r\n?	Sri Sathya Sai Super Speciality Hospital ? 06 Kms\r\n?	Inorbit Mall ? 06 Kms\r\n?	Greenwood High International School ? 08 Kms\r\n?	The Brigade School, Mahadevapura ? 09 Kms\r\n?	Inventure Academy ? 09 Kms\r\n?	Phoenix Market City ? 09 Kms', 'list of documents'),
(26, 'test proj3', NULL, 1, NULL, 3, NULL, 806, 14, '', NULL, '', '13.028140091255281', '77.50880241394043', 'Arun Kumar', '', 'Brahma Holdings', 0, 'Upcoming project', '4545', '435-43534', '34-435', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'overview', 'facilites', NULL, 4, 'locality', 'locality desc', 'documents'),
(27, 'Brigade Exotica', NULL, 1, NULL, NULL, NULL, 818, 474, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '10', '1.7 Crore Onwards', '2640, 3140 sqft', '', 'Axis Bank,Bank of Baroda,Bulls,Citibank,HDFC Ltd,ICICI Bank,PNB Housing Finance,Punjab National Bank,Standard Chartered Bank,State Bank of India,Union Bank of India', '2016-01-01', '2016-09-01', NULL, 1, 0, 'A pre-certified green project with large open spaces and a mini forest, Brigade Exotica brings lung space back to Bangalore.', '', NULL, NULL, 'Old Madras Road', '', ''),
(28, 'Brigade Golden Triangle', NULL, 1, NULL, NULL, NULL, 820, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9715987', '77.59456269999998', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '18', '63 Lakh Onwards', '1630 & 1640 sq. ft 2010 sq. ft', '', 'Axis Bank,Bank of Baroda,Citibank,Corporation Bank,HDFC Ltd,ICICI Bank,PNB Housing Finance,State Bank of India', '1970-01-01', '2016-12-31', NULL, 1, 0, 'A mixed use development, Brigade Golden Triangle offers luxury apartments, state-of the-art office spaces and a retail facility to make life convenient.\r\n\r\nThis 20+ acre development will be ideal as a home or investment. A choice to live here could mean work is just a stroll away. Great connectivity means you are a 30 minute drive from the CBD and a 15 minute one from Whitefield and Hoskote. The proposed shopping centre would be you answer to all personal needs and home necessities, making life that much easier. \r\n\r\nThere will be plenty of recreational opportunities and sport amenities. The green environment will provide you with a sanctuary, lung space, and nature to enjoy undisturbed.', '', NULL, NULL, 'Old Madras Road', '', ''),
(29, 'Brigade Northridge', NULL, 1, NULL, NULL, NULL, 822, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '7', '70 Lakh Onwards', '2 BHK: 1230 & 1290 sqft / 3 BHK: 1610 - 1880 sqft', '', 'Axis Bank,HDFC Ltd,ICICI Bank,PNB Housing Finance,State Bank of India', '1970-01-01', '2015-12-31', NULL, 1, 0, 'Well-located, well-connected and ideal for families and professionals alike, Brigade Northridge is the answer for those looking to live close to but not quite in the heart of town.\r\n\r\nWhile close to several arterial roads, a home here will still let you experience the greenery and serenity of Kogilur Cross Lake. Six hundred apartments on a lush 7.5 acres and plenty of green spaces, Brigade Northridge will offer everything you need and more. Other amenities include a gymnasium, clubhouse, swimming pool and a children?s play area.\r\n', '', NULL, NULL, 'Kogilu Road, Jakkur', 'Jakkur is a general purpose airport serving Bangalore, Karnataka in India. It is the only dedicated general aviation field in Bengaluru. The other three airports are either private military or dedicated to commercial operations. It is spread over an area of 214 acres to the north of Bangalore, just after the Hebbal flyover near the village of Jakkur. The airfield premises include facilities for flight training, area leased to private parties for hangars and maintenance activities and other common facilities.', 'Please bring the following documents when booking an apartment:\r\n?	ID proof ? Driving License / Passport copy / Voter?s ID\r\n?	PAN Copy\r\n?	Passport size photographs ? 2 Nos.\r\n?	Cheque Book'),
(30, 'Brigade Omega', NULL, 1, NULL, NULL, NULL, 829, 520, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '6', '80 Lakh Onwards', '2BHK: 1310-1430 sqft / 3BHK: 1660-1800 sqft /  4BHK: 2390 sqft', '', 'Axis Bank,Canara Bank,Citibank,ICICI Bank,L&T Housing Finance Ltd,State Bank of India,State Bank of Mysore,Syndicate Bank,Vijaya Bank', '1970-01-01', '2015-12-31', NULL, 1, 0, 'Brigade Omega homes are right across from the Thurahalli Forest Reserve off Kanakapura Road. Set in a six-acre property, there is space enough to breathe, play, run and enjoy the freedom of movement. \r\n\r\nRich in verdure, this Brigade property houses a jogging trail through the campus, a gym, badminton court, a swimming pool and plenty of open areas to explore and wander at will. Additionally, spread over the campus are parks and open spaces. There are three towers and 520 apartments at Brigade Omega. The 2, 3 and 4 bedroom apartments are designed to maximise light and ventilation, and range in area from 1290 square feet to 2390 square feet.', '? Car parking at basement level and stilt level on ownership basis only\r\n? Sewage Treatment Plant designed as per load\r\n? Bore well with sump and pump\r\n? Cable TV connection, internet & intercom facility\r\n? Clubhouse with gymnasium, indoor games room, party area, swimming pool & multipurpose hall\r\n? Well-designed landscaped areas for recreation and children\'s play\r\n?  Fire Protection System as per Karnataka fire force requirement', NULL, NULL, 'Banashankari 6th Stage', 'Banashankari abbreviated as BSK, is a neighbourhood near to South Bangalore. It gets its name from the Banashankari Amma Temple on Kanakapura Road, one of Bangalore\'s oldest and well known temples, which was constructed by Subramanya Shetty in 1915. Formed in the mid 2000 and still in nascent stage in terms of development is perhaps the largest. It is further divided into 11 blocks. www.BskToday.com has latest information on Banashankari 6th Stage. ', ''),
(31, 'Brigade Orchards Luxury Apartments', NULL, 1, NULL, NULL, NULL, 831, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '130', '3.6 Crore onwards', '2BHK: 1080 - 1390 sqft / 3BHK: 1290 - 1740 sqft', '', 'Axis Bank,HDFC Ltd,ICICI Bank,IDBI BanK,Kotak Mahindra Bank,Punjab National Bank,State Bank of India,State Bank of Mysore', '1970-01-01', '2016-12-31', NULL, 1, 0, 'A ten-minute drive from the airport, these apartments will be built with state-of the-art technology. \r\nPart of the 130-acre Brigade Orchards Smart Township, the luxury apartments are available across two blocks, Cedar and Deodar. As always, these apartments will be tastefully finished and complemented with thoughtful amenities, features and lush greenery.', '', NULL, NULL, 'Devanahalli', 'Devanahalli, also called \"Devandahalli\", \"Dyaavandalli\", Devanadoddi and Devanapura, is a town and Town Municipal Council in Bangalore Rural district in the state of Karnataka in India. The town is located 40 kilometres (25 mi) to the north-east of Bangalore. Devanahalli is the site of the Bengaluru International Airport, the second largest airport in India. A multi-billion-dollar Devanahalli Business Park with two IT Parks are coming up on nearly 400 acres (1.6 km2) adjoining the airport. An Aerospace Park, Science Park and a 1000 crore-rupee Financial City are also coming up. A new satellite ring road will connect the city with Doddaballapur. Devanahalli is situated near the upcoming $22 Billion, 12,000-acre (49 km2) BIAL IT Investment Region, to be the largest IT region in India.', ''),
(32, 'Brigade Lakefront', NULL, 1, NULL, NULL, NULL, 833, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '20', '76 Lakh Onwards ', '2BHK: 1150 - 1440 sqft / 3BHK: 1660 - 2790 sqft / 4BHK: 3360 sqft', '', 'Andhra Bank,Axis Bank,Citibank,Federal Bank,HDFC Ltd,ICICI Bank,IDBI BanK,State Bank of India', '1970-01-01', '2016-03-01', NULL, 1, 0, 'If life is a journey, we invite you to walk it with us. Situated in the heart of Whitefield, Brigade Lakefront is well connected to various schools, offices, cultural meeting places and parks. But what sets it apart is the space there is around your home where you can simply walk.\r\n\r\nThere are 2, 3 and 4 bedroom apartments ranging from 1330 to 3360 square feet. And all residents have access to the 30,000 squarefoot clubhouse with its rooftop swimming pool, various sporting facilities, large open green spaces to walk about in, a children?s play area and even a senior citizens court.', '', NULL, NULL, 'Whitefield', 'Whitefield is an integral part of Bangalore in the state of Karnataka, India. Established in the late 1800s as a settlement for the Eurasians and Anglo Indians of Bangalore, Whitefield remained a quaint little settlement to the east of Bangalore city till the late 1990s when the local IT boom turned it as a major suburb. It is now a major part of Greater Bangalore. It is also renowned for Sathya Sai Baba\'s ashram called Brindavan and as a haven for multinational information technology companies.', ''),
(33, 'Brigade Panorama', NULL, 1, NULL, NULL, NULL, 835, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '11', '48 Lakh Onwards', '2BHK: 1070; 1120 (SL) / 3BHK: 1410-1710; 1500-1810 (SL)', '', 'Axis Bank,Corporation Bank,HDFC Ltd,ICICI Bank,L&T Housing Finance Ltd,Punjab National Bank,State Bank of India,State Bank of Mysore,State Bank of Patiala', '1970-01-01', '2017-12-31', NULL, 1, 0, 'Set on an 11-acre property on the Bangalore ? Mysore highway, Brigade Panorama offers 2, 3 and 3.5 bedroom apartments ranging from 1030 to 1650 square feet. \r\n\r\nThis is a luxury development where form and function complement each other in the careful design of every space. With regard to connectivity, the NICE corridor is easily accessible as well as the Metro route from Kengeri. Brigade Panorama also offers several amenities including a clubhouse with a gym, a swimming pool, a children?s play area, and other sports facilities for everyone to enjoy.', '', NULL, NULL, 'Mysore Road', 'State Highway 17 (SH-17), also known as Mysore Road, is a state highway connecting the cities of Bangalore and Mysore in the south Indian state of Karnataka. The highway has a total length of 149 kilometres (93 mi). It was built and maintained by the Karnataka Road Development Corporation Limited and inaugurated in 2003. The highway passes through the towns of Ramanagara, Channapatna, Maddur, Mandya and Srirangapatna (Seringapattinam), before entering Mysore. The road is dual carriageway and passes over the Kaveri river.\r\n\r\nThe 62 kilometres (39 mi) Bangalore-Maddur section was upgraded under build-operate-transfer (Annuity) basis by a consortium of NCC Infra and Maytas Infra.[2][3][4] The 49 kilometres (30 mi) Maddur-Mysore stretch was upgraded by Soma under an EPC contract. Bengaluru-Mysore highway is being upgraded to 6 lane and given status of \"NH-275\" .', ''),
(34, 'Wisteria at Brigade Meadows', NULL, 1, NULL, NULL, NULL, 837, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '60', ' 52 Lakh Onwards', '2BHK: 1120- 1300 sqft / 3BR: 1450- 1630 sqft', '', 'HDFC Ltd,Punjab National Bank,State Bank of India,State Bank of Mysore', '1970-01-01', '2016-06-01', NULL, 1, 0, 'Wisteria at Brigade Meadows offers luxurious living in the south of Bangalore along with everything else you could want. \r\n\r\nContemporary designs and elegant finishes dominate the homes here. The two and three bedroom apartments will range in area from 1120 square feet to 1623 square feet. The amenities and facilities available are vast and will include clubhouses, a shopping arcade, all manner of sporting facilities and an equal number of children?s activity spaces. At Wisteria, the idea is that you will find all you need and more within these sixty acres.', '', NULL, NULL, 'Kanakapura Road', 'Kanakapura is a town and the headquarters of Kanakapura Taluk in the Ramanagar district in the state of Karnataka, India. Situated 55 km from the city Bengaluru, this town is famous for the production of silk and granite. It is located among the lush green forests of the state of Karnataka and The town is a tourism hotspot and an often visited tourist favorite in the entire state of Karnataka, as it has something for everyone ranging from avid trekkers to history buffs and wildlife enthusiasts.', ''),
(35, 'Brigade Meadows Value Homes', NULL, 1, NULL, NULL, NULL, 841, 1800, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '60', '38 Lakh Onwards', '1BHK: 700 Sq.ft / 2BHK: 950 Sq.ft / 3BHK: 1150 Sq.ft', '', 'Axis Bank,Bank of Maharashtra,Canara Bank,Corporation Bank,HDFC Ltd,ICICI Bank,IDBI BanK,PNB Housing Finance,State Bank of India,Syndicate Bank', '1970-01-01', '1970-01-01', NULL, 1, 0, 'With everything you need to live a comfortable life. these homes are value for money as well as a great investment for the future. \r\n\r\nThere are 1.5, 2 and 3 bedroom apartments to choose from. Sizes range from 700 to 1150 square feet. \r\nAmenities are aplenty and include a clubhouse, swimming pool, party area, parks and a play area. Additionally there will be a shopping centre, school and commercial centre in close proximity. The property is also very close to the Art of Living Ashram for those so inclined; and there are several reputed schools, hospitals and other social facilities as well nearby.', '', NULL, NULL, 'Kanakapura Road', 'Kanakapura is a town and the headquarters of Kanakapura Taluk in the Ramanagar district in the state of Karnataka, India. Situated 55 km from the city Bengaluru, this town is famous for the production of silk and granite. It is located among the lush green forests of the state of Karnataka and The town is a tourism hotspot and an often visited tourist favorite in the entire state of Karnataka, as it has something for everyone ranging from avid trekkers to history buffs and wildlife enthusiasts.', ''),
(36, 'Brigade Crescent', NULL, 1, NULL, NULL, NULL, 843, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '', '6.8 Crore Onwards', '4190, 4250 & 4290 sqft', '', 'Axis Bank,Citibank,State Bank of India', '1970-01-01', '2016-01-01', NULL, 0, 0, 'An exclusive residence at the heart of Bangalore does not get better than this. Ten four-bedroom apartments, access to all amenities and within close proximity of all manner of social infrastructure; Brigade Crescent is in a class of its own. There is only one apartment per floor and sizes range from 4190 square feet to 4290 square feet. Each apartment has large balconies and a double height deck, giving occupants clear views of the Bangalore skyline. As is befitting, only the best of finishes and fittings have been used throughout the apartment. The elevator opens right into your living area making it both convenient and private for you and your guests. (Your help will have their own room in your apartment, and a separate lift as well.)\r\n\r\nYour home is as private as can be at Brigade Crescent. Outside of it you will have access to facilities shared with the other nine apartments including a rooftop swimming pool, gymnasium, indoor games room and a party area.', '', NULL, NULL, 'Nandidurga Road', 'Nandidurg Road is a well frequented locality based in the Bangalore City. The location is boarded by the Benson Town, Jayamahal Road, Millers Road, Coles Road and Vasanthnagar. From the Majestic main bus stations and the City Railway Station in Bangalore the distance to Nandidurga Road is about 6 kilometers. From the Bangalore International Airport the distance is about 36 kilometers. Some of the main bus stages located in close proximity to the area are YMCA Nandidurg Road, Marappa Garden, Muddamma Garden, Jayamahal and Chinnappa Garden.\r\n\r\nHealth care facilities can be found in plenty with the MS Ramaiah Memorial Hospital, Bhagwan Mahaveery Jain Hospital, Mallya Hospital, Hosmat Hospital and the St. Philomenas Hospital in the vicinity. For those looking for fun and activities, Nandidurga Road offers a variety of choices with a lot of happening places in the area including the Pottery Town, Opus Restaurant, Fun Cinemas, Cafe-Coffee Day, Costa Coffee, The French Loaf, Cafe-Alibaba, Garden Restaurant and Elements Eatery. You can visit any of these restaurants or sample the exclusive Bangalore cuisine and delights at any of the hotels in the vicinity including Jayamahal Palace, Young Island Comforts, Fortune Select or the VSL Grand Services Apartments.\r\n\r\nEducational facilities are well provided in the locality with prominent schools and educational centers like Sri Ayyappan English Nursery, Good Hope High School, Bethany Covent and the Gulabi Girls High School being within easy access from the Nandidurga Road. For shopping needs there are some prestigious shopping complexes within the vicinity like Sigma Mall, Prestige Center Point, Esteem Mall and the New Lifestyle Mall.', ''),
(37, 'Brigade Orchards Parkside Retirement Homes', NULL, 1, NULL, NULL, NULL, 845, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limted', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '130', '3.6 Cr. onwards', '', '', 'Axis Bank,HDFC Ltd,ICICI Bank,IDBI BanK,Kotak Mahindra Bank,Punjab National Bank,South Indian Bank,State Bank of India,State Bank of Mysore', '1970-01-01', '2017-03-31', NULL, 1, 0, 'For those who are done with the rat race but still enjoy the vibrancy of life, these homes are for you. \r\nAll those things you?ve meant to do but never found the time for ? do them all here at the Parkside. Learn a new language, cultivate a new hobby, spend time with friends, and read all those books you?ve collected, all without worrying about life?s mundane details.\r\n\r\nHere at the Parkside, the facilities have been designed so that you have the best of comfort and specialised care. Everything from your chores to your bills, security and medical care are looked after so that you can finally live the life you have earned.\r\n', '', NULL, NULL, 'Devanahalli', 'Devanahalli, also called \"Devandahalli\", \"Dyaavandalli\", Devanadoddi and Devanapura, is a town and Town Municipal Council in Bangalore Rural district in the state of Karnataka in India. The town is located 40 kilometres (25 mi) to the north-east of Bangalore. Devanahalli is the site of the Bengaluru International Airport, the second largest airport in India. A multi-billion-dollar Devanahalli Business Park with two IT Parks are coming up on nearly 400 acres (1.6 km2) adjoining the airport. An Aerospace Park, Science Park and a 1000 crore-rupee Financial City are also coming up.[1] A new satellite ring road will connect the city with Doddaballapur. Devanahalli is situated near the upcoming $22 Billion, 12,000-acre (49 km2) BIAL IT Investment Region, to be the largest IT region in India.\r\n\r\nTotal infrastructure development in the area is estimated to be well over US $30 Billion over the next two years. With significant commercial and residential development in the area, real estate is in high demand in the region. Devanahalli was the birthplace of Tipu Sultan, popularly known as the \"Fishseller of Mysore\"', ''),
(38, 'Brigade Orchards Pavilion Villas', NULL, 1, NULL, NULL, NULL, 847, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '130', '3.6 Cr. onwards.', '4900, 4910, 4920, 4940  & 4950 sqft', '', 'Axis Bank,HDFC Ltd,IDBI BanK,Kotak Mahindra Bank,Punjab National Bank,South Indian Bank,State Bank of India,State Bank of Mysore', '1970-01-01', '2016-06-01', NULL, 1, 0, 'The Pavilion Villas at Brigade Orchards are designed to respect the earth and environment.\r\nPlaced in a secluded enclave, the Pavilion Villas are all about home being the seat of comfort. Privacy amidst green surroundings makes for peace and quiet after a long day, while the elements of light and air are maximised for your comfort. Here, homes hearken back to the homes of yore; they are stately, independent buildings with plenty of garden space. There are three distinct designs to choose from: Tulip, Carnation and Chrysanthemum. Every house is created with all the necessities for a luxurious lifestyle from large bedrooms and walk-in wardrobes, to private gardens and courtyards. Everything about a home here is about a greater quality of life.', '', NULL, NULL, 'Devanahalli', 'Devanahalli, also called \"Devandahalli\", \"Dyaavandalli\", Devanadoddi and Devanapura, is a town and Town Municipal Council in Bangalore Rural district in the state of Karnataka in India. The town is located 40 kilometres (25 mi) to the north-east of Bangalore. Devanahalli is the site of the Bengaluru International Airport, the second largest airport in India. A multi-billion-dollar Devanahalli Business Park with two IT Parks are coming up on nearly 400 acres (1.6 km2) adjoining the airport. An Aerospace Park, Science Park and a 1000 crore-rupee Financial City are also coming up. A new satellite ring road will connect the city with Doddaballapur. Devanahalli is situated near the upcoming $22 Billion, 12,000-acre (49 km2) BIAL IT Investment Region, to be the largest IT region in India.[2]\r\n\r\nTotal infrastructure development in the area is estimated to be well over US $30 Billion over the next two years. With significant commercial and residential development in the area, real estate is in high demand in the region. Devanahalli was the birthplace of Tipu Sultan, popularly known as the \"Fishseller of Mysore.', ''),
(39, 'Brigade Sonata', NULL, 1, NULL, NULL, NULL, 849, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', '', '', '2630, 2780 Sq.ft', '', 'Citibank,Corporation Bank,ICICI Bank', '2016-01-01', '1970-01-01', NULL, 1, 0, 'Situated at the heart of Bangalore?s prestigious Palace Grounds, a home here is a sanctuary. Each home is carefully planned with fluid spaces, privacy, functionality and aesthetics seamlessly brought together. Here is where you find a balance to the world outside. To ensure the ideal living environment at Brigade Sonata, residents also have access to a rooftop swimming pool, gymnasium, party area and landscaped gardens. Envisioned as an urban retreat, the spaces impart a Zen-like ambience. The lobby is tropical; the water body is an oasis, and the polished stone floors and minimal furnishings work in tandem to bring a sense of tranquillity and serenity to you.', '', NULL, NULL, 'Palace Road', '', ''),
(40, 'Brigade Sparkle', NULL, 1, NULL, 4, NULL, 851, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.2976926', '76.60011120000001', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', '3', '', '860 - 1290 Sq.ft', '', 'State Bank of India', '1970-01-01', '2016-01-01', NULL, 0, 0, 'The royal city of Mysore now adds another jewel to its crown. Set in three acres of rolling greens, with an emphasis on landscape and open spaces, the Brigade Group brings you Brigade Sparkle. \r\n\r\nThis development is a four-storeyed residential complex with two- and three-bedroom apartments ranging from 860 to 1290 sq. ft. The built-up areas will take up only 40% of the land area, allowing for plenty of vast open lung space.\r\n\r\nA home at Brigade Sparkle is just what every modern home owner needs. Located in J.P. Nagar, Mysore, Brigade Sparkle is also very well connected to the airport, the ring road and the Mysore railway station.\r\n', '', NULL, NULL, 'J. P. Nagar', '', ''),
(41, 'Brigade Mountain View', NULL, 1, NULL, NULL, NULL, 853, NULL, 'mysore@brigadegroup.com', NULL, '', '12.3271434', '76.627479', 'Suchendra', 'Branch Manager', 'Brigade Enterprises Limited', 1, 'Ongoing project', '4', ' 66 Lakh Onwards', '2BHK: 1220-1320 sqft / 3BHK: 1620-2070 sqft / 4BHK: 2640 & 2720 sqft', '', 'State Bank of India', '1970-01-01', '2017-03-01', NULL, 1, 0, 'Located on the scenic Mysore-Ooty road with a priceless view of the Chamundi Hills, Brigade Mountain View is the home you have always wanted. \r\nThese are 2, 3 and 4-bedroom apartments situated in the heart of Mysore close to the Mysore Palace. Elegance and serenity are the timeless characteristics of these homes, set amongst historical landmarks including the Lalith Mahal Palace, the Mysore Golf Club and the Mysore Turf Club.\r\nIn addition to a luxurious home, the life spaces are equally promising. These include a rooftop swimming pool, a gym, various sports facilities, a children?s play area and a senior citizens? court. Life is a picturesque spot here at Brigade Mountain View.', '', NULL, NULL, 'Ooty Road', '', ''),
(42, 'Brigade Palmgrove', NULL, 3, NULL, NULL, NULL, 855, NULL, 'mysore@brigadegroup.com', NULL, '', '12.3271434', '76.627479', 'Suchendra', 'Branch Manager', 'Brigade Enterprises Limited', 1, 'Ongoing project', '14', '2.2 Crore Onwards', '3300, 5020, 5130 & 5230 sqft', '', 'State Bank of India', '1970-01-01', '2016-03-01', NULL, 1, 0, 'Designed especially for the discerning and privileged, Brigade Palmgrove brings together everything you deem important in your living spaces. \r\nLocated in Bogadi Road, about a kilometre away from the Ring Road, the development will have only 50 villas and 48 apartments. The villas are designed as standalone units; there are also townhouses planned as pairs of conjoined duplexes. Facilities will include membership to a social and sports club, recreation zones, landscaped gardens and jogging tracks.', '', NULL, NULL, 'Bogadi Road', '', ''),
(43, 'Brigade Symphony', NULL, 1, NULL, NULL, NULL, 857, NULL, 'mysore@brigadegroup.com', NULL, '', '12.3271434', '76.627479', 'Suchendra', 'Branch Manager', 'Brigade Enterprises Limited', 1, 'Ongoing project', '7', '33 Lakh Onwards ', '1BHK: 790 & 810 sqft / 2BHK: 1350 sqft / 3BHK: 2130-2150 sqft / 4BHk: 3050 sqft', '', 'City Union Bank,HDFC Ltd,State Bank of India', '1970-01-01', '2015-12-01', NULL, 1, 0, 'These luxury apartments have been designed keeping in mind the rich heritage of Mysore as well as the lifestyle of the Mysorean.\r\nLarge family spaces, a large patio, and airy bedrooms are among the typical features of a Mysore home that have been carefully incorporated in the apartments at Brigade Symphony. The idea is that the soul and heritage of a Mysorean is maintained intact here, only embellished with modern necessities and conveniences. In addition to these thoughtfully laid out apartments, amenities include a clubhouse with a gym, a swimming pool, children?s play area, and other sports facilities.', '', NULL, NULL, 'KRS Road', '', ''),
(44, 'Brigade Bhuwalka Icon', NULL, 11, NULL, NULL, NULL, 859, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '8', '', '17,000 - 50,000 sft', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'This Brigade creation is ideal business address for large and mid sized companies. Strategically located on Whitefield main rd. right next to Brigade Techpark which houses Global MNCs. With floor plate areas of 34,000 square feet to 50,000 square feet, Brigade Bhuwalka Icon is ideal for the growth of any business.\r\nHigh end design and creativity are evident in this building with 3 basements, ground floor plus 9 storeys. Infrastructure leaves nothing to be desired and includes 100% power back up, central air-conditioning, 8 elevators, ample parking, and proposed cafeteria/ restaurant amongst others. The property is also well-connected to several hotels, restaurants, cafes, banks and shopping centres.', 'Grade A Specifications\r\nDouble height reception lobby\r\nCommon cafeteria\r\nLandscape garden\r\nAmple parking space\r\nGood frontage with great visibility\r\nProposed retail on ground & first floor', NULL, NULL, 'Whitefield', 'Proposed Metro Station?Right next to the building\r\nITPB?500 m\r\nTaj Vivanta?500 m\r\nMarriot?1.5 Kms\r\nInorbit Mall?1.5 Kms\r\nSatya Sai Hospital?1.5 Kms\r\nOuter Ring Road?6 Kms\r\nMG Road?16 Kms', ''),
(45, 'Brigade IRV Centre', NULL, 11, NULL, NULL, NULL, 861, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '10', '', '3000 - 12000 sft', '', 'HDFC Ltd,Punjab National Bank', '1970-01-01', '1970-01-01', NULL, 1, 0, '', 'Grade A Specifications\r\nStoried reception lobby\r\nLandscape garden\r\nCommon cafeteria\r\nProposed Coffee Shop', NULL, NULL, '', 'Proposed Metro Station?1 Kms\r\nMarriot Hotel?1.5 Kms\r\nSatya Sai Hospital?1.5 Kms\r\nInorbit Mall?1.5 Kms\r\nAloft Hotel?2 Kms\r\nITPB?2.5 Kms\r\nTaj Vivanta?2.5 Kms\r\nOuter Ring road?5 Kms', ''),
(46, 'Brigade Magnum', NULL, 11, NULL, NULL, NULL, 864, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '4', '', '25000-55000 sft', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Located on about 4 acres of prime land on Bellary Road, Hebbal, en route to the International Airport, Brigade Magnum is suitable for single and multiple occupancies. A Grade ?A? green building, Brigade magnum had offices across two bocks with two floors each and 2 levels of basement parking.\r\nFloor plates are of 25,000 square feet and 55,000 square feet. Supporting infrastructure includes landscaped green, 100% power back up, central air-conditioning, and 9 elevators. Brigade Magnum is perfect for all corporate offices, especially IT and software development companies or R&D centres.', 'Grade A Specifications \r\nExtensive landscaped space \r\nGardens \r\nCafeteria \r\nShared ammenities at lobby level \r\nAmphitheatre \r\nAmple parking space', NULL, NULL, 'Hebbal', 'Columbia Asia Hospital?0.5 kms\r\nEsteem Mall?0.75 Kms\r\nHebbal Lake?2 Kms\r\nBaptist Hospital?3 Kms\r\nPresidency College?4 Kms\r\nVidya Niketan School?5 Kms\r\nMovenpick?6 Kms\r\nHoward Johnson?6 Kms', ''),
(47, 'Nalapad Brigade Centre', NULL, 11, NULL, 10, NULL, 866, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '36', '', '10,000 - 58,000 sft', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'Located strategically in Whitefield, the Nalapad Brigade Center is annexed to Brigade Metropolis, a 36 acre Integrated Enclave. The Nalapad Center comprises a basement, ground-plus-9 floors, a floor plate area of 58,000 square feet, and 540,000 square feet of total development. The infrastructure is solid and includes 13 elevators and a multi-level car park.\r\nIdeal for a variety of businesses, office spaces start at 10,000 square feet onwards and are at the heart of the IT industry in Bangalore. The green surroundings of the Brigade Metropolis Integrated Enclave make this a complete work environment and a worthwhile investment.', 'Grade A specifications \r\nDouble height lobby \r\nCommon cafeteria \r\nLandscape garden \r\nMulti level car park', NULL, NULL, 'Whitefield', 'Regent Club: Part of Brigade Metropolis Proposed Metro Station?100m\r\nPhoenix Market City?800m\r\nOuter Ring Road?2 Kms\r\nITPB?4 Kms\r\nTaj Vivanta?4 Kms\r\nMarriot?4 Kms\r\nMG Road?12 Kms', ''),
(48, 'Signature Towers Brigade Golden Triangle', NULL, 11, NULL, 22, NULL, 868, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', NULL, '', '2500 - 8850 sft', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'Signature Towers forms the commercial wing of the Brigade Golden Triangle, a mixed-use development on Old Madras Road. The Towers houses spacious corridors, large landscaped spaces and ample parking in addition to offices that range in area from 2,500 square feet to 12,000 square feet.\r\nLocated in one of Bangalore?s fastest developing growth corridors, Signature Towers connects the City?s IT hub and the International Airport. Excellent connectivity, great design and infrastructure make investments in office space here a promise of greater returns.', 'Cafeteria \r\nFlexible office spaces \r\nAmple parking space \r\nLandscaped garden \r\nGrade A Specifications \r\nDouble height reception lobby', NULL, NULL, 'Old Madras Road', 'Nearest Bus stop?100 m\r\nHoskote Lake?1 Kms\r\nVibgyor school?1 kms\r\nShell Petrol Bunk?2 Kms\r\nPolice Station?2 Kms\r\nHoskote Industrial hub?4 Kms\r\nHoskote Mission Medical Centre?4 Kms\r\nNew Baldwin International School?5 Kms', ''),
(49, 'Brigade Rubix', NULL, 11, NULL, NULL, NULL, 870, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', NULL, '', '2000 sft - 12000 sft', '', 'Axis Bank,Citibank,Dena Bank', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Located close to the Peenya industrial area and not far from the International Airport, the railway station, bus terminus, and the Metro station, Rubix is a sound choice for a corporate work place. Small businesses that require a self-contained environment would find the ideal solution to work spaces here at Rubix as it integrates all the facilities you need. Infrastructure here includes 100% power back up, ample parking, air-conditioning, high speed elevators and even a food court/cafeteria.', 'Ample parking space \r\nSpacious lobby area \r\nHigh speed elevators \r\nCafeteria \r\nEasy access to social infrastructure nearby', NULL, NULL, 'HMT Township', 'National Institute of Design and CMTI?100 m\r\nMetro Station?0.5 kms\r\nOuter Ring road?1 kms\r\nYeshwantpur Satellite bus stand and railway station?3 Kms\r\nMS Rammiah Institute & College?6 kms', ''),
(50, 'World Trade Center', NULL, 11, NULL, NULL, NULL, 872, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', '40', '', '', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Set in a 40-acre campus the World Trade Center, Bangalore, is the recipient of the CREDAI Real Estate Award (2013) for the Best Office Building in Bangalore. This fine creation has 32 levels, a grand triple height atrium, a helipad and observation deck, 40,000 square feet plates, office spaces starting at 6,000 square feet, and a 9 level car park, amongst other features.\r\nThe Gateway enclave it is situated within also encompasses a 5-star hotel, a mall, club, luxury apartments, hospital, school and even a man-made lake.', 'Grade A++ specification \r\nTriple height reception lobby \r\nCommon Cafeteria \r\nHelipad Multi Level Car Parking', NULL, NULL, 'Malleswaram - Rajajinagar', 'Part of the 40 acre lifestyle enclave?100m\r\nBrigade Gateway comprising of Orion Mall?100m\r\nSheraton Hotel?100m\r\nColumbia Asia Hospital?100m\r\nGateway Apartments?100m\r\nBrigade School?100m\r\nThe Galaxy Club?100m\r\nMetro Station?100m', ''),
(51, 'Orion East Mall', NULL, 10, NULL, NULL, NULL, 874, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', NULL, '', '', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'A world-class mall development worthy of an upmarket locality that is yet untapped by big retail. Orion East brings with it all the hallmarks of the multi-award winning Orion mall brand. Acked by Brigade group\'s stellar credentials across development, marketing and opration management, Orion east will once again raise the benchmark in malls.\r\n\r\nMain Anchor: Cinepolis and Shoppers Stop\r\nOther Brands: Jack & Jones, Vero Moda, Nike, Puma, Van Heusen, Louis Philippe, Allen Solly, Arrow, U.S. Polo Assn., Mc Donalds, Apple and many more.\r\nCatchment Areas: The only mall in the 7 Km radius serving the upmarket localities of Frazer Town, Benson Town , Cooke Town, HRBR Layout & Kamanahalli.', 'Two-level basement car park \r\nShopping \r\nF & B Multiplex \r\nEvents and entertainment', NULL, NULL, 'Banaswadi Main Road', 'Banaswadi Railway Station?1 Kms\r\nITC Infotech?1 Kms', ''),
(52, 'The Arcade at Brigade Meadows', NULL, 10, NULL, 2, NULL, 876, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', '60', '', '', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'ocated at the entrance of a 60 Acre enclave, The Arcade is designed as a neighbourhood shopping centre catering to the residents of Brigade Meadows (around 3000 units ). It would meet the convenience of the residents and serve the needs of the locals and those of the people living in the catchment.\r\nThe Arcade is a G+1 retail development with a proposed mix of supermarkets, food court, coffee shops, fruit and vegetable stalls, pharmacy, clinic etc.\r\n2nd and 3rd floor is being planned for small and medium office spaces.', 'Neighborhood shopping center \r\nPrimary health care \r\nSuper market \r\nF&B \r\nATM', NULL, NULL, 'Kanakapura Road Next to Art of Living', 'Art of Living?1 Kms\r\nProposed Metro station?11 Kms', ''),
(53, 'Nalpad Brigade Broadway', NULL, 10, NULL, NULL, NULL, 878, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Ongoing project', NULL, '', '', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Nalapad Brigade Broadway, a retail and office space is located on KG Road, which is the main Central Business District. It is just 1 km from the City railway station, City bus stand & upcoming metro railway station. It has a great advantage of primary catchment like Gandhinagar, Avenue road, BVK Iyengar road, Chickpet etc. It is right in the main retail and transport hub of Bangalore. The pre-dominant retail, residential canvas in and around the Nalapad Brigade Broadway is one of the USPs of the project. \r\nIt has around around 60,000 sft of retails and 65,000 sft of office space available on Lease.', 'Shopping center \r\nF&B & Two-level basement car park \r\nOffice space', NULL, NULL, 'K G Road', 'City railway station?1 Kms\r\nCity Bus stand?1 Kms\r\nUpcoming Metro railway station?1 Kms', '');
INSERT INTO `property` (`id`, `name`, `member_id`, `prop_type`, `no_units`, `no_floors`, `year_of_construction`, `address_id`, `tot_units`, `site_contact_email`, `site_contact_no`, `image`, `lat`, `long`, `incharge_name`, `designation`, `builder_name`, `is_active`, `proj_status`, `tot_proj_area`, `start_price`, `area_range`, `approvals`, `loan`, `proj_start_date`, `poss_date`, `master_plan`, `featured`, `advertisement`, `overview`, `facilities`, `budget`, `no_block`, `locality`, `locality_desc`, `list_of_doc`) VALUES
(54, 'Orion Mall at Brigade Gateway', NULL, 10, NULL, NULL, NULL, 880, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', NULL, '', '', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'A world-class mall in every facet, the iconic Orion Mall at Brigade Gateway is Brigade Group\'s flagship mall venture and has won every accolade and prestigious award bestowed by the most recognised industry associations in India. This premium mall set high standards across construction, architecture, consumer experience, and marketing; and quickly established itself as the benchmark among its peers. Spread across 8.2 lakh square feet, with every detail of a rich modern retail environment woven in, Orion Mall at Brigade Gateway is a true game changer in the mall industry.\r\n', '? 8.2 lakh square feet. Unique lakeside promenade. \r\n? Tree of Life - The collaboration of a passionate team of design and art brought to life a breathtaking mural enhancing the serenity of the lakeside space. The sun is in yellow jalsalmer stone that glows in the light of the rising and setting sun. The Moon is in translucent white marble. \r\n? The exterior architecture - Designed by New Yorks renowned architectural firm HOK Architects. \r\n? The interiors of the mall - Designed by Foley Designs and DSP. \r\n? The lighting of the facade - So designed that the colours can be changed periodically. \r\n? Expressing a seamless journey of experiences, made enjoyable through engaging spatial experiences. \r\n? Parking area - Dedicated, high tech and well planned that uses modern technology to track empty slots. \r\n? Hassle free parking for over 3500 Cars and 3500 two-wheelers.', NULL, NULL, 'Malleswaram - Rajajinagar', 'WTC?1 Kms\r\nColumbia Asia Hospital?1 Kms\r\nGateway Apartments?1 Kms\r\nBrigade School?1 Kms\r\nThe Galaxy Club?1 Kms\r\nSheraton Hotel?1 Kms\r\nISCKON Bangalore?1 Kms\r\nMetro Station?1 Kms\r\nHigh Ultra Lounge?1 Kms', ''),
(55, 'The Arcade at Brigade Metropolis', NULL, 10, NULL, NULL, NULL, 882, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Completed project', NULL, '', '', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Situated within the well-organised Brigade Metropolis Integrated Enclave, the Arcade forms the retail wing of the campus. Close to Bangalore?s IT hub and other sought-after commercial and residential locations, the Arcade is set to meet the diverse needs of its many visitors. From shopping, dining, lifestyle, entertainment and business, the Arcade has it all. Retail facilities at the Arcade include a supermarket, a 24-hour pharmacy and clinic, beauty salons, cafes and food courts, apart from the numerous lifestyle brands. There will also be small business centres and office spaces, travel desks, ATMs and other complementary services.', 'Shopping: Supermarket, Footwear, Florists, Gifts, Music \r\nBusiness/ Services: Small offices, Business centres \r\nF & B: Cafes, Food courts, Casual dining', NULL, NULL, 'Whitefield', '', ''),
(56, 'Brigade Solitaire', NULL, 10, NULL, NULL, NULL, 884, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 0, 'Completed project', NULL, '', '20910 sft + 3700 terrace', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Brigade Solitaire, a prime retail and office space in the heart of Bangalore is strategically located next to Taj Gateway on Richmond road. It is a stand-alone building with dedicated car parking space. It is among the finest business business complex in Central Business District. Spacious and located inthe heart of the city with multiple access points from MG Road, Brigade Road, Magrath Road and Brunton Road. Brigade Solitaire offers excellent opportunity for your growing business. ', '1 Basement parking \r\nStag parking', NULL, NULL, 'Residency Road', 'Taj Gateway Hotel?1 kms\r\nAdjacent Metro Station?1 kms', ''),
(57, 'Brigade Point', NULL, 10, NULL, NULL, NULL, 886, NULL, 'mysore@brigadegroup.com', NULL, '', '12.9715987', '77.59456269999998', 'Brigade Enterprises Limited', 'Branch Manager', 'Brigade Enterprises Limited', 1, 'Completed project', NULL, '', 'Floor plate Size: 8230 sft', '', '', '1970-01-01', '1970-01-01', NULL, 1, 0, 'Brigade Point is a certain destination for those in and around Mysore, with its green, attractive landscaping, ample parking and easy access. Indeed, a prestigious feather in the cap for the Brigade Group.', 'Premium retail space \r\nBeautiful landscaping \r\nAmple parking space', NULL, NULL, 'Gokulam main Road', '', ''),
(58, 'Sobha Indraprastha', NULL, 1, NULL, 36, NULL, 888, 356, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '9', '', '3BHK: 1950 to 2280 / 3BHK: Dup 2875 to 2970 / 4BHK: 3179 to 3994 / 4BHK: Dup 3715', '', '', '1970-01-01', '2018-03-01', NULL, 1, 0, 'A retreat fit for royalty\r\nIt\'s not easy to please kings. Yet one tries. By bringing everything they want, right where they want them to be. By redefining the parameters of luxury at every step. And above all, by offering an imperial architectural marvel that they have never experienced before.\r\n\r\nSobha Indraprastha, Central Bangalore\'s tallest, ritziest residential tower boasts of an infinity pool on the 37th floor, inspired by the famed infinity pool at Marina Bay Sands, Singapore. A Sky Lounge at 433 feet height, giving the perfect view of the city\'s skyline. It also houses one of the finest retail spaces offering the best of global brands, right within the premises. An 11 screen multiplex, a hyper market, restaurants, an entertainment centre and even a food court are just some of the other features of this retail centre. For those who seek finer details, there\'s a grand clubhouse spread over 36th SE 37th floor, a reading lounge, a squash court, a hi-tech gym, sauna, steam and Jacuzzi, cards and carrom lounge, walkways, children\'s play area and a multi-purpose hall. Indraprastha, an address \'Fit for Kings\' is all set to amaze you discerning taste.\r\n\r\nIn keeping with its commitment to give you the best without any compromises, Sobha Indraprastha has been suitably located in the quaint environs of Rajajinagar, an area that still retains its old-world charm With wide tree-lined avenues and calm neighbourhoods, it will be your own slice of haven away from the madding crowd and the trappings of city life.\r\n\r\nSet in a sprawling 9.37 acre landscape with just 40% built-up are Sobha Indraprastha has limited 356 Super Luxury Apartments & Duplex Penthouses, spacious enough to house your king size living aspirations.\r\n\r\nThere\'s more reason to celebrate. Besides the proximity factor that enables you to easily access the CBD, there\'s the fastest way to travel with the upcoming metro rail link just a hop away from the property.', 'Civic Amentities\r\nLanscaped Area\r\nPergola\r\nGrass Grid\r\nBoundary Line\r\nTransformer Yard\r\nEntry to Lobby\r\nParks & Open spaces\r\nRamp to Basement\r\nFire Drive Way\r\nDG Yard\r\nSecurity Cabin\r\nEntry/Exit\r\nResidential Entry\r\nDrive Way', NULL, 2, 'Minerva Mill,Rajaji Nagar', '', ''),
(59, 'Sobha City', NULL, 1, NULL, NULL, NULL, 890, NULL, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '36', '', '2BR:1519 Sq.ft / 3BR:1795 - 2024 Sq.ft / 3BR: 2005 - 2164 Sq.ft', '', '', '2011-06-01', '2016-12-01', NULL, 1, 0, 'Estupendo. Stupendous.\r\nNothing describes this Mediterranean marvel better.\r\n\r\nIts time to bring out the wines and breads. And the Mozzarella and pasta Because Sobha is bringing you the city.\r\n\r\nA luxurious Mediterranean-themed township barely 3 kms away from Hebbal Ring Road Stunning landscaped gardens Ornate fountains. Pebbled pathways Acres of greenery... a touch here, and a flourish there, make this 36-acre Mediterranean paradise the finest place to live in Bangaluru.\r\n', '', NULL, NULL, 'Near Hebbal Ring Road', 'Proposed Metro Station?1 Kms\r\nMarriot Hotel?1.5 Kms\r\nSatya Sai Hospital?1.5 Kms\r\nInorbit Mall?1.5 Kms\r\nAloft Hotel?2 Kms\r\nITPB?2.5 Kms\r\nTaj Vivanta?2.5 Kms\r\nOuter Ring road?5 Kms', ''),
(60, ' Sobha 25 Richmond', NULL, 1, NULL, NULL, NULL, 892, 7, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '', '', '3Bed: 2777.29 - 2826.72 sq ft /3bed Duplex pent house: 3282.06 - 3314.83 sq.ft', 'BBMP', '', '2015-12-19', '1970-01-01', NULL, 1, 0, 'Welcome to Sobha 25 Richmond, a home that is truly the pinnacle of contemporary architectural design. At Sobha, we always strive to create more than a home. We create masterpieces and Sobha 25 Richmond is among the most coveted.\r\n\r\nWhat makes Sobha 25 Richmond so unique and sought after is not just the location or the amenities, it is also the rare sophistication that comes with a bespoken home. You are at liberty to customize your own home in our state-of-the-art design studio. We will ensure that your home will be tailored to your sense of style, making Sobha 25 Richmond, quite literally, your dream home.\r\n\r\nJust like any collection of masterpieces, Sobha 25 Richmond is as exclusive as it gets. So embrace this rarest of opportunities to live in a home that is the cr?me de la cr?me of the homes in the city.', '', NULL, NULL, 'Richmond Road', 'UB City ? 2 kms\r\n1 MG Mall ? 2.8 kms  \r\nBangalore Club ? 650 meters\r\nGolf Course ? 6kms\r\nVidhan Soudha ? 2.6 kms\r\nRace course ? 5 kms\r\nChinnaswami Stadium ? 3 kms\r\nBowring Club ? 2.5 kms\r\nTaj Vivanta ? 2.7 kms\r\nOberoi ? 2.8 kms\r\nHyatt ? 2.8kms\r\nJW Marriot ? 2kms\r\nITC Gardenia ? 2 kms\r\nBrigade road ? 1.8 kms\r\nRitz Carlton ? 1 kms', ''),
(61, 'Sobha Halcyon', NULL, 1, NULL, 21, NULL, 894, 175, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '3', '', '3BR: 1536 to 1865 / 3BHK 1536 sft to 1574.35 sft / 3BHK: 1814.16 sft - 1890.15 sft', '', '', '2015-03-07', '1970-01-01', NULL, 1, 0, 'Sobha Limited (formerly Sobha Developers Ltd.) presents international quality homes in Whitefield with a vast green space. Sobha Halcyon is spread over 2.97 acres and offers 175 exclusive 3 BHK super luxury apartments with a plethora of world-class amenities. Come and visit the property today and experience Sobha\'s renowned quality and luxury in Whitefield.', '', NULL, NULL, 'Near Kannamangala', '', ''),
(62, 'Sobha Silicon Oasis', NULL, 1, NULL, 20, NULL, 896, 900, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '15', '', '2 BHK: 126.89 -132.93 sq mt / 3 BHK: 140.66 - 148.21 sq mt', '', '', '2014-04-25', '1970-01-01', NULL, 1, 0, 'Sobha Limited (formerly Sobha Developers Ltd.) now enters the city\'s famed IT corridor and brings with it a home that truly belongs here. Sobha Silicon Oasis, spread over 15 acres of lush greenery include 2 & 3 BHK apartments with a host of luxurious amenities. So visit the property today and experience Sobha\'s renowned quality and luxury in Electronic City.', 'Designer landscaping.\r\nWater bodies with fountain\r\nRock Garden\r\nForest Area\r\nPalm Grove Park\r\nGrand tropical style water cascade\r\nSTP - Sewage treatment plant\r\nWater Treatment plant\r\nOrganic waste converter\r\nVisitor Car parks', NULL, NULL, 'Off Hosa Road, Near Electronic City', '', ''),
(63, 'Sobha Eternia', NULL, 1, NULL, 15, NULL, 898, 107, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '6.9', '', '3BR 1879 Sq.ft to 2289 Sq.ft', 'BDA', '', '2012-09-18', '2016-07-01', NULL, 1, 0, '', '', NULL, NULL, 'Off Sarjapur Road', 'Sarjapura is a town situated in Bangalore, Karnataka, India. It is a hobli of Anekal taluk, Bangalore Urban district and is located towards the south-east of Bangalore. Sarjapur is fast-developing part of Bangalore with good road connectivity to key IT cluster areas like Whitefield (15 km), Electronic City (19 km), Outer Ring Road (16 km), Marathahalli and Koramangala. IT major, Infosys has acquired 202 acres of land in Sarjapura to set up an IT SEZ. In October 2013 Azim Premji Foundation announced to acquire 50 acres of land in Sarjapura for a world class university.', ''),
(64, 'Brigade Opus', NULL, 11, NULL, NULL, NULL, 900, NULL, 'enquiry@brigadegroup.com', NULL, '', '12.9651473', '77.57914200000005', 'Brigade Enterprises Limited', '', 'Brigade Enterprises Limited', 1, 'Upcoming project', '4,00,000 sft (Approx.)', '', '', '', '', '1970-01-01', '1970-01-01', NULL, 0, 0, 'This project is located strategically on NH-7, Hebbal and is well connected to Central Business District and rest of the Bangalore. This is a total development of around 4,00,000 sft of office space with Grade A certification and LEED Gold pre-certified. The project is available on Sale and Lease. \r\nBuilt to Suit options are available.\r\n', '', NULL, NULL, 'Hebbal', 'Columbia Asia Hospital?0.5 kms\r\nEsteem Mall?0.75 Kms\r\nHebbal Lake?2 Kms\r\nBaptist Hospital?3 Kms\r\nPresidency College?4 Kms\r\nVidya Niketan School?5 Kms\r\nMovenpick?6 Kms\r\nHoward Johnson?6 Kms', ''),
(65, 'Sobha Palladian', NULL, 1, NULL, 6, NULL, 902, 179, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '5.25', '', '3BR 1901-2666 Sq.ft / 4BR 2722-3298 Sq.ft', 'BBMP,BDA', '', '2013-08-24', '2017-07-01', NULL, 0, 0, '', '', NULL, NULL, 'New Indiranagar', 'Indiranagar was formed as a BDA layout in the early 1980s, and was named after the former Prime Minister of India Indira Gandhi. In the beginning, the locality was primarily a residential area. The area was dotted with large bungalows and independent houses, mostly owned by defence personnel. The Information Technology boom in Bangalore in the late 1990s converted Indiranagar and its two arterial roads(100 Feet Road and Chinmaya Mission Hospital Road) into a commercial area. Today, Indiranagar is an important residential and commercial hub of Bangalore.\r\nIndiranagar is divided into 2 stages, with the 1st stage being the largest. Unlike the southern and western areas of Bangalore which have a predominantly Kannada speaking population or the northern areas of Bangalore which have large populations of Muslims and Tamilians, Indiranagar is a cosmopolitan locality with people from all parts of India living in harmony with each other.', ''),
(66, 'Sobha Clovelly', NULL, 1, NULL, NULL, NULL, 904, 137, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '3.25', '', '', 'BBMP', '', '2015-08-01', '2019-09-01', NULL, 1, 0, 'Sobha Clovelly introduces the boutique living concept to Bangalore whereby each resident benefits from being part of a contemporary, stylish and exclusive development, strategically positioned within Bangalore\'s blue ribbon area.\r\n\r\nSobha Clovelly stands tall along a geographical ridge and thus capitalizes on the beautiful vistas of the Bangalore skyline. These vistas are then drawn into each living, dining and bedroom through large windows, energising internal spaces and inspiring the mind.', '', NULL, 2, 'Banashankari', 'Banashankari, abbreviated as BSK, is a neighbourhood near to South Bangalore. It gets its name from the Banashankari Amma Temple on Kanakapura Road, one of Bangalore\'s oldest and well known temples, which was constructed by Subramanya Shetty in 1915.\r\n\r\nBanashankari is the largest locality in Bangalore, extending all the way from Mysore Road to Kanakapura Road. It is bound by Girinagar and Rajarajeshwari Nagar in the west, Basavangudi in the north, Jayanagar and J.P. Nagar in the east, and Padmanabhanagar, Kumaraswamy Layout, ISRO Layout, Vasanthapura and Uttarahalli in the south', ''),
(67, 'The Park & The Plaza', NULL, 1, NULL, 18, NULL, 906, NULL, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', '', 1, 'Ongoing project', '10.76', '', '2BR 1296 to 1328 Sq. ft / 3BR 1592 to 1703 Sq. ft', '', '', '2015-02-21', '1970-01-01', NULL, 1, 0, 'The premium luxury apartment brings you countless opportunities to expect more from life. It lets you stretch and reach out for all those experiences that you have always longed for. 80% of lush green open space, a 50 mtr. lap pool and a picturesque lake promenade are just some of the few amenities that redefines your concept of fine living. Come and explore beyond your expectations at the Park and the Plaza.\r\n\r\nSobha Limited (formerly Sobha Developers Ltd.) offers premium luxury homes located on the scenic Kanakapura main road. The Park and The Plaza are spread over sprawling 10.7 acres of green space, presents 2 & 3 BHK premium luxury apartments with a host of world-class amenities. Come and visit the property today and experience Sobha\'s renowned quality and luxury in Kanakapura main road.', NULL, NULL, NULL, 'off Kanakapura Road', 'Proposed Metro Station?1 Kms\r\nMarriot Hotel?1.5 Kms\r\nSatya Sai Hospital?1.5 Kms\r\nInorbit Mall?1.5 Kms\r\nAloft Hotel?2 Kms\r\nITPB?2.5 Kms\r\nTaj Vivanta?2.5 Kms\r\nOuter Ring road?5 Kms', ''),
(68, 'Sobha Valley View', NULL, 1, NULL, 13, NULL, 908, 311, 'marketing@sobha.com', NULL, '', '12.9715987', '77.59456269999998', 'sobha Limited', '', 'Sobha Limited', 1, 'Ongoing project', '4.66', '', '2BR 1304 to 1434 Sq.ft / 3BR 1612 to 1947 Sq.ft', '', '', '2014-06-06', '2018-03-01', NULL, 0, 0, 'It is said that destiny decides where you live.\r\n\r\nOne look at Sobha Valley View will establish this fact. This iconic project with 2 and 3 BHK apartments in Banashankari 3rd Stage is a truly unique offering not just because of its strategic location and a setting that is idyllic, but also because everything here is practically accessible within a traffic free drive. What\'s more, this project abounds with exceptional features and is blessed with abundant greenery spread around homes.\r\n\r\nWith features like a Juliet window to gaze from, a winding bicycling track and a jogging track to set the heart racing, beautiful French gardens with antique park benches that reminds one of Europe, a charming Fountain in the Plaza, gorgeous Gazebos spread around the project, a Cascading pool with water spouts, lobbies done in the Baroque, Rococo and Neo Classical styles and more, Sobha Valley View seems to have been designed to give a new definition to Community Living, that epitomizes life at its best.\r\n\r\nToday with the city growing in an unplanned manner, people living in independent homes are forced to go indoors and shut out the world. At Sobha, we wanted to give people a feeling that was completely different and special . This is why Sobha Valley View is designed with such remarkable amenities around the home that draw residents outside and inspires the true bonding that is so essential for leading a joyous and fulfilling life.\r\n\r\nThis is one project where you will surely discover the joy of living and experience serendipity at its best.', NULL, NULL, NULL, 'Near PES University, Banshankari 3rd Stage', 'Banashankari, abbreviated as BSK, is a neighbourhood near to South Bangalore. It gets its name from the Banashankari Amma Temple on Kanakapura Road, one of Bangalore\'s oldest and well known temples, which was constructed by Subramanya Shetty in 1915.\r\n\r\nBanashankari is the largest locality in Bangalore, extending all the way from Mysore Road to Kanakapura Road. It is bound by Girinagar and Rajarajeshwari Nagar in the west, Basavangudi in the north, Jayanagar and J.P. Nagar in the east, and Padmanabhanagar, Kumaraswamy Layout, ISRO Layout, Vasanthapura and Uttarahalli in the south.', ''),
(69, 'Brahma villa apartment', NULL, 1, NULL, NULL, NULL, 914, NULL, '', NULL, '', '13.025556178240322', '77.50760078430176', 'bulider', '', 'Brahma Holdings', 0, 'Upcoming project', '', '', '', '', 'ICICI Bank', '1970-01-01', '1970-01-01', NULL, 1, 0, '', NULL, NULL, NULL, '', '', ''),
(70, 'b1', 103, 2, NULL, NULL, NULL, 917, NULL, '', NULL, '', '12.9715987', '77.59456269999998', '', '', 'Brahma Holdings', 0, 'Upcoming project', '', '', '', '', 'IDBI BanK,Indian Bank,IndusInd Bank,ING Vysya Bank,Jammu and Kashmir Bank,State Bank of Mysore', '1970-01-01', '1970-01-01', NULL, 0, 0, '', NULL, NULL, NULL, '', '', ''),
(71, 'hb1', NULL, 1, NULL, NULL, NULL, 919, NULL, '', NULL, '', '12.9715987', '77.59456269999998', '', '', 'Hexa Builders', 1, 'Upcoming project', '', '100-200 lacs', '', '', 'Allahabad Bank', '1970-01-01', '1970-01-01', NULL, 0, 0, '', NULL, NULL, NULL, '', '', ''),
(72, 'Brigade Altamont', 116, 1, NULL, NULL, NULL, 926, 189, 'inharge@email.com', NULL, '', '12.9651473', '77.57914200000005', 'incharge', 'incharge designation', 'Brigade Enterprises Limited', 1, 'Completed project', '369', '585', '569', 'BDA', 'City Union Bank,Dhanlaxmi Bank,IDBI BanK', '2016-03-04', '2016-03-05', NULL, 1, 0, 'Brigade Altamont is the perfect example of how form and function complement each other to accommodate all the necessities of modern living, while ensuring the efficient use of space.\r\n\r\nEvery living space at Brigade Altamont is designed to make you fall in love with a new quality of life. The layout of each home combined with unique structure and attention to detail, natural ventilation and sunlight makes each home a pleasure. \r\n\r\nA home at Brigade Altamont is where you can relax, find comfort and experience the joys of finer living. At Brigade Altamont everyone can enjoy the advantages of finer living. With a range of lifestyle amenities every individual has the option to unwind and rejuvenate.', NULL, NULL, 5, 'Off Hennur Road', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `property_accessibilities`
--

CREATE TABLE `property_accessibilities` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'Id',
  `accessibility` varchar(100) NOT NULL COMMENT 'Accessibility',
  `remaks` text NOT NULL COMMENT 'Remarks',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On',
  `property_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_amenities`
--

CREATE TABLE `property_amenities` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `amenity` varchar(100) NOT NULL COMMENT 'Amenity',
  `property_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_amenities`
--

INSERT INTO `property_amenities` (`id`, `amenity`, `property_id`, `created_on`) VALUES
(1, 'GYM', 16, '2015-10-20 01:00:17'),
(2, 'IP TV Cable', 16, '2015-10-20 01:00:17'),
(3, 'Swimming Pool', 16, '2015-10-20 01:00:17'),
(4, 'GYM', 17, '2015-10-22 13:25:51'),
(5, 'IP TV Cable', 17, '2015-10-22 13:25:51'),
(6, 'Swimming Pool', 17, '2015-10-22 13:25:51'),
(7, 'Basket Ball Court', 18, '2015-10-26 00:36:24'),
(8, 'Cafeteria / Food Court ', 18, '2015-10-26 00:36:24'),
(9, 'Club House', 18, '2015-10-26 00:36:24'),
(10, 'Convention Center ', 18, '2015-10-26 00:36:24'),
(11, 'Cycling Track', 18, '2015-10-26 00:36:24'),
(12, 'Fitness Center', 18, '2015-10-26 00:36:24'),
(13, 'Garden', 18, '2015-10-26 00:36:24'),
(14, 'GYM', 18, '2015-10-26 00:36:24'),
(15, 'Basketball Court', 18, '2015-10-26 00:36:24'),
(16, 'Internet / Wi-Fi', 18, '2015-10-26 00:36:24'),
(17, 'IP TV Cable', 18, '2015-10-26 00:36:24'),
(18, 'Library', 18, '2015-10-26 00:36:24'),
(19, 'Park', 18, '2015-10-26 00:36:24'),
(20, 'Power Backup', 18, '2015-10-26 00:36:24'),
(21, 'Rain Water Harvesting', 18, '2015-10-26 00:36:24'),
(22, 'Security', 18, '2015-10-26 00:36:24'),
(23, 'SPA', 18, '2015-10-26 00:36:24'),
(24, 'Swimming Pool', 18, '2015-10-26 00:36:24'),
(25, 'Tennis court', 18, '2015-10-26 00:36:24'),
(26, 'Basket Ball Court', 17, '2015-10-26 01:06:01'),
(27, 'Cafeteria / Food Court ', 17, '2015-10-26 01:06:01'),
(28, 'Club House', 17, '2015-10-26 01:06:01'),
(29, 'Convention Center ', 17, '2015-10-26 01:06:01'),
(30, 'Fitness Center', 17, '2015-10-26 01:06:01'),
(31, 'Garden', 17, '2015-10-26 01:06:01'),
(32, 'Basketball Court', 17, '2015-10-26 01:06:01'),
(33, 'Internet / Wi-Fi', 17, '2015-10-26 01:06:01'),
(34, 'Library', 17, '2015-10-26 01:06:01'),
(35, 'Park', 17, '2015-10-26 01:06:01'),
(36, 'Power Backup', 17, '2015-10-26 01:06:01'),
(37, 'Rain Water Harvesting', 17, '2015-10-26 01:06:01'),
(38, 'Security', 17, '2015-10-26 01:06:01'),
(39, 'SPA', 17, '2015-10-26 01:06:01'),
(40, 'Tennis court', 17, '2015-10-26 01:06:01'),
(41, 'Basket Ball Court', 19, '2015-10-26 01:24:48'),
(42, 'Cafeteria / Food Court ', 19, '2015-10-26 01:24:48'),
(43, 'Club House', 19, '2015-10-26 01:24:48'),
(44, 'Convention Center ', 19, '2015-10-26 01:24:48'),
(45, 'Cycling Track', 19, '2015-10-26 01:24:48'),
(46, 'Fitness Center', 19, '2015-10-26 01:24:48'),
(47, 'Garden', 19, '2015-10-26 01:24:48'),
(48, 'GYM', 19, '2015-10-26 01:24:48'),
(49, 'Basketball Court', 19, '2015-10-26 01:24:48'),
(50, 'IP TV Cable', 19, '2015-10-26 01:24:48'),
(51, 'Library', 19, '2015-10-26 01:24:48'),
(52, 'Park', 19, '2015-10-26 01:24:48'),
(53, 'Power Backup', 19, '2015-10-26 01:24:48'),
(54, 'Rain Water Harvesting', 19, '2015-10-26 01:24:48'),
(55, 'Security', 19, '2015-10-26 01:24:48'),
(56, 'SPA', 19, '2015-10-26 01:24:48'),
(57, 'Swimming Pool', 19, '2015-10-26 01:24:48'),
(58, 'Tennis court', 19, '2015-10-26 01:24:48'),
(61, 'Basket Ball Court', 22, '2016-01-04 02:50:10'),
(64, 'Children\'s Play Area', 22, '2016-01-04 02:50:10'),
(65, 'Club House', 22, '2016-01-04 02:50:10'),
(71, 'Gymnasium', 22, '2016-01-04 02:50:10'),
(72, 'Basketball Court', 22, '2016-01-04 02:50:10'),
(87, 'Swimming Pool', 22, '2016-01-04 02:50:10'),
(89, 'Tennis court', 22, '2016-01-04 02:50:10'),
(93, 'Club House', 24, '2016-01-11 06:05:53'),
(94, 'Garden', 24, '2016-01-11 06:05:53'),
(95, 'Squash Court', 24, '2016-01-11 06:05:53'),
(96, 'Swimming Pool', 24, '2016-01-11 06:05:53'),
(97, 'Table Tennis', 24, '2016-01-11 06:05:53'),
(98, 'Tennis court', 24, '2016-01-11 06:05:53'),
(99, 'Terrace Garden', 24, '2016-01-11 06:05:53'),
(100, 'Fitness Center', 23, '2016-01-12 00:18:02'),
(101, 'Swimming Pool', 23, '2016-01-12 00:18:02'),
(102, 'Club House', 27, '2016-01-22 02:48:38'),
(103, 'Gymnasium', 27, '2016-01-22 02:48:38'),
(104, 'Jogging Track', 27, '2016-01-22 02:48:38'),
(105, 'Aerobics/Yoga Room', 29, '2016-01-22 06:47:53'),
(106, 'Children\'s Play Area', 29, '2016-01-22 06:47:53'),
(107, 'Gymnasium', 29, '2016-01-22 06:47:53'),
(108, 'Meditation Area', 29, '2016-01-22 06:47:53'),
(109, 'Swimming Pool', 29, '2016-01-22 06:47:53'),
(110, 'Amphitheatre', 30, '2016-01-23 05:02:36'),
(111, 'Children\'s Play Area', 30, '2016-01-23 05:02:36'),
(112, 'Gymnasium', 30, '2016-01-23 05:02:36'),
(113, 'Meditation Area', 30, '2016-01-23 05:02:36'),
(114, 'Swimming Pool', 30, '2016-01-23 05:02:36'),
(115, 'Children\'s Play Area', 32, '2016-01-23 05:12:32'),
(116, 'Club House', 32, '2016-01-23 05:12:32'),
(117, 'Swimming Pool', 32, '2016-01-23 05:12:32'),
(118, 'Tennis court', 32, '2016-01-23 05:12:32'),
(119, 'Amphitheatre', 33, '2016-01-23 05:15:02'),
(120, 'Club House', 33, '2016-01-23 05:15:02'),
(121, 'Gymnasium', 33, '2016-01-23 05:15:02'),
(122, 'Meditation Area', 33, '2016-01-23 05:15:02'),
(123, 'Swimming Pool', 33, '2016-01-23 05:15:02'),
(124, 'Children\'s Play Area', 34, '2016-01-23 05:18:54'),
(125, 'Club House', 34, '2016-01-23 05:18:54'),
(126, 'Jogging Track', 34, '2016-01-23 05:18:54'),
(127, 'Park', 34, '2016-01-23 05:18:54'),
(128, 'Children\'s Play Area', 35, '2016-01-23 05:21:40'),
(129, 'Park', 35, '2016-01-23 05:21:40'),
(130, 'Swimming Pool', 35, '2016-01-23 05:21:40'),
(131, 'Club House', 28, '2016-01-23 05:24:11'),
(132, 'Gymnasium', 28, '2016-01-23 05:24:11'),
(133, 'Swimming Pool', 28, '2016-01-23 05:24:11'),
(134, 'Arts Village', 31, '2016-01-23 05:28:25'),
(135, 'Signature Club', 31, '2016-01-23 05:28:25'),
(136, 'Social Infrastructure', 31, '2016-01-23 05:28:25'),
(137, 'Sports Area', 31, '2016-01-23 05:28:25'),
(138, 'Commercial Centre', 35, '2016-01-23 05:31:59'),
(139, 'Basketball Court', 35, '2016-01-23 05:31:59'),
(140, 'Landscaped Garden', 35, '2016-01-23 05:31:59'),
(141, 'Shopping Mall', 35, '2016-01-23 05:31:59'),
(142, 'Club House', 35, '2016-01-23 05:32:16'),
(143, 'Gymnasium', 35, '2016-01-23 05:32:16'),
(144, 'Basketball Court', 36, '2016-01-23 05:40:54'),
(145, 'Arts Village', 37, '2016-01-23 06:01:57'),
(146, 'Social Infrastructure', 37, '2016-01-23 06:01:57'),
(147, 'Gymnasium', 38, '2016-01-23 06:32:11'),
(148, 'Basketball Court', 38, '2016-01-23 06:32:11'),
(149, 'Rooftop swimming pool', 38, '2016-01-23 06:32:11'),
(150, 'Children\'s Play Area', 58, '2016-01-25 03:54:58'),
(151, 'Gymnasium', 58, '2016-01-25 03:54:58'),
(152, 'Basketball Court', 58, '2016-01-25 03:54:58'),
(153, 'Squash Court', 58, '2016-01-25 03:54:58'),
(154, 'Table Tennis', 58, '2016-01-25 03:54:58'),
(155, 'Children\'s Play Area', 61, '2016-01-25 05:17:36'),
(156, 'Club House', 61, '2016-01-25 05:17:36'),
(157, 'Jogging Track', 61, '2016-01-25 05:17:36'),
(158, 'Swimming Pool', 61, '2016-01-25 05:17:36'),
(159, 'Tennis court', 61, '2016-01-25 05:17:36'),
(160, 'Jogging Track', 62, '2016-01-25 05:48:36'),
(161, 'Swimming Pool', 62, '2016-01-25 05:48:36'),
(162, 'Tennis court', 62, '2016-01-25 05:48:36'),
(165, 'Gymnasium', 65, '2016-01-29 02:51:43'),
(166, 'Squash Court', 65, '2016-01-29 02:51:43'),
(167, 'Table Tennis', 65, '2016-01-29 02:51:43'),
(168, 'Children\'s Play Area', 65, '2016-01-29 02:52:04'),
(169, 'Jogging Track', 65, '2016-01-29 02:52:04'),
(170, 'Swimming Pool', 65, '2016-01-29 02:52:04'),
(171, 'Badminton Hall', 66, '2016-01-29 03:10:26'),
(172, 'Barbeque Area', 66, '2016-01-29 03:10:26'),
(173, 'Kid\'s Pool', 66, '2016-01-29 03:10:26'),
(174, 'Senior Citizen\'s  Area', 66, '2016-01-29 03:10:26'),
(175, 'Swimming Pool', 66, '2016-01-29 03:10:26'),
(176, 'Table Tennis', 66, '2016-01-29 03:10:26'),
(177, 'Tennis court', 66, '2016-01-29 03:10:26'),
(178, 'Yogo Room', 66, '2016-01-29 03:10:26'),
(179, 'Swimming Pool', 67, '2016-01-30 00:04:42'),
(180, 'Children\'s Play Area', 68, '2016-01-30 02:29:43'),
(181, 'GYM', 68, '2016-01-30 02:29:43'),
(182, 'Jogging Track', 68, '2016-01-30 02:29:43'),
(183, 'Swimming Pool', 68, '2016-01-30 02:29:43'),
(184, 'Basket Ball Court', 63, '2016-01-30 04:40:24'),
(185, 'Children\'s Play Area', 63, '2016-01-30 04:40:24'),
(186, 'Gymnasium', 63, '2016-01-30 04:40:24'),
(187, 'Swimming Pool', 63, '2016-01-30 04:40:24'),
(188, 'Table Tennis', 63, '2016-01-30 04:40:24'),
(189, 'Children\'s Play Area', 21, '2016-01-30 05:13:31'),
(190, 'GYM', 21, '2016-01-30 05:13:31'),
(191, 'Jogging Track', 21, '2016-01-30 05:13:31'),
(192, 'Squash Court', 21, '2016-01-30 05:13:31'),
(193, 'Swimming Pool', 21, '2016-01-30 05:13:31'),
(194, 'Aerobics/Yoga Room', 15, '2016-03-09 00:54:36'),
(195, 'Amphitheatre', 15, '2016-03-09 00:54:36'),
(196, 'ATM', 15, '2016-03-09 00:54:36'),
(197, 'Basket Ball Court', 15, '2016-03-09 00:54:36'),
(198, 'Basketball Court', 15, '2016-03-09 00:54:36'),
(199, 'Billiards Table', 15, '2016-03-09 00:54:36'),
(200, 'Cafeteria / Food Court ', 15, '2016-03-09 00:54:36'),
(201, 'Convention Center ', 15, '2016-03-09 00:54:36'),
(202, 'Cycling Track', 15, '2016-03-09 00:54:36'),
(203, 'Fitness Center', 15, '2016-03-09 00:54:36'),
(204, 'Garden', 15, '2016-03-09 00:54:36'),
(205, 'Hospital', 15, '2016-03-09 00:54:36'),
(206, 'Indoor Games Area', 15, '2016-03-09 00:54:36'),
(207, 'Internet / Wi-Fi', 15, '2016-03-09 00:54:36'),
(208, 'Kid\'s Pool', 15, '2016-03-09 00:54:36'),
(209, 'Lift Service', 15, '2016-03-09 00:54:36'),
(210, 'Lounge Bar', 15, '2016-03-09 00:54:36'),
(211, 'Meditation Area', 15, '2016-03-09 00:54:36'),
(212, 'Park', 15, '2016-03-09 00:54:36'),
(213, 'Rain Water Harvesting', 15, '2016-03-09 00:54:36'),
(214, 'Rooftop swimming pool', 15, '2016-03-09 00:54:36'),
(215, 'Security', 15, '2016-03-09 00:54:36'),
(216, 'Social Infrastructure', 15, '2016-03-09 00:54:36');

-- --------------------------------------------------------

--
-- Table structure for table `property_facilities`
--

CREATE TABLE `property_facilities` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `amenity` varchar(100) NOT NULL COMMENT 'Amenity',
  `property_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_group`
--

CREATE TABLE `property_group` (
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Is Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_group`
--

INSERT INTO `property_group` (`name`, `is_active`) VALUES
('Commercial', 1),
('Residential', 1),
('Retail', 1);

-- --------------------------------------------------------

--
-- Table structure for table `property_partition_type`
--

CREATE TABLE `property_partition_type` (
  `partition_type` varchar(100) NOT NULL COMMENT 'Partition Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `category` varchar(200) DEFAULT NULL COMMENT 'Category',
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_partition_type`
--

INSERT INTO `property_partition_type` (`partition_type`, `is_active`, `category`, `icon`) VALUES
('Attic', 1, NULL, 'default-image.jpg'),
('Balcony', 1, NULL, NULL),
('Bathroom', 1, NULL, NULL),
('Bedroom', 1, NULL, '01-TM-property-for-rent-listing-V2-filters-1.jpg'),
('Dining Room', 1, NULL, NULL),
('Entrance', 1, NULL, NULL),
('Guest Room', 1, NULL, NULL),
('Kitchen', 1, NULL, NULL),
('Living Room', 1, NULL, NULL),
('Pooja Room', 1, NULL, NULL),
('Portico', 1, NULL, NULL),
('Servant Room', 1, NULL, NULL),
('Store Room', 1, NULL, NULL),
('Study Room', 1, NULL, NULL),
('Terrace', 1, NULL, NULL),
('Varandha', 1, NULL, '02-Home-Page-V2-search-prop-type-dropdown.jpg'),
('Visitors Area', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE `property_type` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `group_id` varchar(100) NOT NULL COMMENT 'Group Id',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `is_active` int(11) NOT NULL COMMENT 'Is Active??'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`id`, `group_id`, `name`, `is_active`) VALUES
(1, 'Residential', 'Apartment', 1),
(2, 'Residential', 'House', 1),
(3, 'Residential', 'Villa', 1),
(4, 'Residential', 'Row House', 1),
(5, 'Residential', 'Guest House', 1),
(6, 'Commercial', 'Sheds', 1),
(7, 'Commercial', 'Godowns', 1),
(8, 'Commercial', 'Shops', 1),
(9, 'Commercial', 'Complex', 1),
(10, 'Commercial', 'Malls', 1),
(11, 'Commercial', 'Offices', 1),
(12, 'Commercial', 'SEZ', 1),
(13, 'Commercial', 'IT Park', 1),
(14, 'Commercial', 'Plot/Land', 1),
(15, 'Residential', 'Plot/Land', 1);

-- --------------------------------------------------------

--
-- Table structure for table `property_units`
--

CREATE TABLE `property_units` (
  `name` varchar(150) NOT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `id` bigint(11) UNSIGNED NOT NULL COMMENT 'Id',
  `property_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Id',
  `floor_number` int(11) NOT NULL COMMENT 'Floor Number',
  `block` varchar(100) DEFAULT NULL COMMENT 'Blocks',
  `flat_no` varchar(10) NOT NULL COMMENT 'Flat Number',
  `super_builtup_area` decimal(10,0) DEFAULT NULL COMMENT 'Super Built Up Area',
  `carepet_area` decimal(10,0) DEFAULT NULL COMMENT 'Carpet Area',
  `no_balconies` int(11) NOT NULL DEFAULT '0' COMMENT 'No. of Balconies',
  `no_bathrooms` int(11) NOT NULL DEFAULT '0' COMMENT 'No. of Bathrooms',
  `no_bedrooms` int(11) NOT NULL DEFAULT '0' COMMENT 'No. of Bedrooms',
  `no_servant_room` int(11) NOT NULL DEFAULT '0' COMMENT 'No. of Servant Room',
  `furnished_status` varchar(50) DEFAULT NULL COMMENT 'Furnished Status',
  `door_facing` varchar(100) DEFAULT NULL COMMENT 'Door Facing Direction',
  `two_wheeler_parking_id` varchar(11) DEFAULT NULL COMMENT 'Two Wheeler Parking Id',
  `Four_wheeler_parking_id` varchar(11) DEFAULT NULL COMMENT 'Four Wheeler Parking Id',
  `has_visitor_parking` int(11) NOT NULL DEFAULT '0' COMMENT 'Has Visitors Parking?',
  `are_pets_allowed` int(11) NOT NULL DEFAULT '0' COMMENT 'Are Pets Allowed?',
  `other_terms` text COMMENT 'Other Terms',
  `agreement_file_path` varchar(255) DEFAULT NULL COMMENT 'Agreement File Path',
  `agreement_type` varchar(200) DEFAULT NULL COMMENT 'Agreement Type',
  `prefered_tenant_type` varchar(100) DEFAULT NULL COMMENT 'Preferred Tenant Type',
  `prefered_food_habits` varchar(100) DEFAULT NULL COMMENT 'Preferred Food Habits',
  `tenant_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Tenant',
  `executive_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Executive',
  `builtup_area` decimal(10,0) DEFAULT NULL COMMENT 'Built-up area',
  `image` varchar(255) DEFAULT NULL COMMENT 'Property Image',
  `availability_date` date DEFAULT NULL COMMENT 'Availability Date',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `starting_price` int(11) NOT NULL DEFAULT '0',
  `available_units` int(11) NOT NULL DEFAULT '1',
  `for_sale` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_units`
--

INSERT INTO `property_units` (`name`, `description`, `id`, `property_id`, `floor_number`, `block`, `flat_no`, `super_builtup_area`, `carepet_area`, `no_balconies`, `no_bathrooms`, `no_bedrooms`, `no_servant_room`, `furnished_status`, `door_facing`, `two_wheeler_parking_id`, `Four_wheeler_parking_id`, `has_visitor_parking`, `are_pets_allowed`, `other_terms`, `agreement_file_path`, `agreement_type`, `prefered_tenant_type`, `prefered_food_habits`, `tenant_id`, `executive_id`, `builtup_area`, `image`, `availability_date`, `is_active`, `starting_price`, `available_units`, `for_sale`) VALUES
('VHP-PROP00001', NULL, 19, 15, 13, 'D-Block', '1304', '1520', '1200', 2, 2, 1, 1, 'Furnished', 'West', NULL, NULL, 1, 0, NULL, NULL, NULL, 'Family', 'Vegetarian', NULL, NULL, '1310', NULL, '2016-03-25', 1, 0, 1, 0),
('VHP-PROP00002', NULL, 20, 16, 5, 'A Block', '1504', '1600', NULL, 1, 2, 2, 1, 'Furnished', 'North', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1200', NULL, '2016-03-26', 1, 0, 1, 0),
('VHP-PROP00003', NULL, 21, 18, 10, 'C Block', 'C1506', '1800', '1350', 1, 1, 1, 1, 'Furnished', 'North', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1600', NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00004', NULL, 22, 19, 9, 'A Block', 'A1204', '1500', '1800', 1, 1, 1, 1, 'Furnished', 'East', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1100', NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00005', NULL, 23, 21, 1, '', '1', NULL, NULL, 0, 1, 1, 0, 'Semi-Furnished', '', NULL, NULL, 0, 0, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00006', NULL, 24, 22, 1, '', '1', NULL, '1260', 1, 2, 2, 0, 'Un-Furnished', 'North', NULL, NULL, 1, 0, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1260', NULL, NULL, 1, 0, 189, 0),
('VHP-PROP00007', NULL, 25, 22, 1, '', '1', NULL, '1670', 1, 1, 3, 0, 'Un-Furnished', 'North', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1670', NULL, NULL, 1, 0, 0, 0),
('VHP-PROP00008', NULL, 26, 22, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00009', NULL, 27, 23, 1, 'Block A', '1', NULL, NULL, 0, 1, 3, 0, '', '', NULL, NULL, 1, 0, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0),
('VHP-PROP00010', NULL, 28, 23, 1, '', '1', '2000', '1800', 0, 1, 4, 0, 'Un-Furnished', 'North', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, '1700', 'Error-bad request635999250.jpg', NULL, 1, 0, 1, 0),
('VHP-PROP00011', NULL, 29, 24, 1, 'A', '1', NULL, NULL, 0, 1, 2, 0, 'Un-Furnished', '', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Family', 'Non-Vegetarian', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00012', NULL, 30, 24, 1, 'B', '1', NULL, NULL, 0, 1, 3, 0, '', '', NULL, NULL, 0, 0, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00013', NULL, 31, 24, 1, 'c', '1', NULL, NULL, 0, 1, 4, 0, '', '', NULL, NULL, 0, 0, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00014', NULL, 32, 25, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00015', NULL, 33, 25, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00016', NULL, 34, 26, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00017', NULL, 35, 27, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00018', NULL, 36, 27, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00019', NULL, 37, 28, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00020', NULL, 38, 28, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00021', NULL, 39, 28, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00022', NULL, 40, 29, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00023', NULL, 41, 29, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00024', NULL, 42, 30, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00025', NULL, 43, 30, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00026', NULL, 44, 30, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00027', NULL, 45, 31, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00028', NULL, 46, 31, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00029', NULL, 47, 32, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00030', NULL, 48, 32, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00031', NULL, 49, 32, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00032', NULL, 50, 33, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00033', NULL, 51, 33, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00034', NULL, 52, 34, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00035', NULL, 53, 34, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00036', NULL, 54, 35, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00037', NULL, 55, 35, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00038', NULL, 56, 35, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00039', NULL, 57, 36, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00040', NULL, 58, 37, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00041', NULL, 59, 38, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00042', NULL, 60, 40, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00043', NULL, 61, 40, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00044', NULL, 62, 41, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00045', NULL, 63, 41, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00046', NULL, 64, 41, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00047', NULL, 65, 42, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00048', NULL, 66, 42, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00049', NULL, 67, 43, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00050', NULL, 68, 43, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00051', NULL, 69, 43, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00052', NULL, 70, 43, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00053', NULL, 71, 58, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00054', NULL, 72, 58, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00055', NULL, 73, 59, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00056', NULL, 74, 59, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00057', NULL, 75, 59, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00058', NULL, 76, 60, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00059', NULL, 77, 62, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00060', NULL, 78, 62, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00061', NULL, 79, 63, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00062', NULL, 80, 63, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00063', NULL, 81, 63, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00064', NULL, 82, 65, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00065', NULL, 83, 65, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00066', NULL, 84, 66, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00067', NULL, 85, 66, 1, NULL, '1', NULL, NULL, 0, 1, 4, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00068', NULL, 86, 67, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00069', NULL, 87, 67, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00070', NULL, 88, 68, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00071', NULL, 89, 68, 1, NULL, '1', NULL, NULL, 0, 1, 3, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00072', NULL, 90, 71, 1, NULL, '1', NULL, NULL, 0, 1, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00073', NULL, 91, 71, 1, NULL, '1', NULL, NULL, 0, 1, 2, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0),
('VHP-PROP00074', NULL, 92, 72, 1, '', '6', '54556', '45', 2, 1, 1, 0, 'Furnished', 'North', NULL, NULL, 1, 1, NULL, NULL, NULL, 'Any', 'Non-Vegetarian', NULL, NULL, '545', NULL, '2016-03-05', 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prop_area_partition`
--

CREATE TABLE `prop_area_partition` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `punit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit',
  `area_partition_type` varchar(100) NOT NULL COMMENT 'Area Partition Type',
  `area` decimal(10,2) NOT NULL COMMENT 'Area in sqmtr',
  `remarks` text COMMENT 'Remarks',
  `name` varchar(200) NOT NULL COMMENT 'Name',
  `image` text COMMENT 'Photo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_area_partition`
--

INSERT INTO `prop_area_partition` (`id`, `punit_id`, `area_partition_type`, `area`, `remarks`, `name`, `image`) VALUES
(1, 21, 'Bathroom', '120.00', '', 'Bathroom', NULL),
(2, 21, 'Bedroom', '120.00', '', 'Master Bedroom', NULL),
(3, 21, 'Dining Room', '150.00', '', 'Hall', NULL),
(5, 21, 'Entrance', '100.00', '', 'Room', NULL),
(6, 21, 'Living Room', '150.00', '', 'room', NULL),
(7, 22, 'Bathroom', '100.00', '', 'Western', NULL),
(8, 22, 'Bathroom', '200.00', '', 'Master Room', NULL),
(9, 22, 'Dining Room', '180.00', '', 'Hall', NULL),
(10, 22, 'Entrance', '180.00', '', 'Visiter Room', NULL),
(11, 22, 'Kitchen', '150.00', '', 'Kitchen', NULL),
(13, 24, 'Entrance', '2.68', '', 'Lobby', NULL),
(14, 23, 'Dining Room', '120.00', '', 'Dining Room', NULL),
(15, 23, 'Entrance', '100.00', '', 'Entrance', NULL),
(16, 23, 'Bathroom', '90.00', '', 'bathroom', NULL),
(17, 28, 'Bedroom', '120.00', '', 'Master Bedroom', NULL),
(18, 28, 'Kitchen', '100.00', '', 'Kitchen', NULL),
(19, 28, 'Living Room', '200.00', '', 'Living Room', NULL),
(20, 28, 'Entrance', '300.00', '', 'Entrance', NULL),
(21, 28, 'Balcony', '120.00', '', 'Balcony', NULL),
(22, 24, 'Living Room', '8.95', '', 'Living', NULL),
(23, 24, 'Dining Room', '3.05', '', 'Dining', NULL),
(24, 24, 'Kitchen', '5.15', '', 'Kitchen', NULL),
(25, 24, 'Bedroom', '13.29', '', 'Bedroom', NULL),
(26, 24, 'Bathroom', '4.16', '', 'Common', NULL),
(27, 24, 'Bedroom', '15.61', '', 'Master Bedroom', NULL),
(28, 24, 'Bathroom', '4.16', '', 'Master Bathroom', NULL),
(29, 25, 'Entrance', '2.79', '', 'Lobby', NULL),
(30, 25, 'Living Room', '19.40', '', 'Living', NULL),
(31, 25, 'Dining Room', '18.43', '', 'Dining', NULL),
(32, 25, 'Kitchen', '7.43', '', 'Kitchen', NULL),
(33, 25, 'Bedroom', '12.80', '', 'Bedroom', NULL),
(34, 25, 'Bedroom', '4.16', '', 'Common', NULL),
(35, 25, 'Bedroom', '12.88', '', 'Bedroom', NULL),
(36, 25, 'Bathroom', '3.72', '', 'Bathroom attached', NULL),
(37, 25, 'Bedroom', '17.84', '', 'Master Bedroom', NULL),
(38, 25, 'Balcony', '0.46', '', 'Balcony', NULL),
(39, 25, 'Bathroom', '3.72', '', 'Master Room attached', NULL),
(40, 19, 'Attic', '43.00', 'sdaf', 'sdf', NULL),
(41, 92, 'Bedroom', '50.00', '', 'main bedroom', NULL),
(42, 92, 'Bathroom', '5.00', '', 'attached bathroom', NULL),
(43, 92, 'Balcony', '1.00', '', 'balcony1', NULL),
(44, 92, 'Balcony', '1.00', '', 'balcony2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_attach`
--

CREATE TABLE `prop_unit_attach` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `attach_type` varchar(100) NOT NULL COMMENT 'Attachment Type',
  `file_path` varchar(255) NOT NULL COMMENT 'File Path',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On',
  `prop_unit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit ID',
  `name` varchar(100) NOT NULL COMMENT 'Name'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_finance`
--

CREATE TABLE `prop_unit_finance` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `is_rent` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Rentable?',
  `min_rent` decimal(10,2) DEFAULT NULL COMMENT 'Minimum Rent Amount',
  `max_rent` decimal(10,2) DEFAULT NULL COMMENT 'Maximum Rent Amount',
  `fixed_rent` decimal(10,2) DEFAULT NULL COMMENT 'Fixed Rent Amount',
  `min_rent_deposit` decimal(10,2) DEFAULT NULL COMMENT 'MInimum Rental Deposit',
  `max_rent_deposit` decimal(10,2) DEFAULT NULL COMMENT 'Maximum Rental Deposit',
  `fixed_rent_deposit` decimal(10,2) DEFAULT NULL COMMENT 'Fixed Rental Deposit',
  `yearly_rent_increment` decimal(10,2) DEFAULT NULL COMMENT 'Yearly rental increment',
  `is_lease` int(11) NOT NULL DEFAULT '0' COMMENT 'Is Leaseable?',
  `min_lease` decimal(10,2) DEFAULT NULL COMMENT 'Minimum Lease Amount',
  `max_lease` decimal(10,2) DEFAULT NULL COMMENT 'Maximum Lease Amount',
  `fixed_lease` decimal(10,2) DEFAULT NULL COMMENT 'Fixed Lease',
  `lease_years` int(11) DEFAULT NULL COMMENT 'Lease Years',
  `prop_unit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit ID',
  `for_sale` int(11) NOT NULL DEFAULT '0' COMMENT 'For Sale?',
  `starting_price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Starting Price',
  `available_units` int(11) NOT NULL DEFAULT '1' COMMENT 'Available Units',
  `maintenance_fee` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_unit_finance`
--

INSERT INTO `prop_unit_finance` (`id`, `is_rent`, `min_rent`, `max_rent`, `fixed_rent`, `min_rent_deposit`, `max_rent_deposit`, `fixed_rent_deposit`, `yearly_rent_increment`, `is_lease`, `min_lease`, `max_lease`, `fixed_lease`, `lease_years`, `prop_unit_id`, `for_sale`, `starting_price`, `available_units`, `maintenance_fee`) VALUES
(1, 1, '15000.00', NULL, '14500.00', '150000.00', NULL, '145000.00', '5.00', 1, '300000.00', '350000.00', '300000.00', 3, 20, 0, '0.00', 1, '0.00'),
(2, 1, '15000.00', NULL, '20000.00', '200000.00', NULL, '175000.00', '5.00', 1, '300000.00', '350000.00', '300000.00', 3, 21, 0, '0.00', 1, '0.00'),
(3, 1, '25000.00', NULL, '30000.00', '25000.00', NULL, '300000.00', '5.00', 1, '50000.00', '700000.00', '500000.00', 3, 22, 0, '0.00', 1, '0.00'),
(4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 19, 1, '3400000.00', 10, '0.00'),
(5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 23, 1, '0.00', 1, '0.00'),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 24, 1, '8100000.00', 0, '0.00'),
(7, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 25, 1, '8100000.00', 0, '0.00'),
(8, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 26, 1, '0.00', 1, '0.00'),
(9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 27, 1, '5000.00', 1, '0.00'),
(10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 28, 1, '9100000.00', 0, '0.00'),
(11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 29, 1, '0.00', 1, '0.00'),
(12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 30, 1, '0.00', 1, '0.00'),
(13, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 31, 1, '0.00', 1, '0.00'),
(14, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 32, 1, '0.00', 1, '0.00'),
(15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 33, 1, '0.00', 1, '0.00'),
(16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 34, 1, '0.00', 1, '0.00'),
(17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 35, 1, '0.00', 1, '0.00'),
(18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 36, 1, '0.00', 1, '0.00'),
(19, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 37, 1, '0.00', 1, '0.00'),
(20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 38, 1, '0.00', 1, '0.00'),
(21, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 39, 1, '0.00', 1, '0.00'),
(22, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 40, 1, '7000000.00', 0, '0.00'),
(23, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 41, 1, '0.00', 1, '0.00'),
(24, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 42, 1, '0.00', 1, '0.00'),
(25, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 43, 1, '0.00', 1, '0.00'),
(26, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 44, 1, '0.00', 1, '0.00'),
(27, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 45, 1, '0.00', 1, '0.00'),
(28, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 46, 1, '0.00', 1, '0.00'),
(29, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 47, 1, '0.00', 1, '0.00'),
(30, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 48, 1, '0.00', 1, '0.00'),
(31, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 49, 1, '0.00', 1, '0.00'),
(32, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 50, 1, '0.00', 1, '0.00'),
(33, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 51, 1, '0.00', 1, '0.00'),
(34, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 52, 1, '0.00', 1, '0.00'),
(35, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 53, 1, '0.00', 1, '0.00'),
(36, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 54, 1, '0.00', 1, '0.00'),
(37, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 55, 1, '0.00', 1, '0.00'),
(38, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 56, 1, '0.00', 1, '0.00'),
(39, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 57, 1, '0.00', 1, '0.00'),
(40, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 58, 1, '0.00', 1, '0.00'),
(41, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 59, 1, '0.00', 1, '0.00'),
(42, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 60, 1, '0.00', 1, '0.00'),
(43, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 61, 1, '0.00', 1, '0.00'),
(44, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 62, 1, '0.00', 1, '0.00'),
(45, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 63, 1, '0.00', 1, '0.00'),
(46, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 64, 1, '0.00', 1, '0.00'),
(47, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 65, 1, '0.00', 1, '0.00'),
(48, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 66, 1, '0.00', 1, '0.00'),
(49, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 67, 1, '0.00', 1, '0.00'),
(50, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 68, 1, '0.00', 1, '0.00'),
(51, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 69, 1, '0.00', 1, '0.00'),
(52, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 70, 1, '0.00', 1, '0.00'),
(53, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 71, 1, '0.00', 1, '0.00'),
(54, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 72, 1, '0.00', 1, '0.00'),
(55, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 73, 1, '0.00', 1, '0.00'),
(56, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 74, 1, '0.00', 1, '0.00'),
(57, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 75, 1, '0.00', 1, '0.00'),
(58, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 76, 1, '0.00', 1, '0.00'),
(59, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 77, 1, '0.00', 1, '0.00'),
(60, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 78, 1, '0.00', 1, '0.00'),
(61, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 79, 1, '0.00', 1, '0.00'),
(62, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 80, 1, '0.00', 1, '0.00'),
(63, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 81, 1, '0.00', 1, '0.00'),
(64, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 82, 1, '0.00', 1, '0.00'),
(65, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 83, 1, '0.00', 1, '0.00'),
(66, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 84, 1, '0.00', 1, '0.00'),
(67, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 85, 1, '0.00', 1, '0.00'),
(68, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 86, 1, '0.00', 1, '0.00'),
(69, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 87, 1, '0.00', 1, '0.00'),
(70, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 88, 1, '0.00', 1, '0.00'),
(71, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 89, 1, '0.00', 1, '0.00'),
(72, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 90, 1, '6000.00', 1, '0.00'),
(73, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 91, 1, '0.00', 1, '0.00'),
(74, 1, '20000.00', NULL, '20000.00', '50000.00', NULL, '50000.00', '5.00', 0, NULL, NULL, NULL, NULL, 92, 0, '0.00', 1, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_interior`
--

CREATE TABLE `prop_unit_interior` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `category` varchar(100) NOT NULL COMMENT 'Category',
  `heading` varchar(200) NOT NULL COMMENT 'Heading',
  `description` text NOT NULL COMMENT 'Description',
  `prop_unit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_unit_interior`
--

INSERT INTO `prop_unit_interior` (`id`, `category`, `heading`, `description`, `prop_unit_id`) VALUES
(56, 'Flooring', 'Entrance', '?	Lifts / Lobbies & Corridors: Marble / Granite flooring in ground floor ?	Vitrified tile flooring in upper floors ?	Staircases: Vitrified tile flooring', 25),
(57, 'Flooring', 'Living Room', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(58, 'Flooring', 'Dining Room', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(59, 'Flooring', 'Kitchen', '?	Vitrified tile flooring ?	Provision for modular kitchen', 25),
(60, 'Flooring', 'Bedroom', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(61, 'Flooring', 'Bedroom', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(62, 'Flooring', 'Bedroom', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(63, 'Flooring', 'Bathroom', '?	Anti-skid ceramic tile flooring ?	CP fitting: Jaguar / Equivalent ?	Sanitary Ware: Hindustan / Parryware or  Equivalent', 25),
(64, 'Flooring', 'Bedroom', '?	Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring ?	Other Bedrooms: Vitrified tile flooring ?	Balcony Utility: Ceramic tile flooring', 25),
(65, 'Flooring', 'Balcony', '?	Balcony Utility: Ceramic tile flooring', 25),
(66, 'Flooring', 'Bathroom', '?	Anti-skid ceramic tile flooring ?	CP fitting: Jaguar / Equivalent ?	Sanitary Ware: Hindustan / Parryware or  Equivalent', 25),
(67, 'Fittings', 'Doors & Windows', '?	Apartment Main Door: Solid wood frame with BSTV (Both Side Teack veneer) / Masonite skin shutter with melamine polishing on both sides ?	Bedroom Door: Soild core flush door & Masonite skin with hardwood frame', 25),
(68, 'Fittings', 'Electrical', '?	Concealed PVC conduit with copper wiring ?	Modular switches of Anchor Roma or equivalent', 25),
(69, 'Fittings', 'Power Supply', '?	4 KW for a 2-bedroom apartment ?	6 KW for a 3-bedroom apartment', 25),
(70, 'Fittings', 'Standby Power', '?	100% DG back up for emergency power for lifts, pumps & common lighting ?	Apartment ? DG back up o	2 KW for a 2-bedroom apartment o	3 KW for a 3-bedroom apartment', 25),
(71, 'Fittings', 'Lifts', '?	Wing A-3 passenger lifts and 1 service lift ?	Wing B-2 passenger lifts and 1 service lift', 25),
(72, 'Walls', 'Paint', '?	Exterior finish of long lasting texture paint ?	Internal wall in plastic emulsion & ceiling with oil bound distemper ?	Steel & wood works in synthetic enamel paint ?	Common areas in acrylic paint / oil bound distemper', 25),
(119, 'Flooring', 'Common Areas', '? Waiting Lounge / Reception / Lift Lobby: Imported Marble flooring', 27),
(120, 'Flooring', 'Residences', '? Living / Dining / Family Lounge: Imported marble flooring  ? Master Bedroom: Hardwood flooring  ? Other Bedrooms: Vitrified tiles', 27),
(121, 'Flooring', 'Kitchen', '? Vitrified tile flooring  ? Designer modular kitchen  ? Provision for water heater, water purifier, microwave and distwasher', 27),
(122, 'Flooring', 'Balcony Deck', '? Vitrified tiles with wood look finish', 27),
(123, 'Flooring', 'Bathroom', '? Imported marble flooring for master bathroom  ? Designer vitrified tile flooring for other bathrooms  ? CP & Sanitary Fitting: Kohler / Roca / Bathline or equivalent   ? Accessories: Jaquar / Roca / or equivalent  ? Bathtub is master bedroom: Kohler / Roca / Bathline or equivalent  ', 27),
(124, 'Flooring', 'Utility', '? Vitrified tile flooring ', 27),
(125, 'Fittings', 'Door & windows', '? Apartment main door in teak wood jamb with teak wood shutter  ? Bedroom doors in lacquered PU finished solid wood frame and architrave and shutter with masonite skin solid core on both sides.  ? Windows: Aluminum /UPVC with glazing (Domal or equivalent)', 27),
(126, 'Fittings', 'Air-Conditioning', '? Provision for split A/C in living, dining and bedrooms.', 27),
(127, 'Fittings', 'Power Supply', '? 8 KW for a 3-bedroom apartment  ? 10 KW for a 4-bedroom apartment', 27),
(128, 'Fittings', 'Standby Power', '? 100% emergency power for lifts, pumps and lighting in common areas  ? 5KW for a 3-bedroom apartment  ? 6 KW for a 4-bedroom apartment', 27),
(129, 'Fittings', 'Lifts', '? Block A - 2 passenger lifts and 1 service lift  ? Block B - 1 passenger lifts and 1 service lift', 27),
(130, 'Fittings', 'Security Systems & Automation', '? Provision for basic home automation conducting (Lights, security, air conditioning, video door phone)', 27),
(131, 'Walls', 'Exteriors', '? Long lasting textured paint', 27),
(132, 'Walls', 'Interiors', '? Acrylic emulsion paint for internal ceilings & walls', 27),
(133, 'Flooring', 'Common area', '? Waiting lounge / Reception: Imported marble flooring  ? Staircase: Granite / Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 29),
(134, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Engineered marble  ? Bedrooms: Laminate wooden flooring  ? Balcony / Deck: Matt finished vitrified tiles  ? Kitchen: Engineered marble  ? Bathrooms: Designer / ceramic tiles', 29),
(135, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 29),
(136, 'Fittings', 'Bathrooms', '? CP fittings: Bathline / Kohler or equivalent  ? Sanitary fittings: Roca / American Standard or equivalent', 29),
(137, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter  ? Bedroom & Bathroom doors: Pre-engineered frames & shutters  ? Balcony door: UPVC / Aluminum with 2 track and bug screen  ? Windows: UPVC / anodized Aluminum with glazing', 29),
(138, 'Fittings', 'Power supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 6 kW  ? 4-bedroom apartment: 8 kW  ? Electrical switches: Panasonic or equivalent', 29),
(139, 'Fittings', 'DG backup', '? 2-bedroom apartment: 2 KW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 4 kW', 29),
(140, 'Fittings', 'Air-conditioning', '? Provision for split air-conditioning', 29),
(141, 'Fittings', 'Security', '? Video door phones  ? Provision for home automation', 29),
(142, 'Walls', 'External', '?  High quality texture paint  ', 29),
(143, 'Walls', 'Internal ', '? Walls & Ceilings: Acrylic emulsion paint', 29),
(144, 'Flooring', 'Common area', '? Waiting lounge / Reception: Imported marble flooring  ? Staircase: Granite / Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 30),
(145, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Engineered marble  ? Bedrooms: Laminate wooden flooring  ? Balcony / Deck: Matt finished vitrified tiles  ? Kitchen: Engineered marble  ? Bathrooms: Designer / ceramic tiles', 30),
(146, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 30),
(147, 'Fittings', 'Bathrooms', '? CP fittings: Bathline / Kohler or equivalent  ? Sanitary fittings: Roca / American Standard or equivalent', 30),
(148, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter  ? Bedroom & Bathroom doors: Pre-engineered frames & shutters  ? Balcony door: UPVC / Aluminum with 2 track and bug screen  ? Windows: UPVC / anodized Aluminum with glazing', 30),
(149, 'Fittings', 'Power supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 6 kW  ? 4-bedroom apartment: 8 kW  ? Electrical switches: Panasonic or equivalent', 30),
(150, 'Fittings', 'DG backup', '? 2-bedroom apartment: 2 KW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 4 kW', 30),
(151, 'Fittings', 'Air-conditioning', '? Provision for split air-conditioning', 30),
(152, 'Fittings', 'Security', '? Video door phones  ? Provision for home automation', 30),
(153, 'Walls', 'External', '?  High quality texture paint  ', 30),
(154, 'Walls', 'Internal ', '? Walls & Ceilings: Acrylic emulsion paint', 30),
(155, 'Flooring', 'Common area', '? Waiting lounge / Reception: Imported marble flooring  ? Staircase: Granite / Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 31),
(156, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Engineered marble  ? Bedrooms: Laminate wooden flooring  ? Balcony / Deck: Matt finished vitrified tiles  ? Kitchen: Engineered marble  ? Bathrooms: Designer / ceramic tiles', 31),
(157, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 31),
(158, 'Fittings', 'Bathrooms', '? CP fittings: Bathline / Kohler or equivalent  ? Sanitary fittings: Roca / American Standard or equivalent', 31),
(159, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter  ? Bedroom & Bathroom doors: Pre-engineered frames & shutters  ? Balcony door: UPVC / Aluminum with 2 track and bug screen  ? Windows: UPVC / anodized Aluminum with glazing', 31),
(160, 'Fittings', 'Power supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 6 kW  ? 4-bedroom apartment: 8 kW  ? Electrical switches: Panasonic or equivalent', 31),
(161, 'Fittings', 'DG backup', '? 2-bedroom apartment: 2 KW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 4 kW', 31),
(162, 'Fittings', 'Air-conditioning', '? Provision for split air-conditioning', 31),
(163, 'Fittings', 'Security', '? Video door phones  ? Provision for home automation', 31),
(164, 'Walls', 'External', '?  High quality texture paint  ', 31),
(165, 'Walls', 'Internal ', '? Walls & Ceilings: Acrylic emulsion paint', 31),
(166, 'Flooring', 'Common Area', '? Reception /GF Lobby /lift lobby: green marble in combination with white/jaisalmer marble  ? Staircases / Other lift lobbies & corridors: Vitrified tiles', 37),
(167, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Vitrified tiles  ? Master bedroom: Laminated wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/deck: Ceramic tiles  ? Bathrooms: Anti-skid ceramic tiles  ? Kitchen & Utility: Ceramic tiles', 37),
(168, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 37),
(169, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Jaguar/ Ess Ess or equivalent', 37),
(170, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors : Hardwood frame with painted flush shutter  ? Windows: UPVC/anodised Aluminium with bug screen and safety grill  ? Balcony railing: MS railing', 37),
(171, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom unit  ? 5 kW for a 3 bedroom unit  ? 6 kW for a 4-bedroom unit ', 37),
(172, 'Fittings', 'DG Backup (At Additional cost)', '? 2 kW for 2-bedroom unit  ? 3 kW for 3-bedroom unit', 37),
(173, 'Fittings', 'Electrical', '? Modular switches: Anchor Roma or equivalent make', 37),
(174, 'Fittings', 'Air Conditioning', '? Point provision for split A/c', 37),
(175, 'Fittings', 'Lifts', '? 2 passenger lifts and 1 service lift in each block', 37),
(176, 'Walls', 'Exterior ', '? External texture paint with external grade emulsion', 37),
(177, 'Walls', 'Interior', '?  unit ceilings: Oil bound distemper  ? Internal unit walls: Acrylic emulsion paint', 37),
(178, 'Flooring', 'Common Area', '? Reception /GF Lobby /lift lobby: green marble in combination with white/jaisalmer marble  ? Staircases / Other lift lobbies & corridors: Vitrified tiles', 38),
(179, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Vitrified tiles  ? Master bedroom: Laminated wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/deck: Ceramic tiles  ? Bathrooms: Anti-skid ceramic tiles  ? Kitchen & Utility: Ceramic tiles', 38),
(180, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 38),
(181, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Jaguar/ Ess Ess or equivalent', 38),
(182, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors : Hardwood frame with painted flush shutter  ? Windows: UPVC/anodised Aluminium with bug screen and safety grill  ? Balcony railing: MS railing', 38),
(183, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom unit  ? 5 kW for a 3 bedroom unit  ? 6 kW for a 4-bedroom unit ', 38),
(184, 'Fittings', 'DG Backup (At Additional cost)', '? 2 kW for 2-bedroom unit  ? 3 kW for 3-bedroom unit', 38),
(185, 'Fittings', 'Electrical', '? Modular switches: Anchor Roma or equivalent make', 38),
(186, 'Fittings', 'Air Conditioning', '? Point provision for split A/c', 38),
(187, 'Fittings', 'Lifts', '? 2 passenger lifts and 1 service lift in each block', 38),
(188, 'Walls', 'Exterior ', '? External texture paint with external grade emulsion', 38),
(189, 'Walls', 'Interior', '?  unit ceilings: Oil bound distemper  ? Internal unit walls: Acrylic emulsion paint', 38),
(190, 'Flooring', 'Common Area', '? Reception /GF Lobby /lift lobby: green marble in combination with white/jaisalmer marble  ? Staircases / Other lift lobbies & corridors: Vitrified tiles', 39),
(191, 'Flooring', 'Apartment', '? Living / Dining / Family / Foyer: Vitrified tiles  ? Master bedroom: Laminated wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/deck: Ceramic tiles  ? Bathrooms: Anti-skid ceramic tiles  ? Kitchen & Utility: Ceramic tiles', 39),
(192, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 39),
(193, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Jaguar/ Ess Ess or equivalent', 39),
(194, 'Fittings', 'Doors & Windows', '? Main door: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors : Hardwood frame with painted flush shutter  ? Windows: UPVC/anodised Aluminium with bug screen and safety grill  ? Balcony railing: MS railing', 39),
(195, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom unit  ? 5 kW for a 3 bedroom unit  ? 6 kW for a 4-bedroom unit ', 39),
(196, 'Fittings', 'DG Backup (At Additional cost)', '? 2 kW for 2-bedroom unit  ? 3 kW for 3-bedroom unit', 39),
(197, 'Fittings', 'Electrical', '? Modular switches: Anchor Roma or equivalent make', 39),
(198, 'Fittings', 'Air Conditioning', '? Point provision for split A/c', 39),
(199, 'Fittings', 'Lifts', '? 2 passenger lifts and 1 service lift in each block', 39),
(200, 'Walls', 'Exterior ', '? External texture paint with external grade emulsion', 39),
(201, 'Walls', 'Interior', '?  unit ceilings: Oil bound distemper  ? Internal unit walls: Acrylic emulsion paint', 39),
(202, 'Flooring', 'Common Area ', '? Waiting lounge / Reception - Marble / Granite  ? Staircase - Vitrified tiles ? Lift lobby & Corridors - Vitrified tiles', 40),
(203, 'Flooring', 'Apartment ', '? Living / Dining / Family / Foyer - Vitrified tiles', 40),
(204, 'Flooring', 'Kitchen', '? Vitrified tiles', 40),
(205, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 40),
(206, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 40),
(207, 'Flooring', ' Balcony / Deck ', '? Anti-skid ceramic tiles', 40),
(208, 'Flooring', 'Bathrooms', '? Ceramic tiles', 40),
(209, 'Flooring', 'Servant?s room and bathroom', '? Ceramic tiles', 40),
(210, 'Fittings', 'Air-conditioning', 'Provision for split air-conditioning in living room and bedrooms', 40),
(211, 'Fittings', 'Electricals', '2-bedroom - 4 kW', 40),
(212, 'Fittings', 'Electricals', '3-bedroom - 6 kW', 40),
(213, 'Fittings', 'Electricals', 'Modular switches - Anchor Viola or equivalent', 40),
(214, 'Fittings', 'DG Backup', '2-bedroom - 2 kW', 40),
(215, 'Fittings', 'DG Backup', '3-bedroom - 3 kW', 40),
(216, 'Fittings', 'Kitchen', 'Provision for modular kitchen ? Provision for water purifier point, refrigerator point and microwave point', 40),
(217, 'Fittings', 'Bathrooms ', '? CP fittings - Jaquar or equivalent ? Sanitary fittings - Parryware or equivalent', 40),
(218, 'Fittings', 'Doors & Windows', ' Main door to the apartment - Teakwood frame, natural PU lacquer polish shutter & architrave ? Bedroom & Bathroom doors - Hardwood frame, enamel paint shutter & architrave ? Balcony door - UPVC / Aluminium with 3-track bug screen ? Windows - UPVC / Aluminium with bug screen', 40),
(219, 'Fittings', 'Others ', 'Rainwater harvesting, Organic waste convertor and Groundwater recharging', 40),
(220, 'Walls', 'Exterior', 'External texture paint', 40),
(221, 'Walls', 'Internal ceilings', 'Oil Bound Distemper', 40),
(222, 'Walls', 'Internal walls', ' Emulsion paint', 40),
(223, 'Flooring', 'Common Area ', '? Lifts / Lobbies & Corridors  ? Marble / granite flooring in ground floor  ? Vitrified tile flooring in upper floors  ? Staircases  ? Combination of marble and vitrified tile flooring', 42),
(224, 'Flooring', 'Living / Dining / Foyer  ', 'Vitrified tile', 42),
(225, 'Flooring', 'Master Bedroom', '? Laminated wooden flooring', 42),
(226, 'Flooring', 'Other Bedrooms', '? Vitrified tile flooring', 42),
(227, 'Flooring', 'Balcony / Utility', '? Ceramic tile flooring', 42),
(228, 'Flooring', 'Kitchen', '? Vitrified tile flooring', 42),
(229, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 42),
(230, 'Fittings', 'Doors & Windows', '? Apartments Main Door: Teak wood frame with BSTV (Both Side Teak Veneer) / Masonite skin shutter with melamine polishing on both sides  ? Bedroom Doors: Solid core flush door & Masonite skin with hardwood frame  ? Windows & Ventilators: UPVC / Anodised aluminum windows with sliding shutters and safety grill with provision for bug screen', 42),
(231, 'Fittings', 'Electrical', '? Concealed PVC conduit with copper wiring  ? Modular switches of Anchor Roma or equivalent', 42),
(232, 'Fittings', 'Standby Power', '? 100 % DG back up for emergency power for lifts, pumps & common lighting  ? Apartments - DG back up:  ? 2 kW for a 2-bedroom apartment  ? 3 kW for a 3-bedroom apartment  ? 4 kW for a 4-bedroom apartment', 42),
(233, 'Fittings', 'Bathrooms', '? Anti-skid ceramic tile flooring  ? CP Fittings: ESS / Jaguar make or equivalent  ? Sanitary Ware: Hindustan /Parryware or equivalent', 42),
(234, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom apartment  ? 6 kW for a 3-bedroom apartment  ? 8 kW for a 4-bedroom apartment', 42),
(235, 'Walls', 'Exterior', '? Exterior finish of long lasting texture paint', 42),
(236, 'Walls', 'Interior', '? Internal walls in plastic emulsion & ceiling with Oil bound distemper  ? Steel & wood works in synthetic enamel paint', 42),
(237, 'Walls', 'Common area ', '? In acrylic emulsion paint / oil bound distemper', 42),
(238, 'Flooring', 'Common Area ', '? Lifts / Lobbies & Corridors  ? Marble / granite flooring in ground floor  ? Vitrified tile flooring in upper floors  ? Staircases  ? Combination of marble and vitrified tile flooring', 43),
(239, 'Flooring', 'Living / Dining / Foyer  ', 'Vitrified tile', 43),
(240, 'Flooring', 'Master Bedroom', '? Laminated wooden flooring', 43),
(241, 'Flooring', 'Other Bedrooms', '? Vitrified tile flooring', 43),
(242, 'Flooring', 'Balcony / Utility', '? Ceramic tile flooring', 43),
(243, 'Flooring', 'Kitchen', '? Vitrified tile flooring', 43),
(244, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 43),
(245, 'Fittings', 'Doors & Windows', '? Apartments Main Door: Teak wood frame with BSTV (Both Side Teak Veneer) / Masonite skin shutter with melamine polishing on both sides  ? Bedroom Doors: Solid core flush door & Masonite skin with hardwood frame  ? Windows & Ventilators: UPVC / Anodised aluminum windows with sliding shutters and safety grill with provision for bug screen', 43),
(246, 'Fittings', 'Electrical', '? Concealed PVC conduit with copper wiring  ? Modular switches of Anchor Roma or equivalent', 43),
(247, 'Fittings', 'Standby Power', '? 100 % DG back up for emergency power for lifts, pumps & common lighting  ? Apartments - DG back up:  ? 2 kW for a 2-bedroom apartment  ? 3 kW for a 3-bedroom apartment  ? 4 kW for a 4-bedroom apartment', 43),
(248, 'Fittings', 'Bathrooms', '? Anti-skid ceramic tile flooring  ? CP Fittings: ESS / Jaguar make or equivalent  ? Sanitary Ware: Hindustan /Parryware or equivalent', 43),
(249, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom apartment  ? 6 kW for a 3-bedroom apartment  ? 8 kW for a 4-bedroom apartment', 43),
(250, 'Walls', 'Exterior', '? Exterior finish of long lasting texture paint', 43),
(251, 'Walls', 'Interior', '? Internal walls in plastic emulsion & ceiling with Oil bound distemper  ? Steel & wood works in synthetic enamel paint', 43),
(252, 'Walls', 'Common area ', '? In acrylic emulsion paint / oil bound distemper', 43),
(253, 'Flooring', 'Common Area ', '? Lifts / Lobbies & Corridors  ? Marble / granite flooring in ground floor  ? Vitrified tile flooring in upper floors  ? Staircases  ? Combination of marble and vitrified tile flooring', 44),
(254, 'Flooring', 'Living / Dining / Foyer  ', 'Vitrified tile', 44),
(255, 'Flooring', 'Master Bedroom', '? Laminated wooden flooring', 44),
(256, 'Flooring', 'Other Bedrooms', '? Vitrified tile flooring', 44),
(257, 'Flooring', 'Balcony / Utility', '? Ceramic tile flooring', 44),
(258, 'Flooring', 'Kitchen', '? Vitrified tile flooring', 44),
(259, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 44),
(260, 'Fittings', 'Doors & Windows', '? Apartments Main Door: Teak wood frame with BSTV (Both Side Teak Veneer) / Masonite skin shutter with melamine polishing on both sides  ? Bedroom Doors: Solid core flush door & Masonite skin with hardwood frame  ? Windows & Ventilators: UPVC / Anodised aluminum windows with sliding shutters and safety grill with provision for bug screen', 44),
(261, 'Fittings', 'Electrical', '? Concealed PVC conduit with copper wiring  ? Modular switches of Anchor Roma or equivalent', 44),
(262, 'Fittings', 'Standby Power', '? 100 % DG back up for emergency power for lifts, pumps & common lighting  ? Apartments - DG back up:  ? 2 kW for a 2-bedroom apartment  ? 3 kW for a 3-bedroom apartment  ? 4 kW for a 4-bedroom apartment', 44),
(263, 'Fittings', 'Bathrooms', '? Anti-skid ceramic tile flooring  ? CP Fittings: ESS / Jaguar make or equivalent  ? Sanitary Ware: Hindustan /Parryware or equivalent', 44),
(264, 'Fittings', 'Power Supply', '? 4 kW for a 2-bedroom apartment  ? 6 kW for a 3-bedroom apartment  ? 8 kW for a 4-bedroom apartment', 44),
(265, 'Walls', 'Exterior', '? Exterior finish of long lasting texture paint', 44),
(266, 'Walls', 'Interior', '? Internal walls in plastic emulsion & ceiling with Oil bound distemper  ? Steel & wood works in synthetic enamel paint', 44),
(267, 'Walls', 'Common area ', '? In acrylic emulsion paint / oil bound distemper', 44),
(268, 'Flooring', 'Common area', '? Imported vitrified tiles marble flooring', 45),
(269, 'Flooring', ' Kitchen', '? Superior quality vitrified tiles', 45),
(270, 'Flooring', 'Master Bedroom', '? Laminated wood', 45),
(271, 'Flooring', 'Other Bedroom', '? Superior quality vitrified tiles / laminated wood', 45),
(272, 'Flooring', 'Servant?s Room', '?  Ceramic tiles', 45),
(273, 'Flooring', 'Balconies', '? Superior quality vitrified tiles', 45),
(274, 'Flooring', 'Gymnasium/Home theatre', '? heavy duty ? Vinyl / laminated wooden flooring   ? Vernahahs/Decks: superior quality anti-Skid vitrifies tiles', 45),
(275, 'Fittings', 'Master Toilet', '? 4 fixture toilet  ? Wash Basins, 1 shower Cubicle, 1 EWC, 1 Bath Tub, Granite/marble counter & Ledges Kohler/American standard or equivalent', 45),
(276, 'Fittings', 'Other Toilets', '3 fixture toilet, 1 wash basin, 1 shower cubicle, 1 EWC, Granite/Marble counter & Ledges Kohler/American standard or equivalent', 45),
(277, 'Fittings', 'Doors', '? Entrance Door: Polished teak wood frame & shutter  ? Internal Door: Hard wood frames & flush shutter  ? External Deck/ Balcony Doors: UPVC/Aluminum frames with partially glazed shutters', 45),
(278, 'Fittings', 'Services & fitting', '? Power supply: 10KW', 45),
(279, 'Walls', 'Kitchen', '? 2?.0? above counter vitrified tiles', 45),
(280, 'Walls', 'Toilets', '? up to ceiling bottom vitrified tiles', 45),
(281, 'Walls', ' Servant?s Room', '?Toilet: up tp 7?.0? ht ceramic tiles', 45),
(282, 'Flooring', 'Common area', '? Imported vitrified tiles marble flooring', 46),
(283, 'Flooring', ' Kitchen', '? Superior quality vitrified tiles', 46),
(284, 'Flooring', 'Master Bedroom', '? Laminated wood', 46),
(285, 'Flooring', 'Other Bedroom', '? Superior quality vitrified tiles / laminated wood', 46),
(286, 'Flooring', 'Servant?s Room', '?  Ceramic tiles', 46),
(287, 'Flooring', 'Balconies', '? Superior quality vitrified tiles', 46),
(288, 'Flooring', 'Gymnasium/Home theatre', '? heavy duty ? Vinyl / laminated wooden flooring   ? Vernahahs/Decks: superior quality anti-Skid vitrifies tiles', 46),
(289, 'Fittings', 'Master Toilet', '? 4 fixture toilet  ? Wash Basins, 1 shower Cubicle, 1 EWC, 1 Bath Tub, Granite/marble counter & Ledges Kohler/American standard or equivalent', 46),
(290, 'Fittings', 'Other Toilets', '3 fixture toilet, 1 wash basin, 1 shower cubicle, 1 EWC, Granite/Marble counter & Ledges Kohler/American standard or equivalent', 46),
(291, 'Fittings', 'Doors', '? Entrance Door: Polished teak wood frame & shutter  ? Internal Door: Hard wood frames & flush shutter  ? External Deck/ Balcony Doors: UPVC/Aluminum frames with partially glazed shutters', 46),
(292, 'Fittings', 'Services & fitting', '? Power supply: 10KW', 46),
(293, 'Walls', 'Kitchen', '? 2?.0? above counter vitrified tiles', 46),
(294, 'Walls', 'Toilets', '? up to ceiling bottom vitrified tiles', 46),
(295, 'Walls', ' Servant?s Room', '?Toilet: up tp 7?.0? ht ceramic tiles', 46),
(296, 'Flooring', 'Common Area`', '? Waiting lounge/Reception: Imported marble flooring  ? Staircase: Granite/Vitrified tiles  ? Lift lobby & Corridors: Granite/Vitrified tiles', 47),
(297, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Large size vitrified tiles  ? Master bedroom: Laminate wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/Deck: Matt finished vitrified tiles  ? Kitchen & Utility: Large size vitrified tiles  ? Master bedroom toilet: Designer tile flooring  ? Other toilets: Anti-skid ceramic tiles', 47),
(298, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 47),
(299, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Bathline/Kohler or equivalent', 47),
(300, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter  ? Bedroom & bathroom doors: Pre-engineered frames/Pre-engineered shutters & PU coating  ? Balcony door: UPVC/Aluminium 3 track with bug screen  ? Windows: UPVC/Anodised Aluminium with glazing, sliding type with bug screen', 47),
(301, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning', 47),
(302, 'Fittings', 'Power Supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 5 kW  ? 4-bedroom apartment: 6 kW', 47),
(303, 'Fittings', 'DG Backup', '? 2-bedroom apartment: 2 kW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 5 kW', 47),
(304, 'Fittings', 'Lifts ', '? 2 passenger lifts in each block', 47),
(305, 'Walls', 'Exterior', '? External texture paint with scratch coat', 47),
(306, 'Walls', 'Interior', '? Internal walls & ceilings: Acrylic emulsion paint', 47),
(307, 'Flooring', 'Common Area`', '? Waiting lounge/Reception: Imported marble flooring  ? Staircase: Granite/Vitrified tiles  ? Lift lobby & Corridors: Granite/Vitrified tiles', 48),
(308, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Large size vitrified tiles  ? Master bedroom: Laminate wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/Deck: Matt finished vitrified tiles  ? Kitchen & Utility: Large size vitrified tiles  ? Master bedroom toilet: Designer tile flooring  ? Other toilets: Anti-skid ceramic tiles', 48),
(309, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 48),
(310, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Bathline/Kohler or equivalent', 48),
(311, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter  ? Bedroom & bathroom doors: Pre-engineered frames/Pre-engineered shutters & PU coating  ? Balcony door: UPVC/Aluminium 3 track with bug screen  ? Windows: UPVC/Anodised Aluminium with glazing, sliding type with bug screen', 48),
(312, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning', 48),
(313, 'Fittings', 'Power Supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 5 kW  ? 4-bedroom apartment: 6 kW', 48),
(314, 'Fittings', 'DG Backup', '? 2-bedroom apartment: 2 kW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 5 kW', 48),
(315, 'Fittings', 'Lifts ', '? 2 passenger lifts in each block', 48),
(316, 'Walls', 'Exterior', '? External texture paint with scratch coat', 48),
(317, 'Walls', 'Interior', '? Internal walls & ceilings: Acrylic emulsion paint', 48),
(318, 'Flooring', 'Common Area`', '? Waiting lounge/Reception: Imported marble flooring  ? Staircase: Granite/Vitrified tiles  ? Lift lobby & Corridors: Granite/Vitrified tiles', 49),
(319, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Large size vitrified tiles  ? Master bedroom: Laminate wooden flooring  ? Other bedrooms: Vitrified tiles  ? Balcony/Deck: Matt finished vitrified tiles  ? Kitchen & Utility: Large size vitrified tiles  ? Master bedroom toilet: Designer tile flooring  ? Other toilets: Anti-skid ceramic tiles', 49),
(320, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 49),
(321, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Bathline/Kohler or equivalent', 49),
(322, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter  ? Bedroom & bathroom doors: Pre-engineered frames/Pre-engineered shutters & PU coating  ? Balcony door: UPVC/Aluminium 3 track with bug screen  ? Windows: UPVC/Anodised Aluminium with glazing, sliding type with bug screen', 49),
(323, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning', 49),
(324, 'Fittings', 'Power Supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 5 kW  ? 4-bedroom apartment: 6 kW', 49),
(325, 'Fittings', 'DG Backup', '? 2-bedroom apartment: 2 kW  ? 3-bedroom apartment: 3 kW  ? 4-bedroom apartment: 5 kW', 49),
(326, 'Fittings', 'Lifts ', '? 2 passenger lifts in each block', 49),
(327, 'Walls', 'Exterior', '? External texture paint with scratch coat', 49),
(328, 'Walls', 'Interior', '? Internal walls & ceilings: Acrylic emulsion paint', 49),
(329, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Granite/Vitrified tiles  ? Staircase: Step Tiles/Stone  ? Lift lobby & Corridors: Vitrified tiles', 50),
(330, 'Flooring', 'Living/Dining/ Foyer', '? Vitrified tiles', 50),
(331, 'Flooring', 'Master bedroom', '? Laminate wooden flooring', 50),
(332, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 50),
(333, 'Flooring', 'Balcony/Deck', '? Anti- skid ceramic tiles', 50),
(334, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 50),
(335, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles plus Glazed tile cladding to false ceiling height', 50),
(336, 'Flooring', 'Other toilets', '? Ceramic tiles plus Glazed tile cladding to false ceiling height', 50),
(337, 'Fittings', 'Power Supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 5 kW', 50),
(338, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Hard wood door with flush shutter  ? Bedroom & bathroom doors: Hard wood door with flush shutter  ? Balcony door: Aluminum Glazed sliding door  ? Windows: Anodized Aluminum with glazing, sliding type with provision for bug screen', 50),
(339, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 50),
(340, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning in Living and Master Bedroom', 50),
(341, 'Fittings', 'DG Backup', '? 2-bedroom apartment: 2 kW  ? 3-bedroom apartment: 3 kW', 50),
(342, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Jaguar/Parry ware or equivalent', 50),
(343, 'Fittings', 'Lifts', '? 2 passenger lifts in each block', 50),
(344, 'Walls', 'Exterior', '? External: Weather proof acrylic based paint with textured scratch finish', 50),
(345, 'Walls', 'Interior', '? Internal walls & ceilings: Acrylic emulsion paint', 50),
(346, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Granite/Vitrified tiles  ? Staircase: Step Tiles/Stone  ? Lift lobby & Corridors: Vitrified tiles', 51),
(347, 'Flooring', 'Living/Dining/ Foyer', '? Vitrified tiles', 51),
(348, 'Flooring', 'Master bedroom', '? Laminate wooden flooring', 51),
(349, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 51),
(350, 'Flooring', 'Balcony/Deck', '? Anti- skid ceramic tiles', 51),
(351, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 51),
(352, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles plus Glazed tile cladding to false ceiling height', 51),
(353, 'Flooring', 'Other toilets', '? Ceramic tiles plus Glazed tile cladding to false ceiling height', 51),
(354, 'Fittings', 'Power Supply', '? 2-bedroom apartment: 4 kW  ? 3-bedroom apartment: 5 kW', 51),
(355, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Hard wood door with flush shutter  ? Bedroom & bathroom doors: Hard wood door with flush shutter  ? Balcony door: Aluminum Glazed sliding door  ? Windows: Anodized Aluminum with glazing, sliding type with provision for bug screen', 51),
(356, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 51),
(357, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning in Living and Master Bedroom', 51),
(358, 'Fittings', 'DG Backup', '? 2-bedroom apartment: 2 kW  ? 3-bedroom apartment: 3 kW', 51),
(359, 'Fittings', 'Bathrooms', '? CP & Sanitary fittings: Jaguar/Parry ware or equivalent', 51),
(360, 'Fittings', 'Lifts', '? 2 passenger lifts in each block', 51),
(361, 'Walls', 'Exterior', '? External: Weather proof acrylic based paint with textured scratch finish', 51),
(362, 'Walls', 'Interior', '? Internal walls & ceilings: Acrylic emulsion paint', 51),
(363, 'Flooring', 'Living, dining &kitchen', '? Vitrified tiles', 52),
(364, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 52),
(365, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 52),
(366, 'Flooring', 'Balcony & deck', '? Anti-skid ceramic tiles', 52),
(367, 'Flooring', 'Toilets', '? Ceramic tiles', 52),
(368, 'Flooring', 'Kitchen & utility', '? Vitrified tiles', 52),
(369, 'Flooring', 'Lift lobby & common area', '? Vitrified tiles', 52),
(370, 'Flooring', 'Staircase', '? Step tiles', 52),
(371, 'Flooring', 'Terrace', '? Clay tiles with water proofing', 52),
(372, 'Fittings', 'Toilet Fixtures & Accessories', '? CP fittings: Ess Ess/ Jacquar or equivalent make  ? Sanitary fixtures: Parryware/Hindware or equivalent', 52),
(373, 'Fittings', 'Doors & Windows', '? Main door & bedroom doors: Hardwood door frames, flush door shutters with both sides synthetic enamel painted  ? Windows: Aluminium glazed windows with MS grills & bug screen  ? Balcony & staircase railing: MS railing', 52),
(374, 'Fittings', ' Lifts', '? 2 passenger lifts and 1 staircase for a core of 4 units  ? 2 passenger lifts and 2 staircases for a core of 8 units', 52),
(375, 'Fittings', 'Power supply', '? 3-bedroom: 5 kW  ? 2-bedroom: 4 kW', 52),
(376, 'Fittings', 'Power points for Geysers in all bathrooms', '? 1-bedroom & 1-bedroom with study: 3 kW', 52),
(377, 'Fittings', 'DG backup', '? 3-bedroom: 3 kW  ? 2-bedroom: 2 kW  ?1-bedroom & 1-bedroom with study: 1 kW  ? Modular switches: Anchor Roma or equivalent', 52),
(378, 'Fittings', 'Security System', '? Provision for intercom facility', 52),
(379, 'Walls', 'Exterior', '? Exterior finish of the building: Weather proof acrylic based paint with textured scratch finish', 52),
(380, 'Walls', 'Interior', '? Internal walls of apartments: Acrylic emulsion paint  ? Internal ceilings of apartment: Oil bound distemper', 52),
(381, 'Flooring', 'Living, dining &kitchen', '? Vitrified tiles', 53),
(382, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 53),
(383, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 53),
(384, 'Flooring', 'Balcony & deck', '? Anti-skid ceramic tiles', 53),
(385, 'Flooring', 'Toilets', '? Ceramic tiles', 53),
(386, 'Flooring', 'Kitchen & utility', '? Vitrified tiles', 53),
(387, 'Flooring', 'Lift lobby & common area', '? Vitrified tiles', 53),
(388, 'Flooring', 'Staircase', '? Step tiles', 53),
(389, 'Flooring', 'Terrace', '? Clay tiles with water proofing', 53),
(390, 'Fittings', 'Toilet Fixtures & Accessories', '? CP fittings: Ess Ess/ Jacquar or equivalent make  ? Sanitary fixtures: Parryware/Hindware or equivalent', 53),
(391, 'Fittings', 'Doors & Windows', '? Main door & bedroom doors: Hardwood door frames, flush door shutters with both sides synthetic enamel painted  ? Windows: Aluminium glazed windows with MS grills & bug screen  ? Balcony & staircase railing: MS railing', 53),
(392, 'Fittings', ' Lifts', '? 2 passenger lifts and 1 staircase for a core of 4 units  ? 2 passenger lifts and 2 staircases for a core of 8 units', 53),
(393, 'Fittings', 'Power supply', '? 3-bedroom: 5 kW  ? 2-bedroom: 4 kW', 53),
(394, 'Fittings', 'Power points for Geysers in all bathrooms', '? 1-bedroom & 1-bedroom with study: 3 kW', 53),
(395, 'Fittings', 'DG backup', '? 3-bedroom: 3 kW  ? 2-bedroom: 2 kW  ?1-bedroom & 1-bedroom with study: 1 kW  ? Modular switches: Anchor Roma or equivalent', 53),
(396, 'Fittings', 'Security System', '? Provision for intercom facility', 53),
(397, 'Walls', 'Exterior', '? Exterior finish of the building: Weather proof acrylic based paint with textured scratch finish', 53),
(398, 'Walls', 'Interior', '? Internal walls of apartments: Acrylic emulsion paint  ? Internal ceilings of apartment: Oil bound distemper', 53),
(399, 'Flooring', 'Common Area', '? Turkish marble and granite', 57),
(400, 'Flooring', 'Living / Dining / Foyer/ Family', '? Turkish marble', 57),
(401, 'Flooring', 'Bedrooms', '? Turkish marble / Wooden flooring', 57),
(402, 'Flooring', 'Balcony / Utility', '? Anti skid tiles', 57),
(403, 'Flooring', 'Maid\'s Room', '? Ceramic tiles', 57),
(404, 'Flooring', 'Bedroom toilets', '? Marble-floor and dado up to counter top, shower area full height', 57),
(405, 'Flooring', 'Servant\'s toilet', 'Ceramic tiles on wall and floor', 57),
(406, 'Flooring', 'Kitchen', '? Marble', 57),
(407, 'Fittings', 'Kitchen', '? Modular kitchen  ? Provision for water heater, water purifier and dish washer', 57),
(408, 'Fittings', 'Toilets', '? Chromium plated fittings - Kohler  ? Bathtub in 2 bathrooms   ? Frameless shower cubicles in 2 bathrooms  ? Counters with above-counter designer basins ? One bedroom to have powder room / toilet  ? One bedroom to have washing machine and dryer provision', 57),
(409, 'Fittings', 'Doors', '? Bedroom doors in teakwood jamblining frame with designer shutter', 57),
(410, 'Fittings', 'Windows', '? Aluminum with glazing (Double glazed / laminated)', 57),
(411, 'Fittings', 'Plumbing and Sanitary fixtures', '? Porcelain fixtures (western style W.C. and counter top with designer wash basin)  ? Sanitary ware of Kohler', 57),
(412, 'Fittings', 'Electrical', '? Concealed PVC conduit with copper wiring  ? 15 KW with 100% backup', 57),
(413, 'Fittings', 'Others', '? RCC Framed structure with solid block masonry walls and structural glazing (double glazed / laminated)  ? Anti-termite treatment on entire plinth', 57),
(414, 'Walls', 'Exterior', '? Finish-Combination of ACP / Clay blocks / Textured coating / Stone work', 57),
(415, 'Walls', 'Interior', '? Internal ceilings-Acrylic emulsion painting  ? Internal walls-Acrylic emulsion painting', 57),
(416, 'Walls', 'Common area', '?  Acrylic emulsion / Textured paint', 57),
(417, 'Flooring', 'Living, Dining & Kitchen', '? Vitrified tiles', 54),
(418, 'Flooring', 'Bedrooms', '? Ceramic tiles', 54),
(419, 'Flooring', 'Toilets', '?  Anti-skid Ceramics tiles', 54),
(420, 'Flooring', 'Balcony & Utility', '? Ceramic tiles', 54),
(421, 'Flooring', ' Lift lobby & Common area', '? Vitrified tiles', 54),
(422, 'Flooring', ' Staircase', '? Step tiles (cement / Ceramic)', 54),
(423, 'Flooring', 'Terrace', '? Clay tiles', 54),
(424, 'Flooring', 'Dado', '? Kitchen Dado: Glazed tiles  ? Toilet Dado: Glazed tiles', 54),
(425, 'Fittings', 'Kitchen', '? Granite platform with stainless steel single bowl sink with drain board  ? Provision for water purifier', 54),
(426, 'Fittings', 'Door frames', '? Hardwood door frames', 54),
(427, 'Fittings', 'Door shutter', '? Flush door shutters', 54),
(428, 'Fittings', ' Windows & ventilators', '? Aluminum glazed windows with MS grills & bug screen', 54),
(429, 'Fittings', 'Balcony & staircase railing', '? Ms railing', 54),
(430, 'Fittings', 'Toilets Fixtures & Accessories', '? All CP fitting of Ess Ess / Essco or equivalent make for toilets  ? Solar water heater at additional cost', 54),
(431, 'Fittings', 'Electrical', '? Concealed copper wiring of standard make and modular switched of Anchor Rider or equivalent  ? Power outlets for A/C in master bedroom, provision for A/C  ? Power points for geysers in all the bathrooms  ? For 3-bedroom load: 5 KW, 2-bedroom Load: 4KW, 1-bedroom with study: 3KW', 54),
(432, 'Fittings', 'Lifts ', '? Six passenger capacity lifts', 54),
(433, 'Fittings', 'Backup power', '? Power backup for apartment: 500W for 1-bedroom with study, 750W for 2-bedroom & 1KW for 3-bedroom   ? Power backup for common area / facilities & lifts', 54),
(434, 'Walls', 'Exterior ', '? Finish of the building: weather proof acrylic based paint', 54),
(435, 'Walls', 'Interior ', '? All interior wall & ceiling: Oil bound distemper', 54),
(436, 'Flooring', 'Living, Dining & Kitchen', '? Vitrified tiles', 55),
(437, 'Flooring', 'Bedrooms', '? Ceramic tiles', 55),
(438, 'Flooring', 'Toilets', '?  Anti-skid Ceramics tiles', 55),
(439, 'Flooring', 'Balcony & Utility', '? Ceramic tiles', 55),
(440, 'Flooring', ' Lift lobby & Common area', '? Vitrified tiles', 55),
(441, 'Flooring', ' Staircase', '? Step tiles (cement / Ceramic)', 55),
(442, 'Flooring', 'Terrace', '? Clay tiles', 55),
(443, 'Flooring', 'Dado', '? Kitchen Dado: Glazed tiles  ? Toilet Dado: Glazed tiles', 55),
(444, 'Fittings', 'Kitchen', '? Granite platform with stainless steel single bowl sink with drain board  ? Provision for water purifier', 55),
(445, 'Fittings', 'Door frames', '? Hardwood door frames', 55),
(446, 'Fittings', 'Door shutter', '? Flush door shutters', 55),
(447, 'Fittings', ' Windows & ventilators', '? Aluminum glazed windows with MS grills & bug screen', 55),
(448, 'Fittings', 'Balcony & staircase railing', '? Ms railing', 55),
(449, 'Fittings', 'Toilets Fixtures & Accessories', '? All CP fitting of Ess Ess / Essco or equivalent make for toilets  ? Solar water heater at additional cost', 55),
(450, 'Fittings', 'Electrical', '? Concealed copper wiring of standard make and modular switched of Anchor Rider or equivalent  ? Power outlets for A/C in master bedroom, provision for A/C  ? Power points for geysers in all the bathrooms  ? For 3-bedroom load: 5 KW, 2-bedroom Load: 4KW, 1-bedroom with study: 3KW', 55),
(451, 'Fittings', 'Lifts ', '? Six passenger capacity lifts', 55),
(452, 'Fittings', 'Backup power', '? Power backup for apartment: 500W for 1-bedroom with study, 750W for 2-bedroom & 1KW for 3-bedroom   ? Power backup for common area / facilities & lifts', 55),
(453, 'Walls', 'Exterior ', '? Finish of the building: weather proof acrylic based paint', 55),
(454, 'Walls', 'Interior ', '? All interior wall & ceiling: Oil bound distemper', 55),
(455, 'Flooring', 'Living, Dining & Kitchen', '? Vitrified tiles', 56),
(456, 'Flooring', 'Bedrooms', '? Ceramic tiles', 56),
(457, 'Flooring', 'Toilets', '?  Anti-skid Ceramics tiles', 56),
(458, 'Flooring', 'Balcony & Utility', '? Ceramic tiles', 56),
(459, 'Flooring', ' Lift lobby & Common area', '? Vitrified tiles', 56),
(460, 'Flooring', ' Staircase', '? Step tiles (cement / Ceramic)', 56),
(461, 'Flooring', 'Terrace', '? Clay tiles', 56),
(462, 'Flooring', 'Dado', '? Kitchen Dado: Glazed tiles  ? Toilet Dado: Glazed tiles', 56),
(463, 'Fittings', 'Kitchen', '? Granite platform with stainless steel single bowl sink with drain board  ? Provision for water purifier', 56),
(464, 'Fittings', 'Door frames', '? Hardwood door frames', 56),
(465, 'Fittings', 'Door shutter', '? Flush door shutters', 56),
(466, 'Fittings', ' Windows & ventilators', '? Aluminum glazed windows with MS grills & bug screen', 56),
(467, 'Fittings', 'Balcony & staircase railing', '? Ms railing', 56),
(468, 'Fittings', 'Toilets Fixtures & Accessories', '? All CP fitting of Ess Ess / Essco or equivalent make for toilets  ? Solar water heater at additional cost', 56),
(469, 'Fittings', 'Electrical', '? Concealed copper wiring of standard make and modular switched of Anchor Rider or equivalent  ? Power outlets for A/C in master bedroom, provision for A/C  ? Power points for geysers in all the bathrooms  ? For 3-bedroom load: 5 KW, 2-bedroom Load: 4KW, 1-bedroom with study: 3KW', 56),
(470, 'Fittings', 'Lifts ', '? Six passenger capacity lifts', 56),
(471, 'Fittings', 'Backup power', '? Power backup for apartment: 500W for 1-bedroom with study, 750W for 2-bedroom & 1KW for 3-bedroom   ? Power backup for common area / facilities & lifts', 56),
(472, 'Walls', 'Exterior ', '? Finish of the building: weather proof acrylic based paint', 56),
(473, 'Walls', 'Interior ', '? All interior wall & ceiling: Oil bound distemper', 56),
(474, 'Flooring', 'Common Area', '? Imported vitrified tiles / marble flooring', 58),
(475, 'Flooring', 'Kitchen', '? Super quality vitrified tiles', 58),
(476, 'Flooring', 'Master Bedrrom ', '? Laminated wood', 58),
(477, 'Flooring', 'Other Bedroom ', '? Superior quality vitrified tiles / Laminated wood', 58),
(478, 'Flooring', 'Servant\'s Room', '? Ceramic tiles', 58),
(479, 'Flooring', 'Balconies', '? Super quality vitrified tiles', 58),
(480, 'Flooring', 'All Toilets', '? Granite / Anti-skid vitrified tiles', 58),
(481, 'Flooring', 'Gymnasiuim / Home Theatre ', '? Heavy duty vinyl / Laminated wooden flooring', 58),
(482, 'Flooring', 'Verandah/Decks', '? Super quality anti-skid vitrified tiles', 58),
(483, 'Flooring', 'Doors', '? Entrance Door: Polished Teak wood frame & shutter  ? Internal Door: Hard wood frames & flush shutter  ? External Deck: Balcony doors: UPVC/Aluminum  ? Frames with partially glazed shutter', 58),
(484, 'Flooring', 'Power supply ', '? 10KW', 58),
(485, 'Flooring', 'Standard services', '? Door video phone  ? LPG copper connection   ? Solar water heat: 200L  ? Split A/C provisions  ? Copper piping for internal water supply  ? Rain-water harvesting', 58),
(486, 'Fittings', 'Master Toilet ', '? Wash Basins, 1 Shower Cubicle, 1 EWC, 1 Bath Tub, Granite/Marble counter & ledges Kohier/American standard or equivalent', 58),
(487, 'Fittings', 'Other Toilets', '?  Wash Basin, 1 Shower Cubicle, 1 EWC, 1 Bath Tub, Granite/Marble counter & ledges Kohier/American standard or equivalent', 58),
(488, 'Walls', 'Exterior', '? External textured paint: Paint/Cladding', 58),
(489, 'Walls', 'Interior', '? Internal wall & ceiling plastic emulsion paint', 58),
(490, 'Flooring', 'Common area', '? Imported vitrified tiles/ marble flooring', 59),
(491, 'Flooring', ' Kitchen', '? Superior quality vitrified tiles', 59),
(492, 'Flooring', 'Master Bedrooms', '? Superior quality vitrified tiles / laminated wood', 59),
(493, 'Flooring', 'Servant?s Room', '? Ceramic tiles', 59),
(494, 'Flooring', 'Balconies', '? Superior quality vitrified tiles', 59),
(495, 'Flooring', 'All Toilets', '? Granite / Anti-Skid vitrified tiles', 59),
(496, 'Flooring', 'Gymnasium /Decks', '? Superior quality anti-skid vitrified tiles', 59),
(497, 'Fittings', 'Doors', '? Entrance Door: Polished teak wood frame & shutter  ? Internal Door: Hard wood frame & flush shutter  ? External Deck / Balcony Doors: UPVC/Aluminum frames with partially glazed shutters', 59),
(498, 'Fittings', 'Power supply', '? 10KW', 59),
(499, 'Fittings', 'Standard services', '? Door video phone  ? LPG Copper connection  ? Solar water heater: 200L   ? 50% DG backup ? Split A/C provisions  ? Copper Piping for internal water supply ? Rain water harvesting', 59),
(500, 'Walls', 'Exterior', '? External textured paint: paint/cladding', 59),
(501, 'Walls', 'Interior', '? Internal wall & ceiling: Plastic Emulsion paint', 59),
(502, 'Flooring', 'Common Area', '? Waiting lounge/Reception:Granite / Vitrified / Marble', 62),
(503, 'Flooring', 'Staircase', '? Vitrified tiles', 62),
(504, 'Flooring', 'Lift lobby & Corridors', '? Vitrified tiles', 62),
(505, 'Flooring', 'Apartment', '? Vitrified tiles', 62),
(506, 'Flooring', 'Kitchen', '? Vitrified tiles', 62),
(507, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 62),
(508, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 62),
(509, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 62),
(510, 'Flooring', 'Bathrooms', '? Ceramic tiles', 62),
(511, 'Fittings', 'Kitchen', 'Provision for modular kitchen  ? Provision for water purifier point, refrigerator point, microwave point, stainless steel sink single bowl', 62),
(512, 'Fittings', 'Bathrooms', 'CP fittings, sanitary fittings & accessories: Kohler or equivalent', 62),
(513, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent) ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill', 62),
(514, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning in living and bedrooms', 62),
(515, 'Fittings', 'Power Supply', '? 2-bedroom: 2 kW  ? 3-bedroom: 5 kW   ? 4-bedroom: 6 kW  ? Modular switches: Anchor Roma or equivalent', 62),
(516, 'Fittings', 'DG Backup', '? 2-bedroom: 2 kW  ? 3-bedroom: 3 kW  ? 4-bedroom: 4 kW', 62),
(517, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 62),
(518, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper  ? Internal walls: Acrylic Emulsion paint', 62),
(519, 'Flooring', 'Common Area', '? Waiting lounge/Reception:Granite / Vitrified / Marble', 63),
(520, 'Flooring', 'Staircase', '? Vitrified tiles', 63),
(521, 'Flooring', 'Lift lobby & Corridors', '? Vitrified tiles', 63),
(522, 'Flooring', 'Apartment', '? Vitrified tiles', 63),
(523, 'Flooring', 'Kitchen', '? Vitrified tiles', 63),
(524, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 63);
INSERT INTO `prop_unit_interior` (`id`, `category`, `heading`, `description`, `prop_unit_id`) VALUES
(525, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 63),
(526, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 63),
(527, 'Flooring', 'Bathrooms', '? Ceramic tiles', 63),
(528, 'Fittings', 'Kitchen', 'Provision for modular kitchen  ? Provision for water purifier point, refrigerator point, microwave point, stainless steel sink single bowl', 63),
(529, 'Fittings', 'Bathrooms', 'CP fittings, sanitary fittings & accessories: Kohler or equivalent', 63),
(530, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent) ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill', 63),
(531, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning in living and bedrooms', 63),
(532, 'Fittings', 'Power Supply', '? 2-bedroom: 2 kW  ? 3-bedroom: 5 kW   ? 4-bedroom: 6 kW  ? Modular switches: Anchor Roma or equivalent', 63),
(533, 'Fittings', 'DG Backup', '? 2-bedroom: 2 kW  ? 3-bedroom: 3 kW  ? 4-bedroom: 4 kW', 63),
(534, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 63),
(535, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper  ? Internal walls: Acrylic Emulsion paint', 63),
(536, 'Flooring', 'Common Area', '? Waiting lounge/Reception:Granite / Vitrified / Marble', 64),
(537, 'Flooring', 'Staircase', '? Vitrified tiles', 64),
(538, 'Flooring', 'Lift lobby & Corridors', '? Vitrified tiles', 64),
(539, 'Flooring', 'Apartment', '? Vitrified tiles', 64),
(540, 'Flooring', 'Kitchen', '? Vitrified tiles', 64),
(541, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 64),
(542, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 64),
(543, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 64),
(544, 'Flooring', 'Bathrooms', '? Ceramic tiles', 64),
(545, 'Fittings', 'Kitchen', 'Provision for modular kitchen  ? Provision for water purifier point, refrigerator point, microwave point, stainless steel sink single bowl', 64),
(546, 'Fittings', 'Bathrooms', 'CP fittings, sanitary fittings & accessories: Kohler or equivalent', 64),
(547, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent) ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill', 64),
(548, 'Fittings', 'Air Conditioning', '? Provision for split air-conditioning in living and bedrooms', 64),
(549, 'Fittings', 'Power Supply', '? 2-bedroom: 2 kW  ? 3-bedroom: 5 kW   ? 4-bedroom: 6 kW  ? Modular switches: Anchor Roma or equivalent', 64),
(550, 'Fittings', 'DG Backup', '? 2-bedroom: 2 kW  ? 3-bedroom: 3 kW  ? 4-bedroom: 4 kW', 64),
(551, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 64),
(552, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper  ? Internal walls: Acrylic Emulsion paint', 64),
(553, 'Flooring', 'Kitchen', '? Vitrified Tiles', 65),
(554, 'Flooring', 'Master Bedroom', '? Hardwood Flooring', 65),
(555, 'Flooring', 'Other Bedrooms', '? Laminated Wood / Vitrified Tiles', 65),
(556, 'Flooring', 'Balconies / Decks', '? Anti-skid / Vitrified Tiles', 65),
(557, 'Flooring', 'All Toilets ', '? Anti-Skid Vitrified Tiles', 65),
(558, 'Fittings', 'Kitchen', '? Provision for double bowl sink', 65),
(559, 'Fittings', 'Utility', '? Deep Bowl Stainless Steel Sink with Drain Board', 65),
(560, 'Fittings', 'CP fittings', '? Kohler/ Artize or equivalent', 65),
(561, 'Fittings', 'Sanitary fittings', '? Kohler / Roca/American standard or Equivalent make', 65),
(562, 'Fittings', 'Accessories', '? Jaquar or equivalent', 65),
(563, 'Fittings', 'Bath tub in Master bedroom', '? Roca or equivalent', 65),
(564, 'Fittings', 'EWC', '? Kohler/ Artize or equivalent', 65),
(565, 'Fittings', 'Doors & Windows', '? Entrance Door - Teak wood frame with teak wood panel Shutter  ?  Internal Door - Lacquered PU finished solid wood frame; architrave and shutter with masonite skin solid core on both sides ? External Deck & Balcony Door - Aluminium/UPVC doors with glazing (Domal or equivalent)  ? Windows - Aluminium/ UPVC with glazing (Domal or equivalent)', 65),
(566, 'Fittings', 'Power Supply', '10 KW', 65),
(567, 'Walls', 'Exterior', 'External Finishes - Textured Paint / Cladding', 65),
(568, 'Walls', 'Interior', 'Internal Wall & Ceiling - Acrylic Emulsion Paint', 65),
(569, 'Flooring', 'Kitchen', '? Vitrified Tiles', 66),
(570, 'Flooring', 'Master Bedroom', '? Hardwood Flooring', 66),
(571, 'Flooring', 'Other Bedrooms', '? Laminated Wood / Vitrified Tiles', 66),
(572, 'Flooring', 'Balconies / Decks', '? Anti-skid / Vitrified Tiles', 66),
(573, 'Flooring', 'All Toilets ', '? Anti-Skid Vitrified Tiles', 66),
(574, 'Fittings', 'Kitchen', '? Provision for double bowl sink', 66),
(575, 'Fittings', 'Utility', '? Deep Bowl Stainless Steel Sink with Drain Board', 66),
(576, 'Fittings', 'CP fittings', '? Kohler/ Artize or equivalent', 66),
(577, 'Fittings', 'Sanitary fittings', '? Kohler / Roca/American standard or Equivalent make', 66),
(578, 'Fittings', 'Accessories', '? Jaquar or equivalent', 66),
(579, 'Fittings', 'Bath tub in Master bedroom', '? Roca or equivalent', 66),
(580, 'Fittings', 'EWC', '? Kohler/ Artize or equivalent', 66),
(581, 'Fittings', 'Doors & Windows', '? Entrance Door - Teak wood frame with teak wood panel Shutter  ?  Internal Door - Lacquered PU finished solid wood frame; architrave and shutter with masonite skin solid core on both sides ? External Deck & Balcony Door - Aluminium/UPVC doors with glazing (Domal or equivalent)  ? Windows - Aluminium/ UPVC with glazing (Domal or equivalent)', 66),
(582, 'Fittings', 'Power Supply', '10 KW', 66),
(583, 'Walls', 'Exterior', 'External Finishes - Textured Paint / Cladding', 66),
(584, 'Walls', 'Interior', 'Internal Wall & Ceiling - Acrylic Emulsion Paint', 66),
(585, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Marble  ? Staircase: Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 67),
(586, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Vitrified tiles', 67),
(587, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 67),
(588, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 67),
(589, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 67),
(590, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 67),
(591, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles', 67),
(592, 'Flooring', 'Other toilets', 'Ceramic tiles', 67),
(593, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 67),
(594, 'Fittings', 'Bathrooms', '? CP fittings: Jaguar/ Ess Ess or equivalent', 67),
(595, 'Fittings', 'Sanitary fittings', '? Parryware Cascade or equivalent', 67),
(596, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish  ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill ', 67),
(597, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 67),
(598, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper', 67),
(599, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Marble  ? Staircase: Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 68),
(600, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Vitrified tiles', 68),
(601, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 68),
(602, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 68),
(603, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 68),
(604, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 68),
(605, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles', 68),
(606, 'Flooring', 'Other toilets', 'Ceramic tiles', 68),
(607, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 68),
(608, 'Fittings', 'Bathrooms', '? CP fittings: Jaguar/ Ess Ess or equivalent', 68),
(609, 'Fittings', 'Sanitary fittings', '? Parryware Cascade or equivalent', 68),
(610, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish  ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill ', 68),
(611, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 68),
(612, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper', 68),
(613, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Marble  ? Staircase: Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 69),
(614, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Vitrified tiles', 69),
(615, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 69),
(616, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 69),
(617, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 69),
(618, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 69),
(619, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles', 69),
(620, 'Flooring', 'Other toilets', 'Ceramic tiles', 69),
(621, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 69),
(622, 'Fittings', 'Bathrooms', '? CP fittings: Jaguar/ Ess Ess or equivalent', 69),
(623, 'Fittings', 'Sanitary fittings', '? Parryware Cascade or equivalent', 69),
(624, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish  ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill ', 69),
(625, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 69),
(626, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper', 69),
(627, 'Flooring', 'Common Area', '? Waiting lounge/Reception: Marble  ? Staircase: Vitrified tiles  ? Lift lobby & Corridors: Vitrified tiles', 70),
(628, 'Flooring', 'Apartment', '? Living/Dining/Family/Foyer: Vitrified tiles', 70),
(629, 'Flooring', 'Master bedroom', '? Laminated wooden flooring', 70),
(630, 'Flooring', 'Other bedrooms', '? Vitrified tiles', 70),
(631, 'Flooring', 'Balcony/Deck', '? Antiskid ceramic tiles', 70),
(632, 'Flooring', 'Kitchen & Utility', '? Vitrified tiles', 70),
(633, 'Flooring', 'Master bedroom toilet', '? Ceramic tiles', 70),
(634, 'Flooring', 'Other toilets', 'Ceramic tiles', 70),
(635, 'Fittings', 'Kitchen', '? Provision for modular kitchen', 70),
(636, 'Fittings', 'Bathrooms', '? CP fittings: Jaguar/ Ess Ess or equivalent', 70),
(637, 'Fittings', 'Sanitary fittings', '? Parryware Cascade or equivalent', 70),
(638, 'Fittings', 'Doors & Windows', '? Main door to the apartment: Teakwood frame with designer shutter (Masonite or equivalent)  ? Bedroom doors: Hardwood frame with painted flush shutter ? Bathroom doors: Hardwood frame with flush shutters ? resin coated and painted finish  ? Balcony door: UPVC/Aluminium with 2 ? -track bug screen ? Windows: UPVC/Anodised aluminium with bug screen and safety grill ', 70),
(639, 'Walls', 'Exterior', '? External texture paint external grade emulsion', 70),
(640, 'Walls', 'Interior', '? Internal ceilings: Oil Bound Distemper', 70),
(641, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 73),
(642, 'Flooring', 'Bedrooms', '? Laminated wooden flooring in master bed room only. ? Other bedrooms ? Superior quality vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 73),
(643, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling. ? Plastic emulsion paint for ceiling.', 73),
(644, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Granite vanity counters in all toilets except Servant?s toilet.', 73),
(645, 'Flooring', 'Balconies/Utilities', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet. ? Plastic emulsion paint for ceiling. ? All walls painted in textured pain', 73),
(646, 'Flooring', 'Utility Room', '? Superior quality ceramic tile flooring. ? Plastic emulsion paint for walls & ceiling.', 73),
(647, 'Flooring', 'Staircase', '? Cement concrete for Treads & Risers ? MS handrail as per design.', 73),
(648, 'Flooring', 'Common Area', '? Granite flooring ? Superior quality ceramic tile cladding up to ceiling / False ceiling. ? Plastic emulsion paint for ceiling. ? Granite coping for parapet/MS handrail as per design.', 73),
(649, 'Flooring', 'Doors and Windows', '? Main door and bedroom doors of both sides masonite skin, with timber frame & architraves. ? Toilet door of timber frame and architraves. Shutter with masonite skin on the external side and laminate on the internal side. ? All other external doors to be manufactured in specially designed heavy duty aluminium extruded frames. ? Heavy-duty aluminium windows made from specially designed and manufactured sections. ? Servant toilet door to be manufactured in specially designed aluminium extruded frames.', 73),
(650, 'Fittings', 'Lifts', '? Total no. of 4 lifts of reputed make.', 73),
(651, 'Fittings', 'Electrical', '? Split AC provision in living room and all bedrooms. ? BESCOM power supply: 7 kW in all apartments. ? Generator power back up of 3 kW for apartments and 100% power back up for common area facilities. ? Exhaust fans in kitchen and toilets. ? Television points in living and all bedrooms. ? Telephone points in living and all bedrooms. ? Intercom facility from security cabin to each apartment.', 73),
(652, 'Fittings', 'Plumbing and Sanitary', '? Sanitary fixtures of reputed make in all toilets. ? Chromium plated fittings of reputed make in all toilets. ? Stainless steel single bowl sink with drain board in utility. ? 25 litre capacity geyser in all toilets except powder room ? 15 litre capacity geyser in servant toilet', 73),
(653, 'Walls', 'Painting', '? Textured Paint for Walls. ? Plastic emulsion paint for ceiling', 73),
(654, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 74),
(655, 'Flooring', 'Bedrooms', '? Laminated wooden flooring in master bed room only. ? Other bedrooms ? Superior quality vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 74),
(656, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling. ? Plastic emulsion paint for ceiling.', 74),
(657, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Granite vanity counters in all toilets except Servant?s toilet.', 74),
(658, 'Flooring', 'Balconies/Utilities', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet. ? Plastic emulsion paint for ceiling. ? All walls painted in textured pain', 74),
(659, 'Flooring', 'Utility Room', '? Superior quality ceramic tile flooring. ? Plastic emulsion paint for walls & ceiling.', 74),
(660, 'Flooring', 'Staircase', '? Cement concrete for Treads & Risers ? MS handrail as per design.', 74),
(661, 'Flooring', 'Common Area', '? Granite flooring ? Superior quality ceramic tile cladding up to ceiling / False ceiling. ? Plastic emulsion paint for ceiling. ? Granite coping for parapet/MS handrail as per design.', 74),
(662, 'Flooring', 'Doors and Windows', '? Main door and bedroom doors of both sides masonite skin, with timber frame & architraves. ? Toilet door of timber frame and architraves. Shutter with masonite skin on the external side and laminate on the internal side. ? All other external doors to be manufactured in specially designed heavy duty aluminium extruded frames. ? Heavy-duty aluminium windows made from specially designed and manufactured sections. ? Servant toilet door to be manufactured in specially designed aluminium extruded frames.', 74),
(663, 'Fittings', 'Lifts', '? Total no. of 4 lifts of reputed make.', 74),
(664, 'Fittings', 'Electrical', '? Split AC provision in living room and all bedrooms. ? BESCOM power supply: 7 kW in all apartments. ? Generator power back up of 3 kW for apartments and 100% power back up for common area facilities. ? Exhaust fans in kitchen and toilets. ? Television points in living and all bedrooms. ? Telephone points in living and all bedrooms. ? Intercom facility from security cabin to each apartment.', 74),
(665, 'Fittings', 'Plumbing and Sanitary', '? Sanitary fixtures of reputed make in all toilets. ? Chromium plated fittings of reputed make in all toilets. ? Stainless steel single bowl sink with drain board in utility. ? 25 litre capacity geyser in all toilets except powder room ? 15 litre capacity geyser in servant toilet', 74),
(666, 'Walls', 'Painting', '? Textured Paint for Walls. ? Plastic emulsion paint for ceiling', 74),
(667, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 75),
(668, 'Flooring', 'Bedrooms', '? Laminated wooden flooring in master bed room only. ? Other bedrooms ? Superior quality vitrified tile flooring and skirting. ? Plastic emulsion paint for walls and ceiling.', 75),
(669, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling. ? Plastic emulsion paint for ceiling.', 75),
(670, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Granite vanity counters in all toilets except Servant?s toilet.', 75),
(671, 'Flooring', 'Balconies/Utilities', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet. ? Plastic emulsion paint for ceiling. ? All walls painted in textured pain', 75),
(672, 'Flooring', 'Utility Room', '? Superior quality ceramic tile flooring. ? Plastic emulsion paint for walls & ceiling.', 75),
(673, 'Flooring', 'Staircase', '? Cement concrete for Treads & Risers ? MS handrail as per design.', 75),
(674, 'Flooring', 'Common Area', '? Granite flooring ? Superior quality ceramic tile cladding up to ceiling / False ceiling. ? Plastic emulsion paint for ceiling. ? Granite coping for parapet/MS handrail as per design.', 75),
(675, 'Flooring', 'Doors and Windows', '? Main door and bedroom doors of both sides masonite skin, with timber frame & architraves. ? Toilet door of timber frame and architraves. Shutter with masonite skin on the external side and laminate on the internal side. ? All other external doors to be manufactured in specially designed heavy duty aluminium extruded frames. ? Heavy-duty aluminium windows made from specially designed and manufactured sections. ? Servant toilet door to be manufactured in specially designed aluminium extruded frames.', 75),
(676, 'Fittings', 'Lifts', '? Total no. of 4 lifts of reputed make.', 75),
(677, 'Fittings', 'Electrical', '? Split AC provision in living room and all bedrooms. ? BESCOM power supply: 7 kW in all apartments. ? Generator power back up of 3 kW for apartments and 100% power back up for common area facilities. ? Exhaust fans in kitchen and toilets. ? Television points in living and all bedrooms. ? Telephone points in living and all bedrooms. ? Intercom facility from security cabin to each apartment.', 75),
(678, 'Fittings', 'Plumbing and Sanitary', '? Sanitary fixtures of reputed make in all toilets. ? Chromium plated fittings of reputed make in all toilets. ? Stainless steel single bowl sink with drain board in utility. ? 25 litre capacity geyser in all toilets except powder room ? 15 litre capacity geyser in servant toilet', 75),
(679, 'Walls', 'Painting', '? Textured Paint for Walls. ? Plastic emulsion paint for ceiling', 75),
(680, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Natural / Engineered stone slab /vitrified tile flooring and skirting. ', 82),
(681, 'Flooring', 'Bedrooms', '? Superior quality timber laminated flooring and skirting / equivalent flooring for Master Bedroom.  ? Superior quality vitrified tile flooring and skirting in other bedrooms.', 82),
(682, 'Flooring', 'Toilets', '? Superior quality antiskid ceramic tile flooring.  ? Superior quality ceramic wall tiling up to false ceiling.  ? False ceiling with grid panel.  ? Granite vanity counters in all toilets except servant toilet.  ? Shower Partition in attached toilets .', 82),
(683, 'Flooring', 'Kitchen', '? Natural / Engineered stone slab / vitrified tile flooring  ? Superior quality ceramic tiling dado from floor to ceiling soffit. ', 82),
(684, 'Flooring', 'Balconies / Utilities', '? Superior quality antiskid ceramic tile flooring and skirting. ? Granite coping for parapet / Mild steel handrail ? Plastic emulsion paint for ceiling.', 82),
(685, 'Flooring', 'Domestic help room', '? Superior quality ceramic tile flooring.', 82),
(686, 'Flooring', 'Staircase (Fire Exit Staircase)', '? Concrete treads & risers.', 82),
(687, 'Flooring', 'Common Area', '? Vitrified tile / Granite flooring.  ? Superior quality ceramic tile cladding up to ceiling.', 82),
(688, 'Fittings', 'Main Door/ Bedroom Doors', '? Frame - Timber ? Architrave - Timber ? Shutters - with both side masonite skin.', 82),
(689, 'Fittings', 'Toilet Doors', '? Frame - Timber ? Architrave - Timber ? Shutters with outside masonite and inside laminate ? All other external doors to be manufactured in specially designed aluminium extruded frames and shutter with panels. Heavy-duty aluminium glazed Sliding windows/ top hung windows & French windows made from specially designed and manufactured sections.', 82),
(690, 'Fittings', 'Lifts', '? Total of 10 nos elevators of reputed make (2 / Block )', 82),
(691, 'Fittings', 'Plumbing & Sanitary', '? Sanitary fixtures of reputed make in all toilets ? Chromium plated fittings of reputed make in all toilets ? 25 litre capacity geyser in all toilets ? 15 litre capacity geyser in servant toilet ? Stainless steel single bowl sink with drain board in utility', 82),
(692, 'Fittings', 'Electrical', '? Split AC provision in living room and all bedrooms ? BESCOM power supply: 10 KW 3 phase supply for 3 bedroom unit, 12 KW 3 phase supply for 4 bedroom unit ? Standby power of 3KW for apartments and 100% power backup for common area facilities ? Exhaust fans in Kitchen and toilets ? Television points in living, family, all bedrooms and servant room ? Telephone points in all bed rooms, living and family room ? Intercom facility from security cabin to each apartment', 82),
(693, 'Walls', 'Foyer / Living / Dinning', ' ? Plastic emulsion paint for walls and ceiling.', 82),
(694, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling.', 82),
(695, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling.', 82),
(696, 'Walls', 'Balconies / Utilities', '? Plastic emulsion paint for ceiling. ? All walls external grade textured paint.', 82),
(697, 'Walls', 'Staircase (Fire Exit Staircase)', '? Plastic Emulsion Paint for ceiling.', 82),
(698, 'Walls', 'Common Area', '? Plastic emulsion for ceiling.', 82),
(699, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Natural / Engineered stone slab /vitrified tile flooring and skirting. ', 83),
(700, 'Flooring', 'Bedrooms', '? Superior quality timber laminated flooring and skirting / equivalent flooring for Master Bedroom.  ? Superior quality vitrified tile flooring and skirting in other bedrooms.', 83),
(701, 'Flooring', 'Toilets', '? Superior quality antiskid ceramic tile flooring.  ? Superior quality ceramic wall tiling up to false ceiling.  ? False ceiling with grid panel.  ? Granite vanity counters in all toilets except servant toilet.  ? Shower Partition in attached toilets .', 83),
(702, 'Flooring', 'Kitchen', '? Natural / Engineered stone slab / vitrified tile flooring  ? Superior quality ceramic tiling dado from floor to ceiling soffit. ', 83),
(703, 'Flooring', 'Balconies / Utilities', '? Superior quality antiskid ceramic tile flooring and skirting. ? Granite coping for parapet / Mild steel handrail ? Plastic emulsion paint for ceiling.', 83),
(704, 'Flooring', 'Domestic help room', '? Superior quality ceramic tile flooring.', 83),
(705, 'Flooring', 'Staircase (Fire Exit Staircase)', '? Concrete treads & risers.', 83),
(706, 'Flooring', 'Common Area', '? Vitrified tile / Granite flooring.  ? Superior quality ceramic tile cladding up to ceiling.', 83),
(707, 'Fittings', 'Main Door/ Bedroom Doors', '? Frame - Timber ? Architrave - Timber ? Shutters - with both side masonite skin.', 83),
(708, 'Fittings', 'Toilet Doors', '? Frame - Timber ? Architrave - Timber ? Shutters with outside masonite and inside laminate ? All other external doors to be manufactured in specially designed aluminium extruded frames and shutter with panels. Heavy-duty aluminium glazed Sliding windows/ top hung windows & French windows made from specially designed and manufactured sections.', 83),
(709, 'Fittings', 'Lifts', '? Total of 10 nos elevators of reputed make (2 / Block )', 83),
(710, 'Fittings', 'Plumbing & Sanitary', '? Sanitary fixtures of reputed make in all toilets ? Chromium plated fittings of reputed make in all toilets ? 25 litre capacity geyser in all toilets ? 15 litre capacity geyser in servant toilet ? Stainless steel single bowl sink with drain board in utility', 83),
(711, 'Fittings', 'Electrical', '? Split AC provision in living room and all bedrooms ? BESCOM power supply: 10 KW 3 phase supply for 3 bedroom unit, 12 KW 3 phase supply for 4 bedroom unit ? Standby power of 3KW for apartments and 100% power backup for common area facilities ? Exhaust fans in Kitchen and toilets ? Television points in living, family, all bedrooms and servant room ? Telephone points in all bed rooms, living and family room ? Intercom facility from security cabin to each apartment', 83),
(712, 'Walls', 'Foyer / Living / Dinning', ' ? Plastic emulsion paint for walls and ceiling.', 83),
(713, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling.', 83),
(714, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling.', 83),
(715, 'Walls', 'Balconies / Utilities', '? Plastic emulsion paint for ceiling. ? All walls external grade textured paint.', 83),
(716, 'Walls', 'Staircase (Fire Exit Staircase)', '? Plastic Emulsion Paint for ceiling.', 83),
(717, 'Walls', 'Common Area', '? Plastic emulsion for ceiling.', 83),
(718, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Natural / Vitrified Tile/ Engineered stone flooring and skirting', 84),
(719, 'Flooring', 'Bedrooms', '? Superior quality timber laminated wooden flooring and skirting / equivalent in Master bed room only ? Other bedrooms - Superior quality vitrified tile flooring and skirting', 84),
(720, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring ? Superior quality ceramic wall tiling up to false ceiling ? False ceiling ? Granite vanity counters in all toilets except maid?s toilets ? Glass shower partition in Master bedroom toilets ? Glass shower partitions for all toilets in duplex units only', 84),
(721, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring ? Superior quality ceramic wall tiling up to ceiling', 84),
(722, 'Flooring', 'Utility', '? Superior quality ceramic tile flooring and skirting ? MS handrail as per design/ Granite coping for parapet', 84),
(723, 'Flooring', 'Utility Room', '? Superior quality ceramic tile flooring', 84),
(724, 'Flooring', 'Utility Toilet', '? Ceramic tile flooring ? Ceramic wall tiling up to false ceiling ? False ceiling with grid panels', 84),
(725, 'Flooring', 'Internal Staircase', '? Granite Treads and Risers ? MS Handrail as per design', 84),
(726, 'Flooring', 'Private Garden', '? Soil filling with Lawn ? M.S railing as per design/ parapet with granite coping', 84),
(727, 'Flooring', 'Private Terrace', '? Superior quality ceramic tile flooring and skirting ? MS handrail as per design/ Granite coping for parapet', 84),
(728, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring ? Superior quality ceramic tile cladding up to ceiling / false ceiling ? Plastic emulsion paint for ceiling ? Granite coping for parapet/MS handrail as per design Staircase ? Cement concrete for Treads & Risers ? MS handrail as per design', 84),
(729, 'Fittings', 'Main Door/ Bedroom Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ? with both side Masonite skin', 84),
(730, 'Fittings', ' Toilet Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ? with outside Masonite and inside laminate All other external doors to be manufactured in specially designed heavy-duty powder coated aluminum extruded frames and shutter with panels Windows/Ventilators ? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections', 84),
(731, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling', 84),
(732, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling', 84),
(733, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling', 84),
(734, 'Walls', 'Utility', ' ? Plastic emulsion paint for ceiling wherever applicable ? All walls painted in textured paint ? False ceiling as per design', 84),
(735, 'Walls', 'Utility Room', '? Plastic emulsion paint for walls & ceiling', 84),
(736, 'Walls', 'Internal Staircase', '? Plastic emulsion paint for wall and ceiling', 84),
(737, 'Walls', 'Private Terrace', '? All walls painted in textured paint', 84),
(738, 'Walls', 'Common Area', '  ? Plastic emulsion paint for Walls ? Plastic emulsion paint for ceiling', 84),
(739, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Natural / Vitrified Tile/ Engineered stone flooring and skirting', 85),
(740, 'Flooring', 'Bedrooms', '? Superior quality timber laminated wooden flooring and skirting / equivalent in Master bed room only ? Other bedrooms - Superior quality vitrified tile flooring and skirting', 85),
(741, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring ? Superior quality ceramic wall tiling up to false ceiling ? False ceiling ? Granite vanity counters in all toilets except maid?s toilets ? Glass shower partition in Master bedroom toilets ? Glass shower partitions for all toilets in duplex units only', 85),
(742, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring ? Superior quality ceramic wall tiling up to ceiling', 85),
(743, 'Flooring', 'Utility', '? Superior quality ceramic tile flooring and skirting ? MS handrail as per design/ Granite coping for parapet', 85),
(744, 'Flooring', 'Utility Room', '? Superior quality ceramic tile flooring', 85),
(745, 'Flooring', 'Utility Toilet', '? Ceramic tile flooring ? Ceramic wall tiling up to false ceiling ? False ceiling with grid panels', 85),
(746, 'Flooring', 'Internal Staircase', '? Granite Treads and Risers ? MS Handrail as per design', 85),
(747, 'Flooring', 'Private Garden', '? Soil filling with Lawn ? M.S railing as per design/ parapet with granite coping', 85),
(748, 'Flooring', 'Private Terrace', '? Superior quality ceramic tile flooring and skirting ? MS handrail as per design/ Granite coping for parapet', 85),
(749, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring ? Superior quality ceramic tile cladding up to ceiling / false ceiling ? Plastic emulsion paint for ceiling ? Granite coping for parapet/MS handrail as per design Staircase ? Cement concrete for Treads & Risers ? MS handrail as per design', 85),
(750, 'Fittings', 'Main Door/ Bedroom Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ? with both side Masonite skin', 85),
(751, 'Fittings', ' Toilet Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ? with outside Masonite and inside laminate All other external doors to be manufactured in specially designed heavy-duty powder coated aluminum extruded frames and shutter with panels Windows/Ventilators ? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections', 85),
(752, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling', 85),
(753, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling', 85),
(754, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling', 85),
(755, 'Walls', 'Utility', ' ? Plastic emulsion paint for ceiling wherever applicable ? All walls painted in textured paint ? False ceiling as per design', 85),
(756, 'Walls', 'Utility Room', '? Plastic emulsion paint for walls & ceiling', 85),
(757, 'Walls', 'Internal Staircase', '? Plastic emulsion paint for wall and ceiling', 85),
(758, 'Walls', 'Private Terrace', '? All walls painted in textured paint', 85),
(759, 'Walls', 'Common Area', '  ? Plastic emulsion paint for Walls ? Plastic emulsion paint for ceiling', 85),
(771, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting.', 87),
(772, 'Flooring', ' Bedrooms', '? Superior quality vitrified tile flooring and skirting.', 87),
(773, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Vanity counters for wash basin.', 87),
(774, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling. ? Plastic emulsion paint for ceiling.', 87),
(775, 'Flooring', 'French Balconies/Utility', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet. ? Plastic emulsion paints for ceiling wherever applicable.', 87),
(776, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring. ? Superior quality ceramic tile cladding up to ceiling / False ceiling.', 87),
(777, 'Flooring', 'Staircase', '? Granite for Treads & Risers', 87),
(778, 'Fittings', 'Main Door/ Bedroom Doors', ' Frame ? Timber ? Architrave - Timber ? Shutters ? with both side masonite skin.', 87),
(779, 'Fittings', ' Toilet Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ?with outside masonite and inside laminate Windows/Ventilators ? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections.', 87),
(780, 'Fittings', ' Electrical', '? Providing Light point, Ceiling Fan point, Call Bell Point & Distribution Board in respective areas ? 5KW Single Phase Supply for 3 Bed Room Flats. ? Standby power (Generator back up) of 1 Kilo Watt ? 100% standby power (Generator back up) for common facilities. ? Power connection for split AC in Master Bedroom. ? One telephone point provided in living and one of the Bedrooms? Intercom facility provided from security cabin to each apartment. (Only point). ? Providing and fixing the exhaust fans in kitchen & all the toilets', 87),
(781, 'Fittings', 'Plumbing Specifications', '? Internal water supply ? Internal foul drainage & waste system ? Drainage, waste pipes system ? Water supply in Shaft / Terrace/ Basement high level / External areas: ? External drainage pipes ? External rain water pipes ? Sanitary fixtures in each Toilet (except Servant Toilet)', 87),
(782, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting.', 86),
(783, 'Flooring', ' Bedrooms', '? Superior quality vitrified tile flooring and skirting.', 86),
(784, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Vanity counters for wash basin.', 86),
(785, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling. ? Plastic emulsion paint for ceiling.', 86),
(786, 'Flooring', 'French Balconies/Utility', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet. ? Plastic emulsion paints for ceiling wherever applicable.', 86),
(787, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring. ? Superior quality ceramic tile cladding up to ceiling / False ceiling.', 86),
(788, 'Flooring', 'Staircase', '? Granite for Treads & Risers', 86),
(789, 'Fittings', 'Main Door/ Bedroom Doors', ' Frame ? Timber ? Architrave - Timber ? Shutters ? with both side masonite skin.', 86),
(790, 'Fittings', ' Toilet Doors', '? Frame ? Timber ? Architrave ? Timber ? Shutters ?with outside masonite and inside laminate Windows/Ventilators ? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections.', 86),
(791, 'Fittings', ' Electrical', '? Providing Light point, Ceiling Fan point, Call Bell Point & Distribution Board in respective areas ? 5KW Single Phase Supply for 3 Bed Room Flats. ? Standby power (Generator back up) of 1 Kilo Watt ? 100% standby power (Generator back up) for common facilities. ? Power connection for split AC in Master Bedroom. ? One telephone point provided in living and one of the Bedrooms? Intercom facility provided from security cabin to each apartment. (Only point). ? Providing and fixing the exhaust fans in kitchen & all the toilets', 86),
(792, 'Fittings', 'Plumbing Specifications', '? Internal water supply ? Internal foul drainage & waste system ? Drainage, waste pipes system ? Water supply in Shaft / Terrace/ Basement high level / External areas: ? External drainage pipes ? External rain water pipes ? Sanitary fixtures in each Toilet (except Servant Toilet)', 86),
(793, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling.', 86),
(794, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling.', 86),
(795, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling.', 86),
(796, 'Walls', 'French Balconies/Utility', '? Plastic emulsion paints for ceiling wherever applicable. ? All walls painted in textured paint.', 86),
(797, 'Walls', 'Common Area', '? Plastic emulsion paint for ceiling.', 86),
(798, 'Walls', 'Staircase', '? Plastic emulsion paint for Walls.', 86),
(799, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting.', 88),
(800, 'Flooring', 'Bedrooms', '? Superior quality vitrified tile flooring and skirting.', 88),
(801, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Vanity counter for wash basin.', 88),
(802, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling.', 88),
(803, 'Flooring', 'French Balconies / Utilities', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet.', 88),
(804, 'Flooring', 'Main Door/ Bedroom Doors', '? Frame - Timber ? Architrave - Timber ? Shutters - with both side masonite skin. ? Toilet Doors ? Frame - Timber ? Architrave - Timber ? Shutters - with outside masonite and inside laminate ? All other external doors to be manufactured in specially designed heavy-duty powder coated aluminum extruded frames and shutter with panels.', 88),
(805, 'Flooring', ' Windows/Ventilators', '? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections.', 88),
(806, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring. ? Superior quality ceramic tile cladding up to ceiling / False ceiling.', 88),
(807, 'Flooring', 'Staircase', '? Cement concrete for Treads & Risers', 88),
(808, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling.', 88),
(809, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling.', 88),
(810, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling.', 88),
(811, 'Walls', 'French Balconies / Utilities', '? Plastic emulsion paint for ceiling wherever applicable. ? All walls painted in textured paint.', 88),
(812, 'Walls', 'Common Area', '? Plastic emulsion paint for ceiling.', 88),
(813, 'Walls', 'Staircase', '? Plastic emulsion paint for Walls. ? Plastic emulsion paint for ceiling', 88),
(814, 'Flooring', 'Foyer / Living / Dining', '? Superior quality Vitrified tile flooring and skirting.', 89),
(815, 'Flooring', 'Bedrooms', '? Superior quality vitrified tile flooring and skirting.', 89),
(816, 'Flooring', 'Toilets', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to false ceiling. ? False ceiling with grid panels ? Vanity counter for wash basin.', 89),
(817, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring. ? Superior quality ceramic wall tiling up to ceiling.', 89),
(818, 'Flooring', 'French Balconies / Utilities', '? Superior quality ceramic tile flooring and skirting. ? MS handrail as per design/ Granite coping for parapet.', 89),
(819, 'Flooring', 'Main Door/ Bedroom Doors', '? Frame - Timber ? Architrave - Timber ? Shutters - with both side masonite skin. ? Toilet Doors ? Frame - Timber ? Architrave - Timber ? Shutters - with outside masonite and inside laminate ? All other external doors to be manufactured in specially designed heavy-duty powder coated aluminum extruded frames and shutter with panels.', 89),
(820, 'Flooring', ' Windows/Ventilators', '? Heavy-duty powder coated aluminum glazed sliding windows made from specially designed and manufactured sections.', 89),
(821, 'Flooring', 'Common Area', '? Granite/ Vitrified / equivalent flooring. ? Superior quality ceramic tile cladding up to ceiling / False ceiling.', 89),
(822, 'Flooring', 'Staircase', '? Cement concrete for Treads & Risers', 89),
(823, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling.', 89),
(824, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling.', 89),
(825, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling.', 89),
(826, 'Walls', 'French Balconies / Utilities', '? Plastic emulsion paint for ceiling wherever applicable. ? All walls painted in textured paint.', 89),
(827, 'Walls', 'Common Area', '? Plastic emulsion paint for ceiling.', 89),
(828, 'Walls', 'Staircase', '? Plastic emulsion paint for Walls. ? Plastic emulsion paint for ceiling', 89),
(829, 'Flooring', 'Foyer / Living / Dining', '? Superior quality vitrified tile flooring and skirting', 79),
(830, 'Flooring', 'Bedrooms', '? Superior quality timber laminated flooring / equivalent and skirting for Master Bedroom ? Superior quality vitrified tile flooring and skirting in other bedrooms', 79),
(831, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring ? Superior quality ceramic tiling dado from floor to ceiling soffit', 79),
(832, 'Flooring', 'Toilets', '? Superior quality antiskid ceramic tile flooring ? Superior quality ceramic wall tiling up to false ceiling ? Granite vanity counters in all toilets except servant toilet', 79),
(833, 'Flooring', 'Balconies / Private terrace', '? Superior quality antiskid ceramic tile flooring and skirting ? Granite coping for parapet / Mild steel handrail', 79),
(834, 'Flooring', 'Servant Room', '? Superior quality ceramic tile flooring', 79),
(835, 'Flooring', 'Staircase ', '? Granite/ equivalent for treads & risers', 79),
(836, 'Flooring', 'Common Area', '? Granite / equivalent flooring ? Superior quality ceramic tile cladding up to ceiling  ? Granite coping for parapet / Mild steel handrail', 79),
(837, 'Fittings', 'Main Door/ Bedroom Doors', '? Main door and Bedroom doors of both sides masonite skin, with Timber frame & architraves ? All other external doors to be manufactured in specially designed heavy-duty aluminium extruded frames ? Toilet door of timber frame and architraves. Shutter with Masonite skin on the external side and laminate on the internal side ? Servant toilet door to be manufactured in specially designed aluminium extruded frames  ? High quality ironmongery and fittings for all doors', 79),
(838, 'Fittings', 'Lifts', '? Total no. of 3 lifts of reputed make ? Capacity ? 2 no?s of 8- passengers & 1 no?s of 14 to 16-passengers ? Shutters ?with outside masonite and inside laminate', 79),
(839, 'Fittings', 'Plumbing & Sanitary', '? Sanitary fixtures of reputed make in all toilets ? Chromium plated fittings of reputed make in all toilets ? Stainless steel single bowl sink with drain board in utility', 79),
(840, 'Fittings', 'Electrical', '? BESCOM power supply of three phase 7 Kilo Watt per flat ? Stand by power (Generator back up) of 3 Kilo Watts for each unit ? 100% stand by power (Generator back up) for common facilities ? Provision for AC in all bedrooms & living ? Intercom facility provided from security cabin to each apartment', 79),
(841, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling', 79),
(842, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling', 79),
(843, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling', 79),
(844, 'Walls', 'Toilets', '? False ceiling / Plastic emulsion paint for ceiling', 79),
(845, 'Walls', 'Balconies / Private terrace', '? Plastic emulsion paint for ceiling ? All walls external grade textured paint', 79),
(846, 'Walls', 'Servant Room', '? Plastic emulsion paint for walls & ceiling', 79),
(847, 'Walls', 'Staircase', '? Texture Paint for walls and Plastic emulsion for ceiling', 79),
(848, 'Walls', 'Common Area', '? Plastic emulsion for ceiling', 79),
(849, 'Flooring', 'Foyer / Living / Dining', '? Superior quality vitrified tile flooring and skirting', 80),
(850, 'Flooring', 'Bedrooms', '? Superior quality timber laminated flooring / equivalent and skirting for Master Bedroom ? Superior quality vitrified tile flooring and skirting in other bedrooms', 80),
(851, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring ? Superior quality ceramic tiling dado from floor to ceiling soffit', 80),
(852, 'Flooring', 'Toilets', '? Superior quality antiskid ceramic tile flooring ? Superior quality ceramic wall tiling up to false ceiling ? Granite vanity counters in all toilets except servant toilet', 80),
(853, 'Flooring', 'Balconies / Private terrace', '? Superior quality antiskid ceramic tile flooring and skirting ? Granite coping for parapet / Mild steel handrail', 80),
(854, 'Flooring', 'Servant Room', '? Superior quality ceramic tile flooring', 80),
(855, 'Flooring', 'Staircase ', '? Granite/ equivalent for treads & risers', 80),
(856, 'Flooring', 'Common Area', '? Granite / equivalent flooring ? Superior quality ceramic tile cladding up to ceiling  ? Granite coping for parapet / Mild steel handrail', 80),
(857, 'Fittings', 'Main Door/ Bedroom Doors', '? Main door and Bedroom doors of both sides masonite skin, with Timber frame & architraves ? All other external doors to be manufactured in specially designed heavy-duty aluminium extruded frames ? Toilet door of timber frame and architraves. Shutter with Masonite skin on the external side and laminate on the internal side ? Servant toilet door to be manufactured in specially designed aluminium extruded frames  ? High quality ironmongery and fittings for all doors', 80),
(858, 'Fittings', 'Lifts', '? Total no. of 3 lifts of reputed make ? Capacity ? 2 no?s of 8- passengers & 1 no?s of 14 to 16-passengers ? Shutters ?with outside masonite and inside laminate', 80),
(859, 'Fittings', 'Plumbing & Sanitary', '? Sanitary fixtures of reputed make in all toilets ? Chromium plated fittings of reputed make in all toilets ? Stainless steel single bowl sink with drain board in utility', 80),
(860, 'Fittings', 'Electrical', '? BESCOM power supply of three phase 7 Kilo Watt per flat ? Stand by power (Generator back up) of 3 Kilo Watts for each unit ? 100% stand by power (Generator back up) for common facilities ? Provision for AC in all bedrooms & living ? Intercom facility provided from security cabin to each apartment', 80),
(861, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling', 80),
(862, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling', 80),
(863, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling', 80),
(864, 'Walls', 'Toilets', '? False ceiling / Plastic emulsion paint for ceiling', 80),
(865, 'Walls', 'Balconies / Private terrace', '? Plastic emulsion paint for ceiling ? All walls external grade textured paint', 80),
(866, 'Walls', 'Servant Room', '? Plastic emulsion paint for walls & ceiling', 80),
(867, 'Walls', 'Staircase', '? Texture Paint for walls and Plastic emulsion for ceiling', 80),
(868, 'Walls', 'Common Area', '? Plastic emulsion for ceiling', 80),
(869, 'Flooring', 'Foyer / Living / Dining', '? Superior quality vitrified tile flooring and skirting', 81),
(870, 'Flooring', 'Bedrooms', '? Superior quality timber laminated flooring / equivalent and skirting for Master Bedroom ? Superior quality vitrified tile flooring and skirting in other bedrooms', 81),
(871, 'Flooring', 'Kitchen', '? Superior quality ceramic tile flooring ? Superior quality ceramic tiling dado from floor to ceiling soffit', 81),
(872, 'Flooring', 'Toilets', '? Superior quality antiskid ceramic tile flooring ? Superior quality ceramic wall tiling up to false ceiling ? Granite vanity counters in all toilets except servant toilet', 81),
(873, 'Flooring', 'Balconies / Private terrace', '? Superior quality antiskid ceramic tile flooring and skirting ? Granite coping for parapet / Mild steel handrail', 81);
INSERT INTO `prop_unit_interior` (`id`, `category`, `heading`, `description`, `prop_unit_id`) VALUES
(874, 'Flooring', 'Servant Room', '? Superior quality ceramic tile flooring', 81),
(875, 'Flooring', 'Staircase ', '? Granite/ equivalent for treads & risers', 81),
(876, 'Flooring', 'Common Area', '? Granite / equivalent flooring ? Superior quality ceramic tile cladding up to ceiling  ? Granite coping for parapet / Mild steel handrail', 81),
(877, 'Fittings', 'Main Door/ Bedroom Doors', '? Main door and Bedroom doors of both sides masonite skin, with Timber frame & architraves ? All other external doors to be manufactured in specially designed heavy-duty aluminium extruded frames ? Toilet door of timber frame and architraves. Shutter with Masonite skin on the external side and laminate on the internal side ? Servant toilet door to be manufactured in specially designed aluminium extruded frames  ? High quality ironmongery and fittings for all doors', 81),
(878, 'Fittings', 'Lifts', '? Total no. of 3 lifts of reputed make ? Capacity ? 2 no?s of 8- passengers & 1 no?s of 14 to 16-passengers ? Shutters ?with outside masonite and inside laminate', 81),
(879, 'Fittings', 'Plumbing & Sanitary', '? Sanitary fixtures of reputed make in all toilets ? Chromium plated fittings of reputed make in all toilets ? Stainless steel single bowl sink with drain board in utility', 81),
(880, 'Fittings', 'Electrical', '? BESCOM power supply of three phase 7 Kilo Watt per flat ? Stand by power (Generator back up) of 3 Kilo Watts for each unit ? 100% stand by power (Generator back up) for common facilities ? Provision for AC in all bedrooms & living ? Intercom facility provided from security cabin to each apartment', 81),
(881, 'Walls', 'Foyer / Living / Dining', '? Plastic emulsion paint for walls and ceiling', 81),
(882, 'Walls', 'Bedrooms', '? Plastic emulsion paint for walls and ceiling', 81),
(883, 'Walls', 'Kitchen', '? Plastic emulsion paint for ceiling', 81),
(884, 'Walls', 'Toilets', '? False ceiling / Plastic emulsion paint for ceiling', 81),
(885, 'Walls', 'Balconies / Private terrace', '? Plastic emulsion paint for ceiling ? All walls external grade textured paint', 81),
(886, 'Walls', 'Servant Room', '? Plastic emulsion paint for walls & ceiling', 81),
(887, 'Walls', 'Staircase', '? Texture Paint for walls and Plastic emulsion for ceiling', 81),
(888, 'Walls', 'Common Area', '? Plastic emulsion for ceiling', 81),
(901, 'Flooring', 'Dining Room', 'sdaf', 23),
(902, 'Flooring', 'Entrance', 'sdfsdf', 23),
(903, 'Flooring', 'Bathroom', 'sdf', 23),
(904, 'Fittings', 'sdf', 'dsf', 23),
(905, 'Walls', 'sdfs', 'sdf', 23),
(906, 'Additional Facilities', 'lift', '2', 23),
(907, 'Additional Facilities', 'floor details', '(6 out of 9)', 23),
(908, 'Flooring', 'Bedroom', 'sdfaafsdf', 28),
(909, 'Flooring', 'Kitchen', 'sdf', 28),
(910, 'Flooring', 'Living Room', 'sdf', 28),
(911, 'Flooring', 'Entrance', 'sdf', 28),
(912, 'Flooring', 'Balcony', 'f', 28),
(913, 'Fittings', 'Capentry', 'Doors & Windows: Apartment main door in teak wood jamb with teak wood shutter ', 28),
(914, 'Fittings', 'Capentry', 'Bedroom doors in lacquered PU finished soild wood fraome and architrave and shutter with masonite skin solid core both sides. windows aluminium / upvc with glazing (domal or equivlent)', 28),
(915, 'Fittings', 'Power Supply', '8KW for a 3-bedroom apartment ', 28),
(916, 'Fittings', 'Power Supply', '10Kw for a 4-Bedroom apartmemt', 28),
(917, 'Fittings', 'Stand-by power', ' 100% emergency for lifts, pumps and lighting in common areas', 28),
(918, 'Fittings', 'Stand-by power', ' 5kw for a 3-bedroom apartment', 28),
(919, 'Fittings', 'Stand-by power', '6Kw for a 4-Bedroom apartmemt', 28),
(920, 'Fittings', 'Lifts', ' Block A- 2 Passenger lifts and 1 service lift', 28),
(921, 'Fittings', 'Lifts', ' Block B- 1 passenger lift and 1 service lift', 28),
(922, 'Fittings', 'Security systems & Automation', 'Provision for basic home automation conduiting (lIghts, security, air conditioning, video door phone)', 28),
(923, 'Walls', 'Painting', 'Long lasting textured paint for exteruiors ', 28),
(924, 'Walls', 'Painting', 'Acrylic emulsion paint for internal ceilings & walls', 28),
(925, 'Additional Facilities', 'lift', '2', 28),
(926, 'Additional Facilities', 'floor details', '(6 out 9)', 28),
(970, 'Flooring', 'Common Area', '? Lifts / Lobbies & Corridors: Marble / Granite flooring in ground floor  ? Vitrified tile flooring in upper floors  ? Staircases: Vitrified tile flooring', 24),
(971, 'Flooring', 'Apartments', '? Living / Dining / Foyer: Vitrified tile flooring ?	Master Bedroom: Laminated wooden flooring  ? Other Bedrooms: Vitrified tile flooring  ? Balcony Utility: Ceramic tile flooring', 24),
(972, 'Flooring', 'Kitchen', '? Vitrified tile flooring  ? Provision for modular kitchen', 24),
(973, 'Flooring', 'Bathrooms', '? Anti-skid ceramic tile flooring  ? CP fitting: Jaguar / Equivalent  ? Sanitary Ware: Hindustan / Parryware or  Equivalent', 24),
(974, 'Fittings', 'Doors & Windows', '? Apartment Main Door: Solid wood frame with BSTV (Both Side Teack veneer) / Masonite skin shutter with melamine polishing on both sides  ? Bedroom Door: Soild core flush door & Masonite skin with hardwood frame', 24),
(975, 'Fittings', 'Electrical', '? Concealed PVC conduit with copper wiring  ? Modular switches of Anchor Roma or equivalent', 24),
(976, 'Fittings', 'Power Supply', '? 4 KW for a 2-bedroom apartment  ? 6 KW for a 3-bedroom apartment', 24),
(977, 'Fittings', 'Standby Power', '? 100% DG back up for emergency power for lifts, pumps & common lighting  ? Apartment ? DG back up  ? 2 KW for a 2-bedroom apartment ? 3 KW for a 3-bedroom apartment', 24),
(978, 'Walls', 'Exterior', '? Finish of long lasting texture paint', 24),
(979, 'Walls', '? Internal', '? 100% DG back up for emergency power for lifts, pumps & common lighting  ? Apartment ? DG back up  ? 2 KW for a 2-bedroom apartment ? 3 KW for a 3-bedroom apartment', 24),
(980, 'Additional Facilities', 'lift', '(6 out of 9)', 24),
(981, 'Additional Facilities', 'Bedroom', '4', 24),
(982, 'Availability', 'Availability', '22-02-2016', 24),
(998, 'Flooring', 'Availability', '435', 20),
(999, 'Additional Facilities', 'Lift', 'anti skid vertified tiles', 20),
(1000, 'Additional Facilities', 'green park ', '435', 20),
(1001, 'Additional Facilities', 'sdf', 'sdf', 20),
(1002, 'Availability', 'Availability', '22-02-2016', 20),
(1003, 'Availability', 'Availability', '22-02-2016', 34),
(1004, 'Availability', 'Availability', '22-02-2016', 35),
(1005, 'Availability', 'Availability', '22-02-2016', 36),
(1006, 'Flooring', 'Bedroom floor', 'Vertified Tiles', 92),
(1007, 'Flooring', 'kitchen floor', 'Vertified tiles', 92),
(1008, 'Fittings', 'Electrical', 'double layered', 92),
(1009, 'Walls', 'Exterior', 'Oil paint', 92),
(1010, 'Additional Facilities', 'Lift', '1', 92),
(1011, 'Availability', 'Availability', '05-03-2016', 92),
(1012, 'Flooring', 'sadf', 'sadf', 19),
(1013, 'Fittings', 'asfd', 'sadf', 19),
(1014, 'Additional Facilities', 'Terrace Garden', 'Yes', 19),
(1015, 'Additional Facilities', 'Lift', 'Available', 19),
(1016, 'Additional Facilities', 'Power Backup', 'Available', 19),
(1017, 'Additional Facilities', 'Water Facility', '24 hrs', 19),
(1018, 'Availability', 'Availability', '07-03-2016', 19);

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_interior_cat`
--

CREATE TABLE `prop_unit_interior_cat` (
  `category` varchar(100) NOT NULL COMMENT 'Category',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_unit_interior_cat`
--

INSERT INTO `prop_unit_interior_cat` (`category`, `is_active`) VALUES
('Additional Facilities', 1),
('Availability', 1),
('Fittings', 1),
('Flooring', 1),
('Walls', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_inventory`
--

CREATE TABLE `prop_unit_inventory` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `inventory` int(11) NOT NULL COMMENT 'Inventory',
  `prop_unit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit ID',
  `remarks` text COMMENT 'Remarks',
  `qty` decimal(10,4) NOT NULL COMMENT 'Quantity',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create On',
  `unit` varchar(20) DEFAULT NULL COMMENT 'Unit',
  `brand` varchar(200) NOT NULL COMMENT 'Brand Name',
  `location` text COMMENT 'Location'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_unit_inventory`
--

INSERT INTO `prop_unit_inventory` (`id`, `inventory`, `prop_unit_id`, `remarks`, `qty`, `created_on`, `unit`, `brand`, `location`) VALUES
(1, 36, 41, 'k', '2.0000', '2016-03-05 01:05:01', 'Nos', 'Aviva', 'unknow');

-- --------------------------------------------------------

--
-- Table structure for table `prop_unit_parking`
--

CREATE TABLE `prop_unit_parking` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'Id',
  `no_of_vehicles` int(11) NOT NULL COMMENT 'Number of Vehicles',
  `parking_type` varchar(200) NOT NULL COMMENT 'Parking Type',
  `prop_unit_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property Unit ',
  `location` text COMMENT 'Location',
  `vehicle_type` varchar(50) DEFAULT NULL COMMENT 'Vehicle Type'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop_unit_parking`
--

INSERT INTO `prop_unit_parking` (`id`, `no_of_vehicles`, `parking_type`, `prop_unit_id`, `location`, `vehicle_type`) VALUES
(1, 2, 'Covered', 20, 'upper basement', 'Two Wheeler'),
(2, 2, 'Covered', 21, 'Basement', 'Two Wheeler'),
(3, 1, 'Covered', 22, 'Basement', 'Four Wheeler'),
(4, 1, 'Covered', 28, 'Basement', 'Two Wheeler'),
(5, 1, 'Covered', 24, 'Basement', 'Four Wheeler'),
(6, 1, 'Covered', 25, 'Basement', 'Four Wheeler'),
(7, 1, 'Covered', 40, 'Basement', 'Four Wheeler'),
(8, 3, 'Covered', 92, '', 'Two Wheeler'),
(9, 1, 'Covered', 92, '', 'Four Wheeler'),
(10, 5, 'Covered', 19, '', 'Two Wheeler'),
(11, 2, 'Covered', 19, '', 'Four Wheeler');

-- --------------------------------------------------------

--
-- Table structure for table `request_services`
--

CREATE TABLE `request_services` (
  `service_name` varchar(200) NOT NULL COMMENT 'Service Name',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Created On'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_services`
--

INSERT INTO `request_services` (`service_name`, `is_active`, `created_on`) VALUES
('Electricity Bill payment', 1, '2016-02-24 07:53:10'),
('Masonry', 1, '2016-02-24 07:53:29'),
('Painting', 1, '2016-02-24 07:53:10'),
('Plumbing', 1, '2016-02-24 07:53:39'),
('Water Bill', 1, '2016-02-24 07:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `request_service_activity`
--

CREATE TABLE `request_service_activity` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'Service Name',
  `description` text COMMENT 'Description',
  `start_date` datetime DEFAULT NULL COMMENT 'Probable Start Date',
  `end_date` datetime NOT NULL COMMENT 'Finish Date',
  `property_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Property',
  `tenant_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Tenant',
  `member_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Member',
  `user_id` int(11) DEFAULT NULL COMMENT 'User',
  `exec_id` int(11) DEFAULT NULL COMMENT 'Executive',
  `priority` varchar(20) DEFAULT NULL COMMENT 'Priority',
  `proposed_cost` decimal(10,2) DEFAULT NULL COMMENT 'Proposed Cost',
  `accepted_cost` decimal(10,2) DEFAULT NULL COMMENT 'Accepted Cost',
  `admin_approval` int(11) DEFAULT NULL COMMENT 'Admin Approval',
  `member_approval` int(11) DEFAULT NULL COMMENT 'Member Approval',
  `is_payable` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Payable',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Status',
  `owner` int(11) DEFAULT NULL COMMENT 'Owner',
  `tenant_view` int(11) NOT NULL DEFAULT '0' COMMENT 'Tenant Viewed',
  `owner_view` int(11) NOT NULL DEFAULT '0' COMMENT 'Owner Viewed',
  `admin_view` int(11) NOT NULL DEFAULT '0' COMMENT 'Admin Viewed',
  `executive_view` int(11) NOT NULL DEFAULT '0' COMMENT 'Executive Seen'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_service_activity`
--

INSERT INTO `request_service_activity` (`id`, `name`, `description`, `start_date`, `end_date`, `property_id`, `tenant_id`, `member_id`, `user_id`, `exec_id`, `priority`, `proposed_cost`, `accepted_cost`, `admin_approval`, `member_approval`, `is_payable`, `status`, `owner`, `tenant_view`, `owner_view`, `admin_view`, `executive_view`) VALUES
(1, 'Electricity Bill payment', 'xv', '1970-01-01 00:00:00', '2016-03-02 00:00:00', NULL, NULL, 22, NULL, 105, NULL, '453.00', '3000.00', 115, 109, 1, 2, 109, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `contract_id` varchar(50) NOT NULL COMMENT 'Contract ID',
  `date` date NOT NULL COMMENT 'Date',
  `expiry_date` date NOT NULL COMMENT 'Expiry Date',
  `property_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Property',
  `tos` varchar(100) NOT NULL COMMENT 'Type of Service',
  `prop_type` varchar(100) NOT NULL COMMENT 'Property Type',
  `ptype` varchar(100) NOT NULL COMMENT 'Property Category',
  `member_id` int(11) DEFAULT NULL COMMENT 'Member ID',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created ON',
  `tenant_id` int(11) DEFAULT NULL COMMENT 'Tenant',
  `amount` decimal(10,2) DEFAULT NULL COMMENT 'Amount',
  `duration` int(11) DEFAULT NULL COMMENT 'Duration in months',
  `payment_type` varchar(20) DEFAULT NULL COMMENT 'Payment Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `remarks` text COMMENT 'Remarks',
  `document` varchar(255) DEFAULT NULL COMMENT 'Document'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `contract_id`, `date`, `expiry_date`, `property_id`, `tos`, `prop_type`, `ptype`, `member_id`, `created_on`, `tenant_id`, `amount`, `duration`, `payment_type`, `is_active`, `remarks`, `document`) VALUES
(22, '1234', '2015-05-28', '2015-12-28', 19, 'Tenancy Management Services', 'Residential', 'Apartment', 109, '2015-10-13 08:42:49', NULL, '25000.00', 7, NULL, 1, 'ok', NULL),
(23, '223342', '2016-03-04', '2016-03-04', 20, 'Apartment / House Monitoring', 'Residential', 'Apartment', 109, '2016-03-04 09:40:43', NULL, NULL, NULL, NULL, 1, '', NULL),
(24, '133r4', '2016-03-05', '2016-03-19', 92, 'Tenancy Management Services', 'Residential', 'Apartment', 116, '2016-03-05 01:19:17', NULL, '30000.00', 12, NULL, 0, 'k', NULL),
(25, '43534', '2016-03-05', '2016-03-12', 92, 'Tenancy Management Services', 'Residential', 'Apartment', 116, '2016-03-05 01:29:20', NULL, '43534.00', 354, NULL, 1, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `name` varchar(100) NOT NULL COMMENT 'Type of service',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_type`
--

INSERT INTO `service_type` (`name`, `is_active`) VALUES
('Apartment / House Monitoring', 1),
('Buying Assisting Services', 1),
('Construction Monitoring Services', 1),
('Land Bank Building Services', 1),
('Land Monitoring Services', 1),
('Real Estate Investment Services', 1),
('Selling Assisting Services', 1),
('Site / Plot Monitoring Services', 1),
('Tenancy Management Services', 1),
('Villa / Row House Monitoring', 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT 'State Name',
  `country` varchar(200) NOT NULL COMMENT 'Country Name',
  `code` varchar(20) NOT NULL COMMENT 'State Code'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `country`, `code`) VALUES
(1, 'Andaman and Nicobar Islands', 'India', 'AN'),
(4, 'Arunachal Pradesh', 'India', 'AP'),
(5, 'Bihar', 'India', 'BR'),
(6, 'Chandigarh', 'India', 'CH'),
(7, 'Karnataka', 'India', 'KAR');

-- --------------------------------------------------------

--
-- Table structure for table `telephone`
--

CREATE TABLE `telephone` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `primary_landline` varchar(50) DEFAULT NULL COMMENT 'Landline No.',
  `secondary_landline` varchar(50) DEFAULT NULL COMMENT 'Alternate Landline No.',
  `primary_mobile` varchar(50) DEFAULT NULL COMMENT 'Mobile No.',
  `secondary_mobile` varchar(50) DEFAULT NULL COMMENT 'Alternate Mobile No.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `telephone`
--

INSERT INTO `telephone` (`id`, `primary_landline`, `secondary_landline`, `primary_mobile`, `secondary_mobile`) VALUES
(244, '', '', '12346789', ''),
(245, '', '', '12346789', ''),
(249, '080 2346 2356', '080 2356 2346', '95389 97952', '97952 95389'),
(250, '080 2346 2356', '080 2356 2346', '95389 97952', '97952 95389'),
(251, '080 4137 9200', '080 22210784', '2345', NULL),
(252, '080 2224 9076', '080 9076 2224', '98450 12345', '99451 13911'),
(253, '044 2462 1110', '', '', ''),
(254, '044 2462 1110', '', '', ''),
(255, '080 2650 0744', '', '', ''),
(265, '080 2353 0891', '080 2353 0892', '98452 42606', '98452 42602'),
(266, '080 2353 0891', '080 2353 0892', '98452 42606', '98452 42602'),
(274, '080 4444 1122', '080 4444 1133', '98450 60652', '68450 52606'),
(275, '080 4444 1122', '080 4444 1133', '98450 60652', '68450 52606'),
(278, '080 5555 1122 ', '080 5555 2211', '8197676593', '8197675931'),
(279, '080 8897 2310', '080 2234 1234', '9742307629', '9620128783'),
(280, '080 8897 2310', '080 2234 1234', '9742307629', '9620128783'),
(281, '080 2231 8745', '080 2146 3641', '9611438549', '9739885266'),
(295, '080 5555 4444', '080 4444 5555', '99009 90099', '99119 91199'),
(296, '080 5555 4444', '080 4444 5555', '99009 90099', '99119 91199'),
(297, '080 6666 1111', '080 1111 6666', '123456', NULL),
(298, '080 9999 7777', '080 7777 8888', '99669 96699', '99889 98899'),
(299, '080 3333 6666', '080 4444 6666', '54321', NULL),
(300, '080 9999 4444', '080 4444 9999', '99009 91199', '99119 99009'),
(304, '080 25929081', '080 25929089', '9900005369', '9591950052'),
(305, '080 25929081', '080 25929089', '9900005369', '9591950052'),
(306, '08042040099', '080 41202099', '0000', NULL),
(307, '080 25929081', '080 25929089', '8197676594', '9591950052'),
(308, '080 3333 2222', '080 1111 2222', '1111', NULL),
(309, '080 4444 3333', '080 7777 8888', '77998 89977', '99001 12211'),
(313, '080 8897 3698', '080 2245 8795', '9742307629', '9620128783'),
(314, '080 8897 3698', '080 2245 8795', '9742307629', '9620128783'),
(315, '080 7789 5896', '080 4421 7865', '', NULL),
(318, '080 4412 7899', '080 7445 3694', '9611438549', '9739885266'),
(319, '080 8868 0822', '080 0822 8862', '', NULL),
(320, '080 2525 5252', '080 6666 4444', '98989 12345', '12345 98989'),
(324, '080 5555 8888', '080 7777 8888', '98450 98450', '98451 98451'),
(325, '080 5555 8888', '080 7777 8888', '98450 98450', '98451 98451'),
(326, '080 4120 2199', '080 4444 6666', '0012', NULL),
(327, '080 4545 4646', '080 7575 8585', '99669 96699', '99339 93399'),
(328, '080 5555 4444', '080 4444 6666', '12345', NULL),
(329, '080 7777 4444', '080 2222 3333', '99779 97799', '99889 98899'),
(337, '4545', NULL, '', NULL),
(338, '12345', NULL, '', NULL),
(339, '98800 14308', NULL, '98802 15496 ', NULL),
(341, '080 4444 1111', NULL, '99723 44999 / 97313 13779', NULL),
(342, '', NULL, '98802 15496 | 98800 14308', NULL),
(344, '080 4115 6368', NULL, '', NULL),
(345, '080 2540 5767', NULL, '88800 33444  ', NULL),
(346, '080 4165 9996', NULL, '96206 59955 / 96206 59966', NULL),
(347, '080 2346 0333 ', NULL, '97425 84888 ', NULL),
(348, '080 2780 0026', NULL, '', NULL),
(349, '080 2572 3377 / 2572 4477', NULL, '', NULL),
(350, '080 4341 7777', NULL, '', NULL),
(351, '080 2664 1442', NULL, '80668 86018', NULL),
(352, '080 2504 1700', NULL, '', NULL),
(353, '080 2572 6499 ', NULL, '88800 56789 / 99000 32296 ', NULL),
(354, '', NULL, '76767 68585', NULL),
(355, '', NULL, '96112 65959 / 96206 08021', NULL),
(356, '', NULL, '97383 02049 / 97390 08955', NULL),
(357, '080 4168 7088 / 2543 3353', NULL, '98804 58757 / 98450 21144', NULL),
(358, '', NULL, '98860 64628 / 98860 64627', NULL),
(359, '080 2529 3399 / 25296699', NULL, '76764 19419', NULL),
(360, '', NULL, '98452 60080 / 98450 96786', NULL),
(361, '080 2226 2210 ', NULL, '', NULL),
(362, '080 4048 3000 ', NULL, '80888 87755', NULL),
(363, '', NULL, '76765 45545 / 76766 62233', NULL),
(364, '080 4123 4041 / 41234042 ', NULL, '96866 97540 / 41', NULL),
(365, '', NULL, '97310 04455 / 97310 04411', NULL),
(366, '080 2572 2233 / 55', NULL, '93797 70770 / 93798 89889', NULL),
(367, '080 4112 6794 / 4112 6795', NULL, '98457 45540', NULL),
(368, '', NULL, '96638 85875', NULL),
(369, '', NULL, '95350 00027 / 80656 91581  ', NULL),
(370, '080 4161 3288', NULL, '76763 63636 / 88618 86765', NULL),
(371, '080 2669 0682', NULL, '94480 73568', NULL),
(372, '080 4241 9999', NULL, '', NULL),
(373, '080 4908 1100', NULL, '', NULL),
(374, '080 2785 0069 / 6546 8427', NULL, '94480 58628 / 77607 76319', NULL),
(375, '080 4120 9524', NULL, '95919 99947 /97400 66817', NULL),
(376, '080 4953 7949 / 8877 2299', NULL, '', NULL),
(377, '080 2676 0391', NULL, '', NULL),
(378, '', NULL, '99726 23399 / 99726 43399', NULL),
(379, '080 2550 2550', NULL, '94486 94486', NULL),
(380, '', NULL, '90195 90195', NULL),
(381, '', NULL, '76766 09999', NULL),
(382, '080 4166 4567 / 4152 4567', NULL, '96860 11122 / 96860 31122', NULL),
(383, '080 2536 0458', NULL, '92410 00055', NULL),
(384, '080 2509 6500 / 2509 6501', NULL, '99003 90055 ', NULL),
(385, '080 2572 3992 ', NULL, '', NULL),
(386, '', NULL, '99000 14447', NULL),
(387, '080 2671 0806', NULL, '76768 80880 / 99456 78143', NULL),
(388, '080 2343 8366 / 2343 8399', NULL, '80888 86699', NULL),
(389, '080 4937 8000', NULL, '', NULL),
(390, '080 2661 1711 / 4120 4758', NULL, '', NULL),
(391, '080 2844 3821', NULL, '94496 21321 / 95386 74281', NULL),
(392, '080 2667 2222 / 2661 1222', NULL, '90193 30340', NULL),
(393, '080 2353 5581 / 2353 5582', NULL, '', NULL),
(394, '080 6788 8999', NULL, '', NULL),
(395, '080 4137 9200', NULL, '', NULL),
(396, '', NULL, '90354 41212 / 89510 12131', NULL),
(397, '080 5011 1811', NULL, '99165 85757 / 98803 52565', NULL),
(398, '080 2545 8460', NULL, '98450 27975 / 99646 80200 ', NULL),
(399, '080 2543 7763', NULL, '99021 03339 / 87470 38333', NULL),
(400, '', NULL, '99000 11124', NULL),
(401, '080 4200 0342', NULL, '84315 55666 ', NULL),
(402, '080 4433 4400', NULL, '', NULL),
(403, '080 6765 1000', NULL, '88844 39184 / 88844 39187', NULL),
(404, '', NULL, '90199 01914 / 99720 93737 ', NULL),
(405, '', NULL, '90354 46655 / 99006 89200', NULL),
(406, '080 4245 0000', NULL, '', NULL),
(407, '080 2529 7735 /  4125 5117', NULL, '98455 73075 / 99648 26466', NULL),
(408, '080 4157 0163 /  4157 0164', NULL, '', NULL),
(409, '', NULL, '76761 22000', NULL),
(410, '080 2574 4606', NULL, '', NULL),
(411, '080 4112 2577', NULL, '99000 73744 / 99000 73745', NULL),
(412, '080 2212 3347', NULL, '98802 41990 / 98450 65084', NULL),
(413, '080 4333 5999 ', NULL, '', NULL),
(414, '', NULL, '98459 59911', NULL),
(415, '080 2361 1333', NULL, '76767 77222', NULL),
(416, '080 6999 1616 / 6999 1617', NULL, '', NULL),
(417, '080 2221 3344', NULL, '', NULL),
(418, '080 2226 8786 / 2226 5647', NULL, '', NULL),
(419, '080 2235 0011 / 22350002 ', NULL, '', NULL),
(420, '', NULL, '888 000 1001', NULL),
(421, '080 4912 3000', NULL, '90191 92000', NULL),
(422, '080 3232 2009', NULL, '97318 79321', NULL),
(423, '', NULL, '98807 92738 / 95389 52464', NULL),
(424, '080 4179 9999', NULL, '', NULL),
(425, '', NULL, ' 9900 008124 / 9900 008127 ', NULL),
(426, '080 41675838', NULL, '99000 30089 / 96202 28860', NULL),
(427, '080 4166 8077', NULL, '99000 45996', NULL),
(428, '080 2563 9788', NULL, '98866 68111 / 98866 67222', NULL),
(429, '080 2521 1785 / 2525 5802', NULL, '', NULL),
(430, '080 2841 1077', NULL, '80885 52347 / 80885 52348', NULL),
(431, '080 4207 4477 /  4216 6166', NULL, '', NULL),
(432, '', NULL, '70220 19990 / 70220 19985', NULL),
(433, '', NULL, '88844 20119 / 88844 20120', NULL),
(434, '080 2521 7012 /  4295 1600', NULL, '98457 18048', NULL),
(435, '080 4144 0524', NULL, '99004 20922 / 96325 30688', NULL),
(436, '080 4120 1909 / 4120 1910', NULL, '', NULL),
(437, '080 4249 4249', NULL, '', NULL),
(438, '080 4123 6285 / 4123 6286 ', NULL, '', NULL),
(439, '080 4931 4931', NULL, '', NULL),
(440, '080 2520 3914', NULL, '80888 90006', NULL),
(441, '080 4163 5571', NULL, '97414 55011 / 89513 36200', NULL),
(442, '080 2244 6828', NULL, '96861 11191 / 94481 73494', NULL),
(443, '', NULL, '88845 67567 ', NULL),
(444, '080 2563 0003', NULL, '', NULL),
(445, '080 4146 9220 / 4146 9221', NULL, '', NULL),
(446, '080 6567 7000', NULL, ' 90191 27000', NULL),
(447, '080 2346 3585', NULL, '', NULL),
(448, ' 080 3019 6411', NULL, '', NULL),
(449, '080 2559 5822 / 2559 9515', NULL, '', NULL),
(450, '080 2220 3981', NULL, '88800 01188', NULL),
(451, '080 2572 3339', NULL, '', NULL),
(452, '', NULL, '99016 00266 / 99013 61246', NULL),
(453, '', NULL, '80886 69944', NULL),
(454, '', NULL, '9379 424 669 / 93424 32167 ', NULL),
(455, '080 4173 6111', NULL, '98440 44111 / 98863 77748', NULL),
(456, '', NULL, '76762 64077 / 93412 68009', NULL),
(457, '', NULL, '98805 05027 / 99805 05250', NULL),
(458, '080 5021 4169', NULL, '99005 12439', NULL),
(459, '080 2318 2362', NULL, '99452 79835', NULL),
(460, '080 2213 1390 ', NULL, '92437 90000', NULL),
(461, '080 4256 6100', NULL, '80500 08003', NULL),
(462, ' 080 2350 6538 / 2330 4611', NULL, '98451 96631', NULL),
(463, '', NULL, '953544 4488 / 95354 44499', NULL),
(464, '080 2540 1301', NULL, '95906 166160', NULL),
(465, '080 4669 8111', NULL, '', NULL),
(466, '', NULL, '90080 05029 / 97423 00378', NULL),
(467, '080 2558 0223 / 4132 0752', NULL, '', NULL),
(468, '', NULL, '98450 56677 / 9008 989056 /  96861 91112', NULL),
(469, '080 41306089 ', NULL, '97409 01469 / 9845011820 ', NULL),
(470, '080 4947 7877 / 4947 7899', NULL, '', NULL),
(471, '', NULL, '99009 36299 / 9880121933', NULL),
(472, '', NULL, '93411 12343', NULL),
(473, '080 2314 7663 / 2350 5219', NULL, '99000 31271 / 99000 30423', NULL),
(474, '080 2663 4177', NULL, '76760 00111', NULL),
(475, '080 4665 7812', NULL, '', NULL),
(476, '080 4128 0992', NULL, '76767 77111 / 76767 77555', NULL),
(477, '080 2572 3399 / 2572 0763', NULL, '76765 67567', NULL),
(478, '080 2572 7770 / 4920 2323', NULL, '', NULL),
(483, '080 4050 4200', NULL, '76767 64200', NULL),
(484, '', NULL, '97383 02049 / 97390 08955', NULL),
(485, '', NULL, ' 98458 10025 /  98458 10023 / 9880711036.', NULL),
(486, '080 4123 8801 / 2555 0777', NULL, '99000 27201 / 99000 27204', NULL),
(487, '080 4092 6552', NULL, '99727 62552 / 99727 69552 / 99809 43152', NULL),
(488, '', NULL, '93435 57575', NULL),
(489, '080 2572 5761 / 2572 5762 / 2572 5763', NULL, '76767 64466 / 76767 61133', NULL),
(490, '080 4932 3144', NULL, '76765 55000', NULL),
(491, '', NULL, '88801 31234', NULL),
(492, '080 4967 1111', NULL, '88803 12345', NULL),
(493, '080 4130 0000 ', NULL, '', NULL),
(494, '080 6596 6666 ', NULL, '72590 19693 / 99808 47196', NULL),
(495, '', NULL, '88803 33666 / 81442 22666', NULL),
(496, '080 2668 0555', NULL, '96324 33344 / 96324 33377', NULL),
(497, '', NULL, '90360 93393 / 98802 99422', NULL),
(498, '', NULL, '80422 27999', NULL),
(499, '080 6050 2244', NULL, '99000 57610 / 99000 57620', NULL),
(500, '080 4114 8408 / 4123 5002 ', NULL, '98450 54888', NULL),
(501, '080 2254 1950', NULL, '80887 76699', NULL),
(502, '080 2558 8188', NULL, '95901 13113', NULL),
(503, '', NULL, '76766 64466', NULL),
(504, '080 2679 2676 / 2679 6949', NULL, '88844 53313', NULL),
(505, '080 2678 1999 / 2668 9621', NULL, '99011 00633 / 95911 15555', NULL),
(506, '080 6435 0221', NULL, '97439 44449 / 97439 66669 / 97434 88889', NULL),
(507, '080 2211 1531 / 2211 1532', NULL, '', NULL),
(508, '080 2678 0170', NULL, '', NULL),
(509, '080 6579 0721', NULL, '99013 09690 / 95351 00977 ', NULL),
(510, '080 2658 1216 / 17 / 18', NULL, '96630 95555 / 96630 75555', NULL),
(511, '080 4094 6314', NULL, '99015 33344', NULL),
(512, '080 2348 5221 / 2348 5222', NULL, '99006 19357 / 98450 23651', NULL),
(513, '080 2572 2670 / 71 / 72 / 73 / 74', NULL, '88800 03399 / 80886 78678', NULL),
(514, '040 2779 2325', NULL, '98490 25405', NULL),
(515, '', NULL, '78478 87799', NULL),
(516, '080 2665 0263', NULL, '98803 33233 / 98802 22244', NULL),
(517, '080 4017 4222 / 26', NULL, '', NULL),
(518, '080 4953 8566', NULL, '84949 44652 / 8494913111 / 9448276080', NULL),
(519, '080 2668 8766', NULL, '99451 57780', NULL),
(520, '080 2332 5581 / 82', NULL, '', NULL),
(521, '80 4039 5600', NULL, '', NULL),
(522, '', NULL, '99001 17711 / 87921 17711', NULL),
(523, '080 2571 6942 / 2571 6392', NULL, '', NULL),
(524, '080 4263 6363', NULL, '', NULL),
(525, '', NULL, '95388 87712 / 95380 86145', NULL),
(526, '080 2258 9442 /2258 9443', NULL, '98454 00113', NULL),
(527, '', NULL, '98451 67509 / 98804 66931 / 99009 98444', NULL),
(528, '', NULL, '90194 45566 / 70225 91130', NULL),
(529, '', NULL, '99648 64444 / 99648 62222', NULL),
(530, '080 4113 9758', NULL, '99020 03313 /99020 03315', NULL),
(531, '080 2559 1080', NULL, '', NULL),
(532, '080 2222 3013 / 14 / 15 / 16', NULL, '', NULL),
(533, '', NULL, '97404 55577 / 89710 33999 /  99454 33996', NULL),
(534, '080 4455 5555 / 4343 9999 / 2559 9000', NULL, '', NULL),
(535, '', NULL, '96860 88099 / 98805 88669', NULL),
(536, '080 4090 2405', NULL, '', NULL),
(537, '080 4094 3331/ 4094 3332 ', NULL, '93425 85018 / 7022016100 /01 / 02 ', NULL),
(538, '080 4242 4999', NULL, '', NULL),
(539, '080 2664 5664 / 2664 8664/ 2244 0395', NULL, '', NULL),
(540, '080 2210 4222 / 2210 4223 / 24 / 25 / 26', NULL, '99020 25561 / 62', NULL),
(541, '0080 2544 4666 / 999', NULL, '', NULL),
(542, '080 4246 1461 / 2210 3131 / 32 / 33 / 34', NULL, '98442 52259 / 98442 56073 / 98441 33694', NULL),
(543, '080 2331 7000 / 4241 7000', NULL, '', NULL),
(544, '080 2671 6811', NULL, '', NULL),
(545, '080 2558 9688 / 2558 8333 / 2555 9990', NULL, '', NULL),
(546, '080 2669 0369', NULL, '90352 18930', NULL),
(547, '080 6534 1556', NULL, '', NULL),
(548, '080 2666 8870', NULL, '98807 99975 / 98806 76761', NULL),
(549, '', NULL, '98441 98178 / 94480 79659 / 99019 66311', NULL),
(550, '', NULL, '99805 22522', NULL),
(551, '080 4167 0009', NULL, '90356 18111 / 90356 18222', NULL),
(552, '', NULL, '80881 11888', NULL),
(553, '080 4269 9000', NULL, '', NULL),
(554, '080 4925 7111 / 2559 2334', NULL, '99018 77888', NULL),
(555, '080 4149 7474', NULL, '99807 77775 / 99867 77774', NULL),
(556, '080 2658 3089', NULL, '96637 08109 / 98450 75892', NULL),
(557, '080 4151 6333', NULL, '99000 15931', NULL),
(558, '080 2341 8145 / 2341 0767 ', NULL, '', NULL),
(559, '', NULL, '95900 77888 ', NULL),
(560, '', NULL, '99867 33733 / 99868 86860', NULL),
(561, '080 3295 4611 / 12', NULL, '', NULL),
(562, '080 4215 3334', NULL, '99001 95593 / 93412 46163 ', NULL),
(563, '080 4022 9999', NULL, '', NULL),
(564, ' 080 4115 8525', NULL, '', NULL),
(565, '', NULL, '90191 11525', NULL),
(566, '', NULL, '95351 30763 / 73537 76283', NULL),
(567, '080 4113 3888', NULL, '95900 07788 / 90197 77888', NULL),
(568, '', NULL, '99020 88997', NULL),
(569, '', NULL, '9980 851166 / 90360 51166 ', NULL),
(570, '080 4932 0000', NULL, '', NULL),
(571, '080 2331 8189', NULL, '99451 10009 / 98807 11223', NULL),
(572, '080 4243 3000', NULL, '', NULL),
(573, '080 2665 6111', NULL, '', NULL),
(574, '080 2529 8333', NULL, '88610 05200 / 88610 05199 ', NULL),
(575, '080 2844 3246', NULL, '88808 13322', NULL),
(576, '080 6579 0721', NULL, '99013 09690 / 953510 0977', NULL),
(577, '', NULL, '96327 77669 / 96328 66799', NULL),
(580, '080 2331 8189', NULL, ' 99451 10009', NULL),
(581, '', NULL, '94814 34934 / 98860 09408', NULL),
(582, '', NULL, '93422 28466 / 99164 82604 / 99457 24004', NULL),
(583, '', NULL, '90360 08166 / 90360 08277', NULL),
(584, '', NULL, '96861 11241 /42 / 43', NULL),
(585, '', NULL, '80885 33355', NULL),
(586, '080 4341 3333', NULL, '', NULL),
(587, '080 4298 1823', NULL, '93433 33614', NULL),
(588, '', NULL, '88845 00500 / 88844 00400', NULL),
(589, '', NULL, '80887 60772', NULL),
(590, '080 4219 1306 / 4219 1307', NULL, '92430 00366 / 98864 04430', NULL),
(591, '', NULL, '959144 0964', NULL),
(592, '080 2572 2991', NULL, '97423 25508', NULL),
(593, '080 4206 2038', NULL, '99000 18522 / 78999 55055', NULL),
(594, '080 2561 0049', NULL, '99163 00449', NULL),
(595, '', NULL, '99001 98549 / 98451 88229 / 99725 69999', NULL),
(596, '080 4218 8353', NULL, '95904 44666', NULL),
(597, '', NULL, '99012 40055', NULL),
(598, '080 4245 3000', NULL, '99000 78000', NULL),
(599, '080 2554 5950 / 1 / 2 / 3', NULL, '81979 83557', NULL),
(600, '', NULL, '80880 04488', NULL),
(601, '', NULL, '76765 55666', NULL),
(602, '080 2225 2001 / 02', NULL, '8322 540873', NULL),
(603, '', NULL, '99000 37500 / 84317 77000', NULL),
(604, '', NULL, '92435 55755', NULL),
(605, '', NULL, '80228 67710 / 11 / 12', NULL),
(606, '080 2271 6333', NULL, '99450 04011 / 99450 05011', NULL),
(607, '080 2319 0187 / 2349 2346', NULL, '88806 64455', NULL),
(608, '', NULL, '88807 96796', NULL),
(609, '080 4149 9001 / 4149 9002', NULL, '80884 44777', NULL),
(610, '800 2323 4886', NULL, '', NULL),
(611, '', NULL, '88807 78778 / 80889 98998', NULL),
(612, '080 4119 3293', NULL, '', NULL),
(613, '080 4147 7330 / 31 / 32 ', NULL, '', NULL),
(614, '', NULL, '88803 60360', NULL),
(615, '080 2522 6432 / 3512 / 2998', NULL, '76760 00999', NULL),
(616, '', NULL, '95918 46464', NULL),
(617, '080 4040 0000 / 4911 0000', NULL, '', NULL),
(618, '080 6532 3265 / 6535 3565', NULL, '98860 41641', NULL),
(619, '', NULL, '080 2658 2059 / 99', NULL),
(620, '080 4945 4700', NULL, '', NULL),
(621, '080 4148 6343', NULL, '99002 36000 / 96116 50000', NULL),
(622, '080 4149 6414', NULL, '', NULL),
(623, '080 2660 7999 / 2661 5653', NULL, '98458 55622 / 76765 55444', NULL),
(624, '', NULL, '70220 06191', NULL),
(625, '', NULL, '96869 70329 / 90364 52307', NULL),
(626, '080 2543 4799 / 2544 7788', NULL, '93437 92588 / 95903 09090', NULL),
(627, '', NULL, '90198 90198', NULL),
(628, '', NULL, '99000 69670', NULL),
(629, '80 2553 3252', NULL, '', NULL),
(630, '', NULL, '99000 08347 / 99000 08348', NULL),
(631, '080 4242 7777', NULL, '78150 01455', NULL),
(632, '080 4091 4399', NULL, '98863 00600 / 98867 00900', NULL),
(633, '', NULL, '78480 00099', NULL),
(634, '080 2286 8093 / 2286 8094 / 2286 8095 ', NULL, '99023 60111', NULL),
(635, '0821  4282 999', NULL, '97421 97197 ', NULL),
(636, '', NULL, '84949 21603', NULL),
(641, '0821 4260 273 / 2568 634', NULL, '98451 27297 / 93421 88155', NULL),
(642, '0821 4243 950', NULL, '94484 88441', NULL),
(643, '', NULL, '99000 04400', NULL),
(644, '0821 2333 906 / 2333 521', NULL, '', NULL),
(645, '821 6557 990', NULL, '89700 01234', NULL),
(646, '', NULL, '92435 25710', NULL),
(647, '821 2500 056', NULL, '99001 02041 / 99454 19614', NULL),
(648, '821 3250 050', NULL, ' 98866 03556 / 98866 06337', NULL),
(649, '0821 2903 763', NULL, '98457 90910 / 99800 91182', NULL),
(650, '0821 2415 477 / 2518 969 ', NULL, '', NULL),
(651, '0821 4241 700', NULL, '', NULL),
(652, '08494921603', NULL, '08494921603', NULL),
(660, '080 4932 0000', '', '', ''),
(661, '080 4137 9200', '', '', ''),
(662, '080 4137 9200', '080 4046 7600', '', ''),
(663, '080 4137 9200', '080 4137 9200', '', ''),
(666, '', '', '6987451230', ''),
(667, '564564', '', '', ''),
(676, '4046 7666', '', '', ''),
(677, '080 4046 7666', '', '', ''),
(678, '080 4046 7666', '', '', ''),
(682, '080 4046 7666', '', '', ''),
(683, '080 4046 7666', '', '', ''),
(684, '080 4046 7666', '', '', ''),
(685, '080 4046 7666', '', '', ''),
(686, '080 4046 7666', '', '', ''),
(688, '080 4046 4666', '', '', ''),
(689, '', '4046 7600', '', ''),
(690, '080 4046 7666', '080 4137 9200', '', ''),
(691, '080 4046 7666', '080 4137 9200', '', ''),
(692, '080 4046 7666', '080 4137 9200', '', ''),
(693, '080 4046 7666', '080 4137 9200', '', ''),
(694, '0821 425 2239', '', '', ''),
(695, '0821 425 2239', '', '', ''),
(696, '0821 425 2239', '', '', ''),
(697, '080 4046 7666', '080 4137 9200', '', ''),
(698, '080 4046 7666', '080 4137 9200', '', ''),
(699, '080 4046 7666', '080 4137 9200', '', ''),
(700, '080 4046 7666', '080 4137 9200', '', ''),
(701, '080 4046 7666', '080 4137 9200', '', ''),
(702, '080 4046 7666', '080 4137 9200', '', ''),
(703, '080 4046 7666', '080 4137 9200', '', ''),
(704, '080 4046 7666', '080 4137 9200', '', ''),
(705, '080 4046 7666', '080 4137 9200', '', ''),
(706, '080 4046 7666', '080 4137 9200', '', ''),
(707, '080 4046 7666', '080 4137 9200', '', ''),
(708, '080 4046 7666', '080 4137 9200', '', ''),
(709, '080 4046 7666', '080 4137 9200', '', ''),
(710, '0821 425 2239', '080 4137 9200', '', ''),
(711, '080 4932 0000', '', '', ''),
(712, '080 4932 0000', '', '', ''),
(713, '080 4932 0000', '', '', ''),
(714, '080 4932 0000', '', '', ''),
(715, '080 4932 0000', '', '', ''),
(716, '080 4932 0000', '', '', ''),
(717, '080 4046 7666', '080 4137 9200', '', ''),
(718, '080 4932 0000', '', '', ''),
(719, '080 4932 0000', '', '', ''),
(720, '080 4932 0000', '', '', ''),
(721, '080 4932 0000', '', '', ''),
(722, '', '', '9538997652', ''),
(723, '', '', '9538997652', ''),
(724, '', '', '963852740', ''),
(725, '', '', '35464', ''),
(726, '', '', '3423434', ''),
(727, '435', '435', '', ''),
(728, '4354', '', '', ''),
(729, '123456789', '', '', ''),
(730, '123456789', '', '', ''),
(731, '123465789', '', '', NULL),
(732, '1234567890', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tenant`
--

CREATE TABLE `tenant` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `member_id` int(11) NOT NULL COMMENT 'Member ID',
  `tenant_type` varchar(50) DEFAULT NULL COMMENT 'Tenant Type',
  `tenant_family_type` varchar(100) NOT NULL COMMENT 'Tenant Family Type',
  `no_people` int(11) NOT NULL COMMENT 'No. of People',
  `pan_no` varchar(20) NOT NULL COMMENT 'PAN No.',
  `food_habits` varchar(50) NOT NULL COMMENT 'Food Habits',
  `residential_status` varchar(200) NOT NULL COMMENT 'Residential Status',
  `has_pets` int(11) NOT NULL COMMENT 'Has Pets?',
  `permanent_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Permanent Address',
  `present_address_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Present Address',
  `office_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Office Address',
  `reference_person_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Reference Person ID',
  `profile_pic_path` varchar(255) DEFAULT NULL COMMENT 'Profile Picture Path',
  `pan_file_path` varchar(255) DEFAULT NULL COMMENT 'PAN File Path',
  `id_proof_path` varchar(255) DEFAULT NULL COMMENT 'ID Proof Path',
  `address_proof_path` varchar(255) DEFAULT NULL COMMENT 'Address Proof Path',
  `title_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'Title ID',
  `fname` varchar(100) NOT NULL COMMENT 'First Name',
  `lname` varchar(100) NOT NULL COMMENT 'Last Name',
  `email_id` varchar(255) DEFAULT NULL COMMENT 'Email ID',
  `marital_status` varchar(100) NOT NULL COMMENT 'Marital Status',
  `spouse` varchar(200) DEFAULT NULL COMMENT 'Spouse',
  `wed_ann` date DEFAULT NULL COMMENT 'Wedding Anniversary',
  `relation` varchar(50) DEFAULT NULL COMMENT 'Relation',
  `relative` varchar(100) DEFAULT NULL COMMENT 'Relative',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `company` varchar(200) DEFAULT NULL COMMENT 'Company',
  `designation` varchar(200) DEFAULT NULL COMMENT 'Designation'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant`
--

INSERT INTO `tenant` (`id`, `member_id`, `tenant_type`, `tenant_family_type`, `no_people`, `pan_no`, `food_habits`, `residential_status`, `has_pets`, `permanent_address_id`, `present_address_id`, `office_address_id`, `reference_person_id`, `profile_pic_path`, `pan_file_path`, `id_proof_path`, `address_proof_path`, `title_id`, `fname`, `lname`, `email_id`, `marital_status`, `spouse`, `wed_ann`, `relation`, `relative`, `dob`, `company`, `designation`) VALUES
(7, 104, NULL, 'Family', 4, '12324', 'Vegetarian', 'NRI', 0, 384, 385, 386, NULL, NULL, NULL, NULL, '', 1, 'Kiran', 'Kumar', 'Kiran0604@gmail.com', 'Single', '', '1970-01-01', 'Son of', 'Vijay Kumar', '1975-08-18', 'Hitachi India Pvt. Ltd', 'Team Leader Manager'),
(8, 106, NULL, 'Family', 4, 'AA235640AA', 'Non Vegetarian', 'NRI', 1, 397, 398, 402, NULL, NULL, NULL, NULL, '', NULL, 'Narayan', 'Swamy', 'narayan.s@gmail.com', 'Married', 'parmila', '1991-02-14', 'Son of', 'Natesh', '1971-06-15', 'DGQA', 'Manager'),
(10, 108, NULL, 'Family', 5, '0111122', 'Vegetarian', 'NRI', 0, 403, 404, 405, NULL, NULL, NULL, NULL, '', NULL, 'Malini D', 'Dharmaraya', 'malinidharmaraya@gmail.com', 'Single', '', NULL, 'Daughter of', 'Dharmaraya', '1997-04-08', 'Value Home Properties Pvt.Ltd', 'Telecaller');

-- --------------------------------------------------------

--
-- Table structure for table `tenant_property_allocation`
--

CREATE TABLE `tenant_property_allocation` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `prop_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Property ID',
  `tenant_id` int(20) NOT NULL COMMENT 'Tenant ID',
  `start_date` date DEFAULT NULL COMMENT 'Start Date',
  `expiry_date` date DEFAULT NULL COMMENT 'Expiry Date',
  `vacate_date` date DEFAULT NULL COMMENT 'Vacate Date',
  `rent_date` int(11) DEFAULT NULL COMMENT 'Rent Date',
  `advance` decimal(10,2) DEFAULT NULL COMMENT 'Advance Amount',
  `rent_amt` decimal(10,2) DEFAULT NULL COMMENT 'Rent Amount',
  `rent_lease` varchar(10) DEFAULT NULL COMMENT 'Rent or Lease',
  `increment_perc` decimal(10,2) DEFAULT NULL COMMENT 'Increment Percentage',
  `maintenance` decimal(10,2) NOT NULL COMMENT 'maintainance',
  `next_collection_date` date DEFAULT NULL COMMENT 'Next Collection Date',
  `beneficiary` varchar(100) DEFAULT NULL COMMENT 'Beneficiary',
  `agreement` varchar(255) DEFAULT NULL COMMENT 'Agreement Document',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT 'Is Active?',
  `service_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant_property_allocation`
--

INSERT INTO `tenant_property_allocation` (`id`, `prop_id`, `tenant_id`, `start_date`, `expiry_date`, `vacate_date`, `rent_date`, `advance`, `rent_amt`, `rent_lease`, `increment_perc`, `maintenance`, `next_collection_date`, `beneficiary`, `agreement`, `is_active`, `service_id`) VALUES
(6, 19, 104, '2016-02-01', '2020-07-17', '2015-10-23', 1, '50000.00', '5000.00', 'Rent', '3.00', '0.00', NULL, NULL, NULL, 0, 22),
(7, 19, 104, '2016-03-02', '2016-03-31', '2016-03-10', 1, '34.00', '345.00', 'Rent', '1.00', '0.00', NULL, NULL, NULL, 1, 22),
(8, 20, 104, '2016-03-04', '2016-03-04', '2016-03-18', 4, '435.00', '534.00', 'Rent', NULL, '0.00', NULL, NULL, NULL, 1, 23),
(9, 92, 104, '2016-03-05', '2016-03-31', '2016-03-25', 11, '50000.00', '20000.00', 'Rent', '5.00', '0.00', NULL, NULL, NULL, 0, 24),
(10, 92, 108, '2016-03-05', '2016-03-05', '2016-03-05', 5, '2000.00', '10000.00', 'Rent', '43.00', '0.00', NULL, NULL, NULL, 1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `tenant_reference`
--

CREATE TABLE `tenant_reference` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `title` int(11) NOT NULL COMMENT 'Title',
  `fname` varchar(100) NOT NULL COMMENT 'First Name',
  `lname` varchar(100) NOT NULL COMMENT 'Last Name',
  `email` varchar(200) DEFAULT NULL COMMENT 'Email ID',
  `designation` varchar(100) DEFAULT NULL COMMENT 'Designation',
  `address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Address',
  `company` varchar(200) DEFAULT NULL COMMENT 'Company',
  `office_address_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Office Address',
  `tenant_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Tenant'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tenant_supervisions`
--

CREATE TABLE `tenant_supervisions` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID',
  `contract_id` varchar(50) NOT NULL COMMENT 'Contract ID',
  `date` date NOT NULL COMMENT 'Date',
  `expiry_date` date NOT NULL COMMENT 'Expiry Date',
  `property_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Property',
  `tos` varchar(100) NOT NULL COMMENT 'Type of Service',
  `prop_type` varchar(100) NOT NULL COMMENT 'Property Type',
  `ptype` varchar(100) NOT NULL COMMENT 'Property Category',
  `member_id` int(11) DEFAULT NULL COMMENT 'Member ID',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created ON',
  `tenant_id` int(11) DEFAULT NULL COMMENT 'Tenant',
  `amount` decimal(10,2) DEFAULT NULL COMMENT 'Amount',
  `duration` int(11) DEFAULT NULL COMMENT 'Duration in months',
  `rent_lease` varchar(20) DEFAULT NULL COMMENT 'Rent Or Lease',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?',
  `remarks` text COMMENT 'Remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tenant_type`
--

CREATE TABLE `tenant_type` (
  `name` varchar(50) NOT NULL COMMENT 'Tenant  Type',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT 'Is Active?'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant_type`
--

INSERT INTO `tenant_type` (`name`, `is_active`) VALUES
('Bachelor', 1),
('Family', 1);

-- --------------------------------------------------------

--
-- Table structure for table `todo_list`
--

CREATE TABLE `todo_list` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `exec_id` int(11) NOT NULL COMMENT 'Executive ID',
  `todo` varchar(200) NOT NULL COMMENT 'To Do',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Status',
  `date` datetime NOT NULL COMMENT 'Date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todo_list`
--

INSERT INTO `todo_list` (`id`, `exec_id`, `todo`, `status`, `date`) VALUES
(1, 105, 'tod1', 1, '2016-02-25 11:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(101, 'EmcZyQqjz9JDpB258ZOOJg7MIPi2zmqj', 1585549957, 1),
(101, 'jZ1eZa1Na11dqRssSnvnb1Yoxx32rlOF', 1444744680, 0),
(103, 'Te2LEb4HsUR7paufo8inz-QtRAo0AFdz', 1444744998, 0),
(104, '7zp3u5jHgjEa9X0KhM0sOogykhqHI0w7', 1444745444, 0),
(105, 'rkDAnZ7_88xbRU1vYSSO2U3yiVvwwo1k', 1444978241, 0),
(106, 'R84vtzTDEQfLBiIPdE90y7iidf3-4PqN', 1444979443, 0),
(108, 'eBJE15KaJLBdiq4oDt2v5uD9BN5W5qSb', 1444979931, 0),
(109, '-Wl3qb1xJTHJnMoFapGtZKLBm1mVQMhb', 1445319164, 0),
(110, 'SgMIpiRyNUF4qX2oL0TSOzUull8wEsCL', 1445538886, 0),
(111, 'rj-lmeIchqqoThnGP8XgtrlkvkVD2-0h', 1445837971, 0),
(113, 'qKIdErRSny3w0CGusp1YB61Fwz7CoXws', 1445841967, 0),
(114, 'c6ZD_LmxO_500M3lryh3h93Mgm9TdUhO', 1454328204, 0),
(115, 'SNVo7fwxS8c1JSi3rt846KRQnL3houbQ', 1456320757, 0),
(116, 'W-P6zNw4ujXejnTDAHDgU2dnofQ4NCxA', 1457157323, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `user_type` varchar(50) DEFAULT NULL COMMENT 'User Type',
  `last_login_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `user_type`, `last_login_at`) VALUES
(101, 'EMP-1510/001', 'rohith.vcnr@gmail.com', '$2y$12$8z5o5GJRrCjUgo4.AuAGYOLGQN/of/C8/gyr.XdGwJ2MzkAEYxewC', 'aC_WkA011_HsBiY7aInjgUZsz0n0cnSL', NULL, NULL, NULL, '127.0.0.1', 1444744680, 1444744680, 0, 'Data Entry', 1593769811),
(103, 'IND-1510/001', 'Vinayak.g@gmail.com', '$2y$12$NQ9IWfMzxotDTL4228s7zejhuRJXYFd4YE6T2p7RXoYY5R9sx8RTy', 'dJjeeN67FOeYvDD4V7jp0YN7NUoWW0rf', NULL, NULL, NULL, '127.0.0.1', 1444744998, 1444744998, 0, 'Individual', 0),
(104, 'TEN-1510/001', 'Kiran0604@gmail.com', '$2y$12$REZPrJVMeuJdJEkxEA.HCeLs08gH7Ot5OtmGPKLOkz17iKlBHzYF.', '4Ch2KHgUfs-LerM3Mcc-X7oEjG2ACS3I', NULL, NULL, NULL, '127.0.0.1', 1444745444, 1444745444, 0, 'Tenant', 0),
(105, 'EMP-1510/002', 'ravi.m@gmail.com', '$2y$12$CHoZhoeIv15svl7jNVFIX.NtqLcAOOTKk2QDz/J69QfUSEw5g4maO', 'KjfuEIOoj-yrMTi7QyMcOyzAcUxpvoT_', NULL, NULL, NULL, '122.172.166.136', 1444978241, 1444978241, 0, 'Executive', 0),
(106, 'TEN-1510/002', 'narayan.s@gmail.com', '$2y$12$1itiNW7P4hxsHfP8hqIZZeIqxRSinE3srKNBeiwxjAJPViiC52.3a', 'EinbYTcfe-oCjc2kEK8PCCzCel58gJJd', NULL, NULL, NULL, '122.172.166.136', 1444979443, 1444979443, 0, 'Tenant', 0),
(108, 'TEN-1510/003', 'malinidharmaraya@gmail.com', '$2y$12$ilS81hV23or5rf6S/b8fHe.y46rXdoxaQkRUuuFn2B2uL5rF0.jMu', 'BBAF3ltdcHFMeE6LWNHVPIUD2sA172KB', NULL, NULL, NULL, '122.172.166.136', 1444979931, 1444979931, 0, 'Tenant', 0),
(109, 'IND-1510/002', 'vijay@gmail.com', '$2y$12$vQ/D6Ww56RakU.5EfHmrU.KfF0IqniDLylz7dWMOC6/31JSfBiR1G', '5wYO6QUdszxC0D8cQSBMAkGt7OaykLm3', NULL, NULL, NULL, '122.172.47.234', 1445319164, 1445319164, 0, 'Individual', 0),
(110, 'IND-1510/003', 'raghu0604@gmail.com', '$2y$12$rpyMaFGowrBLGzZTzeV/qe..VeQEEHMxQWNdym.evYarVHpwrqlJ.', 'VN6pq4-Kgp0gKuvo9q3NM7ANlAECTNjv', NULL, NULL, NULL, '223.227.129.222', 1445538886, 1445538886, 0, 'Individual', 0),
(111, 'IND-1510/004', 'malini22dharmaraya@gmail.com', '$2y$12$zzc4VdC.W9NOdoR/AiNQwOEUP16M.ZPIibbWG8IgHz2cIsqBgKbye', 'uCP0reh1vX677iwWZol7k8BSqUoEKgi2', NULL, NULL, NULL, '122.172.90.86', 1445837971, 1445837971, 0, 'Individual', 0),
(113, 'IND-1510/005', 'shashi0605@ymail.com', '$2y$12$NQO2TehZ8suKQmPNkTDI0uNLF0ozt4uUd4pCj4UoLQGLyaehdTexa', 'fg4L47KOBdax1qdrJSZ-c8AfSYc3r6AG', NULL, NULL, NULL, '122.172.90.86', 1445841967, 1445841967, 0, 'Individual', 0),
(114, 'IND-1602/006', 'bk.subbaiah52@gmail.com', '$2y$12$GhF7ThfONjpZLqTKTRS3pOqT7AJrtBaVtSL56H3Xl0mAEOlR6tCha', 'Q_ImF09YCOD2ov3sscRzt3FGLIbul1HM', NULL, NULL, NULL, '122.167.27.183', 1454328204, 1454328204, 0, 'Individual', 0),
(115, 'ADM-1602/001', 'sdaf@gmaoicom.com', '$2y$12$8z5o5GJRrCjUgo4.AuAGYOLGQN/of/C8/gyr.XdGwJ2MzkAEYxewC', 'zlRue284ggl-r74we-QOrkDV0ekGHPq-', NULL, NULL, NULL, '127.0.0.1', 1456320757, 1456321010, 0, 'Admin', 0),
(116, 'IND-1603/007', 'binu@gmail.com', '$2y$12$U.ai17mGzvCMMVTXt/bAD.ETjL9js858QJI8C9k//aW3zcXqVdDYa', '3VPA847AhJu6Ul_FPpcb8OKqpBSXYZB-', NULL, NULL, NULL, '127.0.0.1', 1457157323, 1457157323, 0, 'Individual', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessibility_type`
--
ALTER TABLE `accessibility_type`
  ADD PRIMARY KEY (`accessibility_type`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `telephone_id` (`telephone_id`);

--
-- Indexes for table `amenity_type`
--
ALTER TABLE `amenity_type`
  ADD PRIMARY KEY (`amenity`);

--
-- Indexes for table `apartment`
--
ALTER TABLE `apartment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area_partition_type`
--
ALTER TABLE `area_partition_type`
  ADD PRIMARY KEY (`partition_type`);

--
-- Indexes for table `attachment_type`
--
ALTER TABLE `attachment_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attachment` (`attachment`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `bank_loans`
--
ALTER TABLE `bank_loans`
  ADD PRIMARY KEY (`loan`);

--
-- Indexes for table `brand_names`
--
ALTER TABLE `brand_names`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `builder`
--
ALTER TABLE `builder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address` (`address`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reg_off_address_id` (`reg_off_address_id`),
  ADD KEY `company_type_id` (`company_type_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `reg_off_address_id_2` (`reg_off_address_id`),
  ADD KEY `head_off_address_id` (`head_off_address_id`),
  ADD KEY `branch_off_address_id` (`branch_off_address_id`),
  ADD KEY `authorized_person_id` (`authorized_person_id`),
  ADD KEY `company_type_id_2` (`company_type_id`),
  ADD KEY `reg_off_address_id_3` (`reg_off_address_id`),
  ADD KEY `head_off_address_id_2` (`head_off_address_id`),
  ADD KEY `branch_off_address_id_2` (`branch_off_address_id`),
  ADD KEY `authorized_person_id_2` (`authorized_person_id`),
  ADD KEY `authorized_person_id_3` (`authorized_person_id`);

--
-- Indexes for table `company_type`
--
ALTER TABLE `company_type`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`designation`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state` (`state`);

--
-- Indexes for table `executive`
--
ALTER TABLE `executive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `address_proof_id` (`address_proof_id`),
  ADD KEY `present_address_id` (`present_address_id`),
  ADD KEY `permanent_address_id` (`permanent_address_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `title` (`title`),
  ADD KEY `title_2` (`title`);

--
-- Indexes for table `executive_activity`
--
ALTER TABLE `executive_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`),
  ADD KEY `prop_id` (`prop_id`),
  ADD KEY `activity` (`activity`);

--
-- Indexes for table `executive_activity_attachment`
--
ALTER TABLE `executive_activity_attachment`
  ADD KEY `activity_id` (`activity_id`);

--
-- Indexes for table `executive_activity_log`
--
ALTER TABLE `executive_activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `tenant_id` (`tenant_id`),
  ADD KEY `activity_id` (`activity_id`);

--
-- Indexes for table `executive_activity_property`
--
ALTER TABLE `executive_activity_property`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tenant_service` (`tenant_service`),
  ADD KEY `owner_service` (`owner_service`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `executive_activity_txn_details`
--
ALTER TABLE `executive_activity_txn_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_id` (`activity_log_id`);

--
-- Indexes for table `executive_activity_txn_files`
--
ALTER TABLE `executive_activity_txn_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `txn_id` (`txn_id`);

--
-- Indexes for table `executive_activity_type`
--
ALTER TABLE `executive_activity_type`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `executive_attachments`
--
ALTER TABLE `executive_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`),
  ADD KEY `attachment_type` (`attachment_type`);

--
-- Indexes for table `executive_certification`
--
ALTER TABLE `executive_certification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_computer_skill`
--
ALTER TABLE `executive_computer_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_education`
--
ALTER TABLE `executive_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_experience`
--
ALTER TABLE `executive_experience`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_insurance`
--
ALTER TABLE `executive_insurance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_language`
--
ALTER TABLE `executive_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_properties`
--
ALTER TABLE `executive_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`,`property`),
  ADD KEY `property` (`property`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `executive_reference`
--
ALTER TABLE `executive_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_travels`
--
ALTER TABLE `executive_travels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_travel_details`
--
ALTER TABLE `executive_travel_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `executive_vehicle`
--
ALTER TABLE `executive_vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `flat_accessibility_type`
--
ALTER TABLE `flat_accessibility_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_amenity_type`
--
ALTER TABLE `flat_amenity_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_finance`
--
ALTER TABLE `flat_finance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_inventory`
--
ALTER TABLE `flat_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_lease`
--
ALTER TABLE `flat_lease`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_media`
--
ALTER TABLE `flat_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_rent`
--
ALTER TABLE `flat_rent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `govt_approvals`
--
ALTER TABLE `govt_approvals`
  ADD PRIMARY KEY (`approvals`);

--
-- Indexes for table `gratuity_services`
--
ALTER TABLE `gratuity_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service` (`tos`),
  ADD KEY `request_service` (`request_service`);

--
-- Indexes for table `individual`
--
ALTER TABLE `individual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `local_person` (`local_person`,`permanent_address_id`,`present_address_id`,`office_address_id`,`title`),
  ADD KEY `permanent_address_id` (`permanent_address_id`),
  ADD KEY `present_address_id` (`present_address_id`),
  ADD KEY `office_address_id` (`office_address_id`),
  ADD KEY `title` (`title`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat` (`cat`);

--
-- Indexes for table `inventory_brands`
--
ALTER TABLE `inventory_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory` (`inventory`);

--
-- Indexes for table `inventory_category`
--
ALTER TABLE `inventory_category`
  ADD PRIMARY KEY (`cat`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `office_branches`
--
ALTER TABLE `office_branches`
  ADD PRIMARY KEY (`branch`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address` (`address`,`off_address`),
  ADD KEY `title` (`title`),
  ADD KEY `off_address` (`off_address`),
  ADD KEY `off_address_2` (`off_address`);

--
-- Indexes for table `person_title`
--
ALTER TABLE `person_title`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `addres_id` (`address_id`),
  ADD KEY `prop_type` (`prop_type`);

--
-- Indexes for table `properties_pics`
--
ALTER TABLE `properties_pics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prop_id` (`prop_id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `addres_id` (`address_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `prop_type` (`prop_type`);

--
-- Indexes for table `property_accessibilities`
--
ALTER TABLE `property_accessibilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `property_id_2` (`property_id`),
  ADD KEY `accessibility` (`accessibility`);

--
-- Indexes for table `property_amenities`
--
ALTER TABLE `property_amenities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `amenity` (`amenity`),
  ADD KEY `property_id` (`property_id`);

--
-- Indexes for table `property_facilities`
--
ALTER TABLE `property_facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `amenity` (`amenity`),
  ADD KEY `property_id` (`property_id`);

--
-- Indexes for table `property_group`
--
ALTER TABLE `property_group`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `property_partition_type`
--
ALTER TABLE `property_partition_type`
  ADD PRIMARY KEY (`partition_type`),
  ADD KEY `partition_type` (`partition_type`);

--
-- Indexes for table `property_type`
--
ALTER TABLE `property_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `property_units`
--
ALTER TABLE `property_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`);

--
-- Indexes for table `prop_area_partition`
--
ALTER TABLE `prop_area_partition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `punit_id` (`punit_id`),
  ADD KEY `area_partition_type` (`area_partition_type`);

--
-- Indexes for table `prop_unit_attach`
--
ALTER TABLE `prop_unit_attach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prop_unit_id` (`prop_unit_id`);

--
-- Indexes for table `prop_unit_finance`
--
ALTER TABLE `prop_unit_finance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prop_unit_id` (`prop_unit_id`);

--
-- Indexes for table `prop_unit_interior`
--
ALTER TABLE `prop_unit_interior`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`),
  ADD KEY `prop_unit_id` (`prop_unit_id`);

--
-- Indexes for table `prop_unit_interior_cat`
--
ALTER TABLE `prop_unit_interior_cat`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `prop_unit_inventory`
--
ALTER TABLE `prop_unit_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory` (`inventory`,`prop_unit_id`),
  ADD KEY `brand` (`brand`),
  ADD KEY `inventory_2` (`inventory`),
  ADD KEY `prop_unit_id` (`prop_unit_id`);

--
-- Indexes for table `prop_unit_parking`
--
ALTER TABLE `prop_unit_parking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prop_unit_id` (`prop_unit_id`);

--
-- Indexes for table `request_services`
--
ALTER TABLE `request_services`
  ADD PRIMARY KEY (`service_name`);

--
-- Indexes for table `request_service_activity`
--
ALTER TABLE `request_service_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`,`tenant_id`,`member_id`,`admin_approval`,`member_approval`),
  ADD KEY `property_id_2` (`property_id`),
  ADD KEY `tenant_id` (`tenant_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `exec_id` (`exec_id`),
  ADD KEY `owner` (`owner`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `property_id_2` (`property_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `tos` (`tos`),
  ADD KEY `tenant_id` (`tenant_id`);

--
-- Indexes for table `service_type`
--
ALTER TABLE `service_type`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country` (`country`);

--
-- Indexes for table `telephone`
--
ALTER TABLE `telephone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenant`
--
ALTER TABLE `tenant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tenant_type` (`tenant_type`),
  ADD KEY `tenant_type_2` (`tenant_type`),
  ADD KEY `permanent_address_id` (`permanent_address_id`),
  ADD KEY `present_address_id` (`present_address_id`),
  ADD KEY `office_address_id` (`office_address_id`),
  ADD KEY `reference_person_id` (`reference_person_id`),
  ADD KEY `title_id` (`title_id`);

--
-- Indexes for table `tenant_property_allocation`
--
ALTER TABLE `tenant_property_allocation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prop_id` (`prop_id`,`tenant_id`),
  ADD KEY `tenant_id` (`tenant_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `tenant_reference`
--
ALTER TABLE `tenant_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_id` (`address_id`,`office_address_id`),
  ADD KEY `tenant_id` (`tenant_id`),
  ADD KEY `office_address_id` (`office_address_id`);

--
-- Indexes for table `tenant_supervisions`
--
ALTER TABLE `tenant_supervisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `property_id_2` (`property_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `tos` (`tos`),
  ADD KEY `tenant_id` (`tenant_id`);

--
-- Indexes for table `tenant_type`
--
ALTER TABLE `tenant_type`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `todo_list`
--
ALTER TABLE `todo_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exec_id` (`exec_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=927;
--
-- AUTO_INCREMENT for table `apartment`
--
ALTER TABLE `apartment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `attachment_type`
--
ALTER TABLE `attachment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `brand_names`
--
ALTER TABLE `brand_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=592;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `executive`
--
ALTER TABLE `executive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `executive_activity`
--
ALTER TABLE `executive_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'iD';
--
-- AUTO_INCREMENT for table `executive_activity_log`
--
ALTER TABLE `executive_activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `executive_activity_property`
--
ALTER TABLE `executive_activity_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `executive_activity_txn_details`
--
ALTER TABLE `executive_activity_txn_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `executive_activity_txn_files`
--
ALTER TABLE `executive_activity_txn_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `executive_attachments`
--
ALTER TABLE `executive_attachments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `executive_certification`
--
ALTER TABLE `executive_certification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `executive_computer_skill`
--
ALTER TABLE `executive_computer_skill`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `executive_education`
--
ALTER TABLE `executive_education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `executive_experience`
--
ALTER TABLE `executive_experience`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `executive_insurance`
--
ALTER TABLE `executive_insurance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `executive_language`
--
ALTER TABLE `executive_language`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `executive_properties`
--
ALTER TABLE `executive_properties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `executive_reference`
--
ALTER TABLE `executive_reference`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `executive_travels`
--
ALTER TABLE `executive_travels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `executive_travel_details`
--
ALTER TABLE `executive_travel_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `executive_vehicle`
--
ALTER TABLE `executive_vehicle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `flat_accessibility_type`
--
ALTER TABLE `flat_accessibility_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `flat_amenity_type`
--
ALTER TABLE `flat_amenity_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `flat_finance`
--
ALTER TABLE `flat_finance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `flat_inventory`
--
ALTER TABLE `flat_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `flat_lease`
--
ALTER TABLE `flat_lease`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `flat_media`
--
ALTER TABLE `flat_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `flat_rent`
--
ALTER TABLE `flat_rent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `gratuity_services`
--
ALTER TABLE `gratuity_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `individual`
--
ALTER TABLE `individual`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `inventory_brands`
--
ALTER TABLE `inventory_brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=1290;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `person_title`
--
ALTER TABLE `person_title`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `properties_pics`
--
ALTER TABLE `properties_pics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=1046;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `property_accessibilities`
--
ALTER TABLE `property_accessibilities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT for table `property_amenities`
--
ALTER TABLE `property_amenities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT for table `property_facilities`
--
ALTER TABLE `property_facilities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `property_type`
--
ALTER TABLE `property_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `property_units`
--
ALTER TABLE `property_units`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `prop_area_partition`
--
ALTER TABLE `prop_area_partition`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `prop_unit_attach`
--
ALTER TABLE `prop_unit_attach`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `prop_unit_finance`
--
ALTER TABLE `prop_unit_finance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `prop_unit_interior`
--
ALTER TABLE `prop_unit_interior`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=1019;
--
-- AUTO_INCREMENT for table `prop_unit_inventory`
--
ALTER TABLE `prop_unit_inventory`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prop_unit_parking`
--
ALTER TABLE `prop_unit_parking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `request_service_activity`
--
ALTER TABLE `request_service_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `telephone`
--
ALTER TABLE `telephone`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=733;
--
-- AUTO_INCREMENT for table `tenant`
--
ALTER TABLE `tenant`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tenant_property_allocation`
--
ALTER TABLE `tenant_property_allocation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tenant_reference`
--
ALTER TABLE `tenant_reference`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `tenant_supervisions`
--
ALTER TABLE `tenant_supervisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `todo_list`
--
ALTER TABLE `todo_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`telephone_id`) REFERENCES `telephone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `brand_names`
--
ALTER TABLE `brand_names`
  ADD CONSTRAINT `brand_names_ibfk_1` FOREIGN KEY (`category`) REFERENCES `inventory_category` (`cat`) ON UPDATE CASCADE;

--
-- Constraints for table `builder`
--
ALTER TABLE `builder`
  ADD CONSTRAINT `builder_ibfk_1` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `company_ibfk_1` FOREIGN KEY (`company_type_id`) REFERENCES `company_type` (`name`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `company_ibfk_2` FOREIGN KEY (`reg_off_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `company_ibfk_3` FOREIGN KEY (`head_off_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `company_ibfk_4` FOREIGN KEY (`branch_off_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `company_ibfk_5` FOREIGN KEY (`authorized_person_id`) REFERENCES `person` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `district_ibfk_1` FOREIGN KEY (`state`) REFERENCES `state` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive`
--
ALTER TABLE `executive`
  ADD CONSTRAINT `executive_ibfk_3` FOREIGN KEY (`address_proof_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_ibfk_4` FOREIGN KEY (`present_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_ibfk_5` FOREIGN KEY (`permanent_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_ibfk_6` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_ibfk_7` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_ibfk_8` FOREIGN KEY (`title`) REFERENCES `person_title` (`name`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `executive_activity`
--
ALTER TABLE `executive_activity`
  ADD CONSTRAINT `executive_activity_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_ibfk_2` FOREIGN KEY (`prop_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_ibfk_3` FOREIGN KEY (`activity`) REFERENCES `executive_activity_type` (`type`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_activity_log`
--
ALTER TABLE `executive_activity_log`
  ADD CONSTRAINT `executive_activity_log_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_log_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_log_ibfk_3` FOREIGN KEY (`tenant_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_log_ibfk_4` FOREIGN KEY (`activity_id`) REFERENCES `executive_activity_property` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_log_ibfk_5` FOREIGN KEY (`property_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_activity_property`
--
ALTER TABLE `executive_activity_property`
  ADD CONSTRAINT `executive_activity_property_ibfk_1` FOREIGN KEY (`type`) REFERENCES `executive_activity_type` (`type`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_property_ibfk_2` FOREIGN KEY (`tenant_service`) REFERENCES `tenant_property_allocation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_activity_property_ibfk_3` FOREIGN KEY (`owner_service`) REFERENCES `service` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_activity_txn_details`
--
ALTER TABLE `executive_activity_txn_details`
  ADD CONSTRAINT `executive_activity_txn_details_ibfk_1` FOREIGN KEY (`activity_log_id`) REFERENCES `executive_activity_log` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_activity_txn_files`
--
ALTER TABLE `executive_activity_txn_files`
  ADD CONSTRAINT `executive_activity_txn_files_ibfk_3` FOREIGN KEY (`txn_id`) REFERENCES `executive_activity_txn_details` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_attachments`
--
ALTER TABLE `executive_attachments`
  ADD CONSTRAINT `executive_attachments_ibfk_3` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_certification`
--
ALTER TABLE `executive_certification`
  ADD CONSTRAINT `executive_certification_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_computer_skill`
--
ALTER TABLE `executive_computer_skill`
  ADD CONSTRAINT `executive_computer_skill_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_education`
--
ALTER TABLE `executive_education`
  ADD CONSTRAINT `executive_education_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_experience`
--
ALTER TABLE `executive_experience`
  ADD CONSTRAINT `executive_experience_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_insurance`
--
ALTER TABLE `executive_insurance`
  ADD CONSTRAINT `executive_insurance_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_language`
--
ALTER TABLE `executive_language`
  ADD CONSTRAINT `executive_language_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_properties`
--
ALTER TABLE `executive_properties`
  ADD CONSTRAINT `executive_properties_ibfk_2` FOREIGN KEY (`property`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_properties_ibfk_3` FOREIGN KEY (`exec_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `executive_properties_ibfk_4` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_reference`
--
ALTER TABLE `executive_reference`
  ADD CONSTRAINT `executive_reference_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_travels`
--
ALTER TABLE `executive_travels`
  ADD CONSTRAINT `executive_travels_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `executive_travel_details`
--
ALTER TABLE `executive_travel_details`
  ADD CONSTRAINT `executive_travel_details_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `executive_vehicle`
--
ALTER TABLE `executive_vehicle`
  ADD CONSTRAINT `executive_vehicle_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executive` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gratuity_services`
--
ALTER TABLE `gratuity_services`
  ADD CONSTRAINT `gratuity_services_ibfk_1` FOREIGN KEY (`tos`) REFERENCES `service_type` (`name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `gratuity_services_ibfk_2` FOREIGN KEY (`request_service`) REFERENCES `request_services` (`service_name`) ON UPDATE CASCADE;

--
-- Constraints for table `individual`
--
ALTER TABLE `individual`
  ADD CONSTRAINT `individual_ibfk_1` FOREIGN KEY (`local_person`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `individual_ibfk_2` FOREIGN KEY (`permanent_address_id`) REFERENCES `address` (`id`),
  ADD CONSTRAINT `individual_ibfk_3` FOREIGN KEY (`present_address_id`) REFERENCES `address` (`id`),
  ADD CONSTRAINT `individual_ibfk_4` FOREIGN KEY (`office_address_id`) REFERENCES `address` (`id`),
  ADD CONSTRAINT `individual_ibfk_6` FOREIGN KEY (`title`) REFERENCES `person_title` (`id`);

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `inventory_category` (`cat`) ON UPDATE CASCADE;

--
-- Constraints for table `inventory_brands`
--
ALTER TABLE `inventory_brands`
  ADD CONSTRAINT `inventory_brands_ibfk_1` FOREIGN KEY (`inventory`) REFERENCES `inventory` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `person_ibfk_1` FOREIGN KEY (`title`) REFERENCES `person_title` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `person_ibfk_2` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `person_ibfk_3` FOREIGN KEY (`off_address`) REFERENCES `address` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `properties_ibfk_2` FOREIGN KEY (`prop_type`) REFERENCES `property_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `properties_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `properties_pics`
--
ALTER TABLE `properties_pics`
  ADD CONSTRAINT `properties_pics_ibfk_2` FOREIGN KEY (`prop_id`) REFERENCES `property` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `property`
--
ALTER TABLE `property`
  ADD CONSTRAINT `property_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `property_ibfk_4` FOREIGN KEY (`prop_type`) REFERENCES `property_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `property_ibfk_5` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `property_accessibilities`
--
ALTER TABLE `property_accessibilities`
  ADD CONSTRAINT `property_accessibilities_ibfk_1` FOREIGN KEY (`accessibility`) REFERENCES `accessibility_type` (`accessibility_type`) ON UPDATE CASCADE,
  ADD CONSTRAINT `property_accessibilities_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `property_amenities`
--
ALTER TABLE `property_amenities`
  ADD CONSTRAINT `property_amenities_ibfk_1` FOREIGN KEY (`amenity`) REFERENCES `amenity_type` (`amenity`) ON UPDATE CASCADE,
  ADD CONSTRAINT `property_amenities_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `property_facilities`
--
ALTER TABLE `property_facilities`
  ADD CONSTRAINT `property_facilities_ibfk_1` FOREIGN KEY (`amenity`) REFERENCES `amenity_type` (`amenity`) ON UPDATE CASCADE,
  ADD CONSTRAINT `property_facilities_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `property_type`
--
ALTER TABLE `property_type`
  ADD CONSTRAINT `property_type_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `property_group` (`name`) ON UPDATE CASCADE;

--
-- Constraints for table `property_units`
--
ALTER TABLE `property_units`
  ADD CONSTRAINT `property_units_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_area_partition`
--
ALTER TABLE `prop_area_partition`
  ADD CONSTRAINT `prop_area_partition_ibfk_1` FOREIGN KEY (`punit_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prop_area_partition_ibfk_2` FOREIGN KEY (`area_partition_type`) REFERENCES `property_partition_type` (`partition_type`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_unit_attach`
--
ALTER TABLE `prop_unit_attach`
  ADD CONSTRAINT `prop_unit_attach_ibfk_1` FOREIGN KEY (`prop_unit_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_unit_finance`
--
ALTER TABLE `prop_unit_finance`
  ADD CONSTRAINT `prop_unit_finance_ibfk_1` FOREIGN KEY (`prop_unit_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_unit_interior`
--
ALTER TABLE `prop_unit_interior`
  ADD CONSTRAINT `prop_unit_interior_ibfk_1` FOREIGN KEY (`category`) REFERENCES `prop_unit_interior_cat` (`category`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prop_unit_interior_ibfk_2` FOREIGN KEY (`prop_unit_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_unit_inventory`
--
ALTER TABLE `prop_unit_inventory`
  ADD CONSTRAINT `prop_unit_inventory_ibfk_1` FOREIGN KEY (`inventory`) REFERENCES `inventory` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prop_unit_inventory_ibfk_3` FOREIGN KEY (`prop_unit_id`) REFERENCES `prop_area_partition` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `prop_unit_parking`
--
ALTER TABLE `prop_unit_parking`
  ADD CONSTRAINT `prop_unit_parking_ibfk_1` FOREIGN KEY (`prop_unit_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `request_service_activity`
--
ALTER TABLE `request_service_activity`
  ADD CONSTRAINT `request_service_activity_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_service_activity_ibfk_2` FOREIGN KEY (`tenant_id`) REFERENCES `tenant_property_allocation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_service_activity_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `service` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_service_activity_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_service_activity_ibfk_5` FOREIGN KEY (`exec_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_service_activity_ibfk_6` FOREIGN KEY (`owner`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `service_ibfk_3` FOREIGN KEY (`tos`) REFERENCES `service_type` (`name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `service_ibfk_4` FOREIGN KEY (`tenant_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `service_ibfk_5` FOREIGN KEY (`property_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_ibfk_2` FOREIGN KEY (`country`) REFERENCES `country` (`name`) ON UPDATE CASCADE;

--
-- Constraints for table `tenant`
--
ALTER TABLE `tenant`
  ADD CONSTRAINT `tenant_ibfk_1` FOREIGN KEY (`permanent_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_ibfk_2` FOREIGN KEY (`present_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_ibfk_3` FOREIGN KEY (`office_address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_ibfk_4` FOREIGN KEY (`reference_person_id`) REFERENCES `person` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tenant_property_allocation`
--
ALTER TABLE `tenant_property_allocation`
  ADD CONSTRAINT `tenant_property_allocation_ibfk_1` FOREIGN KEY (`prop_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_property_allocation_ibfk_2` FOREIGN KEY (`tenant_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_property_allocation_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tenant_reference`
--
ALTER TABLE `tenant_reference`
  ADD CONSTRAINT `tenant_reference_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_reference_ibfk_2` FOREIGN KEY (`tenant_id`) REFERENCES `tenant` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_reference_ibfk_3` FOREIGN KEY (`office_address_id`) REFERENCES `address` (`id`) ON UPDATE SET NULL;

--
-- Constraints for table `tenant_supervisions`
--
ALTER TABLE `tenant_supervisions`
  ADD CONSTRAINT `tenant_supervisions_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_supervisions_ibfk_2` FOREIGN KEY (`tos`) REFERENCES `service_type` (`name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_supervisions_ibfk_3` FOREIGN KEY (`tenant_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tenant_supervisions_ibfk_4` FOREIGN KEY (`property_id`) REFERENCES `property_units` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `todo_list`
--
ALTER TABLE `todo_list`
  ADD CONSTRAINT `todo_list_ibfk_1` FOREIGN KEY (`exec_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
