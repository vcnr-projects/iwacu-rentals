<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            //'identityClass' => 'common\models\User',
             //'enableAutoLogin' => true,
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
            'enableRegistration'=>false,

        ],
        'member' => [
            'class' => 'app\modules\member\Member',
        ],
        'service' => [
            'class' => 'app\modules\service\ServiceModule',
        ],
        'realty' => [
            'class' => 'app\modules\realty\RealtyModule',
        ],
        'assorted' => [
            'class' => 'app\modules\assorted\AssortedModule',
        ],
    ],
    'components' => [

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,

        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
        ],
        /*'view' => [
            //'basePath' => '@app/themes/basic',
            //'baseUrl' => '@web/themes/basic',
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/themes/views/user'
                ],
            ],
        ],*/

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user',

                ],
            ],
        ],

        /*'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],*/
    ],
    'params' => $params,
];
