<?php

namespace app\modules\member;

class Member extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\member\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
