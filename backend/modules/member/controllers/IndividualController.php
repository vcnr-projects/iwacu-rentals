<?php

namespace app\modules\member\controllers;

use app\modules\member\models\Person;
use Yii;
use app\modules\member\models\Individual;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * IndividualController implements the CRUD actions for Individual model.
 */
class IndividualController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete','incharge'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','incharge'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','incharge'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Individual models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Individual::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Individual model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Individual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Individual();


        $wiz=(isset($_GET['wiz']))?true:false;

        $profile_pic_path = $model->profile_image;
        $pan_file_path = $model->pan_filepath;
        $id_proof_path = $model->id_proofpath;
        $present_address_proof = $model->present_address_proof;
        $permanent_address_proof = $model->permanent_address_proof;

        //$model->scenario="create";
        if (isset($_POST['Individual'])) {

            $model->load(Yii::$app->request->post());

            $model->peradrs->setAttributes($_POST['Address']);
            $model->preadrs->setAttributes($_POST['PAddress']);
            $model->offadrs->setAttributes($_POST['OAddress']);

            $model->pertel->setAttributes($_POST['Telephone']);
            $model->pretel->setAttributes($_POST['PTelephone']);
            $model->offtel->setAttributes($_POST['OTelephone']);


            if(!empty($_FILES['Individual']['name']['profile_image'])){
                $found=false;
                $model->profile_image = UploadedFile::getInstance($model,'profile_image');
                $model->profile_image->name=$model->profile_image->baseName .rand(10000000,999999999) .'.' . $model->profile_image->extension;
                $model->profile_image->saveAs('uploads/' . $model->profile_image->name );
            }


            $model->pan_filepath = UploadedFile::getInstance($model, 'pan_filepath');
            if ($model->pan_filepath) {
                $model->pan_filepath->name = $model->pan_filepath->baseName . rand(10000000, 999999999) . '.' . $model->pan_filepath->extension;
                $model->pan_filepath->saveAs('uploads/' . $model->pan_filepath->name);
            } else {
                $model->pan_filepath = $pan_file_path;
            }

            $model->id_proofpath = UploadedFile::getInstance($model, 'id_proofpath');
            if ($model->id_proofpath) {
                $model->id_proofpath->name = $model->id_proofpath->baseName . rand(10000000, 999999999) . '.' . $model->id_proofpath->extension;
                $model->id_proofpath->saveAs('uploads/' . $model->id_proofpath->name);
            } else {
                $model->id_proofpath = $id_proof_path;
            }

            $model->present_address_proof = UploadedFile::getInstance($model, 'present_address_proof');
            if ($model->present_address_proof) {
                $model->present_address_proof->name = $model->present_address_proof->baseName . rand(10000000, 999999999) . '.' . $model->present_address_proof->extension;
                $model->present_address_proof->saveAs('uploads/' . $model->present_address_proof->name);
            } else {
                $model->present_address_proof = $present_address_proof;
            }

            $model->permanent_address_proof = UploadedFile::getInstance($model, 'permanent_address_proof');
            if ($model->permanent_address_proof) {
                $model->permanent_address_proof->name = $model->permanent_address_proof->baseName . rand(10000000, 999999999) . '.' . $model->permanent_address_proof->extension;
                $model->permanent_address_proof->saveAs('uploads/' . $model->permanent_address_proof->name);
            } else {
                $model->permanent_address_proof = $permanent_address_proof;
            }


            if($model->saver())
                if($wiz)
                    return $this->redirect(['incharge', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Individual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $wiz=(isset($_GET['wiz']))?true:false;
        $img=$model->profile_image;
        $pan_file_path = $model->pan_filepath;
        $id_proof_path = $model->id_proofpath;
        $present_address_proof = $model->present_address_proof;
        $permanent_address_proof = $model->permanent_address_proof;

        if (isset($_POST['Individual'])) {
            $model->load(Yii::$app->request->post());

            $model->peradrs->setAttributes($_POST['Address']);
            $model->preadrs->setAttributes($_POST['PAddress']);
            $model->offadrs->setAttributes($_POST['OAddress']);

            $model->pertel->setAttributes($_POST['Telephone']);
            $model->pretel->setAttributes($_POST['PTelephone']);
            $model->offtel->setAttributes($_POST['OTelephone']);


            $model->profile_image=$img;
            if(!empty($_FILES['Individual']['name']['profile_image'])){
                $found=false;
                $model->profile_image = UploadedFile::getInstance($model,'profile_image');
                $model->profile_image->name=$model->profile_image->baseName .rand(10000000,999999999) .'.' . $model->profile_image->extension;
                $model->profile_image->saveAs('uploads/' . $model->profile_image->name );
            }


            $model->pan_filepath = UploadedFile::getInstance($model, 'pan_filepath');
            if ($model->pan_filepath) {
                $model->pan_filepath->name = $model->pan_filepath->baseName . rand(10000000, 999999999) . '.' . $model->pan_filepath->extension;
                $model->pan_filepath->saveAs('uploads/' . $model->pan_filepath->name);
            } else {
                $model->pan_filepath = $pan_file_path;
            }

            $model->id_proofpath = UploadedFile::getInstance($model, 'id_proofpath');
            if ($model->id_proofpath) {
                $model->id_proofpath->name = $model->id_proofpath->baseName . rand(10000000, 999999999) . '.' . $model->id_proofpath->extension;
                $model->id_proofpath->saveAs('uploads/' . $model->id_proofpath->name);
            } else {
                $model->id_proofpath = $id_proof_path;
            }



             $model->present_address_proof = UploadedFile::getInstance($model, 'present_address_proof');
            if ($model->present_address_proof) {
                $model->present_address_proof->name = $model->present_address_proof->baseName . rand(10000000, 999999999) . '.' . $model->present_address_proof->extension;
                $model->present_address_proof->saveAs('uploads/' . $model->present_address_proof->name);
            } else {
                $model->present_address_proof = $present_address_proof;
            }


            $model->permanent_address_proof = UploadedFile::getInstance($model, 'permanent_address_proof');
            if ($model->permanent_address_proof) {
                $model->permanent_address_proof->name = $model->permanent_address_proof->baseName . rand(10000000, 999999999) . '.' . $model->permanent_address_proof->extension;
                $model->permanent_address_proof->saveAs('uploads/' . $model->permanent_address_proof->name);
            } else {
                $model->permanent_address_proof = $permanent_address_proof;
            }


            if($model->saver())
                if($wiz)
                    return $this->redirect(['incharge', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Individual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Individual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Individual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Individual::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionIncharge($id)
    {



        $comp=Individual::findOne($id);
        if($comp){
            $model=$comp->getLocalPerson()->one();
        }else{
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            return;
        }

        if(empty($model)){
            $model = new Person();
        }

        $img=$model->profile_image;

        if (isset($_POST['Person'])) {
            $model->load(Yii::$app->request->post());

            $model->adrs->setAttributes($_POST['Address']);
            $model->oadrs->setAttributes($_POST['OAddress']);
            $model->atel->setAttributes($_POST['Telephone']);
            $model->otel->setAttributes($_POST['OTelephone']);

            $model->profile_image = UploadedFile::getInstance($model, 'profile_image');
            if($model->profile_image){
                $model->profile_image->name=$model->profile_image->baseName .rand(10000000,999999999) .'.' . $model->profile_image->extension;
                $model->profile_image->saveAs('uploads/' . $model->profile_image->name );
            }else{
                $model->profile_image=$img;
            }

            if($comp->saverincharge($model))
                return $this->redirect(['view', 'id' => $comp->id,'viewtab'=>"off"]);
        }  {
            return $this->render('_formperson', [
                'model' => $model,
                'id'=>$comp->id
            ]);
        }
    }
}
