<?php

namespace app\modules\member\controllers;

use Yii;
use app\modules\member\models\Company;
use app\modules\member\models\Person;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete','incharge'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','incharge'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','incharge'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Company::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();
        $wiz=(isset($_GET['wiz']))?true:false;

        if (isset($_POST['Company'])) {
            $model->load(Yii::$app->request->post());

            $model->headAdrs->setAttributes($_POST['HeadAddress']);
            $model->braAdrs->setAttributes($_POST['BranchAddress']);
            $model->regAdrs->setAttributes($_POST['RegAddress']);

            $model->htel->setAttributes($_POST['HTelephone']);
            $model->rtel->setAttributes($_POST['RTelephone']);
            $model->btel->setAttributes($_POST['BTelephone']);


            if(!empty($_FILES['Company']['name']['mou_filepath'])){
                $found=false;

                $model->mou_filepath = UploadedFile::getInstance($model,'mou_filepath');
                $model->mou_filepath->name=$model->mou_filepath->baseName .rand(10000000,999999999) .'.' . $model->mou_filepath->extension;
                $model->mou_filepath->saveAs('uploads/' . $model->mou_filepath->name );

            }

            if(!empty($_FILES['Company']['name']['aou_filepath'])){
                $found=false;

                $model->aou_filepath = UploadedFile::getInstance($model,'aou_filepath');
                $model->aou_filepath->name=$model->aou_filepath->baseName .rand(10000000,999999999) .'.' . $model->aou_filepath->extension;
                $model->aou_filepath->saveAs('uploads/' . $model->aou_filepath->name );
            }

            if(!empty($_FILES['Company']['name']['logo'])){
                $found=false;

                $model->logo = UploadedFile::getInstance($model,'logo');
                $model->logo->name=$model->logo->baseName .rand(10000000,999999999) .'.' . $model->logo->extension;
                $model->logo->saveAs('uploads/' . $model->logo->name );
            }






            if($model->saver())
                if($wiz)
                    return $this->redirect(['incharge', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $wiz=(isset($_GET['wiz']))?true:false;

        if (isset($_POST['Company'])) {
            $model->load(Yii::$app->request->post());

            $model->headAdrs->setAttributes($_POST['HeadAddress']);
            $model->braAdrs->setAttributes($_POST['BranchAddress']);
            $model->regAdrs->setAttributes($_POST['RegAddress']);
            $model->htel->setAttributes($_POST['HTelephone']);
            $model->rtel->setAttributes($_POST['RTelephone']);
            $model->btel->setAttributes($_POST['BTelephone']);


            if(!empty($_FILES['Company']['name']['mou_filepath'])){
                $found=false;

                $model->mou_filepath = UploadedFile::getInstance($model,'mou_filepath');
                $model->mou_filepath->name=$model->mou_filepath->baseName .rand(10000000,999999999) .'.' . $model->mou_filepath->extension;
                $model->mou_filepath->saveAs('uploads/' . $model->mou_filepath->name );

            }

            if(!empty($_FILES['Company']['name']['aou_filepath'])){
                $found=false;

                $model->aou_filepath = UploadedFile::getInstance($model,'aou_filepath');
                $model->aou_filepath->name=$model->aou_filepath->baseName .rand(10000000,999999999) .'.' . $model->aou_filepath->extension;
                $model->aou_filepath->saveAs('uploads/' . $model->aou_filepath->name );

            }






            if($model->saver())
                if($wiz)
                    return $this->redirect(['createwiz_office', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionIncharge($id)
    {



        $comp=Company::findOne($id);
        if($comp){
            $model=$comp->getAuthorizedPerson()->one();
        }else{
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            return;
        }
        if(empty($model)){
            $model = new Person();
        }



        if (isset($_POST['Person'])) {
            $model->load(Yii::$app->request->post());

            $model->adrs->setAttributes($_POST['Address']);
            $model->oadrs->setAttributes($_POST['OAddress']);
            $model->atel->setAttributes($_POST['Telephone']);
            $model->otel->setAttributes($_POST['OTelephone']);

            if($comp->saverincharge($model))
                return $this->redirect(['view', 'id' => $comp->id]);
        }
        {
            return $this->render('_formperson', [
                'model' => $model,
                'id'=>$comp->id
            ]);
        }
    }
}
