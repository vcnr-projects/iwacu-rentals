<?php

namespace app\modules\member\controllers;

use app\modules\member\models\TenantReference;
use Yii;
use app\modules\member\models\Tenant;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TenantController implements the CRUD actions for Tenant model.
 */
class TenantController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'
                    ,'formwiz'
                    ,'reference'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'formwiz'
                            ,'reference'
                        ],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'
                            ,'formwiz'
                            ,'reference'
                        ],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tenant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Tenant::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tenant model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tenant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tenant();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tenant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tenant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tenant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tenant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tenant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionFormwiz($id=0)
    {
        $model = Tenant::findOne($id);
        if (!$model)
            $model = new Tenant();
        $wiz = (isset($_GET['wiz'])) ? true : false;


        $profile_pic_path = $model->profile_pic_path;
        $pan_file_path = $model->pan_file_path;
        $id_proof_path = $model->id_proof_path;
        $photo_id_proof = $model->photo_id_proof;

        if (isset($_POST['Tenant'])) {

            $model->load(Yii::$app->request->post());

            $model->peradrs->setAttributes($_POST['Address']);
            $model->preadrs->setAttributes($_POST['PAddress']);
            $model->offadrs->setAttributes($_POST['OAddress']);

            $model->pertel->setAttributes($_POST['Telephone']);
            $model->pretel->setAttributes($_POST['PTelephone']);
            $model->offtel->setAttributes($_POST['OTelephone']);
           // Yii::trace(VarDumper::dumpAsString($model->preadrs ),'vardump');


            $model->profile_pic_path = UploadedFile::getInstance($model, 'profile_pic_path');
            if ($model->profile_pic_path) {
                $model->profile_pic_path->name = $model->profile_pic_path->baseName . rand(10000000, 999999999) . '.' . $model->profile_pic_path->extension;
                $model->profile_pic_path->saveAs('uploads/' . $model->profile_pic_path->name);
            } else {
                $model->profile_pic_path = $profile_pic_path;

            }

            $model->pan_file_path = UploadedFile::getInstance($model, 'pan_file_path');
            if ($model->pan_file_path) {
                $model->pan_file_path->name = $model->pan_file_path->baseName . rand(10000000, 999999999) . '.' . $model->pan_file_path->extension;
                $model->pan_file_path->saveAs('uploads/' . $model->pan_file_path->name);
            } else {
                $model->pan_file_path = $pan_file_path;
            }

            $model->id_proof_path = UploadedFile::getInstance($model, 'id_proof_path');
            if ($model->id_proof_path) {
                $model->id_proof_path->name = $model->id_proof_path->baseName . rand(10000000, 999999999) . '.' . $model->id_proof_path->extension;
                $model->id_proof_path->saveAs('uploads/' . $model->id_proof_path->name);
            } else {
                $model->id_proof_path = $id_proof_path;
            }

            $model->photo_id_proof = UploadedFile::getInstance($model, 'photo_id_proof');
            if ($model->photo_id_proof) {
                $model->photo_id_proof->name = $model->photo_id_proof->baseName . rand(10000000, 999999999) . '.' . $model->photo_id_proof->extension;
                $model->photo_id_proof->saveAs('uploads/' . $model->photo_id_proof->name);
            } else {
                $model->photo_id_proof = $photo_id_proof;
            }

            $model->address_proof_path = UploadedFile::getInstance($model, 'address_proof_path');
            if ($model->address_proof_path) {
                $model->address_proof_path->name = $model->address_proof_path->baseName . rand(10000000, 999999999) . '.' . $model->address_proof_path->extension;
                $model->address_proof_path->saveAs('uploads/' . $model->address_proof_path->name);
            } else {
                $model->address_proof_path = $photo_id_proof;
            }


            if ($model->saver())
                if ($wiz)
                    return $this->redirect(['createwiz_experience', 'id' => $model->id, 'wiz' => 1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);


        }
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionReference($id){

        $comp=Tenant::findOne($id);
        if($comp){
            $model=$comp->getReferencePerson()->one();
        }else{
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            return;
        }
        if(empty($model)){
            $model = new TenantReference();
        }

        if (isset($_POST['TenantReference'])) {
            $model->load(Yii::$app->request->post());

            $model->adrs->setAttributes($_POST['Address']);
            $model->offadrs->setAttributes($_POST['OAddress']);
            $model->atel->setAttributes($_POST['Telephone']);
            $model->otel->setAttributes($_POST['OTelephone']);

            if($comp->saverreference($model))
                return $this->redirect(['view', 'id' => $comp->id,'viewtab'=>"off"]);
        }  {
            return $this->render('_formreference', [
                'model' => $model,
            ]);
        }
    }




}
