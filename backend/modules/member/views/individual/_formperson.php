<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Person */
/* @var $form yii\widgets\ActiveForm */



/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */

$this->title = 'Local Person ';
$this->params['breadcrumbs'][] = ['label' => 'Individuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs ">
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/member/individual/update","id"=>$id])  ?>">Individual</a></li>
        <li class="active" ><a data-toggle="tab"  href="#">Local Person</a></li>
    </ul>

    <div class="person-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?php echo $form->errorSummary($model); ?>


        <?= $form->field($model, 'title',['template'=>'<label class="childdispInline " > <span class="redstar">Title</span>  :{input}{error}</label>'])->radioList(
            \yii\helpers\ArrayHelper::map(\app\models\PersonTitle::find()->asArray()->all(),
                'id','name')
        ) ?>


        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'relation',['template'=>'<label>Relationship with member</label>{input}{error}'])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


        <? //= $form->field($model, 'relation_person')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'dob')->widget(\kartik\date\DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter Date of Birth'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy',
                'todayHighlight'=>true,
            ]
        ]); ?>

        <?= $form->field($model, 'marital_status',['template'=>'<label class="childdispInline " > <span class="redstar">Marital Status</span>  :{input}{error}</label>'])->radioList(["Single"=>"Single","Married"=>"Married"]) ?>

        <?= $form->field($model, 'gender',['template'=>'<label class="childdispInline " > <span class="redstar">Gender</span>  :{input}{error}</label>'])->radioList(["Male"=>"Male","Female"=>"Female"]) ?>


        <?= $form->field($model, 'spouse')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'wed_ann')->widget(\kartik\widgets\DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter Wedding anniversary'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy',
            ]
        ]); ?>

        <?php
        $baseurl=Yii::$app->urlManager->baseUrl;
        if($model->profile_image)
            echo "
                <a href='{$baseurl}/uploads/{$model->profile_image}'>{$model->profile_image}</a>
           ";
        ?>
        <?=
        $form->field($model, 'profile_image')->widget(\kartik\widgets\FileInput::classname(), [
            'options' => ['accept' => 'application/pdf,image/*','showUpload'=>false,],
            'pluginOptions'=>['showUpload'=>false]
            ,
        ]); ?>




        <!--begin:  address-->

        <?php
        $ha=$model->getAddress0()->one();
        if(($ha)){
            $model->adrs->setAttributes($ha->getAttributes());
            $model->adrs->id=$ha->id;
        }
        $hatel=$model->adrs->getTelephone()->one();
        if(empty($hatel)){
            $hatel=new \app\models\Telephone();
            $model->atel=$hatel;
        }else {
            $model->atel = new \app\models\Telephone();
            $model->atel->setAttributes($hatel->getAttributes());
            $model->atel->id=$hatel->id;
        }
        ?>
        <div class=" panel panel-default ">
            <div class="panel-heading">Residential Address </div>
            <div class="panel-body">
                <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
                <div style="display: none">
                <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
                </div>

                <?= $form->field($model->adrs, 'city')->
                widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                    'wildcard' => '%QUERY',
                                    'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                    'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                    'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                                ],

                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'pluginEvents'=>[

                            "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#peraddress-country').val(cd.country);
                $('#peraddress-state').val(cd.state);
            }
        }
     }",
                            "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);
     }",
                        ],
                        'options' => ["id"=>'peraddress-city'],
                    ]) ?>

                <?= $form->field($model->adrs, 'postal_code')->textInput() ?>


                <?= $form->field($model->adrs, 'country')->textInput(["id"=>'peraddress-country']);
                /*widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                    //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ["id"=>'peraddress-country'],
                        //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                    ])*/ ?>

                <?= $form->field($model->adrs, 'state')->textInput(["id"=>'peraddress-state']);
                /*widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                    //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY',
                                    'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-state').val())
                                                    settings.url=settings.url+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),
                                ]
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ["id"=>'peraddress-state'],
                        //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                    ])*/ ?>


               <!-- <?/*= $form->field($model->adrs, 'city')->textInput() */?>
                <?/*= $form->field($model->adrs, 'state')->textInput() */?>
                --><?/*= $form->field($model->adrs, 'country')->textInput() */?>
                <?= $form->field($model->atel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) ?>
                <?= $form->field($model->atel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) ?>
                <?= $form->field($model->atel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) ?>
                <?= $form->field($model->atel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) ?>

            </div>
        </div>
        <!--end:  address-->

        <!--begin: office address-->

        <?php
        $ha=$model->getOffAddress()->one();
        if(($ha)){
            $model->oadrs->setAttributes($ha->getAttributes());
            $model->oadrs->id=$ha->id;
        }
        $hatel=$model->oadrs->getTelephone()->one();
        if(empty($hatel)){
            $hatel=new \app\models\Telephone();
            $model->otel=$hatel;
        }else {
            $model->otel = new \app\models\Telephone();
            $model->otel->setAttributes($hatel->getAttributes());
            $model->otel->id=$hatel->id;
        }
        ?>
        <div class=" panel panel-default ">
            <div class="panel-heading"> Office Details </div>
            <div class="panel-body">
                <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>


                <?php $model->oadrs->scenario="notrequired" ?>
                <?= $form->field($model->oadrs, 'adrs_line1',['template'=>"<label class='noredstar'>Address</label>{input}{error}"])->textarea(['name'=>'OAddress[adrs_line1]']) ?>
                <div style="display: none">
                <?= $form->field($model->oadrs, 'adrs_line2',['template'=>"<label class='noredstar'>Address</label>{input}{error}"])->textarea(['name'=>'OAddress[adrs_line2]']) ?>
                </div>

                <?= $form->field($model->oadrs, 'city',['template'=>"<label class='noredstar'>City</label>{input}{error}"])->
                widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'&q=%QUERY',
                                    'wildcard' => '%QUERY',
                                    'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                    'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                    'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                                ],

                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'pluginEvents'=>[

                            "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#oaddress-country').val(cd.country);
                $('#oaddress-state').val(cd.state);
            }
        }
     }",
                            "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                        ],
                        'options' => ["id"=>'oaddress-city','name'=>'OAddress[city]'],
                    ]) ?>

                <?= $form->field($model->oadrs, 'postal_code',['template'=>"<label class='noredstar'>Postal Code</label>{input}{error}"])->textInput(['name'=>'OAddress[postal_code]']) ?>

                <?= $form->field($model->oadrs, 'state',['template'=>"<label class='noredstar'>State</label>{input}{error}"])->textInput(["id"=>'oaddress-state','name'=>'OAddress[state]']);
                /*widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                    //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY',
                                    'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#oaddress-state').val())
                                                    settings.url=settings.url+'&country='+$('#oaddress-country').val()
                                                    return settings;

                                                }"),
                                ]
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ["id"=>'oaddress-state','name'=>'OAddress[state]'],
                        //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                    ])*/ ?>

                <?= $form->field($model->oadrs, 'country',['template'=>"<label class='noredstar'>Country</label>{input}{error}"])->textInput(["id"=>'oaddress-country','name'=>'OAddress[country]']);
               /* widget(\kartik\typeahead\Typeahead::className(),[
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                'remote' => [
                                    'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                    //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ["id"=>'oaddress-country','name'=>'OAddress[country]'],
                        //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                    ])*/ ?>




                <?/*= $form->field($model->oadrs, 'city')->textInput(['name'=>'OAddress[city]']) */?><!--
                <?/*= $form->field($model->oadrs, 'state')->textInput(['name'=>'OAddress[state]']) */?>
                <?/*= $form->field($model->oadrs, 'country')->textInput(['name'=>'OAddress[country]']) */?>
                -->
                <?/*= $form->field($model->otel,'primary_landline')->textInput(['name'=>'OTelephone[primary_landline]']) */?><!--
                <?/*= $form->field($model->otel,'secondary_landline')->textInput(['name'=>'OTelephone[secondary_landline]']) */?>
                <?/*= $form->field($model->otel,'primary_mobile')->textInput(['name'=>'OTelephone[primary_mobile]']) */?>
                --><?/*= $form->field($model->otel,'secondary_mobile')->textInput(['name'=>'OTelephone[secondary_mobile]']) */?>
                <?= $form->field($model->otel,'primary_landline',['template'=>"<label>Telephone No. Direct</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_landline]']) ?>
                <?= $form->field($model->otel,'secondary_landline',['template'=>"<label>Telephone No. office</label>{input}{error}"])->textInput(['name'=>'OTelephone[secondary_landline]']) ?>
                <?= $form->field($model->otel,'primary_mobile',['template'=>"<label>Extension No.</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_mobile]']) ?>

            </div>
        </div>
        <!--end: office address-->




        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>



