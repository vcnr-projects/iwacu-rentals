<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Individual */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'Individuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <? //= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>
    <?php
    $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
    $actclstxt='class="active"';
    ?>
    <ul class="nav nav-tabs ">
        <li  <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen">Individual</a></li>
        <li  <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Local Person</a></li>
<!--        <li><a  data-toggle="tab" href="#contract">Contracts</a></li>
-->
        <li  <?= ($viewtab=="prop")?$actclstxt:"" ?> ><a  data-toggle="tab" href="#prop">Properties</a></li>
        <li  <?= ($viewtab=="mprop")?$actclstxt:"" ?> ><a  data-toggle="tab" href="#mprop">Managed Properties</a></li>
    </ul>
    <div class="tab-content">
        <!-- begin:individual-->
        <div id="gen" class="tab-pane fade in   <?= ($viewtab=="gen")?"active":"" ?> ">
            <h3>Individual Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/individual/update","id"=>$model->id]) ?>" >Edit</a>
            <div class="row">
                <div class="col-md-9">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label'=>"Member ID",
                                'attribute'=>'userrow.username',
                            ],

                            [
                                'label'=>"Title",
                                'attribute'=>'title0.name',
                            ],


                            'fname',
                            'lname',
                            'relation',
                            'relative',

                            'email:email',
                            'dob',
                            'gender',

                            'marital_status',
                            'spouse',
                            'wed_ann',
                            'residential_status',
                            'pan',
                            [
                                'label'=>" PAN Card",
                                'format'=>'html',
                                'value'=>($model->pan_filepath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->pan_filepath,["target"=>"_blank"]):"",
                            ],

                            'id_proof_doctype',

                            [
                                'label'=>" ID Proof ",
                                'format'=>'html',
                                'value'=>($model->id_proofpath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->id_proofpath,["target"=>"_blank"]):"",
                            ],
                            [
                                'attribute'=>'fpreadrs',
                                'format'=>'Ntext',
                                'label'=>"Present Address",
                            ],
                            [
                                'label'=>" Present Address Proof ",
                                'format'=>'html',
                                'value'=>($model->present_address_proof)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->present_address_proof,["target"=>"_blank"]):"",
                            ],
                            [
                                'attribute'=>'fperadrs',
                                'format'=>'Ntext',
                                'label'=>"Permanent Address",
                            ],
                            [
                                'label'=>" Permanent Address Proof ",
                                'format'=>'html',
                                'value'=>($model->permanent_address_proof)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->permanent_address_proof,["target"=>"_blank"]):"",
                            ],
                            'company_name',
                            'designation',
                            [
                                'attribute'=>'foffadrs',
                                'format'=>'Ntext',
                                'label'=>"Office Address",
                            ],
                        ],
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <img src="<?= Yii::$app->urlManager->baseUrl."/uploads/".$model->profile_image ?>" />
                </div>
            </div>
        </div>
        <!-- end:individual-->

        <!--begin:Authroized person details-->
        <div id="off" class="tab-pane fade in  <?= ($viewtab=="off")?"active":"" ?> ">
            <h3>Local Person Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/individual/incharge","id"=>$model->id]) ?>" >Edit</a>
            <?php
            $model->aperson=$model->getLocalPerson()->one();
            if($model->aperson):
                ?>
                <div class="row">
                <div class="col-md-9">
                <?= DetailView::widget([
                'model' => $model->aperson,
                'attributes' => [
                    [
                        'attribute'=>'title0.name',
                        'label'=>'Title',
                    ],
                    'fname',
                    'lname',
                    'relation',
                    'email:email',
                    //'relation_person',
                    'dob',
                    'marital_status',
                    'gender',
                    'spouse',
                    'wed_ann',
                    [
                        'attribute'=>'fadrs',
                        'format'=>'Ntext',
                        'label'=>"Address",
                    ],
                    /*[
                        'label'=>" Permanent Address Proof ",
                        'format'=>'html',
                        'value'=>($model->permanent_address_proof)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->permanent_address_proof,["target"=>"_blank"]):"",
                    ],*/
                    'company',
                    'designation',
                    [
                        'attribute'=>'foadrs',
                        'format'=>'Ntext',
                        'label'=>" Office Address",
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <img src="<?= Yii::$app->urlManager->baseUrl."/uploads/".$model->aperson->profile_image ?>" />
        </div>
                </div>
            <?php endif; ?>
        </div>
        <!--end:Authroized person  details-->


        <!--begin:Contract details
        <div id="contract" class="tab-pane fade in ">
            <h3>Contract Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/service/contract/create","member_id"=>$model->member_id]) ?>" >Create Contract</a>
            <?php
            $contracts=\app\modules\service\models\Service::findAll(["member_id"=>$model->member_id]);


            foreach($contracts as $contract):
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= DetailView::widget([
                            'model' => $contract,
                            'attributes' => [
                                [
                                    'label'=>'Edit',
                                    'format'=>'html',
                                    'value'=>Html::a("Update Contract",['/service/contract/update',"id"=>$contract->id,"member_id"=>$contract->member_id])
                                ],
                                'contract_id',
                                'date',
                                'expiry_date',
                                'tos',
                                'prop_type',
                                'ptype',
                                [
                                    'attribute'=>'property_id',
                                    'format'=>'html',
                                    'value'=>($contract->property_id)?'property.name':Html::a("Create Property",['/service/property/create',"contract_id"=>$contract->id])
                                ]
                            ],
                        ]) ?>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
        <!--end:Contract details-->


        <!-- begin :Property -->
        <div id="prop" class="tab-pane fade in  <?= ($viewtab=="prop")?"active":"" ?>  ">
            <h3>Property  Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/property/create","member_id"=>$model->member_id]) ?>" >Create Property</a>

            <?php
            $model->properties=$model->getProperty()->all();

            ?>
            <div class="well">
                <h3>Property</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name.</th>
                        <th>Property Type</th>
                        <th>No. of Units</th>
                        <th>View</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($model->properties as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?> </td>
                            <td><?= $par->name ?></td>
                            <td><?= $par->getPropType()->one()->name ?></td>
                            <td><?= $par->no_units ?></td>
                            <td>
                                <?php
                                $url=Yii::$app->urlManager->createUrl(["/realty/property/view","id"=>$par->id]);
                                echo "
                                <a href='{$url}'><span class='glyphicon glyphicon-eye-open'></span></a>
                                ";
                                ?>
                            </td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end :Property -->


        <!-- begin :managed Property -->
        <div id="mprop" class="tab-pane fade in  <?= ($viewtab=="mprop")?"active":"" ?>  ">
            <h3>Managed Properties  Details</h3>

            <?php
            $sql="select pu.*,p.name as pname ,s.contract_id,s.expiry_date
            from property p ,property_units pu left join (select * from  service where is_active=1) s on s.property_id=pu.id and s.tos='Tenancy Management'
                  where pu.property_id=p.id   and p.member_id=:id";
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$model->member_id);
            $res=$cmd->queryAll();
            //var_dump($res);

            ?>
            <div class="well">
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th> Name.</th>
                        <th>Property Name</th>
                        <th>Contract</th>
                        <th>Contract Expiry Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($res as $i=> $par){
                        $par=(object)$par;
                        ?>
                        <tr>
                            <td><?= $i+1 ?> </td>
                            <td><?= $par->name ?></td>
                            <td><?= $par->pname ?></td>
                            <td><?= $par->contract_id ?></td>
                            <td><?= $par->expiry_date ?></td>
                            <td>
                                <?php
                                $url=Yii::$app->urlManager->createUrl(["/realty/propertyunits/view","id"=>$par->id]);
                                echo "
                                <a href='{$url}'><span class='glyphicon glyphicon-eye-open'></span></a>
                                ";
                                ?>
                            </td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end :managed Property -->



    </div>




</div>
