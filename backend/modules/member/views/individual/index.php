<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Individuals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Individual', ['create',"wiz"=>1], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

		
 		 'fname',
       			 'lname',
             'email:email',
            //'id',
            //'member_id',
            //'local_person',
            //'residential_status',
            //'permanent_address_id',
            // 'present_address_id',
            // 'company_name',
            // 'designation',
            // 'office_address_id',
            // 'profile_image',
            // 'title',
           
            // 'dob',
            // 'marital_status',
            // 'spouse',
            // 'wed_ann',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}',],
        ],
    ]); ?>

</div>
