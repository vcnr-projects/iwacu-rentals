<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Individual */

$this->title = 'Update Individual: ' . ' ' . $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'Individuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="individual-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs ">
        <li class="active" ><a  data-toggle="tab"  href="#">Individual</a></li>
        <li  ><a   href="<?= Yii::$app->urlManager->createUrl(["/member/individual/incharge","id"=>$model->id])  ?>">Local Person</a></li>
    </ul>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
