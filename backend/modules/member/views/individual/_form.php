<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Individual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="individual-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model); ?>


    <?php //= $form->field($model, 'member_id')->textInput() ?>

    <?php //= $form->field($model, 'local_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title',['template'=>'<label class="childdispInline " > <span class="redstar">Title</span>  :{input}{error}</label>'])->radioList(
        \yii\helpers\ArrayHelper::map(\app\models\PersonTitle::find()->asArray()->all(),
            'id','name')
    ) ?>

    <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'relation',['template'=>'<label class="childdispInline " > <span class="redstar">Relationship</span>  :{input}{error}</label>'])->radioList(["Son of"=>"Son of","Wife of"=>"Wife of","Daughter of"=>"Daughter of"])?>

    <?= $form->field($model, 'relative',['template'=>'{input}{error}'])->textInput(['maxlength' => true,"placeholder"=>"Relative Name"]) ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Date of Birth'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?= $form->field($model, 'gender',['template'=>'<label class="childdispInline " > <span class="redstar">Gender</span>  :{input}{error}</label>'])->radioList(["Male"=>"Male","Female"=>"Female"]) ?>



    <?= $form->field($model, 'marital_status',['template'=>'<label class="childdispInline " > <span class="redstar">Marital Status</span>  :{input}{error}</label>'])->radioList(["Single"=>"Single","Married"=>"Married"]) ?>

    <?= $form->field($model, 'spouse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wed_ann')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Wedding Anniversary'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?php
    $otherval=($model->residential_status!='NRI'&&$model->residential_status!='PIO'&&$model->residential_status!='Resident Indian')?$model->residential_status:"";

    ?>
    <?= $form->field($model, 'residential_status',['template'=>'<label class="childdispInline " > <span class="redstar">Residential Status</span>  :{input}{error}</label>'])->radioList(["Resident Indian"=>"Resident Indian","NRI"=>"NRI","PIO"=>"PIO",$otherval=>"Other"],['inline'=>true ,'class'=>'resident'])?>
    <div id="otherinputdiv" <?= ($model->residential_status!='NRI'&&$model->residential_status!='PIO'&&$model->residential_status!='Resident Indian'&&$model->residential_status!='')?"": 'style="display: none"' ?> >
        <input type="text" id="otherinput" onblur="otherinputfill(this.value)" class="form-control" placeholder="Residential status"  value="<?= $otherval?>" />
    </div>
    <?php
    $script = <<< JS

$(document).ready(function(){
    $("input[type=radio]",".resident").on('change',function(){
        //alert($(this).val());
        var val=$(this).val();
        if(val==''){
            $("#otherinputdiv").css("display","block");
        }else{
            $("#otherinputdiv").css("display","none");
        }

    });

});
function otherinputfill(val){
$("input[type=radio]",".resident").each(function(){
//alert($(this).closest('label').text() );
    if($(this).closest('label').text().trim()=="Other".trim()){
        $(this).val(val);
           // alert($(this).val() );

    }
});
}


JS;
    $this->registerJs($script,\yii\web\View::POS_END);
    ?>
    <?/*= $form->field($model, 'residential_status')->widget(\kartik\typeahead\TypeaheadBasic::className(),[
        'data' => [
            "NRI",
            "Resident Indian",
            "PIO",
        ],
        'pluginOptions' => ['highlight' => true],
    ])
    */?>

    <? //= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>


    <?php
    $baseurl=Yii::$app->urlManager->baseUrl;
    if($model->profile_image)
        echo "
                <a href='{$baseurl}/uploads/{$model->profile_image}'>{$model->profile_image}</a>
           ";
    ?>
    <?=
    $form->field($model, 'profile_image')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*','showUpload'=>false,],
        'pluginOptions'=>['showUpload'=>false]
        ,
    ]); ?>


    <?= $form->field($model, 'pan')->textInput(['maxlength' => true]) ?>

    <?php

    if($model->pan_filepath)
        echo "
                <a href='{$baseurl}/uploads/{$model->pan_filepath}'>{$model->pan_filepath}</a>
           ";
    ?>
    <?= $form->field($model, 'pan_filepath')->widget(\kartik\file\FileInput::className(),
        ['pluginOptions'=>['showUpload' => false]]) ?>


    <? //=$form->field($model,'id_proof_doctype')->radioList(["DL"=>"DL","Aadar card"=>"Aadar card","Passport"=>"Passport","Voter Id"=>"Voter Id"],["onchange"=>"$('#idproof').css('display','inherit')"])?>



    <?php
/*
    if(!$model->id_proof_doctype) {
        echo "<div style='display:none' id='idproof'>";
    }
    if($model->id_proofpath)
        echo "
                <a href='{$baseurl}/uploads/{$model->id_proofpath}'>{$model->id_proofpath}</a>
           ";
    */?><!--
    <?/*= $form->field($model, 'id_proofpath')->widget(\kartik\file\FileInput::className(),
        ['pluginOptions'=>['showUpload' => false]]) */?>

    --><?php /*if(!$model->id_proof_doctype) {
        echo "</div >";
    }  */?>


    <!--begin: present address-->
    <?php
    $ha=$model->getPresentAddress()->one();
    if(($ha)){
        $model->preadrs->setAttributes($ha->getAttributes());
        $model->preadrs->id=$ha->id;
    }
    $hatel=$model->preadrs->getTelephone()->one();
    if(empty($hatel)){
        if(!$model->pretel) {
            $hatel = new \app\models\Telephone();
            $model->pretel = $hatel;
        }
    }else {
        $model->pretel = new \app\models\Telephone();
        $model->pretel->setAttributes($hatel->getAttributes());
        $model->pretel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading"> Present Address

        </div>
        <div class="panel-body">
            <?= $form->field($model->preadrs, 'adrs_line1')->textarea(['name'=>'PAddress[adrs_line1]']) ?>
            <div style="display:none">
                <?= $form->field($model->preadrs, 'adrs_line2')->textarea(['name'=>'PAddress[adrs_line2]']) ?>
            </div>

            <?= $form->field($model->preadrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#preaddress-country').val(cd.country);
                $('#preaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#preaddress-country').val(datum.country);
            $('#preaddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'preaddress-city','name'=>'PAddress[city]'],
                ]) ?>

            <?= $form->field($model->preadrs, 'postal_code')->textInput(['name'=>'PAddress[postal_code]']) ?>


            <?= $form->field($model->preadrs, 'state')->textInput(["id"=>'preaddress-state','name'=>'PAddress[state]']);
            /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#preaddress-state').val())
                                                    settings.url=settings.url+'&country='+$('#preaddress-country').val()
                                                    return settings;

                                                }"),
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'preaddress-state','name'=>'PAddress[state]'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>
            <?= $form->field($model->preadrs, 'country')->textInput(["id"=>'preaddress-country','name'=>'PAddress[country]']);
            /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'preaddress-country','name'=>'PAddress[country]'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>




            <? //= $form->field($model->preadrs, 'city')->textInput(['name'=>'PAddress[city]']) ?>
            <? //= $form->field($model->preadrs, 'state')->textInput(['name'=>'PAddress[state]']) ?>
            <? //= $form->field($model->preadrs, 'country')->textInput(['name'=>'PAddress[country]']) ?>
            <?= $form->field($model->pretel,'primary_landline')->textInput(['name'=>'PTelephone[primary_landline]']) ?>
            <?= $form->field($model->pretel,'secondary_landline')->textInput(['name'=>'PTelephone[secondary_landline]']) ?>
            <?= $form->field($model->pretel,'primary_mobile')->textInput(['name'=>'PTelephone[primary_mobile]']) ?>
            <?= $form->field($model->pretel,'secondary_mobile')->textInput(['name'=>'PTelephone[secondary_mobile]']) ?>

            <?= $form->field($model->preadrs, 'proof_doc_type')->radioList([
                "DL"=>"DL"
                ,"Aadhar Card"=>"Aadhar Card"
                ,"Passport"=>"Passport"
                ,"Voter ID"=>"Voter ID"
            ],['name'=>'PAddress[proof_doc_type]'])
            ?>
            <?php

            if($model->present_address_proof)
                echo "
                <a href='{$baseurl}/uploads/{$model->present_address_proof}'>{$model->present_address_proof}</a>
           ";
            ?>
            <?= $form->field($model, 'present_address_proof')->widget(\kartik\file\FileInput::className(),
                ['pluginOptions'=>['showUpload' => false]]) ?>

        </div>
        <script>
            function sapa(){
                /*$("[name='PAddress\[adrs_line1\]']").get(0).value=($("[name='Address\[adrs_line1\]']").get(0).value);
                $("[name='PAddress\[adrs_line2\]']").get(0).value=($("[name='Address\[adrs_line2\]']").get(0).value);
                $("[name='PAddress\[city\]']").get(0).value=($("[name='Address\[city\]']").get(0).value);
                $("[name='PAddress\[state\]']").get(0).value=($("[name='Address\[state\]']").get(0).value);
                $("[name='PAddress\[country\]']").get(0).value=($("[name='Address\[country\]']").get(0).value);
                $("[name='PAddress\[postal_code\]']").get(0).value=($("[name='Address\[postal_code\]']").get(0).value);
                $("[name='PTelephone\[primary_landline\]']").get(0).value=($("[name='Telephone\[primary_landline\]']").get(0).value);
                $("[name='PTelephone\[secondary_landline\]']").get(0).value=($("[name='Telephone\[secondary_landline\]']").get(0).value);
                $("[name='PTelephone\[primary_mobile\]']").get(0).value=($("[name='Telephone\[primary_mobile\]']").get(0).value);
                $("[name='PTelephone\[secondary_mobile\]']").get(0).value=($("[name='Telephone\[secondary_mobile\]']").get(0).value);
                */
                $("[name='Address\[adrs_line1\]']").get(0).value=($("[name='PAddress\[adrs_line1\]']").get(0).value);
                $("[name='Address\[adrs_line2\]']").get(0).value=($("[name='PAddress\[adrs_line2\]']").get(0).value);
                $("[name='Address\[city\]']").get(0).value=($("[name='PAddress\[city\]']").get(0).value);
                $("[name='Address\[state\]']").get(0).value=($("[name='PAddress\[state\]']").get(0).value);
                $("[name='Address\[country\]']").get(0).value=($("[name='PAddress\[country\]']").get(0).value);
                $("[name='Address\[postal_code\]']").get(0).value=($("[name='PAddress\[postal_code\]']").get(0).value);
                $("[name='Telephone\[primary_landline\]']").get(0).value=($("[name='PTelephone\[primary_landline\]']").get(0).value);
                $("[name='Telephone\[secondary_landline\]']").get(0).value=($("[name='PTelephone\[secondary_landline\]']").get(0).value);
                $("[name='Telephone\[primary_mobile\]']").get(0).value=($("[name='PTelephone\[primary_mobile\]']").get(0).value);
                $("[name='Telephone\[secondary_mobile\]']").get(0).value=($("[name='PTelephone\[secondary_mobile\]']").get(0).value);

            }
        </script>

    </div>
    <!--end: present address-->

    <!--begin:  address-->

    <?php
    $ha=$model->getPermanentAddress()->one();
    if(($ha)){
        $model->peradrs->setAttributes($ha->getAttributes());
        $model->peradrs->id=$ha->id;
    }
    $hatel=$model->peradrs->getTelephone()->one();
    if(empty($hatel)){
        if(!$model->pertel) {
            $hatel = new \app\models\Telephone();
            $model->pertel = $hatel;
        }
    }else {

        $model->pertel = new \app\models\Telephone();
        $model->pertel->setAttributes($hatel->getAttributes());
        $model->pertel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading">Permanent Address  <button type="button" onclick="sapa()">Same as Present Address</button></div>
        <div class="panel-body">
            <?= $form->field($model->peradrs, 'adrs_line1')->textarea() ?>
            <div style="display:none">
            <?= $form->field($model->peradrs, 'adrs_line2')->textarea() ?>
            </div>

            <script>
                var cityData={};
            </script>
            <?php
            $cityurl=Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']);
            ?>


            <?= $form->field($model->peradrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                 'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

    "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];
            if(cd.value&&cd.value==datum){
                $('#peraddress-country').val(cd.country);
                $('#peraddress-state').val(cd.state);
            }
        }
     }",
    "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'peraddress-city'],
                ]) ?>
            <?= $form->field($model->peradrs, 'postal_code')->textInput() ?>

            <?= $form->field($model->peradrs, 'state')->textInput(["id"=>'peraddress-state'])
            /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-state').val())
                                                    settings.url=settings.url+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'peraddress-state'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>

            <?= $form->field($model->peradrs, 'country')->textInput(["id"=>'peraddress-country']);
            /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'peraddress-country'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ]) */?>





            <? //= $form->field($model->peradrs, 'city')->textInput() ?>
            <? //= $form->field($model->peradrs, 'state')->textInput() ?>
            <? //= $form->field($model->peradrs, 'country')->textInput() ?>
            <?= $form->field($model->pertel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) ?>
            <?= $form->field($model->pertel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) ?>
            <?= $form->field($model->pertel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) ?>
            <?= $form->field($model->pertel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) ?>
            <?= $form->field($model->peradrs, 'proof_doc_type')->radioList([
                "DL"=>"DL"
                ,"Aadhar Card"=>"Aadhar Card"
                ,"Passport"=>"Passport"
                ,"Voter ID"=>"Voter ID"
            ],[])
            ?>
            <?php

            if($model->permanent_address_proof)
                echo "
                <a href='{$baseurl}/uploads/{$model->permanent_address_proof}'>{$model->permanent_address_proof}</a>
           ";
            ?>
            <?= $form->field($model, 'permanent_address_proof')->widget(\kartik\file\FileInput::className(),
                ['pluginOptions'=>['showUpload' => false]]) ?>
        </div>
    </div>
    <!--end:  address-->



    <!--begin:  office address-->
    <?php
    $ha=$model->getOfficeAddress()->one();
    if(($ha)){
        $model->offadrs->setAttributes($ha->getAttributes());
        $model->offadrs->id=$ha->id;
    }
    $hatel=$model->offadrs->getTelephone()->one();
    if(empty($hatel)){
        if(!empty($model->offtel)) {
            $hatel = new \app\models\Telephone();
            $model->offtel = $hatel;
        }
    }else {
        $model->offtel = new \app\models\Telephone();
        $model->offtel->setAttributes($hatel->getAttributes());
        $model->offtel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading"> Office Details </div>
        <div class="panel-body">
            <?= $form->field($model,'company_name')->textInput()?>
            <?= $form->field($model,'designation')->textInput()?>
            <?php $model->offadrs->scenario="notrequired" ?>
            <?= $form->field($model->offadrs, 'adrs_line1')->textarea(['name'=>'OAddress[adrs_line1]']) ?>
            <div style="display:none">
            <?= $form->field($model->offadrs, 'adrs_line2')->textarea(['name'=>'OAddress[adrs_line2]']) ?>
            </div>

            <?= $form->field($model->offadrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#offaddress-country').val(cd.country);
                $('#offaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#offaddress-country').val(datum.country);
            $('#offaddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'offaddress-city','name'=>'OAddress[city]'],

                ]) ?>

            <?= $form->field($model->offadrs, 'state')->textInput(["id"=>'offaddress-state','name'=>'OAddress[state]']);
           /* widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#offaddress-state').val())
                                                    settings.url=settings.url+'&country='+$('#offaddress-country').val()
                                                    return settings;

                                                }"),
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'offaddress-state','name'=>'OAddress[state]'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>


            <?= $form->field($model->offadrs, 'country')->textInput(["id"=>'offaddress-country','name'=>'OAddress[country]']);
            /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'offaddress-country','name'=>'OAddress[country]'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>





            <? //= $form->field($model->offadrs, 'city')->textInput(['name'=>'OAddress[city]']) ?>
            <? //= $form->field($model->offadrs, 'state')->textInput(['name'=>'OAddress[state]']) ?>
            <? //= $form->field($model->offadrs, 'country')->textInput(['name'=>'OAddress[country]']) ?>
            <?= $form->field($model->offadrs, 'postal_code')->textInput(['name'=>'OAddress[postal_code]']) ?>
            <?/*= $form->field($model->offtel,'primary_landline')->textInput(['name'=>'OTelephone[primary_landline]']) */?><!--
            <?/*= $form->field($model->offtel,'secondary_landline')->textInput(['name'=>'OTelephone[secondary_landline]']) */?>
            <?/*= $form->field($model->offtel,'primary_mobile')->textInput(['name'=>'OTelephone[primary_mobile]']) */?>
            --><?/*= $form->field($model->offtel,'secondary_mobile')->textInput(['name'=>'OTelephone[secondary_mobile]']) */?>
            <?= $form->field($model->offtel,'primary_landline',['template'=>"<label>Telephone No. Direct</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_landline]']) ?>
            <?= $form->field($model->offtel,'secondary_landline',['template'=>"<label>Telephone No. office</label>{input}{error}"])->textInput(['name'=>'OTelephone[secondary_landline]']) ?>
            <?= $form->field($model->offtel,'primary_mobile',['template'=>"<label>Extension No.</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_mobile]']) ?>


        </div>
    </div>
    <!--end:  address-->





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
