<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'fname',
            'lname',
            'email:email',
            // 'relation',
            // 'relation_person',
            // 'dob',
            // 'marital_status',
            // 'spouse',
            // 'wed_ann',
            // 'address',
            // 'off_address',
            // 'company',
            // 'designation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
