<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Person */
/* @var $form yii\widgets\ActiveForm */



/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */

$this->title = 'Reference Person ';
$this->params['breadcrumbs'][] = ['label' => 'Tenant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'title',['template'=>'<label class="childdispInline " > <span class="redstar">Title</span>  :{input}{error}</label>'])->radioList(
        \yii\helpers\ArrayHelper::map(\app\models\PersonTitle::find()->asArray()->all(),
            'id','name')
    ) ?>

    <?= $form->field($model, 'fname') ?>
    <?= $form->field($model, 'lname') ?>

    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'company') ?>
    <?= $form->field($model, 'designation') ?>

    <!--begin:  address-->

    <?php
    $ha=$model->getAddress()->one();
    if(($ha)){
        $model->adrs->setAttributes($ha->getAttributes());
        $model->adrs->id=$ha->id;
    }else{
        $ha=new app\models\Address();
    }
    $hatel=$model->adrs->getTelephone()->one();
    if(empty($hatel)){
        $hatel=new \app\models\Telephone();
        $model->atel=$hatel;
    }else {
        $model->atel = new \app\models\Telephone();
        $model->atel->setAttributes($hatel->getAttributes());
        $model->atel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading">Address </div>
        <div class="panel-body "  >
            <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
            <div style="display: none">
            <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
            </div>
            <? //= $form->field($model->adrs, 'city')->textInput() ?>
            <?= $form->field($model->adrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#address-country').val(cd.country);
                $('#address-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                    ],
                    'options' => ["id"=>'address-city','name'=>'Address[city]'],
                ]) ?>
            <?= $form->field($model->adrs, 'state')->textInput() ?>
            <?= $form->field($model->adrs, 'country')->textInput() ?>
            <?= $form->field($model->adrs, 'postal_code')->textInput() ?>
            <?= $form->field($model->atel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) ?>
            <?= $form->field($model->atel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) ?>
            <?= $form->field($model->atel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) ?>
            <?= $form->field($model->atel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) ?>

        </div>
    </div>
    <!--end:  address-->

    <!--begin: office address-->

    <?php
    $ha=$model->getOfficeAddress()->one();
    if(($ha)){
        $model->offadrs->setAttributes($ha->getAttributes());
        $model->offadrs->id=$ha->id;
    }else{
        $ha=new app\models\Address();
    }
    $hatel=$model->offadrs->getTelephone()->one();
    if(empty($hatel)){
        $hatel=new \app\models\Telephone();
        $model->otel=$hatel;
    }else {
        $model->otel = new \app\models\Telephone();
        $model->otel->setAttributes($hatel->getAttributes());
        $model->otel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading" ><a href="#demo" class="btn-block " data-toggle="collapse">Office Address</a> </div>
        <div class="panel-body collapse <?= isset($model->offadrs->adrs_line1)?" in ":"  "?>" id="demo">
           <?php  $model->offadrs->scenario="notRequired" ?>
            <?= $form->field($model->offadrs, 'adrs_line1')->textarea(['name'=>'OAddress[adrs_line1]']) ?>
            <div style="display: none">
            <?= $form->field($model->offadrs, 'adrs_line2')->textarea(['name'=>'OAddress[adrs_line2]']) ?>
            </div>
            <? //= $form->field($model->offadrs, 'city')->textInput(['name'=>'OAddress[city]']) ?>

            <?= $form->field($model->offadrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#oaddress-country').val(cd.country);
                $('#oaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                    ],
                    'options' => ["id"=>'oaddress-city','name'=>'OAddress[city]'],
                ]) ?>

            <?= $form->field($model->offadrs, 'state')->textInput(['name'=>'OAddress[state]','id'=>'oaddress-state']) ?>
            <?= $form->field($model->offadrs, 'country')->textInput(['name'=>'OAddress[country]','id'=>'oaddress-country']) ?>
            <?= $form->field($model->offadrs, 'postal_code')->textInput(['name'=>'OAddress[postal_code]']) ?>
            <?= $form->field($model->otel,'primary_landline',['template'=>"<label>Telephone No. Direct</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_landline]']) ?>
            <?= $form->field($model->otel,'secondary_landline',['template'=>"<label>Telephone No. office</label>{input}{error}"])->textInput(['name'=>'OTelephone[secondary_landline]']) ?>
            <?= $form->field($model->otel,'primary_mobile',['template'=>"<label>Extension No.</label>{input}{error}"])->textInput(['name'=>'OTelephone[primary_mobile]']) ?>
            <? //= $form->field($model->otel,'secondary_mobile')->textInput(['name'=>'OTelephone[secondary_mobile]']) ?>

        </div>
    </div>
    <!--end: office address-->

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->