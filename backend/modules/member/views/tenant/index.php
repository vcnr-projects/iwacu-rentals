<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tenant', ['formwiz'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

		'fname',
             'lname',
             'email_id:email',
		'no_people',
            /*'id',
            'member_id',
            'tenant_type',
            'tenant_family_type',
            'no_people',
*/
            // 'pan_no',
            // 'food_habits',
            // 'residential_status',
            // 'has_pets',
            // 'permanent_address_id',
            // 'present_address_id',
            // 'office_address_id',
            // 'reference_person_id',
            // 'profile_pic_path',
            // 'pan_file_path',
            // 'id_proof_path',
            // 'address_proof_path',
            // 'title_id',
            // 'fname',
            // 'lname',
            // 'email_id:email',
            // 'marital_status',
            // 'spouse',
            // 'wed_ann',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
            ],
        ],
    ]); ?>

</div>
