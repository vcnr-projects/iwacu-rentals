<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Tenant */

$this->title = 'Tenant Details Form';
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
