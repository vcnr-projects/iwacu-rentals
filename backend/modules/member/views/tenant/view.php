<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Tenant */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?/*= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) */?><!--
        --><?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>
    <?php
    $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
    $actclstxt='class="active"';
    ?>
    <ul class="nav nav-tabs ">
        <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen">Tenant</a></li>
        <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Reference Person</a></li>
    </ul>

    <div class="tab-content">
        <!-- begin:tenant-->
        <div id="gen" class="tab-pane fade in <?= ($viewtab=="gen")?"active":"" ?>">
            <h3>Tenant Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/tenant/formwiz","id"=>$model->id]) ?>" >Edit</a>


            <div class="row">
                <div class="col-md-9">

            <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'member_id',
            //'tenant_type',

            [
              'attribute'=> 'title.name',
              'label'=>'Title'
            ],

            'fname',
            'lname',
            'relation',
            'relative',
            'email_id:email',
            'dob',
            'marital_status',
            'spouse',
            'wed_ann',
            'tenant_family_type',
            'no_people',
            'food_habits',


            'residential_status',
            [
                'attribute'=>'has_pets',
                'value'=>($model->has_pets)?"Yes":"NO",
            ],
            'pan_no',



            //'profile_pic_path',
            [
                'label'=>" PAN ",
                'format'=>'html',
                'value'=>($model->pan_file_path)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->pan_file_path,["target"=>"_blank"]):"",
            ],
            'id_proof_doctype',
            [
                'label'=>" Photo ID Proof ",
                'format'=>'html',
                'value'=>($model->photo_id_proof)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->photo_id_proof,["target"=>"_blank"]):"",
            ],
            [
                'attribute'=>'fperadrs',
                'format'=>'Ntext',
                'label'=>"Permanent Address",
            ],
            [
                'label'=>" Address Proof ",
                'format'=>'html',
                'value'=>($model->address_proof_path)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->address_proof_path,["target"=>"_blank"]):"",
            ],
            [
                'attribute'=>'fpreadrs',
                'format'=>'Ntext',
                'label'=>"Present Address",
            ],
            [
                'label'=>"Present Address Proof ",
                'format'=>'html',
                'value'=>($model->id_proof_path)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->id_proof_path,["target"=>"_blank"]):"",
            ],
            'company',
            'designation',


            [
                'attribute'=>'foffadrs',
                'format'=>'Ntext',
                'label'=>"Office Address",
            ],
        ],
    ]) ?>
                    </div>
                <div class="col-md-3">
                    <img src="<?= Yii::$app->urlManager->baseUrl."/uploads/".$model->profile_pic_path ?>" />
                    </div>
                </div>
        </div>
        <!-- end:tenant-->


        <!--begin:Reference person details-->
        <div id="off" class="tab-pane fade in <?= ($viewtab=="off")?"active":"" ?> ">
            <h3>Reference Person Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/tenant/reference","id"=>$model->id]) ?>" >Edit</a>
            <?php
            $model->aperson=$model->getReferencePerson()->one();
            if($model->aperson):
                ?>
                <?= DetailView::widget([
                'model' => $model->aperson,
                'attributes' => [

                    [
                        'attribute'=> 'title0.name',
                        'label'=>'Title'
                    ],
                    'fname',
                    'lname',
                    'email:email',
                    'company',
                    'designation',
                    [
                        'attribute'=>'fadrs',
                        'format'=>'Ntext',
                        'label'=>"Address",
                    ],
                    [
                        'attribute'=>'foadrs',
                        'format'=>'Ntext',
                        'label'=>" Office Address",
                    ],

                ],
            ]) ?>
            <?php endif; ?>
        </div>
        <!--end:Authroized person  details-->


    </div>
