<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?/*= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) */?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>-->

    <ul class="nav nav-tabs ">
        <li class="active"><a data-toggle="tab" href="#gen">Company</a></li>
        <li><a data-toggle="tab" href="#off">Authorized Person</a></li>
    </ul>

    <div class="tab-content">

        <!--begin:Company details-->
        <div id="gen" class="tab-pane fade in active">
            <h3>Company Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/company/update","id"=>$model->id]) ?>" >Edit</a>
            <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            //'member_id',
            'company_type_id',
            'name',
            [
                'attribute'=>'fhadrs',
                'format'=>'Ntext',
                'label'=>"Head  Office Address",
            ],
            [
                'attribute'=>'fradrs',
                'format'=>'Ntext',
                'label'=>"Registered  Office Address",
            ],
            [
                'attribute'=>'fbadrs',
                'format'=>'Ntext',
                'label'=>"Branch  Office Address",
            ],
            'pan_number',
            [
                //'attribute'=>'adrsProofpath',
                'label'=>" MOU Document",
                'format'=>'html',
                'value'=>($model->mou_filepath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->mou_filepath,["target"=>"_blank"]):"",
            ],
            [
                //'attribute'=>'adrsProofpath',
                'label'=>" AOU Document",
                'format'=>'html',
                'value'=>($model->aou_filepath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->aou_filepath,["target"=>"_blank"]):"",
            ],

        ],
    ]) ?>
        </div>
        <!--end:Company details-->


        <!--begin:Authroized person details-->
        <div id="off" class="tab-pane fade in ">
            <h3>Authorised Person Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/member/company/incharge","id"=>$model->id]) ?>" >Edit</a>
            <?php
              $model->aperson=$model->getAuthorizedPerson()->one();
              if($model->aperson):
            ?>
            <?= DetailView::widget([
                'model' => $model->aperson,
                'attributes' => [

                    [
                      'attribute'=>'title0.name',
                        'label'=>'Title',
                    ],

                    'fname',
                    'lname',
                    'email:email',
                    'relation',
                    'relation_person',
                    'dob',
                    'marital_status',
                    'spouse',
                    'wed_ann',
                    [
                        'attribute'=>'fadrs',
                        'format'=>'Ntext',
                        'label'=>"Address",
                    ],
                    [
                        'attribute'=>'foadrs',
                        'format'=>'Ntext',
                        'label'=>" Office Address",
                    ],
                    'company',
                    'designation',
                ],
            ]) ?>
            <?php endif; ?>
        </div>
        <!--end:Authroized person  details-->



    </div>

</div>
