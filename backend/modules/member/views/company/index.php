<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Company', ['create',"wiz"=>1], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            //'member_id',
            'company_type_id',
            'name',
            //'reg_off_address_id',
            // 'head_off_address_id',
            // 'branch_off_address_id',
            // 'pan_number',
            // 'authorized_person_id',
            // 'mou_filepath',
            // 'aou_filepath',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
