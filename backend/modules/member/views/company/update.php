<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */

$this->title = 'Update Company: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs ">
        <li class="active"><a data-toggle="tab" href="#gen">Company</a></li>
        <li><a  href="<?= Yii::$app->urlManager->createUrl(["/member/company/incharge","id"=>$model->id])  ?>">Authorized Person</a></li>
    </ul>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
