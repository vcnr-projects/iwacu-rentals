<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model); ?>


    <?//= $form->field($model, 'member_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_type_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\modules\member\models\CompanyType::find()->asArray()->all(),'name','name'
    )) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <!--begin: head office address-->

    <?php
    $ha=$model->getHeadOffAddress()->one();
    if(($ha)){
        $model->headAdrs->setAttributes($ha->getAttributes());
        $model->headAdrs->id=$ha->id;
    }
    $hatel=$model->headAdrs->getTelephone()->one();
    if(empty($hatel)){
        $hatel=new \app\models\Telephone();
        $model->htel=$hatel;
    }else {
        $model->htel = new \app\models\Telephone();
        $model->htel->setAttributes($hatel->getAttributes());
        $model->htel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading">Head Office Address </div>
        <div class="panel-body">
            <?= $form->field($model->headAdrs, 'adrs_line1')->textarea() ?>
            <div style="display:none">
            <?= $form->field($model->headAdrs, 'adrs_line2')->textarea() ?>
            </div>

            <?= $form->field($model->headAdrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#headaddress-country').val(cd.country);
                $('#headaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#headaddress-country').val(datum.country);
            $('#headaddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'headaddress-city','name'=>'HeadAddress[city]'],

                ]) ?>


            <? //= $form->field($model->headAdrs, 'city')->textInput() ?>
            <?= $form->field($model->headAdrs, 'postal_code')->textInput() ?>
            <?= $form->field($model->headAdrs, 'state')->textInput() ?>
            <?= $form->field($model->headAdrs, 'country')->textInput() ?>

            <?= $form->field($model->htel,'primary_landline',['template'=>'<label>Phone No. Broad Line</label>{input}{error}'])->textInput(['name'=>'HTelephone[primary_landline]']) ?>
            <? //= $form->field($model->htel,'secondary_landline')->textInput(['name'=>'HTelephone[secondary_landline]']) ?>
            <? //= $form->field($model->htel,'primary_mobile')->textInput(['name'=>'HTelephone[primary_mobile]']) ?>
            <? //= $form->field($model->htel,'secondary_mobile')->textInput(['name'=>'HTelephone[secondary_mobile]']) ?>

        </div>
    </div>
    <!--end: head office address-->

    <!--begin: reg office address-->

    <?php
    $ra=$model->getRegOffAddress()->one();
    if(($ha)){
        $model->regAdrs->setAttributes($ha->getAttributes());
        $model->regAdrs->id=$ha->id;
    }
    $ratel=$model->regAdrs->getTelephone()->one();
    if(empty($ratel)){
        $ratel=new \app\models\Telephone();
        $model->rtel=$ratel;
    }else {
        $model->rtel = new \app\models\Telephone();
        $model->rtel->setAttributes($ratel->getAttributes());
        $model->rtel->id=$ratel->id;
    }
    ?>
    <div class=" panel panel-default ">
        <div class="panel-heading">Registered Office Address </div>
        <div class="panel-body">
            <?= $form->field($model->regAdrs, 'adrs_line1')->textarea() ?>
            <div style="display:none">
                <?= $form->field($model->regAdrs, 'adrs_line2')->textarea() ?>
            </div>

            <?= $form->field($model->regAdrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#regaddress-country').val(cd.country);
                $('#regaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#regaddress-country').val(datum.country);
            $('#regaddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'regaddress-city','name'=>'RegAddress[city]'],

                ]) ?>
            <?= $form->field($model->regAdrs, 'postal_code')->textInput() ?>
            <?= $form->field($model->regAdrs, 'state')->textInput() ?>
            <?= $form->field($model->regAdrs, 'country')->textInput() ?>

            <?= $form->field($model->rtel,'primary_landline',['template'=>'<label>Phone No. Broad Line</label>{input}{error}'])->textInput(['name'=>'RTelephone[primary_landline]']) ?>
            <? //= $form->field($model->rtel,'secondary_landline')->textInput(['name'=>'RTelephone[secondary_landline]']) ?>
            <? //= $form->field($model->rtel,'primary_mobile')->textInput(['name'=>'RTelephone[primary_mobile]']) ?>
            <? //= $form->field($model->rtel,'secondary_mobile')->textInput(['name'=>'RTelephone[secondary_mobile]']) ?>

        </div>
    </div>
    <!--end: reg office address-->

    <!--begin: branch office address-->

    <?php
    $ba=$model->getBranchOffAddress()->one();
    if(($ba)){
        $model->braAdrs->setAttributes($ha->getAttributes());
        $model->braAdrs->id=$ba->id;
    }
    $batel=$model->braAdrs->getTelephone()->one();
    if(empty($batel)){
        $batel=new \app\models\Telephone();
        $model->btel=$batel;
    }else {
        $model->btel = new \app\models\Telephone();
        $model->btel->setAttributes($batel->getAttributes());
        $model->btel->id=$batel->id;
    }

    ?>
    <?php $model->braAdrs->scenario="notrequired" ?>
    <div class=" panel panel-default ">
        <div class="panel-heading">Branch Office Address </div>
        <div class="panel-body">
            <?= $form->field($model->braAdrs, 'adrs_line1')->textarea() ?>
            <div style="display:none">
                <?= $form->field($model->braAdrs, 'adrs_line2')->textarea() ?>
            </div>

            <?= $form->field($model->braAdrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.console.log(query);
                                                    console.console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                    return settings;

                                                }"),*/
                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                            ],

                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'pluginEvents'=>[

                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#branchaddress-country').val(cd.country);
                $('#branchaddress-state').val(cd.state);
            }
        }
     }",
                        "typeahead:select" => "function(e, datum) { console.log(datum);
            $('#branchaddress-country').val(datum.country);
            $('#branchaddress-state').val(datum.state);
     }",
                    ],
                    'options' => ["id"=>'branchaddress-city','name'=>'BranchAddress[city]'],

                ]) ?>
            <?= $form->field($model->braAdrs, 'state')->textInput() ?>
            <?= $form->field($model->braAdrs, 'country')->textInput() ?>
            <?= $form->field($model->braAdrs, 'postal_code')->textInput() ?>
            <?= $form->field($model->btel,'primary_landline',['template'=>'<label>Phone No. Broad Line</label>{input}{error}'])->textInput(['name'=>'BTelephone[primary_landline]']) ?>
            <? //= $form->field($model->btel,'secondary_landline')->textInput(['name'=>'BTelephone[secondary_landline]']) ?>
            <? //= $form->field($model->btel,'primary_mobile')->textInput(['name'=>'BTelephone[primary_mobile]']) ?>
            <? //= $form->field($model->btel,'secondary_mobile')->textInput(['name'=>'BTelephone[secondary_mobile]']) ?>

        </div>
    </div>
    <!--end: reg office address-->


    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'logo')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*',],
    ]); ?>
    <?//= $form->field($model, 'authorized_person_id')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'mou_filepath')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*,application/octet-stream',],
    ]); ?>

    <?=
    $form->field($model, 'aou_filepath')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*,application/octet-stream',],
    ]); ?>

    <?//= $form->field($model, 'mou_filepath')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'aou_filepath')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
