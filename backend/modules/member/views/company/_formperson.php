<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Person */
/* @var $form yii\widgets\ActiveForm */



/* @var $this yii\web\View */
/* @var $model app\modules\member\models\Company */

$this->title = 'Authorised Person ';
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs ">
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/member/company/update","id"=>$id])  ?>">Company</a></li>
        <li class="active" ><a data-toggle="tab"  href="#">Authorized Person</a></li>
    </ul>

    <div class="person-form">

        <?php $form = ActiveForm::begin(); ?>
        <?php echo $form->errorSummary($model); ?>


        <?= $form->field($model, 'title')->dropDownList(
            \yii\helpers\ArrayHelper::map(\app\models\PersonTitle::find()->asArray()->all(),
                'id','name')
        ) ?>

        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'relation')->dropDownList([
            "Son of"=>"Son of",
            "Wife of"=>"Wife of",
            "Daughter of"=>"Daughter of",
        ]) ?>

        <?= $form->field($model, 'relation_person')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'dob')->widget(\kartik\date\DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter Date of Birth'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy',
            ]
        ]); ?>

        <?= $form->field($model, 'marital_status')->radioList(["Single"=>"Single","Married"=>"Married"]) ?>

        <?= $form->field($model, 'gender')->radioList(["Male"=>"Male","Female"=>"Female"]) ?>


        <?= $form->field($model, 'spouse')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'wed_ann')->widget(\kartik\widgets\DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter Date of Birth'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy',
            ]
        ]); ?>

        <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>



        <!--begin:  address-->

        <?php
        $ha=$model->getAddress0()->one();
        if(($ha)){
            $model->adrs->setAttributes($ha->getAttributes());
            $model->adrs->id=$ha->id;
        }
        $hatel=$model->adrs->getTelephone()->one();
        if(empty($hatel)){
            $hatel=new \app\models\Telephone();
            $model->atel=$hatel;
        }else {
            $model->atel = new \app\models\Telephone();
            $model->atel->setAttributes($hatel->getAttributes());
            $model->atel->id=$hatel->id;
        }
        ?>
        <div class=" panel panel-default ">
            <div class="panel-heading">Address </div>
            <div class="panel-body">
                <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
                <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
                <?= $form->field($model->adrs, 'city')->textInput() ?>
                <?= $form->field($model->adrs, 'state')->textInput() ?>
                <?= $form->field($model->adrs, 'country')->textInput() ?>
                <?= $form->field($model->adrs, 'postal_code')->textInput() ?>
                <?= $form->field($model->atel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) ?>
                <?= $form->field($model->atel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) ?>
                <?= $form->field($model->atel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) ?>
                <?= $form->field($model->atel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) ?>

            </div>
        </div>
        <!--end:  address-->

        <!--begin: office address-->

        <?php
        $ha=$model->getOffAddress()->one();
        if(($ha)){
            $model->oadrs->setAttributes($ha->getAttributes());
            $model->oadrs->id=$ha->id;
        }
        $hatel=$model->oadrs->getTelephone()->one();
        if(empty($hatel)){
            $hatel=new \app\models\Telephone();
            $model->otel=$hatel;
        }else {
            $model->otel = new \app\models\Telephone();
            $model->otel->setAttributes($hatel->getAttributes());
            $model->otel->id=$hatel->id;
        }
        ?>
        <div class=" panel panel-default ">
            <div class="panel-heading"> Office Address </div>
            <div class="panel-body">
                <?= $form->field($model->oadrs, 'adrs_line1')->textarea(['name'=>'OAddress[adrs_line1]']) ?>
                <?= $form->field($model->oadrs, 'adrs_line2')->textarea(['name'=>'OAddress[adrs_line2]']) ?>
                <?= $form->field($model->oadrs, 'city')->textInput(['name'=>'OAddress[city]']) ?>
                <?= $form->field($model->oadrs, 'state')->textInput(['name'=>'OAddress[state]']) ?>
                <?= $form->field($model->oadrs, 'country')->textInput(['name'=>'OAddress[country]']) ?>
                <?= $form->field($model->oadrs, 'postal_code')->textInput(['name'=>'OAddress[postal_code]']) ?>
                <?= $form->field($model->otel,'primary_landline')->textInput(['name'=>'OTelephone[primary_landline]']) ?>
                <?= $form->field($model->otel,'secondary_landline')->textInput(['name'=>'OTelephone[secondary_landline]']) ?>
                <?= $form->field($model->otel,'primary_mobile')->textInput(['name'=>'OTelephone[primary_mobile]']) ?>
                <?= $form->field($model->otel,'secondary_mobile')->textInput(['name'=>'OTelephone[secondary_mobile]']) ?>

            </div>
        </div>
        <!--end: office address-->




        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>



