<?php

namespace app\modules\member\models;

use app\models\Telephone;
use app\models\Address;
use dektrium\user\models\User;
use Yii;
use yii\base\Exception;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "company".
 *
 * @property string $id
 * @property string $member_id
 * @property string $company_type_id
 * @property string $name
 * @property string $reg_off_address_id
 * @property string $head_off_address_id
 * @property string $branch_off_address_id
 * @property string $pan_number
 * @property string $authorized_person_id
 * @property string $mou_filepath
 * @property string $aou_filepath
 *
 * @property Person $authorizedPerson
 * @property CompanyType $companyType
 * @property Address $regOffAddress
 * @property Address $headOffAddress
 * @property Address $branchOffAddress
 */
class Company extends \yii\db\ActiveRecord
{

    protected $user;
    public $headAdrs;
    public $regAdrs;
    public $braAdrs;
    public $htel;
    public $rtel;
    public $btel;
    public $aperson;

    public function __construct(){
        $this->headAdrs = new HeadAddress();
        $this->regAdrs = new RegAddress();
        $this->braAdrs = new BranchAddress();
        $this->htel = new Telephone();
        $this->rtel = new Telephone();
        $this->btel = new Telephone();
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'company_type_id', 'name', 'reg_off_address_id', 'pan_number','email'], 'required'],
            [['member_id', 'reg_off_address_id', 'head_off_address_id', 'branch_off_address_id', 'authorized_person_id'], 'integer'],
            [['company_type_id', 'name','email'], 'string', 'max' => 200],
            [['pan_number'], 'string', 'max' => 50],
            [['mou_filepath', 'aou_filepath','logo'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member',
            'company_type_id' => 'Company Type',
            'name' => 'Name',
            'reg_off_address_id' => 'Registered Office Address',
            'head_off_address_id' => 'Head Office Address',
            'branch_off_address_id' => 'Branch Office Address',
            'pan_number' => 'PAN No.',
            'authorized_person_id' => 'Authorized Person',
            'mou_filepath' => 'MOU',
            'aou_filepath' => 'AOU',
            'email' => 'Email ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorizedPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'authorized_person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompanyType::className(), ['name' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegOffAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'reg_off_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeadOffAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'head_off_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchOffAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'branch_off_address_id']);
    }

    public function saver(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if($this->isNewRecord){
                /* $user=new User();
                 $user->username=$this->fname." ".$this->lname.date('d');
                 $user->email=$this->email;*/
                $this->user = \Yii::createObject([
                    'class'    => \common\models\User::className(),
                    'scenario' => 'register'
                ]);

                $this->user->setAttributes([
                    'email'    => $this->email,
                    'username' => 'VHP',
                    'password' => '1234567890',
                    'user_type' => 'Company',
                ]);

                Yii::trace(get_class($this->user),'vardump');
                if(!$this->user->register()){
                    Yii::trace(VarDumper::dumpAsString($this->user->getErrors()),'vardump');
                    foreach($this->user->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);

                    throw new Exception($errmsg);
                }
                else
                    $this->member_id=$this->user->id;


                if(!empty($this->htel->primary_landline)||!empty($this->htel->primary_mobile)) {
                    if (!$this->htel->save()) {
                        foreach($this->htel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->headAdrs->telephone_id=$this->htel->id;
                    }
                }
                if(!$this->headAdrs->save()){
                    foreach($this->headAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->head_off_address_id = $this->headAdrs->id;
                }



                //reg
                if(!empty($this->rtel->primary_landline)||!empty($this->rtel->primary_mobile)) {
                    if (!$this->rtel->save()) {
                        foreach($this->rtel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->regAdrs->telephone_id=$this->rtel->id;
                    }
                }
                if(!$this->regAdrs->save()){
                    foreach($this->regAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->reg_off_address_id = $this->regAdrs->id;
                }

                //branch
                if(!empty($this->btel->primary_landline)||!empty($this->btel->primary_mobile)) {
                    if (!$this->btel->save()) {
                        foreach($this->btel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->braAdrs->telephone_id=$this->btel->id;
                    }
                }
                if(!$this->braAdrs->save()){
                    foreach($this->braAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->branch_off_address_id = $this->braAdrs->id;
                }




            }else{
                // update address

                /* begin: address*/
                $hadrs=Address::findOne($this->head_off_address_id);
                $hadrs->setAttributes(['adrs_line1'=>$this->headAdrs->adrs_line1,
                    'adrs_line2'=>$this->headAdrs->adrs_line2,
                    'city'=>$this->headAdrs->city,
                    'state'=>$this->headAdrs->state,
                    'country'=>$this->headAdrs->country,
                    'postal_code'=>$this->headAdrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->htel->primary_landline,
                    'primary_mobile'=>$this->htel->primary_mobile,
                    'secondary_landline'=>$this->htel->secondary_landline,
                    'secondary_mobile'=>$this->htel->secondary_mobile,
                ]);

                $this->htel=$htel;
                if (!$this->htel->save()) {
                    foreach($this->htel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->htel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->headAdrs=$hadrs;

                /*end: address*/

                /* begin: reg address*/
                $hadrs=Address::findOne($this->reg_off_address_id);
                $hadrs->setAttributes(['adrs_line1'=>$this->regAdrs->adrs_line1,
                    'adrs_line2'=>$this->regAdrs->adrs_line2,
                    'city'=>$this->regAdrs->city,
                    'state'=>$this->regAdrs->state,
                    'country'=>$this->regAdrs->country,
                    'postal_code'=>$this->regAdrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->rtel->primary_landline,
                    'primary_mobile'=>$this->rtel->primary_mobile,
                    'secondary_landline'=>$this->rtel->secondary_landline,
                    'secondary_mobile'=>$this->rtel->secondary_mobile,
                ]);

                $this->rtel=$htel;
                if (!$this->rtel->save()) {
                    foreach($this->rtel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->rtel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->regAdrs=$hadrs;

                /*end: address*/


                /* begin: branch address*/
                $hadrs=Address::findOne($this->branch_off_address_id);
                $hadrs->setAttributes(['adrs_line1'=>$this->braAdrs->adrs_line1,
                    'adrs_line2'=>$this->braAdrs->adrs_line2,
                    'city'=>$this->braAdrs->city,
                    'state'=>$this->braAdrs->state,
                    'country'=>$this->braAdrs->country,
                    'postal_code'=>$this->braAdrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->btel->primary_landline,
                    'primary_mobile'=>$this->btel->primary_mobile,
                    'secondary_landline'=>$this->btel->secondary_landline,
                    'secondary_mobile'=>$this->btel->secondary_mobile,
                ]);

                $this->btel=$htel;
                if (!$this->btel->save()) {
                    foreach($this->btel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->btel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->braAdrs=$hadrs;

                /*end: address*/

            }

            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{


            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

    public function saverincharge($person){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {


                if($person->isNewRecord){

                    //addresss
                    if(!empty($person->atel->primary_landline)||!empty($this->atel->primary_mobile)) {
                        if (!$person->atel->save()) {
                            foreach($person->atel->getErrors() as $msg)
                                $errmsg.=join("<br/>",$msg);
                        }else{
                            $person->adrs->telephone_id=$person->atel->id;
                        }
                    }
                    if(!$person->adrs->save()){
                        foreach($person->adrs->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                    else {
                        $person->address = $person->adrs->id;
                    }

                    // office addresss
                    if(!empty($person->otel->primary_landline)||!empty($this->otel->primary_mobile)) {
                        if (!$person->otel->save()) {
                            foreach($person->otel->getErrors() as $msg)
                                $errmsg.=join("<br/>",$msg);
                        }else{
                            $person->oadrs->telephone_id=$person->otel->id;
                        }
                    }
                    if(!$person->oadrs->save()){
                        foreach($person->oadrs->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                    else {
                        $person->off_address = $person->oadrs->id;
                    }


                }
                else{

                /* begin: address*/
                $hadrs=Address::findOne($person->address);
                $hadrs->setAttributes(['adrs_line1'=>$person->adrs->adrs_line1,
                    'adrs_line2'=>$person->adrs->adrs_line2,
                    'city'=>$person->adrs->city,
                    'state'=>$person->adrs->state,
                    'country'=>$person->adrs->country,
                    'postal_code'=>$person->adrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$person->atel->primary_landline,
                    'primary_mobile'=>$person->atel->primary_mobile,
                    'secondary_landline'=>$person->atel->secondary_landline,
                    'secondary_mobile'=>$person->atel->secondary_mobile,
                ]);

                $person->atel=$htel;
                if (!$person->atel->save()) {
                    foreach($person->atel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$person->atel->id;
                }



                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $person->adrs=$hadrs;



                /*end: address*/

                    /* begin: office address*/
                    $hadrs=Address::findOne($person->off_address);
                    $hadrs->setAttributes(['adrs_line1'=>$person->oadrs->adrs_line1,
                        'adrs_line2'=>$person->oadrs->adrs_line2,
                        'city'=>$person->oadrs->city,
                        'state'=>$person->oadrs->state,
                        'country'=>$person->oadrs->country,
                        'postal_code'=>$person->oadrs->postal_code,
                    ]);

                    $htel=$hadrs->getTelephone()->one();
                    if($htel==null){
                        $htel=new Telephone();
                    }
                    $htel->setAttributes([
                        'primary_landline'=>$person->otel->primary_landline,
                        'primary_mobile'=>$person->otel->primary_mobile,
                        'secondary_landline'=>$person->otel->secondary_landline,
                        'secondary_mobile'=>$person->otel->secondary_mobile,
                    ]);

                    $person->otel=$htel;
                    if (!$person->otel->save()) {
                        foreach($person->otel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $hadrs->telephone_id=$person->otel->id;
                    }


                    if(!$hadrs->save()){
                        foreach($hadrs->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else
                        $person->oadrs=$hadrs;

                    /*end: address*/

                }



            if(!$person->save()){
                foreach($person->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{

                $this->authorized_person_id=$person->id;
                if(!$this->save()){
                    foreach($this->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }

            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $person->addError("errors",$errmsg);
            return false;
        }
        return true;



    }



    public  function getfhadrs(){

        $adrs=$this->getHeadOffAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"");
    }


    public  function getfradrs(){

        $adrs=$this->getRegOffAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"");
    }

    public  function getfbadrs(){

        $adrs=$this->getBranchOffAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"");
    }





}
