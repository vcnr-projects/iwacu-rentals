<?php

namespace app\modules\member\models;

use Yii;

/**
 * This is the model class for table "tenant_type".
 *
 * @property string $name
 * @property integer $is_active
 */
class TenantType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Tenant  Type',
            'is_active' => 'Is Active?',
        ];
    }
}
