<?php

namespace app\modules\member\models;

use app\models\PersonTitle;
use Yii;
use \app\models\Address;
use \app\models\Telephone;


/**
 * This is the model class for table "person".
 *
 * @property string $id
 * @property string $title
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $relation
 * @property string $relation_person
 * @property string $dob
 * @property string $marital_status
 * @property string $spouse
 * @property string $wed_ann
 * @property string $address
 * @property string $off_address
 * @property string $company
 * @property string $designation
 *
 * @property Company[] $companies
 * @property Address $offAddress
 * @property PersonTitle $title0
 * @property Address $address0
 */
class Person extends \yii\db\ActiveRecord
{
    public $adrs;
    public $oadrs;
    public $atel;
    public $otel;

    public function __construct(){
        $this->adrs = new Address();
        $this->oadrs = new Address();

        $this->atel = new Telephone();
        $this->otel = new Telephone();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'fname', 'lname', 'email', 'dob', 'marital_status', 'address','gender'], 'required'],
            [['title', 'address', 'off_address'], 'integer'],
            [['dob', 'wed_ann'], 'safe'],
            [['fname', 'lname', 'relation', 'designation'], 'string', 'max' => 100],
            [['email'], 'string', 'max' => 255],
            [['relation_person', 'spouse', 'company'], 'string', 'max' => 200],
            [['marital_status'], 'string', 'max' => 20],
            [['profile_image'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email' => 'Email ID',
            'relation' => 'Relation',
            'relation_person' => 'Relation Person',
            'dob' => 'Date of Birth',
            'marital_status' => 'Marital Status',
            'spouse' => 'Spouse Name',
            'wed_ann' => 'Wedding Anniversary',
            'address' => 'Address',
            'off_address' => 'Office Address',
            'company' => 'Company Name',
            'designation' => 'Designation',
            'gender' => 'Gender',
            'profile_image' => 'Contact Person Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['authorized_person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'off_address']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle0()
    {
        return $this->hasOne(PersonTitle::className(), ['id' => 'title']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress0()
    {
        return $this->hasOne(Address::className(), ['id' => 'address']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(strtotime($this->dob)!==strtotime('01-01-1970')){
                $this->dob=date('Y-m-d',strtotime($this->dob));
            }else{
                $this->dob=null;
            }

            if(strtotime($this->wed_ann)!==strtotime('01-01-1970')){
                $this->wed_ann=date('Y-m-d',strtotime($this->wed_ann));
            }else{
                $this->wed_ann=null;
            }





            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(strtotime($this->wed_ann)==strtotime('01-01-1970')){
            $this->wed_ann=null;

        }else{
            $this->wed_ann=date('d-m-Y',strtotime($this->wed_ann));
        }

        if(strtotime($this->dob)==strtotime('01-01-1970')){
            $this->dob=null;

        }else{
            $this->dob=date('d-m-Y',strtotime($this->dob));
        }



        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }


    public  function getfadrs(){

        $adrs=$this->getAddress0()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"");
    }


    public  function getfoadrs(){

        $adrs=$this->getOffAddress()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }


}
