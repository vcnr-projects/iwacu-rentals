<?php

namespace app\modules\member\models;

use app\models\Address;
use app\models\PersonTitle;
use app\models\Telephone;
use dektrium\user\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "tenant".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $tenant_type
 * @property string $tenant_family_type
 * @property integer $no_people
 * @property string $pan_no
 * @property string $food_habits
 * @property string $residential_status
 * @property integer $has_pets
 * @property string $permanent_address_id
 * @property string $present_address_id
 * @property string $office_address_id
 * @property string $reference_person_id
 * @property string $profile_pic_path
 * @property string $pan_file_path
 * @property string $id_proof_path
 * @property string $address_proof_path
 * @property string $title_id
 * @property string $fname
 * @property string $lname
 * @property string $email_id
 * @property integer $marital_status
 * @property string $spouse
 * @property string $wed_ann
 * @property string $relation
 * @property string $relative
 * @property string $dob
 *
 * @property Address $permanentAddress
 * @property Address $presentAddress
 * @property Address $officeAddress
 * @property Person $referencePerson
 */
class Tenant extends \yii\db\ActiveRecord
{

    public $peradrs;
    public $preadrs;
    public $offadrs;
    public $pertel;
    public $pretel;
    public $offtel;
    public $user;
    public $aperson;


    public function __construct(){
        $this->peradrs = new Address();
        $this->preadrs = new Address();
        $this->offadrs = new Address();
        $this->pertel = new Telephone();
        $this->pretel = new Telephone();
        $this->offtel = new Telephone();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_id','member_id', 'tenant_family_type', 'no_people', 'pan_no', 'food_habits', 'residential_status', 'has_pets', 'present_address_id', 'fname', 'lname', 'marital_status'], 'required'],
            [['member_id', 'no_people', 'has_pets', 'permanent_address_id', 'present_address_id', 'office_address_id', 'reference_person_id', 'title_id'], 'integer'],
            [['wed_ann', 'dob'], 'safe'],
            [['tenant_type', 'food_habits', 'relation'], 'string', 'max' => 50],
            [['tenant_family_type', 'fname', 'lname', 'relative'], 'string', 'max' => 100],
            [['pan_no'], 'string', 'max' => 20],
            [['id_proof_doctype'], 'string', 'max' => 20],
            [['residential_status', 'spouse'], 'string', 'max' => 200],
            [[ 'email_id','company','designation'], 'string', 'max' => 255],
            [['profile_pic_path', 'pan_file_path', 'id_proof_path', 'address_proof_path'], 'file'],
            //[['marital_status'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'tenant_type' => 'Tenant Type',
            'tenant_family_type' => 'Tenant Family Type',
            'no_people' => 'No. of People',
            'pan_no' => 'PAN No.',
            'food_habits' => 'Food Habits',
            'residential_status' => 'Residential Status',
            'has_pets' => 'Has Pets?',
            'permanent_address_id' => 'Permanent Address',
            'present_address_id' => 'Present Address',
            'office_address_id' => 'Office Address',
            'reference_person_id' => 'Reference Person ID',
            'profile_pic_path' => 'Profile Photo',
            'pan_file_path' => 'PAN Card',
            'id_proof_path' => 'Present Address  proof',
            'address_proof_path' => 'Address Proof ',
            'title_id' => 'Title ID',
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email_id' => 'Email ID',
            'marital_status' => 'Marital Status',
            'spouse' => 'Spouse',
            'wed_ann' => 'Wedding Anniversary',
            'relation' => 'Relation',
            'relative' => 'Relative',
            'dob' => 'Date of Birth',
            'company' => 'Company',
            'designation' => 'Designation',
            'id_proof_doctype' => 'ID Proof Document Type',
            'photo_id_proof' => 'Photo ID proof',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermanentAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'permanent_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'present_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'office_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferencePerson()
    {
        return $this->hasOne(TenantReference::className(), ['id' => 'reference_person_id']);
    }

    public function gettitle()
    {
        return $this->hasOne(PersonTitle::className(), ['id' => 'title_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->wed_ann)){
                $this->wed_ann=date('Y-m-d',strtotime($this->wed_ann));
            }else{
                $this->wed_ann=null;
            }

            if(strtotime($this->dob)!==strtotime('01-01-1970')){
                $this->dob=date('Y-m-d',strtotime($this->dob));
            }else{
                $this->dob=null;
            }


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(!strtotime($this->wed_ann)){
            $this->wed_ann=null;

        }else{
            $this->wed_ann=date('d-m-Y',strtotime($this->wed_ann));
        }
        if(strtotime($this->dob)==strtotime('01-01-1970')){
            $this->dob=null;

        }else{
            $this->dob=date('d-m-Y',strtotime($this->dob));
        }



        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }


    public  function getfperadrs(){

        $adrs=$this->getPermanentAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"")."

          Id Proof Document Type:
          {$adrs->proof_doc_type}
         ";
    }

    public  function getfpreadrs(){

        $adrs=$this->getPresentAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"")."

          Id Proof Document Type:
          {$adrs->proof_doc_type}
         ";
    }

    public  function getfoffadrs(){

        $adrs=$this->getOfficeAddress()->one();
        if($adrs) {

            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }


    public function saver(){

        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {

            if($this->isNewRecord){
                $this->user = \Yii::createObject([
                    'class'    => \common\models\User::className(),
                    'scenario' => 'register'
                ]);

                $this->user->setAttributes([
                    'email'    => $this->email_id,
                    'username' => "VHP",
                    'password' => '1234567890',
                    'user_type' => 'Tenant',
                ]);

                if(!$this->user->register()){
                    Yii::trace(VarDumper::dumpAsString($this->user->getErrors()),'vardump');
                    foreach($this->user->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else
                    $this->member_id=$this->user->id;

            }


            //begin:permanent adrs
            Yii::trace(VarDumper::dumpAsString($this->peradrs),'vardump');
            $hadrs=Address::findOne($this->permanent_address_id);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$this->peradrs->adrs_line1,
                'adrs_line2'=>$this->peradrs->adrs_line2,
                'city'=>$this->peradrs->city,
                'state'=>$this->peradrs->state,
                'country'=>$this->peradrs->country,
                'postal_code'=>$this->peradrs->postal_code,
                'proof_doc_type'=>$this->peradrs->proof_doc_type,
            ]);

            $htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$this->pertel->primary_landline,
                'primary_mobile'=>$this->pertel->primary_mobile,
                'secondary_landline'=>$this->pertel->secondary_landline,
                'secondary_mobile'=>$this->pertel->secondary_mobile,
            ]);

            $this->pertel=$htel;
            if (!$this->pertel->save()) {
                foreach($this->pertel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                $hadrs->telephone_id=$this->pertel->id;
            }


            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $this->peradrs = $hadrs;
                $this->permanent_address_id = $this->peradrs->id;
            }

            //end:permanent adrs


            //begin:present adrs
            Yii::trace(VarDumper::dumpAsString($this->preadrs),'vardump');
            $hadrs=Address::findOne($this->present_address_id);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$this->preadrs->adrs_line1,
                'adrs_line2'=>$this->preadrs->adrs_line2,
                'city'=>$this->preadrs->city,
                'state'=>$this->preadrs->state,
                'country'=>$this->preadrs->country,
                'postal_code'=>$this->preadrs->postal_code,
                'proof_doc_type'=>$this->preadrs->proof_doc_type,

            ]);

            $htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$this->pretel->primary_landline,
                'primary_mobile'=>$this->pretel->primary_mobile,
                'secondary_landline'=>$this->pretel->secondary_landline,
                'secondary_mobile'=>$this->pretel->secondary_mobile,
            ]);

            $this->pretel=$htel;
            if (!$this->pretel->save()) {
                foreach($this->pretel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                $hadrs->telephone_id=$this->pretel->id;
            }


            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $this->preadrs = $hadrs;
                $this->present_address_id = $this->preadrs->id;
            }
            Yii::trace(VarDumper::dumpAsString($this->present_address_id ),'vardump');

            //end:present adrs

            //begin: office adrs
            if($this->offadrs->adrs_line1){
                $hadrs=Address::findOne($this->office_address_id);
                if(!$hadrs){
                    $hadrs=new Address();
                }
                $hadrs->setAttributes(['adrs_line1'=>$this->offadrs->adrs_line1,
                    'adrs_line2'=>$this->offadrs->adrs_line2,
                    'city'=>$this->offadrs->city,
                    'state'=>$this->offadrs->state,
                    'country'=>$this->offadrs->country,
                    'postal_code'=>$this->offadrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->offtel->primary_landline,
                    'primary_mobile'=>$this->offtel->primary_mobile,
                    'secondary_landline'=>$this->offtel->secondary_landline,
                    'secondary_mobile'=>$this->offtel->secondary_mobile,
                ]);

                $this->offtel=$htel;
                if (!$this->offtel->save()) {
                    foreach($this->offtel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->offtel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else {
                    $this->offadrs = $hadrs;
                    $this->office_address_id = $this->offadrs->id;
                }

            }else{
                Address::deleteAll(["id"=>$this->office_address_id]);
                $this->office_address_id=null;
            }
            //end: office adrs

            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{


            }

            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;
    }


    public function saverreference($person){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            //begin: adrs
            Yii::trace(VarDumper::dumpAsString($person->adrs),'vardump');
            $hadrs=Address::findOne($person->address_id);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$person->adrs->adrs_line1,
                'adrs_line2'=>$person->adrs->adrs_line2,
                'city'=>$person->adrs->city,
                'state'=>$person->adrs->state,
                'country'=>$person->adrs->country,
                'postal_code'=>$person->adrs->postal_code,
            ]);

            $htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$person->atel->primary_landline,
                'primary_mobile'=>$person->atel->primary_mobile,
                'secondary_landline'=>$person->atel->secondary_landline,
                'secondary_mobile'=>$person->atel->secondary_mobile,
            ]);

            $person->atel=$htel;
            if (!$person->atel->save()) {
                foreach($person->atel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                $hadrs->telephone_id=$person->atel->id;
            }


            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $person->adrs = $hadrs;
                $person->address_id = $person->adrs->id;
            }
            //end: adrs

            //begin: office adrs
            Yii::trace(VarDumper::dumpAsString($person->offadrs), 'vardump');

            if($person->offadrs->adrs_line1) {
                Yii::trace(VarDumper::dumpAsString($person->offadrs), 'vardump');
                $hadrs = Address::findOne($person->office_address_id);
                if (!$hadrs) {
                    $hadrs = new Address();
                }
                $hadrs->setAttributes(['adrs_line1' => $person->offadrs->adrs_line1,
                    'adrs_line2' => $person->offadrs->adrs_line2,
                    'city' => $person->offadrs->city,
                    'state' => $person->offadrs->state,
                    'country' => $person->offadrs->country,
                    'postal_code' => $person->offadrs->postal_code,
                ]);

                $htel = $hadrs->getTelephone()->one();
                if ($htel == null) {
                    $htel = new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline' => $person->otel->primary_landline,
                    'primary_mobile' => $person->otel->primary_mobile,
                    'secondary_landline' => $person->otel->secondary_landline,
                    'secondary_mobile' => $person->otel->secondary_mobile,
                ]);

                $person->otel = $htel;
                if (!$person->otel->save()) {
                    foreach ($person->otel->getErrors() as $msg)
                        $errmsg .= join("<br/>", $msg);
                } else {
                    $hadrs->telephone_id = $person->otel->id;
                }


                if (!$hadrs->save()) {
                    foreach ($hadrs->getErrors() as $msg)
                        $errmsg .= join("<br/>", $msg);
                } else {
                    $person->offadrs = $hadrs;
                    $person->office_address_id = $person->offadrs->id;
                }
            }else{
                $person->office_address_id =null;
            }
            //end: office adrs


            $person->tenant_id=$this->id;
            if(!$person->save()){
                foreach($person->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{

                $this->reference_person_id=$person->id;
                if(!$this->save()){
                    foreach($this->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }

            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $person->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

}
