<?php

namespace app\modules\member\models;

use app\models\PersonTitle;
use app\modules\realty\models\Property;
use dektrium\user\models\User;
use Yii;
use app\models\Address;
use app\models\Telephone;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "individual".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $local_person
 * @property string $residential_status
 * @property string $permanent_address_id
 * @property string $present_address_id
 * @property string $company_name
 * @property string $designation
 * @property string $office_address_id
 * @property string $profile_image
 * @property string $title
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $dob
 * @property string $marital_status
 * @property string $spouse
 * @property string $wed_ann
 * @property string $gender
 *
 * @property Person $title0
 * @property Person $localPerson
 * @property Address $permanentAddress
 * @property Address $presentAddress
 * @property Address $officeAddress
 */
class Individual extends \yii\db\ActiveRecord
{
    public $peradrs;
    public $preadrs;
    public $offadrs;
    public $pertel;
    public $pretel;
    public $offtel;
    public $user;
    public $aperson;

    public $properties;

    public function __construct(){
        $this->peradrs = new Address();
        $this->preadrs = new Address();
        $this->offadrs = new Address();
        $this->pertel = new Telephone();
        $this->pretel = new Telephone();
        $this->offtel = new Telephone();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'individual';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'permanent_address_id', 'present_address_id', 'title', 'fname', 'lname', 'email', 'dob', 'marital_status','gender'], 'required'],
            [['member_id', 'local_person', 'permanent_address_id', 'present_address_id', 'office_address_id', 'title'], 'integer'],
            [['dob', 'wed_ann'], 'safe'],
            [['residential_status','pan'], 'string', 'max' => 50],
            [['company_name', 'fname', 'lname', 'spouse'], 'string', 'max' => 200],
            [['designation'], 'string', 'max' => 100],
            [[ 'email','relation','relative','id_proof_doctype'], 'string', 'max' => 255],
            [['marital_status'], 'string', 'max' => 10],
            [['email'], 'email'],
            [['profile_image','pan_filepath','id_proofpath','present_address_proof','permanent_address_proof'], 'file'],
        ];
    }


    /*public function scenarios()
    {
        $scenarios = parent::scenarios();
        if(!isset($scenarios[$this->scenario])){
            $scenarios[$this->scenario]=array_keys($this->attributeLabels());
        }
        else{
            $scenarios['create'] =array_keys($this->attributeLabels());//Scenario Values Only Accepted

        }
        return $scenarios;
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'local_person' => 'Local Person',
            'residential_status' => 'Residential Status',
            'permanent_address_id' => 'Permanent Address',
            'present_address_id' => 'Present Address',
            'company_name' => 'Company Name',
            'designation' => 'Designation',
            'office_address_id' => 'Office Address',
            'profile_image' => 'Upload Profile Image',
            'title' => 'Title',
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email' => 'Email',
            'dob' => 'Date of Birth',
            'marital_status' => 'Marital Status',
            'spouse' => 'Spouse Name',
            'wed_ann' => 'Wedding Anniversary',
            'gender' => 'Gender',
            'relation' => 'Relationship',
            'relative' => 'Relative',
            'pan' => 'PAN No.',
            'pan_filepath' => 'Upload PAN Card',
            'id_proofpath' => 'Upload Photo ID Proof ',
            'id_proof_doctype'=>'ID Proof Document Type',
            'present_address_proof'=>'Present Address Proof',
            'permanent_address_proof'=>'Permanent Address Proof'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle0()
    {
        return $this->hasOne(PersonTitle::className(), ['id' => 'title']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'local_person']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermanentAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'permanent_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'present_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'office_address_id']);
    }

    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['member_id' => 'member_id']);
    }

    public function getUserrow()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'member_id']);
    }


    public function saver(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if($this->isNewRecord){
                /* $user=new User();
                 $user->username=$this->fname." ".$this->lname.date('d');
                 $user->email=$this->email;*/
                $this->user = \Yii::createObject([
                    'class'    => \common\models\User::className(),
                    'scenario' => 'register'
                ]);

                $this->user->setAttributes([
                    'email'    => $this->email,
                    'username' => "VHP",
                    'password' => '1234567890',
                    'user_type' => 'Individual',
                ]);

                if(!$this->user->register()){
                    Yii::trace(VarDumper::dumpAsString($this->user->getErrors()),'vardump');
                    foreach($this->user->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else
                    $this->member_id=$this->user->id;


               // if(!empty($this->pertel->primary_landline)||!empty($this->pertel->primary_mobile)) {
                    if (!$this->pertel->save()) {
                        foreach($this->pertel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->peradrs->telephone_id=$this->pertel->id;
                    }
                //}
                if(!$this->peradrs->save()){
                    foreach($this->peradrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->permanent_address_id = $this->peradrs->id;
                }



                //present
                Yii::trace(VarDumper::dumpAsString($this->pretel),'vardump');
                //if(!empty($this->pretel->primary_landline)||!empty($this->pretel->primary_mobile)) {
                    if (!$this->pretel->save()) {
                        foreach($this->pretel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->preadrs->telephone_id=$this->pretel->id;
                    }
                //}
                if(!$this->preadrs->save()){
                    foreach($this->preadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->present_address_id = $this->preadrs->id;
                }

                //office
                if($this->offadrs->adrs_line1) {
                    if (!empty($this->offtel->primary_landline) || !empty($this->offtel->primary_mobile)) {
                        if (!$this->offtel->save()) {
                            foreach ($this->offtel->getErrors() as $msg)
                                $errmsg .= join("<br/>", $msg);
                        } else {
                            $this->offadrs->telephone_id = $this->offtel->id;
                        }
                    }
                    if (!$this->offadrs->save()) {
                        foreach ($this->offadrs->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $this->office_address_id = $this->offadrs->id;
                    }
                }




            }else{
                // update address

                /* begin: address*/
                $hadrs=Address::findOne($this->permanent_address_id);
                $hadrs->setAttributes(['adrs_line1'=>$this->peradrs->adrs_line1,
                    'adrs_line2'=>$this->peradrs->adrs_line2,
                    'city'=>$this->peradrs->city,
                    'state'=>$this->peradrs->state,
                    'country'=>$this->peradrs->country,
                    'postal_code'=>$this->peradrs->postal_code,
                    'proof_doc_type'=>$this->peradrs->proof_doc_type,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->pertel->primary_landline,
                    'primary_mobile'=>$this->pertel->primary_mobile,
                    'secondary_landline'=>$this->pertel->secondary_landline,
                    'secondary_mobile'=>$this->pertel->secondary_mobile,
                ]);

                $this->pertel=$htel;
                if (!$this->pertel->save()) {
                    foreach($this->pertel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->pertel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->peradrs=$hadrs;

                /*end: address*/

                /* begin: present address*/
                Yii::trace(VarDumper::dumpAsString($this->pretel),'vardump');

                $hadrs=Address::findOne($this->present_address_id);
                $hadrs->setAttributes(['adrs_line1'=>$this->preadrs->adrs_line1,
                    'adrs_line2'=>$this->preadrs->adrs_line2,
                    'city'=>$this->preadrs->city,
                    'state'=>$this->preadrs->state,
                    'country'=>$this->preadrs->country,
                    'postal_code'=>$this->preadrs->postal_code,
                    'proof_doc_type'=>$this->preadrs->proof_doc_type,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$this->pretel->primary_landline,
                    'primary_mobile'=>$this->pretel->primary_mobile,
                    'secondary_landline'=>$this->pretel->secondary_landline,
                    'secondary_mobile'=>$this->pretel->secondary_mobile,
                ]);

                $this->pretel=$htel;
                if (!$this->pretel->save()) {
                    foreach($this->pretel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$this->pretel->id;
                }


                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->preadrs=$hadrs;

                /*end: address*/


                /* begin: office address*/
                if($this->offadrs->adrs_line1) {
                    $hadrs = Address::findOne($this->office_address_id);
                    if(!$hadrs){
                        $hadrs= new Address();
                    }
                    $hadrs->setAttributes(['adrs_line1' => $this->offadrs->adrs_line1,
                        'adrs_line2' => $this->offadrs->adrs_line2,
                        'city' => $this->offadrs->city,
                        'state' => $this->offadrs->state,
                        'country' => $this->offadrs->country,
                        'postal_code' => $this->offadrs->postal_code,
                    ]);

                    $htel = $hadrs->getTelephone()->one();
                    if ($htel == null) {
                        $htel = new Telephone();
                    }
                    $htel->setAttributes([
                        'primary_landline' => $this->offtel->primary_landline,
                        'primary_mobile' => $this->offtel->primary_mobile,
                        'secondary_landline' => $this->offtel->secondary_landline,
                        'secondary_mobile' => $this->offtel->secondary_mobile,
                    ]);

                    $this->offtel = $htel;
                    if (!$this->offtel->save()) {
                        foreach ($this->offtel->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $hadrs->telephone_id = $this->offtel->id;
                    }


                    if (!$hadrs->save()) {
                        foreach ($hadrs->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $this->offadrs = $hadrs;
                        $this->office_address_id=$hadrs->id;
                    }
                }else{
                    $hadrs = Address::findOne($this->office_address_id);
                    if($hadrs)
                        $hadrs->delete();
                }

                /*end: address*/

            }

            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{


            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->wed_ann)!==strtotime('01-01-1970')){
                $this->wed_ann=date('Y-m-d',strtotime($this->wed_ann));
            }else{
                $this->wed_ann=null;
            }

            if(strtotime($this->dob)!==strtotime('01-01-1970')){
                $this->dob=date('Y-m-d',strtotime($this->dob));
            }else{
                $this->dob=null;
            }


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(strtotime($this->wed_ann)==strtotime('01-01-1970')){
            $this->wed_ann=null;

        }else{
            $this->wed_ann=date('d-m-Y',strtotime($this->wed_ann));
        }
        if(strtotime($this->dob)==strtotime('01-01-1970')){
            $this->dob=null;

        }else{
            $this->dob=date('d-m-Y',strtotime($this->dob));
        }



        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }


    public  function getfperadrs(){

        $adrs=$this->getPermanentAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"") ."

          Id Proof Document Type:
          {$adrs->proof_doc_type}
         " ;
    }

    public  function getfpreadrs(){

        $adrs=$this->getPresentAddress()->one();
        $tel=$adrs->getTelephone()->one();
        // var_dump($adrs->telephone_id);
        if(empty($adrs)){
            $adrs=new Address();
        }
        if(empty($tel)){
            $tel=new Telephone();
        }

        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        ":"") ."

          Id Proof Document Type:
          {$adrs->proof_doc_type}
         " ;
    }

    public  function getfoffadrs(){

        $adrs=$this->getOfficeAddress()->one();
        if($adrs) {

            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }

    public function saverincharge($person){
        //Yii::trace(VarDumper::dumpAsString($person),'vardump');
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {


            if($person->isNewRecord){

                //addresss
                if(!empty($person->atel->primary_landline)||!empty($person->atel->primary_mobile)) {
                    if (!$person->atel->save()) {
                        foreach($person->atel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $person->adrs->telephone_id=$person->atel->id;
                    }
                }
                if(!$person->adrs->save()){
                    foreach($person->adrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $person->address = $person->adrs->id;
                }

                // office addresss
                if(!empty($person->otel->primary_landline)||!empty($person->otel->primary_mobile)) {
                    if (!$person->otel->save()) {
                        foreach($person->otel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $person->oadrs->telephone_id=$person->otel->id;
                    }
                }
                if($person->oadrs->adrs_line1)
                if(!$person->oadrs->save()){
                    foreach($person->oadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $person->off_address = $person->oadrs->id;
                }


            }
            else{

                /* begin: address*/
                $hadrs=Address::findOne($person->address);
                $hadrs->setAttributes(['adrs_line1'=>$person->adrs->adrs_line1,
                    'adrs_line2'=>$person->adrs->adrs_line2,
                    'city'=>$person->adrs->city,
                    'state'=>$person->adrs->state,
                    'country'=>$person->adrs->country,
                    'postal_code'=>$person->adrs->postal_code,
                ]);

                $htel=$hadrs->getTelephone()->one();
                if($htel==null){
                    $htel=new Telephone();
                }
                $htel->setAttributes([
                    'primary_landline'=>$person->atel->primary_landline,
                    'primary_mobile'=>$person->atel->primary_mobile,
                    'secondary_landline'=>$person->atel->secondary_landline,
                    'secondary_mobile'=>$person->atel->secondary_mobile,
                ]);

                $person->atel=$htel;
                if (!$person->atel->save()) {
                    foreach($person->atel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $hadrs->telephone_id=$person->atel->id;
                }



                if(!$hadrs->save()){
                    foreach($hadrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $person->adrs=$hadrs;



                /*end: address*/

                /* begin: office address*/
                $hadrs = Address::findOne($person->off_address);
                if(($hadrs)) {
                    $person->oadrs->id = $hadrs->id;
                    $person->oadrs->telephone_id = $hadrs->telephone_id;
                    Yii::trace(VarDumper::dumpAsString($person->oadrs),'vardump');
                    Yii::trace(VarDumper::dumpAsString($hadrs),'vardump');
                }
                if($person->oadrs->adrs_line1) {

                    if (!$hadrs) {
                        $hadrs = new Address();
                    }
                    $hadrs->setAttributes(['adrs_line1' => $person->oadrs->adrs_line1,
                        'adrs_line2' => $person->oadrs->adrs_line2,
                        'city' => $person->oadrs->city,
                        'state' => $person->oadrs->state,
                        'country' => $person->oadrs->country,
                        'postal_code' => $person->oadrs->postal_code,
                    ]);

                    $htel = $hadrs->getTelephone()->one();
                    if ($htel == null) {
                        $htel = new Telephone();
                    }
                    $htel->setAttributes([
                        'primary_landline' => $person->otel->primary_landline,
                        'primary_mobile' => $person->otel->primary_mobile,
                        'secondary_landline' => $person->otel->secondary_landline,
                        'secondary_mobile' => $person->otel->secondary_mobile,
                    ]);

                    $person->otel = $htel;
                    if (!$person->otel->save()) {
                        foreach ($person->otel->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $hadrs->telephone_id = $person->otel->id;
                    }


                    if (!$hadrs->save()) {
                        foreach ($hadrs->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $person->oadrs = $hadrs;
                        $person->off_address = $person->oadrs->id;
                    }
                }else{
                    $person->off_address=null;
                    $person->save();
                    Telephone::deleteAll(["id"=>$person->oadrs->telephone_id]);
                    Address::deleteAll(["id"=>$person->oadrs->id]);
                }

                /*end: address*/

            }



            if(!$person->save()){
                foreach($person->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{

                $this->local_person=$person->id;
                if(!$this->save()){
                    foreach($this->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }

            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $person->addError("errors",$errmsg);
            return false;
        }
        return true;



    }


}
