<?php

namespace app\modules\member\models;

use app\models\PersonTitle;
use Yii;
use app\models\Address;
use app\models\Telephone;

/**
 * This is the model class for table "tenant_reference".
 *
 * @property string $id
 * @property integer $title
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $designation
 * @property string $address_id
 * @property string $company
 * @property string $office_address_id
 * @property string $tenant_id
 *
 * @property Address $officeAddress
 * @property Address $address
 * @property Tenant $tenant
 */
class TenantReference extends \yii\db\ActiveRecord
{

    public $adrs;
    public $offadrs;
    public $atel;
    public $otel;

    public function __construct(){
        $this->adrs = new Address();
        $this->offadrs = new Address();
        $this->atel = new Telephone();
        $this->otel = new Telephone();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'fname', 'lname', 'tenant_id'], 'required'],
            [['title', 'address_id', 'office_address_id', 'tenant_id'], 'integer'],
            [['fname', 'lname', 'designation'], 'string', 'max' => 100],
            [['email', 'company'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email' => 'Email ID',
            'designation' => 'Designation',
            'address_id' => 'Address',
            'company' => 'Company',
            'office_address_id' => 'Office Address',
            'tenant_id' => 'Tenant',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'office_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['id' => 'tenant_id']);
    }
    public function getTitle0()
    {
        return $this->hasOne(PersonTitle::className(), ['id' => 'title']);
    }

    public  function getfadrs(){

        $adrs=$this->getAddress()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }

    public  function getfoadrs(){

        $adrs=$this->getOfficeAddress()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }
}
