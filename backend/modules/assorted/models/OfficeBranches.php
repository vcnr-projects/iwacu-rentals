<?php

namespace app\modules\assorted\models;

use Yii;

/**
 * This is the model class for table "office_branches".
 *
 * @property string $branch
 * @property string $city
 * @property string $adrs
 * @property string $contact_no
 * @property string $state
 */
class OfficeBranches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_branches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch', 'city'], 'required'],
            [['adrs'], 'string'],
            [['branch', 'city', 'state'], 'string', 'max' => 100],
            [['contact_no'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch' => 'Branch Name',
            'city' => 'Branch City',
            'adrs' => 'Address',
            'contact_no' => 'Contact No.',
            'state' => 'State',
        ];
    }
}
