<?php

namespace app\modules\assorted\models;

use Yii;

/**
 * This is the model class for table "designation".
 *
 * @property string $designation
 * @property integer $is_active
 */
class Designation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'designation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['designation'], 'required'],
            [['is_active'], 'integer'],
            [['designation'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'designation' => 'Designation',
            'is_active' => 'Is Active?',
        ];
    }
}
