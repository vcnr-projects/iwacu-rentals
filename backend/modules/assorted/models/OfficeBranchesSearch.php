<?php

namespace app\modules\assorted\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\assorted\models\OfficeBranches;

/**
 * OfficeBranchesSearch represents the model behind the search form about `app\modules\assorted\models\OfficeBranches`.
 */
class OfficeBranchesSearch extends OfficeBranches
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch', 'city', 'adrs', 'contact_no', 'state'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OfficeBranches::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'adrs', $this->adrs])
            ->andFilterWhere(['like', 'contact_no', $this->contact_no])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
