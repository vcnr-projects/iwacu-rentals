<?php

namespace app\modules\assorted;

class AssortedModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\assorted\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
