<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\assorted\models\DesignationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Designations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="designation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Designation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'designation',
            [
                'attribute'=>'is_active',
                'label'=>"Status",
                'filter'=>Html::dropDownList((new ReflectionClass($searchModel))->getShortName()."[is_active]",$searchModel->is_active,["1"=>"Active","0"=>"In-Active"],["prompt"=>"All","class"=>"form-control"]),
                'value'=>function($data){
                    return ($data->is_active)?"Active":"In-Active";
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
