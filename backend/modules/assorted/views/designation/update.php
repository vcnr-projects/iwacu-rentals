<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\assorted\models\Designation */

$this->title = 'Update Designation: ' . ' ' . $model->designation;
$this->params['breadcrumbs'][] = ['label' => 'Designations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->designation, 'url' => ['view', 'id' => $model->designation]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="designation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
