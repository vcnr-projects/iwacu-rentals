<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\assorted\models\OfficeBranches */

$this->title = 'Create Office Branches';
$this->params['breadcrumbs'][] = ['label' => 'Office Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-branches-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
