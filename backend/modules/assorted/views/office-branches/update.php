<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\assorted\models\OfficeBranches */

$this->title = 'Update Office Branches: ' . ' ' . $model->branch;
$this->params['breadcrumbs'][] = ['label' => 'Office Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->branch, 'url' => ['view', 'id' => $model->branch]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="office-branches-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
