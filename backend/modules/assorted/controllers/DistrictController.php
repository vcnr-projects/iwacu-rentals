<?php

namespace app\modules\assorted\controllers;

use Yii;
use app\modules\assorted\models\District;
use app\modules\assorted\models\DistrictSearch;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DistrictController implements the CRUD actions for District model.
 */
class DistrictController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'
                    ,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'jsonautocomplete'
                            ,'listofcities'
                            ,'jsondet'
                        ],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'
                            ,'jsonautocomplete'
                            ,'listofcities'
                            ,'jsondet'
                        ],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all District models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DistrictSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single District model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new District model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new District();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing District model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing District model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the District model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return District the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = District::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionJsonautocomplete($q = null,$state=null,$country=null)
    {
        header('Content-Type: application/json');



        $sql="select d.name from district d,state s, country c
          where s.country=c.name and d.state=s.id and c.name like :country and s.name like :state
          and d.name like :district ";
        $command = Yii::$app->db->createCommand($sql);
        $command->bindValue(":country","%$country%",true);
        $command->bindValue(":state","%$state%",true);
        $command->bindValue(":district","%$q%",true);
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['name']];
        }
        echo Json::encode($out);
        //return  json_encode (Country::find()->select("name as value")->asArray()->all());
    }

    public function actionListofcities($q=null){
        $sql="select d.name ,c.name as country,s.name as state from district d,country c,state s
          where s.country=c.name and d.state=s.id
          and d.name like :district ";
        $command = Yii::$app->db->createCommand($sql);

        $command->bindValue(":district","%$q%",true);
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['name'],'country'=>$d['country'],'state'=>$d['state']];
        }
        echo Json::encode($out);
    }

    public function actionJsondet($q=null){
        $sql="select d.name ,c.name as country,s.name as state from district d,country c,state s
          where s.country=c.name and d.state=s.id
          and d.name like :district ";
        $command = Yii::$app->db->createCommand($sql);

        $command->bindValue(":district","%$q%",true);
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out = ['value' => $d['name'],'country'=>$d['country'],'state'=>$d['state']];
        }
        echo Json::encode($out);
    }

}
