<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Builder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="builder-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'about_developer')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'logo',['template'=>'{label} '.$model->logo.'{input}{error}'])->fileInput() ?>
    <?= $form->field($model, 'banner_img',['template'=>'{label} '.$model->logo.'{input}{error}'])->fileInput() ?>
    <?php
    $ha=$model->getAddress0()->one();
    if(($ha)){
        $model->adrs->setAttributes($ha->getAttributes());
        $model->adrs->id=$ha->id;
    }
    $hatel=$model->adrs->getTelephone()->one();
    if(empty($hatel)){
        $hatel=new \app\models\Telephone();
        $model->tel=$hatel;
    }else {
        $model->tel = new \app\models\Telephone();
        $model->tel->setAttributes($hatel->getAttributes());
        $model->tel->id=$hatel->id;
    }
    ?>

    <div class="panel-heading">  Address Details </div>
    <div class="panel-body">
        <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
        <div style="display: none">
            <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
        </div>

        <?= $form->field($model->adrs, 'city')->
        widget(\kartik\typeahead\Typeahead::className(),[
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                        'remote' => [
                            'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                            //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                            'wildcard' => '%QUERY',
                            /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                //console.console.log(query);
                                                console.console.log(settings);

                                                settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                return settings;

                                            }"),*/
                            'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                            'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                            'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                        ],

                    ]
                ],
                'pluginOptions' => ['highlight' => true],
                'pluginEvents'=>[

                    "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#address-country').val(cd.country);
                $('#address-state').val(cd.state);
            }
        }
     }",
                    "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                ],
                'options' => ["id"=>'preaddress-city'],
            ]) ?>
        <?= $form->field($model->adrs, 'postal_code')->textInput() ?>

        <?= $form->field($model->adrs, 'state')->textInput();
        ?>

        <?= $form->field($model->adrs, 'country')->textInput()
        ?>



        <?= $form->field($model->tel,'primary_landline')->textInput()->label('Land Line') ?>
        <?//= $form->field($model->tel,'secondary_landline')->textInput() ?>
        <?= $form->field($model->tel,'primary_mobile')->textInput() ?>
        <? //= $form->field($model->tel,'secondary_mobile')->textInput() ?>



        <?= $form->field($model, 'associate')->widget(\kartik\widgets\SwitchInput::className(),[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]) ?>


    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
