<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Builder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Builders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="builder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="row">
    <div class="col-md-9 ">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                //'name',
                'email:email',
                'url:url',
                'about_developer:ntext',
                [
                    'attribute'=>'fadrs',
                    'format'=>'Ntext',
                    'label'=>" Address",
                ],
                [
                    'attribute'=>'associate',
                    'value'=>($model->associate)?"Yes":"NO",
                ],

            ],
        ]) ?>

    </div>
    <div class="col-md-3">
        <?= Html::img(Yii::$app->urlManager->baseUrl."/uploads/".$model->logo); ?>
    </div>
    <div class="col-md-3">
        <?= Html::img(Yii::$app->urlManager->baseUrl."/uploads/".$model->banner_img); ?>
    </div>
</div>


</div>
