<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\BuilderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Builders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="builder-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Builder', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    Html::dropDownList('',$searchModel->xsearch
        ,["All"=>"All","Associate"=>"Associate","Non-Associate"=>"Non-Associate"]
        ,["onchange"=>"gridupdate(this.value)","class"=>"form-control"])
    ?>
    <script>

        function gridupdate(val){
//alert("Bill");
            // jQuery('#w1').yiiGridView('applyFilter',{"filterSelector":"#w1-filters input, #w1-filters select"});
            //$.pjax.reload({container:'#w1'});
            $("#BuilderSearch-xsearch").val(val);
            $.pjax.reload({
                type: 'GET',
                container: "#w1",
                timeout: 2000,
                data: $('#w1-filters  input').serialize()
            });
        }

    </script>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'name',
            'email:email',
            [
                'class'=>'app\components\HiddenDataColumn',
                'attribute'=>'url',
                'label'=>'URl',
                'hiddendata'=>['xsearch'],
                'filter'=>false,
            ],
            //'about_developer:ntext',
            // 'logo',
            // 'address',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
