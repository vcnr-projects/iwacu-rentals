<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Inventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cat')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\InventoryCategory::find()->andFilterWhere(["is_active"=>1])->asArray()->all(),'cat','cat')
        ,["prompt"=>"Select","onchange"=>"changecat(this.value)"]) ?>

    <script>
        function changecat(cat){
            $.getJSON("<?= Yii::$app->urlManager->createUrl("/realty/inventory/getcatbrands") ?>",{cat:cat},function(data){
                    console.log(data);
                $("#inventorybrands-brand").html('');
                for(var i=0;i<data.length;i++){
                    var lab=$("<label></label>");
                    var inp=$("<input type=checkbox name='InventoryBrands[brand][]'  />");
                    $(inp).attr("value",data[i]);
                    $(lab).append(inp);
                    $(lab).append(data[i]);
                    $("#inventorybrands-brand").append(lab);
                }

            })
        }
    </script>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php $list=\yii\helpers\ArrayHelper::map(\app\modules\realty\models\BrandNames::find()->andWhere(["category"=>$model->cat])->asArray()->all(),'brandname','brandname')  ?>

    <?php $sellist=[];
    foreach($model->getInventoryBrands()->all() as $i=> $par){
        $sellist[]=$par->brand;
    }
    $model->brandnames[0]->brand=$sellist;

    ?>
    <?= $form->field($model->brandnames[0], 'brand')->checkboxlist($list);?>


    <? //= $form->field($model,'brandnames')->checkboxList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\BrandNames::find(["category"=>$model->cat])->asArray()->all(),'cat','cat')) ?>

    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
