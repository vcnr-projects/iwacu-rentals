<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Properties */

$this->title = 'Create Project Specification';
$this->params['breadcrumbs'][] = ['label' => 'Project Specification', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
