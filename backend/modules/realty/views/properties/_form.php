<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false', ['position' => yii\web\View::POS_HEAD]);


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
<div class="col-md-6">

<div class="property-form ">

<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

<?= $form->errorSummary($model) ?>


<?= $form->field($model, 'propGroup')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\PropertyGroup::find()->andFilterWhere(["is_active"=>1])->asArray()->all(),'name','name')
    ,[
        'prompt'=>'Select',
        'onchange'=>'
             $( "select#properties-prop_type" ).html( "<option>Loading...</option>" );
                $.post( "'.Yii::$app->urlManager->createUrl('/realty/propertytype/ptypefrompgroup').'",{"depdrop_parents":$(this).val()}, function( data ) {
                  data=JSON.parse(data);
                  $( "select#properties-prop_type" ).html( "<option>Select</option>" );
                  for(var i=0;i<data.output.length;i++){
                  var opt=$("<option></option>");
                  $(opt).attr("value",data.output[i][\'id\']);
                  $(opt).text(data.output[i][\'name\']);
                    $( "select#properties-prop_type" ).append(opt);
                  }
                });
            '
    ]
) ?>

<?= $form->field($model, 'prop_type')->dropDownList(\yii\helpers\ArrayHelper::map( \app\modules\realty\models\PropertyType::find()->asArray()->all(),'id','name'),['prompt'=>'Select',
    'onchange'=>'
                //$("#property-name").val($("option:selected",this).html()+"-");
                $("#propnamelabel").html($("option:selected",this).html()+" Name");
            '
]); ?>
<?/*= $form->field($model, 'prop_type')->widget(\kartik\depdrop\DepDrop::classname(), [
        //'options' => ['id'=>'subcat-id'],
        'pluginOptions'=>[
            'depends'=>['property-propgroup'],
            'placeholder' => 'Select...',
            'url' => \yii\helpers\Url::to(['/realty/propertytype/ptypefrompgroup'])
        ]
    ]); */?>
<?php
$data=\app\modules\realty\models\Builder::find()->select('id')->column();
?>
<?= $form->field($model, 'name',['template'=>'<label class="control-label" id="propnamelabel" for="property-name">Project Name</label>{input}{error}'])->textInput() ?>

<?= $form->field($model, 'builder_name')->widget(\kartik\typeahead\Typeahead::classname(), [
    'options' => ['placeholder' => 'Bulider Name ...'],
    'pluginOptions' => ['highlight'=>true],
    'dataset' => [
        [
            'local' => $data,
            'limit' => 10
        ]
    ]
]); ?>

<? // = $form->field($model, 'no_units')->textInput() ?>
<?= $form->field($model, 'no_floors')->textInput() ?>

<?= $form->field($model, 'tot_units')->textInput() ?>
<?= $form->field($model, 'tot_proj_area')->textInput() ?>
<?= $form->field($model, 'area_range')->textInput() ?>
<?= $form->field($model, 'start_price')->textInput() ?>
<?= $form->field($model, 'loan')->widget(\kartik\switchinput\SwitchInput::classname(), [
    'pluginOptions'=>[
        'handleWidth'=>60,
        'onText'=>'Yes',
        'offText'=>'NO'
    ]]
); ?>

<?= $form->field($model, 'featured')->widget(\kartik\switchinput\SwitchInput::classname(), [
        'pluginOptions'=>[
            'handleWidth'=>60,
            'onText'=>'Yes',
            'offText'=>'NO'
        ]]
); ?>


<?= $form->field($model, 'year_of_construction')->textInput() ?>

<?= $form->field($model, 'approvals')->textarea() ?>
<?= $form->field($model, 'proj_start_date')->widget(\kartik\widgets\DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter  date ...'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd-mm-yyyy',
        'todayHighlight'=>true

    ]
]); ?>
<?= $form->field($model, 'poss_date')->widget(\kartik\widgets\DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter  date ...'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd-mm-yyyy',
        'todayHighlight'=>true

    ]
]); ?>

<?= $form->field($model, 'proj_status')->dropDownList([
    "Upcoming project"=>"Upcoming project",
    "Pre-launch project"=>"Pre-launch project",
    "Ongoing project"=>"Ongoing project",
    "Completed project"=>"Completed project",
],['prompt'=>'Select']) ?>

<?= $form->field($model, 'master_plan')->fileInput() ?>

<!--begin:  address-->

<?php
$ha=$model->getAddress()->one();
if(($ha)){
    $model->adrs->setAttributes($ha->getAttributes());
    $model->adrs->id=$ha->id;
}
$hatel=$model->adrs->getTelephone()->one();
if(empty($hatel)){
    $hatel=new \app\models\Telephone();
    $model->tel=$hatel;
}else {
    $model->tel = new \app\models\Telephone();
    $model->tel->setAttributes($hatel->getAttributes());
    $model->tel->id=$hatel->id;
}
?>
<div class=" panel panel-default ">
    <!--<div class="panel-heading"> In-charge person </div>
    <div class="panel-body">

        <?/*= $form->field($model, 'incharge_name')->textInput() */?>
        <?/*= $form->field($model, 'designation')->textInput() */?>
        <?/*= $form->field($model, 'site_contact_email')->textInput() */?>
        <?/*= $form->field($model->tel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) */?>
        <?/*= $form->field($model->tel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) */?>
        <?/*= $form->field($model->tel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) */?>
        <?/*= $form->field($model->tel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) */?>

    </div>-->
    <div class="panel-heading">  Address Details </div>
    <div class="panel-body">

        <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
        <div style="display: none">
            <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
        </div>
        <?= $form->field($model->adrs, 'country')->
        widget(\kartik\typeahead\Typeahead::className(),[
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                        'remote' => [
                            'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                            //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ]
                    ]
                ],
                'pluginOptions' => ['highlight' => true],
                'options' => ["id"=>'address-country'],
                //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
            ]) ?>

        <?= $form->field($model->adrs, 'state')->
        widget(\kartik\typeahead\Typeahead::className(),[
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                        'remote' => [
                            'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                            //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                            'wildcard' => '%QUERY',
                            'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#address-state').val())
                                                    settings.url=settings.url+'&country='+$('#address-country').val()
                                                    return settings;

                                                }"),
                        ]
                    ]
                ],
                'pluginOptions' => ['highlight' => true],
                'options' => ["id"=>'address-state'],
                //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
            ]) ?>

        <?= $form->field($model->adrs, 'city')->
        widget(\kartik\typeahead\Typeahead::className(),[
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                        'remote' => [
                            'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/jsonautocomplete']).'&q=%QUERY',
                            //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                            'wildcard' => '%QUERY',
                            'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.log(query);
                                                    console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#address-city').val())
                                                    settings.url=settings.url+'&state='+$('#address-state').val()+'&country='+$('#address-country').val()
                                                    return settings;

                                                }"),
                            /*'ajax' => ['complete' => new \yii\web\JsExpression("function(response){jQuery('#serial_product').removeClass('loading');checkresult(response.responseText);return true}")]*/

                        ]
                    ]
                ],
                'pluginOptions' => ['highlight' => true],
                'options' => ["id"=>'address-city'],
            ]) ?>



        <?= $form->field($model->adrs, 'postal_code')->textInput() ?>


    </div>
    <button type="button" class="btn btn-green" onclick="getadrs()">Get Lat and Long</button>
    <?= $form->field($model, 'lat')->textInput() ?>
    <?= $form->field($model, 'long')->textInput() ?>
    <script>
        function getadrs(){
            var address='';
            address+=$("#address-adrs_line1").val()+" ";
            address+=$("#address-city").val()+" ";
            address+=$("#address-state").val()+" ";
            address+=$("#address-country").val()+" ";
            //alert(address);
            findAddress(address);
        }
    </script>

</div>
<!--end:  address-->

<?php
$files= explode("^m",$model->image);
foreach($files as $file){
    if(!empty($file)){
        $baseurl=Yii::$app->urlManager->baseUrl;
        echo Html::a($file,$baseurl."/uploads/".$file);
        echo "<br/>";
    }
}
?>
<?= $form->field($model, 'image[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>



<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>

</div>
<style>
    #gmap_canvas {top:800px; height: 500px }
</style>
<div class="col-md-6 ">
    <div id="gmap_canvas">
    </div>
    <script>
        var geocoder;
        var map;
        var markers = Array();
        var infos = Array();
        function initialize() {

            // prepare Geocoder
            geocoder = new google.maps.Geocoder();

            // set initial position (New York)
            var myLatlng = new google.maps.LatLng(13.0300,77.5140);

            var myOptions = { // default map options
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

            google.maps.event.addListener(map, 'click', function(event) {
                //call function to create marker
                if (markers) {
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setMap(null);
                        markers.pop();
                    }
                }

                markers.push( createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng));
            });


        }
        window.onLoad=initialize();
        // find address function
        function findAddress(address) {
            //var address = document.getElementById("gmap_where").value;

            // script uses our 'geocoder' in order to find location by address name
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) { // and, if everything is ok

                    // we will center map
                    var addrLocation = results[0].geometry.location;
                    map.setCenter(addrLocation);

                    // store current coordinates into hidden variables
                    console.log(results[0]);
                    //console.log(results[0].geometry.location.lng());
                    var lat=results[0].geometry.location.lat();
                    var lng=results[0].geometry.location.lng();
                    fillLatLontoinp(lat,lng);
                    //document.getElementById('lat').value = results[0].geometry.location.$a;
                    //document.getElementById('lng').value = results[0].geometry.location.ab;

                    // and then - add new custom marker
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat,lng),
                        map: map,
                        title: results[0].formatted_address
                    });
                    /*var addrMarker = new google.maps.Marker({
                     position: addrLocation,
                     map: map,
                     title: results[0].formatted_address,
                     icon: 'marker.png'
                     });*/
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        function createMarker(latlng, name, html) {
            console.log(latlng);
            var lat=latlng.lat();
            var lng=latlng.lng();
            fillLatLontoinp(lat,lng);
            var contentString = html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                zIndex: Math.round(latlng.lat()*-100000)<<5
            });

            /*google.maps.event.addListener(marker, 'click', function() {
             infowindow.setContent(contentString);
             infowindow.open(map,marker);
             });
             google.maps.event.trigger(marker, 'click');*/
            return marker;
        }


        function fillLatLontoinp(lat,lon){
            document.getElementById("properties-lat").value=lat;
            document.getElementById("properties-long").value=lon;
        }
    </script>
</div>

</div>

