<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Properties */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Project Specification', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $baseurl=Yii::$app->urlManager->baseUrl;
    $files= $model->getPropertiesPics()->all();
    $fileanchor='';
    foreach($files as $file){
        if(!empty($file)){

            $fileanchor.= Html::a($file->pic,$baseurl."/uploads/".$file->pic);
            $fileanchor.= "<br/>";
        }
    }

    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',

            [
              'label'=>'Property Group',
                'attribute'=>'propGroup',
            ],

            'propType.name',
            //'no_units',
            'tot_units',
            'no_floors',
            'tot_proj_area',
            'area_range',
            'start_price',
            [
                'label'=>" Loans",
                'value'=>($model->loan)?"Yes":"No"
            ],
            [
                'label'=>" Featured",
                'value'=>($model->featured)?"Yes":"No"
            ],
            'proj_start_date',
            'poss_date',
            'proj_status',
            [
                'label'=>" Master Plan",
                'format'=>'HTML',
                'value'=>Html::a("Master Plan",Yii::$app->urlManager->baseUrl."/uploads/".$model->master_plan)
            ],
            //'site_contact_no',
            'year_of_construction',
            [
                'attribute'=>'fadrs',
                'format'=>'Ntext',
                'label'=>" Address",
            ],
            ['label'=>'Photos',
                'value'=>$fileanchor,
                'format'=>'html'
            ],

        ],
    ]) ?>

</div>
