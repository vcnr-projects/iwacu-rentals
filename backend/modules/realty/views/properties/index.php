<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\PropertiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Specification';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project Specification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            //'prop_type',
            'tot_units',
            'no_floors',
            // 'year_of_construction',
            // 'address_id',
            // 'tot_units',
            // 'site_contact_email:email',
            // 'site_contact_no',
            // 'image:ntext',
            // 'lat',
            // 'long',
            // 'incharge_name',
            // 'designation',
            // 'builder_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
