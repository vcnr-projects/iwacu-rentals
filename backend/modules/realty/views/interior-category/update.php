<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropUnitInteriorCat */

$this->title = 'Update Prop Unit Interior Cat: ' . ' ' . $model->category;
$this->params['breadcrumbs'][] = ['label' => 'Prop Unit Interior Cats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->category, 'url' => ['view', 'id' => $model->category]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prop-unit-interior-cat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
