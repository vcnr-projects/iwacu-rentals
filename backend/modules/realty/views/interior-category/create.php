<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropUnitInteriorCat */

$this->title = 'Create Property Unit Interior Category';
$this->params['breadcrumbs'][] = ['label' => 'Property Unit Interior Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-unit-interior-cat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
