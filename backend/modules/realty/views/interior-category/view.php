<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropUnitInteriorCat */

$this->title = $model->category;
$this->params['breadcrumbs'][] = ['label' => 'Property Unit Interior Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-unit-interior-cat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->category], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->category], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],
        ],
    ]) ?>

</div>
