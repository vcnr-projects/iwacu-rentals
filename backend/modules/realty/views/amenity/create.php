<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\AmenityType */

$this->title = 'Create Amenity Type';
$this->params['breadcrumbs'][] = ['label' => 'Amenity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenity-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
