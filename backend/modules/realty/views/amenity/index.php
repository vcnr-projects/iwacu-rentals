<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\AmenityTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Amenity Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenity-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Amenity Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'amenity',
            [
                'attribute'=>'is_present',
                'filter'=>Html::activeDropDownList($searchModel,'is_present',
                    [
                        "1"=>"Yes",
                        "0"=>"No",
                    ],
                    ['class'=>'form-control','prompt' => 'Select Active Status']
                ),
                'value'=>function($data){
                    return ($data->is_present)?"Yes":"NO";
                }
            ],
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
