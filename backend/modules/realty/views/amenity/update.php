<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\AmenityType */

$this->title = 'Update Amenity Type: ' . ' ' . $model->amenity;
$this->params['breadcrumbs'][] = ['label' => 'Amenity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->amenity, 'url' => ['view', 'id' => $model->amenity]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="amenity-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
