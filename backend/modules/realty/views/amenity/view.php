<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\AmenityType */

$this->title = $model->amenity;
$this->params['breadcrumbs'][] = ['label' => 'Amenity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenity-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->amenity], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->amenity], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amenity',
            [
                'attribute'=>"icon",
                'format'=>'HTML',
                'value'=>Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon)
            ],
            [
                'attribute'=>'is_present',
                'value'=>($model->is_present)?"Yes":"NO",
            ],
            'created_on',
        ],
    ]) ?>

</div>
