<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\AmenityType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amenity-type-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'amenity')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'is_present')->textInput() ?>

    <?= Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon) ?>
    <?= $form->field($model, 'icon')->fileInput() ?>


    <?= $form->field($model, 'type')->dropDownList(['Amenity'=>'Amenity',"Facility"=>"Facility"]); ?>

    <?= $form->field($model, 'is_present')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
