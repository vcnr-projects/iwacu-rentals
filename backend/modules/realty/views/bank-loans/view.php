<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\BankLoans */

$this->title = $model->loan;
$this->params['breadcrumbs'][] = ['label' => 'Bank Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-loans-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->loan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->loan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'loan',
            [
                'attribute'=>"icon",
                'format'=>'HTML',
                'value'=>Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon)
            ],
            //'icon',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],
        ],
    ]) ?>

</div>
