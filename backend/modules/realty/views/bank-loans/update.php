<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\BankLoans */

$this->title = 'Update Bank Loans: ' . ' ' . $model->loan;
$this->params['breadcrumbs'][] = ['label' => 'Bank Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->loan, 'url' => ['view', 'id' => $model->loan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bank-loans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
