<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyPartitionType */

$this->title = 'Update Property Partition Type: ' . ' ' . $model->partition_type;
$this->params['breadcrumbs'][] = ['label' => 'Property Partition Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->partition_type, 'url' => ['view', 'id' => $model->partition_type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-partition-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
