<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyPartitionType */

$this->title = 'Create Property Partition Type';
$this->params['breadcrumbs'][] = ['label' => 'Property Partition Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-partition-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
