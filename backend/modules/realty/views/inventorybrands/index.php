<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\InventoryBrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventory Brands';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-brands-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Inventory Brands', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'inventoryName',
            'brand',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
