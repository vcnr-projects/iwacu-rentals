<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\InventoryBrands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-brands-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inventory')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\Inventory::find()->asArray()->all(),'id'
        ,function($model, $defaultValue) {
            //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
            return $model["name"]." [".$model["cat"]."]";
        })
        ) ?>

    <?= $form->field($model, 'brand')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
