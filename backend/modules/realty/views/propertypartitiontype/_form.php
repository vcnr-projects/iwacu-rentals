<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyPartitionType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-partition-type-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'partition_type')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'is_active')->textInput() ?>

    <? //= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon) ?>
    <?= $form->field($model, 'icon')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
