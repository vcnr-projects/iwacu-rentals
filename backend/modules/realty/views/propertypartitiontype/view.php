<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyPartitionType */

$this->title = $model->partition_type;
$this->params['breadcrumbs'][] = ['label' => 'Property Partition Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-partition-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->partition_type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->partition_type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'partition_type',
            [
                'attribute'=>"icon",
                'format'=>'HTML',
                'value'=>Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon)
            ],
            /*'is_active',
            'category',*/
        ],
    ]) ?>

</div>
