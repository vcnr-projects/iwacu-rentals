<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\InventoryCategory */

$this->title = 'Update Inventory Category: ' . ' ' . $model->cat;
$this->params['breadcrumbs'][] = ['label' => 'Inventory Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cat, 'url' => ['view', 'id' => $model->cat]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inventory-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
