<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\InventoryCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventory Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Inventory Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cat',
            'description:ntext',
            [
                'attribute'=>'is_active',
                'filter'=>Html::activeDropDownList($searchModel,'is_active',
                    [
                        "1"=>"Yes",
                        "0"=>"No",
                    ],
                    ['class'=>'form-control','prompt' => 'Select Active Status']
                ),
                'value'=>function($data){
                    return ($data->is_active)?"Yes":"NO";
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
