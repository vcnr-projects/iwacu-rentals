<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropAreaPartitionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prop-area-partition-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'punit_id') ?>

    <?= $form->field($model, 'area_partition_type') ?>

    <?= $form->field($model, 'area') ?>

    <?= $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
