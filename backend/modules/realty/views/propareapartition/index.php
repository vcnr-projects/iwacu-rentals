<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\PropAreaPartitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prop Area Partitions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-area-partition-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Prop Area Partition', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'punit_id',
            'area_partition_type',
            'area',
            'remarks:ntext',
            // 'name',
            // 'image:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
