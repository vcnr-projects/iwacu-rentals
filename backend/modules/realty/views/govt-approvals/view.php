<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\GovtApprovals */

$this->title = $model->approvals;
$this->params['breadcrumbs'][] = ['label' => 'Govt Approvals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="govt-approvals-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->approvals], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->approvals], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $baseurl=Yii::$app->urlManager->baseUrl; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'approvals',

            [
                'attribute'=>"icon",
                'format'=>'HTML',
                'value'=>Html::a($model->icon,Yii::$app->urlManager->baseUrl."/uploads/".$model->icon)
            ],
            //'icon',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],
        ],
    ]) ?>

</div>
