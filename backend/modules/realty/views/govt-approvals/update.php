<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\GovtApprovals */

$this->title = 'Update Govt Approvals: ' . ' ' . $model->approvals;
$this->params['breadcrumbs'][] = ['label' => 'Govt Approvals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->approvals, 'url' => ['view', 'id' => $model->approvals]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="govt-approvals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
