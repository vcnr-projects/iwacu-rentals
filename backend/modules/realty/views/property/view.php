<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Property */
$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false&libraries=places', ['position' => yii\web\View::POS_HEAD]);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?php $user= $model->getMember()->one();
        if($user) {
            if ($user->user_type == 'Individual') {
                $individual = \app\modules\member\models\Individual::findOne(["member_id" => $user->id]);
                $url = Yii::$app->urlManager->createUrl(["/member/individual/view", 'id' => $individual->id]);
                $ulab = "Back to Individual [{$individual->fname}]";
            }
            if ($user->user_type == 'Company') {
                $individual = \app\modules\member\models\Company::findOne(["member_id" => $user->id]);
                $url = Yii::$app->urlManager->createUrl(["/member/company/view", 'id' => $individual->id]);
                $ulab = "Back to Company [{$individual->fname}]";
            }
            echo Html::a($ulab, $url, ['class' => 'btn btn-primary']) ;
        }
        ?>

    </p>
    <ul class="nav nav-tabs ">
        <?php
        $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
        $actclstxt='class="active"';
        ?>
        <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen"><?= explode("_",$model->scenario)[1] ?> </a></li>
        <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Amenities</a></li>
        <li <?= ($viewtab=="park")?$actclstxt:"" ?>  ><a data-toggle="tab" href="#park">Accessibilities</a></li>
        <li <?= ($viewtab=="punits")?$actclstxt:"" ?> ><a data-toggle="tab" href="#punits"><?= explode("_",$model->scenario)[1]?> Units</a></li>
    </ul>


    <div class="tab-content">

       <!-- begin: property detail-->
    <div id="gen" class="tab-pane fade in <?= ($viewtab=="gen")?"active":"" ?> ">
        <h3><?= explode("_",$model->scenario)[1] ?>  Detail </h3>
        <a href="<?= Yii::$app->urlManager->createUrl(["/realty/property/update","id"=>$model->id]) ?>" >Edit</a>


    <?php $member=$model->getMember()->one() ?>

        <?php
        $baseurl=Yii::$app->urlManager->baseUrl;
        $files= $model->getPropertiesPics()->all();
        $fileanchor='';
        foreach($files as $file){
            if(!empty($file)){

                $fileanchor.= Html::a($file->pic,$baseurl."/uploads/".$file->pic);
                $fileanchor.= "<br/>";
            }
        }

        ?>
        <style>
            .hover01 figure img {
                -webkit-transform: scale(1);
                transform: scale(1);
                -webkit-transition: .3s ease-in-out;
                transition: .3s ease-in-out;
            }
            .hover01 figure:hover img {
                -webkit-transform: scale(1.3);
                transform: scale(1.3);
            }

            .gallery_image{
                margin: 10px;

            }

            .img_cont_span{
                position: relative;
                display: inline-block;
            }
            .img_cont_span .afterimage{
                position: absolute;
                background-color: #ffffff;
                display: none;

            }

            .img_cont_span:hover > img.gallery_image{
                -webkit-transform: scale(1.5);
                transform: scale(1.5);
                z-index: 1;
                margin: 20px;


            }

            .img_cont_span:hover > span.afterimage{
                font-size: 30px;
                color:red;
                z-index: 99;
                background-color: #ffffff;
                opacity: 0.7;
                top:25%;
                left:40%;

                display: block !important;
                border: 1px;
                border-radius: 3px;
                cursor: pointer;


            }




        </style>
        <script>
            function deleteImage(obj){
                //alert($(obj).attr("sid"));
                $(obj).fadeOut(500);
                var did=$(obj).attr("sid");
                //$(obj).fadeIn();
                $.post('<?php echo \yii\helpers\Url::to(['/realty/property/deleteproppics',"id"=>$model->id]) ?>',{'deleteId':did}
            ,function(data){
                //data=JSON.parse(data);
                        //alert(data);
                        console.log(data);
                if(data.success){
                    //alert(data.success);
                    //window.location.reload();
                    $(obj).closest('.img_cont_span').remove();
                }
            });
            }
        </script>
        <div class="row">
            <div class="col-md-9">

                <?php
                $baseurl=Yii::$app->urlManager->baseUrl;
                $files= $model->getPropertiesPics()->all();
                $fileanchor='';
                $masterPlan='';
                $floorPlan='';
                $brochure='';
                $video='';
                $aform='';
                $doc='';
                $csp='';
                $bip='';
                if($files)
                    foreach($files as $file){
                        if(!empty($file)){

                            if($file->type=="Master Plan"){
                                $masterPlan.=Html::a($file->pic,$baseurl."/uploads/".$file->pic);
                                $masterPlan.="<br/>";
                            }
                            elseif($file->type=="Gallery") {
                                //$fileanchor .= Html::img( $baseurl . "/uploads/" . $file->pic,['width'=>"100px",'height'=>"100px","id"=>$file->id,'alt'=>$file->pic,'class'=>'gallery_image']);
                                $fileanchor .= "<span class='img_cont_span' sid='{$file->id}' ><img src='".\yii\helpers\Url::to( $baseurl . "/uploads/" . $file->pic)."' class='gallery_image' alt='{$file->pic}' width='100px' height='100px' class='gallery_image'  /><span class='afterimage'  sid='{$file->id}' onclick='deleteImage(this)' >X</span></span></span>";

                            }
                            elseif($file->type=="Floor Plan") {
                                $floorPlan .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $floorPlan .= "<br/>";
                            }
                            elseif($file->type=="Brochure") {
                                $brochure .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $brochure .= "<br/>";
                            }
                            elseif($file->type=="Walkthrough Video") {
                                if(!strrpos($file->pic,"http://")){
                                    $file->pic="http://".$file->pic;
                                }
                                $video .= Html::a($file->pic,$file->pic);
                                $video .= "<br/>";
                            }
                            elseif($file->type=="Allotment Form") {
                                $aform .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $aform .= "<br/>";
                            }
                            elseif($file->type=="List of documents") {
                                $doc .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $doc .= "<br/>";
                            }

                            elseif($file->type=="Construction Status Photos") {
                                $csp .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $csp .= "<br/>";
                            }

                            elseif($file->type=="Banner Image") {
                                $bip .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                                $bip .= "<br/>";
                            }

                        }
                    }
                ?>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'label'=>'Member',
                'value'=>($member)?($member->username." / ".$member->email):"",
                'visible'=>($member)?true:false
            ],

            'propGroup',
            'propType.name',
            'proj_status',
            'builder_name',
            'name',
            'tag_line',
            'overview',
            'locality',
            'locality_desc',
            'tot_proj_area',
            'area_range',
            'start_price',
            'proj_start_date',
            'poss_date',
            'tot_units',
            'no_floors',
            //'no_units',
            'no_block',

            [
                'label'=>" Master Plan",
                'format'=>'HTML',
                'value'=>$masterPlan
            ],
            [
                'label'=>"Floor Plan",
                'format'=>'HTML',
                'value'=>$floorPlan
            ],
            [
                'label'=>"Brochure",
                'format'=>'HTML',
                'value'=>$brochure
            ],
            [
                'label'=>"Walkthrough Video",
                'format'=>'HTML',
                'value'=>$video
            ],
            /*[
                'label'=>"Gallery",
                'format'=>'RAW',
                'value'=>$fileanchor
            ],*/
            [
                'label'=>"Construction Status Photos",
                'format'=>'HTML',
                'value'=>$csp
            ],
            [
                'label'=>"Banner Image",
                'format'=>'HTML',
                'value'=>$bip
            ],
            'facilities',
            [
                'label'=>" Approvals",
                'value'=>($model->approvals)
            ],
            [
                'label'=>" Loans",
                'value'=>($model->loan)
            ],
            [
                'attribute'=>'list_of_doc',
                'format'=>'Ntext'
            ],

            [
                'label'=>" Featured",
                'value'=>($model->featured)?"Yes":"No"
            ],
            'incharge_name',
            'designation',
            'site_contact_email',
            [
                'attribute'=>'fadrs',
                'format'=>'Ntext',
                'label'=>" Address",
            ],
            'lat',
            'long',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],


            //'overview',

            //'start_price',




            /*'proj_status',

            [
                'label'=>"Gallery",
                'format'=>'HTML',
                'value'=>$fileanchor
            ],
            [
                'label'=>"Allotment Form",
                'format'=>'HTML',
                'value'=>$doc
            ],

            //'site_contact_no',
            'year_of_construction',
            ['label'=>'Photos',
                'value'=>$fileanchor,
                'format'=>'html'
            ],

            ['label'=>'Photos',
                'value'=>$fileanchor,
                'format'=>'html'
            ],
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],*/


        ],
    ]) ?>

            </div>
            <div class="col-md-3">
                <?php
                    if(!empty($files)&&isset($files[0])){
                        echo Html::img($baseurl."/uploads/".$file->pic);
                    }
                ?>
                <div class="gallery_container">
                    <div id="gallery_images">
                    <h4>Gallery</h4>
                    <?= $fileanchor?>
                    </div>
                    <div class="addinput">
                        <form id="imageForm" enctype="multipart/form-data">
                            <input name="file" type="file" />
                            <input class="button" type="button" value="Upload" />
                        </form>
                        <!--<progress></progress>-->
                    </div>
                    <script>

                        function buttonclick() {
                            $(".button").click(function () {
                                var formData = new FormData($("#imageForm")[0]);
                                $.ajax({
                                    url: '<?php echo \yii\helpers\Url::to(['/realty/property/addproppics',"id"=>$model->id]) ?>',
                                    type: 'POST',
                                    xhr: function () {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if (myXhr.upload) {
                                            myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                                            return myXhr;
                                        }
                                    },
                                   // beforeSend: beforeSendHandler,
                                    success: completeHandler,
                                    error: errorHandler,
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });
                            });
                        }
                        window.onload=buttonclick;

                        function progressHandlingFunction(e){
                            if(e.lengthComputable){
                                $('progress').attr({value: e.loaded,max: e.total});
                            }
                        }
                        function completeHandler(e){
                            //alert("complete");
                            console.log(e.file_name);
                            var html=$("<span class='img_cont_span' sid='"+ e.sid+"'><img src='"+ e.file_name+"' class='gallery_image' alt='"+ e.file_name+"' width='100px' height='100px'><span class='afterimage' sid='"+ e.sid+"' onclick='deleteImage(this)'>X</span></span>");
                            $("#gallery_images").append(html);
                        }
                        function errorHandler(e){
                            alert("error");
                        }

                    </script>
                </div>

            </div>
        </div>


    </div>
        <!-- end: property detail-->


        <!-- begin: amenities detail-->
        <div id="off" class="tab-pane fade in  <?= ($viewtab=="off")?"active":"" ?>">
            <h3><?= explode("_",$model->scenario)[1] ?> Amenities  Detail </h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/property/amenitiesform","id"=>$model->id]) ?>" >Edit</a>




            <div class="well">
                <h3>Amenities</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Amenity</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->amenities as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->amenity ?></td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>


            <div class="well">
                <h3>Facilities</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Facility</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $model->facilities=$model->getPropertyFacilities()->all();
                    foreach($model->facilities as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->amenity ?></td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>


        </div>
        <!-- end: amenties detail-->

        <!-- begin: Accessibilites detail-->
        <div id="park" class="tab-pane fade in <?= ($viewtab=="park")?"active":"" ?> ">
            <h3><?= explode("_",$model->scenario)[1] ?> Accessibilites  Detail </h3>

            <div id="places">

            </div>
            <!-- <a href="<?= Yii::$app->urlManager->createUrl(["/realty/property/accessibilitesform","id"=>$model->id]) ?>" >Edit</a>

            <div class="well">
                <h3>Accessibilites</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Accessibilites</th>
                        <th>Remarks</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->accessibilities as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->accessibility ?></td>
                            <td><?= $par->remaks ?></td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
            -->

            <!--begin: map-->
            <style>
                /* main styles */

                #gmap_canvas {
                    height: 700px;
                    position: relative;
                    width: 100%;
                    float: left;
                }
                .actions {
                    background-color: #FFFFFF;
                    bottom: 600px;
                    padding: 10px;
                    position: relative;
                    float:right;
                    width: 170px;
                    z-index: 2;

                    border-top: 1px solid #abbbcc;
                    border-left: 1px solid #a7b6c7;
                    border-bottom: 1px solid #a1afbf;
                    border-right: 1px solid #a7b6c7;
                    -webkit-border-radius: 12px;
                    -moz-border-radius: 12px;
                    border-radius: 12px;
                }
                .actions label {
                    display: block;
                    margin: 2px 0 5px 10px;
                    text-align: left;
                }
                .actions input, .actions select {
                    width: 85%;
                }
                .button {
                    background-color: #d7e5f5;
                    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #d7e5f5), color-stop(100%, #cbe0f5));
                    background-image: -webkit-linear-gradient(top, #d7e5f5, #cbe0f5);
                    background-image: -moz-linear-gradient(top, #d7e5f5, #cbe0f5);
                    background-image: -ms-linear-gradient(top, #d7e5f5, #cbe0f5);
                    background-image: -o-linear-gradient(top, #d7e5f5, #cbe0f5);
                    background-image: linear-gradient(top, #d7e5f5, #cbe0f5);
                    border-top: 1px solid #abbbcc;
                    border-left: 1px solid #a7b6c7;
                    border-bottom: 1px solid #a1afbf;
                    border-right: 1px solid #a7b6c7;
                    -webkit-border-radius: 12px;
                    -moz-border-radius: 12px;
                    border-radius: 12px;
                    -webkit-box-shadow: inset 0 1px 0 0 white;
                    -moz-box-shadow: inset 0 1px 0 0 white;
                    box-shadow: inset 0 1px 0 0 white;
                    color: #1a3e66;
                    font: normal 11px "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Geneva, Verdana, sans-serif;
                    line-height: 1;
                    margin-bottom: 5px;
                    padding: 6px 0 7px 0;
                    text-align: center;
                    text-shadow: 0 1px 1px #fff;
                    width: 150px;
                }
                .button:hover {
                    background-color: #ccd9e8;
                    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ccd9e8), color-stop(100%, #c1d4e8));
                    background-image: -webkit-linear-gradient(top, #ccd9e8, #c1d4e8);
                    background-image: -moz-linear-gradient(top, #ccd9e8, #c1d4e8);
                    background-image: -ms-linear-gradient(top, #ccd9e8, #c1d4e8);
                    background-image: -o-linear-gradient(top, #ccd9e8, #c1d4e8);
                    background-image: linear-gradient(top, #ccd9e8, #c1d4e8);
                    border-top: 1px solid #a1afbf;
                    border-left: 1px solid #9caaba;
                    border-bottom: 1px solid #96a3b3;
                    border-right: 1px solid #9caaba;
                    -webkit-box-shadow: inset 0 1px 0 0 #f2f2f2;
                    -moz-box-shadow: inset 0 1px 0 0 #f2f2f2;
                    box-shadow: inset 0 1px 0 0 #f2f2f2;
                    color: #163659;
                    cursor: pointer;
                }
                .button:active {
                    border: 1px solid #8c98a7;
                    -webkit-box-shadow: inset 0 0 4px 2px #abbccf, 0 0 1px 0 #eeeeee;
                    -moz-box-shadow: inset 0 0 4px 2px #abbccf, 0 0 1px 0 #eeeeee;
                    box-shadow: inset 0 0 4px 2px #abbccf, 0 0 1px 0 #eeeeee;
                }
            </style>
            <div id="container" class="container">
                <div id="gmap_canvas" ></div>
                <div class="actions">
                   <!-- <div class="button">
                        <label for="gmap_where">Where:</label>
                        <input id="gmap_where" type="text" name="gmap_where" /></div>
                    <div id="button2" class="button" onclick="findAddress(); return false;">Search for address</div>
                    -->
                    <div class="button">
                        <label for="gmap_keyword">Keyword (optional):</label>
                        <input id="gmap_keyword" type="text" name="gmap_keyword" /></div>
                    <div class="button">
                        <label for="gmap_type">Type:</label>
                        <select id="gmap_type">
                            <option value="art_gallery">Art gallery</option>
                            <option value="atm">atm</option>
                            <option value="bank">bank</option>
                            <option value="bar">bar</option>
                            <option value="cafe">cafe</option>
                            <option value="food">food</option>
                            <option value="hospital">hospital</option>
                            <option value="police">police</option>
                            <option value="store">store</option>
                            <option value="book_store">Book store</option>
                            <option value="car_repair">Car Repair</option>
                            <option value="doctor">Doctor</option>
                            <option value="electronics_store">Electronics Store</option>
                        </select>
                    </div>
                    <div class="button">
                        <label for="gmap_radius">Radius:</label>
                        <select id="gmap_radius">
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                            <option value="1500">1500</option>
                            <option value="5000">5000</option>
                        </select>
                    </div>
                    <input type="hidden" id="lat" name="lat" value="<?=$model->lat ?>" />
                    <input type="hidden" id="lng" name="lng" value="<?=$model->long ?>" />
                    <div id="button1" class="button" onclick="findPlaces(); return false;">Search for objects</div>
                </div>
            </div>
            <script>
                var geocoder;
                var map;
                var markers = Array();
                var infos = Array();
                function fillPredefinedPlaces(){

                    var lat = document.getElementById('lat').value;
                    var lng = document.getElementById('lng').value;
                    var cur_location = new google.maps.LatLng(lat, lng);
                    var request1= {
                        location: cur_location,
                        radius: 5000,
                        query: "food"
                    };

                    // send request
                   var service = new google.maps.places.PlacesService(map);

                    service.textSearch(request1, function(results, status, pagination){
                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            console.log(results);
                            for(var i=0;i<results.length;i++){
                               // $("#places").append(results[i].name);
                            }
                            /*resultList = resultList.concat(results);
                             plotResultList();*/
                        }
                    });
                }

                function initialize() {

                    // prepare Geocoder
                    geocoder = new google.maps.Geocoder();

                    // set initial position (New York)
                    var myLatlng = new google.maps.LatLng(<?=$model->lat ?>,<?=$model->long ?>);
		    // myLatlng=new google.maps.LatLng(37.4419, -122.1419);

                    var myOptions = { // default map options
                        zoom: 14,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: 'Property'
                    });
                    //alert();
                    //google.maps.event.trigger(map, 'resize');
                    //map.setCenter(new google.maps.LatLng(37.4419, -122.1419), 13);

                    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {

                        google.maps.event.trigger(map, 'resize');
                        map.setCenter(myLatlng,14);
                        //$("#gmap_canvas").css("width", '100%').css("height", 400);
                    });

                    fillPredefinedPlaces();

                    //

                }

                // clear overlays function
                function clearOverlays() {
                    if (markers) {
                        for (i in markers) {
                            markers[i].setMap(null);
                        }
                        markers = [];
                        infos = [];
                    }
                }

                // clear infos function
                function clearInfos() {
                    if (infos) {
                        for (i in infos) {
                            if (infos[i].getMap()) {
                                infos[i].close();
                            }
                        }
                    }
                }

                // find address function
                function findAddress() {
                    var address = document.getElementById("gmap_where").value;

                    // script uses our 'geocoder' in order to find location by address name
                    geocoder.geocode( { 'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) { // and, if everything is ok

                            // we will center map
                            var addrLocation = results[0].geometry.location;
                            map.setCenter(addrLocation);

                            // store current coordinates into hidden variables
                            document.getElementById('lat').value = results[0].geometry.location.$a;
                            document.getElementById('lng').value = results[0].geometry.location.ab;

                            // and then - add new custom marker
                            var addrMarker = new google.maps.Marker({
                                position: addrLocation,
                                map: map,
                                title: results[0].formatted_address,
                                icon: 'marker.png'
                            });
                        } else {
                            alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }

                // find custom places function
                function findPlaces() {

                    // prepare variables (filter)
                    var type = document.getElementById('gmap_type').value;
                    var radius = document.getElementById('gmap_radius').value;
                    var keyword = document.getElementById('gmap_keyword').value;

                    var lat = document.getElementById('lat').value;
                    var lng = document.getElementById('lng').value;
                    var cur_location = new google.maps.LatLng(lat, lng);

                    // prepare request to Places
                    var request = {
                        location: cur_location,
                        radius: radius,
                        types: [type]
                    };
                    if (keyword) {
                        request.keyword = [keyword];
                    }

                    var request1= {
                        location: cur_location,
                        radius: radius,
                        query: type
                    };

                    // send request
                    service = new google.maps.places.PlacesService(map);
                    service.search(request, createMarkers);

                    service.textSearch(request1, function(results, status, pagination){
                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            console.log(results);
                            /*resultList = resultList.concat(results);
                            plotResultList();*/
                        }
                    });
                }

                // create markers (from 'findPlaces' function)
                function createMarkers(results, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {

                        // if we have found something - clear map (overlays)
                        clearOverlays();

                        // and create new markers by search result
                        for (var i = 0; i < results.length; i++) {
                            createMarker(results[i]);
                        }
                    } else if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
                        alert('Sorry, nothing is found');
                    }
                }

                // creare single marker function
                function createMarker(obj) {

                    // prepare new Marker object
                    var mark = new google.maps.Marker({
                        position: obj.geometry.location,
                        map: map,
                        title: obj.name
                    });
                    markers.push(mark);

                    // prepare info window
                    var infowindow = new google.maps.InfoWindow({
                        content: '<img src="' + obj.icon + '" /><font style="color:#000;">' + obj.name +
                        '<br />Rating: ' + obj.rating + '<br />Vicinity: ' + obj.vicinity + '</font>'
                    });

                    // add event handler to current marker
                    google.maps.event.addListener(mark, 'click', function() {
                        clearInfos();
                        infowindow.open(map,mark);
                    });
                    infos.push(infowindow);
                }

                // initialization
                google.maps.event.addDomListener(window, 'load', initialize);
            </script>
            <!--end: map-->


        </div>
        <!-- end: Accessibilites detail-->


        <!-- begin: prop unites detail-->
        <div id="punits" class="tab-pane fade in  <?= ($viewtab=="punits")?"active":"" ?>  ">
            <h3><?= explode("_",$model->scenario)[1] ?> Units </h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/create","id"=>$model->id]) ?>" >Create </a>

            <div class="well">
                <h3><?= explode("_",$model->scenario)[1]?> Units</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name</th>
                        <th>View</th>
                        <th>Contract</th>
                        <th>Tenant</th>

                        <th>Executive</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $model->properties=$model->getPropertyUnits()->all();

                    if(count($model->properties)) {
                        $proptype = $model->getPropType()->one()->name;
                        if ($model->no_units > count($model->properties)) {
                            $cprop=count($model->properties);
                            $pprop=$model->no_units-$cprop;
                            echo "
                <div class='alert-danger'>
                   Total No. of {$proptype} Units : {$model->no_units} <br/>
                   Created No. of  {$proptype} Units : {$cprop} <br/>
                   Pending No. of  {$proptype} Units : {$pprop} <br/>
                </div>
                ";
                        }
                    }
                    foreach($model->properties as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= "$model->name [ $par->block - $par->floor_number - $par->flat_no ]" ?>
                                <br/> Availability Date:<?= $par->availability_date?>
                            </td>
                            <td>
                                <?php
                                $url=Yii::$app->urlManager->createUrl(["/realty/propertyunits/view","id"=>$par->id]);
                                echo "
                                <a href='{$url}'><span class='glyphicon glyphicon-eye-open'></span></a>
                                ";
                                ?>
                            </td>
                            <td>
                                <?php
                                $service= $par->getActiveContract()->one();
                                if($service){
                                    $anchor=Html::a('Update Contract',Yii::$app->urlManager->createUrl(["/service/contract/update","id"=>$service->id,"member_id"=>$model->member_id]));
                                }else{
                                    $anchor=Html::a('Create Contract',Yii::$app->urlManager->createUrl(["/service/contract/create","member_id"=>$model->member_id,"property_id"=>$par->id]));

                                }
                                //$url=Yii::$app->urlManager->createUrl(["/service/contract/create","id"=>$par->id]);
                                /*echo "
                                <a href='{$url}'><span class='glyphicon glyphicon-eye-open'></span></a>
                                ";*/
                                echo $anchor;
                                ?>
                            </td>
                            <td>
                                <?php
                                $contracts=$par->getAcitveTenant()->all();
                                if(!empty($contracts)){
                                    foreach($contracts as $contract) {
                                        $user = $contract->getTenant()->one();
                                        $tenant = \app\modules\member\models\Tenant::find()->filterWhere(["member_id" => $user->id])->one();
                                        //echo $anchor=Html::a($par->getTenant()->one()->fname,Yii::$app->urlManager->createUrl(["/realty/propertyunits/allocatetenant","id"=>$par->id,"member_id"=>$model->member_id]));
                                        echo $anchor = Html::a($tenant->fname, Yii::$app->urlManager->createUrl(["/service/tenant-property-allocation/update", "id" => $contract->id,"member_id"=>$model->member_id]));
                                        echo "<br/>Expiry Date:".$contract->expiry_date;
                                        echo "<br/>";
                                    }
                                }else
                                     echo $anchor=Html::a('Tenant allocate',Yii::$app->urlManager->createUrl(["/service/tenant-property-allocation/create","prop_id"=>$par->id,"member_type"=>"Tenant"]));
                                ?>
                            </td>

                            <td>
                                <?php
                                $execs=$par->getExecutives()->all();
                                if(!empty($execs)){
                                    foreach($execs as $contract) {
                                        $user = $contract->getExec()->one();
                                        //$tenant = \app\modules\member\models\Tenant::find()->filterWhere(["member_id" => $user->id])->one();
                                        //echo $anchor=Html::a($par->getTenant()->one()->fname,Yii::$app->urlManager->createUrl(["/realty/propertyunits/allocatetenant","id"=>$par->id,"member_id"=>$model->member_id]));
                                        echo $anchor = Html::a($user->username, Yii::$app->urlManager->createUrl(["/executive-properties/update", "id" => $contract->id]));
                                        echo "<br/>";
                                    }
                                }else
                                    echo $anchor=Html::a('Executive allocate',Yii::$app->urlManager->createUrl(["/executive-properties/create","propid"=>$par->id]));

                                /*
                                if($par->executive_id){
                                    echo $anchor=Html::a($par->getExecutive()->one()->fname,Yii::$app->urlManager->createUrl(["/realty/propertyunits/allocateexecutive","id"=>$par->id,"member_id"=>$model->member_id]));
                                }else
                                    echo $anchor=Html::a('Executive allocate',Yii::$app->urlManager->createUrl(["/executive-properties/create","propid"=>$par->id]));
                                */
                                ?>
                            </td>


                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>


        </div>
        <!-- end: prop units detail-->


    </div>

</div>
