<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>true,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>






    <!--begin:amenity-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour1"> Amenities </a>
            </h4>
        </div>
        <div id="collapseFour1" class="panel-collapse collapse1">
            <div class="panel-body">
                <?php $list=\yii\helpers\ArrayHelper::map(\app\modules\realty\models\AmenityType::find()->where("type='Amenity'")->asArray()->all(),'amenity','amenity')  ?>

                <?php $sellist=[];
                foreach($model->amenities as $i=> $par){
                    $sellist[]=$par->amenity;
                }
                $model->amenities[0]->amenity=$sellist;
                 $model->amenities[0]->scenario='bulkInsert';
                ?>
                <?= $form->field($model->amenities[0], 'amenity')->checkboxlist($list
                    ,['item' =>
                        function ($index, $label, $name, $checked, $value) {
                            return Html::checkbox($name, $checked, [
                                'value' => $value,
                                'label' =>  $label ,
                                'labelOptions' => [
                                    'class' => 'ckbox ckbox-primary col-md-2',
                                ],
                                //'id' => $label,
                            ]);
                        },

                    ]);?>


                <?php
                $script = <<< JS
/*
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('');
});*/

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:amenity-->


    <!--begin:facility-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Facilities </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <?php $list=\yii\helpers\ArrayHelper::map(\app\modules\realty\models\AmenityType::find()->where("type='Facility'")->asArray()->all(),'amenity','amenity')  ?>

                <?php $sellist=[];
                foreach($model->facilities as $i=> $par){
                    $sellist[]=$par->amenity;
                }
                $model->facilities[0]->amenity=$sellist;
                $model->facilities[0]->scenario='bulkInsert';
                ?>
                <?= $form->field($model->facilities[0], 'amenity')->checkboxlist($list
                    ,['item' =>
                        function ($index, $label, $name, $checked, $value) {
                            return Html::checkbox($name, $checked, [
                                'value' => $value,
                                'label' =>  $label ,
                                'labelOptions' => [
                                    'class' => 'ckbox ckbox-primary col-md-2',
                                ],
                                //'id' => $label,
                            ]);
                        },

                    ]
                );?>


                <?php
                $script = <<< JS
/*
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('');
});*/

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:facility-->


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>