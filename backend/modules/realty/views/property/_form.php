<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false', ['position' => yii\web\View::POS_HEAD]);


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row" style="position: relative">
    <div class="col-md-6">

<div class="property-form ">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->errorSummary($model) ?>

    <?php //var_dump($model->scenario) ?>
    <?php if($model->scenario!="Builder"){ ?>
    <?= $form->field($model, 'member_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\User::find()->andWhere("user_type='Individual' or user_type='Company' ")->asArray()->all()
        ,'id'
        ,function($model, $defaultValue) {
            //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
            return $model["username"]." / ".$model["email"];
        }
    ),['prompt'=>'Select']) ?>

    <?php } ?>

    <?= $form->field($model, 'propGroup')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\PropertyGroup::find()->andFilterWhere(["is_active"=>1])->asArray()->all(),'name','name')
    ,[
            'prompt'=>'Select',
            'onchange'=>'
             $( "select#property-prop_type" ).html( "<option>Loading...</option>" );
                $.post( "'.Yii::$app->urlManager->createUrl('/realty/propertytype/ptypefrompgroup').'",{"depdrop_parents":$(this).val()}, function( data ) {
                  data=JSON.parse(data);
                  $( "select#property-prop_type" ).html( "<option>Select</option>" );
                  for(var i=0;i<data.output.length;i++){
                  var opt=$("<option></option>");
                  $(opt).attr("value",data.output[i][\'id\']);
                  $(opt).text(data.output[i][\'name\']);
                    $( "select#property-prop_type" ).append(opt);
                  }
                });
            '
        ]
    ) ?>



    <?= $form->field($model, 'prop_type')->dropDownList(\yii\helpers\ArrayHelper::map( \app\modules\realty\models\PropertyType::find()->asArray()->all(),'id','name'),['prompt'=>'Select',
        'onchange'=>'
                //$("#property-name").val($("option:selected",this).html()+"-");
                $("#propnamelabel").html("Project/"+$("option:selected",this).html()+" Name");
            '
    ]); ?>
    <?/*= $form->field($model, 'prop_type')->widget(\kartik\depdrop\DepDrop::classname(), [
        //'options' => ['id'=>'subcat-id'],
        'pluginOptions'=>[
            'depends'=>['property-propgroup'],
            'placeholder' => 'Select...',
            'url' => \yii\helpers\Url::to(['/realty/propertytype/ptypefrompgroup'])
        ]
    ]); */?>


    <?= $form->field($model, 'proj_status')->dropDownList([
        "Upcoming project"=>"Upcoming project",
        "Pre-launch project"=>"Pre-launch project",
        "Ongoing project"=>"Ongoing project",
        "Completed project"=>"Completed project",
    ],['prompt'=>'Select']) ?>

    <?php
    $sql="select name from district ";
    $cmd=Yii::$app->db->createCommand($sql);
    $data=$cmd->queryColumn();
    ?>

    <?= $form->field($model, 'city')->
    widget(\kartik\typeahead\Typeahead::className(),[
            'dataset' => [
                [
                    'local' => $data,
                    'limit' => 10
                ]
            ],
            'pluginOptions' => ['highlight' => true],
            'options' => ["id"=>'property-city','onblur'=>'$("#address-city").val(this.value);fillstatecountry(this.value)'],
        ]) ?>


    <?php
    $data=\app\modules\realty\models\Builder::find()->select('id')->column();
    ?>

    <?= $form->field($model, 'builder_name')->widget(\kartik\typeahead\Typeahead::classname(), [
        'options' => ['placeholder' => 'Bulider Name ...','onblur'=>'getProjects(this.value);'],
        'pluginOptions' => ['highlight'=>true],
        'dataset' => [
            [
                'local' => $data,
                'limit' => 10
            ]
        ]
    ]); ?>

    <script>

    </script>

    <?= $form->field($model, 'name',['template'=>'<label class="control-label" id="propnamelabel" for="property-name">Project Name</label>{input}{error}'])->widget(\kartik\typeahead\Typeahead::classname(), [
        'options' => ['placeholder' => 'Project Name ...','onblur'=>'getProjectDetails(this.value)'],
        'pluginOptions' => ['highlight'=>true],
        'dataset' => [
            [
                'local' => 'projects',
                'limit' => 10
            ]
        ]
    ]); ?>

    <?php
    /*= $form->field($model, 'name',['template'=>'<label class="control-label" id="propnamelabel" for="property-name">Project Name</label>{input}{error}'])->textInput(
        ['maxlength' => true,'onblur'=>'getProjectDetails(this.value)']) */
    ?>






    <script>



        function fillstatecountry(val){
            $.get("<?= Yii::$app->urlManager->createUrl("/assorted/district/jsondet")?>",{

                "q":val

            },function(data){
                data=JSON.parse(data);
               console.log(data);
                $("#address-country").val(data.country);
                $("#address-state").val(data.state);

            });
        }

        function getProjectDetails(name){
            var city=$("#property-city").val();

            $("#property-overview").val('');
            $("#property-locality").val('');
            $("#property-locality_desc").val('');
            $("#property-tot_proj_area").val('');
            $("#property-area_range").val('');
            $("#property-start_price").val('');
            $("#property-proj_start_date").val('');
            $("#property-poss_date").val('');
            $("#property-tot_units").val('');
            $("#property-no_floors").val('');
            $("#property-no_block").val('');
            $("#property-facilities").text('');
            $("#property-list_of_doc").text('');

            $("#copyFilesList").empty();
            $("#uploadcopy").attr("disabled",true);




            $.get("<?= Yii::$app->urlManager->createUrl("/realty/property/projectdetails")?>",{
                "name":name,
                "city":city
            },function(data){
                data=JSON.parse(data);

                $("#property-overview").val(data.overview);
                $("#property-locality").val(data.locality);
                $("#property-locality_desc").val(data.locality_desc);
                $("#property-tot_proj_area").val(data.tot_proj_area);
                $("#property-area_range").val(data.area_range);
                $("#property-start_price").val(data.start_price);
                $("#property-proj_start_date").val(data.start_date);
                $("#property-poss_date").val(data.poccession_date);
                $("#property-tot_units").val(data.tot_units);
                $("#property-no_floors").val(data.no_floors);
                $("#property-no_block").val(data.no_block);
                $("#property-facilities").text(data.facilities);
                $("#property-list_of_doc").text(data.list_of_doc);

                for(var i=0;i<data.pics.length;i++){
                    $("#uploadcopy").removeAttr("disabled");
                    var pic=data.pics[i];
                    $("#copyFilesList").append("<a href='<?= Yii::$app->urlManager->baseUrl."/uploads/"?>"+pic.pic+"'>"+pic.type+" ["+pic.pic+"]"+"</a><br/>");
                }

                data.approvals=data.approvals.split(",");
                for(var i=0;i<data.approvals.length;i++){
                    $(".approvals input[value='"+data.approvals[i]+"']").attr('checked', true);
                }




                $("#property-builder_name").val(data.builder_name);
                $("#property-lat").val(data.lat);
                $("#property-long").val(data.long);
                $("#property-year_of_construction").val(data.year_of_construction);
                $("#address-adrs_line1").val(data.adrs_line1);
                $("#address-country").val(data.country);
                $("#address-state").val(data.state);
                $("#address-postal_code").val(data.postal_code);
            });
        }


        function getLocalityDesc(val){

            $.get("<?= Yii::$app->urlManager->createUrl("/realty/property/getlocalitydesc")?>",{
                "locality":val

            },function(data){

                data=JSON.parse(data);
                if(data){
                    $("#property-locality_desc").val(data.locality_desc);
                }


            });
        }


        function getProjects(val){

            var city=$("#property-city").val();
            $.get("<?= Yii::$app->urlManager->createUrl("/realty/property/projectsofbulider")?>",{
                "name":val,
                "city":city

            },function(data){
                //alert(data);
                data=JSON.parse(data);
                 property_name_data_1 = new Bloodhound({"datumTokenizer":Bloodhound.tokenizers.whitespace,"queryTokenizer":Bloodhound.tokenizers.whitespace,"local":data});
                kvInitTA('property-name', typeahead_7864e59a, [{"limit":10, minLength: 0,"name":"property_name_data_1","source":property_name_data_1.ttAdapter()}]);

                //$("#property-name").data('typeahead').source = data;
                if(data){

                    //$("#property-locality_desc").val(data.locality_desc);
                }


            });
        }

        window.onload=function() {
            getProjects('<?= $model->builder_name?>');
        }

    </script>

    <?= $form->field($model, 'tag_line')->textInput() ?>
    <?= $form->field($model, 'overview')->textarea() ?>
    <?= $form->field($model, 'locality')->textInput(["onblur"=>"getLocalityDesc(this.value)"]) ?>
    <?= $form->field($model, 'locality_desc')->textarea() ?>

    <?= $form->field($model, 'tot_proj_area')->textInput() ?>
    <?= $form->field($model, 'area_range')->textInput() ?>
    <?= $form->field($model, 'start_price')->textInput() ?>


    <?= $form->field($model, 'proj_start_date')->widget(\kartik\widgets\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter  date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true

        ]
    ]); ?>

    <?= $form->field($model, 'poss_date')->widget(\kartik\widgets\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter  date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true

        ]
    ]); ?>


    <?= $form->field($model, 'tot_units')->textInput() ?>
    <?= $form->field($model, 'no_floors')->textInput() ?>
    <?= $form->field($model, 'no_block')->textInput() ?>

    <?= $form->field($model, 'bedroom_type')->textInput() ?>
    <? //= $form->field($model, 'servant_room')->textInput() ?>
    <? //= $form->field($model, 'no_bathroom')->textInput() ?>

    <label><input type="checkbox" name="uploadcopy" id="uploadcopy" disabled />Copy Images from Project details </label>
    <div id="copyFilesList">

    </div>
    <?php
    $baseurl=Yii::$app->urlManager->baseUrl;
    $files= $model->getPropertiesPics()->all();
    $fileanchor='';
    $masterPlan='';
    $floorPlan='';
    $brochure='';
    $video='';
    $aform='';
    $doc='';
    $csp='';
    $bip='';

    $video=[];

    if($files)
        foreach($files as $file){
            if(!empty($file)){

                if($file->type=="Master Plan"){
                    $masterPlan.=Html::a($file->pic,$baseurl."/uploads/".$file->pic);
                    $masterPlan.="<br/>";
                }
                elseif($file->type=="Gallery") {
                    $fileanchor .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $fileanchor .= "<br/>";
                }
                elseif($file->type=="Floor Plan") {
                    $floorPlan .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $floorPlan .= "<br/>";
                }
                elseif($file->type=="Brochure") {
                    $brochure .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $brochure .= "<br/>";
                }
                elseif($file->type=="Walkthrough Video") {
                    /*$video .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $video .= "<br/>";*/
                    $video[]=$file->pic;
                }
                elseif($file->type=="Allotment Form") {
                    $aform .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $aform .= "<br/>";
                }
                elseif($file->type=="List of documents") {
                    $doc .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $doc .= "<br/>";
                }

                elseif($file->type=="Construction Status Photos") {
                    $csp .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $csp .= "<br/>";
                }
                elseif($file->type=="Banner Image") {
                    $bip .= Html::a($file->pic, $baseurl . "/uploads/" . $file->pic);
                    $bip .= "<br/>";
                }

            }
        }
    ?>

    <?= $form->field($model, 'image[]',['template'=>'{label}'.$masterPlan.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[master_plan][]'])->label('Master Plan') ?>

    <?= $form->field($model, 'image[]',['template'=>'{label}'.$floorPlan.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[floor_plan][]'])->label('Floor Plan') ?>

    <?= $form->field($model, 'image[]',['template'=>'{label}'.$brochure.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[brochure][]'])->label('Brochure ') ?>


    <?= $form->field($model, 'image[]',['template'=>'{label}'.$fileanchor.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[gallery][]'])->label('Gallery') ?>

    <?= $form->field($model, 'image[]',['template'=>'{label}'.$csp.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[csp][]'])->label('Construction Status Photos') ?>

    <?= $form->field($model, 'image[]',['template'=>'{label}'.$aform.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[form][]'])->label('Allotment Form ') ?>


    <?= $form->field($model, 'image[]',['template'=>'{label}'.$bip.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*', 'name'=>'image[banner][]'])->label('Banner Image') ?>


    <?= $form->field($model, 'image[]',['template'=>'{label}{input}{error}'])->textInput([ 'name'=>'image[video][]','value'=>(isset($video[0])?$video[0]:"")])->label('Walkthrough Video ') ?>


    <?= $form->field($model, 'facilities')->textarea() ?>

    <?php

        $model->approvals=explode(",",$model->approvals);
    ?>
    <?= $form->field($model, 'approvals')->checkboxList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\GovtApprovals::findAll(["is_active"=>"1"]),'approvals','approvals'),["class"=>"approvals"]) ?>
    <?php

    $model->loan=explode(",",$model->loan);

    //$form->fieldConfig['horizontalCheckboxTemplate']='<div class=\"col-md-4\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>';

    ?>

    <style>

    </style>
    <div class="row">
    <?= $form->field($model, 'loan')
        ->checkboxList(
            \yii\helpers\ArrayHelper::map(\app\modules\realty\models\BankLoans::findAll(["is_active"=>"1"]),'loan','loan')
        ,['item' =>
                function ($index, $label, $name, $checked, $value) {
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => "<span class='col-md-9'>". $label."</span>" ,
                        'labelOptions' => [
                            'class' => 'ckbox ckbox-primary col-md-4',
                        ],
                        //'id' => $label,
                    ]);
                },

        ]) ?>
    </div>
    <? //= $form->field($model, 'image[]',['template'=>'{label}'.$doc.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*','name'=>'image[doc][]'])->label('List of documents ') ?>


    <?= $form->field($model, 'list_of_doc')->textarea() ?>


    <? //= $form->field($model, 'start_price')->textInput() ?>
    <?/*= $form->field($model, 'loan')->widget(\kartik\switchinput\SwitchInput::classname(), [
            'pluginOptions'=>[
                'handleWidth'=>60,
                'onText'=>'Yes',
                'offText'=>'NO'
            ]]
    ); */?>

    <?= $form->field($model, 'featured')->widget(\kartik\switchinput\SwitchInput::classname(), [
            'pluginOptions'=>[
                'handleWidth'=>60,
                'onText'=>'Yes',
                'offText'=>'NO'
            ]]
    ); ?>





    <?/*= $form->field($model, 'year_of_construction')->textInput() */?>






    <? //= $form->field($model, 'master_plan')->fileInput() ?>




    <!--begin:  address-->

    <?php
    $ha=$model->getAddress()->one();
    if(($ha)){
        $model->adrs->setAttributes($ha->getAttributes());
        $model->adrs->id=$ha->id;
    }
    $hatel=$model->adrs->getTelephone()->one();
    if(empty($hatel)){
        $hatel=new \app\models\Telephone();
        $model->tel=$hatel;
    }else {
        $model->tel = new \app\models\Telephone();
        $model->tel->setAttributes($hatel->getAttributes());
        $model->tel->id=$hatel->id;
    }
    ?>
    <div class=" panel panel-default ">

        <div class="panel-heading"> In-charge person details </div>
        <div class="panel-body">

            <?= $form->field($model, 'incharge_name')->textInput() ?>
            <?= $form->field($model, 'designation')->textInput() ?>
            <?= $form->field($model, 'site_contact_email')->textInput() ?>
            <?= $form->field($model->tel,'primary_landline')->textInput(['name'=>'Telephone[primary_landline]']) ?>
            <?= $form->field($model->tel,'secondary_landline')->textInput(['name'=>'Telephone[secondary_landline]']) ?>
            <?= $form->field($model->tel,'primary_mobile')->textInput(['name'=>'Telephone[primary_mobile]']) ?>
            <?= $form->field($model->tel,'secondary_mobile')->textInput(['name'=>'Telephone[secondary_mobile]']) ?>

        </div>


        <div class="panel-heading">  Address Details </div>
        <div class="panel-body">

            <?= $form->field($model->adrs, 'adrs_line1')->textarea() ?>
            <div style="display: none">
            <?= $form->field($model->adrs, 'adrs_line2')->textarea() ?>
            </div>

            <?= $form->field($model->adrs, 'city')->
            widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/jsonautocomplete']).'?q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    //console.log(query);
                                                    console.log(settings);

                                                    settings.url=settings.url.replace(/%QUERY/,$('#address-city').val())
                                                    settings.url=settings.url+'&state='+$('#address-state').val()+'&country='+$('#address-country').val()
                                                    return settings;

                                                }"),
                                /*'ajax' => ['complete' => new \yii\web\JsExpression("function(response){jQuery('#serial_product').removeClass('loading');checkresult(response.responseText);return true}")]*/

                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'address-city','readonly'=>"readonly"],
                ]) ?>

            <?= $form->field($model->adrs, 'postal_code')->textInput() ?>


            <?= $form->field($model->adrs, 'state')->textInput(["id"=>'address-state']);
           /* widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/state/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY',
                                'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                    settings.url=settings.url.replace(/%QUERY/,$('#address-state').val())
                                                    settings.url=settings.url+'&country='+$('#address-country').val()
                                                    return settings;

                                                }"),
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => [],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>


            <?= $form->field($model->adrs, 'country')->textInput( ["id"=>'address-country']);
           /*widget(\kartik\typeahead\Typeahead::className(),[
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                            'remote' => [
                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/country/jsonautocomplete']).'&q=%QUERY',
                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginOptions' => ['highlight' => true],
                    'options' => ["id"=>'address-country'],
                    //'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                ])*/ ?>








        </div>
        <button type="button" class="btn btn-green" onclick="getadrs()">Get Lat and Long</button>
        <?= $form->field($model, 'lat')->textInput() ?>
        <?= $form->field($model, 'long')->textInput() ?>
        <script>
            function getadrs(){
                var address='';
                address+=$("#address-adrs_line1").val()+" ";
                address+=$("#address-city").val()+" ";
                address+=$("#address-state").val()+" ";
                address+=$("#address-country").val()+" ";
                //alert(address);
                findAddress(address);
            }
        </script>


    </div>
    <!--end:  address-->
    <?php
    $files= explode("^m",$model->image);
    foreach($files as $file){
        if(!empty($file)){
            $baseurl=Yii::$app->urlManager->baseUrl;
            echo Html::a($file,$baseurl."/uploads/".$file);
            echo "<br/>";
        }
    }

    ?>




    <? //= $form->field($model, 'image[]',['template'=>'{label}'.$fileanchor.'{input}{error}'])->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>



    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    </div>
    <style>
        #gmap_canvas { height: 500px }
    </style>
    <div class="col-md-6 " style="bottom: 0px;position: absolute;right: 0;min-width:50% ">
        <div id="gmap_canvas">
        </div>
        <script>
            var geocoder;
            var map;
            var markers = Array();
            var infos = Array();
            function initialize() {

                // prepare Geocoder
                geocoder = new google.maps.Geocoder();

                // set initial position (New York)
                var myLatlng = new google.maps.LatLng(13.0300,77.5140);

                var myOptions = { // default map options
                    zoom: 14,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

                google.maps.event.addListener(map, 'click', function(event) {
                    //call function to create marker
                    if (markers) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(null);
                            markers.pop();
                        }
                    }

                    markers.push( createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng));
                });


            }
            window.onLoad=initialize();
            // find address function
            function findAddress(address) {
                //var address = document.getElementById("gmap_where").value;

                // script uses our 'geocoder' in order to find location by address name
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) { // and, if everything is ok

                        // we will center map
                        var addrLocation = results[0].geometry.location;
                        map.setCenter(addrLocation);

                        // store current coordinates into hidden variables
                        console.log(results[0]);
                        //console.log(results[0].geometry.location.lng());
                        var lat=results[0].geometry.location.lat();
                        var lng=results[0].geometry.location.lng();
                        fillLatLontoinp(lat,lng);
                        //document.getElementById('lat').value = results[0].geometry.location.$a;
                        //document.getElementById('lng').value = results[0].geometry.location.ab;

                        // and then - add new custom marker
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat,lng),
                            map: map,
                            title: results[0].formatted_address
                        });
                        /*var addrMarker = new google.maps.Marker({
                            position: addrLocation,
                            map: map,
                            title: results[0].formatted_address,
                            icon: 'marker.png'
                        });*/
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

            function createMarker(latlng, name, html) {
                console.log(latlng);
                var lat=latlng.lat();
                var lng=latlng.lng();
                fillLatLontoinp(lat,lng);
                var contentString = html;
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    zIndex: Math.round(latlng.lat()*-100000)<<5
                });

                /*google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map,marker);
                });
                google.maps.event.trigger(marker, 'click');*/
                return marker;
            }


            function fillLatLontoinp(lat,lon){
                document.getElementById("property-lat").value=lat;
                document.getElementById("property-long").value=lon;
            }
        </script>
    </div>

</div>

