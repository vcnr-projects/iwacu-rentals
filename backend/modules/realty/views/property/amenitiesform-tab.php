<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>true,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


    <!--begin:partition-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Amenities </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Amenity type</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->amenities as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" name="PropUnitParking[id][]" value="<?=$par->id ?>" />
                                <?= $form->field($par, 'amenity',['template'=>'{input}{error}'])
                                    ->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\AmenityType::find()->asArray()->all(),'amenity','amenity')
                                        ,['name'=>'PropertyAmenities[amenity][]'])?>

                            </td>


                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('');
});
JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:parerience-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>