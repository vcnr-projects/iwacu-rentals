<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\PropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(isset($_GET["builder"])){
    $this->title = 'Project';
    //Yii::$app->request->queryParams["mtype"]="Builder";
}else{
    $this->title = 'Properties';
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);
        $txt="Create Property";
        $pathArray=['create',"wiz"=>"wiz"];
        //var_dump($searchModel->mtype);

        if(isset($searchModel->mtype)&&$searchModel->mtype=="Builder"){
            $pathArray=['create',"builder"=>"builder","wiz"=>"wiz"];
        }

    ?>

    <p>
        <?= Html::a($txt, $pathArray, ['class' => 'btn btn-success']) ?>
        <? //= Html::a('Create Property for Builder', ['create',"builder"=>"builder"], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
     Html::dropDownList('',$searchModel->xsearch
         ,["All"=>"All","Featured Properties"=>"Featured Properties","Normal"=>"Normal"]
         ,["onchange"=>"gridupdate(this.value)","class"=>"form-control"])
    ?>

    <script>

        function gridupdate(val){
//alert("Bill");
            // jQuery('#w1').yiiGridView('applyFilter',{"filterSelector":"#w1-filters input, #w1-filters select"});
            //$.pjax.reload({container:'#w1'});
            $("#PropertySearch-xsearch").val(val);
            $.pjax.reload({
                type: 'GET',
                container: "#w1",
                timeout: 2000,
                data: $('#w1-filters  input').serialize()
            });
        }

    </script>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'member_id',
            [
                'attribute'=>'ptype',
                'label'=>'Poperty Type',
                //'filter'=>Html::activeDropDownList($searchModel,'mtype',["All"=>"All","Builder"=>"Builder","Member"=>"Member"],["class"=>"form-control"]),
                'value'=>function($model){
                    return $model->propType->name;
                }
            ],
            'no_units',
             //'no_floors',
            [
                'class'=>'app\components\HiddenDataColumn',
                'attribute'=>'no_floors',
                'label'=>'NO. of Floors',
                'hiddendata'=>['xsearch'],
                'filter'=>false,
            ],
            [
                'attribute'=>'mtype',
                'label'=>'Owner Type',
                'filter'=>Html::activeDropDownList($searchModel,'mtype',["All"=>"All","Builder"=>"Builder","Member"=>"Member"],["class"=>"form-control"]),
                'value'=>function($model){
                    return $model->getMetype();
                }
            ],
            // 'year_of_construction',
            // 'addres_id',

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}{update}',
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
