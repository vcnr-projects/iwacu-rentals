<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\BrandNames */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-names-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'brandname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\InventoryCategory::find()->asArray()->all(),'cat','cat'),["prompt"=>"Select"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
