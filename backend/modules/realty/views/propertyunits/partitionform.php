<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


    <!--begin:partition-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Partitions </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Partition Type</th>
                        <th>Partition Name</th>
                        <th>Area in sqmtr</th>
                        <th>Remarks</th>
                        <th>Photo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(empty($model->partitons)){
                        $model->partitons=[];
                    }
                    $cprop=count($model->partitons);
                    $pprop=$model->no_balconies+$model->no_bathrooms+$model->no_bedrooms+$model->no_servant_room-$cprop;

                    for($i=0;$i<$pprop;$i++){

                        $model->partitons[]=new \app\modules\realty\models\PropAreaPartition();
                    }

                    foreach($model->partitons as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" name="PropAreaPartition[id][]" value="<?=$par->id ?>" />

                                <?php $parcat=explode("_",$model->scenario)[1] ?>
                                <?= $form->field($par, 'area_partition_type',['template'=>'{input}{error}'])
                                    //->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\PropertyPartitionType::find()->andWhere(["category"=>$parcat])->asArray()->all(),'partition_type','partition_type')
                                    ->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\PropertyPartitionType::find()->asArray()->all(),'partition_type','partition_type')
                                        ,['name'=>'PropAreaPartition[area_partition_type][]'])?>
                            </td>
                            <td>
                                <?= $form->field($par, 'name',['template'=>'{input}{error}'])->
                                    /*widget(\kartik\typeahead\Typeahead::className(),[
                                        'dataset' => [
                                            [
                                                'local' => \app\modules\realty\models\PropAreaPartition::find()->select(['distinct `name` as name'])->asArray()->column(),
                                                'limit' => 10
                                            ]
                                        ],
                                        'pluginOptions' => ['highlight' => true],
                                        'options' => ['placeholder' => 'Name','name'=>'PropAreaPartition[name][]',"class"=>"apttype","id"=>'pap'.$i],
                                    ])*/
                                textInput(['name'=>'PropAreaPartition[name][]',"class"=>"apttype form-control"])
                                ?>
                            </td>

                            <td >
                                <?= $form->field($par, 'area',['template'=>'{input}{error}'])->textInput(['name'=>'PropAreaPartition[area][]']) ?>
                            </td>
                            <td >
                                <?= $form->field($par, 'remarks',['template'=>'{input}{error}'])->textArea(['name'=>'PropAreaPartition[remarks][]']) ?>
                            </td>
                            <td class="filetd" >
                                <?php
                                $files= explode("^m",$par->image);
                                foreach($files as $file){
                                    if(!empty($file)){
                                        $baseurl=Yii::$app->urlManager->baseUrl;
                                        echo Html::a($file,$baseurl."/uploads/".$file);
                                        echo "<br/>";
                                    }
                                }

                                ?>
                                <?= $form->field($par, 'image',['template'=>'{input}{error}'])->fileInput(['name'=>'PropAreaPartition[image][]','class'=>'fileattachc','multiple' => true, 'accept' => 'image/*']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $papdata=\app\modules\realty\models\PropAreaPartition::find()->select(['distinct `name` as name'])->asArray()->column();
                $papdata=json_encode($papdata);
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('',pluginactivate);
});
var firstexec=0;

function pluginactivate(tr){

/*if( tr ){
var inp=$("<input name='PropAreaPartition[name][]' class='form-control' />");
$("td:eq(1)",tr).html(inp);
        var propareapartition_name_data_2 = new Bloodhound({"datumTokenizer":Bloodhound.tokenizers.whitespace,"queryTokenizer":Bloodhound.tokenizers.whitespace,"local":$papdata});
        $(inp).typeahead(typeahead_7864e59a,[{"limit":10,"source":propareapartition_name_data_2.ttAdapter()}]);


}*/

    if(firstexec>0){
    $(".filetd",tr).html('');
    var inp=$('<input type="file" name="attachments0" accept="image/*" multiple class="fileattachc" />');
    $(".filetd",tr).html(inp);
    }
    resetAttachName();
    firstexec++;

}

function resetAttachName(){
    $(".fileattachc").each(function(i){
        $(this).attr("name","attachments"+i+"[]");
    });
}

function delrowcallback(){
  resetAttachName();
}



JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:parerience-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>