<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>true,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


    <!--begin:partition-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Inventory   </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Inventory</th>
                        <th>Brand Name</th>
                        <th>Quantity/Area</th>
                        <th>Unit</th>
                        <th>Location</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->inventories as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" name="PropUnitInventory[id][]" value="<?=$par->id ?>" />
                                <?= $form->field($par, 'inventory',['template'=>'{input}{error}'])->
                                    dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\Inventory::find()->asArray()->all()
                                    ,'id'
                                        ,function($model, $defaultValue) {
                                            //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
                                            return $model["name"]." [".$model["cat"]."]";
                                        }
                                        ),[
                                        'prompt'=>'Select',
                                        'onchange'=>'
            var tr=$(this).closest("tr");
             $( "select#propunitinventory-brand",tr ).html( "<option>Loading...</option>" );
                $.post( "'.Yii::$app->urlManager->createUrl('/realty/inventorybrands/brandfrompinventory').'",{"depdrop_parents":$(this).val()}, function( data ) {
                  data=JSON.parse(data);
                  $( "select#propunitinventory-brand",tr ).html( "<option>Select</option>" );
                  for(var i=0;i<data.output.length;i++){
                  var opt=$("<option></option>");
                  $(opt).attr("value",data.output[i][\'id\']);
                  $(opt).text(data.output[i][\'name\']);
                    $( "select#propunitinventory-brand",tr ).append(opt);
                  }
                });
            ',
                                   'name'=>'PropUnitInventory[inventory][]' ]);
                                ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'brand',['template'=>'{input}{error}'])
                                    ->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\InventoryBrands::find()->asArray()->all(),'brand','brand')
                                    ,['prompt'=>'Select','name'=>'PropUnitInventory[brand][]'])?>
                            </td>

                            <td>
                                <?= $form->field($par, 'qty',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInventory[qty][]']) ?>
                            </td>

                            <td>
                                <?= $form->field($par, 'unit',['template'=>'{input}{error}'])->dropDownList(["Nos"=>"Nos","Sqft"=>"Sqft"],['name'=>'PropUnitInventory[unit][]']) ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'location',['template'=>'{input}{error}'])->textArea(['name'=>'PropUnitInventory[location][]']) ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'remarks',['template'=>'{input}{error}'])->textArea(['name'=>'PropUnitInventory[remarks][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('');
});
JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:parerience-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>