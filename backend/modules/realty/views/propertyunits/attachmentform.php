<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>true,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


    <!--begin:partition-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Attachments </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name</th>
                        <th>Attachment Type</th>
                        <th>File</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->attachments as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" name="PropUnitAttach[id][]" value="<?=$par->id ?>" />
                                <?= $form->field($par, 'name',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitAttach[name][]'])
                                ?>
                            </td>
                            <td>

                                <?= $form->field($par, 'attach_type',['template'=>'{input}{error}'])
                                    ->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\AttachmentType::find()->asArray()->all(),'attachment','attachment')
                                    ,['name'=>'PropUnitAttach[attach_type][]'])?>
                            </td>

                            <td >
                                <?php
                                $baseurl=Yii::$app->urlManager->baseUrl;
                                if($par->attach_type){
                                    echo "
                                    <a href='{$baseurl}/uploads/{$par->file_path}'>{$par->file_path}</a>
                                    ";
                                } ?>
                                <?= $form->field($par, 'file_path',['template'=>'{input}{error}'])->fileInput(['name'=>'PropUnitAttach[file_path][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('');
});
JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:parerience-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>