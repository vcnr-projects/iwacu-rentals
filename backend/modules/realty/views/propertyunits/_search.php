<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnitsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-units-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'property_id') ?>

    <?= $form->field($model, 'floor_number') ?>

    <?php // echo $form->field($model, 'block') ?>

    <?php // echo $form->field($model, 'flat_no') ?>

    <?php // echo $form->field($model, 'super_builtup_area') ?>

    <?php // echo $form->field($model, 'carepet_area') ?>

    <?php // echo $form->field($model, 'no_balconies') ?>

    <?php // echo $form->field($model, 'no_bathrooms') ?>

    <?php // echo $form->field($model, 'no_bedrooms') ?>

    <?php // echo $form->field($model, 'no_servant_room') ?>

    <?php // echo $form->field($model, 'furnished_status') ?>

    <?php // echo $form->field($model, 'door_facing') ?>

    <?php // echo $form->field($model, 'two_wheeler_parking_id') ?>

    <?php // echo $form->field($model, 'Four_wheeler_parking_id') ?>

    <?php // echo $form->field($model, 'has_visitor_parking') ?>

    <?php // echo $form->field($model, 'are_pets_allowed') ?>

    <?php // echo $form->field($model, 'other_terms') ?>

    <?php // echo $form->field($model, 'agreement_file_path') ?>

    <?php // echo $form->field($model, 'agreement_type') ?>

    <?php // echo $form->field($model, 'prefered_tenant_type') ?>

    <?php // echo $form->field($model, 'prefered_food_habits') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
