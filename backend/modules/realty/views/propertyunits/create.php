<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnits */

//$this->title = 'Create Property Units';
$this->title = $model->scenarioLabel("Create Property Units");
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-units-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
