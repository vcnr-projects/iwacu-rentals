<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnits */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-units-form">

    <?php $form = \common\models\ActiveScenarioForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <? //= $model->scenario ?>

    <? //= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>




    <div style="display: block">
    <?= $form->field($model, 'property_id')->dropDownList(\yii\helpers\ArrayHelper::map(
       \app\modules\service\models\Property::find()->asArray()->all(),'id','name'
    ));
    ?>
    </div>

    <?= $form->field($model, 'block')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'floor_number')->textInput() ?>
    <?php
      if(!$model->property->member_id) {
          ?>

          <?= $form->field($model, 'available_units')->textInput() ?>

      <?php
      }
    ?>

    <?= $form->field($model, 'flat_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'door_facing')->radioList(["North"=>"North","South"=>"South","West"=>"West","East"=>"East"])?>



    <?= $form->field($model, 'carepet_area',['template'=>"{label}<div class='sqmtr'> In Sqmtr :  ".$model->carepet_area*0.093."</div>{input}{error}"])->textInput(['maxlength' => true,'onkeypress'=>'$(this).closest(".form-group").find(".sqmtr").html("In Sqmtr : "+this.value*0.093.toFixed(3))']) ?>

    <?= $form->field($model, 'builtup_area',['template'=>"{label}<div class='sqmtr'> In Sqmtr :  ".$model->builtup_area*0.093."</div>{input}{error}"])->textInput(['maxlength' => true,'onkeypress'=>'$(this).closest(".form-group").find(".sqmtr").html("In Sqmtr : "+this.value*0.093.toFixed(3))']) ?>


    <?= $form->field($model, 'super_builtup_area',['template'=>"{label}<div class='sqmtr'> In Sqmtr :  ".$model->super_builtup_area*0.093."</div>{input}{error}"])->textInput(['maxlength' => true,'onkeypress'=>'$(this).closest(".form-group").find(".sqmtr").html("In Sqmtr : "+this.value*0.093.toFixed(3))']) ?>


    <? //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'no_balconies')->textInput() ?>

    <?= $form->field($model, 'no_bathrooms')->textInput() ?>

    <?= $form->field($model, 'no_bedrooms')->textInput() ?>

    <?= $form->field($model, 'no_servant_room')->textInput() ?>

   <!-- --><?/*= $form->field($model, 'furnished_status')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Furnished',
            'offText'=>'Unfurnished',
        ]
    ]) */?>
    <?=
    $form->field($model, 'furnished_status')->radioList(["Furnished"=>"Furnished","Semi-Furnished"=>"Semi-Furnished","Un-Furnished"=>"Un-Furnished"])
    ?>



    <?= $form->field($model, 'two_wheeler_parking_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Four_wheeler_parking_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'has_visitor_parking')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <?= $form->field($model, 'are_pets_allowed')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <?= $form->field($model, 'other_terms')->textarea(['rows' => 6]) ?>



    <?= $form->field($model, 'agreement_file_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agreement_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prefered_tenant_type')->radioList(["Bachelor"=>"Bachelor","Family"=>"Family","Any"=>"Any"]) ?>

    <?= $form->field($model, 'prefered_food_habits')->radioList(["Vegetarian"=>"Vegetarian","Non-Vegetarian"=>"Non-Vegetarian"]) ?>


    <?php
    if($model->image) {
        $baseurl = Yii::$app->urlManager->baseUrl;
        echo Html::a($model->image, $baseurl . "/uploads/" . $model->image);
    }
    ?>
    <?=
    $form->field($model, 'image')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*',],
    ]); ?>

    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php \common\models\ActiveScenarioForm::end(); ?>

</div>
