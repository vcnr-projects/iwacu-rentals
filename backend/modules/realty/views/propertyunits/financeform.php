<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var ->finance$model app\modules\realty\models\PropertyUnits */
/* @var $form yii\widgets\ActiveForm */
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="property-units-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>true,'options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model->finance); ?>

    <div class="well"> Rent Details
    <?= $form->field($model->finance, 'is_rent')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <?= $form->field($model->finance, 'min_rent')->textInput(['maxlength' => true]) ?>
    <? //= $form->field($model->finance, 'max_rent')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model->finance, 'fixed_rent')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model->finance, 'min_rent_deposit')->textInput(['maxlength' => true]) ?>
    <? //= $form->field($model->finance, 'max_rent_deposit')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model->finance, 'fixed_rent_deposit')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model->finance, 'yearly_rent_increment')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="well"> Lease Details

        <?= $form->field($model->finance, 'is_lease')->widget(\kartik\widgets\SwitchInput::className(),[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]) ?>
        <?= $form->field($model->finance, 'min_lease')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model->finance, 'max_lease')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model->finance, 'fixed_lease')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model->finance, 'lease_years')->textInput(['maxlength' => true]) ?>

    </div>

    <div class="well">Sale Details
        <?= $form->field($model->finance, 'for_sale')->widget(\kartik\widgets\SwitchInput::className(),[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]) ?>
        <?= $form->field($model->finance, 'starting_price')->textInput() ?>
        <?= $form->field($model->finance, 'available_units')->textInput() ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->finance->isNewRecord ? 'Create' : 'Update', ['class' => $model->finance->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
