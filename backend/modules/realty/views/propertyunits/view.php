<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnits */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-units-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?php $user= $model->getProperty()->one()->getMember()->one();
        if($user) {
            if ($user->user_type == 'Individual') {
                $individual = \app\modules\member\models\Individual::findOne(["member_id" => $user->id]);
                $url = Yii::$app->urlManager->createUrl(["/member/individual/view", 'id' => $individual->id]);
                $ulab = "Back to Individual [{$individual->fname}]";
            }
            if ($user->user_type == 'Company') {
                $individual = \app\modules\member\models\Company::findOne(["member_id" => $user->id]);
                $url = Yii::$app->urlManager->createUrl(["/member/company/view", 'id' => $individual->id]);
                $ulab = "Back to Company [{$individual->fname}]";
            }
            echo Html::a($ulab, $url, ['class' => 'btn btn-primary']);
        }
        ?>

        <?php

        $ulab="Back to Property Group ";
        $url=Yii::$app->urlManager->createUrl(["/realty/property/view",'id'=>$model->getProperty()->one()->id]);

        ?>
        <?= Html::a($ulab, $url, ['class' => 'btn btn-primary']) ?>

        <? //= Html::a('Partitions', ['partitionform', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>


    <?php
    $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
    $actclstxt='class="active"';
    ?>

    <ul class="nav nav-tabs ">
        <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen"><?= explode("_",$model->scenario)[1] ?> Units</a></li>
        <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Partitions</a></li>

        <li <?= ($viewtab=="park")?$actclstxt:"" ?> ><a data-toggle="tab" href="#park">Parking</a></li>
        <li <?= ($viewtab=="int")?$actclstxt:"" ?> ><a data-toggle="tab" href="#int">Specification</a></li>

        <!--        <li><a data-toggle="tab" href="#attach">Attachments</a></li>
-->        <li <?= ($viewtab=="fin")?$actclstxt:"" ?> ><a data-toggle="tab" href="#fin">Financial Details</a></li>
     <!--   <li><a data-toggle="tab" href="#inv">Inventory Details</a></li>-->

    </ul>

    <div class="tab-content">

        <!--begin:partition unit-->
        <div id="gen" class="tab-pane fade in   <?= ($viewtab=="gen")?"active":"" ?> ">
            <h3><?= explode("_",$model->scenario)[1] ?> Unit Detail </h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/update","id"=>$model->id]) ?>" >Edit</a>

            <?php
            $tenant=$model->getTenant()->one();
            $executive=$model->getExecutive()->one();

            $attr=[

                [
                    'label'=>'Property Name',
                    'attribute'=>'property.name',
                ],
                'block',
                'floor_number',
                'available_units',
                'starting_price',
                'flat_no',
                'door_facing',
                [
                    'attribute'=>'carepet_area',
                    'value'=>$model->carepet_area."(".$model->carepet_area*0.093." sqmtr)",
                ],
                [
                    'attribute'=>'builtup_area',
                    'value'=>$model->builtup_area."(".$model->builtup_area*0.093." sqmtr)",
                ],
                [
                    'attribute'=>'super_builtup_area',
                    'value'=>$model->super_builtup_area."(".$model->super_builtup_area*0.093." sqmtr)",
                ],
                //'description:ntext',
                'no_balconies',
                'no_bathrooms',
                'no_bedrooms',
                'no_servant_room',
                [
                    'attribute'=>'furnished_status',
                    'value'=>($model->furnished_status),
                ],
                [
                    'attribute'=>'has_visitor_parking',
                    'value'=>($model->has_visitor_parking)?"Yes":"NO",
                ],
                [
                    'attribute'=>'are_pets_allowed',
                    'value'=>($model->are_pets_allowed)?"Yes":"NO",
                ],
                //'other_terms:ntext',
                /*'agreement_file_path',
                'agreement_type',*/
                'prefered_tenant_type',
                'prefered_food_habits',
                [
                    //'attribute'=>'adrsProofpath',
                    'attribute'=>"image",
                    'format'=>'html',
                    'value'=>($model->image)?Html::a("Photo",Yii::$app->urlManager->baseUrl."/uploads/".$model->image,["target"=>"_blank"]):"",
                ],
                [
                    'attribute'=>'is_active',
                    'value'=>($model->is_active)?"Yes":"NO",
                ],
                /*[
                    //'attribute'=>'tenant.fname',
                    'value'=> ($tenant)? Html::a($tenant->fname,Yii::$app->urlManager->createUrl(["/member/tenant/view",'id'=>$tenant->id])):"",

                    'format'=>'Html',
                    'label'=>'Tenant Name',
                ],
                [
                    //'attribute'=>'tenant.fname',
                    'value'=> ($executive)? Html::a($executive->fname,Yii::$app->urlManager->createUrl(["/member/executive/view",'id'=>$executive->id])):"",

                    'format'=>'Html',
                    'label'=>'Executive Name',
                ],*/
            ];

            if($model->property->member_id){
               $attr= [

                    [
                        'label'=>'Property Name',
                        'attribute'=>'property.name',
                    ],
                    'block',
                    'floor_number',

                    'starting_price',
                    'flat_no',
                    'door_facing',
                    [
                        'attribute'=>'carepet_area',
                        'value'=>$model->carepet_area."(".$model->carepet_area*0.093." sqmtr)",
                    ],
                    [
                        'attribute'=>'builtup_area',
                        'value'=>$model->builtup_area."(".$model->builtup_area*0.093." sqmtr)",
                    ],
                    [
                        'attribute'=>'super_builtup_area',
                        'value'=>$model->super_builtup_area."(".$model->super_builtup_area*0.093." sqmtr)",
                    ],
                    //'description:ntext',
                    'no_balconies',
                    'no_bathrooms',
                    'no_bedrooms',
                    'no_servant_room',
                    [
                        'attribute'=>'furnished_status',
                        'value'=>($model->furnished_status),
                    ],
                    [
                        'attribute'=>'has_visitor_parking',
                        'value'=>($model->has_visitor_parking)?"Yes":"NO",
                    ],
                    [
                        'attribute'=>'are_pets_allowed',
                        'value'=>($model->are_pets_allowed)?"Yes":"NO",
                    ],
                    //'other_terms:ntext',
                    /*'agreement_file_path',
                    'agreement_type',*/
                    'prefered_tenant_type',
                    'prefered_food_habits',
                    [
                        //'attribute'=>'adrsProofpath',
                        'attribute'=>"image",
                        'format'=>'html',
                        'value'=>($model->image)?Html::a("Photo",Yii::$app->urlManager->baseUrl."/uploads/".$model->image,["target"=>"_blank"]):"",
                    ],
                    [
                        'attribute'=>'is_active',
                        'value'=>($model->is_active)?"Yes":"NO",
                    ],
                    /*[
                        //'attribute'=>'tenant.fname',
                        'value'=> ($tenant)? Html::a($tenant->fname,Yii::$app->urlManager->createUrl(["/member/tenant/view",'id'=>$tenant->id])):"",

                        'format'=>'Html',
                        'label'=>'Tenant Name',
                    ],
                    [
                        //'attribute'=>'tenant.fname',
                        'value'=> ($executive)? Html::a($executive->fname,Yii::$app->urlManager->createUrl(["/member/executive/view",'id'=>$executive->id])):"",

                        'format'=>'Html',
                        'label'=>'Executive Name',
                    ],*/
                ];
            }

            ?>
            <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attr
    ]) ?>

        </div>
        <!--end:partition unit-->


        <!-- begin :Partition -->
        <div id="off" class="tab-pane fade in  <?= ($viewtab=="off")?"active":"" ?>  ">
            <h3>Partition Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/partitionform","id"=>$model->id]) ?>" >Edit</a>

            <div class="well">
        <h3>Partitions</h3>
                <?php
                //$model->partitons=$model->getPropertyUnits()->all();

                if(count($model->partitons)) {
                    $proptype = $model->getProperty()->one()->getPropType()->one()->name;
                    if ($model->no_balconies+$model->no_bathrooms+$model->no_bedrooms+$model->no_servant_room > count($model->partitons)) {
                        $cprop=count($model->partitons);
                        $pprop=$model->no_balconies+$model->no_bathrooms+$model->no_bedrooms+$model->no_servant_room-$cprop;
                        $tprop=$model->no_balconies+$model->no_bathrooms+$model->no_bedrooms+$model->no_servant_room;

                        echo "
                <div class='alert-danger'>
                   Total No. of Partition Units : {$tprop} <br/>
                   Created No. of  Partition Units : {$cprop} <br/>
                   Pending No. of  Partition Units : {$pprop} <br/>
                </div>
                ";
                    }
                }
                ?>
        <table  class="table">
            <thead>
            <tr>
                <th>SL No.</th>
                <th>Partition Type</th>
                <th>Partition Description</th>

                <th>Area in sqmtr</th>
                <th>Remarks</th>
                <th>Images</th>
                <th>Inventory Details</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($model->partitons as $i=> $par){
                ?>
                <tr>
                    <td><?= $i+1 ?></td>
                    <td><?= $par->area_partition_type ?></td>
                    <td><?= $par->name ?></td>

                    <td><?= $par->area ?></td>
                    <td><?= $par->remarks ?></td>
                    <td><?php
                        $files= explode("^m",$par->image);
                        foreach($files as $file){
                            if(!empty($file)){
                                $baseurl=Yii::$app->urlManager->baseUrl;
                                echo Html::a($file,$baseurl."/uploads/".$file);
                                echo "<br/>";
                            }
                        }

                        ?></td>
                    <td> <span data-toggle="collapse" data-target="#partition<?=$i ?>" class="glyphicon glyphicon-collapse-down"></span></td>
                </tr>
                <!--begin partition inventory-->
                <tr id="partition<?=$i ?>"  class="collapse">
                   <td colspan="10">
                       <div class="well">
                           <h3>Inventory Details</h3>
                           <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propareapartition/inventoryform","id"=>$par->id]) ?>" >Edit</a>
                           <table  class="table">
                               <thead>
                               <tr>
                                   <th>SL No.</th>
                                   <th>Inventory</th>
                                   <th>Brand Name</th>
                                   <th>Quantity/Area</th>
                                   <th>Unit</th>
                                   <th>Location</th>
                                   <th>Remarks</th>
                               </tr>
                               </thead>
                               <tbody>

                               <?php
                               $par->inventories=$par->getPropUnitInventories()->all();
                               foreach($par->inventories as $j=> $par1){
                                   ?>
                                   <tr>
                                       <td><?= $j+1 ?></td>
                                       <td><?= $par1->getInventory0()->one()->name ?></td>
                                       <td><?= $par1->brand ?></td>
                                       <td><?= $par1->qty ?></td>
                                       <td><?= $par1->unit ?></td>
                                       <td><?= $par1->location ?></td>
                                       <td><?= $par1->remarks ?></td>
                                   </tr>
                               <?php } ?>
                               </tbody>
                           </table>
                       </div>
                   </td>

                </tr>
                <!--end partition inventory-->
            <?php } ?>
            </tbody>
        </table>
    </div>
        </div>
    <!-- end :Partition -->

        <!--begin: parking-->
        <div id="park" class="tab-pane fade in  <?= ($viewtab=="park")?"active":"" ?> ">
            <h3>Parking Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/parkingform","id"=>$model->id]) ?>" >Edit</a>
            <div class="well">
                <h3>Parking</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>No. of Vehicles</th>
                        <th>Vehicle Type</th>
                        <th>Parking Type</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->parkings as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->no_of_vehicles ?></td>
                            <td><?= $par->vehicle_type ?></td>
                            <td><?= $par->parking_type ?></td>
                            <td><?= $par->location ?></td>
                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
            <!--end: parking-->

        <!--begin: attachments-->
        <div id="attach" class="tab-pane fade in   <?= ($viewtab=="attach")?"active":"" ?> ">
            <h3>Attachments</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/attachmentform","id"=>$model->id]) ?>" >Edit</a>
            <div class="well">
                <h3>Attachment Details</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name</th>
                        <th>Attachment Type</th>
                        <th>File</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->attachments as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->name ?></td>
                            <td><?= $par->attach_type ?></td>

                            <td>
                                <?php $baseurl=Yii::$app->urlManager->baseUrl; ?>
                                <?= Html::a($par->file_path,"{$baseurl}/uploads/{$par->file_path}")   ?></td>
                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--end: attachemnts-->


        <!--begin:financne unit-->
        <div id="fin" class="tab-pane fade in   <?= ($viewtab=="fin")?"active":"" ?>  ">
            <h3><?= explode("_",$model->scenario)[1] ?> Finance Detail </h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/financeform","id"=>$model->id]) ?>" >Edit</a>

            <?= DetailView::widget([
                'model' => $model->finance,
                'attributes' => [
                    [
                        'attribute'=>'for_sale',
                        'value'=>($model->finance->for_sale)?"Yes":"NO",
                    ],
                    'starting_price',
                    'available_units',
                    [
                        'attribute'=>'is_rent',
                        'value'=>($model->finance->is_rent)?"Yes":"NO",
                    ],
                    'min_rent',
                    //'max_rent',
                    'fixed_rent',
                    'min_rent_deposit',
                    //'max_rent_deposit',
                    'fixed_rent_deposit',
                    'yearly_rent_increment',
                    [
                        'attribute'=>'is_lease',
                        'value'=>($model->finance->is_lease)?"Yes":"NO",
                    ],
                    'min_lease',
                    'max_lease',
                    'fixed_lease',
                    'lease_years',

                ],
            ]) ?>

        </div>
        <!--end:financne unit-->


        <!--begin: inventory
        <div id="inv" class="tab-pane fade in ">
            <h3>Inventory Details</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/inventoryform","id"=>$model->id]) ?>" >Edit</a>
            <div class="well">
                <h3>Inventory</h3>
                <table  class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Inventory</th>
                        <th>Brand Name</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Location</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($model->inventories as $i=> $par){
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $par->getInventory0()->one()->name ?></td>
                            <td><?= $par->brand ?></td>
                            <td><?= $par->qty ?></td>
                            <td><?= $par->unit ?></td>
                            <td><?= $par->location ?></td>
                            <td><?= $par->remarks ?></td>

                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--end: inventory-->

        <!--begin: interior descriptoin-->
        <div id="int" class="tab-pane fade in  <?= ($viewtab=="int")?"active":"" ?> ">
            <h3> Specification</h3>
            <a href="<?= Yii::$app->urlManager->createUrl(["/realty/propertyunits/interior","id"=>$model->id]) ?>" >Edit</a>
            <div class="row">
                <div class="col-md-3" style="border-right: 1px solid black">
                    <div class="page-heading">Flooring</div>
                    <div class="panel-body">
                        <?php
                            $floorings=$model->getInteriorsFlooring()->all();
                        foreach( $floorings as $i=> $par){
                            echo "<b>{$par->heading}</b>: {$par->description}<br/>";
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-3" style="border-right: 1px solid black">
                    <div class="page-heading">Fitting</div>
                    <div class="panel-body">
                        <?php
                        $floorings=$model->getInteriorsFittings()->all();
                        foreach( $floorings as $i=> $par){
                            echo "<b>{$par->heading}</b>: {$par->description}<br/>";
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-3" style="border-right: 1px solid black">
                    <div class="page-heading">Walls</div>
                    <div class="panel-body">
                        <?php
                        $floorings=$model->getInteriorsWalls()->all();
                        foreach( $floorings as $i=> $par){
                            echo "<b>{$par->heading}</b>: {$par->description}<br/>";
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="page-heading">Additional Facilities</div>
                    <div class="panel-body">
                        <?php
                        $floorings=$model->getAdditionalFacilities()->all();
                        foreach( $floorings as $i=> $par){
                            echo "<b>{$par->heading}</b>: {$par->description}<br/>";
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
             $avail=$model->getAvailability()->one();
            if($avail){
                echo "<b>Availability :</b> {$avail["description"]}";
            }
            ?>

        </div>
        <!--end: interior descriptoin-->



    </div>





</div>
