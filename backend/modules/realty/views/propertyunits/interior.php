<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


    <!--begin:flooring-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Flooring </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Heading</th>
                        <th>Description</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $floorings=$model->getInteriorsFlooring()->all();
                    if(empty($floorings)){
                        $partition=$model->getPropAreaPartitions()->all();
                        foreach($partition as $p){
                            $floor=new \app\modules\realty\models\PropUnitInterior();
                            $floor->heading=$p->area_partition_type;
                            $floor->category="Flooring";
                            $floorings[]=$floor;
                        }
                        if(empty($partition)){

                            $floorings[]=new \app\modules\realty\models\PropUnitInterior();
                            if(isset($_GET['wiz'])){
                                $sql="
                                  select puf.* from  prop_unit_interior puf,property_units pu
                                   where
                                   pu.id=(
                                    select pu.id from property p,address a,property_units pu
                                    where pu.property_id=p.id and p.name=:name and a.city=:city limit 1
                                   )
                                   and puf.prop_unit_id=pu.id
                                   and puf.category='Flooring'

                                ";
                                $cmd=Yii::$app->db->createCommand($sql);
                                $cmd->bindValue(":name",$model->property->name);
                                $cmd->bindValue(":city",$model->property->address->city);
                                $rs1=$cmd->queryAll();
                                if(!empty($rs1)){
                                    $floorings=[];
                                }
                                foreach($rs1 as $rs){
                                    $f=new \app\modules\realty\models\PropUnitInterior();
                                    $f->heading=$rs["heading"];
                                    $f->description=$rs["description"];
                                    $f->category="Flooring";
                                    $floorings[]=$f;
                                }
                            }
                        }
                    }
                    foreach( $floorings as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" class="cat" cat="Flooring" name="PropUnitInterior[category][]" value="<?=$par->category ?>" />
                                <?= $form->field($par, 'heading',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[heading][]'])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'description',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[description][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('',pluginactivate);
});
JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:flooring-->


    <!--begin:fittings-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Fittings </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="fittab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Heading</th>
                        <th>Description</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $fittings=$model->getInteriorsFittings()->all();
                    if(empty($fittings)){
                        $fit=new \app\modules\realty\models\PropUnitInterior();
                        $fit->category="Fittings";
                        $fittings[]=$fit;

                        if(isset($_GET['wiz'])){
                            $sql="
                                  select puf.* from  prop_unit_interior puf,property_units pu
                                   where
                                   pu.id=(
                                    select pu.id from property p,address a,property_units pu
                                    where pu.property_id=p.id and p.name=:name and a.city=:city limit 1
                                   )
                                   and puf.prop_unit_id=pu.id
                                   and puf.category='Fittings'

                                ";
                            $cmd=Yii::$app->db->createCommand($sql);
                            $cmd->bindValue(":name",$model->property->name);
                            $cmd->bindValue(":city",$model->property->address->city);
                            $rs1=$cmd->queryAll();
                            if(!empty($rs1)){
                                $fittings=[];
                            }
                            foreach($rs1 as $rs){
                                $f=new \app\modules\realty\models\PropUnitInterior();
                                $f->heading=$rs["heading"];
                                $f->description=$rs["description"];
                                $f->category="Fittings";
                                $fittings[]=$f;
                            }
                        }

                    }
                    foreach( $fittings as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" class="cat" cat="Fittings" name="PropUnitInterior[category][]" value="<?=$par->category ?>" />
                                <?= $form->field($par, 'heading',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[heading][]'])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'description',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[description][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var fittab;
$(document).ready(function(){
 fittab=$('#fittab').medgridc('',pluginactivate);
});
JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:fittings-->


    <!--begin:walls-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Walls </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="walltab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Heading</th>
                        <th>Description</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $walls=$model->getInteriorsWalls()->all();
                    if(empty($walls)){
                        $wa=new \app\modules\realty\models\PropUnitInterior();
                        $wa->category="Walls";
                        $walls[]=$wa;

                        if(isset($_GET['wiz'])){
                            $sql="
                                  select puf.* from  prop_unit_interior puf,property_units pu
                                   where
                                   pu.id=(
                                    select pu.id from property p,address a,property_units pu
                                    where pu.property_id=p.id and p.name=:name and a.city=:city limit 1
                                   )
                                   and puf.prop_unit_id=pu.id
                                   and puf.category='Walls'

                                ";
                            $cmd=Yii::$app->db->createCommand($sql);
                            $cmd->bindValue(":name",$model->property->name);
                            $cmd->bindValue(":city",$model->property->address->city);
                            $rs1=$cmd->queryAll();
                            if(!empty($rs1)){
                                $walls=[];
                            }
                            foreach($rs1 as $rs){
                                $f=new \app\modules\realty\models\PropUnitInterior();
                                $f->heading=$rs["heading"];
                                $f->description=$rs["description"];
                                $f->category="Flooring";
                                $walls[]=$f;
                            }
                        }

                    }
                    foreach( $walls as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" class="cat" cat="Walls" name="PropUnitInterior[category][]" value="<?=$par->category ?>" />
                                <?= $form->field($par, 'heading',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[heading][]'])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'description',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[description][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var walltab;
$(document).ready(function(){
 walltab=$('#walltab').medgridc('',pluginactivate);

});

function pluginactivate(){
    $(".cat").each(function(){
        $(this).val($(this).attr('cat'));
    });
}

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:walls-->


    <!--begin:addfac-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Additional Facilities </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="addfactab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Heading</th>
                        <th>Description</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql="select amenity from amenity_type WHERE type='Facility' and is_present=1 ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $res=($cmd->queryColumn());
                    ?>
                    <?php
                    $walls=$model->getAdditionalFacilities()->all();
                    if(empty($walls)){
                        $wa=new \app\modules\realty\models\PropUnitInterior();
                        $wa->category="Additional Facilities";
                        $walls[]=$wa;

                        if(isset($_GET['wiz'])){
                            $sql="
                                  select puf.* from  prop_unit_interior puf,property_units pu
                                   where
                                   pu.id=(
                                    select pu.id from property p,address a,property_units pu
                                    where pu.property_id=p.id and p.name=:name and a.city=:city limit 1
                                   )
                                   and puf.prop_unit_id=pu.id
                                   and puf.category='Additional Facilities'

                                ";
                            $cmd=Yii::$app->db->createCommand($sql);
                            $cmd->bindValue(":name",$model->property->name);
                            $cmd->bindValue(":city",$model->property->address->city);
                            $rs1=$cmd->queryAll();
                            if(!empty($rs1)){
                                $walls=[];
                            }
                            foreach($rs1 as $rs){
                                $f=new \app\modules\realty\models\PropUnitInterior();
                                $f->heading=$rs["heading"];
                                $f->description=$rs["description"];
                                $f->category="Flooring";
                                $walls[]=$f;
                            }
                        }


                    }
                    $data=[ 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado'];
                    foreach( $walls as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" class="cat" cat="Additional Facilities" name="PropUnitInterior[category][]" value="<?=$par->category ?>" />
                                <? //= $form->field($par, 'heading',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[heading][]','class'=>"addfachead form-control"]) ?>

                                <?= $form->field($par, 'heading',['template'=>'{input}{error}'])->widget(\yii\jui\AutoComplete::classname(), [
                                    'clientOptions'=>["source"=>$res],
                                    'options' => ['placeholder' => 'Facility','class'=>'form-control addfachead','id'=>"a1",'name'=>'PropUnitInterior[heading][]'],
                                ]); ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'description',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[description][]']) ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                //$sql="select amenity from amenity_type WHERE type='Facility' and is_present=1 ";
                //$cmd=Yii::$app->db->createCommand($sql);
                $res=base64_encode(json_encode($res));

                $script = <<< JS
var addfactab;
var facilities=JSON.parse(atob('$res'));
$(document).ready(function(){
 addfactab=$('#addfactab').medgridc('',pluginactivate3);

});

function pluginactivate3(){
    $(".cat").each(function(){
        $(this).val($(this).attr('cat'));
    });
    //$(".addfachead").typeahead("destroy");

$(".addfachead").autocomplete({source:facilities});

}

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:addfac-->


    <!--begin:addfac-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"> Availability Date </a>
            </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="avaliltab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Heading</th>
                        <th>Date</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $walls=$model->getAvailability()->all();
                    if(empty($walls)){
                        $wa=new \app\modules\realty\models\PropUnitInterior();
                        $wa->category="Availability";
                        $walls[]=$wa;

                    }
                    foreach( $walls as $i=> $par):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <input type="hidden" class="cat" cat="Availability" name="PropUnitInterior[category][]" value="<?=$par->category ?>" />
                                <?= $form->field($par, 'heading',['template'=>'{input}{error}'])->textInput(['name'=>'PropUnitInterior[heading][]','value'=>'Availability','readonly'=>'readonly'])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($par, 'description',['template'=>'{input}{error}'])->widget(\kartik\widgets\DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Enter Availability  date ...'
                                                    , 'name'=>'PropUnitInterior[description][]'
                                        ,"required"=>"required"
                                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd-mm-yyyy',
                                        'todayHighlight'=>true

                                    ]
                                ])
                                 ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
/*
var addfactab;
$(document).ready(function(){
 addfactab=$('#addfactab').medgridc('',pluginactivate3);

});

function pluginactivate3(){
    $(".cat").each(function(){
        $(this).val($(this).attr('cat'));
    });
}
*/

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:availaablity-->




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>