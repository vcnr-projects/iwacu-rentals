<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnits */
/* @var $form yii\widgets\ActiveForm */
?>
<?php


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\PropertyUnits */

$this->title = 'Allocate Tenant : ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-units-update">

    <h1><?= Html::encode($this->title) ?></h1>

<div class="property-units-form">

    <?php $form = \common\models\ActiveScenarioForm::begin(); ?>

    <? //= $model->scenario ?>




    <?= $form->field($model, 'tenant_id')->dropDownList(\yii\helpers\ArrayHelper::map(
       \common\models\User::find()->andFilterWhere(["user_type"=>"Tenant"])->asArray()->all(),'id','username'
    ),["prompt"=>"Select"]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php \common\models\ActiveScenarioForm::end(); ?>

</div>
