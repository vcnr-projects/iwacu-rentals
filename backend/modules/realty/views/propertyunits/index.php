<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\PropertyUnitsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Property Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-units-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <? //= Html::a('Create Property Units', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',[
                'label'=>'Property Name',
                'attribute'=>'pname',
                'value'=>function($model){
                    return $model->property->name;
                }
            ],
            [
                'label'=>'Property Type',
                'attribute'=>'ptype',
                'value'=>function($model){
                    return $model->property->propType->name;
                }
            ],

            //'description:ntext',
            //'id',
            //'property_id',
            //'floor_number',
            // 'block',
            // 'flat_no',
            // 'super_builtup_area',
            // 'carepet_area',
            // 'no_balconies',
            // 'no_bathrooms',
            // 'no_bedrooms',
            // 'no_servant_room',
            // 'furnished_status',
            // 'door_facing',
            // 'two_wheeler_parking_id',
            // 'Four_wheeler_parking_id',
            // 'has_visitor_parking',
            // 'are_pets_allowed',
            // 'other_terms:ntext',
            // 'agreement_file_path',
            // 'agreement_type',
            // 'prefered_tenant_type',
            // 'prefered_food_habits',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}',
            ],
        ],
    ]); ?>

</div>
