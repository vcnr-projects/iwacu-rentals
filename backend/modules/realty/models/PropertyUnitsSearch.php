<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\PropertyUnits;

/**
 * PropertyUnitsSearch represents the model behind the search form about `app\modules\realty\models\PropertyUnits`.
 */
class PropertyUnitsSearch extends PropertyUnits
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'block', 'flat_no', 'door_facing', 'two_wheeler_parking_id', 'Four_wheeler_parking_id', 'other_terms', 'agreement_file_path',
                'agreement_type', 'prefered_tenant_type', 'prefered_food_habits','pname','ptype'], 'safe'],
            [['id', 'property_id', 'floor_number', 'no_balconies', 'no_bathrooms', 'no_bedrooms', 'no_servant_room', 'furnished_status', 'has_visitor_parking', 'are_pets_allowed'], 'integer'],
            [['super_builtup_area', 'carepet_area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertyUnits::find();
        $query->joinWith(['pType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'property_id' => $this->property_id,
            'floor_number' => $this->floor_number,
            'super_builtup_area' => $this->super_builtup_area,
            'carepet_area' => $this->carepet_area,
            'no_balconies' => $this->no_balconies,
            'no_bathrooms' => $this->no_bathrooms,
            'no_bedrooms' => $this->no_bedrooms,
            'no_servant_room' => $this->no_servant_room,
            'furnished_status' => $this->furnished_status,
            'has_visitor_parking' => $this->has_visitor_parking,
            'are_pets_allowed' => $this->are_pets_allowed,
        ]);

        $query->andFilterWhere(['like', 'property_units.name', $this->name])
            ->andFilterWhere(['like', 'property_units.description', $this->description])
            ->andFilterWhere(['like', 'property_units.block', $this->block])
            ->andFilterWhere(['like', 'property_units.flat_no', $this->flat_no])
            ->andFilterWhere(['like', 'property_units.door_facing', $this->door_facing])
            ->andFilterWhere(['like', 'property_units.two_wheeler_parking_id', $this->two_wheeler_parking_id])
            ->andFilterWhere(['like', 'property_units.Four_wheeler_parking_id', $this->Four_wheeler_parking_id])
            ->andFilterWhere(['like', 'property_units.other_terms', $this->other_terms])
            ->andFilterWhere(['like', 'property_units.agreement_file_path', $this->agreement_file_path])
            ->andFilterWhere(['like', 'property_units.agreement_type', $this->agreement_type])
            ->andFilterWhere(['like', 'property_units.prefered_tenant_type', $this->prefered_tenant_type])
            ->andFilterWhere(['like', 'property_units.prefered_food_habits', $this->prefered_food_habits])
            ->andFilterWhere(['like', 'property.name', $this->pname])
            ->andFilterWhere(['like', 'property_type.name', $this->ptype]);

        $dataProvider->sort->attributes['pname'] = [
            'asc' => ['property.name' => SORT_ASC],
            'desc' => ['property.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['ptype'] = [
            'asc' => ['property_type.name' => SORT_ASC],
            'desc' => ['property_type.name' => SORT_DESC],
        ];

        return $dataProvider;
    }
}
