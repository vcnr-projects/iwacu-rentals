<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "brand_names".
 *
 * @property string $brandname
 * @property string $category
 * @property integer $id
 *
 * @property InventoryCategory $category0
 */
class BrandNames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand_names';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brandname'], 'required'],
            [['brandname'], 'string', 'max' => 200],
            [['category'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'brandname' => 'Brand Name',
            'category' => 'Category',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(InventoryCategory::className(), ['cat' => 'category']);
    }
}
