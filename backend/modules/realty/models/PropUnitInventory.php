<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_inventory".
 *
 * @property string $id
 * @property integer $inventory
 * @property string $prop_unit_id
 * @property string $remarks
 * @property string $qty
 * @property string $created_on
 * @property string $unit
 * @property string $brand
 * @property string $location
 *
 * @property PropertyUnits $propUnit
 * @property Inventory $inventory0
 */
class PropUnitInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory', 'prop_unit_id', 'qty', 'brand'], 'required'],
            [['inventory', 'prop_unit_id'], 'integer'],
            [['remarks', 'location'], 'string'],
            [['qty'], 'number'],
            [['created_on'], 'safe'],
            [['unit'], 'string', 'max' => 20],
            [['brand'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory' => 'Inventory',
            'prop_unit_id' => 'Property Unit ID',
            'remarks' => 'Remarks',
            'qty' => 'Quantity',
            'created_on' => 'Create On',
            'unit' => 'Unit',
            'brand' => 'Brand Name',
            'location' => 'Location',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory0()
    {
        return $this->hasOne(Inventory::className(), ['id' => 'inventory']);
    }
}
