<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\InventoryBrands;

/**
 * InventoryBrandsSearch represents the model behind the search form about `app\modules\realty\models\InventoryBrands`.
 */
class InventoryBrandsSearch extends InventoryBrands
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inventory'], 'integer'],
            [['brand','inventoryName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryBrands::find();
        $query->joinWith('inventory0');
        $query->select(['inventory_brands.*','inventory.name as inventoryName']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'brand',
                'inventoryName' => [
                    'asc' => ['inventory.name' => SORT_ASC],
                    'desc' => ['inventory.name' => SORT_DESC],
                    'label' => 'Inventory Name'
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inventory' => $this->inventory,
        ]);

        $query->andFilterWhere(['like', 'brand', $this->brand]);
        $query->andFilterWhere(['like', 'inventory.name', $this->inventoryName]);

        return $dataProvider;
    }
}
