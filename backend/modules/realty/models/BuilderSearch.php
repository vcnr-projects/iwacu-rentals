<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\Builder;

/**
 * BuilderSearch represents the model behind the search form about `app\modules\realty\models\Builder`.
 */
class BuilderSearch extends Builder
{

    public $xsearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'email', 'url', 'about_developer', 'logo','xsearch'], 'safe'],
            [['address'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Builder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'address' => $this->address,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'about_developer', $this->about_developer])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        if($this->xsearch=="Associate"){
            $query->andFilterWhere([ 'associate'=> 1]);
        }
        if($this->xsearch=="Non-Associate"){
            $query->andFilterWhere([ 'associate'=> 0]);
        }

        return $dataProvider;
    }
}
