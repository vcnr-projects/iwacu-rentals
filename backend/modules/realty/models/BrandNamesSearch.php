<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\BrandNames;

/**
 * BrandNamesSearch represents the model behind the search form about `app\modules\realty\models\BrandNames`.
 */
class BrandNamesSearch extends BrandNames
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brandname', 'category'], 'safe'],
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrandNames::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'brandname', $this->brandname])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
}
