<?php

namespace app\modules\realty\models;

use app\models\Address;
use app\models\Telephone;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "properties".
 *
 * @property string $id
 * @property string $name
 * @property integer $prop_type
 * @property integer $no_units
 * @property integer $no_floors
 * @property integer $year_of_construction
 * @property string $address_id
 * @property integer $tot_units
 * @property string $site_contact_email
 * @property string $site_contact_no
 * @property string $image
 * @property string $lat
 * @property string $long
 * @property string $incharge_name
 * @property string $designation
 * @property string $builder_name
 *
 * @property PropertyType $propType
 * @property Address $address
 */
class Properties extends \yii\db\ActiveRecord
{
    public $propGroup;
    public $adrs;
    public $tel;
    public $amenities;
    public $accessibilities;
    public $properties;
    public $pics=[];


    public function __construct(){
        $this->adrs = new Address();
        $this->tel = new Telephone();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [[ 'no_units', 'no_floors', 'year_of_construction', 'address_id', 'tot_units'], 'integer'],
            [['prop_type'], 'integer','message'=>'Property Type is must'],
            [['image'], 'string'],
            [['name', 'incharge_name', 'builder_name'], 'string', 'max' => 200],
            [['site_contact_email'], 'string', 'max' => 255],
            [['site_contact_no'], 'string', 'max' => 20],
            [['lat', 'long'], 'string', 'max' => 30],
            [['designation'], 'string', 'max' => 100],

            [['tot_proj_area', 'loan','featured'], 'integer'],
            [['image', 'approvals'], 'string'],
            [['start_price', 'area_range'], 'number'],
            [['proj_start_date', 'poss_date'], 'safe'],
            [['master_plan'], 'file'],
            [['proj_status'], 'string', 'max' => 100],



        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'prop_type' => 'Property Type',
            'no_units' => 'No. of Units to be Managed',
            'no_floors' => 'No. of Floors',
            'year_of_construction' => 'Year of Construction',
            'address_id' => 'Address ',
            'tot_units' => 'Total No. of Units',
            'site_contact_email' => 'On-site Contact Email ID',
            'site_contact_no' => 'On-site Contact No. ',
            'image' => 'Photos',
            'lat' => 'Latitude',
            'long' => 'Longitude',
            'incharge_name' => 'In-charge Name',
            'designation' => 'In-charge designation',
            'builder_name' => 'Builder Name',

            'proj_status' => 'Project Status',
            'tot_proj_area' => 'Total Project Area in acres',
            'start_price' => 'Price starting form',
            'area_range' => 'Area range',
            'approvals' => 'Approvals',
            'loan' => 'Loans',
            'proj_start_date' => 'Project Start Date',
            'poss_date' => 'Possession handing over date',
            'master_plan' => 'Master Plan',
            'featured' => 'Featured',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropType()
    {
        return $this->hasOne(PropertyType::className(), ['id' => 'prop_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertiesPics()
    {
        return $this->hasMany(PropertiesPics::className(), ['prop_id' => 'id']);
    }



    public  function getfadrs(){

        $adrs=$this->getAddress()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->poss_date)!==strtotime('01-01-1970')){
                $this->poss_date=date('Y-m-d',strtotime($this->poss_date));
            }else{
                $this->poss_date=null;
            }

            if(strtotime($this->proj_start_date)!==strtotime('01-01-1970')){
                $this->proj_start_date=date('Y-m-d',strtotime($this->proj_start_date));
            }else{
                $this->proj_start_date=null;
            }


            return true;
        } else {
            return false;
        }
    }


    public function afterFind(){

        $this->propGroup=$this->getPropType()->one()->group_id;

        if(strtotime($this->poss_date)==strtotime('01-01-1970')){
            $this->poss_date=null;

        }else{
            $this->poss_date=date('d-m-Y',strtotime($this->poss_date));
        }
        if(strtotime($this->proj_start_date)==strtotime('01-01-1970')){
            $this->proj_start_date=null;

        }else{
            $this->proj_start_date=date('d-m-Y',strtotime($this->proj_start_date));
        }

        parent::afterFind ();

    }

    public function saver(){

        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {


            //begin: adrs
            Yii::trace(VarDumper::dumpAsString($this->adrs),'vardump');
            $hadrs=Address::findOne($this->address_id);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$this->adrs->adrs_line1,
                'adrs_line2'=>$this->adrs->adrs_line2,
                'city'=>$this->adrs->city,
                'state'=>$this->adrs->state,
                'country'=>$this->adrs->country,
                'postal_code'=>$this->adrs->postal_code,
            ]);

            /*$htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$this->tel->primary_landline,
                'primary_mobile'=>$this->tel->primary_mobile,
                'secondary_landline'=>$this->tel->secondary_landline,
                'secondary_mobile'=>$this->tel->secondary_mobile,
            ]);

            $this->tel=$htel;
            if (!$this->tel->save()) {
                foreach($this->tel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                $hadrs->telephone_id=$this->tel->id;
            }*/




            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $this->adrs = $hadrs;
                $this->address_id = $this->adrs->id;
            }

            //end: adrs



            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{

                if(!empty($this->pics)){
                    PropertiesPics::deleteAll(["prop_id"=>$this->id]);
                    Yii::trace(VarDumper::dumpAsString(count($this->pics)),'vardump');

                    foreach($this->pics as $pic){

                        $pic->prop_id=$this->id;
                        if(!$pic->save()){
                            foreach($pic->getErrors() as $msg)
                                $errmsg.=join("<br/>",$msg);
                        }
                    }
                }

            }

            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;
    }
}
