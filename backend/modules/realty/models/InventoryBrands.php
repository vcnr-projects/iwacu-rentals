<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "inventory_brands".
 *
 * @property string $id
 * @property integer $inventory
 * @property string $brand
 *
 * @property Inventory $inventory0
 */
class InventoryBrands extends \yii\db\ActiveRecord
{

    public $inventoryName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory', 'brand'], 'required'],
            [['inventory'], 'integer'],
            [['brand'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory' => 'Inventory',
            'inventoryName' => 'Inventory Name',
            'brand' => 'Brand Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory0()
    {
        return $this->hasOne(Inventory::className(), ['id' => 'inventory']);
    }

   /* public function getInventoryName()
    {
        return $this->hasMany(Inventory::className(), ['id' => 'inventory'])->one()->name;
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryBrands()
    {
        return $this->hasMany(InventoryBrands::className(), ['inventory' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnitInventories()
    {
        return $this->hasMany(PropUnitInventory::className(), ['inventory' => 'id']);
    }

}
