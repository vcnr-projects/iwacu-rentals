<?php

namespace app\modules\realty\models;

use app\models\Address;
use app\models\Telephone;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "builder".
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $url
 * @property string $about_developer
 * @property string $logo
 * @property string $address
 *
 * @property Address $address0
 */
class Builder extends \yii\db\ActiveRecord
{
    public  $adrs;
    public  $tel;

    public function __construct(){
        $this->adrs = new Address();
        $this->tel = new Telephone();
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'builder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id', 'email', 'address'], 'required'],
            [['id', 'address'], 'required'],
            [['about_developer'], 'string'],
            [['address','associate'], 'integer'],
            [['logo','banner_img'], 'file'],
            [['email'], 'email'],
            [['id', 'name', 'email', 'url'], 'string', 'max' => 200],
            //[['logo'], 'required','on'=>'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Name',
            'name' => 'Name',
            'email' => 'Email ID',
            'url' => 'URL',
            'about_developer' => 'About Developer',
            'logo' => 'Logo',
            'address' => 'Address',
            'banner_img' => 'Banner Image',
            'associate' => 'Is Associate?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress0()
    {
        return $this->hasOne(Address::className(), ['id' => 'address']);
    }


    public  function getfadrs(){

        $adrs=$this->getAddress0()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }

    public function saver(){

        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {


            //begin: adrs
            Yii::trace(VarDumper::dumpAsString($this->adrs),'vardump');
            $hadrs=Address::findOne($this->address);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$this->adrs->adrs_line1,
                'adrs_line2'=>$this->adrs->adrs_line2,
                'city'=>$this->adrs->city,
                'state'=>$this->adrs->state,
                'country'=>$this->adrs->country,
                'postal_code'=>$this->adrs->postal_code,
            ]);

            $htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$this->tel->primary_landline,
                'primary_mobile'=>$this->tel->primary_mobile,
                'secondary_landline'=>$this->tel->secondary_landline,
                'secondary_mobile'=>$this->tel->secondary_mobile,
            ]);

            $this->tel=$htel;
            if (!$this->tel->save()) {
                /*foreach($this->tel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);*/
            }else{
                $hadrs->telephone_id=$this->tel->id;
            }


            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $this->adrs = $hadrs;
                $this->address = $this->adrs->id;
            }

            //end: adrs



            if(!$this->save()){
                /*foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);*/
                $errmsg.=" ";
            }

            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;
    }



}
