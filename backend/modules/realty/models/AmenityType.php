<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "amenity_type".
 *
 * @property string $amenity
 * @property integer $is_present
 * @property string $created_on
 * @property PropertyAmenities[] $propertyAmenities
 */
class AmenityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'amenity_type';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['amenity', 'is_present'], 'required'],
            [['is_present'], 'integer'],
            [['created_on'], 'safe'],
            [['icon'], 'file'],
            [['amenity'], 'unique'],
            [['amenity','type'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'amenity' => 'Amenity / Facility',
            'is_present' => 'Is Active?',
            'created_on' => 'Created On',
            'type' => 'Type',
            'icon' => 'Icon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAmenities(){
        return $this->hasMany(PropertyAmenities::className(), ['amenity' => 'amenity']);
    }
}
