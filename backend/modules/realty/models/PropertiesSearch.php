<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\Properties;

/**
 * PropertiesSearch represents the model behind the search form about `app\modules\realty\models\Properties`.
 */
class PropertiesSearch extends Properties
{
    public $xsearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prop_type', 'no_units', 'no_floors', 'year_of_construction', 'address_id', 'tot_units'], 'integer'],
            [['name', 'site_contact_email', 'site_contact_no', 'image', 'lat', 'long', 'incharge_name', 'designation', 'builder_name','xsearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Properties::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'prop_type' => $this->prop_type,
            'no_units' => $this->no_units,
            'no_floors' => $this->no_floors,
            'year_of_construction' => $this->year_of_construction,
            'address_id' => $this->address_id,
            'tot_units' => $this->tot_units,
        ]);



        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'site_contact_email', $this->site_contact_email])
            ->andFilterWhere(['like', 'site_contact_no', $this->site_contact_no])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'long', $this->long])
            ->andFilterWhere(['like', 'incharge_name', $this->incharge_name])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'builder_name', $this->builder_name]);

        if($this->xsearch=="Featured Properties"){
            $query->andFilterWhere([ 'featured'=> 1]);
        }

        return $dataProvider;
    }
}
