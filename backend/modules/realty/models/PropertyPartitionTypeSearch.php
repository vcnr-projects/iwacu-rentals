<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\PropertyPartitionType;

/**
 * PropertyPartitionTypeSearch represents the model behind the search form about `app\modules\realty\models\PropertyPartitionType`.
 */
class PropertyPartitionTypeSearch extends PropertyPartitionType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partition_type', 'category'], 'safe'],
            [['is_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertyPartitionType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'partition_type', $this->partition_type])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
}
