<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\PropAreaPartition;

/**
 * PropAreaPartitionSearch represents the model behind the search form about `app\modules\realty\models\PropAreaPartition`.
 */
class PropAreaPartitionSearch extends PropAreaPartition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'punit_id'], 'integer'],
            [['area_partition_type', 'remarks', 'name', 'image'], 'safe'],
            [['area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropAreaPartition::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'punit_id' => $this->punit_id,
            'area' => $this->area,
        ]);

        $query->andFilterWhere(['like', 'area_partition_type', $this->area_partition_type])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
