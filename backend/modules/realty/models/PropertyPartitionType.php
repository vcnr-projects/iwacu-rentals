<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "property_partition_type".
 *
 * @property string $partition_type
 * @property integer $is_active
 * @property string $category
 *
 * @property PropAreaPartition[] $propAreaPartitions
 */
class PropertyPartitionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_partition_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partition_type'], 'required'],
            [['is_active'], 'integer'],
            [['partition_type'], 'string', 'max' => 100],
            [['icon'], 'file'],
            [['category'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'partition_type' => 'Partition Type',
            'is_active' => 'Is Active?',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropAreaPartitions()
    {
        return $this->hasMany(PropAreaPartition::className(), ['area_partition_type' => 'partition_type']);
    }
}
