<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_attach".
 *
 * @property string $id
 * @property string $attach_type
 * @property string $file_path
 * @property string $created_on
 * @property string $prop_unit_id
 * @property string $name
 *
 * @property PropertyUnits $propUnit
 */
class PropUnitAttach extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_attach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attach_type', 'prop_unit_id', 'name'], 'required'],
            [['created_on'], 'safe'],
            [['prop_unit_id'], 'integer'],
            [['attach_type', 'name'], 'string', 'max' => 100],
            [['file_path'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attach_type' => 'Attachment Type',
            'file_path' => 'File Path',
            'created_on' => 'Created On',
            'prop_unit_id' => 'Property Unit ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_unit_id']);
    }
}
