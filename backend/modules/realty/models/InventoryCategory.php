<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "inventory_category".
 *
 * @property string $cat
 * @property string $description
 * @property integer $is_active
 *
 * @property Inventory[] $inventories
 */
class InventoryCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['cat'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cat' => 'Inventory Category',
            'description' => 'Description',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventory::className(), ['cat' => 'cat']);
    }
}
