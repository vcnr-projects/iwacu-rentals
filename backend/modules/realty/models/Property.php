<?php

namespace app\modules\realty\models;

use app\models\Address;
use app\models\Telephone;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "property".
 *
 * @property string $id
 * @property string $name
 * @property integer $member_id
 * @property integer $prop_type
 * @property integer $no_units
 * @property integer $no_floors
 * @property integer $year_of_construction
 * @property string $addres_id
 *
 * @property PropertyType $propType
 * @property Address $addres
 * @property User $member
 * @property PropertyAccessibilities[] $propertyAccessibilities
 * @property PropertyAmenities[] $propertyAmenities
 * @property PropertyUnits[] $propertyUnits
 * @property Service[] $services
 */
class Property extends \yii\db\ActiveRecord
{
    public $propGroup;
    public $adrs;
    public $tel;
    public $amenities;
    public $facilities;
    public $accessibilities;
    public $properties;
    public $city;
    public $pics;

    public $bedroom_type;
    public $servant_room;
    public $no_bathroom;
    public $tag_line = ""; /* added by rohith, to clear error on 18 june 2020 */


    public function __construct(){
        $this->adrs = new Address();
        $this->tel = new Telephone();
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        if(!isset($scenarios[$this->scenario])){
            $scenarios[$this->scenario]=array_keys($this->attributeLabels());
        }
        else{
            $scenarios['bulider'] =array_keys($this->attributeLabels());//Scenario Values Only Accepted
        }
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules(){

        return [
            [['name', 'prop_type'], 'required'],
            [['member_id', 'no_units', 'tot_units', 'no_floors', 'year_of_construction', 'address_id','is_active'], 'integer'],
            [['name','propGroup','builder_name'], 'string', 'max' => 200],
            [['site_contact_no'], 'string', 'max' => 15],
            [['site_contact_email','incharge_name','designation'], 'string'],
            [['lat','long'], 'number'],
            [['image'], 'safe'],
            //[['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],

            [['featured','advertisement'], 'integer'],
            [['image', 'approvals'], 'string'],
            [['start_price', 'area_range','tag_line'], 'string', 'max' => 100],
            [['proj_start_date', 'poss_date','locality_desc'], 'safe'],
            [['master_plan'], 'file'],
            [['proj_status','locality','tot_proj_area'], 'string', 'max' => 100],

            //[['loan'], 'string', 'max' => 100],
            [['overview','facilities','locality_desc','loan'], 'string', 'max' => 9000],
            [['no_block'], 'integer'],
            //[['no_block'], 'integer','isEmpty'=>function($value){$this->no_block=0;}],
            [['list_of_doc','city'], 'safe'],
            [['prop_type'], 'integer','message'=>"Property Type is required"],
            //[['no_block'], 'default', 'setOnEmpty' => true, 'value' => 0]
            //[['bedroom_type','servant_room','no_bathroom'], 'safe'],

        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $default= [
            'id' => 'ID',
            'name' => 'Project Name',
            'member_id' => 'Member ID',
            'prop_type' => 'Property Type',
            'no_units' => 'No. of Units to be Managed',
            'tot_units' => 'Total No. of Units ',
            'no_floors' => 'No. of Floors',
            'year_of_construction' => 'Year of Construction',
            'address_id' => 'Address ',
            'propGroup' => 'Property Group ',
            'site_contact_no' => 'On-site Contact No. ',
            'site_contact_email' => ' Contact Email ',
            'image' => 'Photos ',
            'lat' => 'Latitude ',
            'long' => 'Longitude ',
            'incharge_name' => ' Name ',
            'designation' => ' Designation ',
            'builder_name' => 'Builder Name ',
            'is_active' => 'Is Active? ',

            'proj_status' => 'Project Status',
            'tot_proj_area' => 'Total Project Area in acres',
            'start_price' => 'Price range',
            'area_range' => 'Area range',
            'approvals' => 'Approvals',
            'loan' => 'Loans',
            'proj_start_date' => 'Project Start Date',
            'poss_date' => 'Possession handing over date',
            'master_plan' => 'Master Plan',
            'featured' => 'Featured',
            'advertisement' => 'Advertisement',


            'bedroom_type' => 'Bedroom Type',
            'servant_room' => 'Servant Room',
            'no_bathroom' => 'No. of bathrooms',
            'no_block' => 'No. of Blocks',
            'locality' => 'Location',
            'locality_desc' => 'Location Description',


            'overview' => 'Overview',
            'facilities' => 'Facilities',
            'tag_line' => 'Tag Line',
            'list_of_doc' => 'List of Documents',
        ];

        if($this->scenario=="default"){
            return $default;
        }elseif($this->scenario=="builder"){
            //return array_replace($default,$create_apartment);
            //$default;
            return array_unshift($default,'member_id');
        }else{
            return $default;
        }

        return $default;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropType()
    {
        return $this->hasOne(PropertyType::className(), ['id' => 'prop_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAccessibilities()
    {
        return $this->hasMany(PropertyAccessibilities::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAmenities()
    {
        return $this->hasMany(PropertyAmenities::className(), ['property_id' => 'id']);
    }

    public function getPropertyFacilities()
    {
        return $this->hasMany(PropertyFacilities::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyUnits()
    {
        return $this->hasMany(PropertyUnits::className(), ['property_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertiesPics()
    {
        return $this->hasMany(PropertiesPics::className(), ['prop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['property_id' => 'id']);
    }

    public  function getfadrs(){

        $adrs=$this->getAddress()->one();
        if($adrs) {
            $tel = $adrs->getTelephone()->one();
            // var_dump($adrs->telephone_id);
            if (empty($adrs)) {
                $adrs = new Address();
            }
            if (empty($tel)) {
                $tel = new Telephone();
            }

            return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}

        " . (($tel != null) ? "
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}

        " : "");
        }
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->poss_date)!==strtotime('01-01-1970')){
                $this->poss_date=date('Y-m-d',strtotime($this->poss_date));
            }else{
                $this->poss_date=null;
            }

            if(strtotime($this->proj_start_date)!==strtotime('01-01-1970')){
                $this->proj_start_date=date('Y-m-d',strtotime($this->proj_start_date));
            }else{
                $this->proj_start_date=null;
            }


            return true;
        } else {
            return false;
        }
    }

    public function afterFind(){

        $this->propGroup=$this->getPropType()->one()->group_id;

        $adrs=$this->getAddress()->one();
        if($adrs){
            $this->city=$adrs->city;
        }

        if(strtotime($this->poss_date)==strtotime('01-01-1970')){
            $this->poss_date=null;

        }else{
            $this->poss_date=date('d-m-Y',strtotime($this->poss_date));
        }
        if(strtotime($this->proj_start_date)==strtotime('01-01-1970')){
            $this->proj_start_date=null;

        }else{
            $this->proj_start_date=date('d-m-Y',strtotime($this->proj_start_date));
        }

        $pu=$this->getPropertyUnits()->all();
        $this->bedroom_type='';
        if($pu){
            foreach($pu as $p){
                $this->bedroom_type.=$p->no_bedrooms.",";
                $this->servant_room=$p->no_servant_room;
                $this->no_bathroom=$p->no_bathrooms;
            }
            $this->bedroom_type= substr($this->bedroom_type,0,strlen($this->bedroom_type)-1);
        }

    }

    public function saver(){

        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {

            
            //begin: adrs
            Yii::trace(VarDumper::dumpAsString($this->adrs),'vardump');
            $hadrs=Address::findOne($this->address_id);
            if(!$hadrs){
                $hadrs=new Address();
            }
            $hadrs->setAttributes(['adrs_line1'=>$this->adrs->adrs_line1,
                'adrs_line2'=>$this->adrs->adrs_line2,
                'city'=>$this->adrs->city,
                'state'=>$this->adrs->state,
                'country'=>$this->adrs->country,
                'postal_code'=>$this->adrs->postal_code,
            ]);

            $htel=$hadrs->getTelephone()->one();
            if($htel==null){
                $htel=new Telephone();
            }
            $htel->setAttributes([
                'primary_landline'=>$this->tel->primary_landline,
                'primary_mobile'=>$this->tel->primary_mobile,
                'secondary_landline'=>$this->tel->secondary_landline,
                'secondary_mobile'=>$this->tel->secondary_mobile,
            ]);

            $this->tel=$htel;
            if (!$this->tel->save()) {
                foreach($this->tel->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                $hadrs->telephone_id=$this->tel->id;
            }


            if(!$hadrs->save()){
                foreach($hadrs->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else {
                $this->adrs = $hadrs;
                $this->address_id = $this->adrs->id;
            }

            //end: adrs



            if(!$this->save()){
                $errmsg.= ' ';
                /*foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);*/
            }else{

                $sql="select * from properties p , address a where p.address_id=a.id and p.name=:name and a.city=:city";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":name",$this->name);
                $cmd->bindValue(":city",$this->adrs->city);
                $res=$cmd->queryOne();
                if(!$res){
                    $pties=new Properties();
                    $pties->name=$this->name;
                    $pties->prop_type=$this->prop_type;
                    $pties->no_floors=$this->no_floors;
                    $pties->year_of_construction=$this->year_of_construction;
                    $adrs=new Address();
                    $adrs->adrs_line1=$this->adrs->adrs_line1;
                    $adrs->city=$this->adrs->city;
                    $adrs->state=$this->adrs->state;
                    $adrs->country=$this->adrs->country;
                    $adrs->postal_code=$this->adrs->postal_code;
                    if($adrs->save()){
                        $pties->address_id=$adrs->id;
                    }
                    $pties->image=$this->image;
                    $pties->lat=$this->lat;
                    $pties->long=$this->long;
                    $pties->builder_name=$this->builder_name;
                    $pties->save();

                }

                $sql="select count(*) c from property_units where property_id =:id  ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$this->id);
                $res=$cmd->queryOne();

                $bhk=explode(",",$this->bedroom_type);
                if($res["c"]==0)
                foreach($bhk as $b){
                    if(empty($b))continue;
                    $pu=new PropertyUnits();
                    $pu->name=$pu->getLatestPropname();
                    $pu->property_id=$this->id;
                    $pu->floor_number=1;
                    $pu->no_bedrooms=$b;
                    $pu->no_servant_room=0;
                    $pu->no_bathrooms=1;//$this->no_bathroom;
                    $pu->flat_no='1';
                    Yii::trace(VarDumper::dumpAsString($this->no_bathroom),'vardump');
                    Yii::trace(VarDumper::dumpAsString($this->servant_room),'vardump');
                    Yii::trace(VarDumper::dumpAsString($b),'vardump');
                    if(!$pu->save()){
                        foreach($pu->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                    $pf=new PropUnitFinance();
                    $pf->prop_unit_id=$pu->id;
                    if(!$this->member_id)
                        $pf->for_sale=1;
                    else{
                        $pf->is_rent=1;
                    }
                    if(!$pf->save()){
                        foreach($pu->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                }

                if(!empty($this->pics)){
                    PropertiesPics::deleteAll(["prop_id"=>$this->id]);
                    Yii::trace(VarDumper::dumpAsString(count($this->pics)),'vardump');

                    foreach($this->pics as $pic){

                        Yii::trace(VarDumper::dumpAsString($pic),'vardump');
                        Yii::trace(VarDumper::dumpAsString($this->id),'vardump');
                        $pic->isNewRecord=true;
                        $pic->prop_id=$this->id;
                        $pic->id=null;
                        if(!$pic->save()){
                            foreach($pic->getErrors() as $msg)
                                $errmsg.=join("<br/>",$msg);
                        }
                    }
                }

            }

            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;
    }

    //begin amenities saver
    public function saveAmenities(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->amenities as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from property_amenities where property_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();


            $saveids=[];
            foreach($this->facilities as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from property_facilities where property_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end amenities saver

    //begin amenities saver
    public function saveAccessibilities(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->accessibilities as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from property_accessibilities where property_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end amenities saver


    public function  getMetype(){
        return (isset($this->member_id)?"Member":"Builder");
    }

}
