<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_finance".
 *
 * @property string $id
 * @property integer $is_rent
 * @property string $min_rent
 * @property string $max_rent
 * @property string $fixed_rent
 * @property string $min_rent_deposit
 * @property string $max_rent_deposit
 * @property string $fixed_rent_deposit
 * @property string $yearly_rent_increment
 * @property integer $is_lease
 * @property string $min_lease
 * @property string $max_lease
 * @property string $fixed_lease
 * @property integer $lease_years
 * @property string $prop_unit_id
 *
 * @property PropertyUnits $propUnit
 */
class PropUnitFinance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_finance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_rent', 'is_lease', 'lease_years', 'prop_unit_id','available_units','for_sale'], 'integer'],
            [['min_rent', 'max_rent', 'fixed_rent', 'min_rent_deposit', 'max_rent_deposit', 'fixed_rent_deposit', 'yearly_rent_increment', 'min_lease', 'max_lease', 'fixed_lease','starting_price'], 'number'],
            [['prop_unit_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_rent' => 'Is Rentable?',
            'min_rent' => 'Minimum Rent Amount',
            'max_rent' => 'Maximum Rent Amount',
            'fixed_rent' => 'Fixed Rent Amount',
            'min_rent_deposit' => 'MInimum Rental Deposit',
            'max_rent_deposit' => 'Maximum Rental Deposit',
            'fixed_rent_deposit' => 'Fixed Rental Deposit',
            'yearly_rent_increment' => 'Yearly rental increment in percentage',
            'is_lease' => 'Is Leaseable?',
            'min_lease' => 'Minimum Lease Amount',
            'max_lease' => 'Maximum Lease Amount',
            'fixed_lease' => 'Fixed Lease',
            'lease_years' => 'Lease Years',
            'prop_unit_id' => 'Property Unit ID',
            'for_sale' => 'For Sale?',
            'available_units' => 'Available Units',
            'starting_price' => 'Starting Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_unit_id']);
    }
}
