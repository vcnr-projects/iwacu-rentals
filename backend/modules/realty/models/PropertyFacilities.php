<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "property_facilities".
 *
 * @property string $id
 * @property string $amenity
 * @property string $property_id
 * @property string $created_on
 *
 * @property AmenityType $amenity0
 * @property Property $property
 */
class PropertyFacilities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_facilities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amenity', 'property_id'], 'required','on'=>'default'],
            [['property_id'], 'integer'],
            [['created_on'], 'safe'],
            [['amenity'], 'string', 'max' => 100],
            [[ 'property_id'], 'required','on'=>'bulkInsert'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amenity' => 'Facilities',
            'property_id' => 'Property',
            'created_on' => 'Created On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenity0()
    {
        return $this->hasOne(AmenityType::className(), ['amenity' => 'amenity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
