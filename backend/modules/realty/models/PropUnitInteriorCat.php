<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_interior_cat".
 *
 * @property string $category
 * @property integer $is_active
 *
 * @property PropUnitInterior[] $propUnitInteriors
 */
class PropUnitInteriorCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_interior_cat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['is_active'], 'integer'],
            [['category'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category' => 'Category',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnitInteriors()
    {
        return $this->hasMany(PropUnitInterior::className(), ['category' => 'category']);
    }
}
