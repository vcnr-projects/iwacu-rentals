<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\AmenityType;

/**
 * AmenityTypeSearch represents the model behind the search form about `app\modules\realty\models\AmenityType`.
 */
class AmenityTypeSearch extends AmenityType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amenity', 'created_on','type'], 'safe'],
            [['is_present'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmenityType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_present' => $this->is_present,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'amenity', $this->amenity]);
        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
