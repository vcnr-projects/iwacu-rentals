<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\GovtApprovals;

/**
 * GovtApprovalsrCatSearch represents the model behind the search form about `app\modules\realty\models\GovtApprovals`.
 */
class GovtApprovalsrCatSearch extends GovtApprovals
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approvals', 'icon'], 'safe'],
            [['is_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GovtApprovals::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'approvals', $this->approvals])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
