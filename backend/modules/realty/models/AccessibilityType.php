<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "accessibility_type".
 *
 * @property string $accessibility_type
 * @property integer $is_active
 *
 * @property PropertyAccessibilities[] $propertyAccessibilities
 */
class AccessibilityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accessibility_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accessibility_type'], 'required'],
            [['is_active'], 'integer'],
            [['accessibility_type'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'accessibility_type' => 'Accessibility Type',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAccessibilities()
    {
        return $this->hasMany(PropertyAccessibilities::className(), ['accessibility' => 'accessibility_type']);
    }
}
