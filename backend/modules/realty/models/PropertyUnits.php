<?php

namespace app\modules\realty\models;

use app\models\Executive;
use app\models\ExecutiveProperties;
use app\modules\member\models\Tenant;
use app\modules\service\models\Service;
use app\modules\service\models\TenantPropertyAllocation;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "property_units".
 *
 * @property string $name
 * @property string $description
 * @property string $id
 * @property string $property_id
 * @property integer $floor_number
 * @property string $block
 * @property string $flat_no
 * @property string $super_builtup_area
 * @property string $carepet_area
 * @property integer $no_balconies
 * @property integer $no_bathrooms
 * @property integer $no_bedrooms
 * @property integer $no_servant_room
 * @property integer $furnished_status
 * @property string $door_facing
 * @property string $two_wheeler_parking_id
 * @property string $Four_wheeler_parking_id
 * @property integer $has_visitor_parking
 * @property integer $are_pets_allowed
 * @property string $other_terms
 * @property string $agreement_file_path
 * @property string $agreement_type
 * @property string $prefered_tenant_type
 * @property string $prefered_food_habits
 *
 * @property PropAreaPartition[] $propAreaPartitions
 * @property PropUnitAttach[] $propUnitAttaches
 * @property PropUnitParking[] $propUnitParkings
 * @property Property $property
 */
class PropertyUnits extends \yii\db\ActiveRecord
{
    public $partitons;
    public $parkings;
    public $attachments;
    public $finance;
    public $inventories;
    public $interior;
    public $pname;
    public $ptype;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_units';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        if(!isset($scenarios[$this->scenario])){
            $scenarios[$this->scenario]=array_keys($this->attributeLabels());
        }
        else{
            $scenarios['create_apartment'] =array_keys($this->attributeLabels());//Scenario Values Only Accepted
        }
        return $scenarios;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[[ 'property_id', 'floor_number', 'flat_no'], 'required'],
            [['property_id', 'flat_no'], 'required' ],
            [['description', 'other_terms','name'], 'string'],
            [['property_id', 'no_balconies', 'no_bathrooms', 'no_bedrooms', 'no_servant_room', 'has_visitor_parking', 'are_pets_allowed','is_active','available_units'], 'integer'],
            [['super_builtup_area', 'carepet_area','builtup_area','starting_price'], 'number'],
            [['name'], 'string', 'max' => 150],
            [['block', 'door_facing', 'prefered_tenant_type', 'furnished_status', 'prefered_food_habits'], 'string', 'max' => 100],
            [['flat_no', 'floor_number'], 'string', 'max' => 10],
            [['two_wheeler_parking_id', 'Four_wheeler_parking_id'], 'string', 'max' => 11],
            [['agreement_file_path'], 'string', 'max' => 255],
            [['agreement_type'], 'string', 'max' => 200],
            [['availability_date'], 'safe'],
            [['image'],'safe']
            //[['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $default= [
            'name' => 'Name',
            'description' => 'Description',
            'id' => 'Id',
            'property_id' => 'Property ID',
            'floor_number' => 'Floor Number',
            'block' => 'Block/Wing/Tower',
            'flat_no' => 'Flat Number',
            'super_builtup_area' => 'Super Built Up Area in  sqft',
            'carepet_area' => 'Carpet Area in sqft',
            'no_balconies' => 'No. of Balconies',
            'no_bathrooms' => 'No. of Bathrooms',
            'no_bedrooms' => 'No. of Bedrooms',
            'no_servant_room' => 'No. of Servant Room',
            'furnished_status' => 'Furnished Status',
            'door_facing' => 'Door Facing Direction',
            'two_wheeler_parking_id' => 'Two Wheeler Parking Id',
            'Four_wheeler_parking_id' => 'Four Wheeler Parking Id',
            'has_visitor_parking' => 'Has Visitors Parking?',
            'are_pets_allowed' => 'Are Pets Allowed?',
            'other_terms' => 'Other Terms',
            'agreement_file_path' => 'Agreement File Path',
            'agreement_type' => 'Agreement Type',
            'prefered_tenant_type' => 'Preferred Tenant Type',
            'prefered_food_habits' => 'Preferred Food Habits',
            'tenant_id' => 'Tenant',
            'executive_id' => 'Executive',
            'builtup_area' => 'Built Up Area sqft',
            'image' => 'Floor plan photo',
            'availability_date' => 'Availability Date',
            'is_active' => 'Is Active',
            'available_units' => 'Available Units',
            'starting_price' => 'Starting Price',
        ];

        $create_apartment=[
            'property_id' => 'Apartment ID',
            'property.name' => 'Apartment Name',
        ];

        if($this->scenario=="default"){
                return $default;
         }elseif($this->scenario=="create_apartment"){
           return array_replace($default,$create_apartment);
        }else{
            return $default;
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropAreaPartitions()
    {
        return $this->hasMany(PropAreaPartition::className(), ['punit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnitAttaches()
    {
        return $this->hasMany(PropUnitAttach::className(), ['prop_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnitParkings()
    {
        return $this->hasMany(PropUnitParking::className(), ['prop_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    public function getPType()
    {
        return $this->hasOne(PropertyType::className(),['id' => 'prop_type'] )->viaTable('property',['id' => 'property_id'] );
    }

    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['member_id' => 'tenant_id']);
    }

    public function getExecutive()
    {
        return $this->hasOne(Executive::className(), ['user_id' => 'executive_id']);
    }

    public function getFinances()
    {
        return $this->hasMany(PropUnitFinance::className(), ['prop_unit_id' => 'id']);
    }

    public function getPropUnitInventories()
    {
        return $this->hasMany(PropUnitInventory::className(), ['prop_unit_id' => 'id']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['property_id' => 'id']);
    }

    public function getActiveContract()
    {
        return $this->hasMany(Service::className(), ['property_id' => 'id'])->filterWhere(["is_active"=>1]);
    }


    public function getAcitveTenant()
    {
        return $this->hasMany(TenantPropertyAllocation::className(), ['prop_id' => 'id'])->filterWhere(["is_active"=>1]);
    }


    public function getExecutives()
    {
        return $this->hasOne(ExecutiveProperties::className(), ['property' => 'id'])->filterWhere(["is_active"=>1]);
    }

    public function getInteriors()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id']);//->filterWhere(["is_active"=>1]);
    }

    public function getInteriorsFlooring()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id'])->filterWhere(["category"=>"Flooring"]);
    }

    public function getInteriorsFittings()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id'])->filterWhere(["category"=>"Fittings"]);
    }

    public function getInteriorsWalls()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id'])->filterWhere(["category"=>"Walls"]);
    }

    public function getAdditionalFacilities()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id'])->filterWhere(["category"=>"Additional Facilities"]);
    }

    public function getAvailability()
    {
        return $this->hasOne(PropUnitInterior::className(), ['prop_unit_id' => 'id'])->filterWhere(["category"=>"Availability"]);
    }



    public function scenarioLabel($altLabel){

       /* $sl=[
            "create_apartment"=>"Create Apartment Units",
            "default"=>$altLabel,
        ];
        return $sl[$this->scenario];*/
        return ucwords( str_replace("_"," ",$this->scenario)." Units");
    }


    public function attributeScenarioAcess()
    {
        return [
            'name' => [
                
            ],
            'description' =>[
                //"gm" => array("write"),
            ],
            'id' =>[
                "*"=>["read","write"],
            ],
            'property_id' =>[
                //"*"=>["read","write"],
            ],
            'floor_number' =>[],
            'block' =>[],
            'flat_no' =>[],
            'super_builtup_area' =>[],
            'carepet_area' =>[],
            'no_balconies' =>[],
            'no_bathrooms' =>[],
            'no_bedrooms' =>[],
            'no_servant_room' =>[],
            'furnished_status' =>[],
            'door_facing' =>[],
            'two_wheeler_parking_id' =>[
                "*"=>["read","write"],
            ],
            'Four_wheeler_parking_id' =>[
                "*"=>["read","write"],
            ],
            'has_visitor_parking' =>[],
            'are_pets_allowed' =>[],
            'other_terms' =>[
                "*"=>["read","write"],
            ],
            'agreement_file_path' =>[
                "*"=>["read","write"],
            ],
            'agreement_type' =>[
                "*"=>["read","write"],
            ],
            'prefered_tenant_type' =>[],
            'prefered_food_habits' =>[],
        ];
    }



    //begin partitin saver
    public function savePartition(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
           $saveids=[];
            foreach($this->partitons as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg) {
                        $errmsg .= implode("",$msg)."<br/>";
                    }

                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {
                $sql = "delete from prop_area_partition where punit_id={$this->id} and id not in ({$ids}) ";
                $cmd = $connection->createCommand($sql);
                $cmd->execute();
            }
            else{
                $errmsg.="Atleast One partition should be present";
            }



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');
            $errmsg=explode("<br/>",$errmsg);
            if(!$errmsg[count($errmsg)-1])unset($errmsg[count($errmsg)-1]);
            $this->addErrors($errmsg);
            return false;
        }
        return true;



    }
    //end partitin saver

    //begin parking saver
    public function saveParking(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->parkings as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
              //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from prop_unit_parking where prop_unit_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            $errmsg=explode("<br/>",$errmsg);
            \yii::trace($errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end parking saver


    //begin attachment saver
    public function saveAttachments(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->attachments as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from prop_unit_attach where prop_unit_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end attachment saver


    //begin inventory saver
    public function saveInventories(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->inventories as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from prop_unit_inventory where prop_unit_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end attachment saver


    //begin interior saver
    public function saveInterior(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {

            $puids=[];
            $sql="
            select  prop_unit_id from prop_unit_interior pui
where prop_unit_id =:id
            ";
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$this->id);
            $res=$cmd->queryOne();
            if(!$res){
                $sql="
select pu.id from property_units pu
where property_id=(
select p.id from property p, property_units pu
where p.id=pu.property_id  and  pu.id=:id )

and id not in (select  prop_unit_id from prop_unit_interior pui)

  ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$this->id);
                $puids=$cmd->queryColumn();
            }else{
                $puids[]=$this->id;
            }



            $sql = "delete from prop_unit_interior where prop_unit_id={$this->id}  ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();


            Yii::trace(VarDumper::dumpAsString($puids),'vardump');
            Yii::trace(VarDumper::dumpAsString(!$res),'vardump');


            foreach($puids as $pu){

                foreach ($this->interior as $par) {
                    //$par->prop_unit_id = $this->id;
                    $par->id=null;
                    $par->isNewRecord=true;
                    $par->prop_unit_id = $pu;
                    if (!$par->save()) {
                        foreach ($par->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    } else {
                        $saveids[] = $par->id;
                    }
                }
            }


            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end interior saver


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(!$this->name){
                $this->name=$this->getLatestPropname();
            }

            if(strtotime($this->availability_date)!==false){
                $this->availability_date=date('Y-m-d',strtotime($this->availability_date));
            }else{
                $this->availability_date=null;
            }



            return true;
        } else {
            return false;
        }
    }

    public function afterFind ()
    {

        if(strtotime($this->availability_date)==false){
            $this->availability_date=null;

        }else{
            $this->availability_date=date('d-m-Y',strtotime($this->availability_date));
        }
          parent::afterFind ();
    }



    public  function getLatestPropname(){



        /*if($this->user_type=='Executive')
            $usertypecode='VHP-EMPID';
        else{
            $usertypecode="VHP-$this->user_type";
        }*/

        $pref="VHP-PROP";
        //$pref=$usertypecode;

        $lbn=0001;

        $sql="select max(name) as mname from property_units ";
        $cmd = \Yii::$app->db->createCommand($sql);

        $result = $cmd->queryAll();
        //$lbn=6000;
        foreach ($result as $row){
            preg_match('/\d+$/',$row["mname"],$match);
            if(isset($match[0])&&$match[0]) {
                Yii::trace(VarDumper::dumpAsString($match),'vardump');
                $lbn = $match[0];
                $lbn++;
            }

        }

        /* $sql="select * from auto_no_init where name like 'batch' ";
         $cmd = Yii::app()->db->createCommand($sql);
         $res=$cmd->queryRow();
         if($res&&$res["reset"]==1){
             preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
             // var_dump($match);
             if(isset($match[1]))
                 $pref=$match[1];
             if(isset($match[2]))
                 $lbn=$match[2];

         }elseif($res){
             // var_dump($res["slno"]);
             preg_match('/(.*?)(\d+$)/',trim($res["slno"]),$match);
             //  var_dump($match);
             if(isset($match[1]))
                 $pref=$match[1];
         }*/
        //$billno="MED/". date("Y")."/"."INVOICE/".sprintf("%05d", $lbn+1);
        //$billno=date("Y")."/".$pref.sprintf("%05d", $lbn);
        $billno=$pref.sprintf("%05d", $lbn);
        return $billno;

    }


    //end class
}
