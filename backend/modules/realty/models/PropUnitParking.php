<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_parking".
 *
 * @property string $id
 * @property integer $no_of_vehicles
 * @property string $parking_type
 * @property string $prop_unit_id
 * @property string $location
 *
 * @property PropertyUnits $propUnit
 */
class PropUnitParking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_parking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_of_vehicles', 'parking_type', 'prop_unit_id'], 'required'],
            [['no_of_vehicles', 'prop_unit_id'], 'integer'],
            [['location'], 'string'],
            [['parking_type', 'vehicle_type'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'no_of_vehicles' => 'Number of Vehicles',
            'parking_type' => 'Parking Type',
            'vehicle_type' => 'Vehicle Type',
            'prop_unit_id' => 'Property Unit ',
            'location' => 'Location',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_unit_id']);
    }
}
