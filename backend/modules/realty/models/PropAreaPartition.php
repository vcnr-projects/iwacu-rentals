<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_area_partition".
 *
 * @property string $id
 * @property string $punit_id
 * @property string $area_partition_type
 * @property string $area
 * @property string $remarks
 * @property string $name
 *
 * @property PropertyPartitionType $areaPartitionType
 * @property PropertyUnits $punit
 */
class PropAreaPartition extends \yii\db\ActiveRecord
{
    public $inventories;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_area_partition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['punit_id', 'area_partition_type', 'area', 'name'], 'required'],
            [['punit_id'], 'integer'],
            [['area'], 'number'],
            [['remarks'], 'string'],
            [['area_partition_type'], 'string', 'max' => 100],
            [['image'], 'file'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'punit_id' => 'Property Unit',
            'area_partition_type' => 'Area Partition Type',
            'area' => 'Area in sqmtr',
            'remarks' => 'Remarks',
            'name' => 'Name',
            'image' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreaPartitionType()
    {
        return $this->hasOne(PropertyPartitionType::className(), ['partition_type' => 'area_partition_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPunit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'punit_id']);
    }

    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id'])->via('punit');
    }

    public function getPropUnitInventories()
    {
        return $this->hasMany(PropUnitInventory::className(), ['prop_unit_id' => 'id']);
    }

    //begin inventory saver
    public function saveInventories(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $saveids=[];
            foreach($this->inventories as $par){
                if(!$par->save()){
                    foreach($par->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $saveids[]=$par->id;
                }
            }

            $ids=implode(",",$saveids);
            if($ids) {

            }
            else{
                //  $errmsg.="Atleast One parking should be present";
                $ids=0;
            }

            $sql = "delete from prop_unit_inventory where prop_unit_id={$this->id} and id not in ({$ids}) ";
            $cmd = $connection->createCommand($sql);
            $cmd->execute();



            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }
    //end attachment saver


}
