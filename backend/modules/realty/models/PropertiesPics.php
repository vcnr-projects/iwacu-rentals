<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "properties_pics".
 *
 * @property string $id
 * @property string $prop_id
 * @property string $pic
 *
 * @property Properties $prop
 */
class PropertiesPics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properties_pics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'prop_id', 'pic','type'], 'required'],
            [['id', 'prop_id'], 'integer'],
            [['pic','type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prop_id' => 'Property ID',
            'pic' => 'Picture Path',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProp()
    {
        return $this->hasOne(Properties::className(), ['id' => 'prop_id']);
    }
}
