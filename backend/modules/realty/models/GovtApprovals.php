<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "govt_approvals".
 *
 * @property string $approvals
 * @property integer $is_active
 * @property string $icon
 */
class GovtApprovals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'govt_approvals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approvals'], 'required'],
            [['is_active'], 'integer'],
            [['approvals'], 'string', 'max' => 100],
            //[['icon'], 'string', 'max' => 200]
            [['icon'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'approvals' => 'Approvals',
            'is_active' => 'Is Active?',
            'icon' => 'Icon',
        ];
    }
}
