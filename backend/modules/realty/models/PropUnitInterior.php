<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "prop_unit_interior".
 *
 * @property string $id
 * @property string $category
 * @property string $heading
 * @property string $description
 * @property string $prop_unit_id
 *
 * @property PropertyUnits $propUnit
 * @property PropUnitInteriorCat $category0
 */
class PropUnitInterior extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_unit_interior';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'heading', 'description', 'prop_unit_id'], 'required'],
            [['description'], 'string'],
            [['prop_unit_id'], 'integer'],
            [['category'], 'string', 'max' => 100],
            [['heading'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'heading' => 'Heading',
            'description' => 'Description',
            'prop_unit_id' => 'Property Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnit()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(PropUnitInteriorCat::className(), ['category' => 'category']);
    }
}
