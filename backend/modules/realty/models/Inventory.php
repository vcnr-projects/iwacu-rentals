<?php

namespace app\modules\realty\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "inventory".
 *
 * @property integer $id
 * @property string $name
 * @property string $cat
 * @property string $description
 * @property integer $is_active
 *
 * @property InventoryCategory $cat0
 */
class Inventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $brandnames;

    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'cat'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['name', 'cat'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Inventory Name',
            'cat' => 'Inventory Category',
            'description' => 'Description',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat0()
    {
        return $this->hasOne(InventoryCategory::className(), ['cat' => 'cat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryBrands()
    {
        return $this->hasMany(InventoryBrands::className(), ['inventory' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropUnitInventories()
    {
        return $this->hasMany(PropUnitInventory::className(), ['inventory' => 'id']);
    }

    public function getBrands()
    {
        $brands= $this->hasMany(InventoryBrands::className(), ['inventory' => 'id'])->all();
        $btxt='';
        foreach($brands as $b){
            $btxt.=" {$b->brand}, ";
        }
        return $btxt;
    }


    public function saveInventoryBrands(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {


            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }else{
                InventoryBrands::deleteAll(['inventory'=>$this->id]);
                foreach($this->brandnames as $par){
                    $par->inventory=$this->id;
                    if(!$par->save()){
                        Yii::trace(VarDumper::dumpAsString($par));
                        foreach($par->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                }
            }


            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

}
