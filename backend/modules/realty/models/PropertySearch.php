<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\Property;
use yii\helpers\VarDumper;

/**
 * PropertySearch represents the model behind the search form about `app\modules\realty\models\Property`.
 */
class PropertySearch extends Property
{
    public $mtype;
    public $xsearch;
    public $ptype;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id', 'prop_type', 'no_units', 'no_floors', 'year_of_construction', 'address_id'], 'integer'],
            [['name','mtype','xsearch','ptype'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Property::find();
        $query->joinWith(['propType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'member_id' => $this->member_id,
            'prop_type' => $this->prop_type,
            'no_units' => $this->no_units,
            'no_floors' => $this->no_floors,
            'year_of_construction' => $this->year_of_construction,
            'address_id' => $this->address_id,
        ]);

        $query->andFilterWhere(['like', 'property.name', $this->name]);
        $query->andFilterWhere(['like', 'property_type.name', $this->ptype]);
        if(isset($this->mtype)){
            if($this->mtype=="Builder"){
                //$query->andFilterWhere(['is ', 'member_id', null]);
                $query->andWhere("member_id is null");
            }
            if($this->mtype=="Member"){
                //$query->andFilterWhere(['is ', 'member_id', null]);
                $query->andWhere("member_id is not null");
            }
        }

        if($this->xsearch=="Featured Properties"){
            $query->andFilterWhere([ 'featured'=> 1]);
        }
        if($this->xsearch=="Normal"){
            $query->andFilterWhere([ 'featured'=> 0]);
        }
        Yii::trace(VarDumper::dumpAsString($this->mtype),'vardump');
        Yii::trace(VarDumper::dumpAsString($dataProvider),'vardump');

        $dataProvider->sort->attributes['ptype'] = [
            'asc' => ['property_type.name' => SORT_ASC],
            'desc' => ['property_type.name' => SORT_DESC],
        ];
        return $dataProvider;
    }
}
