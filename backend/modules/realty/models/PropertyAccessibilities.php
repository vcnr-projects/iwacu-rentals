<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "property_accessibilities".
 *
 * @property string $id
 * @property string $accessibility
 * @property string $remaks
 * @property string $created_on
 * @property string $property_id
 *
 * @property Property $property
 * @property AccessibilityType $accessibility0
 */
class PropertyAccessibilities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_accessibilities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accessibility', 'remaks', 'property_id'], 'required'],
            [['remaks'], 'string'],
            [['created_on'], 'safe'],
            [['property_id'], 'integer'],
            [['accessibility'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'accessibility' => 'Accessibility',
            'remaks' => 'Remarks',
            'created_on' => 'Created On',
            'property_id' => 'Property ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessibility0()
    {
        return $this->hasOne(AccessibilityType::className(), ['accessibility_type' => 'accessibility']);
    }
}
