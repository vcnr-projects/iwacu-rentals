<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "property_type".
 *
 * @property integer $id
 * @property string $group_id
 * @property string $name
 * @property integer $is_active
 *
 * @property Property[] $properties
 * @property PropertyGroup $group
 */
class PropertyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'name', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['group_id', 'name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'group_id' => 'Group Id',
            'name' => 'Property Type',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['prop_type' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(PropertyGroup::className(), ['name' => 'group_id']);
    }
}
