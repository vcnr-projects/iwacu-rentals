<?php

namespace app\modules\realty\models;

use Yii;

/**
 * This is the model class for table "bank_loans".
 *
 * @property string $loan
 * @property string $icon
 * @property integer $is_active
 */
class BankLoans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_loans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan'], 'required'],
            [['is_active'], 'integer'],
            [['loan'], 'string', 'max' => 100],
            //[['icon'], 'string', 'max' => 255]
            [['icon'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan' => 'Loan',
            'icon' => 'Icon',
            'is_active' => 'Is Active?',
        ];
    }
}
