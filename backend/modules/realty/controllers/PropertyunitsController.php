<?php

namespace app\modules\realty\controllers;

use app\modules\realty\models\PropAreaPartition;
use app\modules\realty\models\Property;
use app\modules\realty\models\PropUnitAttach;
use app\modules\realty\models\PropUnitFinance;
use app\modules\realty\models\PropUnitInterior;
use app\modules\realty\models\PropUnitInventory;
use app\modules\realty\models\PropUnitParking;
use Yii;
use app\modules\realty\models\PropertyUnits;
use app\modules\realty\models\PropertyUnitsSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PropertyunitsController implements the CRUD actions for PropertyUnits model.
 */
class PropertyunitsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'
                    ,'partitionform'
                    ,'parkingform'
                    ,'attachmentform'
                    ,'financeform'
                    ,'inventoryform'
                    ,'allocatetenant'
                    ,'allocateexecutive'
                    ,'getmember'
                    ,'interior'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'partitionform'
                            ,'parkingform'
                            ,'attachmentform'
                            ,'financeform'
                            ,'inventoryform'
                            ,'allocatetenant'
                            ,'allocateexecutive'
                            ,'getmember'
                            ,'interior'
                        ],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'
                            ,'partitionform'
                            ,'parkingform'
                            ,'attachmentform'
                            ,'financeform'
                            ,'inventoryform'
                            ,'allocatetenant'
                            ,'allocateexecutive'
                            ,'getmember'
                            ,'interior'
                        ],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PropertyUnits models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertyUnitsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PropertyUnits model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("view_".$proptype);

        $model->partitons=$model->getPropAreaPartitions()->all();
        $model->parkings=$model->getPropUnitParkings()->all();
        $model->attachments=$model->getPropUnitAttaches()->all();
        $model->finance=PropUnitFinance::findOne(["prop_unit_id"=>$id]);
        $model->inventories=$model->getPropUnitInventories()->all();
        if(!$model->finance){
            $model->finance=new PropUnitFinance();
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new PropertyUnits model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=0)
    {
        $model = new PropertyUnits();
        $model->available_units=1;

        $model->setScenario("create_apartment");
        if($id>0){
            $prop=Property::findOne($id);
            $proptype= $prop->getPropType()->one()->name;
            $model->setScenario("create_".strtolower($proptype));
        }
        $model->property_id=$id;

        $image=$model->image;

        if (isset($_POST['PropertyUnits'])) {
            $model->setAttributes($_POST['PropertyUnits']);
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                $model->image->name = $model->image->baseName . rand(10000000, 999999999) . '.' . $model->image->extension;
                $model->image->saveAs('uploads/' . $model->image->name);
            } else {
                $model->image = $image;
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PropertyUnits model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;

        $model->setScenario("create_".strtolower($proptype));
        $image=$model->image;

        //$model->setScenario("create_apartment");
        if ($model->load(Yii::$app->request->post())  ) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                $model->image->name = $model->image->baseName . rand(10000000, 999999999) . '.' . $model->image->extension;
                $model->image->saveAs('uploads/' . $model->image->name);
            } else {
                $model->image = $image;
            }
            if($model->save())
            return $this->redirect(['view', 'id' => $model->id]);
        }  {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PropertyUnits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PropertyUnits model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PropertyUnits the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyUnits::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionPartitionform($id)
    {
        $model =  $this->findModel($id);
        $model->setScenario("formPartition_apartment");
        $model->partitons=$model->getPropAreaPartitions()->all();
        if(!count($model->partitons)){
            $model->partitons[]=new PropAreaPartition();
        }
        if (isset($_POST['PropAreaPartition'])) {
            $model->partitons=array();
            foreach($_POST['PropAreaPartition']['name'] as $i=>$name ){
                $parid=$_POST['PropAreaPartition']['id'][$i];
                $prevpap=PropAreaPartition::findOne(["id"=>$parid,"punit_id"=>$id]);

                if(!$prevpap){
                    $prevpap=new PropAreaPartition();
                }else{
                    $image=$prevpap->image;
                }
                $prevpap->punit_id=$model->id;
                $prevpap->area_partition_type=$_POST['PropAreaPartition']['area_partition_type'][$i];
                $prevpap->area=$_POST['PropAreaPartition']['area'][$i];
                $prevpap->remarks=$_POST['PropAreaPartition']['remarks'][$i];
                $prevpap->name=$_POST['PropAreaPartition']['name'][$i];

                //$prevpap->save();

                //UploadedFile::getInstanceByName("attachments" . $i)
                $cm='';
                if(!empty($_FILES['attachments'.$i]['name'][0])){
                    //foreach($_FILES['attachments'.$i]['name'] as $fname){
                        $files=UploadedFile::getInstancesByName('attachments'.$i);
                        Yii::trace(VarDumper::dumpAsString($files),'vardump');
                        foreach($files as $file){
                            if($file){
                                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                                $file->saveAs('uploads/' . $file->name);
                                $cm.=$file."^m";
                            }
                        }
                    //}
                    $prevpap->image=$cm;
                }elseif(!empty($image)){
                    $prevpap->image=$image;
                }

                $model->partitons[]=$prevpap;
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->savePartition())
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>"off"]);
        } {
            return $this->render('partitionform', [
                'model' => $model,
            ]);
        }
    }

    public function actionParkingform($id)
    {
        $model =  $this->findModel($id);
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("formPartition_".strtolower($proptype));
        //$model->setScenario("formPartition_apartment");
        $model->parkings=$model->getPropUnitParkings()->all();
        if(!count($model->parkings)){
            $model->parkings[]=new PropUnitParking();
        }
        if (isset($_POST['PropUnitParking'])) {
            $model->parkings=array();
            foreach($_POST['PropUnitParking']['no_of_vehicles'] as $i=>$name ){
                if($name>0) {
                    $parid = $_POST['PropUnitParking']['id'][$i];
                    $prevpap = PropUnitParking::findOne(["id" => $parid, "prop_unit_id" => $id]);
                    if (!$prevpap) {
                        $prevpap = new PropUnitParking();
                    }
                    $prevpap->prop_unit_id = $model->id;
                    $prevpap->no_of_vehicles = $_POST['PropUnitParking']['no_of_vehicles'][$i];
                    $prevpap->parking_type = $_POST['PropUnitParking']['parking_type'][$i];
                    $prevpap->vehicle_type = $_POST['PropUnitParking']['vehicle_type'][$i];
                    $prevpap->location = $_POST['PropUnitParking']['location'][$i];
                    //$prevpap->save();
                    $model->parkings[] = $prevpap;
                }
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->saveParking())
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'park']);
        } else {
            return $this->render('parkingform', [
                'model' => $model,
            ]);
        }
    }

    public function actionAttachmentform($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("formAttachment_".strtolower($proptype));
        //$model->setScenario("formPartition_apartment");
        $model->attachments=$model->getPropUnitAttaches()->all();
        if(!count($model->attachments)){
            $model->attachments[]=new PropUnitAttach();
        }
        if (isset($_POST['PropUnitAttach'])) {
            $model->attachments=array();
            foreach($_POST['PropUnitAttach']['name'] as $i=>$name ){
                $parid = $_POST['PropUnitAttach']['id'][$i];
                $prevpap = PropUnitAttach::findOne(["id" => $parid, "prop_unit_id" => $id]);
                if (!$prevpap) {
                    $prevpap = new PropUnitAttach();
                }else{
                    $filepath=$prevpap->file_path;
                }
                $prevpap->prop_unit_id = $model->id;
                $prevpap->name = $_POST['PropUnitAttach']['name'][$i];
                $prevpap->attach_type = $_POST['PropUnitAttach']['attach_type'][$i];

                if(isset(UploadedFile::getInstances($prevpap,'file_path')[$i])) {
                    $prevpap->file_path = UploadedFile::getInstances($prevpap, 'file_path')[$i];

                    //$prevpap->save();
                    $prevpap->file_path->name = $prevpap->file_path->baseName . rand(10000000, 999999999) . '.' . $prevpap->file_path->extension;
                    $prevpap->file_path->saveAs('uploads/' . $prevpap->file_path->name);
                }
               // Yii::trace(VarDumper::dumpAsString($filepath),'vardump');
                if(!$prevpap->file_path&&isset($filepath)){
                    $prevpap->file_path=$filepath;
                }
                $model->attachments[] = $prevpap;
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->saveAttachments())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('attachmentform', [
                'model' => $model,
            ]);
        }
    }


    public function actionFinanceform($id)
    {
        $model =  $this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("formFinance_".strtolower($proptype));
        $model->finance=PropUnitFinance::findOne(["prop_unit_id"=>$id]);
        if(!$model->finance){
            $model->finance=new PropUnitFinance();
        }
        if (isset($_POST['PropUnitFinance'])) {
            $model->finance->setAttributes($_POST['PropUnitFinance']);
            $model->finance->prop_unit_id=$id;
            if($model->finance->save())
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'fin']);
        }  {
            return $this->render('financeform', [
                'model' => $model,
            ]);
        }
    }

    public function actionInventoryform($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("formInventory_".strtolower($proptype));
        //$model->setScenario("formPartition_apartment");
        $model->inventories=$model->getPropUnitInventories()->all();
        if(!count($model->inventories)){
            $model->inventories[]=new PropUnitInventory();
        }
        if (isset($_POST['PropUnitInventory'])) {
            $model->inventories=array();
            foreach($_POST['PropUnitInventory']['inventory'] as $i=>$name ){
                if($name>0) {
                    $parid = $_POST['PropUnitInventory']['id'][$i];
                    $prevpap = PropUnitInventory::findOne(["id" => $parid, "prop_unit_id" => $id]);
                    if (!$prevpap) {
                        $prevpap = new PropUnitInventory();
                    }
                    $prevpap->prop_unit_id = $model->id;
                    $prevpap->inventory = $_POST['PropUnitInventory']['inventory'][$i];
                    $prevpap->qty = $_POST['PropUnitInventory']['qty'][$i];
                    $prevpap->location = $_POST['PropUnitInventory']['location'][$i];
                    $prevpap->unit = $_POST['PropUnitInventory']['unit'][$i];
                    $prevpap->brand = $_POST['PropUnitInventory']['brand'][$i];
                    $prevpap->remarks = $_POST['PropUnitInventory']['remarks'][$i];
                    //$prevpap->save();
                    $model->inventories[] = $prevpap;
                }
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->saveInventories())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('inventoryform', [
                'model' => $model,
            ]);
        }
    }



    public function actionAllocatetenant($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;

        $model->setScenario("allocateTenant_".strtolower($proptype));

        //$model->setScenario("create_apartment");
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/realty/property/view', 'id' => $model->getProperty()->one()->id]);
        } else {
            return $this->render('allocateTenant', [
                'model' => $model,
            ]);
        }
    }
    public function actionAllocateexecutive($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;

        $model->setScenario("allocateExecutive_".strtolower($proptype));

        //$model->setScenario("create_apartment");
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/realty/property/view', 'id' => $model->getProperty()->one()->id,'viewtab'=>'punits']);
        } else {
            return $this->render('allocateExecutive', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetmember($propid){
        $sql="select u.username,u.email,u.id from  property_units pu,property p ,user u where pu.property_id=p.id
        and p.member_id=u.id  and pu.id=:id ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$propid);
        return json_encode($cmd->queryOne());
    }


    public function actionInterior($id)
    {
        $model=$this->findModel($id);
        //$proptype=$model->getProperty()->one()-> getPropType()->one()->name;

        //$model->setScenario("allocateExecutive_".strtolower($proptype));

        //$model->setScenario("create_apartment");
        if (isset($_POST['PropUnitInterior'])) {
            $model->interior=array();
            foreach($_POST['PropUnitInterior']['heading'] as $i=>$head ){


                if(!empty($head)) {
                    $int=new PropUnitInterior();
                   // Yii::trace(VarDumper::dumpAsString($id),'vardump');
                   // Yii::trace(VarDumper::dumpAsString($id),'vardump');
                    $int->category=$_POST['PropUnitInterior']['category'][$i];
                    $int->heading=$_POST['PropUnitInterior']['heading'][$i];
                    $int->description=$_POST['PropUnitInterior']['description'][$i];

                    $model->interior[] = $int;
                }
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->saveInterior())
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'int']);
        }
        {
            return $this->render('interior', [
                'model' => $model,
            ]);
        }
    }

}
