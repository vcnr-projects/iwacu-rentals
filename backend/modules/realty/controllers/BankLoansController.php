<?php

namespace app\modules\realty\controllers;

use Yii;
use app\modules\realty\models\BankLoans;
use app\modules\realty\models\BankLoansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BankLoansController implements the CRUD actions for BankLoans model.
 */
class BankLoansController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all BankLoans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BankLoansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BankLoans model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BankLoans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BankLoans();

        if ($model->load(Yii::$app->request->post())  ) {
            $model->icon=UploadedFile::getInstance($model,'icon');
            if($model->icon&&$model->icon->saveAs('uploads/' . $model->icon->name)) {

            }

            if($model->save())
                return $this->redirect(['view', 'id' => $model->loan]);
        }  {
        return $this->render('create', [
            'model' => $model,
        ]);
        }
    }

    /**
     * Updates an existing BankLoans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $icon=$model->icon;
        if ($model->load(Yii::$app->request->post())  ) {

            $model->icon=UploadedFile::getInstance($model,'icon');
            //Yii::trace(VarDumper::dumpAsString($model->icon),'vardump');
            if($model->icon&&$model->icon->saveAs('uploads/' . $model->icon->name)) {

                //$model->icon=$model->icon->name;
            }else{
                $model->icon=$icon;
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->loan]);
        }  {
        return $this->render('update', [
            'model' => $model,
        ]);
        }
    }

    /**
     * Deletes an existing BankLoans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BankLoans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BankLoans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BankLoans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
