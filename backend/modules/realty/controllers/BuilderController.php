<?php

namespace app\modules\realty\controllers;

use Yii;
use app\modules\realty\models\Builder;
use app\modules\realty\models\BuilderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BuilderController implements the CRUD actions for Builder model.
 */
class BuilderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Builder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BuilderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Builder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Builder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Builder();
        //$model->scenario='create';

        if ($model->load(Yii::$app->request->post()) ) {
            $model->adrs->setAttributes($_POST['Address']);
            $model->tel->setAttributes($_POST['Telephone']);
            $model->logo = UploadedFile::getInstance($model, 'logo');
            $model->banner_img = UploadedFile::getInstance($model, 'banner_img');
            if($model->logo){
                $file=$model->logo;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);

            }
            if($model->banner_img){
                $file=$model->banner_img;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);

            }

            if( $model->saver())
                return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Builder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $logo=$model->logo;
        $banner_img=$model->banner_img;
        if ($model->load(Yii::$app->request->post()) ) {
            $model->adrs->setAttributes($_POST['Address']);
            $model->tel->setAttributes($_POST['Telephone']);
            $model->logo = UploadedFile::getInstance($model, 'logo');
            $model->banner_img = UploadedFile::getInstance($model, 'banner_img');
            if($model->logo){
                $file=$model->logo;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);

            }else{
                $model->logo=$logo;
            }
            if($model->banner_img){
                $file=$model->banner_img;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);

            }else{
                $model->banner_img=$banner_img;
            }

            if( $model->saver())
                return $this->redirect(['view', 'id' => $model->id]);
        }

        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Builder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Builder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Builder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Builder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
