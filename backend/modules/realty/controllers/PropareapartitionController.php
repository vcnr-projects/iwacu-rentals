<?php

namespace app\modules\realty\controllers;

use app\modules\realty\models\PropAreaPartition;
use app\modules\realty\models\PropUnitInventory;
use yii\filters\VerbFilter;

class PropareapartitionController extends \yii\web\Controller
{


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete','inventoryform'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','inventoryform'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','inventoryform'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
        $searchModel = new PropAreaPartitionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInventoryform($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getProperty()->one()-> getPropType()->one()->name;
        $model->setScenario("formInventory_".strtolower($proptype));
        //$model->setScenario("formPartition_apartment");
        $model->inventories=$model->getPropUnitInventories()->all();
        if(!count($model->inventories)){
            $model->inventories[]=new PropUnitInventory();
        }
        if (isset($_POST['PropUnitInventory'])) {
            $model->inventories=array();
            foreach($_POST['PropUnitInventory']['inventory'] as $i=>$name ){
                if($name>0) {
                    $parid = $_POST['PropUnitInventory']['id'][$i];
                    $prevpap = PropUnitInventory::findOne(["id" => $parid, "prop_unit_id" => $id]);
                    if (!$prevpap) {
                        $prevpap = new PropUnitInventory();
                    }
                    $prevpap->prop_unit_id = $model->id;
                    $prevpap->inventory = $_POST['PropUnitInventory']['inventory'][$i];
                    $prevpap->qty = $_POST['PropUnitInventory']['qty'][$i];
                    $prevpap->location = $_POST['PropUnitInventory']['location'][$i];
                    $prevpap->unit = $_POST['PropUnitInventory']['unit'][$i];
                    $prevpap->brand = $_POST['PropUnitInventory']['brand'][$i];
                    $prevpap->remarks = $_POST['PropUnitInventory']['remarks'][$i];
                    //$prevpap->save();
                    $model->inventories[] = $prevpap;
                }
            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            if($model->saveInventories())
                return $this->redirect(['/realty/propertyunits/view', 'id' => $model->getPunit()->one()->id,'viewtab'=>'off']);
        } else {
            return $this->render('inventoryform', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = PropAreaPartition::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
