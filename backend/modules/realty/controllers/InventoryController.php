<?php

namespace app\modules\realty\controllers;

use app\modules\realty\models\InventoryBrands;
use Yii;
use app\modules\realty\models\Inventory;
use app\modules\realty\models\InventorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InventoryController implements the CRUD actions for Inventory model.
 */
class InventoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Inventory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InventorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inventory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Inventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Inventory();
        $model->brandnames=$model->getInventoryBrands()->all();
        if(!count($model->brandnames)){
            $model->brandnames[]=new InventoryBrands();
        }

        if ($model->load(Yii::$app->request->post()) ) {

            $model->brandnames=[];
            if(!empty($_POST['InventoryBrands']['brand']))
            foreach($_POST['InventoryBrands']['brand'] as $i=>$brand){

                //$parid = (isset($_POST['PropertyAmenities']['id'][$i]))?$_POST['PropertyAmenities']['id'][$i]:"";

                $prevpap = new InventoryBrands();

               // $prevpap->inventory = $model->id;
                $prevpap->brand = $_POST['InventoryBrands']['brand'][$i];
                //$prevpap->save();
                $model->brandnames[] = $prevpap;
                //$prevpap->save();

            }

            if($model->saveInventoryBrands())
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Inventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->brandnames=$model->getInventoryBrands()->all();
        if(!count($model->brandnames)){
            $model->brandnames[]=new InventoryBrands();
        }
        if ($model->load(Yii::$app->request->post()) ) {

            $model->brandnames=array();
            if(!empty($_POST['InventoryBrands']['brand']))
            foreach($_POST['InventoryBrands']['brand'] as $i=>$brand){

                //$parid = (isset($_POST['PropertyAmenities']['id'][$i]))?$_POST['PropertyAmenities']['id'][$i]:"";

                    $prevpap = new InventoryBrands();

                $prevpap->inventory = $model->id;
                $prevpap->brand = $_POST['InventoryBrands']['brand'][$i];
                //$prevpap->save();
                $model->brandnames[] = $prevpap;


            }

            if( $model->saveInventoryBrands())
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Inventory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inventory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetcatbrands($cat){
        $sql="select brandname from brand_names where category=:cat";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":cat",$cat);
        $result=$cmd->queryColumn();
        return json_encode($result);
    }
}
