<?php

namespace app\modules\realty\controllers;

use Yii;
use app\modules\realty\models\PropertyPartitionType;
use app\modules\realty\models\PropertyPartitionTypeSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PropertypartitiontypeController implements the CRUD actions for PropertyPartitionType model.
 */
class PropertypartitiontypeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PropertyPartitionType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertyPartitionTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PropertyPartitionType model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertyPartitionType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PropertyPartitionType();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->icon=UploadedFile::getInstance($model,'icon');
            if($model->icon&&$model->icon->saveAs('uploads/' . $model->icon->name)) {

            }
            if($model->save())
            return $this->redirect(['view', 'id' => $model->partition_type]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PropertyPartitionType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $icon=$model->icon;
        if ($model->load(Yii::$app->request->post())) {
            $model->icon=UploadedFile::getInstance($model,'icon');

            if($model->icon&&$model->icon->saveAs('uploads/' . $model->icon->name)){
                //$model->icon=$model->icon->name;
            }else{
                Yii::trace(VarDumper::dumpAsString($model->icon),'vardump');
                $model->icon=$icon;
            }

            if($model->save())
            return $this->redirect(['view', 'id' => $model->partition_type]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PropertyPartitionType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PropertyPartitionType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PropertyPartitionType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyPartitionType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
