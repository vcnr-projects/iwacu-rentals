<?php

namespace app\modules\realty\controllers;

use app\modules\realty\models\PropertiesPics;
use Yii;
use app\modules\realty\models\Properties;
use app\modules\realty\models\PropertiesSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PropertiesController implements the CRUD actions for Properties model.
 */
class PropertiesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Properties models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Properties model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Properties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Properties();


        if (isset($_POST['Properties']))  {

                $model->load(Yii::$app->request->post());
                $model->adrs->setAttributes($_POST['Address']);
                //$model->tel->setAttributes($_POST['Telephone']);

            $model->master_plan = UploadedFile::getInstance($model, 'master_plan');
            if($model->master_plan){
                $file=$model->master_plan;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
            }

                if(!empty($_FILES['Properties']['name']['image'][0])) {
                    $model->image = UploadedFile::getInstances($model, 'image');
                    $cm = '';
                    foreach ($model->image as $file) {

                        $file->name = $file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                        $file->saveAs('uploads/' . $file->name);
                        $cm .= $file . "^m";


                        $pics=new PropertiesPics();
                        $pics->pic=$file->name;
                        $model->pics[]=$pics;

                    }
                    $model->image = $cm;
                }

                if ($model->saver())

                        return $this->redirect(['view', 'id' => $model->id]);

        }  {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Properties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $image=$model->image;
        $masterplan=$model->master_plan;
        $model->pics=$model->getPropertiesPics();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->adrs->setAttributes($_POST['Address']);


            $model->master_plan = UploadedFile::getInstance($model, 'master_plan');
            if($model->master_plan){
                $file=$model->master_plan;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
            }else{
                $model->master_plan=$masterplan;
            }



            $model->pics=[];

            $model->image = UploadedFile::getInstances($model, 'image');
            $cm='';
            foreach ($model->image as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
                $cm.=$file."^m";

                $pics=new PropertiesPics();

                $pics->pic=$file->name;
                $model->pics[]=$pics;
            }
            Yii::trace(VarDumper::dumpAsString( count($model->pics)),'vardump');
            if(empty($cm)){
                $model->image=$image;
            }else
                $model->image=$cm;
            if($model->saver())
                return $this->redirect(['view', 'id' => $model->id]);
        }  {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Properties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Properties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Properties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Properties::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public  function actionProjectdetails($name,$city){
        $sql="select p.*,a.* from properties p , address a where p.address_id=a.id and p.name=:name and a.city=:city";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":name",$name);
        $cmd->bindValue(":city",$city);
        $res=$cmd->queryOne();
        return json_encode($res);
    }
}
