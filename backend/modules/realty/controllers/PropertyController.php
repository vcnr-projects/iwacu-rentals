<?php

namespace app\modules\realty\controllers;

use app\modules\realty\models\PropertiesPics;
use app\modules\realty\models\PropertyAccessibilities;
use app\modules\realty\models\PropertyAmenities;
use app\modules\realty\models\PropertyFacilities;
use backend\components\AccessRule;
use Yii;
use app\modules\realty\models\Property;
use app\modules\realty\models\PropertySearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index','view','create','update'
                    ,'delete'
                    ,'amenitiesform'
                    ,'accessibilitesform'
                    ,'arojectdetails'
                    ,'getlocalitydesc'
                    ,'projectsofbulider'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'delete'
                            ,'amenitiesform'
                            ,'accessibilitesform'
                            ,'arojectdetails'
                            ,'getlocalitydesc'
                            ,'projectsofbulider'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'delete'
                            ,'amenitiesform'
                            ,'accessibilitesform'
                            ,'arojectdetails'
                            ,'getlocalitydesc'
                            ,'projectsofbulider'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertySearch();

        if(isset($_GET["builder"])){
            $searchModel->mtype="Builder";
            //Yii::$app->request->queryParams["mtype"]="Builder";
        }else{
            $searchModel->mtype="Member";
        }


        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Property model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getPropType()->one()->name;

        $model->setScenario("view_".$proptype);
        $model->amenities=$model->getPropertyAmenities()->all();
        //$model->facilities=$model->getPropertyFacilities()->all();
        $model->accessibilities=$model->getPropertyAccessibilities()->all();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $wiz = (isset($_GET['wiz'])) ? true : false;


        $model = new Property();
        $model->member_id=(isset($_GET["member_id"]))?$_GET["member_id"]:null;

        if(isset($_GET["builder"])){
            $model->scenario="Builder";
        }


        if (isset($_POST['Property']))  {

            $model->load(Yii::$app->request->post());
            //Yii::trace(VarDumper::dumpAsString($model->no_block),'vardump');


            $model->bedroom_type=$_POST['Property']['bedroom_type'];
            //$model->servant_room=$_POST['Property']['servant_room'];
            //$model->servant_room=$_POST['Property']['no_bathroom'];

            if(!empty($model->approvals)){
                $model->approvals=implode(",",$model->approvals);
            }
            if(!empty($model->loan)){
                $model->loan=implode(",",$model->loan);
            }


            $model->adrs->setAttributes($_POST['Address']);
            $model->tel->setAttributes($_POST['Telephone']);

            $model->master_plan = UploadedFile::getInstance($model, 'master_plan');
            if($model->master_plan){
                $file=$model->master_plan;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
            }


            $model->image = UploadedFile::getInstances($model, 'image');
            $cm='';

            $model->pics=[];
            if(isset($_POST["uploadcopy"])){
                $sql="select p.id from property p, address a
where p.address_id=a.id  and p.name=:name and a.city=:city limit 1";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":name",$model->name);
                $cmd->bindValue(":city",$model->adrs->city);
                $ps=$cmd->queryScalar();
                $sql="select pp.* from properties_pics pp where prop_id=:id ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$ps);
                $pps=$cmd->queryAll();
                foreach($pps as $pp){
                    $pics=new PropertiesPics();
                    $pics->pic=$pp["pic"];
                    $pics->type=$pp["type"];
                    //$pics->pic=$file->name;
                    $model->pics[]=$pics;
                }


            }
            foreach ($model->image as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
                $cm.=$file."^m";

                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $model->pics[]=$pics;

            }
            $model->image=$cm;



            //begin: multipe uploads
            $images=UploadedFile::getInstancesByName('image[master_plan]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Master Plan";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Master Plan"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[floor_plan]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Floor Plan";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Floor Plan"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[gallery]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Gallery";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Gallery"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[brochure]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Brochure";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Brochure"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            /*$images=UploadedFile::getInstancesByName('image[brochure]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Brochure";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Brochure"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }*/


            /*$images=UploadedFile::getInstancesByName('image[video]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Walkthrough Video";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Walkthrough Video"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }*/

            if(!empty($_POST['image']['video'][0])){
                $pics=new PropertiesPics();
                $pics->pic=$_POST['image']['video'][0];
                $pics->type="Walkthrough Video";
                $model->pics[]=$pics;
            }


            $images=UploadedFile::getInstancesByName('image[aform]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Allotment Form";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Allotment Form"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[doc]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="List of documents";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"List of documents"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }



            $images=UploadedFile::getInstancesByName('image[csp]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Construction Status Photos";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Construction Status Photos"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[banner]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Banner Image";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Banner Image"])->all();
                if($files)
                    array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            //end: multipe uploads




            if($model->saver())
                if($wiz){
                    $sql="select id from property_units where property_id=:id  limit 1";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$model->id);
                    $res=$cmd->queryOne();
                    if($res){
                        return $this->redirect(['/realty/propertyunits/interior', 'id' => $res["id"], 'wiz' => 1]);
                    }
                    else
                        return $this->redirect(['view', 'id' => $model->id]);
                }
                else
                    return $this->redirect(['view', 'id' => $model->id]);
        }
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $image=$model->image;
        $masterplan=$model->master_plan;
        $model->pics=$model->getPropertiesPics();


        if ($model->load(Yii::$app->request->post()) ) {
            $model->bedroom_type=$_POST['Property']['bedroom_type'];
            //$model->servant_room=$_POST['Property']['servant_room'];
            //$model->no_bathroom=$_POST['Property']['no_bathroom'];

            $model->adrs->setAttributes($_POST['Address']);
            $model->tel->setAttributes($_POST['Telephone']);

            if(!empty($model->approvals)){
                $model->approvals=implode(",",$model->approvals);
            }
            if(!empty($model->loan)){
                $model->loan=implode(",",$model->loan);
            }

            $model->master_plan = UploadedFile::getInstance($model, 'master_plan');
            if($model->master_plan){
                $file=$model->master_plan;
                $file->name = $file->baseName . date('Y-m-d H:i:s'). '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
            }else{
                $model->master_plan=$masterplan;
            }

            $model->pics=[];
            $model->image = UploadedFile::getInstances($model, 'image');
            $cm='';
            foreach ($model->image as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);
                $cm.=$file."^m";

                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $model->pics[]=$pics;

            }
            if(empty($cm)){
                $model->image=$image;
            }else
            $model->image=$cm;


            //begin: multipe uploads
            $images=UploadedFile::getInstancesByName('image[master_plan]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Master Plan";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Master Plan"])->all();
                if($files)
                    $model->pics=array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[floor_plan]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Floor Plan";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Floor Plan"])->all();
                if($files)
                    $model->pics=array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[gallery]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Gallery";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Gallery"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[brochure]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Brochure";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Brochure"])->all();
                if($files)
                    $model->pics=array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            /*$images=UploadedFile::getInstancesByName('image[brochure]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Brochure";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Brochure"])->all();
                if($files)
                    $model->pics=array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }*/


            /*$images=UploadedFile::getInstancesByName('image[video]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Walkthrough Video";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Walkthrough Video"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }*/

            $files= $model->getPropertiesPics()->filterWhere(["type"=>"Walkthrough Video"])->all();
            if($files){
               if(isset($files[0])&&isset($_POST['image']['video'][0])){
                   $files[0]->pic=$_POST['image']['video'][0];
                   $model->pics[]=$files[0];
               }
            }else if(!empty($_POST['image']['video'][0])){
                $pics=new PropertiesPics();
                $pics->pic=$_POST['image']['video'][0];
                $pics->type="Walkthrough Video";
                $model->pics[]=$pics;
            }



            $images=UploadedFile::getInstancesByName('image[aform]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Allotment Form";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Allotment Form"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[doc]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="List of documents";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"List of documents"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }


            $images=UploadedFile::getInstancesByName('image[csp]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Construction Status Photos";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Construction Status Photos"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }

            $images=UploadedFile::getInstancesByName('image[banner]');
            $mp=0;
            foreach ($images as $file) {
                $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
                $file->saveAs('uploads/' . $file->name);


                $pics=new PropertiesPics();
                $pics->pic=$file->name;
                $pics->type="Banner Image";
                $model->pics[]=$pics;
                $mp++;

            }
            if($mp==0){
                $files= $model->getPropertiesPics()->filterWhere(["type"=>"Banner Image"])->all();
                if($files)
                    $model->pics= array_merge($model->pics,$files);
                Yii::trace(VarDumper::dumpAsString($model->pics),'vardump');
            }

            //end: multipe uploads


            if($model->saver())
            return $this->redirect(['view', 'id' => $model->id]);
        }  {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAmenitiesform($id)
    {

        $model=$this->findModel($id);

        $proptype=$model->getPropType()->one()->name;
        $model->setScenario("formAmenities_".strtolower($proptype));
        $model->amenities=$model->getPropertyAmenities()->all();
        $model->facilities=$model->getPropertyFacilities()->all();
        if(!count($model->amenities)){
            $model->amenities[]=new PropertyAmenities();
        }
        if(!count($model->facilities)){
            $model->facilities[]=new PropertyFacilities();
        }

        if (isset($_POST['PropertyAmenities'])||isset($_POST['PropertyFacilities'])) {
            $model->amenities=array();
            if(!empty($_POST['PropertyAmenities']['amenity']))
            foreach($_POST['PropertyAmenities']['amenity'] as $i=>$amenity){

                    //$parid = (isset($_POST['PropertyAmenities']['id'][$i]))?$_POST['PropertyAmenities']['id'][$i]:"";
                    $prevpap = PropertyAmenities::findOne(["amenity" => $amenity, "property_id" => $id]);
                    if (!$prevpap) {
                        $prevpap = new PropertyAmenities();
                    }
                    $prevpap->property_id = $model->id;
                    $prevpap->amenity = $_POST['PropertyAmenities']['amenity'][$i];
                    //$prevpap->save();
                    $model->amenities[] = $prevpap;

            }

            $model->facilities=array();
            if(!empty($_POST['PropertyFacilities']['amenity']))
                foreach($_POST['PropertyFacilities']['amenity'] as $i=>$amenity){

                //$parid = (isset($_POST['PropertyAmenities']['id'][$i]))?$_POST['PropertyAmenities']['id'][$i]:"";
                $prevpap = PropertyFacilities::findOne(["amenity" => $amenity, "property_id" => $id]);
                if (!$prevpap) {
                    $prevpap = new PropertyFacilities();
                }
                $prevpap->property_id = $model->id;
                $prevpap->amenity = $_POST['PropertyFacilities']['amenity'][$i];
                //$prevpap->save();
                $model->facilities[] = $prevpap;

            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            //Yii::trace(VarDumper::dumpAsString($model))
            if($model->saveAmenities())
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>"off"]);
        }  {
            return $this->render('amenitiesform', [
                'model' => $model,
            ]);
        }
    }

    public function actionAccessibilitesform($id)
    {
        $model=$this->findModel($id);
        $proptype=$model->getPropType()->one()->name;
        $model->setScenario("formAccessibilites_".strtolower($proptype));
        $model->accessibilities=$model->getPropertyAccessibilities()->all();
        if(!count($model->accessibilities)){
            $model->accessibilities[]=new PropertyAccessibilities();
        }
        if (isset($_POST['PropertyAccessibilities'])) {
            $model->accessibilities=array();
            foreach($_POST['PropertyAccessibilities']['accessibility'] as $i=>$name ){

                $parid = (isset($_POST['PropertyAmenities']['id'][$i]))?$_POST['PropertyAmenities']['id'][$i]:"";
                $prevpap = PropertyAccessibilities::findOne(["id" => $parid, "property_id" => $id]);
                if (!$prevpap) {
                    $prevpap = new PropertyAccessibilities();
                }
                $prevpap->property_id = $model->id;
                $prevpap->accessibility = $_POST['PropertyAccessibilities']['accessibility'][$i];
                $prevpap->remaks = $_POST['PropertyAccessibilities']['remarks'][$i];
                //$prevpap-accessibility
                $model->accessibilities[] = $prevpap;

            }

            /*$model->setAttributes($_POST['PropertyUnits']);*/
            //Yii::trace(VarDumper::dumpAsString($model))
            if($model->saveAccessibilities())
                return $this->redirect(['view', 'id' => $model->id]);
        }  {
        return $this->render('accessibilitiesform', [
            'model' => $model,
        ]);
    }
    }


    public  function actionProjectdetails($name,$city){
        $sql="select p.*,a.*,p.id as pid
,date_format(p.proj_start_date,'%d-%m-%Y')as start_date
,date_format(p.poss_date,'%d-%m-%Y')as poccession_date
from property p , address a
where p.address_id=a.id  and p.name=:name and a.city=:city";


        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":name",$name);
        $cmd->bindValue(":city",$city);
        $res=$cmd->queryOne();
        if($res) {
            $sql = "select  pp.* from  property p,properties_pics pp where  pp.prop_id=p.id and p.id=:id ";
            $cmd = Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id",$res["pid"]);
            $res["pics"]=$cmd->queryAll();
        }

        return json_encode($res);
    }

    public  function actionGetlocalitydesc($locality){
        $sql="select p.locality_desc from property p where locality like :locality order by id desc limit 1 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":locality",$locality);
        $res=$cmd->queryOne();
        return json_encode($res);
    }


    public  function actionProjectsofbulider($name,$city){
        $sql="select p.name from property p , address a where p.address_id=a.id and p.builder_name=:name and a.city=:city";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":name",$name);
        $cmd->bindValue(":city",$city);
        $res=$cmd->queryColumn();
        return json_encode($res);
    }

    public  function actionDeleteproppics($id){
        Yii::$app->response->format=Response::FORMAT_JSON;
        if(isset($_POST["deleteId"])){
            if(PropertiesPics::deleteAll("prop_id=:pid and id=:id",[":pid"=>$id,":id"=>$_POST["deleteId"]])){

                return ["success"=>true];
            }
        }
        return ["success"=>false];
    }


    public  function actionAddproppics($id){
        Yii::$app->response->format=Response::FORMAT_JSON;
        $model=$this->findModel($id);
        $file=UploadedFile::getInstanceByName('file');
        if($file){
            $file->name=$file->baseName . rand(10000000, 999999999) . '.' . $file->extension;
            if($file->saveAs('uploads/' . $file->name)){
                $pp=new PropertiesPics();
                $pp->prop_id=$id;
                $pp->type="Gallery";
                $pp->pic=$file->name;
                if($pp->save()){
                    return ["success"=>true,"sid"=>$pp->id,"file_name"=>Url::to(['/uploads/'.$file->name])];
                }

            }

        }

        return ["success"=>false];
    }

}
