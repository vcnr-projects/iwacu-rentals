<?php

namespace app\modules\service\controllers;

use app\modules\service\models\ExecutiveActivityProperty;
use app\modules\service\models\ExecutiveActivityType;
use Yii;
use app\modules\service\models\TenantPropertyAllocation;
use app\modules\service\models\TenantPropertyAllocationSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TenantPropertyAllocationController implements the CRUD actions for TenantPropertyAllocation model.
 */
class TenantPropertyAllocationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete','activity'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','activity'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','activity'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TenantPropertyAllocation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TenantPropertyAllocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TenantPropertyAllocation model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TenantPropertyAllocation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TenantPropertyAllocation();
        $model->prop_id=(isset($_GET["prop_id"]))?$_GET["prop_id"]:"";

        if ($model->load(Yii::$app->request->post())  ) {


            $model->agreement = UploadedFile::getInstance($model, 'agreement');
            if ($model->agreement) {
                $model->agreement->name = $model->agreement->baseName . rand(10000000, 999999999) . '.' . $model->agreement->extension;
                $model->agreement->saveAs('uploads/' . $model->agreement->name);
            }

            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        }  {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TenantPropertyAllocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $agreement=$model->agreement;

        if ($model->load(Yii::$app->request->post()) ) {
            $model->agreement = UploadedFile::getInstance($model, 'agreement');
            if ($model->agreement) {
                $model->agreement->name = $model->agreement->baseName . rand(10000000, 999999999) . '.' . $model->agreement->extension;
                $model->agreement->saveAs('uploads/' . $model->agreement->name);
            }else{
                $model->agreement=$agreement;
            }

            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        }  {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TenantPropertyAllocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TenantPropertyAllocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TenantPropertyAllocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenantPropertyAllocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionActivity($id)
    {
        $model = $this->findModel($id);


        if (isset($_POST['ExecutiveActivityProperty']) ) {
            $eaps=[];

            if(isset($_POST['ExecutiveActivityProperty']['type']))
            foreach($_POST['ExecutiveActivityProperty']['type'] as $i => $type){
                $eat=ExecutiveActivityType::find()->filterWhere(['type'=>$type])->one();
                if($eat) {
                    Yii::trace(VarDumper::dumpAsString($type),'vardump');
                    Yii::trace(VarDumper::dumpAsString($eat),'vardump');

                    $eap = new ExecutiveActivityProperty();
                    $eap->type = $type;
                    $eap->tenant_service = $id;
                    $eap->recursion = $eat->recursion;
                    $eap->collection = $eat->collection;
                    $eap->payment = $eat->payment;
                    $eap->collection_day = $_POST['ExecutiveActivityProperty'][$type]['collection_day'];
                    $eap->remainder_days = $_POST['ExecutiveActivityProperty'][$type]['remainder_days'];
                    $eap->next_collection_date = $_POST['ExecutiveActivityProperty'][$type]['next_collection_date'];
                    if(strtotime($eap->next_collection_date)!==false){
                        $eap->next_collection_date=date('Y-m-d',strtotime($eap->next_collection_date));
                    }else {
                        $date = new \DateTime(date('Y-m-d', strtotime($model->start_date)));
                        $date->add(new \DateInterval("P{$eap->recursion}M"));
                        //echo $date->format('Y-m-d') . "\n";
                        Yii::trace(VarDumper::dumpAsString($eap->recursion), 'vardump');
                        Yii::trace(VarDumper::dumpAsString($date->format("Y-m-{$eap->collection_day}")), 'vardump');
                        $eap->next_collection_date = $date->format("Y-m-{$eap->collection_day}");
                    }
                    $eaps[]=$eap;
                }
            }

            if($model->saveEAP($eaps))
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'off']);
        }
        {
            return $this->render('activity', [
                'model' => $model,
            ]);
        }
    }


}
