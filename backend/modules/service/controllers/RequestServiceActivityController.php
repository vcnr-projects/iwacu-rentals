<?php

namespace app\modules\service\controllers;

use backend\components\AccessRule;
use Yii;
use app\modules\service\models\RequestServiceActivity;
use app\modules\service\models\RequestServiceActivitySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestServiceActivityController implements the CRUD actions for RequestServiceActivity model.
 */
class RequestServiceActivityController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','approve'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestServiceActivity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestServiceActivitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestServiceActivity model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RequestServiceActivity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestServiceActivity();

        if ($model->load(Yii::$app->request->post()))  {
            if($model->admin_approval==1){
                $model->admin_approval=Yii::$app->user->id;
            }
            if(!empty($model->tenant_id)){
                $sql="select * from tenant_property_allocation tpa,service s
 where tpa.prop_id=s.property_id and s.is_active=1 and tpa.id=:id";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$model->tenant_id);
                $res=$cmd->queryOne();
                $model->owner=$res["member_id"];
            }
            if( $model->save())
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RequestServiceActivity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            if(!empty($model->tenant_id)){
                $sql="select * from tenant_property_allocation tpa,service s
 where tpa.prop_id=s.property_id and s.is_active=1 and tpa.id=:id";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":id",$model->tenant_id);
                $res=$cmd->queryOne();
                $model->owner=$res["member_id"];
            }
            if($model->admin_approval==1){
                $model->admin_approval=Yii::$app->user->id;
            }
            if( $model->save())
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RequestServiceActivity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);//->delete();
        $model->status=-1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestServiceActivity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RequestServiceActivity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequestServiceActivity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->admin_approval=Yii::$app->user->id;
            $model->status=1;
            if( $model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('approve', [
                'model' => $model,
            ]);
        }
    }
}
