<?php

namespace app\modules\service\controllers;

use app\modules\member\models\Individual;
use app\modules\realty\models\Property;
use app\modules\realty\models\PropertyUnits;
use app\modules\service\models\ExecutiveActivityProperty;
use app\modules\service\models\ExecutiveActivityType;
use common\models\User;
use Yii;
use app\modules\service\models\Service;
use app\modules\service\models\ServiceSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ContractController implements the CRUD actions for Service model.
 */
class ContractController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete','activity'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','activity'],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','activity'],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();
        $model->contract_id=$model->getLatestContractID();
        $model->member_id=(isset($_REQUEST["member_id"]))?$_REQUEST["member_id"]:"";
        $model->property_id=(isset($_REQUEST["property_id"]))?$_REQUEST["property_id"]:"";
        $model->tos=(isset($_REQUEST["tos"]))?$_REQUEST["tos"]:"";
        $model->member_type=(isset($_REQUEST["member_type"]))?$_REQUEST["member_type"]:"";

        if ($model->load(Yii::$app->request->post())){

            /*$user=User::findOne($model->member_id);
            if($user->user_type=='Individual'){
                $individual=Individual::findOne(["member_id"=>$model->member_id]);
                return $this->redirect(['/member/individual/view', 'id' => $individual->id]);
            }
            if($user->user_type=='Company'){
                $individual=Company::findOne(["member_id"=>$model->member_id]);
                return $this->redirect(['/member/company/view', 'id' => $individual->id]);
            }
            if($user->user_type=='Tenant'){
                $individual=Tenant::findOne(["member_id"=>$model->member_id]);
                return $this->redirect(['/member/tenant/view', 'id' => $individual->id]);
            }*/


            $model->document = UploadedFile::getInstance($model, 'document');
            if ($model->document) {
                $model->document->name = $model->document->baseName . rand(10000000, 999999999) . '.' . $model->document->extension;
                $model->document->saveAs('uploads/' . $model->document->name);
            }


            if($model->save()) {
                $prop = PropertyUnits::findOne($model->property_id);
                $prop_id = $prop->getProperty()->one()->id;
                return $this->redirect(['/realty/property/view', 'id' => $prop_id, 'viewtab' => 'punits']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $redirect=(isset($_REQUEST["member_id"]))?true:false;
        $model = $this->findModel($id);
        $document=$model->document;

        if ($model->load(Yii::$app->request->post()) ) {


            $model->document = UploadedFile::getInstance($model, 'document');
            if ($model->document) {
                $model->document->name = $model->agreement->baseName . rand(10000000, 999999999) . '.' . $model->agreement->extension;
                $model->document->saveAs('uploads/' . $model->document->name);
            }else{
                $model->document=$document;
            }

            if( $model->save()) {
                if ($redirect) {
                    $prop = PropertyUnits::findOne($model->property_id);
                    $prop_id = $prop->getProperty()->one()->id;
                    return $this->redirect(['/realty/property/view', 'id' => $prop_id, 'viewtab' => 'punits']);


                    $user = User::findOne($model->member_id);
                    if ($user->user_type == 'Individual') {
                        $individual = Individual::findOne(["member_id" => $model->member_id]);
                        return $this->redirect(['/member/individual/view', 'id' => $individual->id]);
                    }
                    if ($user->user_type == 'Company') {
                        $individual = Company::findOne(["member_id" => $model->member_id]);
                        return $this->redirect(['/member/company/view', 'id' => $individual->id]);
                    }
                    if ($user->user_type == 'Tenant') {
                        $individual = Tenant::findOne(["member_id" => $model->member_id]);
                        return $this->redirect(['/member/tenant/view', 'id' => $individual->id]);
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }  {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionActivity($id)
    {
        $model = $this->findModel($id);


        if (isset($_POST['ExecutiveActivityProperty']) ) {
            $eaps=[];

            if(isset($_POST['ExecutiveActivityProperty']['type']))
                foreach($_POST['ExecutiveActivityProperty']['type'] as $i => $type){
                    $eat=ExecutiveActivityType::find()->filterWhere(['type'=>$type])->one();
                    if($eat) {
                        Yii::trace(VarDumper::dumpAsString($type),'vardump');
                        Yii::trace(VarDumper::dumpAsString($eat),'vardump');

                        $eap = new ExecutiveActivityProperty();
                        $eap->type = $type;
                        $eap->owner_service = $id;
                        $eap->recursion = $eat->recursion;
                        $eap->collection = $eat->collection;
                        $eap->payment = $eat->payment;
                        $eap->collection_day = $_POST['ExecutiveActivityProperty'][$type]['collection_day'];
                        $eap->remainder_days = $_POST['ExecutiveActivityProperty'][$type]['remainder_days'];
                        $eap->next_collection_date = $_POST['ExecutiveActivityProperty'][$type]['next_collection_date'];
                        if(strtotime($eap->next_collection_date)!==false){
                            $eap->next_collection_date=date('Y-m-d',strtotime($eap->next_collection_date));
                        }else {
                            $date = new \DateTime(date('Y-m-d', strtotime($model->date)));
                            $date->add(new \DateInterval("P{$eap->recursion}M"));
                            //echo $date->format('Y-m-d') . "\n";
                            Yii::trace(VarDumper::dumpAsString($eap->recursion), 'vardump');
                            Yii::trace(VarDumper::dumpAsString($date->format("Y-m-{$eap->collection_day}")), 'vardump');
                            $eap->next_collection_date = $date->format("Y-m-{$eap->collection_day}");
                        }
                        $eaps[]=$eap;
                    }
                }

            if($model->saveEAP($eaps))
                return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'off']);
        }
        {
            return $this->render('activity', [
                'model' => $model,
            ]);
        }
    }
}
