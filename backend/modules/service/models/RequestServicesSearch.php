<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\RequestServices;

/**
 * RequestServicesSearch represents the model behind the search form about `app\modules\service\models\RequestServices`.
 */
class RequestServicesSearch extends RequestServices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name', 'created_on'], 'safe'],
            [['is_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestServices::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_active' => $this->is_active,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'service_name', $this->service_name]);

        return $dataProvider;
    }
}
