<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "request_services".
 *
 * @property string $service_name
 * @property integer $is_active
 * @property string $created_on
 */
class RequestServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name'], 'required'],
            [['is_active'], 'integer'],
            [['created_on'], 'safe'],
            [['service_name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_name' => 'Service Name',
            'is_active' => 'Is Active?',
            'created_on' => 'Created On',
        ];
    }
}
