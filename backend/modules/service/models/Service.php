<?php

namespace app\modules\service\models;

use app\modules\realty\models\PropertyUnits;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "service".
 *
 * @property string $id
 * @property string $contract_id
 * @property string $date
 * @property string $expiry_date
 * @property string $property_id
 * @property string $tos
 * @property string $prop_type
 * @property string $ptype
 * @property integer $member_id
 * @property string $created_on
 * @property integer $tenant_id
 *
 * @property User $tenant
 * @property Property $property
 * @property User $member
 * @property ServiceType $tos0
 */
class Service extends \yii\db\ActiveRecord
{
    public $member_type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_id', 'date', 'expiry_date', 'tos', 'prop_type', 'ptype'], 'required'],
            [['date', 'expiry_date', 'created_on'], 'safe'],
            [['property_id', 'member_id', 'tenant_id', 'duration','is_active'], 'integer'],
            [['amount'], 'number'],
            [['contract_id'], 'string', 'max' => 50],
            [['tos', 'prop_type', 'ptype'], 'string', 'max' => 100],
            [['remarks'], 'safe'],
            [['payment_type'], 'string', 'max' => 20],
            [['document'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'Contract ID',
            'date' => 'Date',
            'expiry_date' => 'Expiry Date',
            'property_id' => 'Property',
            'tos' => 'Type of Service',
            'prop_type' => 'Property Type',
            'ptype' => 'Property Category',
            'member_id' => 'Member ID',
            'created_on' => 'Created ON',
            'tenant_id' => 'Tenant',
            'amount' => 'Amount',
            'duration' => 'Duration in months',
            'payment_type' => 'Payment Type',
            'is_active' => 'Is Active?',
            'remarks' => 'Remarks',
            'document' => 'Contract Document',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(User::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTos0()
    {
        return $this->hasOne(ServiceType::className(), ['name' => 'tos']);
    }

    public function getMemberID()
    {
        $member =$this->hasOne(User::className(), ['id' => 'member_id'])->one();
        if($member){
            return $member->username;
        }else{
            return null;
        }
    }

    public function getPropName()
    {
        $model=\app\modules\realty\models\PropertyUnits::findOne($this->property_id);

        if($model){
            return $model->name;
        }else{
            return null;
        }


    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(empty($this->contract_id)){
                $this->contract_id=$this->getLatestContractID();
            }

            if(strtotime($this->date)!==strtotime('01-01-1970')){
                $this->date=date('Y-m-d',strtotime($this->date));
            }else{
                $this->date=null;
            }

            if(strtotime($this->expiry_date)!==strtotime('01-01-1970')){
                $this->expiry_date=date('Y-m-d',strtotime($this->expiry_date));
            }else{
                $this->expiry_date=null;
            }


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(strtotime($this->expiry_date)==strtotime('01-01-1970')){
            $this->expiry_date=null;

        }else{
            $this->expiry_date=date('d-m-Y',strtotime($this->expiry_date));
        }
        if(strtotime($this->date)==strtotime('01-01-1970')){
            $this->date=null;

        }else{
            $this->date=date('d-m-Y',strtotime($this->date));
        }



        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }


    public function afterSave($insert, $changedAttributes){
        if($this->is_active==0) {
            $sql = "update tenant_property_allocation set is_active=0 where service_id=:id";
            $cmd = Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":id", $this->id);
            $cmd->execute();
        }
    }

    public static function getLatestContractID(){

        $pref="CONTR/";
        $lbn=0;
        $sql="select contract_id from service   order by id desc limit 1  ";
        $cmd = Yii::$app->db->createCommand($sql);
        $result = $cmd->queryAll();
        foreach ($result as $row){
            preg_match('/\d+$/',$row["contract_id"],$match);
            $lbn=$match[0];
        }
        $lbn++;
        if(date('m')<=3){
            $y=(date('Y')-1)."-".date('y');
        }else{
            $y=(date('Y'))."-".(date('y')+1);
        }
        $billno=$y."/".$pref.sprintf("%04d", $lbn);
        return $billno;

    }


    public function saveEAP($eaps){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $sql="select type,id
            from executive_activity_property where  owner_service=:ts ";
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":ts",$this->id);
            $res=$cmd->queryAll();
            $prevactivity=[];
            foreach($res as $r){
                //$prevactivity[$r["type"]]=$r["id"];
                $prevactivity[$r["type"]]=$r;
            }

            /* if(empty($eaps)){
                 $errmsg.="Atleast one Activity should be given";
             }else
             */
            {
                //ExecutiveActivityProperty::deleteAll(["tenant_service"=>$this->id]);
                foreach($eaps as $eap ){

                    if(isset($prevactivity[$eap->type])){
                        Yii::trace(VarDumper::dumpAsString($eap),'vardump');
                        //$eap->id=$prevactivity[$eap->type];
                        $eap->is_active=1;
                        $eap->isNewRecord=false;
                        $eap->oldAttributes=$prevactivity[$eap->type];
                        // unset($eap->oldAttributes);
                        Yii::trace(VarDumper::dumpAsString(($eap->oldAttributes)),'vardump');


                        unset($prevactivity[$eap->type]);
                    }
                    /*if($eap->collection==0&&$eap->payment==0){
                         $eap->is_payable=-1;
                     }*/

                    if(!$eap->save()){
                        foreach($eap->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                }

                foreach($prevactivity as $pa => $id){
                    $sql="update executive_activity_property set is_active=0 where id= :id ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$id["id"]);
                    $cmd->execute();
                }
            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;

    }


}
