<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\RequestServiceActivity;

/**
 * RequestServiceActivitySearch represents the model behind the search form about `app\modules\service\models\RequestServiceActivity`.
 */
class RequestServiceActivitySearch extends RequestServiceActivity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'property_id', 'tenant_id', 'member_id', 'user_id', 'exec_id', 'admin_approval', 'member_approval', 'is_payable', 'status'], 'integer'],
            [['name', 'description', 'start_date', 'end_date', 'priority'], 'safe'],
            [['proposed_cost', 'accepted_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestServiceActivity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'property_id' => $this->property_id,
            'tenant_id' => $this->tenant_id,
            'member_id' => $this->member_id,
            'user_id' => $this->user_id,
            'exec_id' => $this->exec_id,
            'proposed_cost' => $this->proposed_cost,
            'accepted_cost' => $this->accepted_cost,
            'admin_approval' => $this->admin_approval,
            'member_approval' => $this->member_approval,
            'is_payable' => $this->is_payable,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'priority', $this->priority]);

        return $dataProvider;
    }
}
