<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\TenantPropertyAllocation;

/**
 * TenantPropertyAllocationSearch represents the model behind the search form about `app\modules\service\models\TenantPropertyAllocation`.
 */
class TenantPropertyAllocationSearch extends TenantPropertyAllocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prop_id', 'tenant_id', 'is_active'], 'integer'],
            [['start_date', 'expiry_date', 'vacate_date', 'rent_date', 'rent_lease', 'next_collection_date', 'beneficiary', 'agreement'], 'safe'],
            [['advance', 'rent_amt', 'increment_perc'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantPropertyAllocation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'prop_id' => $this->prop_id,
            'tenant_id' => $this->tenant_id,
            'start_date' => $this->start_date,
            'expiry_date' => $this->expiry_date,
            'vacate_date' => $this->vacate_date,
            'rent_date' => $this->rent_date,
            'advance' => $this->advance,
            'rent_amt' => $this->rent_amt,
            'increment_perc' => $this->increment_perc,
            'next_collection_date' => $this->next_collection_date,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'rent_lease', $this->rent_lease])
            ->andFilterWhere(['like', 'beneficiary', $this->beneficiary])
            ->andFilterWhere(['like', 'agreement', $this->agreement]);

        return $dataProvider;
    }
}
