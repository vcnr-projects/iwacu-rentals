<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "service_type".
 *
 * @property string $name
 * @property integer $is_active
 *
 * @property Service[] $services
 */
class ServiceType extends \yii\db\ActiveRecord
{
    public $gratuity=[];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['gratuity'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Type of service',
            'is_active' => 'Is Active?',
            'gratuity' => 'Executive Activities',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['tos' => 'name']);
    }

    public function getGratuityServices()
    {
        return $this->hasMany(GratuityServices::className(), ['tos' => 'name']);
    }

    public function AfterSave($insert, $changedAttributes)
    {
        GratuityServices::deleteAll(["tos"=>$this->name]);
            if(!empty($this->gratuity)){

                foreach($this->gratuity as $g){
                    $gratuity=new GratuityServices();
                    $gratuity->tos=$this->name;
                    $gratuity->request_service=$g;
                    $gratuity->save();
                }
            }

        parent::afterSave($insert, $changedAttributes);
    }


    public function afterFind ()
    {

        $sql="select request_service from gratuity_services where tos=:tos";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":tos",$this->name);
        $this->gratuity=$cmd->queryColumn();

        parent::afterFind ();
    }


}
