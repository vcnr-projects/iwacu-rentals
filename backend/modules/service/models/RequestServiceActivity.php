<?php

namespace app\modules\service\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "request_service_activity".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $property_id
 * @property string $tenant_id
 * @property string $member_id
 * @property integer $user_id
 * @property integer $exec_id
 * @property string $priority
 * @property string $proposed_cost
 * @property string $accepted_cost
 * @property integer $admin_approval
 * @property integer $member_approval
 * @property integer $is_payable
 * @property integer $status
 *
 * @property User $exec
 * @property PropertyUnits $property
 * @property TenantPropertyAllocation $tenant
 * @property Service $member
 * @property User $user
 */
class RequestServiceActivity extends \yii\db\ActiveRecord
{
    public $statusText;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_service_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'end_date'], 'required'],
            [['description'], 'string'],
            [['start_date', 'end_date','statusText'], 'safe'],
            [['property_id', 'tenant_id', 'member_id', 'user_id', 'exec_id', 'admin_approval', 'member_approval', 'is_payable', 'status'], 'integer'],
            [['proposed_cost', 'accepted_cost'], 'number'],
            [['name'], 'string', 'max' => 200],
            [['priority'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Service Name',
            'description' => 'Description',
            'start_date' => 'Probable Start Date',
            'end_date' => 'Finish Date',
            'property_id' => 'Property',
            'tenant_id' => 'Tenant',
            'member_id' => 'Member',
            'user_id' => 'User',
            'exec_id' => 'Executive',
            'priority' => 'Priority',
            'proposed_cost' => 'Proposed Cost',
            'accepted_cost' => 'Accepted Cost',
            'admin_approval' => 'Admin Approval',
            'member_approval' => 'Member Approval',
            'is_payable' => 'Is Payable',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(TenantPropertyAllocation::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Service::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    
    public  function getStatusText(){
        $this->statusText='';
        switch ($this->status*1){
            case 0 :
                $this->statusText="Pending Admin Approval";
                break;
            case 1:
                $this->statusText="Pending Member/Authorized Approval";
                break;
            case 2:
                $this->statusText="Pending for Work-Start";
                break;
            case 3:
                $this->statusText="Work Started";
                break;
            case 3:
                $this->statusText="Work Finished";
                break;
            case -1:
                $this->statusText="Cancelled";
                break;
            default:
                $this->statusText=$this->status*1;
        }
        return $this->statusText;
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->start_date)!==strtotime('01-01-1970')){
                $this->start_date=date('Y-m-d',strtotime($this->start_date));
            }else{
                $this->start_date=null;
            }

            if(strtotime($this->end_date)!==strtotime('01-01-1970')){
                $this->end_date=date('Y-m-d',strtotime($this->end_date));
            }else{
                $this->end_date=null;
            }


            if(!$this->exec_id) {
                if ($this->member_id) {
                    $sql = "
select ep.exec_id from
 executive_properties ep,service s
where ep.service_id=s.id and  s.id=:property_id and s.is_active=1 and ep.is_active=1

";
                    $cmd = Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":property_id", $this->member_id);
                    $res = $cmd->queryOne();
                    if ($res) {
                        $this->exec_id = $res["exec_id"];
                    }
                }
                if ($this->tenant_id) {
                    $sql = "
select ep.exec_id from
 executive_properties ep,service s,tenant_property_allocation tpa
where ep.service_id=s.id  and s.is_active=1 and ep.is_active=1
and tpa.service_id=s.id and tpa.id=:id

";
                    $cmd = Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id", $this->tenant_id);
                    $res = $cmd->queryOne();
                    if ($res) {
                        $this->exec_id = $res["exec_id"];
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(!strtotime($this->start_date)){
            $this->start_date=null;

        }else{
            $this->start_date=date('d-m-Y',strtotime($this->start_date));
        }
        if(strtotime($this->end_date)==strtotime('01-01-1970')){
            $this->end_date=null;

        }else{
            $this->end_date=date('d-m-Y',strtotime($this->end_date));
        }



         parent::afterFind ();
    }


    public function getContract(){
        $sql="select u.username,pu.name from service s,property_units pu ,user u
 where s.member_id=u.id and  s.property_id=pu.id and s.id=:id

 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$this->member_id);
        $res=$cmd->queryOne();
        if($res){
            return $res["username"]."[".$res["name"]."]";
        }
        return false;
    }

    public function getTenantAllocation(){
        $sql="select u.username,pu.name from tenant_property_allocation tpa
,property_units pu ,user u
 where tpa.tenant_id=u.id and  tpa.prop_id=pu.id and tpa.id=:id

 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$this->tenant_id);
        $res=$cmd->queryOne();
        if($res){
            return $res["username"]."[".$res["name"]."]";
        }
        return false;
    }

    public function getIntiatedUser(){
        $sql="select u.username from user u
 where u.id=:id

 ";
        $cmd=Yii::$app->db->createCommand($sql);
        $cmd->bindValue(":id",$this->user_id);
        $res=$cmd->queryOne();
        if($res){
            return $res["username"];
        }
        return false;
    }
    public function getExecutiveName()
    {
        $user= $this->hasOne(User::className(), ['id' => 'exec_id'])->one();
        if($user){
            return $user->username;
        }
        return false;
    }

}
