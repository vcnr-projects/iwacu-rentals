<?php

namespace app\modules\service\models;

use app\modules\realty\models\PropertyUnits;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "tenant_property_allocation".
 *
 * @property string $id
 * @property string $prop_id
 * @property integer $tenant_id
 * @property string $start_date
 * @property string $expiry_date
 * @property string $vacate_date
 * @property integer $rent_date
 * @property string $advance
 * @property string $rent_amt
 * @property string $rent_lease
 * @property string $increment_perc
 * @property string $next_collection_date
 * @property string $beneficiary
 * @property string $agreement
 * @property integer $is_active
 *
 * @property User $tenant
 * @property PropertyUnits $prop
 */
class TenantPropertyAllocation extends \yii\db\ActiveRecord
{
    public $availability_date;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_property_allocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prop_id', 'tenant_id', 'expiry_date','rent_lease'], 'required'],
            [['prop_id', 'tenant_id', 'rent_date', 'is_active', 'service_id','duration'], 'integer'],
            [['start_date', 'expiry_date', 'vacate_date', 'next_collection_date','availability_date'], 'safe'],
            [['advance', 'rent_amt', 'increment_perc','maintenance'], 'number'],
            [['rent_lease'], 'string', 'max' => 10],
            [['beneficiary'], 'string', 'max' => 100],
            //[['agreement'], 'string', 'max' => 255]
            [['agreement'], 'file'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prop_id' => 'Property ID',
            'tenant_id' => 'Tenant ID',
            'start_date' => 'Agreement Start Date',
            'expiry_date' => 'Agreement Expiry Date',
            'vacate_date' => 'Vacate By Date',
            'rent_date' => 'Rent Pay Date',
            'advance' => 'Advance Amount',
            'rent_amt' => 'Rent Amount',
            'rent_lease' => 'Rent or Lease',
            'increment_perc' => 'Increment Percentage',
            'next_collection_date' => 'Next Collection Date',
            'beneficiary' => 'Beneficiary',
            'agreement' => 'Agreement Document',
            'is_active' => 'Is Active?',
            'availability_date' => 'Property Availability Date',
            'service_id' => 'Owner Contract ',
            'maintenance' => 'Maintenance Fee',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(User::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProp()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_id']);
    }

    public function getRequestServiceActivities()
    {
        return $this->hasMany(RequestServiceActivity::className(), ['tenant_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    public function getPropName()
    {
        $model=\app\modules\realty\models\PropertyUnits::findOne($this->prop_id);
        $prop= $model->getProperty()->one();
        return "$prop->name [ $model->block - $model->floor_number - $model->flat_no ]";

    }

    public function getTenantID()
    {
        $tenant=$this->getTenant()->one();
        return $tenant->username ." / ".$tenant->email;

    }


    public function beforeSave($insert)
    {
        Yii::trace(VarDumper::dumpAsString('befor save'),'vardump');

        if (parent::beforeSave($insert)) {


            if(strtotime($this->start_date)!==false){
                $this->start_date=date('Y-m-d',strtotime($this->start_date));
            }else{
                $this->start_date=null;
            }

            if(strtotime($this->expiry_date)!==false){
                $this->expiry_date=date('Y-m-d',strtotime($this->expiry_date));
            }else{
                $this->expiry_date=null;
            }

            if(strtotime($this->vacate_date)!==false){
                $this->vacate_date=date('Y-m-d',strtotime($this->vacate_date));
            }else{
                $this->vacate_date=null;
            }

            $service=Service::findByCondition(["property_id"=>$this->prop_id,"is_active"=>1])->one();
            Yii::trace(VarDumper::dumpAsString($service),'vardump');
            if($service){
                $this->service_id=$service->id;
            }else{
                $this->addError('error',"Contract for this property is not signed");
                return false;
            }


            $id=($this->id)?$this->id:0;
            $exits=TenantPropertyAllocation::find()->where(" prop_id =:propid and is_active=1 and id !=:id",[":propid"=>$this->prop_id,":id"=>$id] )->one();
            if($exits){
                $this->addError("prop_id","This Property is already allocated");
                return false;
            }

            if(empty($this->maintenance)){
                $this->maintenance=0;
            }

            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {

        if(strtotime($this->start_date)==false){
            $this->start_date=null;

        }else{
            $this->start_date=date('d-m-Y',strtotime($this->start_date));
        }
        if(strtotime($this->expiry_date)==false){
            $this->expiry_date=null;

        }else{
            $this->expiry_date=date('d-m-Y',strtotime($this->expiry_date));
        }
        if(strtotime($this->vacate_date)==false){
            $this->vacate_date=null;

        }else{
            $this->vacate_date=date('d-m-Y',strtotime($this->vacate_date));
        }

        $prop=$this->getProp()->one();
        $this->availability_date=$prop->availability_date;






        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }


    public function afterSave($insert, $changedAttributes)
    {
        $prop=$this->getProp()->one();
        if(strtotime($this->availability_date)==false){
            $this->availability_date=null;

        }else{
            $this->availability_date=date('Y-m-d',strtotime($this->availability_date));
        }
        $prop->availability_date=$this->availability_date;
        $prop->save();



        parent::afterSave($insert, $changedAttributes);
    }

    public function saveEAP($eaps){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            $sql="select type,id
            from executive_activity_property where  tenant_service=:ts ";
            $cmd=Yii::$app->db->createCommand($sql);
            $cmd->bindValue(":ts",$this->id);
            $res=$cmd->queryAll();
            $prevactivity=[];
            foreach($res as $r){
                //$prevactivity[$r["type"]]=$r["id"];
                $prevactivity[$r["type"]]=$r;
            }

           /* if(empty($eaps)){
                $errmsg.="Atleast one Activity should be given";
            }else
            */
            {
                //ExecutiveActivityProperty::deleteAll(["tenant_service"=>$this->id]);
                foreach($eaps as $eap ){

                    if(isset($prevactivity[$eap->type])){
                        Yii::trace(VarDumper::dumpAsString($eap),'vardump');
                        //$eap->id=$prevactivity[$eap->type];
                        $eap->is_active=1;
                        $eap->isNewRecord=false;
                        $eap->oldAttributes=$prevactivity[$eap->type];
                       // unset($eap->oldAttributes);
                        Yii::trace(VarDumper::dumpAsString(($eap->oldAttributes)),'vardump');


                        unset($prevactivity[$eap->type]);
                    }
                   /* if($eap->collection==0&&$eap->payment==0){
                        $eap->is_payable=-1;
                    }*/
                    if(!$eap->save()){
                        foreach($eap->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }
                }

                foreach($prevactivity as $pa => $id){
                    $sql="update executive_activity_property set is_active=0 where id= :id ";
                    $cmd=Yii::$app->db->createCommand($sql);
                    $cmd->bindValue(":id",$id["id"]);
                    $cmd->execute();
                }
            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;

    }

}
