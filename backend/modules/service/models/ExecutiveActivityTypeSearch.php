<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\ExecutiveActivityType;

/**
 * ExecutiveActivityTypeSearch represents the model behind the search form about `app\modules\service\models\ExecutiveActivityType`.
 */
class ExecutiveActivityTypeSearch extends ExecutiveActivityType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'remarks'], 'safe'],
            [['is_active', 'recursion', 'collection', 'payment', 'collection_day', 'remainder_days'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExecutiveActivityType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_active' => $this->is_active,
            'recursion' => $this->recursion,
            'collection' => $this->collection,
            'payment' => $this->payment,
            'collection_day' => $this->collection_day,
            'remainder_days' => $this->remainder_days,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
