<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "property".
 *
 * @property string $id
 * @property string $property_id
 * @property string $name
 *
 * @property Service[] $services
 */
class Property extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'name'], 'required'],
            [['property_id', 'name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['property_id' => 'id']);
    }
}
