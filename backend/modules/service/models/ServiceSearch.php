<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\Service;

/**
 * ServiceSearch represents the model behind the search form about `app\modules\service\models\Service`.
 */
class ServiceSearch extends Service
{
    public $propName;
    public $memberID;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'property_id', 'member_id'], 'integer'],
            [['contract_id', 'date', 'expiry_date', 'tos', 'prop_type', 'ptype', 'created_on','propName','memberID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Service::find();
        $query->joinWith('property');
        $query->joinWith('member');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'expiry_date' => $this->expiry_date,
            'property_id' => $this->property_id,
            'member_id' => $this->member_id,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'contract_id', $this->contract_id])
            ->andFilterWhere(['like', 'tos', $this->tos])
            ->andFilterWhere(['like', 'prop_type', $this->prop_type])
            ->andFilterWhere(['like', 'ptype', $this->ptype])
            ->andFilterWhere(['like', 'property_units.name', $this->propName])
            ->andFilterWhere(['like', 'user.username', $this->memberID]);

        $dataProvider->setSort([
            'attributes' => [
                'contract_id',
                'tos',
                'date',
                'expiry_date',
                'propName' => [
                    'asc' => [
                        'property_units.name' => SORT_ASC
                    ],
                    'desc' => [
                        'property_units.name' => SORT_DESC,
                    ],
                    'label' => 'Property Name',
                    'default' => SORT_ASC
                ],
                'memberID' => [
                    'asc' => ['user.username' => SORT_ASC, 'user.username' => SORT_ASC],
                    'desc' => ['user.username' => SORT_DESC, 'user.username' => SORT_DESC],
                    'label' => 'Member ID',
                    'default' => SORT_ASC
                ],

            ]
        ]);

        return $dataProvider;
    }
}
