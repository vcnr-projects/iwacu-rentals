<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "executive_activity_property".
 *
 * @property integer $id
 * @property string $tenant_service
 * @property integer $recursion
 * @property integer $collection
 * @property integer $payment
 * @property integer $collection_day
 * @property integer $remainder_days
 * @property integer $next_collection_date
 * @property string $owner_service
 * @property string $type
 *
 * @property ExecutiveActivityType $type0
 * @property TenantPropertyAllocation $tenantService
 * @property Service $ownerService
 */
class ExecutiveActivityProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_service', 'recursion', 'collection', 'payment', 'collection_day', 'remainder_days', 'owner_service'], 'integer'],
            [['recursion', 'collection', 'payment', 'collection_day', 'remainder_days', 'next_collection_date', 'type'], 'required'],
            [['type'], 'string', 'max' => 200],
            [['is_active'], 'integer'],
            [[ 'next_collection_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenant_service' => 'Tenant Service',
            'recursion' => 'Recursion',
            'collection' => 'Collection',
            'payment' => 'Payment',
            'collection_day' => 'Collection Day',
            'remainder_days' => 'Remainder Days',
            'next_collection_date' => 'Next Collection Date',
            'owner_service' => 'Owner Service',
            'type' => 'Activity',
            'is_active' => 'Is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(ExecutiveActivityType::className(), ['type' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantService()
    {
        return $this->hasOne(TenantPropertyAllocation::className(), ['id' => 'tenant_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerService()
    {
        return $this->hasOne(Service::className(), ['id' => 'owner_service']);
    }
}
