<?php

namespace app\modules\service\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\service\models\GratuityServices;

/**
 * GratuityServicesSearch represents the model behind the search form about `app\modules\service\models\GratuityServices`.
 */
class GratuityServicesSearch extends GratuityServices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['tos', 'request_service'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GratuityServices::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'tos', $this->tos])
            ->andFilterWhere(['like', 'request_service', $this->request_service]);

        return $dataProvider;
    }
}
