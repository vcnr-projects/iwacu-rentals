<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "executive_activity_type".
 *
 * @property string $type
 * @property integer $is_active
 * @property integer $recursion
 * @property integer $collection
 * @property integer $payment
 * @property integer $collection_day
 * @property integer $remainder_days
 * @property string $remarks
 *
 * @property ExecutiveActivity[] $executiveActivities
 * @property ExecutiveActivityProperty[] $executiveActivityProperties
 */
class ExecutiveActivityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['is_active', 'recursion', 'collection', 'payment', 'collection_day', 'remainder_days','relates_to','category'], 'integer'],
            [[ 'collection_day'], 'integer','min'=>1,'max'=>31],
            [['remarks'], 'string'],
            [['type'], 'string', 'max' => 200],
            [['remainder_days','collection'],'default','value'=>'1'],
            [['recursion'],'default','value'=>null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Executive Activity Type',
            'is_active' => 'is Active?',
            'recursion' => 'Recursion in months',
            'collection' => 'Is Collectable?',
            'payment' => 'Is Payable?',
            'collection_day' => 'Activity Day from (any date in the month)',
            'remainder_days' => 'Remainder Days',
            'remarks' => 'Remarks',
            'relates_to' => 'Related To',
            'category' => 'Activity Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveActivities()
    {
        return $this->hasMany(ExecutiveActivity::className(), ['activity' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveActivityProperties()
    {
        return $this->hasMany(ExecutiveActivityProperty::className(), ['type' => 'type']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(!$this->collection_day){
                $this->collection_day=1;
            }


            return true;
        } else {
            return false;
        }
    }
}
