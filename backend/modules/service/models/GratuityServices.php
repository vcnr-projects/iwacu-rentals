<?php

namespace app\modules\service\models;

use Yii;

/**
 * This is the model class for table "gratuity_services".
 *
 * @property integer $id
 * @property string $tos
 * @property string $request_service
 *
 * @property RequestServices $requestService
 * @property ServiceType $tos0
 */
class GratuityServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gratuity_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tos', 'request_service'], 'required'],
            [['tos', 'request_service'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tos' => 'Type of Service',
            'request_service' => 'Request Service',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestService()
    {
        return $this->hasOne(RequestServices::className(), ['service_name' => 'request_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTos0()
    {
        return $this->hasOne(ServiceType::className(), ['name' => 'tos']);
    }
}
