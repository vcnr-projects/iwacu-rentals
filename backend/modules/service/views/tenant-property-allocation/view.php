<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\TenantPropertyAllocation */

$this->title = $model->propName;
$this->params['breadcrumbs'][] = ['label' => 'Tenant Property Allocations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-property-allocation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        $propu=\app\modules\realty\models\PropertyUnits::findOne($model->prop_id);
        //$prop=\app\modules\realty\models\PropertyUnits::findOne($prop->property_id);
        ?>
        <?= Html::a('Property Details', ['/realty/property/view', 'id' =>$propu->property_id,'viewtab'=>'punits' ], ['class' => 'btn btn-primary']) ?>

    </p>

    <ul class="nav nav-tabs ">
        <?php
        $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
        $actclstxt='class="active"';
        ?>
        <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen">Tenant </a></li>
        <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Activity</a></li>
    </ul>

    <div class="tab-content">

        <!-- begin: tenant detail-->
        <div id="gen" class="tab-pane fade in <?= ($viewtab=="gen")?"active":"" ?> ">



            <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
              'attribute'=>'propName',
                'label'=>"Porperty Name"
               // 'value'=>$model->getProp()->one()->
            ],
            //'prop_id',
            [
                'attribute'=>'tenantID',
                'label'=>"Tenant ID"
                // 'value'=>$model->getProp()->one()->
            ],
            'start_date',
            'expiry_date',
            'vacate_date',
            'rent_date',
            'rent_lease',
            'advance',

            'rent_amt',

            'increment_perc',
            'next_collection_date',
            //'beneficiary',
            [
                'label'=>" Agreement",
                'format'=>'html',
                'value'=>($model->agreement)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->agreement,["target"=>"_blank"]):"",
            ],
            [
                'attribute'=>'service_id',
                'value'=>($model->getService()->one()->contract_id),
            ],

            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Active":"Void",
            ],

        ],
    ]) ?>

        </div>
        <!--end:tenant detail-->
        <!--begin:activity detail-->
        <div id="off" class="tab-pane fade in  <?= ($viewtab=="off")?"active":"" ?>">
            <a href="<?= Yii::$app->urlManager->createUrl(["/service/tenant-property-allocation/activity","id"=>$model->id]) ?>" >Edit</a>
            <style>
                .half-opacity{
                    opacity: 0.5;
                }
            </style>
            <table class="table">
                <thead>
                <tr>
                    <td>SL No.</td>
                    <td>Type</td>
                    <td>Collection Day</td>
                    <td>Remainder Days</td>
                    <td>Next Collection Date</td>
                    <td>Is Active?</td>
                </tr>
                </thead>
                <tbody>

                <?php


                $sql="
    select * from  executive_activity_property where tenant_service={$model->id}
                         ";
                //$eats=\app\modules\service\models\ExecutiveActivityType::find()->all();
                $cmd=Yii::$app->db->createCommand($sql);
                $eats=$cmd->queryAll();
                foreach($eats as $i=> $eat){
                    $eat=(object)$eat;
                    ?>
                    <tr class="<?=($eat->is_active==0)?"half-opacity":"" ?>">
                        <td><?= $i+1 ?></td>
                        <td>
                            <label>

                                <?= $eat->type ?>
                            </label></td>
                        <td><?= $eat->collection_day ?></td>
                        <td><?= $eat->remainder_days ?></td>
                        <td><?= date('d-m-Y',strtotime($eat->next_collection_date)) ?></td>
                        <td><?= ($eat->is_active)?"Yes":"No"?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
            <!--end:activity detail-->

</div>
