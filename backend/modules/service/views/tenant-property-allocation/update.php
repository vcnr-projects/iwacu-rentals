<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\TenantPropertyAllocation */

$prop=$model->getProp()->one();

$this->title = 'Update Tenant Property Allocation: ' . ' ' . $prop->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenant Property Allocations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $prop->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tenant-property-allocation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs ">
        <li class="active" ><a  data-toggle="tab"  href="#">Tenant Property Allocation</a></li>
        <li  ><a   href="<?= Yii::$app->urlManager->createUrl(["/service/tenant-property-allocation/activity","id"=>$model->id])  ?>">Property Activities</a></li>
    </ul>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
