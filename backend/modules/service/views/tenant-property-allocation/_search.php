<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\TenantPropertyAllocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-property-allocation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'prop_id') ?>

    <?= $form->field($model, 'tenant_id') ?>

    <?= $form->field($model, 'start_date') ?>

    <?= $form->field($model, 'expiry_date') ?>

    <?php // echo $form->field($model, 'vacate_date') ?>

    <?php // echo $form->field($model, 'rent_date') ?>

    <?php // echo $form->field($model, 'advance') ?>

    <?php // echo $form->field($model, 'rent_amt') ?>

    <?php // echo $form->field($model, 'rent_lease') ?>

    <?php // echo $form->field($model, 'increment_perc') ?>

    <?php // echo $form->field($model, 'next_collection_date') ?>

    <?php // echo $form->field($model, 'beneficiary') ?>

    <?php // echo $form->field($model, 'agreement') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
