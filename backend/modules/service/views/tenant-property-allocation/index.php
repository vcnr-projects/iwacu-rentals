<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\service\models\TenantPropertyAllocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant Property Allocations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-property-allocation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tenant Property Allocation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'prop_id',
            'tenant_id',
            'start_date',
            'expiry_date',
            // 'vacate_date',
            // 'rent_date',
            // 'advance',
            // 'rent_amt',
            // 'rent_lease',
            // 'increment_perc',
            // 'next_collection_date',
            // 'beneficiary',
            // 'agreement',
            // 'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
