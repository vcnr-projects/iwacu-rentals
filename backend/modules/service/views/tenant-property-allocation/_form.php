<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\TenantPropertyAllocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-property-allocation-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model); ?>

    <?php
        $memberList=\yii\helpers\ArrayHelper::map(\common\models\User::find()->andFilterWhere(["user_type"=>'Tenant'])->asArray()->all()
        ,'id'
        ,function($model, $defaultValue) {
        //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
        return $model["username"]." / ".$model["email"];
        }
        );
    ?>

    <?php
    if(isset($_GET['prop_id'])){
        $PropertyArray=\app\modules\realty\models\PropertyUnits::find()->where(["id"=>$_GET['prop_id']])->asArray()->all();
    }else
        $PropertyArray=\app\modules\realty\models\PropertyUnits::find()->asArray()->all();

    ?>

    <?= $form->field($model, 'prop_id')->dropDownList(\yii\helpers\ArrayHelper::map($PropertyArray,'id',
            function($model, $defaultValue){
                $model=\app\modules\realty\models\PropertyUnits::findOne($model["id"]);
                $prop= $model->getProperty()->one();
                return "$model->name - $prop->name [ $model->block - $model->floor_number - $model->flat_no ]";
            })
        ,['prompt'=>"Select Property"]) ?>

    <?= $form->field($model, 'tenant_id')->dropDownList($memberList,['prompt'=>"Select Tenant"]) ?>

    <?= $form->field($model, 'rent_lease',['template'=>'<label class="childdispInline " > <span class="redstar">Rent Or Lease</span>  :{input}{error}</label>'])
        ->radioList(["Rent"=>"Rent","Lease"=>"Lease"],["onchange"=>"rentorleasesel()"]) ?>

    <?= $form->field($model, 'start_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Commencement Date',"onchange"=>"calExpiryDate()"],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?= $form->field($model, 'duration')->textInput(["onchange"=>"calExpiryDate()","onblur"=>"calExpiryDate()"]) ?>

    <script>
        function rentorleasesel(){
           var val= $("[name^='TenantPropertyAllocation\[rent_lease\]']:checked").val();
            if(val=="Rent"){
                $("#tenantpropertyallocation-duration").val(11);
            }else{
                $("#tenantpropertyallocation-duration").val(36);
            }
            calExpiryDate();
        }

        function calExpiryDate(){
            var date=$("#tenantpropertyallocation-start_date").val();
            var duration=$("#tenantpropertyallocation-duration").val();
            try{
                var dt=date.split("-");
                var tdt=new Date(dt[2],dt[1],dt[0]);
                var addmonth=tdt.getMonth()*1 + duration*1;
                tdt.setMonth(addmonth);
                var dd = ("0" + tdt.getDate()).slice(-2);

                var mm =("0" + (tdt.getMonth() + 1)).slice(-2);

                var y = tdt.getFullYear();
                if(isNaN(y)){
                    throw ("NaN");
                }
                $("#tenantpropertyallocation-expiry_date").val(dd+"-"+mm+"-"+y);
            }catch(e){

            }
        }
    </script>

    <?= $form->field($model, 'expiry_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Expiry Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>



    <?/*= $form->field($model, 'rent_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Rent Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd',
            'todayHighlight'=>true,
        ]
    ]); */?>

    <?= $form->field($model, 'vacate_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Vacate Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?= $form->field($model, 'availability_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Availability Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>



    <?= $form->field($model, 'advance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rent_amt')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'maintenance')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'increment_perc')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'next_collection_date')->textInput() ?>

    <? //= $form->field($model, 'beneficiary')->textInput(['maxlength' => true]) ?>

    <?php
    $baseurl=Yii::$app->urlManager->baseUrl;
    if($model->agreement)
        echo "
                <a href='{$baseurl}/uploads/{$model->agreement}'>{$model->agreement}</a>
           ";
    ?>
    <?=
    $form->field($model, 'agreement')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*','showUpload'=>false,],
        'pluginOptions'=>['showUpload'=>false]
        ,
    ]); ?>

    <? //= $form->field($model, 'agreement')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Void',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
