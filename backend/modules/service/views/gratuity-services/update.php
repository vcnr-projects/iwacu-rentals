<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\GratuityServices */

$this->title = 'Update Gratuity Services: ' . ' ' . $model->tos;
$this->params['breadcrumbs'][] = ['label' => 'Gratuity Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gratuity-services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
