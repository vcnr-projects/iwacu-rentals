<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\service\models\GratuityServices */

$this->title = 'Create Gratuity Services';
$this->params['breadcrumbs'][] = ['label' => 'Gratuity Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gratuity-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
