<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\GratuityServices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gratuity-services-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tos')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\modules\service\models\ServiceType::find()->asArray()->all(),'name','name'
    ),["prompt"=>"Select"]) ?>

    <?= $form->field($model, 'request_service')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\modules\service\models\RequestServices::find()->asArray()->all(),'service_name','service_name'
    ),["prompt"=>"Select"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
