<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\service\models\GratuityServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gratuity Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gratuity-services-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gratuity Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'tos',
            'request_service',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
