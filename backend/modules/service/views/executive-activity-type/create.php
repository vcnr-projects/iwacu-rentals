<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ExecutiveActivityType */

$this->title = 'Create Executive Activity Type';
$this->params['breadcrumbs'][] = ['label' => 'Executive Activity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-activity-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
