<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ExecutiveActivityType */

$this->title = 'Update Executive Activity Type: ' . ' ' . $model->type;
$this->params['breadcrumbs'][] = ['label' => 'Executive Activity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->type, 'url' => ['view', 'id' => $model->type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="executive-activity-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
