<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ExecutiveActivityType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="executive-activity-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'recursion')->textInput() ?>

    <?= $form->field($model, 'collection')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <?= $form->field($model, 'payment')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <?= $form->field($model, 'collection_day')->textInput() ?>

    <?= $form->field($model, 'remainder_days')->textInput() ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'relates_to')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Owner',
            'offText'=>'Tenant',
        ]
    ]) ?>
    <? //= $form->field($model, 'category')->dropDownList(["0"=>"Collect Now","1"=>"Bill & Pay Slip","2"=>"Meetings","3"=>"Periodic Visit"],["class"=>"form-control"])?>
    <?= $form->field($model, 'category')->dropDownList(["0"=>"Collect & Upload Bill/Pay slips","2"=>"Meetings","3"=>"Periodic Visit"],["class"=>"form-control"])?>
    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
