<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ExecutiveActivityTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="executive-activity-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'is_active') ?>

    <?= $form->field($model, 'recursion') ?>

    <?= $form->field($model, 'collection') ?>

    <?= $form->field($model, 'payment') ?>

    <?php // echo $form->field($model, 'collection_day') ?>

    <?php // echo $form->field($model, 'remainder_days') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
