<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ExecutiveActivityType */

$this->title = $model->type;
$this->params['breadcrumbs'][] = ['label' => 'Executive Activity Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-activity-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->type], ['class' => 'btn btn-primary']) ?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'type',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],
            'recursion',
            [
                'attribute'=>'collection',
                'value'=>($model->collection)?"Yes":"NO",
            ],
            [
                'attribute'=>'payment',
                'value'=>($model->payment)?"Yes":"NO",
            ],
            [
                'attribute'=>'Related To',
                'value'=>($model->collection)?"Owner":"Tenant",
            ],
            'collection_day',
            'remainder_days',
            'remarks:ntext',
        ],
    ]) ?>

</div>
