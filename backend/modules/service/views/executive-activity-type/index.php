<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\service\models\ExecutiveActivityTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Executive Activity Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-activity-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Executive Activity Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'type',

            'recursion',
            [
                'attribute'=>'collection',
                'filter'=>Html::activeDropDownList($searchModel,'collection',
                    [
                        "1"=>"Yes",
                        "0"=>"No",
                    ],
                    ['class'=>'form-control','prompt' => 'Select ']
                ),
                'value'=>function($data){
                    return ($data->collection)?"Yes":"NO";
                }
            ],
            [
                'attribute'=>'is_active',
                'filter'=>Html::activeDropDownList($searchModel,'is_active',
                    [
                        "1"=>"Yes",
                        "0"=>"No",
                    ],
                    ['class'=>'form-control','prompt' => 'Select Active Status']
                ),
                'value'=>function($data){
                    return ($data->is_active)?"Yes":"NO";
                }
            ],

            //'collection',
            //'payment',
            // 'collection_day',
            // 'remainder_days',
            // 'remarks:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}'
            ],
        ],
    ]); ?>

</div>
