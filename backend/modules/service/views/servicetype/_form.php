<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\ServiceType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'gratuity')->checkboxList(\yii\helpers\ArrayHelper::map(\app\modules\service\models\ExecutiveActivityType::find()->asArray()->all(),'type','type')
    ) ?>

    <?//= $form->field($model, 'is_active')->textInput() ?>
    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
