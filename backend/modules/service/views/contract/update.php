<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\Service */

$this->title = 'Update Contract: ' . ' ' . $model->propName;
$this->params['breadcrumbs'][] = ['label' => 'Contract', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->propName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs ">
        <li class="active" ><a  data-toggle="tab"  href="#">Contract</a></li>
        <li  ><a   href="<?= Yii::$app->urlManager->createUrl(["/service/contract/activity","id"=>$model->id])  ?>">Property Activities</a></li>
    </ul>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
