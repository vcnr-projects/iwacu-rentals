<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\Service */

$this->title = $model->contract_id;
$this->params['breadcrumbs'][] = ['label' => 'Contract', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       <!-- --><?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <ul class="nav nav-tabs ">
        <?php
        $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
        $actclstxt='class="active"';
        ?>
        <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen">Contract for service </a></li>
        <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Activity</a></li>
    </ul>

    <div class="tab-content">

        <!-- begin: tenant detail-->
        <div id="gen" class="tab-pane fade in <?= ($viewtab=="gen")?"active":"" ?> ">


        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'contract_id',
            'date',
            'expiry_date',
            [
                'label'=>'Property',
                'format'=>"HTML",
                'value'=>
                     Html::a($model->property->name,["/realty/propertyunits/view","id"=>$model->property->id]),

            ],

            'tos',
            'prop_type',
            'ptype',
            'member_id',
            'created_on',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Active":"Void",
            ],
        ],
    ]) ?>

        </div>
        <!--end:tenant detail-->
        <!--begin:activity detail-->
        <div id="off" class="tab-pane fade in  <?= ($viewtab=="off")?"active":"" ?>">
            <a href="<?= Yii::$app->urlManager->createUrl(["/service/contract/activity","id"=>$model->id]) ?>" >Edit</a>
            <style>
                .half-opacity{
                    opacity: 0.5;
                }
            </style>
            <table class="table">
                <thead>
                <tr>
                    <td>SL No.</td>
                    <td>Type</td>
                    <td>Activity Day</td>
                    <td>Remainder Days</td>
                    <td>Next Activity Date</td>
                    <td>Is Active?</td>
                </tr>
                </thead>
                <tbody>

                <?php


                $sql="
    select * from  executive_activity_property where owner_service={$model->id}
                         ";
                //$eats=\app\modules\service\models\ExecutiveActivityType::find()->all();
                $cmd=Yii::$app->db->createCommand($sql);
                $eats=$cmd->queryAll();
                foreach($eats as $i=> $eat){
                    $eat=(object)$eat;
                    ?>
                    <tr class="<?=($eat->is_active==0)?"half-opacity":"" ?>">
                        <td><?= $i+1 ?></td>
                        <td>
                            <label>

                                <?= $eat->type ?>
                            </label></td>
                        <td><?= $eat->collection_day ?></td>
                        <td><?= $eat->remainder_days ?></td>
                        <td><?= date('d-m-Y',strtotime($eat->next_collection_date)) ?></td>
                        <td><?= ($eat->is_active)?"Yes":"No"?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <!--end:activity detail-->

        </div>

</div>
