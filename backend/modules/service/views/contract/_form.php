<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'contract_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tos')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\service\models\ServiceType::find()->asArray()->all(),'name','name'),["prompt"=>"Select"]) ?>


    <?php
        $memberList=\yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all()
            ,'id'
            ,function($model, $defaultValue) {
                //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
                return $model["username"]." / ".$model["email"];
            }
        );


    if(!empty($model->member_type)){
        $memberList=\yii\helpers\ArrayHelper::map(\common\models\User::find()->andFilterWhere(["user_type"=>$model->member_type])->asArray()->all()
            ,'id'
            ,function($model, $defaultValue) {
                //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
                return $model["username"]." / ".$model["email"];
            }
        );
    }

    if(isset($_GET['member_id'])){
        $memberList=\yii\helpers\ArrayHelper::map(\common\models\User::find()->andFilterWhere(["id"=>$_GET['member_id']])->asArray()->all()
            ,'id'
            ,function($model, $defaultValue) {
                //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
                return $model["username"]." / ".$model["email"];
            }
        );
    }
    // $memberList=array();

    /*if(!empty($model->property_id)){
        $propu=\app\modules\realty\models\PropertyUnits::findOne($model->property_id);
        $prop=\app\modules\realty\models\Property::findOne($propu->property_id);
        $user=\common\models\User::findOne($prop->member_id);
        $memberList[$user->id]=$user->username." / ".$user->email;
    }*/

    if(isset($_GET['property_id'])){
        $PropertyArray=\app\modules\realty\models\PropertyUnits::find()->where(["id"=>$_GET['property_id']])->asArray()->all();
    }else
       $PropertyArray=\app\modules\realty\models\PropertyUnits::find()->asArray()->all();
    ?>

    <?= $form->field($model, 'property_id')->dropDownList(\yii\helpers\ArrayHelper::map($PropertyArray,'id',
            function($model, $defaultValue){
                $model=\app\modules\realty\models\PropertyUnits::findOne($model["id"]);
                $prop= $model->getProperty()->one();
                return " $model->name - $prop->name [ $model->block - $model->floor_number - $model->flat_no ]";
            })
        ,['prompt'=>"Select Property","onchange"=>"changeproperty(this.value)"]) ?>
    <script>
        function changeproperty(val){
            $("#service-member_id option").remove();
            $.get("<?= Yii::$app->urlManager->createUrl("/realty/propertyunits/getmember")?>"
                ,{"propid":val},function(data){
                    data=JSON.parse(data);
                    var opt=$("<option></option>");
                    $(opt).val(data.id);
                    $(opt).html(data.username+" / "+data.email);
                    $("#service-member_id").append(opt);

            });
        }
    </script>

    <?=  ($model->tenant_id=="")? $form->field($model, 'member_id')->dropDownList($memberList) :"" ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Date',"onchange"=>"calExpiryDate()"],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?= $form->field($model, 'duration')->textInput(["onchange"=>"calExpiryDate()","onblur"=>"calExpiryDate()"]) ?>

    <script>
        function calExpiryDate(){
            var date=$("#service-date").val();
            var duration=$("#service-duration").val();
            try{
                var dt=date.split("-");
                var tdt=new Date(dt[2],dt[1],dt[0]);
                var addmonth=tdt.getMonth()*1 + duration*1;
                tdt.setMonth(addmonth);
                var dd = ("0" + tdt.getDate()).slice(-2);

                var mm =("0" + (tdt.getMonth() + 1)).slice(-2);
                var y = tdt.getFullYear();
                $("#service-expiry_date").val(dd+"-"+mm+"-"+y);
            }catch(e){

            }
        }
    </script>
    <?= $form->field($model, 'expiry_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Expiry Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>








    <?= $form->field($model, 'prop_type')->dropDownList(["Residential"=>"Residential",]) ?>

    <?= $form->field($model, 'ptype')->dropDownList(["Apartment"=>"Apartment",]) ?>




    <? //= ($model->member_id=="")? $form->field($model, 'tenant_id')->textInput() :"" ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <? //= $form->field($model, 'payment_type')->dropDownList(["Once/Lease"=>"Once/Lease","Monthly/Rent"=>"Monthly/Rent","Yearly"=>"Yearly"]) ?>


    <?php
    $baseurl=Yii::$app->urlManager->baseUrl;
    if($model->document)
        echo "
                <a href='{$baseurl}/uploads/{$model->document}'>{$model->document}</a>
           ";
    ?>
    <?=
    $form->field($model, 'document')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'application/pdf,image/*','showUpload'=>false,],
        'pluginOptions'=>['showUpload'=>false]
        ,
    ]); ?>


    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
