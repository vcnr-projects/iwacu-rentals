<?php
$prop=$model->getProperty()->one();

$this->title = 'Update Tenant Property Allocation: ' . ' ' . $prop->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenant Property Allocations ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $prop->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Activities';
?>
<?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>
    <table class="table">
        <thead>
            <tr>
                <td>SL No.</td>
                <td>Type</td>
                <td>Activity Day</td>
                <td>Remainder Days</td>
                <td>Next Ativity Date</td>
            </tr>
        </thead>
        <tbody>

<?php

 $sql="select if(count(*)=0,true,false ) from  executive_activity_property where owner_service={$model->id}";
    $cmd=Yii::$app->db->createCommand($sql);
 $isNew=$cmd->queryScalar();

    $sql="select t.type,ifnull(p.collection_day,t.collection_day) as collection_day
    ,ifnull(p.remainder_days,t.remainder_days) as remainder_days
    ,if(p.type is null,false,true) as selected

    ,if(p.next_collection_date is null,'',date_format(next_collection_date,'%d-%m-%Y')) as next_collection_date
    ,ifnull((select true from gratuity_services where tos=(select tos from service where id={$model->id} ) and request_service=t.type ),false) as isDefaultTos

    from executive_activity_type t left join
    (select * from  executive_activity_property where owner_service={$model->id} and is_active=1) p
                        on t.type=p.type
                         where t.relates_to=1
                         ";
    //$eats=\app\modules\service\models\ExecutiveActivityType::find()->all();
    $cmd=Yii::$app->db->createCommand($sql);
    $eats=$cmd->queryAll();
    foreach($eats as $i=> $eat){
    $eat=(object)$eat;
    ?>
        <tr >
            <td><?= $i+1 ?></td>
            <td>
                <label>
                    <input  type="checkbox" name="ExecutiveActivityProperty[type][]" value="<?= $eat->type ?>"
                    <?= ($eat->selected==1||($isNew&&$eat->isDefaultTos))?"checked":"" ?>
                    />
                    <?= $eat->type ?>
                </label></td>
            <td><input class="form-control" type="text" name="ExecutiveActivityProperty[<?= $eat->type ?>][collection_day]" value="<?= $eat->collection_day ?>"  /></td>
            <td><input class="form-control" type="text" name="ExecutiveActivityProperty[<?= $eat->type ?>][remainder_days]" value="<?= $eat->remainder_days ?>"  /></td>
            <td>
                <?php
                echo \kartik\widgets\DatePicker::widget([
                    'name' => "ExecutiveActivityProperty[$eat->type][next_collection_date]",
                    'type' => \kartik\date\DatePicker::TYPE_INPUT,
                    'value' =>  $eat->next_collection_date,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ]);
                ?>
            </td>
<!--            <td><input class="form-control" class="" type="text" name="ExecutiveActivityProperty[<?/*= $eat->type */?>][next_collection_date]" value="<?/*= $eat->next_collection_date */?>"  /></td>
-->        </tr>
    <?php
    }
?>
        </tbody>
    </table>

<button type="submit" class="btn btn-block btn-primary">Submit</button>
<?php \yii\bootstrap\ActiveForm::end(); ?>
