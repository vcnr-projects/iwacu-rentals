<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServices */

$this->title = 'Create Request Services';
$this->params['breadcrumbs'][] = ['label' => 'Request Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
