<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServices */

$this->title = 'Update Request Services: ' . ' ' . $model->service_name;
$this->params['breadcrumbs'][] = ['label' => 'Request Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->service_name, 'url' => ['view', 'id' => $model->service_name]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="request-services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
