<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\service\models\RequestServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Request Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-services-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Request Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'service_name',
            [
                'attribute'=>'is_active',
                'value'=>function($data){
                    return ($data->is_active)?"Yes":"NO";
                },
            ],
            'created_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
