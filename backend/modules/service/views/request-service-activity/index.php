<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\service\models\RequestServiceActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Request Service Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-service-activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Request Service Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'description:ntext',
            //'start_date',
            'end_date',
            [
                'attribute'=>'status',
                'filter'=>Html::dropDownList('RequestServiceActivitySearch[status]'
                    ,$searchModel->status
                    ,[
                    ""=>"Select"
                    ,"0"=>"Pending Admin Approval"
                    ,"1"=>"Pending Final Approval"
                    ,"2"=>"Work Started"
                    ,"3"=>"Work Finished"
                    ,"-1"=>" Cancelled"
                ],['class'=>'form-control']),
                'value'=>function ($model, $key, $index, $column){
                    return $model->getStatusText();
                }
            ],
            // 'property_id',
            // 'tenant_id',
            // 'member_id',
            // 'user_id',
            // 'exec_id',
            // 'priority',
            // 'proposed_cost',
            // 'accepted_cost',
            // 'admin_approval',
            // 'member_approval',
            // 'is_payable',
            // 'status',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}{delete}{approve}',
                  'buttons' => [
                        'approve' => function ($url, $model) {
                            $user=\common\models\User::findOne(Yii::$app->user->id);
                            if($user->user_type=="Admin")
                            return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                'title' => 'Approve',
                            ]);
                        }
                    ],
            ],
        ],
    ]); ?>

</div>
