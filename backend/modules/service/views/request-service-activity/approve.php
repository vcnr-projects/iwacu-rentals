<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServiceActivity */
/* @var $form yii\widgets\ActiveForm */
?>
<?php


/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServiceActivity */

$this->title = 'Approve Request Service Activity: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Request Service Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Approve';
?>
<div class="request-service-activity-update">

    <h1><?= Html::encode($this->title) ?></h1>
<div class="request-service-activity-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'accepted_cost')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'is_payable')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <? //= $form->field($model, 'status')->dropDownList(["0"=>"Pending for Approval","1"=>"Cancel"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Approve', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    </div>
