<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServiceActivity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Request Service Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-service-activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'name',
            'description:ntext',
            'start_date',
            'end_date',
            //'property_id',
            [
                'attribute'=>'TenantAllocation',
                'label'=>'Tenant Initiated',
                 'visible'=>($model->getTenantAllocation())?true:false,
            ],
            [
                'attribute'=>'Contract',
                'label'=>'Owner Initiated',
                'visible'=>(($model->getContract())?true:false),
            ],
            [
                'attribute'=>'IntiatedUser',
                'label'=>'User Initiated',
                'visible'=>(($model->getIntiatedUser())?true:false),
            ],


            //'Contract',
            //'user_id',
            [
                'attribute'=>'ExecutiveName',
                'label'=>'Executive',
            ],
            'priority',
            [
                'attribute'=>'is_payable',
                'value'=>($model->is_payable)?"Yes":"NO",
            ],

            'proposed_cost',
            'accepted_cost',

            [
                'attribute'=>'admin_approval',
                'value'=>(($model->admin_approval)?'Approved':'Yet to Approve'),
            ],
            //'admin_approval',
            [
                'attribute'=>'member_approval',
                'value'=>(($model->member_approval)?'Approved':'Yet to Approve'),
            ],
            //'member_approval',

            'StatusText',
        ],
    ]) ?>

    <?php
    $user=\common\models\User::findOne(Yii::$app->user->id);
    if($user&&$user->user_type=="Admin"&&!$model->admin_approval){
    ?>
    <div class="request-service-activity-form">

        <?php $form = \yii\widgets\ActiveForm::begin([
            'action' => \yii\helpers\Url::to(['/service/request-service-activity/approve','id'=>$model->id]),
        ]); ?>


        <?= $form->field($model, 'accepted_cost')->textInput(['maxlength' => true]) ?>



        <?= $form->field($model, 'is_payable')->widget(\kartik\widgets\SwitchInput::className(),[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]) ?>

        <? //= $form->field($model, 'status')->dropDownList(["0"=>"Pending for Approval","1"=>"Cancel"]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Approve', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>


    </div>

    <?php } ?>
