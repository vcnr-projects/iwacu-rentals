<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServiceActivity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-service-activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->dropDownList(\yii\helpers\ArrayHelper::map( \app\modules\service\models\RequestServices::find()->asArray()->all()
    ,'service_name','service_name'),['prompt'=>"Select"]
    ) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Probable Start Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <?= $form->field($model, 'end_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Finish Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); ?>

    <? //= $form->field($model, 'property_id')->textInput(['maxlength' => true]) ?>

    <?php if(empty($model->member_id)){?>
    <?= $form->field($model, 'tenant_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\modules\service\models\TenantPropertyAllocation::find()->filterWhere(["is_active"=>"1"])->asArray()->all(),'id'
            , function($model, $defaultValue){
                $sql="select u.username,pu.name from user u,property_units pu where u.id=:member_id and pu.id=:property_id ";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":member_id",$model["tenant_id"]);
                $cmd->bindValue(":property_id",$model["prop_id"]);
                $res=$cmd->queryOne();

                return   $res["username"]."[".$res["name"]."]"; //" $model->name - $prop->name [ $model->block - $model->floor_number - $model->flat_no ]";
            }
        )
        ,['prompt'=>"Select"]) ?>
    <?php }?>

    <?php if(empty($model->tenant_id)){?>
    <?= $form->field($model, 'member_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\service\models\Service::find()->asArray()->all(),'id',
            function($model, $defaultValue){
                $sql="select pu.name,u.username from property_units pu,user u where u.id=:member_id and pu.id=:property_id";
                $cmd=Yii::$app->db->createCommand($sql);
                $cmd->bindValue(":member_id",$model["member_id"]);
                $cmd->bindValue(":property_id",$model["property_id"]);
                $res=$cmd->queryOne();

                return   $res["username"]."[".$res["name"]."]"; //" $model->name - $prop->name [ $model->block - $model->floor_number - $model->flat_no ]";
            })
        ,['prompt'=>"Select Property","onchange"=>"changeproperty(this.value)"]) ?>
    <?php }?>

    <?php if(empty($model->member_id)&&empty($model->tenant_id)){?>
    <?= $form->field($model, 'user_id')->textInput() ?>
    <?php }?>

    <?= $form->field($model, 'exec_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(\common\models\User::find()->filterWhere(["user_type"=>"Executive"])->asArray()->all(),'id','username')
    ,['prompt'=>"Select"]) ?>

    <?= $form->field($model, 'priority')->dropDownList([
        "Normal"=>"Normal",
        "High Priority"=>"High Priority",

        "Low Priority"=>"Low Priority",
    ],['prompt'=>"select"]) ?>

    <?= $form->field($model, 'proposed_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accepted_cost')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'admin_approval')->checkbox([Yii::$app->user->id=>"Approve"]) ?>

    <? //= $form->field($model, 'member_approval')->textInput() ?>

    <?= $form->field($model, 'is_payable')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <? //= $form->field($model, 'status')->dropDownList(["0"=>"Pending for Approval","1"=>"Cancel"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
