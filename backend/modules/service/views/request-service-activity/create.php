<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\service\models\RequestServiceActivity */

$this->title = 'Create Request Service Activity';
$this->params['breadcrumbs'][] = ['label' => 'Request Service Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-service-activity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
