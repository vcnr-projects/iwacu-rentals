<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveProperties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="executive-properties-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'exec_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        \common\models\User::find()->andFilterWhere(["user_type"=>"Executive"])->asArray()->all(),'id','username'
    ),["prompt"=>"Select"]); ?>

    <?= $form->field($model, 'property')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\PropertyUnits::find()->asArray()->all(),'id',
            function($model, $defaultValue){
                $model=\app\modules\realty\models\PropertyUnits::findOne($model["id"]);
                $prop= $model->getProperty()->one();
                return $model->name." -> $prop->name [ $model->block - $model->floor_number - $model->flat_no ]";;
                //return "$prop->name [ $model->block - $model->floor_number - $model->flat_no ]";
            })
        ,['prompt'=>"Select Property"])  ?>

    <?/*= $form->field($model, 'date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); */?><!--

    --><?/*= $form->field($model, 'expiry_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter Expiry Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true,
        ]
    ]); */?>

    <?= $form->field($model, 'is_active')->widget(\kartik\widgets\SwitchInput::className(),[
        'pluginOptions'=>[
            'onText'=>'Yes',
            'offText'=>'No',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
