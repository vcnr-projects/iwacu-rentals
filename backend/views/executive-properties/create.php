<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveProperties */

$this->title = 'Create Executive Property Assignment';
$this->params['breadcrumbs'][] = ['label' => 'Executive Property Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-properties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
