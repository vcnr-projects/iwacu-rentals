<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveProperties */

$this->title = $model->propertyName;
$this->params['breadcrumbs'][] = ['label' => 'Executive Property Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-properties-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
         $propu=\app\modules\realty\models\PropertyUnits::findOne($model->property);
         //$prop=\app\modules\realty\models\PropertyUnits::findOne($prop->property_id);
        ?>
        <?= Html::a('Property Details', ['/realty/property/view', 'id' =>$propu->property_id,'viewtab'=>'punits' ], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute'=>'execId',
                'label'=>'Executive ID',
            ],
            [
                'attribute'=>'propertyName',
                'label'=>'Property ID',
            ],
            'date',
            //'expiry_date',
            [
                'attribute'=>'is_active',
                'value'=>($model->is_active)?"Yes":"NO",
            ],
        ],
    ]) ?>

</div>
