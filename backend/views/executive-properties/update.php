<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveProperties */

$this->title = 'Update Property Assignment: ' . ' ' . $model->propertyName;
$this->params['breadcrumbs'][] = ['label' => 'Executive Property Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->propertyName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="executive-properties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
