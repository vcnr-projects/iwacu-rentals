<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <style>
        #w0::after{
            background-image: url('<?= Yii::$app->urlManager->baseUrl ?>/address-top-bg.jpg');
            position: relative;
            bottom:10px;
            width: 100%;
        }
        #greenwhite{
            position: fixed;
            margin-top:30px;
            z-index:99999;
        }
        /*.navbar-inverse{
            background-image: url('address-top-bg.jpg') !important;
            background-repeat: no-repeat;
        }
*/
        .navbar-brand{
            margin-top: -30px;

        }
        .navbar-brand img{
            height: 80px;
        }
        ul.navbar-nav li a{
            font-size: 16px;
            font-weight: bold;
        }

    </style>
    <style>
        ul a{
            font-weight: bold;
        }

        .nav-tabs{
            margin-bottom: 20px !important;
        }
        .ui-icon-plusthick{
            font-stretch: extra-condensed;
            outline: 1px solid green;
            background-color: #008000;
            padding: 3px;
            color:white
        }
        div label > input {
            margin-left: 25px !important;
        }
        .childdispInline div{
            display: inline-block !important;
        }

        div.required > label::after{
            content: ' *';
            color:red;
        }

        .childdispInline::after{
            content: '' !important;
        }

        .redstar::after{
            content: ' *';
            color:red;
        }
        .noredstar::after{
            content:'' !important;
        }
        .displayBlock{
            display: inline-block !important;
        }

    </style>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php

        $navItems=[
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Executive ',
                'items'=>[
                    ['label' => 'Executive', 'url' => ['/executive/index']],
                    ['label' => 'Executive Activity Type', 'url' => ['/service/executive-activity-type/index']],
                ]
            ],
            //['label' => 'Executive ', 'url' => ['/executive/index']],

            ['label' => 'Service Requests ', 'url' => ['/service/request-service-activity/index']],
            //['label' => 'Company', 'url' => ['/member/company/index']],
            ['label' => 'Builder/Developer',
                'items'=>[
                    ['label' => 'Builder' ,'url' => ['/realty/builder'] ],
                    ['label' => 'Project', 'url' => ['/realty/property/index',"builder"=>"builder"]],
                    ['label' => 'Banks', 'url' => ['/realty/bank-loans/index']],
                    ['label' => 'Approvals', 'url' => ['/realty/govt-approvals/index']],
                    //['label' => 'Property Units', 'url' => ['/realty/propertyunits/index']],
                ]
            ],

            ['label' => 'Member ',
                'items'=>[
                    ['label' => 'Company', 'url' => ['/member/company/index']],
                    ['label' => 'Individual', 'url' => ['/member/individual/index']],
                    ['label' => 'Tenant', 'url' => ['/member/tenant/index']],
                ]
            ],
            ['label' => 'Property',
                'items'=>[
                    ['label' => 'Property', 'url' => ['/realty/property/index']],

                    ['label' => 'Property Units', 'url' => ['/realty/propertyunits/index']],
                    ['label' => 'Tenant allocation', 'url' => ['/service/tenant-property-allocation/index']],
                ]

            ],
            ['label' => 'Master Data',
                'items'=>[
                    ['label' => 'Country', 'url' => ['/assorted/country/index']],
                    ['label' => 'State ', 'url' => ['/assorted/state/index']],
                    ['label' => 'District/City ', 'url' => ['/assorted/district/index']],
                    ['label' => 'Designation ', 'url' => ['/assorted/designation/index']],
                    ['label' => 'Department ', 'url' => ['/assorted/department/index']],
                    //['label' => 'Project Specification', 'url' => ['/realty/properties/index']],
                    ['label' => 'Property Group', 'url' => ['/realty/propertygroup/index']],
                    ['label' => 'Property Type', 'url' => ['/realty/propertytype/index']],
                    ['label' => 'Inventory Category', 'url' => ['/realty/inventorycategory/index']],
                    ['label' => 'Inventory', 'url' => ['/realty/inventory/index']],
                   // ['label' => 'Inventory Brands', 'url' => ['/realty/inventorybrands/index']],
                    ['label' => 'Amenities', 'url' => ['/realty/amenity/index']],
                    ['label' => 'Area Partitions', 'url' => ['/realty/propertypartitiontype/index']],
                    ['label' => 'Services', 'url' => ['/service/servicetype/index']],
                    ['label' => 'Brand Names', 'url' => ['/realty/brandname/index']],
                    ['label' => 'Executive Activity Type', 'url' => ['/service/executive-activity-type/index']],

                   // ['label' => 'Services List' ,'url' => ['/service/request-services'] ],
                   // ['label' => 'Gratuity Services' ,'url' => ['/service/gratuity-services'] ],



                ],
            ]


            //['label' => 'Status', 'url' => ['/status/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']]
        ];
        if (Yii::$app->user->isGuest) {
           // array_push($navItems,['label' => 'Sign In', 'url' => ['/user/security/login']],['label' => 'Sign Up', 'url' => Yii::$app->urlManager->createUrl("/user/registration/register")]);
        } else {

            $test=['label' => Yii::$app->user->identity->username ,
                'items'=>[
                    ['label' => 'Logout',  'url' => ['/site/logout'],'linkOptions' => ['data-method' => 'post']],
                   /* ['label' => 'Individual', 'url' => ['/member/individual/index']],
                    ['label' => 'Tenant', 'url' => ['/member/tenant/index']],*/
                ]
            ];
            array_push($navItems,$test);

            /*array_push($navItems,['label' => 'Logout  (' . Yii::$app->user->identity->username . ')',

                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']]
            );*/
        }

            NavBar::begin([
                //'brandLabel' => 'VHP',
                'brandLabel' => Html::img('@web/value-home-properties.png', ['alt'=>Yii::$app->name]),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = $navItems;
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>
        <br clear="all"/>
        <img id="greenwhite" src="<?= Yii::$app->urlManager->baseUrl ?>/address-top-bg.jpg" width="100%" height="8px" style="" />

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy;2015 Value Home Properties Pvt Ltd </p>
        <p class="pull-right"><b><?= "Medies Pvt Ltd" ?></b></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
