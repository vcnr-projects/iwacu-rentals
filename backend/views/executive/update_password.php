<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = 'Update Executive: ' . ' ' . $model->Empid;
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Empid, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="executive-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <?php $form->errorSummary($model); ?>
    <?= $form->field($model, 'old_password')->passwordInput() ?>
    <?= $form->field($model, 'new_password')->passwordInput() ?>
    <?= $form->field($model, 'confirm_password')->passwordInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
