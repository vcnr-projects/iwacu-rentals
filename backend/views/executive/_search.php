<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="executive-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'address_proof_id') ?>

    <?= $form->field($model, 'address_proof_path') ?>

    <?= $form->field($model, 'id_proof_path') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'last_active') ?>

    <?php // echo $form->field($model, 'present_address_id') ?>

    <?php // echo $form->field($model, 'permanent_address_id') ?>

    <?php // echo $form->field($model, 'license_no') ?>

    <?php // echo $form->field($model, 'license_expiry') ?>

    <?php // echo $form->field($model, 'department_id') ?>

    <?php // echo $form->field($model, 'designation') ?>

    <?php // echo $form->field($model, 'joining_date') ?>

    <?php // echo $form->field($model, 'salary') ?>

    <?php // echo $form->field($model, 'applicable_for_incentives') ?>

    <?php // echo $form->field($model, 'applicable_for_food_allowance') ?>

    <?php // echo $form->field($model, 'mobile_bill_limit') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
