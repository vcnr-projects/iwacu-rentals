<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use kartik\money\MaskMoney;
use kartik\widgets\FileInput;
?>
<?php $form = ActiveForm::begin(); ?>
<?php echo $form->errorSummary($model); ?>

<div class="panel panel-default">
    <!--<div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Office</a>
        </h4>
    </div>-->
    <div id="collapseOne" class="panel-collapse collapse in">

        <div class="panel-body">
            <?= $form->field($model, 'department_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(),'id','name')) ?>

            <?php
            $branch=\app\modules\assorted\models\OfficeBranches::findOne($model->branch);
            if($branch)
                $model->city=$branch->city;
            ?>

            <?= $form->field($model, 'city')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\OfficeBranches::find()->select(' distinct `city` ')->all(),'city','city')
                ,["prompt"=>"Select"
                    ,'onchange'=>'
             $( "select#executive-branch" ).html( "<option>Loading...</option>" );
                $.post( "'.Yii::$app->urlManager->createUrl('/assorted/office-branches/list-office-branches').'",{"depdrop_parents":$(this).val()}, function( data ) {
                  data=JSON.parse(data);
                  $( "select#executive-branch" ).html( "<option>Select</option>" );
                  for(var i=0;i<data.output.length;i++){
                  var opt=$("<option></option>");
                  $(opt).attr("value",data.output[i][\'id\']);
                  $(opt).text(data.output[i][\'name\']);
                    $( "select#executive-branch" ).append(opt);
                  }
                });
            '
                ]) ?>
            <?= $form->field($model, 'branch')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\OfficeBranches::find()->all(),'branch'
                ,function($model,$defaultValue){
                    return "{$model["branch"]}  ";
                }),["prompt"=>"Select "]) ?>
            <?= $form->field($model, 'designation')->dropDownList(\yii\helpers\ArrayHelper::map(
                \app\modules\assorted\models\Designation::find(["is_active"=>1])->all(),'designation','designation'
            ),["prompt"=>"Select "]) ?>
            <?= $form->field($model, 'joining_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter joining date ...'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy',

                ]
            ]); ?>
            <?= $form->field($model, 'salary')->textInput(["type"=>"number"]);

                /*->widget(MaskMoney::classname(), [
                'pluginOptions' => [
                    'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
                    'suffix' => '',
                    'affixesStay' => true,
                    'thousands' => ',',
                    'decimal' => '.',
                    'precision' => 2,
                    'allowZero' => true,
                    'allowNegative' => false,
                ]
            ]);*/ ?>
            <?= $form->field($model, 'appraisal_perc')->textInput(["type"=>"number"]) ?>

            <?= $form->field($model, 'applicable_for_incentives')->widget(SwitchInput::classname(), ['pluginOptions'=>["onText"=>'Yes',"offText"=>"No"]]) ?>
            <?= $form->field($model, 'applicable_for_food_allowance')->widget(SwitchInput::classname(), ['pluginOptions'=>["onText"=>'Yes',"offText"=>"No"]]) ?>
            <?= $form->field($model, 'mobile_bill_limit')->textInput();
                /*->widget(MaskMoney::classname(), [
                'pluginOptions' => [
                    'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
                    'suffix' => '',
                    'affixesStay' => true,
                    'thousands' => ',',
                    'decimal' => '.',
                    'precision' => 2,
                    'allowZero' => true,
                    'allowNegative' => false,
                ]
            ])*/ ?>
            <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
                'pluginOptions'=>[
                    'handleWidth'=>60,
                    'onText'=>'Active',
                    'offText'=>'Inactive',
                    /*'onSwitchChange'=> 'function(event, state) {
                // Return false to prevent the toggle from switching.
                return false;
            }'*/
                ],
                'pluginEvents' => [
                    "init.bootstrapSwitch" => "function() {  }",
    "switchChange.bootstrapSwitch" => "function(event,state) {

                if(state==0){
                return true;
                }
        var b= confirm('Are you sure the executive is active');
                   if(!b&&state){
                     $(event.target).bootstrapSwitch('toggleState');
                     return;
                     event.preventDefault();
                   }
    return true;
    }",
],
                /*'options'=>[
                    "onchange"=>"if(this.value==1){
                    var b= confirm('Are you sure the executive is active');
                    if(!b){
                      this.value=0
                    }
                    }"
                ]*/
            ]) ?>


        </div>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>

<?php ActiveForm::end(); ?>