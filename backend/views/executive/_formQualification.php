<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use kartik\money\MaskMoney;
use kartik\widgets\FileInput;
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);

?>
<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>

<div class="panel panel-default">
    <!--<div class="panel-heading">

        <h4 class="panel-title">
            <b>  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Qualification</a></b>
        </h4>
    </div>-->
    <div id="collapseOne" class="panel-collapse collapse in">

        <!--begin : education -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Education</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse1">
                <div class="panel-body">
                    <table id="edutab" class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Qualification</th>
                            <th>College/University</th>
                            <th>Year of Passing</th>
                            <th>Percentage</th>
                            <th>Documents</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $model->edu=$model->getExecutiveEducations()->all();
                        $model->csk=$model->getExecutiveComputerSkills()->all();
                        $model->cert=$model->getExecutiveCertifications()->all();
                        $model->lang=$model->getExecutiveLanguages()->all();


                        if(!count($model->edu)){
                            $model->edu=array();
                            $model->edu[] = new \app\models\ExecutiveEducation();
                        }

                        if(!count($model->csk)){
                            $model->csk=array();
                            $model->csk[] = new \app\models\ExecutiveComputerSkill();
                        }

                        if(!count($model->cert)){
                            $model->cert=array();
                            $model->cert[] = new \app\models\ExecutiveCertification();
                        }
                        if(!count($model->lang)){
                            $model->lang=array();
                            $model->lang[] = new \app\models\ExecutiveLanguage();
                        }

                          foreach($model->edu as $i=> $edu):
                        ?>
                        <tr>
                            <td><?= $i+1 ?> </td>
                            <td>
                                <?= $form->field($edu, 'qualification',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveEducation[qualification][]'])

                                ?>
                            </td>
                            <td>
                                <?= $form->field($edu, 'college_university',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveEducation[college_university][]']) ?>

                            </td>
                            <td class="yoptd">

                                <?//= $form->field($edu, 'yop',['template'=>'{input}{error}'])->textInput(['type'=>'number','name'=>'ExecutiveEducation[yop][]']) ?>
                                <?= $form->field($edu, 'yop',['template'=>'{input}{error}'])
                                    ->widget(\kartik\widgets\DatePicker::classname(),[
                                        'options' => ['placeholder' => 'Enter License Expiry Date ...'],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'format'=> " yyyy",
                                            'viewMode'=> "years",
                                            'minViewMode'=> "years"
                                        ],
                                        'options'=>[
                                            'class'=>'date-ui yopinp form-control',
                                            'name'=>'ExecutiveEducation[yop][]',

                                        ]

                                    ]);

                                ?>

                            </td>
                            <td>
                                <?= $form->field($edu, 'percentage',['template'=>'{input}{error}'])->textInput(['type'=>'number','name'=>'ExecutiveEducation[percentage][]']) ?>

                            </td>
                            <td class="filetd" >
                                <?php
                                if(!empty($edu->qualification)){
                                    $a= $model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$edu->qualification])->one();
                                    if(isset($a)){
                                        echo "Uploaded file: {$a->file_path} ";
                                    }
                                }
                                ?>
                                <input type="file" name="attachments<?php echo $i ?>" class="fileattach" />
                            </td>

                        </tr>
                              <?php endforeach;?>
                        </tbody>

                    </table>

                    <?php
                    $script = <<< JS

var edutab;
$(document).ready(function(){

 edutab=$('#edutab').medgridc('',edutabpluginact);

});
var firstexe=0;
function edutabpluginact(tr){
    if(firstexe>0){
    $(".filetd",tr).html('');
    var inp=$('<input type="file" name="attachments" class="fileattach" />');
    $(".filetd",tr).html(inp);
    }
    $(".fileattach").each(function(i){
        $(this).attr("name","attachments"+i);
    });
    firstexe++;

      $("#edutab tbody tr").each(function(){
    var tr=this;
    var toinp=$(".yopinp ",tr).get(0);
    $(".yoptd",tr).html('');
        $(".yoptd",tr).html(toinp);
    //var toinp=$(".date-ui",tr);
     $(toinp).kvDatepicker({format: " yyyy",
    viewMode: "years",
    minViewMode: "years"
    ,'autoclose':true});
    /*$(".dateui").each(function(){
            if(!$(this).data('krajeeKvdatepicker')){
                $(this).kvDatepicker({'format':'dd-mm-yyyy'});
            }
    });*/
    });
}

JS;
                    $this->registerJs($script,\yii\web\View::POS_END);
                    ?>
                </div>
            </div>
        </div>

        <!--end : education -->


        <!--begin:languages -->
        <!--<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Languages</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse1">
                <div class="panel-body">
                    <table id="langtab" class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Language</th>
                            <th>Expertise </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
/*                        foreach($model->lang as $i=> $csk):
                            */?>
                        <tr>
                            <td><?/*= $i+1 */?></td>
                            <td>
                                <?/*= $form->field($csk, 'language',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveLanguage[language][]'])

                                */?>
                            </td>
                            <td>
                                <?/*= $form->field($csk, 'proficiency',['template'=>'{input}{error}'])->dropDownList(["Speak"=>"Speak","Read"=>"Read","Read,Write"=>"Read,Write"],['name'=>'ExecutiveLanguage[proficiency][]']) */?>

                            </td>


                        </tr>
                            <?php /*endforeach; */?>
                        </tbody>

                    </table>

                    <?php
/*                    $script = <<< JS

var langtab;
$(document).ready(function(){

 langtab=$('#langtab').medgridc();

});


JS;
                    $this->registerJs($script,\yii\web\View::POS_END);
                    */?>
                </div>
            </div>
        </div>-->
        <!--end:languages-->


        <!--begin:computer skills-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Skills</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse1">
                <div class="panel-body">
                    <table id="csktab" class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Skill</th>
                            <th>Expertise</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($model->csk as $i=> $csk):
                            ?>
                            <tr>
                                <td>1</td>
                                <td>
                                    <?= $form->field($csk, 'skill',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveComputerSkill[skill][]'])

                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($csk, 'expertise',['template'=>'{input}{error}'])->dropDownList(["Beginner"=>"Beginner","Intermediate"=>"Intermediate","Expert"=>"Expert"],['name'=>'ExecutiveComputerSkill[expertise][]']) ?>

                                </td>


                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    </table>

                    <?php
                    $script = <<< JS

var csktab;
$(document).ready(function(){

 csktab=$('#csktab').medgridc();

});


JS;
                    $this->registerJs($script,\yii\web\View::POS_END);
                    ?>
                </div>
            </div>
        </div>
        <!--end:computer skills-->


        <!--begin : certification -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Other Certificates </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse1">
                <div class="panel-body">
                    <table id="certtab" class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Certificate</th>
                            <th>Issued Date</th>
                            <th>Documents</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($model->cert as $i=> $edu):
                            ?>
                            <tr>
                                <td><?= $i+1 ?> </td>
                                <td>
                                    <?= $form->field($edu, 'certificate',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveCertification[certificate][]'])

                                    ?>
                                </td>


                                <td class="fromdatetd">
                                    <?= $form->field($edu, 'issued_date',['template'=>'{input}{error}'])
                                        ->widget(\kartik\widgets\DatePicker::classname(), [
                                            'options' => ['placeholder' => 'Enter Issued Date'],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'format' => 'dd-mm-yyyy',
                                            ],
                                            'options'=>[
                                                'class'=>'date-ui form-control',
                                                'name'=>'ExecutiveCertification[issued_date][]',

                                            ]

                                        ]);

                                    ?> </td>

                                </td>

                                <td class="filetd" >
                                    <?php
                                    if(!empty($edu->certificate)){
                                        $a= $model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$edu->certificate])->one();
                                        if(isset($a)){
                                            echo "Uploaded file: {$a->file_path} ";
                                        }
                                    }
                                    ?>
                                    <input type="file" name="attachments<?php echo $i ?>" class="fileattachc" />
                                </td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>

                    </table>

                    <?php
                    $script = <<< JS

var certtab;
$(document).ready(function(){
 certtab=$('#certtab').medgridc('',certtabpluginact);
});
var firstexec=0;
function certtabpluginact(tr){

$("#certtab tbody tr").each(function(){
    var tr=this;
    var frominp=$("td:eq(2) .date-ui",tr).get(0);
    $(".tfromdatetd",tr).html('');
    $(".fromdatetd",tr).html(frominp);
    //var toinp=$(".date-ui",tr);
     $(frominp).kvDatepicker({'format':'dd-mm-yyyy','autoclose':true});
    /*$(".dateui").each(function(){
            if(!$(this).data('krajeeKvdatepicker')){
                $(this).kvDatepicker({'format':'dd-mm-yyyy'});
            }
    });*/
    });


    if(firstexec>0){
    $(".filetd",tr).html('');
    var inp=$('<input type="file" name="attachmentsCert0" class="fileattachc" />');
    $(".filetd",tr).html(inp);
    }
    $(".fileattachc").each(function(i){
        $(this).attr("name","attachmentsCert"+i);
    });
    firstexec++;
}

JS;
                    $this->registerJs($script,\yii\web\View::POS_END);
                    ?>
                </div>
            </div>
        </div>

        <!--end : certification -->



    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>

<?php ActiveForm::end(); ?>