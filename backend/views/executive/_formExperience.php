<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>
<!--begin:experience-->
<div class="panel panel-default">
    <!--<div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Experience </a>
        </h4>
    </div>-->

    <div id="collapseFour" class="panel-collapse collapse1">
        <div class="panel-body">
            <label><input type="checkbox" name="fresher" value="1" onchange="fresherclick(this)"/>&nbsp;&nbsp;&nbsp;&nbsp;Fresher</label>
            <script>
                function fresherclick(obj){
                    if($(obj).is(":checked")){
                        alert("All Experiece Data will be deleted");
                    }
                }
            </script>
            <table id="exptab" class="table">
                <thead>
                <tr>
                    <th>SL No.</th>
                    <th>Company</th>
                    <th>Designation</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th> Experience/Reliving/Others </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $model->exp=$model->getExecutiveExperiences()->all();
                $model->vech=$model->getExecutiveVehicles()->all();

                if(!count($model->exp)){
                    $model->exp=array();
                    $model->exp[] = new \app\models\ExecutiveExperience();
                }
                if(!count($model->vech)){
                    $model->vech=array();
                    $model->vech[] = new \app\models\ExecutiveVehicle();
                }
                foreach($model->exp as $i=> $exp):
                    ?>
                    <tr>
                        <td>1</td>
                        <td>
                            <?= $form->field($exp, 'company',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveExperience[company][]'])
                            ?>
                        </td>
                        <td>
                            <?= $form->field($exp, 'designation',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveExperience[designation][]']) ?>
                        </td>
                        <td class="fromdatetd">
                            <?= $form->field($exp, 'from',['template'=>'{input}{error}'])
                                ->widget(\kartik\widgets\DatePicker::classname(),[
                                    'options' => ['placeholder' => 'Enter License Expiry Date ...'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd-mm-yyyy',
                                    ],
                                    'options'=>[
                                        'class'=>'date-ui form-control',
                                        'name'=>'ExecutiveExperience[from][]',

                                    ]

                                ]);

                            ?>                        </td>
                        <td  class="todatetd">
                            <?= $form->field($exp, 'to',['template'=>'{input}{error}'])
                                ->widget(\kartik\widgets\DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Enter License Expiry Date ...'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd-mm-yyyy',
                                    ],
                                    'options'=>[
                                        'class'=>'date-ui form-control',
                                        'name'=>'ExecutiveExperience[to][]'
                                    ]

                                ]);

                            ?>
                        </td>
                        <td class="filetd" >
                            <?php
                            if(!empty($exp->designation)){
                                $a= $model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$exp->designation.$exp->company])->one();
                                if(isset($a)){
                                    echo "Uploaded file: {$a->file_path} ";
                                }
                            }
                            ?>
                            <input type="file" name="attachments<?php echo $i ?>" class="fileattach" />
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php
            $script = <<< JS
var exptab;
$(document).ready(function(){
 exptab=$('#exptab').medgridc('',exptabPlugAct);
 var firstexe=0;

 function exptabPlugAct(tr){
    $("#exptab tbody tr").each(function(){
    var tr=this;
    var toinp=$("td:eq(4) .date-ui",tr).get(0);
    var frominp=$("td:eq(3) .date-ui",tr).get(0);
    $(".todatetd",tr).html('');
    $(".tfromdatetd",tr).html('');
   // var toinp=$("<input type=text name='ExecutiveExperience[to][]' class='date-ui form-control' />");
   // var frominp=$("<input type=text name='ExecutiveExperience[from][]' class='date-ui form-control' />");
        $(".todatetd",tr).html(toinp);
    $(".fromdatetd",tr).html(frominp);
    //var toinp=$(".date-ui",tr);
     $(toinp).kvDatepicker({'format':'dd-mm-yyyy','endDate': 'd','autoclose':true});
     $(frominp).kvDatepicker({'format':'dd-mm-yyyy','endDate': 'd','autoclose':true});
     $(frominp).kvDatepicker({'format':'dd-mm-yyyy','autoclose':true});
    /*$(".dateui").each(function(){
            if(!$(this).data('krajeeKvdatepicker')){
                $(this).kvDatepicker({'format':'dd-mm-yyyy'});
            }
    });*/
    });

    if(firstexe>0){
    $(".filetd",tr).html('');
    var inp=$('<input type="file" name="attachments" class="fileattach" />');
    $(".filetd",tr).html(inp);
    }
    $(".fileattach").each(function(i){
        $(this).attr("name","attachments"+i);
    });
    firstexe++;

 }
});



JS;
            $this->registerJs($script,\yii\web\View::POS_END);
            ?>
        </div>
    </div>
</div>
<!--end:experience-->



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>