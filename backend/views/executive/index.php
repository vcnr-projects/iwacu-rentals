<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExecutiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Executives';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <a href="<?php echo Yii::$app->urlManager->createUrl(["/executive/createwiz_general",'id'=>0,'wiz'=>1]) ?>"
            class="btn btn-success"
            >Create Executive</a>
        <? //= Html::a('Create Executive', ['createwiz_general&id=0&wiz=1'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'empid',
                'label'=>"Employee ID",
                //'filter'=>Html::textInput('ExecutiveSearch[empid]',$searchModel->empid),
            ],
            [
                'attribute'=>'fullname',
                'label'=>"Full Name",
            ],
            [
                'attribute'=>'contact',
                'label'=>"Contact",
            ],
            [
                'attribute'=>'activestatus',
                'label'=>"Status",
                'filter'=>Html::dropDownList("ExecutiveSearch[activestatus]",$searchModel->activestatus,["1"=>"Active","0"=>"In-Active"],["prompt"=>"All","class"=>"form-control"])
            ],

            //'address_proof_id',

            // 'created_on',
            // 'last_active',
            // 'present_address_id',
            // 'permanent_address_id',
            // 'license_no',
            // 'license_expiry',
            // 'department_id',
            // 'designation',
            // 'joining_date',
            // 'salary',
            // 'applicable_for_incentives',
            // 'applicable_for_food_allowance',
            // 'mobile_bill_limit',
            // 'status',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update-password}',
                'buttons'=>[
                    'update-password' => function ($url, $model) {
                        return Html::a('&nbsp;&nbsp;<span class="glyphicon glyphicon-lock"></span>', $url, [
                            'title' => Yii::t('yii', 'Update Password'),
                        ]);

                    }
                ]
            ],
        ],
    ]); ?>

</div>
