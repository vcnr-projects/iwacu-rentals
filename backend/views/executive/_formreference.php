<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveReference */
/* @var $form ActiveForm */




/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = 'Create Executive [Reference Section]';
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="form">
    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs ">
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_general","id"=>$id]) ?>">Personal Information</a></li>
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_qualification","id"=>$id]) ?>">Qualification</a></li>
        <li><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_experience","id"=>$id]) ?>">Experience </a></li>
        <li><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_vehicle","id"=>$id]) ?>"> Vehicle Details</a></li>
        <li class="active"><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_reference","id"=>$id]) ?>">Reference</a></li>
        <li  ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_office","id"=>$id]) ?>">Office</a></li>
        <li  ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_insurance","id"=>$id]) ?>">Medical Insurance</a></li>

    </ul>


    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <? //= $form->errorSummary($model) ?>


    <div id="proto" class="panel">

    <?= $form->field($model[0], 'name')->textInput(['name'=>"ExecutiveReference[name][]"]) ?>
    <?= $form->field($model[0], 'company') ->textInput(['name'=>"ExecutiveReference[company][]"])?>
    <?//= $form->field($model, 'exec_id') ?>
    <?= $form->field($model[0], 'designation')->textInput(['name'=>"ExecutiveReference[designation][]"]) ?>
    <?= $form->field($model[0], 'phno')->textInput(['name'=>"ExecutiveReference[phno][]"]) ?>
    <?= $form->field($model[0], 'email')->textInput(['name'=>"ExecutiveReference[email][]"]) ?>

    <?= $form->field($model[0], 'adrs')->textarea(['name'=>"ExecutiveReference[adrs][]"]) ?>
    <?= $form->field($model[0], 'remarks')->textarea(['name'=>"ExecutiveReference[remarks][]"]) ?>
    <?
    $baseurl=Yii::$app->urlManager->baseUrl;
    if($model[0]->reference_letter){
        echo "
            <div class='uploadeddiv'>
            Uploaded File:
        <a href='{$baseurl}'>{$model[0]->reference_letter}</a>
        </div>
        ";
    }
    ?>
    <? /*= $form->field($model, 'reference_letter[]')->widget(\kartik\file\FileInput::className(),['pluginOptions'=>[
        'showUpload'=>false
    ]])*/ ?>
    <?= $form->field($model[0], 'reference_letter[]')->fileInput() ?>

       <!-- <button type="button" class="btn" onclick="$(this).closest('#proto').remove()" >Delete</button>-->
        <hr style="border-top:1px solid #000000"/>
    </div>
    <button type="button" class="btn pull-right "
            onclick="addref()">Add one more reference </button>
        <script>
            function addref(){
                var cln=$('#proto').clone();
                $('input,textarea',cln).val('');
                $('.uploadeddiv',cln).html('');
                $('#nextreferer').append(cln);
            }
        </script>
    <div id="nextreferer" >
        <?php foreach($model as $i=> $mdel){
            if($i==0)continue;
            ?>
            <?= $form->field($model[0], 'name')->textInput(['name'=>"ExecutiveReference[name][]"]) ?>
            <?= $form->field($model[0], 'company') ->textInput(['name'=>"ExecutiveReference[company][]"])?>
            <?//= $form->field($model, 'exec_id') ?>
            <?= $form->field($model[0], 'designation')->textInput(['name'=>"ExecutiveReference[designation][]"]) ?>
            <?= $form->field($model[0], 'phno')->textInput(['name'=>"ExecutiveReference[phno][]"]) ?>
            <?= $form->field($model[0], 'email')->textInput(['name'=>"ExecutiveReference[email][]"]) ?>
            <?= $form->field($model[0], 'adrs')->textarea(['name'=>"ExecutiveReference[adrs][]"]) ?>
            <?= $form->field($model[0], 'remarks')->textarea(['name'=>"ExecutiveReference[remarks][]"]) ?>

            <?
            $baseurl=Yii::$app->urlManager->baseUrl;
            if($mdel->reference_letter){
                echo "
            Uploaded File:
        <a href='{$baseurl}'>{$mdel->reference_letter}</a>
        ";
            }
            ?>
            <? /*= $form->field($model, 'reference_letter[]')->widget(\kartik\file\FileInput::className(),['pluginOptions'=>[
        'showUpload'=>false
    ]])*/ ?>
            <?= $form->field($mdel, 'reference_letter[]')->fileInput() ?>
        <?php
        }
        ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn  btn-block btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->