<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use kartik\money\MaskMoney;
use kartik\widgets\FileInput;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>


<div class="panel panel-default">
    <!--<div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Personal Information</a>
        </h4>
    </div>-->
    <style>
        div label > input {
            margin-left: 25px !important;
            /* padding-left: 100px !important;
             color: red;*/
        }
        /*div label:first-child > input {
            margin-left: 0px !important;

        }*/
        .childdispInline div{
            display: inline-block !important;
        }
        .ui-icon-plusthick{
            font-stretch: extra-condensed;
            outline: 1px solid green;
            background-color: #008000;
            padding: 3px;
            color:white
        }

        div.required > label::after{
            content: ' *';
            color:red;
        }

        .childdispInline::after{
            content: '' !important;
        }

        .redstar::after{
            content: ' *';
            color:red;
        }


    </style>



    <div id="collapseOne" class="panel-collapse collapse in">

        <div class="panel-body">
            <?= $form->field($model, 'title',['template'=>'<label class="childdispInline " > <span class="redstar">Title</span>  :{input}{error}</label>'])->radioList(
                \yii\helpers\ArrayHelper::map(\app\models\PersonTitle::find()->asArray()->all(),
                    'name','name')
                ) ?>
            <?= $form->field($model, 'fname')->textInput() ?>

            <?= $form->field($model, 'lname')->textInput() ?>
            <?= $form->field($model, 'fathername')->textInput() ?>
            <?= $form->field($model, 'mname')->textInput() ?>

            <?= $form->field($model, 'marital_status',['template'=>'<label class="childdispInline" > Marital Status :{input}{error}</label>'])
                ->radioList(["Single"=>"Single","Married"=>"Married"]) ?>
            <?= $form->field($model, 'gender',['template'=>'<label class="childdispInline" > Gender :{input}{error}</label>'])->radioList(["Male"=>"Male","Female"=>"Female"]) ?>
            <?= $form->field($model, 'email')->textInput() ?>
            <?= $form->field($model, 'dob')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter Date of Birth'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy',
                ]
            ]); ?>
            <?/*= $form->field($model, 'license_no')->textInput() */?><!--
            --><?/*= $form->field($model, 'license_expiry')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter License Expiry Date ...'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy',
                ]
            ]); */?>
            <?php
            $model->attach=$model->getExecutiveAttachments()->filterWhere(["attachment_type"=>"Address Proof"])
                ->orWhere(["attachment_type"=>"ID Proof"])
                ->orWhere(["attachment_type"=>"Present Address Proof"])
                ->orWhere(["attachment_type"=>"Photo"])
                ->orWhere(["attachment_type"=>"Resume"])
                ->all();
            $apa;
            $ipa;
            Yii::trace(\yii\helpers\VarDumper::dumpAsString($model->attach),'vardump');
            foreach($model->attach as $ma){
                if($ma->attachment_type=="Address Proof"){
                    $apa=$ma;
                }elseif($ma->attachment_type=="ID Proof"){
                    $ipa=$ma;
                }
                elseif($ma->attachment_type=="Photo"){
                    $photoattach=$ma;
                }
                elseif($ma->attachment_type=="Resume"){
                    $resumeattach=$ma;
                }
                elseif($ma->attachment_type=="Present Address Proof"){
                    $pap=$ma;
                }

            }

            ?>


            <!--<div class="form-group field-executiveattachments-id">
                <label class="control-label" for="executiveattachments-id">Driving License</label>
                <?php
/*                if(isset($ipa)){
                    echo "
            <div>
                Uploaded File : {$ipa->file_path}
            </div>
            ";
                }else{
                    $ipa=new \app\models\ExecutiveAttachments();
                }

                */?>
                <?php
/*                echo FileInput::widget([
                    'name' => 'idproofdoc',
                    'pluginOptions' => [
                        'showUpload' => false,

                    ]
                ]);
                */?>
                <div class="help-block"></div>
            </div>-->

            <!--begin :photo-->
            <div class="form-group field-executiveattachments-id">
                <label class="control-label" for="executiveattachments-id">Executive Photo</label>
                <?php
                if(isset($photoattach)){
                    echo "
            <div>
                Uploaded File :<a href='".Yii::$app->urlManager->baseUrl."/uploads/".$photoattach->file_path."'>   {$photoattach->file_path}</a>
            </div>
            ";
                }else{
                    $photoattach=new \app\models\ExecutiveAttachments();
                }


                ?>
                <?php
                echo FileInput::widget([
                    'name' => 'photodoc',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showUpload' => false,

                    ]
                ]);
                ?>
                <div class="help-block"></div>
            </div>
            <!--end: photo-->

            <!--begin :resume-->
            <div class="form-group field-executiveattachments-id">
                <label class="control-label" for="executiveattachments-id">Resume</label>
                <?php
                if(isset($resumeattach)){
                    echo "
            <div>
                Uploaded File :<a href='".Yii::$app->urlManager->baseUrl."/uploads/".$resumeattach->file_path."'>   {$resumeattach->file_path}</a>
            </div>
            ";
                }else{
                    $resumeattach=new \app\models\ExecutiveAttachments();
                }

                ?>
                <?php
                echo FileInput::widget([
                    'name' => 'resumedoc',
                    'pluginOptions' => [
                        'showUpload' => false,

                    ]
                ]);
                ?>
                <div class="help-block"></div>
            </div>
            <!--end: resume-->





            <div class="panel-body">

                <?php

                $pera=$model->getAddressProof()->one();


                if(($pera)){
                    $model->proAdrs->setAttributes($pera->getAttributes());
                    $model->proAdrs->id=$pera->id;
                }

                $protel=$model->proAdrs->getTelephone()->one();
                Yii::trace(\yii\helpers\VarDumper::dumpAsString($protel),'vardump');
                if(empty($protel)){
                    $protel=new \app\models\ProTelephone();
                    $model->proTel=$protel;
                }else {
                    $model->proTel = new \app\models\ProTelephone();
                    $model->proTel->setAttributes($protel->getAttributes());
                }
                //$protel=new \app\models\Telephone();

                $prea=$model->getPresentAddress()->one();
                if(($prea)){
                    $model->preAdrs->setAttributes($prea->getAttributes());
                }
                $pretel=$model->preAdrs->getTelephone()->one();

                if(empty($pretel)){

                    $pretel=new \app\models\PreTelephone();
                    $model->preTel=$pretel;

                }else {

                    $model->preTel = new \app\models\PreTelephone();
                    $model->preTel->setAttributes($pretel->getAttributes());

                }



                $pera=$model->getPermanentAddress()->one();
                if(($pera)){
                    $model->perAdrs->setAttributes($pera->getAttributes());
                }
                ?>

                <div class="row">


                    <div class=" panel-default col-md-12">
                        <div class="panel-heading">
                            Present Address


                        </div>
                        <div class="panel-body">
                            <?= $form->field($model->preAdrs, 'adrs_line1')->textarea() ?>
                            <div style="display: none">
                                <?= $form->field($model->preAdrs, 'adrs_line2')->textarea() ?>
                            </div>

                            <?= $form->field($model->preAdrs, 'city')->
                            widget(\kartik\typeahead\Typeahead::className(),[
                                    'dataset' => [
                                        [
                                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                            'display' => 'value',
                                            //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                            'remote' => [
                                                'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'?q=%QUERY',
                                                //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                                'wildcard' => '%QUERY',
                                                /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                                    //console.console.log(query);
                                                                    console.console.log(settings);

                                                                    settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                                    settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                                    return settings;

                                                                }"),*/
                                                'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                                'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                                'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                                            ],

                                        ]
                                    ],
                                    'pluginOptions' => ['highlight' => true],
                                    'pluginEvents'=>[

                                        "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#preaddress-country').val(cd.country);
                $('#preaddress-state').val(cd.state);
            }
        }
     }",
                                        "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                                    ],
                                    'options' => ["id"=>'preaddress-city'],
                                ]) ?>
                            <?= $form->field($model->preAdrs, 'postal_code')->textInput() ?>

                            <?= $form->field($model->preAdrs, 'state')->textInput();
                             ?>

                            <?= $form->field($model->preAdrs, 'country')->textInput()
                            ?>



                            <?= $form->field($model->preTel,'primary_landline')->textInput() ?>
                            <?= $form->field($model->preTel,'secondary_landline')->textInput() ?>
                            <?= $form->field($model->preTel,'primary_mobile')->textInput() ?>
                            <?= $form->field($model->preTel,'secondary_mobile')->textInput() ?>

                            <?= $form->field($model->preAdrs, 'proof_doc_type')->radioList([
                                "DL"=>"DL"
                                ,"Aadhar Card"=>"Aadhar Card"
                                ,"Passport"=>"Passport"
                                ,"Voter ID"=>"Voter ID"
                            ])
                            ?>

                            <!--begin :present address proof-->
                            <div class="form-group field-executiveattachments-id">
                                <label class="control-label" for="executiveattachments-id">Present Address Proof</label>
                                <?php
                                if(isset($pap)){
                                    echo "
            <div>
                Uploaded File : <a href='".Yii::$app->urlManager->baseUrl."/uploads/".$pap->file_path."'>   {$pap->file_path}</a>
            </div>
            ";
                                }else{
                                    $pap=new \app\models\ExecutiveAttachments();
                                }

                                ?>
                                <?php
                                echo FileInput::widget([
                                    'name' => 'presentadrsproof',
                                    'options' => ['accept' => 'application/pdf,image/*,application/octet-stream',],
                                    'pluginOptions' => [
                                        'showUpload' => false,

                                    ]

                                ]);
                                ?>
                                <div class="help-block"></div>
                            </div>
                            <!--end: present address proof-->
                            <?/*=



                            $form->field($pap, 'file_path',['template'=>'<label>Present Address Proof </label>{input}{error}'])->widget(FileInput::classname(), [
                                'name'=>'fasad',
                                'options' => ['accept' => 'application/pdf,image/*,application/octet-stream',],
                            ]); */?>

                        </div>
                    </div>
                    <!--
                    <div class=" panel-default col-md-6" style="display: none">
                        <div class="panel-heading">
                            Permanent Address
                            <button type="button" class="btn btn-green pull-right " onclick="samePresentAdrs()">Same as Present Address </button>
                            <script>
                                function samePresentAdrs(){

                                    $("[id=peraddress-adrs_line1]")[0].value=($("[id=preaddress-adrs_line1]")[0].value);
                                    $("[id=peraddress-adrs_line2]")[0].value=($("[id=preaddress-adrs_line2]")[0].value);
                                    $("[id=peraddress-city]")[0].value=($("[id=preaddress-city]")[0].value);
                                    $("[id=peraddress-state]")[0].value=($("[id=preaddress-state]")[0].value);
                                    $("[id=peraddress-country]")[0].value=($("[id=preaddress-country]")[0].value);
                                    $("[id=peraddress-postal_code]")[0].value=($("[id=preaddress-postal_code]")[0].value);

                                }
                            </script>
                        </div>
                        <div class="panel-body">
                            <?/*= $form->field($model->perAdrs, 'adrs_line1')->textarea() */?>
                            <?/*= $form->field($model->perAdrs, 'adrs_line2')->textarea() */?>
                            <?/*= $form->field($model->perAdrs, 'city')->textInput() */?>
                            <?/*= $form->field($model->perAdrs, 'state')->textInput() */?>
                            <?/*= $form->field($model->perAdrs, 'country')->textInput() */?>
                            <?/*= $form->field($model->perAdrs, 'postal_code')->textInput() */?>


                        </div>
                    </div>
                    -->
                </div>

                <div class=" panel panel-default ">
                    <div class="panel-heading">Permanent Address

                        <button type="button" class="btn btn-green pull-right " onclick="sameAdrsProof()">Same as  Present Address</button>
                        <script>
                            function sameAdrsProof(){

                               /* $("[id=preaddress-adrs_line1]")[0].value=($("[id=proaddress-adrs_line1]")[0].value);
                                $("[id=preaddress-adrs_line2]")[0].value=($("[id=proaddress-adrs_line2]")[0].value);
                                $("[id=preaddress-city]")[0].value=($("[id=proaddress-city]")[0].value);
                                $("[id=preaddress-state]")[0].value=($("[id=proaddress-state]")[0].value);
                                $("[id=preaddress-country]")[0].value=($("[id=proaddress-country]")[0].value);
                                $("[id=preaddress-postal_code]")[0].value=($("[id=proaddress-postal_code]")[0].value);


                                $("[id=pretelephone-primary_landline]")[0].value=($("[id=protelephone-primary_landline]")[0].value);
                                $("[id=pretelephone-secondary_landline]")[0].value=($("[id=protelephone-secondary_landline]")[0].value);
                                $("[id=pretelephone-primary_mobile]")[0].value=($("[id=protelephone-primary_mobile]")[0].value);
                                $("[id=pretelephone-secondary_mobile]")[0].value=($("[id=protelephone-secondary_mobile]")[0].value);
                               */ //alert($("#executiveattachments-file_path").val());
                                $("[id=proaddress-adrs_line1]")[0].value=($("[id=preaddress-adrs_line1]")[0].value);
                                $("[id=proaddress-adrs_line2]")[0].value=($("[id=preaddress-adrs_line2]")[0].value);
                                $("[id=proaddress-city]")[0].value=($("[id=preaddress-city]")[0].value);
                                $("[id=proaddress-state]")[0].value=($("[id=preaddress-state]")[0].value);
                                $("[id=proaddress-country]")[0].value=($("[id=preaddress-country]")[0].value);
                                $("[id=proaddress-postal_code]")[0].value=($("[id=preaddress-postal_code]")[0].value);


                                $("[id=protelephone-primary_landline]")[0].value=($("[id=pretelephone-primary_landline]")[0].value);
                                $("[id=protelephone-secondary_landline]")[0].value=($("[id=pretelephone-secondary_landline]")[0].value);
                                $("[id=protelephone-primary_mobile]")[0].value=($("[id=pretelephone-primary_mobile]")[0].value);
                                $("[id=protelephone-secondary_mobile]")[0].value=($("[id=pretelephone-secondary_mobile]")[0].value);


                                //$("#w4").val($("#executiveattachments-file_path").val());
                                //$("#executiveattachments-file_path").val($("#w4").val());

                            }
                        </script>
                    </div>
                    <div class="panel-body">
                        <?= $form->field($model->proAdrs, 'adrs_line1')->textarea() ?>
                        <div style="display: none">
                        <?= $form->field($model->proAdrs, 'adrs_line2')->textarea() ?>
                        </div>



                        <?php $baseUrl= Yii::$app->urlManager->getBaseUrl(); ?>

                        <?= $form->field($model->proAdrs, 'city')->
                        widget(\kartik\typeahead\Typeahead::className(),[
                                'dataset' => [
                                    [
                                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                        'display' => 'value',
                                        //'prefetch' => Yii::$app->urlManager->createUrl( '/assorted/country/jsonautocomplete'),
                                        'remote' => [
                                            'url' =>Yii::$app->urlManager->createUrl( ['/assorted/district/listofcities']).'&q=%QUERY',
                                            //'url' => Url::to(['assorted/country/jsonautocomplete']) . '?q=%QUERY',
                                            'wildcard' => '%QUERY',
                                            /*'prepare'=>new \yii\web\JsExpression("function(query, settings){
                                                                //console.console.log(query);
                                                                console.console.log(settings);

                                                                settings.url=settings.url.replace(/%QUERY/,$('#peraddress-city').val())
                                                                settings.url=settings.url+'&state='+$('#peraddress-state').val()+'&country='+$('#peraddress-country').val()
                                                                return settings;

                                                            }"),*/
                                            'ajax' => ['complete' => new \yii\web\JsExpression("function(response){
                                                alert();//jQuery('#serial_product')
                                            }")],
                                            'itemSelected' =>new \yii\web\JsExpression("function(response){
                                                alert(respnose);
                                            }"),
                                            'transform'=>new \yii\web\JsExpression("function(response){
                                                if(typeof response !='undefined')
                                                cityData=response;
                                                console.log('transform');
                                                console.log(cityData);
                                                return response;
                                            }")

                                        ],

                                    ]
                                ],
                                'pluginOptions' => ['highlight' => true],
                                'pluginEvents'=>[

                                    "typeahead:change" => "function(e, datum) {
    console.log('change');
    console.log(cityData);
        for(i in cityData){
          var cd=cityData[i];

            if(cd.value&&cd.value==datum){
                $('#proaddress-country').val(cd.country);
                $('#proaddress-state').val(cd.state);
            }
        }
     }",
                                    "typeahead:select" => "function(e, datum) { console.log(datum);
            /*$('#peraddress-country').val(datum.country);
            $('#peraddress-state').val(datum.state);*/
     }",
                                ],
                                'options' => ["id"=>'proaddress-city'],
                            ]) ?>
                        <?= $form->field($model->proAdrs, 'postal_code')->textInput() ?>

                        <?= $form->field($model->proAdrs, 'state')->textInput();
                        ?>

                        <?= $form->field($model->proAdrs, 'country')->textInput()
                        ?>
                        <?= $form->field($model->proTel,'primary_landline')->textInput() ?>
                        <?= $form->field($model->proTel,'secondary_landline')->textInput() ?>
                        <?= $form->field($model->proTel,'primary_mobile')->textInput() ?>
                        <?= $form->field($model->proTel,'secondary_mobile')->textInput() ?>
                        <?= $form->field($model->proAdrs, 'proof_doc_type')->radioList([
                            "DL"=>"DL"
                            ,"Aadhar Card"=>"Aadhar Card"
                            ,"Passport"=>"Passport"
                            ,"Voter ID"=>"Voter ID"
                        ])
                        ?>
                        <?php


                        if(isset($apa)){
                            echo "
                                <div >
                                  Uploaded File :<a href='".Yii::$app->urlManager->baseUrl."/uploads/".$apa->file_path."'>   {$apa->file_path}</a>
                                </div>
                                ";
                        }else{
                            $apa=new \app\models\ExecutiveAttachments();
                        }

                        ?>
                        <?=


                        $form->field($apa, 'file_path',['template'=>'<label>Permanent Address Proof </label>{input}{error}'])->widget(FileInput::classname(), [
                            'name'=>'fasd',
                            'options' => ['accept' => 'application/pdf,image/*,application/octet-stream',],
                            'pluginOptions' => [
                                'showUpload' => false,

                            ]
                        ]); ?>

                    </div>
                </div>



            </div>

        </div>
    </div>
</div>

    <!--begin:languages-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Languages</a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="langtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Language</th>
                        <th>Expertise </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $model->lang=$model->getExecutiveLanguages()->all();
                    if(!count($model->lang)){
                        $model->lang=[];
                        $model->lang[]=new \app\models\ExecutiveLanguage();
                    }
                    foreach($model->lang as $i=> $csk):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <?= $form->field($csk, 'language',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveLanguage[language][]'])

                                ?>
                            </td>
                            <td>
                                <?= $form->field($csk, 'proficiency',['template'=>'{input}{error}'])->dropDownList(["Speak"=>"Speak","Read"=>"Read","Read,Write"=>"Read,Write"],['name'=>'ExecutiveLanguage[proficiency][]']) ?>

                            </td>


                        </tr>
                    <?php endforeach; ?>
                    </tbody>

                </table>

                <?php
                $this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);

                $script = <<< JS

var langtab;
$(document).ready(function(){

 langtab=$('#langtab').medgridc();

});


JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:languages-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>

<?php ActiveForm::end(); ?>