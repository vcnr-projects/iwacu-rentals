<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = 'Create Executive';
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
