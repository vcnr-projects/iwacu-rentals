<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use kartik\money\MaskMoney;
use kartik\widgets\FileInput;



/* @var $this yii\web\View */
/* @var $model app\models\Executive */
/* @var $form yii\widgets\ActiveForm */



$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
?>
<style>
    .panel-heading h4 a:after {
        /* symbol for "opening" panels */
        font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
        content:"\e080";    /* adjust as needed, taken from bootstrap.css */
        float: right;        /* adjust as needed */
        color: grey;         /* adjust as needed */
    }
    .panel-heading h4 a.collapsed:after {
        /* symbol for "collapsed" panels */
        content:  "\e114";    /* adjust as needed, taken from bootstrap.css */
    }
    .panel-heading h4 a{
        display:block;
    }
</style>

<div class="executive-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $form->errorSummary($model); ?>


    <div class="form-data">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1. General</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <?= $form->field($model, 'fname')->textInput() ?>
                        <?= $form->field($model, 'lname')->textInput() ?>
                        <?= $form->field($model, 'mname')->textInput() ?>

                        <?= $form->field($model, 'email')->textInput() ?>
                        <?= $form->field($model, 'license_no')->textInput() ?>
                        <?= $form->field($model, 'license_expiry')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter License Expiry Date ...'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy',

                            ]
                        ]); ?>
                        <?= $form->field($model, 'department_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(),'id','name')) ?>
                        <?= $form->field($model, 'designation')->textInput() ?>
                        <?= $form->field($model, 'joining_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter joining date ...'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy',

                            ]
                        ]); ?>
                        <?= $form->field($model, 'salary')->widget(MaskMoney::classname(), [
                            'pluginOptions' => [
                                'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
                                'suffix' => '',
                                'affixesStay' => true,
                                'thousands' => ',',
                                'decimal' => '.',
                                'precision' => 2,
                                'allowZero' => true,
                                'allowNegative' => false,
                            ]
                        ]); ?>
                        <?= $form->field($model, 'applicable_for_incentives')->widget(SwitchInput::classname(), []) ?>
                        <?= $form->field($model, 'applicable_for_food_allowance')->widget(SwitchInput::classname(), []) ?>
                        <?= $form->field($model, 'mobile_bill_limit')->widget(MaskMoney::classname(), [
                            'pluginOptions' => [
                                'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
                                'suffix' => '',
                                'affixesStay' => true,
                                'thousands' => ',',
                                'decimal' => '.',
                                'precision' => 2,
                                'allowZero' => true,
                                'allowNegative' => false,
                            ]
                        ]) ?>
                        <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
                            'pluginOptions'=>[
                            'handleWidth'=>60,
                            'onText'=>'Active',
                            'offText'=>'Inactive'
                            ]
                        ]) ?>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2.Address</a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">


                        <div class=" panel panel-default ">
                            <div class="panel-heading"> Address Proof</div>
                            <div class="panel-body">
                                <?= $form->field($model->proAdrs, 'adrs_line1')->textarea() ?>
                                <?= $form->field($model->proAdrs, 'adrs_line2')->textarea() ?>
                                <?= $form->field($model->proAdrs, 'city')->textInput() ?>
                                <?= $form->field($model->proAdrs, 'state')->textInput() ?>
                                <?= $form->field($model->proAdrs, 'country')->textInput() ?>
                                <?= $form->field($model->proAdrs, 'postal_code')->textInput() ?>
                                <?= $form->field($model, 'address_proof_path')->widget(FileInput::classname(), [
                                    'options' => ['accept' => 'application/pdf,image/*,application/octet-stream'],
                                ]); ?>

                            </div>
                        </div>

                        <div class="row">


                        <div class=" panel-default col-md-6">
                            <div class="panel-heading">Present Address</div>
                            <div class="panel-body">
                                <?= $form->field($model->preAdrs, 'adrs_line1')->textarea() ?>
                                <?= $form->field($model->preAdrs, 'adrs_line2')->textarea() ?>
                                <?= $form->field($model->preAdrs, 'city')->textInput() ?>
                                <?= $form->field($model->preAdrs, 'state')->textInput() ?>
                                <?= $form->field($model->preAdrs, 'country')->textInput() ?>
                                <?= $form->field($model->preAdrs, 'postal_code')->textInput() ?>

                            </div>
                        </div>

                        <div class=" panel-default col-md-6">
                            <div class="panel-heading">Permanent Address</div>
                            <div class="panel-body">
                                <?= $form->field($model->perAdrs, 'adrs_line1')->textarea() ?>
                                <?= $form->field($model->perAdrs, 'adrs_line2')->textarea() ?>
                                <?= $form->field($model->perAdrs, 'city')->textInput() ?>
                                <?= $form->field($model->perAdrs, 'state')->textInput() ?>
                                <?= $form->field($model->perAdrs, 'country')->textInput() ?>
                                <?= $form->field($model->perAdrs, 'postal_code')->textInput() ?>

                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">3. Education</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table id="edutab" class="table">
                            <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Qualification</th>
                                <th>College/University</th>
                                <th>Year of Passing</th>
                                <th>Percentage</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1 </td>
                                    <td>
                                        <?= $form->field($model->edu, 'qualification',['template'=>'{input}{error}'])->textInput()

                                        ?>
                                    </td>
                                    <td>
                                        <?= $form->field($model->edu, 'college_university',['template'=>'{input}{error}'])->textInput() ?>

                                    </td>
                                    <td>
                                        <?= $form->field($model->edu, 'yop',['template'=>'{input}{error}'])->textInput(['type'=>'number']) ?>

                                    </td>
                                    <td>
                                        <?= $form->field($model->edu, 'percentage',['template'=>'{input}{error}'])->textInput(['type'=>'number']) ?>

                                    </td>

                                </tr>
                            </tbody>

                        </table>

                        <?php
                        $script = <<< JS

var edutab;
$(document).ready(function(){

 edutab=$('#edutab').medgridc('nrw');

});


JS;
                        $this->registerJs($script,\yii\web\View::POS_END);
                        ?>
                    </div>
                </div>
            </div>

            <!--begin:computer skills-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">4. Computer Skills</a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table id="csktab" class="table">
                            <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Skill</th>
                                <th>Expertise</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <?= $form->field($model->csk, 'skill',['template'=>'{input}{error}'])->textInput()

                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($model->csk, 'expertise',['template'=>'{input}{error}'])->textInput() ?>

                                </td>


                            </tr>
                            </tbody>

                        </table>

                        <?php
                        $script = <<< JS

var csktab;
$(document).ready(function(){

 csktab=$('#csktab').medgridc();

});


JS;
                        $this->registerJs($script,\yii\web\View::POS_END);
                        ?>
                    </div>
                </div>
            </div>
            <!--end:computer skills-->

            <!--begin: Experience-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">5. Experience</a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table id="exptab" class="table">
                            <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Company</th>
                                <th>Designation</th>
                                <th>From Date</th>
                                <th>To Date</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <?= $form->field($model->exp, 'company',['template'=>'{input}{error}'])->textInput()

                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($model->exp, 'designation',['template'=>'{input}{error}'])->textInput() ?>

                                </td>
                                <td>
                                    <?= $form->field($model->exp, 'from',['template'=>'{input}{error}'])->textInput(["class"=>"form-control dateui"]) ?>

                                </td>
                                <td>
                                    <?= $form->field($model->exp, 'to',['template'=>'{input}{error}'])->textInput(["class"=>"form-control dateui"]) ?>

                                </td>




                            </tr>
                            </tbody>

                        </table>

                        <?php
                        $script = <<< JS

var exptab;
$(document).ready(function(){

 exptab=$('#exptab').medgridc('',exptabPlugAct);
 function exptabPlugAct(){
    $(".dateui").each(function(){
            if(!$(this).data('krajeeKvdatepicker')){
                $(this).kvDatepicker({'format':'dd-mm-yyyy'});
            }
    });

    console.log($("#executive-joining_date").data());
 }

});


JS;
                        $this->registerJs($script,\yii\web\View::POS_END);
                        ?>
                    </div>
                </div>
            </div>
            <!--end: Experience-->

            <!--begin: Attachement-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">6. Attachments</a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table id="athtab" class="table">
                            <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Attachment Type</th>
                                <th>Attachment</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <?= $form->field($model->attach, 'attachment_type',['template'=>'{input}{error}'])->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\AttachmentType::find(["is_active"=>0])->all(),'id','attachment'))

                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($model->attach, 'file_path',['template'=>'{input}{error}'])->fileInput(["class"=>"form-control fileattach"]) ?>

                                </td>


                            </tr>
                            </tbody>

                        </table>

                        <?php
                        $script = <<< JS

var athtab;
$(document).ready(function(){

 athtab=$('#athtab').medgridc('');
 function athtabPlugAct(){
    $(".fileattach").each(function(){
        //alert($(this).data('fileinput'));

            if(typeof $(this).data('fileinput') == 'undefined'){
                var cl=$(this).clone();
                var td=$(this).closest('td');


                $(this).closest('.file-input').remove();
                $(td).append(cl);
                $(cl).fileinput();
            }
    });

    //console.log($("#executive-address_proof_path").data());
 }

});


JS;
                        $this->registerJs($script,\yii\web\View::POS_END);
                        ?>
                    </div>
                </div>
            </div>
            <!--end: Attachement-->

        </div>
    </div>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
