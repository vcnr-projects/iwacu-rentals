<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = 'Create Executive [Personal Information Section]';
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs ">
        <li class="active" ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_general","id"=>$model->id]) ?>">Personal Information</a></li>
       <!--
       <li><a  href="<?/*= Yii::$app->urlManager->createUrl(["/executive/createwiz_qualification","id"=>$model->id]) */?>">Qualification</a></li>
        <li><a  href="<?/*= Yii::$app->urlManager->createUrl(["/executive/createwiz_experience","id"=>$model->id]) */?>">Experience cum Vehicle</a></li>
        <li><a  href="<?/*= Yii::$app->urlManager->createUrl(["/executive/createwiz_vehicle","id"=>$model->id]) */?>">Experience cum Vehicle</a></li>
        <li><a  href="<?/*= Yii::$app->urlManager->createUrl(["/executive/createwiz_reference","id"=>$model->id]) */?>">Experience cum Vehicle</a></li>
        <li><a  href="<?/*= Yii::$app->urlManager->createUrl(["/executive/createwiz_office","id"=>$model->id]) */?>">Office</a></li>

-->


    </ul>

    <?= $this->render('_formGeneral', [
        'model' => $model,
    ]) ?>

</div>
