<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveReference */
/* @var $form ActiveForm */




/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = 'Create Executive [Insurance Section]';
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="form">
    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs ">
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_general","id"=>$id]) ?>">Personal Information</a></li>
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_qualification","id"=>$id]) ?>">Qualification</a></li>
        <li><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_experience","id"=>$id]) ?>">Experience </a></li>
        <li><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_vehicle","id"=>$id]) ?>"> Vehicle</a></li>
        <li ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_reference","id"=>$id]) ?>">Reference</a></li>
        <li  ><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_office","id"=>$id]) ?>">Office</a></li>
        <li  class="active"><a  href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_insurance","id"=>$id]) ?>">Medical Insurance</a></li>
    </ul>


    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->errorSummary($model) ?>
    <?= $form->field($model, 'company') ?>

    <?= $form->field($model, 'policy_no') ?>
    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(),[
        'options' => ['placeholder' => 'Enter  Date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=> true,
            'endDate'=>'d'
        ],
    ]) ?>

    <?= $form->field($model, 'expiry_dt')->widget(\kartik\date\DatePicker::className(),[
        'options' => ['placeholder' => 'Enter Expiry Date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=> true,
            'endDate'=>'d'
        ],
    ]) ?>
    <?= $form->field($model, 'ins_amt') ?>
    <?= $form->field($model, 'amt') ?>

    <div class="form-group">
    <?
    $baseurl=Yii::$app->urlManager->baseUrl;
    if($model->certificate){
        echo "
            Uploaded File:
        <a href='{$baseurl}'>{$model->certificate}</a>
        ";
    }
    ?>

    <?php
    echo \kartik\widgets\FileInput::widget([
        'name' => 'ExecutiveInsurance[certificate]',
        //'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showUpload' => false,

        ]
    ]);
    ?>
    </div>

    <? //= $form->field($model, 'certificate')->fileInput() ?>

    <?//= $form->field($model, 'exec_id') ?>

    <? //= $form->field($model, 'duration') ?>
    <? //= $form->field($model, 'type')->dropDownList(["Premium"=>"Premium","Standard"=>"Standard"]) ?>


    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn  btn-block btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->