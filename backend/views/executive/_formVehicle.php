<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 14/8/15
 * Time: 5:18 PM
 */
$this->registerJsFile(\Yii::$app->request->BaseUrl.'/js/medgridc.js', ['depends' => [yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_END]);
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype'=>'multipart/form-data']]); ?>
<?php echo $form->errorSummary($model); ?>



    <!--begin:vehicle-->
    <div class="panel panel-default">
        <!--<div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> Vehicles </a>
            </h4>
        </div>-->
        <div id="collapseFour" class="panel-collapse collapse1">
            <div class="panel-body">
                <table id="vechtab" class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Vehicle No.</th>
                        <th>Vehicle Type</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Insurance Company</th>
                        <th>Insurance Expiry Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $model->vech=$model->getExecutiveVehicles()->all();
                    if(!count($model->vech)){
                        $model->vech=[];
                        $model->vech[]=new \app\models\ExecutiveVehicle();
                    }
                    foreach($model->vech as $i=> $exp):
                        ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td>
                                <?= $form->field($exp, 'vehicle_no',['template'=>'{input}{error}'])->textInput([
                                    'name'=>'ExecutiveVehicle[vehicle_no][]'
                                    ,'onkeyup'=>"this.value=this.value.toUpperCase()"])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($exp, 'vehicle_type',['template'=>'{input}{error}'])->dropDownList(['Two wheeler'=>'Two wheeler','Four wheeler'=>'Four wheeler'],['name'=>'ExecutiveVehicle[vehicle_type][]'])?>
                            </td>
                            <td>
                                <?= $form->field($exp, 'make',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveVehicle[make][]']) ?>
                            </td>
                            <td>
                                <?= $form->field($exp, 'model',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveVehicle[model][]']) ?>
                            </td>
                            <td>
                                <?= $form->field($exp, 'insurance',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveVehicle[insurance][]'])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($exp, 'insurance_expiry_dt',['template'=>'{input}{error}'])->textInput(['name'=>'ExecutiveVehicle[insurance_expiry_dt][]','class'=>'dateui form-control'])
                                ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                $script = <<< JS
var vechtab;
$(document).ready(function(){
 vechtab=$('#vechtab').medgridc('',pluginactivate);
});

function pluginactivate(tr){
    $(".dateui").kvDatepicker({"autoclose":true,"format":"dd-mm-yyyy","startDate": '+1d'});
}

JS;
                $this->registerJs($script,\yii\web\View::POS_END);
                ?>
            </div>
        </div>
    </div>
    <!--end:experience-->

    <div>
        <?= $form->field($model, 'license_no')->textInput() ?>
        <?= $form->field($model, 'licence_issue_place')->textInput() ?>
        <?= $form->field($model, 'license_expiry')->widget(\kartik\date\DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter License Expiry Date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy',
            ]
        ]); ?>

        <div class="form-group field-executiveattachments-id">
            <label class="control-label" for="executiveattachments-id">Driving License Upload</label>
            <?php

            $model->attach=$model->getExecutiveAttachments()
                ->andWhere(["attachment_type"=>"ID Proof"])
                ->all();

            foreach($model->attach as $ma){
                if($ma->attachment_type=="ID Proof"){
                    $ipa=$ma;
                }
            }


            if(isset($ipa)){
                $url=Yii::$app->urlManager->baseUrl.$ipa->file_path;
                echo "
                    <div>
                        Uploaded File : <a href='{ $url }'>{$ipa->file_path} </a>
                    </div>
                    ";
            }else{
                $ipa=new \app\models\ExecutiveAttachments();
            }

            ?>
            <?php
            echo \kartik\file\FileInput::widget([
                'name' => 'idproofdoc',
                'pluginOptions' => [
                    'showUpload' => false,

                ]
            ]);
            ?>
            <div class="help-block"></div>
        </div>



    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>


<?php ActiveForm::end(); ?>