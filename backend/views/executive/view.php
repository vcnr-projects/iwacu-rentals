<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Executive */

$this->title = $model->fname." ".$model->lname ;
$this->params['breadcrumbs'][] = ['label' => 'Executives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    h3{
        font-weight: bold;
    }
    .nav-tabs{
        margin-bottom: 20px;
    }
</style>
<div class="executive-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?/*= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) */?><!--
        --><?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>



</div>

<ul class="nav nav-tabs ">
    <?php
    $viewtab=(isset($_REQUEST["viewtab"]))?$_REQUEST["viewtab"]:"gen";
    $actclstxt='class="active"';
    ?>
    <li <?= ($viewtab=="gen")?$actclstxt:"" ?> ><a data-toggle="tab" href="#gen">Personal Information</a></li>
    <li <?= ($viewtab=="qual")?$actclstxt:"" ?> ><a data-toggle="tab" href="#qual">Qualification</a></li>
    <li <?= ($viewtab=="ecv")?$actclstxt:"" ?> ><a data-toggle="tab" href="#ecv">Experience </a></li>
    <li <?= ($viewtab=="vech")?$actclstxt:"" ?> ><a data-toggle="tab" href="#vech">Vehicle Details </a></li>
    <li <?= ($viewtab=="ref")?$actclstxt:"" ?> ><a data-toggle="tab" href="#ref">Reference</a></li>
    <li <?= ($viewtab=="off")?$actclstxt:"" ?> ><a data-toggle="tab" href="#off">Office</a></li>
    <li <?= ($viewtab=="ins")?$actclstxt:"" ?> ><a data-toggle="tab" href="#ins">Medical Insurance</a></li>

</ul>

<div class="tab-content">
    <div id="gen" class="tab-pane fade in  <?= ($viewtab=="gen")?"active":"" ?> ">
        <!--<h3>Personal Information </h3>-->
        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_general","id"=>$model->id]) ?>" >Edit</a>
        <div class="row">
       <div class=" col-md-6">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute'=>'title0.name',
                    'label'=>'Title'
                ],
                'fname',
                'lname',
                'user.username',
                'fathername',
                'mname',
                'dob',
                'email:email',
                'last_active',
                'license_no',
                'license_expiry',
                [
                    'attribute'=>'fproadrs',
                    'format'=>'Ntext',
                    'label'=>"Address Proof",
                ],
                [
                    'attribute'=>'fpreadrs',
                    'format'=>'Ntext',
                    'label'=>"Present Address",
                ],
                /*[
                    'attribute'=>'fperadrs',
                    'format'=>'Ntext',
                    'label'=>"Permanent Address",
                ],*/
                'created_on',
                [
                    //'attribute'=>'adrsProofpath',
                    'label'=>" Address Proof Document",
                    'format'=>'html',
                    'value'=>($model->adrsProofpath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->adrsProofpath,["target"=>"_blank"]):"",
                ],
                [
                    //'attribute'=>'adrsProofpath',
                    'label'=>" Present Address Proof Document",
                    'format'=>'html',
                    'value'=>($model->preadrspath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->preadrspath,["target"=>"_blank"]):"",
                ],
                [
                    //'attribute'=>'adrsProofpath',
                    'label'=>" Resume Document",
                    'format'=>'html',
                    'value'=>($model->resumepath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->resumepath,["target"=>"_blank"]):"",
                ],


            ],
        ]) ?>
       </div>
        <div  class="col-md-6">
            <img src="<?= Yii::$app->urlManager->baseUrl."/uploads/".$model->photopath ?>" alt="Photo" />
        </div>
        </div>
        <?php
        $model->lang=$model->getExecutiveLanguages
        ()->all();
        if(!empty($model->lang)){
            ?>
            <div>
                <h3>Known Languages</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Language</th>
                        <th>Expertise</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php  foreach($model->lang as $i=> $edu):
                        ?>
                        <tr>
                            <td><?= $i+1 ?> </td>
                            <td>
                                <?=
                                $edu->language;

                                ?>
                            </td>
                            <td>
                                <?=
                                $edu->proficiency;

                                ?>

                            </td>

                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        <?
        }
        ?>
    </div>
    <div id="off" class="tab-pane fade  in <?= ($viewtab=="off")?"active":"" ?> ">
        <div class="panel ">
            <div class="panel-heading">
                <!--<h3>Office Section</h3>-->
                <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_office","id"=>$model->id]) ?>" >Edit</a>

            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'department.name',
                        'branch',
                        'officeBranch.city',
                        'designation',
                        'joining_date',
                        'salary',
                        'appraisal_perc',
                        [
                          'attribute'=>'afi',
                            'label'=>"Applicable for Incentives"
                        ],
                        [
                            'attribute'=>'affw',
                            'label'=>"Applicable for food allowance"
                        ],
                        'mobile_bill_limit',
                        [
                            'attribute'=>'stat',
                            'label'=>"Status"
                        ],
                    ],
                ]) ?>
            </div>
        </div>


    </div>
    <div id="qual" class="tab-pane fade in  <?= ($viewtab=="qual")?"active":"" ?> ">

        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_qualification","id"=>$model->id]) ?>" >Edit</a>

        <div class="panel-body">

            <?php
             $model->edu=$model->getExecutiveEducations()->all();
            if(!empty($model->edu)){
                ?>
                <div>
                    <h3>Education</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Qualification</th>
                            <th>College/University</th>
                            <th>Year of Passing</th>
                            <th>Percentage</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($model->edu as $i=> $edu):
                        ?>
                        <tr>
                            <td><?= $i+1 ?> </td>
                            <td>
                                <?=
                                $edu->qualification;

                                ?>
                            </td>
                            <td>
                                <?=
                                $edu->college_university;

                                ?>

                            </td>
                            <td>
                                <?=
                                $edu->yop;

                                ?>

                            </td>
                            <td>
                                <?=
                                $edu->percentage;

                                ?>%

                            </td>
                            <td>
                                <?php
                                   $ea=$model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$edu->qualification])->one();
                                    if($ea){
                                        echo "
                                        <a href='uploads/".$ea->file_path."'
                                           target=_blank > Education Certificate </a>
                                        ";
                                        //echo Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$ea->adrsProofpath,["target"=>"_blank"]);
                                    }
                                ?>
                            </td>

                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?
            }
            ?>


            <?php
/*            $model->lang=$model->getExecutiveLanguages
            ()->all();
            if(!empty($model->lang)){
                */?><!--
                <div>
                    <h3>Known Languages</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Language</th>
                            <th>Expertise</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /* foreach($model->lang as $i=> $edu):
                            */?>
                            <tr>
                                <td><?/*= $i+1 */?> </td>
                                <td>
                                    <?/*=
                                    $edu->language;

                                    */?>
                                </td>
                                <td>
                                    <?/*=
                                    $edu->proficiency;

                                    */?>

                                </td>

                            </tr>
                        <?php /*endforeach;*/?>
                        </tbody>
                    </table>
                </div>
            --><?/*
            }
            */?>

            <?php
            $model->csk=$model->getExecutiveComputerSkills()->all();
            if(!empty($model->csk)){
                ?>
                <div>
                    <h3> Skills</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Skill</th>
                            <th>Expertise</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($model->csk as $i=> $edu):
                            ?>
                            <tr>
                                <td><?= $i+1 ?> </td>
                                <td>
                                    <?=
                                    $edu->skill;

                                    ?>
                                </td>
                                <td>
                                    <?=
                                    $edu->expertise;

                                    ?>

                                </td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?
            }
            ?>


            <?php
            $model->cert=$model->getExecutiveCertifications()->all();
            if(!empty($model->cert)){
                ?>
                <div>
                    <h3>Other Certificates</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Certificate</th>
                            <th>Issued Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($model->cert as $i=> $edu):
                            ?>
                            <tr>
                                <td><?= $i+1 ?> </td>
                                <td>
                                    <?=
                                    $edu->certificate;

                                    ?>
                                </td>
                                <td>
                                    <?=
                                    $edu->issued_date;

                                    ?>

                                </td>
                                <td>
                                <?php
                                $ea=$model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$edu->certificate])->one();
                                if($ea){
                                    echo "
                                        <a href='uploads/".$ea->file_path."'
                                           target=_blank >Other Certificate </a>
                                        ";
                                    //echo Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$ea->adrsProofpath,["target"=>"_blank"]);
                                }
                                ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?
            }
            ?>

        </div>
    </div>

    <!--begin ecv-->
    <div id="ecv" class="tab-pane fade  in <?= ($viewtab=="ecv")?"active":"" ?>">
        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_experience","id"=>$model->id]) ?>" >Edit</a>

        <div class="panel-body">

            <?php
            $model->exp=$model->getExecutiveExperiences()->all();
            if(!empty($model->exp)){
                ?>
                <div>
                    <h3>Experience</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Company</th>
                            <th>Designation</th>
                            <th>From Date</th>
                            <th>To Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($model->exp as $i=> $edu):
                            ?>
                            <tr>
                                <td><?= $i+1 ?> </td>
                                <td>
                                    <?=
                                    $edu->company;

                                    ?>
                                </td>
                                <td>
                                    <?=
                                    $edu->designation;

                                    ?>

                                </td>
                                <td>
                                    <?=
                                    $edu->from;

                                    ?>

                                </td>
                                <td>
                                    <?=
                                    $edu->to;

                                    ?>

                                </td>
                                <td>
                                    <?php
                                    $ea=$model->getExecutiveAttachments()->filterWhere(["attachment_type"=>$edu->designation.$edu->company])->one();
                                    if($ea){
                                        echo "
                                        <a href='uploads/".$ea->file_path."'
                                           target=_blank >Document </a>
                                        ";
                                        //echo Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$ea->adrsProofpath,["target"=>"_blank"]);
                                    }
                                    ?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?
            }else{
                echo "<b>Fresher</b>";
            }
            ?>



        </div>
    </div>
    <!--end ecv-->


    <!--begin: vehicle-->
    <div id="vech" class="tab-pane fade  in  <?= ($viewtab=="vech")?"active":"" ?>">
        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_vehicle","id"=>$model->id]) ?>" >Edit</a>
        <div class="panel-body">



            <?php
            $model->vech=$model->getExecutiveVehicles()->all();
            if(!empty($model->vech)){
                ?>
                <div>
                    <h3>Vehicle</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>SL No.</th>
                            <th>Vehicle No.</th>
                            <th>Vehicle Type</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>Insurance Company</th>
                            <th>Insurance Expiry Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($model->vech as $i=> $edu):
                            ?>
                            <tr>
                                <td><?= $i+1 ?> </td>
                                <td>
                                    <?=
                                    $edu->vehicle_no;

                                    ?>
                                </td>
                                <td>
                                    <?=
                                    $edu->vehicle_type;
                                    ?>

                                </td>
                                <td><?= $edu->make ?></td>
                                <td><?= $edu->model ?></td>
                                <td><?= $edu->insurance ?></td>
                                <td><?= $edu->insurance_expiry_dt ?></td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?
            }
            ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'license_no',
                    'license_expiry',
                    'licence_issue_place',
                    [
                        'label'=>" License Document",
                        'format'=>'html',
                        'value'=>($model->adrsProofpath)?Html::a("Document",Yii::$app->urlManager->baseUrl."/uploads/".$model->adrsProofpath,["target"=>"_blank"]):"",
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <!--end: vehicle-->


       <!-- begin:reference-->
    <div id="ref" class="tab-pane fade  in   <?= ($viewtab=="ref")?"active":"" ?> ">
        <?php $model->reference=\app\models\ExecutiveReference::findAll(['exec_id'=>$model->id]) ?>
        <!--<h3>Reference Section</h3>-->
        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_reference","id"=>$model->id]) ?>" >Edit</a>
        <div class="row">
            <div class=" col-md-9">
                <?php foreach($model->reference as $reference):?>
                <?= DetailView::widget([
                    'model' => $reference,
                    'attributes' => [
                        'name',
                        'company',
                        'designation',
                        'phno',
                        'remarks:Ntext',
                        'adrs:Ntext',
                        [
                            'label'=>"Reference Letter",
                            'format'=>'html',
                            'value'=>($reference->reference_letter)?Html::a("$reference->reference_letter",Yii::$app->urlManager->baseUrl."/uploads/".$reference->reference_letter,["target"=>"_blank"]):"",
                        ],


                    ],
                ]);
                endforeach;
                ?>
            </div>
            <div  class="col-md-1">
<!--                <img src="<?/*= Yii::$app->urlManager->baseUrl."/uploads/".$model->photopath */?>" alt="Photo" />
-->            </div>
        </div>
    </div>
       <!-- end:reference-->


    <!-- begin:insurance-->
    <div id="ins" class="tab-pane fade  in    <?= ($viewtab=="ins")?"active":"" ?> ">
        <?php $model->insurance=\app\models\ExecutiveInsurance::findOne(['exec_id'=>$model->id]) ?>
        <!--<h3>Insurance Section</h3>-->
        <a href="<?= Yii::$app->urlManager->createUrl(["/executive/createwiz_insurance","id"=>$model->id]) ?>" >Edit</a>
        <div class="row">
            <div class=" col-md-9">
                <?php if($model->insurance):?>
                    <?= DetailView::widget([
                        'model' => $model->insurance,
                        'attributes' => [
                            'company',
                            'policy_no',
                            'date',
                            'expiry_dt',
                            'ins_amt',
                            'amt',
                            [
                                'label'=>"Insurance Certificate",
                                'format'=>'html',
                                'value'=>($model->insurance->certificate)?Html::a("{$model->insurance->certificate}",Yii::$app->urlManager->baseUrl."/uploads/".$model->insurance->certificate,["target"=>"_blank"]):"",
                            ],
                            //'duration',
                            //'type',

                        ],
                    ]);
                endif;
                ?>
            </div>
            <div  class="col-md-1">
                <!--                <img src="<?/*= Yii::$app->urlManager->baseUrl."/uploads/".$model->photopath */?>" alt="Photo" />
-->            </div>
        </div>
    </div>
    <!-- end:insurance-->





</div>
