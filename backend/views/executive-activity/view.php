<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveActivity */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Executive Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'activity',
            'exec_id',
            'created_on',
            'start_date',
            'end_date',
            'status',
            'prop_id',
            'remarks:ntext',
        ],
    ]) ?>

</div>
