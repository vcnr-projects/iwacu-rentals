<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveActivity */

$this->title = 'Create Executive Activity';
$this->params['breadcrumbs'][] = ['label' => 'Executive Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="executive-activity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
