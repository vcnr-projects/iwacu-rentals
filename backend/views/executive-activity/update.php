<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveActivity */

$this->title = 'Update Executive Activity: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Executive Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="executive-activity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
