<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExecutiveActivity */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="executive-activity-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'exec_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\models\Executive::find()->where(["status"=>1])->asArray()->all(),'id'
        ,function($model, $defaultValue) {
            //Yii::trace(\yii\helpers\VarDumper::dumpAsString($model),'vardump');
            $user=\common\models\User::findOne($model["user_id"]);
            return $user->username;
        }),["prompt"=>"Select"]
    )?>

    <?= $form->field($model, 'activity')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\ExecutiveActivityType::find()->where(["is_active"=>1])->asArray()->all(),'type','type'),['prompt'=>"Select"]) ?>



    <? //= $form->field($model, 'created_on')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Start Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true
        ]
    ]); ?>

    <?= $form->field($model, 'end_date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'End Date'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight'=>true
        ]
    ]); ?>

    <?= $form->field($model, 'status')->radioList(["Completed"=>"Completed","Pending"=>"Pending"]) ?>

    <?= $form->field($model, 'prop_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\modules\realty\models\PropertyUnits::find()->asArray()->all()
        ,'id'
        ,function($model){
            $prop=\app\modules\service\models\Property::findOne($model["property_id"]);
            return $prop->name." {$model["floor_number"]} - {$model["block"]} - {$model["flat_no"]} ";
        }
    ),["prompt"=>"Select"]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
