<?php
namespace app\components;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\VarDumper;

class HiddenDataColumn extends DataColumn{
    //put your code here

    public $hiddendata;
    public function renderFilterCellContent() {

       // var_dump($this->grid->filterModel);
        $retdata="";
        $reflect = new \ReflectionClass($this->grid->filterModel);


        if(isset($this->filter)&&$this->filter)
          $retdata.= Html::activeTextInput($this->grid->filterModel, $this->attribute,['class'=>'form-control']);
        if(isset($this->hiddendata)){
            foreach($this->hiddendata as $dat){
                \Yii::trace(VarDumper::dumpAsString($this->grid->filterModel),'vardump');

                $retdata.=  Html::activeHiddenInput($this->grid->filterModel,$dat, array('id'=>$reflect->getShortName() ."-".$dat));
            }
        }
        return $retdata;
    }
}