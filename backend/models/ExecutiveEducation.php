<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_education".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $qualification
 * @property string $college_university
 * @property integer $yop
 * @property double $percentage
 *
 * @property Executive $exec
 */
class ExecutiveEducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'qualification', 'college_university', 'yop'], 'required'],
            [['exec_id', 'yop'], 'integer'],
            [['percentage'], 'number'],
            [['qualification'], 'string', 'max' => 100],
            [['college_university'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'qualification' => 'qualification',
            'college_university' => 'College / University',
            'yop' => 'Year of Passing',
            'percentage' => 'Percentage',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }
}
