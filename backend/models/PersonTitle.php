<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_title".
 *
 * @property string $id
 * @property string $name
 * @property integer $is_active
 *
 * @property Executive[] $executives
 */
class PersonTitle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person_title';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_active' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutives()
    {
        return $this->hasMany(Executive::className(), ['title' => 'name']);
    }
}
