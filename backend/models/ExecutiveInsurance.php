<?php

namespace app\models;

use Yii;
use app\components\LessThanDateValidator;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "executive_insurance".
 *
 * @property string $id
 * @property string $policy_no
 * @property string $amt
 * @property string $expiry_dt
 * @property string $type
 * @property integer $exec_id
 * @property integer $duration
 *
 * @property Executive $exec
 */
class ExecutiveInsurance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_insurance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'amt', 'exec_id','company'], 'required'],
            [[ 'exec_id', 'duration'], 'integer'],
            [['amt','ins_amt'], 'number'],
            [['expiry_dt','date'], 'safe'],
            [['policy_no','company','certificate'], 'string', 'max' => 200],
            [['type'], 'string', 'max' => 100],
            [['date'], 'datelessthan', 'params' =>'expiry_dt'],
            [['expiry_dt'], 'datelessthan', 'params' =>'current date check'],
        ];
    }

    public function datelessthan($attribute,$params) {

        if($params="current date check") {
            if (strtotime($this->{$attribute}) && strtotime(date('Y-m-d')) && strtotime($this->{$attribute}) > strtotime(date('Y-m-d'))) {
                $this->addError($attribute, 'Expiry Date greater than Today\'s Date');
            }
            return;
        }
        Yii::trace(VarDumper::dumpAsString($params),'vardump');
        if(strtotime($this->{$attribute})&&strtotime($this->{$params})&&strtotime($this->{$attribute})>strtotime($this->{$params})){
            $this->addError($attribute,' Date greater than Expiry Date');
        }
        /*if (!Yii::app()->user->isGuest and Elist::model()->exists('address=:address and owner_id=:owner_id',array(':address'=>$this->$attribute,':owner_id'=>Yii::app()->user->id)))
            $this->addError($attribute, 'Sorry, you\'re already using that name for another list.');
    */
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'policy_no' => 'Policy No.',
            'amt' => 'Premium Amount [Yearly]',
            'ins_amt' => 'Total Insured Amount',
            'expiry_dt' => 'Expiry Date',
            'date' => 'From Date',
            'type' => 'Insurance Type',
            'exec_id' => 'Executive ID',
            'duration' => 'Duration in Months',
            'company' => 'Insurance Company Name',
            'certificate' => 'Medical Certificate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(strtotime($this->date)!==strtotime('01-01-1970')){
                $this->date=date('Y-m-d',strtotime($this->date));
            }else{
                $this->date=null;
            }

            if(strtotime($this->expiry_dt)!==strtotime('01-01-1970')){
                $this->expiry_dt=date('Y-m-d',strtotime($this->expiry_dt));
            }else{
                $this->expiry_dt=null;
            }


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {
        if(strtotime($this->date)==strtotime('01-01-1970')){
            $this->date=null;

        }else{
            $this->date=date('d-m-Y',strtotime($this->date));
        }

        if(strtotime($this->expiry_dt)==strtotime('01-01-1970')){
            $this->expiry_dt=null;

        }else{
            $this->expiry_dt=date('d-m-Y ',strtotime($this->expiry_dt));
        }

        parent::afterFind ();
    }


}
