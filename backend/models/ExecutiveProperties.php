<?php

namespace app\models;

use app\modules\realty\models\PropertyUnits;
use app\modules\service\models\Service;
use app\modules\service\models\ServiceType;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "executive_properties".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $property
 * @property string $date
 * @property string $expiry_date
 * @property integer $is_active
 *
 * @property Executive $exec
 * @property PropertyUnits $property0
 */
class ExecutiveProperties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'property'], 'required'],
            [['exec_id', 'property', 'is_active', 'service_id'], 'integer'],
            [['date', 'expiry_date'], 'safe'],
            [['date'], 'datelessthan', 'params' =>'expiry_date']
        ];
    }

    public function datelessthan($attribute,$params) {

        //Yii::trace(VarDumper::dumpAsString($params),'vardump');
        if(strtotime($this->{$attribute})&&strtotime($this->{$params})&&strtotime($this->{$attribute})>strtotime($this->{$params})){
            $this->addError($attribute,' Date greater than Expiry Date');
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'property' => 'Property',
            'date' => 'Activation Date',
            'expiry_date' => 'Expiry Date',
            'is_active' => 'Is Active?',
            'service_id' => 'Owner Contract ',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty0()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property']);
    }


    public function getExecId()
    {
        return $this->hasOne(User::className(), ['id' => 'exec_id'])->one()->username;
    }

    public function getPropertyName()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'property'])->one()->name;
    }

    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if(strtotime($this->date)!==false){
                $this->date=date('Y-m-d',strtotime($this->date));

            }else{
                $this->date=date('Y-m-d');
            }

            if(strtotime($this->expiry_date)!==false){
                $this->expiry_date=date('Y-m-d',strtotime($this->expiry_date));
            }else{
                $this->expiry_date=null;
            }

            $service=Service::findByCondition(["property_id"=>$this->property,"is_active"=>1])->one();
            Yii::trace(VarDumper::dumpAsString($service),'vardump');
            if($service){
                $this->service_id=$service->id;
            }else{
                $this->addError('property',"Contract for this property is not signed");
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterFind ()
    {

        if(strtotime($this->date)==false){
            $this->date=null;

        }else{
            $this->date=date('d-m-Y',strtotime($this->date));
        }

        if(strtotime($this->expiry_date)==false){
            $this->expiry_date=null;

        }else{
            $this->expiry_date=date('d-m-Y',strtotime($this->expiry_date));
        }

        parent::afterFind ();
    }




}
