<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telephone".
 *
 * @property string $id
 * @property string $primary_landline
 * @property string $secondary_landline
 * @property string $primary_mobile
 * @property string $secondary_mobile
 *
 * @property Address[] $addresses
 */
class Telephone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telephone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['primary_landline', 'secondary_landline', 'primary_mobile', 'secondary_mobile'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'primary_landline' => 'Landline No.',
            'secondary_landline' => 'Alternate Landline No.',
            'primary_mobile' => 'Mobile No.',
            'secondary_mobile' => 'Alternate Mobile No.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['telephone_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(!$this->primary_mobile
                &&!$this->primary_landline
                &&!$this->secondary_landline
                &&!$this->secondary_mobile
            ){
                $this->addError('primary_mobile',"Mobile No. or any one Contact NO.  is required");
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

}
