<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_activity".
 *
 * @property string $id
 * @property string $activity
 * @property integer $exec_id
 * @property string $created_on
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property string $prop_id
 * @property string $remarks
 *
 * @property Executive $exec
 * @property PropertyUnits $prop
 * @property ExecutiveActivityType $activity0
 */
class ExecutiveActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activity', 'exec_id', 'created_on', 'start_date', 'status', 'prop_id'], 'required'],
            [['exec_id', 'prop_id'], 'integer'],
            [['created_on', 'start_date', 'end_date'], 'safe'],
            [['remarks'], 'string'],
            [['activity'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'iD',
            'activity' => 'Activity',
            'exec_id' => 'Executive ID',
            'created_on' => 'Created On',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'prop_id' => 'Property ID',
            'remarks' => 'Remarks',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProp()
    {
        return $this->hasOne(PropertyUnits::className(), ['id' => 'prop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity0()
    {
        return $this->hasOne(ExecutiveActivityType::className(), ['type' => 'activity']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(strtotime($this->created_on)!==strtotime('01-01-1970')){
                $this->created_on=date('Y-m-d H:i:s',strtotime($this->created_on));
            }else{
                $this->created_on=date('Y-m-d H:i:s');
            }

            if(strtotime($this->start_date)!==strtotime('01-01-1970')){
                $this->start_date=date('Y-m-d',strtotime($this->start_date));
            }else{
                $this->start_date=null;
            }

            if(strtotime($this->end_date)!==strtotime('01-01-1970')){
                $this->end_date=date('Y-m-d',strtotime($this->end_date));
            }else{
                $this->end_date=null;
            }



            //if($this->isNewRecord){
            // $this->created_on=date('Y-m-d H:i:s');
            //}


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {
        if(strtotime($this->created_on)==strtotime('01-01-1970')){
            $this->joining_date=null;

        }else{
            $this->created_on=date('d-m-Y H:i:s',strtotime($this->created_on));
        }

        if(strtotime($this->start_date)==strtotime('01-01-1970')){
            $this->start_date=null;

        }else{
            $this->start_date=date('d-m-Y H:i:s',strtotime($this->start_date));
        }
        if(strtotime($this->end_date)==strtotime('01-01-1970')){
            $this->end_date=null;

        }else{
            $this->end_date=date('d-m-Y',strtotime($this->end_date));
        }




        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }
}
