<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_reference".
 *
 * @property string $id
 * @property string $name
 * @property string $company
 * @property integer $exec_id
 * @property string $designation
 * @property string $phno
 * @property string $reference_letter
 * @property string $remarks
 * @property string $adrs
 *
 * @property Executive $exec
 */
class ExecutiveReference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'company', 'exec_id', 'designation', 'phno'], 'required'],
            [['exec_id'], 'integer'],
            [['remarks', 'adrs'], 'string'],
            [['name', 'company', 'designation'], 'string', 'max' => 200],
            [['phno'], 'string', 'max' => 20],
            [['email'], 'email'],
            [['reference_letter'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Referrer Name',
            'company' => 'Company',
            'exec_id' => 'Executive ID',
            'designation' => 'Designation',
            'phno' => 'Contact No.',
            'reference_letter' => 'Reference Letter',
            'remarks' => 'Remarks',
            'adrs' => 'Address',
            'email' => 'Email ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }
}
