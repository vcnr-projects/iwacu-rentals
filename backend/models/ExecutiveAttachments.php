<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_attachments".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $attachment_type
 * @property string $file_path
 * @property string $created_on
 * @property string $remarks
 *
 * @property Executive $exec
 * @property AttachmentType $attachmentType
 */
class ExecutiveAttachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'attachment_type'], 'required'],
            [['exec_id'], 'integer'],
            [['created_on'], 'safe'],
            [['remarks'], 'string'],
            [['attachment_type'], 'string', 'max' => 100],
           // [['file_path'], 'string', 'max' => 255]
            [['file_path'], 'file']
        ];
    }

    /*
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'attachment_type' => 'Attachment Type',
            'file_path' => 'File',
            'created_on' => 'Created Date',
            'remarks' => 'Remarks',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentType()
    {
        return $this->hasOne(AttachmentType::className(), ['attachment' => 'attachment_type']);
    }
}
