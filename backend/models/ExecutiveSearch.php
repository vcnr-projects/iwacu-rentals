<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Executive;
use yii\helpers\VarDumper;

/**
 * ExecutiveSearch represents the model behind the search form about `app\models\Executive`.
 */
class ExecutiveSearch extends Executive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'address_proof_id', 'present_address_id', 'permanent_address_id', 'department_id', 'applicable_for_incentives', 'applicable_for_food_allowance', 'status'], 'integer'],
            [['fname','lname','email',  'created_on', 'last_active', 'license_no', 'license_expiry', 'designation', 'joining_date','title','gender', 'status','empid'], 'safe'],
            [['salary', 'mobile_bill_limit'], 'number'],
            [['activestatus', 'fullname','contact','empid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Executive::find();
        $query->sql="select e.*,concat(e.fname,' ',e.lname) as fullname,t.primary_mobile as contact,u.username as empid
        ,if(e.status=1,'Active','In-Active') as activestatus
        from executive e left join user u on e.user_id=u.id
        left join address a on a.id=e.present_address_id left join telephone t on a.telephone_id=t.id ";




        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        Yii::trace(VarDumper::dumpAsString($this->empid),'vardump');
        Yii::trace(VarDumper::dumpAsString(!$this->validate()),'vardump');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,

            'address_proof_id' => $this->address_proof_id,
            'created_on' => $this->created_on,
            'last_active' => $this->last_active,
            'present_address_id' => $this->present_address_id,
            'permanent_address_id' => $this->permanent_address_id,
            'license_expiry' => $this->license_expiry,
            'department_id' => $this->department_id,
            'joining_date' => $this->joining_date,
            'salary' => $this->salary,
            'applicable_for_incentives' => $this->applicable_for_incentives,
            'applicable_for_food_allowance' => $this->applicable_for_food_allowance,
            'mobile_bill_limit' => $this->mobile_bill_limit,
            'status' => $this->status,
            'gender' => $this->gender,
            'title' => $this->title,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'license_no', $this->license_no])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'designation', $this->designation]);

        $whereSql='';


        $firstCondition=true;
        if(isset($this->empid)){
            $and='';
            if($firstCondition){
               $firstCondition=false;
            }else{
                $and=" and ";
            }

            $whereSql.= "$and u.username like '%{$this->empid}%' ";
        }
        if(isset($this->fullname)){
            $and='';
            if($firstCondition){
                $firstCondition=false;
            }else{
                $and=" and ";
            }

            $whereSql.= "$and concat(e.fname,' ',e.lname) like '%{$this->fullname}%' ";
        }
        if(isset($this->contact)){
            $and='';
            if($firstCondition){
                $firstCondition=false;
            }else{
                $and=" and ";
            }

            $whereSql.= "$and t.primary_mobile like '%{$this->contact}%' ";
        }
        if(isset($this->activestatus)&&$this->activestatus!=''){
            $and='';
            if($firstCondition){
                $firstCondition=false;
            }else{
                $and=" and ";
            }

            $whereSql.= "$and e.status = '{$this->activestatus}' ";
        }

        if($whereSql!=''){
            $query->sql.=" where ".$whereSql;
        }



      /*  $query->andFilterWhere(['like', 'empid', $this->empid])
            ->andFilterWhere(['like', 'activestatus', $this->activestatus]);*/

        Yii::trace(VarDumper::dumpAsString($this->activestatus),'vardump');
        Yii::trace(VarDumper::dumpAsString($query),'vardump');


        return $dataProvider;
    }
}
