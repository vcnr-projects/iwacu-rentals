<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property string $id
 * @property string $adrs_line1
 * @property string $adrs_line2
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property string $telephone_id
 *
 * @property Telephone $telephone
 * @property Executive[] $executives
 * @property Executive[] $executives0
 * @property Executive[] $executives1
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adrs_line1', 'city', 'state', 'country', 'postal_code'], 'required','on'=>"default"],
            [['adrs_line1', 'adrs_line2'], 'string'],
            [['telephone_id'], 'integer'],
            [['postal_code'],'number'],
            [['city', 'state'], 'string', 'max' => 200],
            [['country','proof_doc_type'], 'string', 'max' => 100]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['notRequired'] = ['adrs_line1', 'city', 'state', 'country', 'postal_code'];//Scenario Values Only Accepted
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adrs_line1' => 'Address',
            'adrs_line2' => 'Address Line 2',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'postal_code' => 'Postal Code',
            'telephone_id' => 'Telephone ',
            'proof_doc_type' => 'ID Proof Document Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelephone()
    {
        return $this->hasOne(Telephone::className(), ['id' => 'telephone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutives()
    {
        return $this->hasMany(Executive::className(), ['address_proof_id' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutives0()
    {
        return $this->hasMany(Executive::className(), ['present_address_id' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutives1()
    {
        return $this->hasMany(Executive::className(), ['permanent_address_id' => 'ID']);
    }
}
