<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_travels".
 *
 * @property string $id
 * @property integer $exec_id
 * @property double $km
 * @property string $date
 * @property double $fuel
 * @property string $created_on
 */
class ExecutiveTravels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_travels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'km', 'date', 'fuel'], 'required'],
            [['exec_id'], 'integer'],
            [['km', 'fuel'], 'number'],
            [['date', 'created_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'km' => 'KM',
            'date' => 'Date of Travelling',
            'fuel' => 'Fuel Consumed',
            'created_on' => 'Created Date',
        ];
    }
}
