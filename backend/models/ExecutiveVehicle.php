<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_vehicle".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $vehicle_no
 * @property string $vehicle_type
 * @property string $make
 * @property string $model
 *
 * @property Executive $exec
 */
class ExecutiveVehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_vehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'vehicle_no', 'vehicle_type'], 'required'],
            [['exec_id'], 'integer'],
            [['vehicle_no','insurance_expiry_dt'], 'string', 'max' => 20],
            [['vehicle_type', 'make', 'model','insurance'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'vehicle_no' => 'Vehicle No.',
            'vehicle_type' => 'Vehicle Type',
            'make' => 'Make',
            'model' => 'Model',
            'insurance_expiry_dt' => 'Insurance Expiry Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(strtotime($this->insurance_expiry_dt)){
                $this->insurance_expiry_dt=date('Y-m-d',strtotime($this->insurance_expiry_dt));
            }else{
                $this->insurance_expiry_dt=null;
            }



            //if($this->isNewRecord){
            // $this->created_on=date('Y-m-d H:i:s');
            //}


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {
        if(strtotime($this->insurance_expiry_dt)==strtotime('01-01-1970')){
            $this->insurance_expiry_dt=null;

        }else{
            $this->insurance_expiry_dt=date('d-m-Y',strtotime($this->insurance_expiry_dt));
        }


        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }
}
