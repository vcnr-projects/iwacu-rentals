<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_language".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $language
 * @property string $proficiency
 *
 * @property Executive $exec
 */
class ExecutiveLanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'language'], 'required'],
            [['exec_id'], 'integer'],
            [['language'], 'string', 'max' => 50],
            [['proficiency'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'language' => 'Language',
            'proficiency' => 'Proficiency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }
}
