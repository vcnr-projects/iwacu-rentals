<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attachment_type".
 *
 * @property integer $id
 * @property string $attachment
 * @property integer $is_active
 *
 * @property ExecutiveAttachments[] $executiveAttachments
 */
class AttachmentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attachment'], 'required'],
            [['is_active'], 'integer'],
            [['attachment'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attachment' => 'Attachment Type',
            'is_active' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveAttachments()
    {
        return $this->hasMany(ExecutiveAttachments::className(), ['attachment_type' => 'id']);
    }
}
