<?php

namespace app\models;

use dektrium\user\helpers\Password;
use Yii;
use dektrium\user\models\User;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "executive".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $mname
 * @property string $lname
 * @property string $fname
 * @property string $email
 * @property string $address_proof_id
 * @property string $created_on
 * @property string $last_active
 * @property string $present_address_id
 * @property string $permanent_address_id
 * @property string $license_no
 * @property string $license_expiry
 * @property integer $department_id
 * @property string $designation
 * @property string $joining_date
 * @property double $salary
 * @property integer $applicable_for_incentives
 * @property integer $applicable_for_food_allowance
 * @property double $mobile_bill_limit
 * @property integer $status
 * @property string $title
 * @property string $gender
 * @property string $fathername
 * @property string $marital_status
 * @property string $appraisal_perc
 *
 * @property PersonTitle $title0
 * @property User $user
 * @property Address $addressProof
 * @property Address $presentAddress
 * @property Address $permanentAddress
 * @property Department $department
 * @property ExecutiveAttachments[] $executiveAttachments
 * @property ExecutiveCertification[] $executiveCertifications
 * @property ExecutiveComputerSkill[] $executiveComputerSkills
 * @property ExecutiveEducation[] $executiveEducations
 * @property ExecutiveLanguage[] $executiveLanguages
 * @property ExecutiveVehicle[] $executiveVehicles
 */
class Executive extends \yii\db\ActiveRecord
{

    public  $preAdrs;
    public  $perAdrs;
    public  $proAdrs;
    public  $proTel;
    public  $preTel;
    public  $edu;
    public  $exp;
    public  $csk;
    public  $lang;
    public  $cert;
    public  $vech;
    public  $attach;
    public  $reference;
    public  $insurance;
    protected $user;
    public $city;

    public $empid;
    public $contact;
    public $fullname;
    public $activestatus;

    public $old_password;
    public $new_password;
    public $confirm_password;

    public function __construct(){
        if(!$this->user_id) {
            $this->preAdrs = new PreAddress();
            $this->perAdrs = new PerAddress();
            $this->proAdrs = new ProAddress();
            $this->proTel = new ProTelephone();
            $this->preTel = new PreTelephone();
            //$this->reference = new ExecutiveReference();
            $this->edu[] = new ExecutiveEducation();
            $this->exp[] = new ExecutiveExperience();
            $this->csk[] = new ExecutiveComputerSkill();
            $this->lang[] = new ExecutiveLanguage();
            $this->cert[] = new ExecutiveCertification();
            $this->vech[] = new ExecutiveVehicle();
            $this->attach[]=new ExecutiveAttachments();
            $this->reference[]=new ExecutiveReference();
        }else{

            /*$this->edu=$this->getExecutiveEducations()->all();
            $this->exp=$this->getExecutiveExperiences()->all();
            $this->csk=$this->getExecutiveComputerSkills()->all();*/
        }
    }

    /** @inheritdoc */
   /* public function init()
    {
        $this->user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'register'
        ]);

    }*/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive';
    }

    public function behaviors()
    {
        return [

            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_on'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['created_on'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'lname', 'fname','title','email', 'gender','marital_status'], 'required'],
            [['user_id', 'address_proof_id', 'present_address_id', 'permanent_address_id', 'department_id', 'applicable_for_incentives', 'applicable_for_food_allowance', 'status'], 'integer'],
            [['created_on', 'last_active', 'license_expiry', 'joining_date','dob'], 'safe'],
            [['salary', 'mobile_bill_limit','appraisal_perc'], 'number'],
            [['mname', 'lname', 'fname', 'license_no'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 255],
            [['designation',  'license_no', 'license_expiry',  'licence_issue_place'], 'string', 'max' => 100],
            [['title','fathername'], 'string', 'max' => 200],
            [['gender','marital_status'], 'string', 'max' => 10],
            [['branch','office_city'], 'string', 'max' => 100],
            [['old_password', 'confirm_password','new_password'], 'string','min'=>6, 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'user_id' => 'User ID',
            'mname' => 'Mother\'s Name',
            'lname' => 'Last Name',
            'fname' => 'First Name',
            'email' => 'Email ID',
            'address_proof_id' => 'Address Proof',
            'created_on' => 'Created on',
            'last_active' => 'Last Active Date',
            'present_address_id' => 'Present Address',
            'permanent_address_id' => 'Permanent Address',
            'license_no' => 'Driving License No.',
            'license_expiry' => 'Driving License Expiry Date',
            'department_id' => 'Department',
            'designation' => 'Designation',
            'joining_date' => 'Joining Date',
            'salary' => 'Salary',
            'applicable_for_incentives' => 'Incentive Applicability',
            'applicable_for_food_allowance' => 'Food Incentive Applicability',
            'mobile_bill_limit' => 'Mobile Bill Limit',
            'status' => 'Status',
            'title' => 'Title',
            'gender' => 'Gender',
            'fathername' => 'Father\'s Name',
            'marital_status' => 'Marital Status',
            'appraisal_perc' => 'Appraisal Percentage',
            'dob' => 'Date of Birth',
            'branch' => 'Branch',
            'City' => 'Office City',
            'licence_issue_place' => 'Licence Issue Place',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle0()
    {
        return $this->hasOne(PersonTitle::className(), ['name' => 'title']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressProof()
    {
        return $this->hasOne(Address::className(), ['ID' => 'address_proof_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentAddress()
    {
        return $this->hasOne(Address::className(), ['ID' => 'present_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermanentAddress()
    {
        return $this->hasOne(Address::className(), ['ID' => 'permanent_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveAttachments()
    {
        return $this->hasMany(ExecutiveAttachments::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveCertifications()
    {
        return $this->hasMany(ExecutiveCertification::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveComputerSkills()
    {
        return $this->hasMany(ExecutiveComputerSkill::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveEducations()
    {
        return $this->hasMany(ExecutiveEducation::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveLanguages()
    {
        return $this->hasMany(ExecutiveLanguage::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveVehicles()
    {
        return $this->hasMany(ExecutiveVehicle::className(), ['exec_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveExperiences()
    {
        return $this->hasMany(ExecutiveExperience::className(), ['exec_id' => 'id']);
    }


    public function getOfficeBranch()
    {
        return $this->hasOne(OfficeBranches::className(), ['branch' => 'branch']);
    }




    /**
     * save General section of executive
     * @return boolean
     */
    public function saveGeneral(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
            if($this->isNewRecord){
               /* $user=new User();
                $user->username=$this->fname." ".$this->lname.date('d');
                $user->email=$this->email;*/
                $this->user = \Yii::createObject([
                    'class'    => \common\models\User::className(),
                    'scenario' => 'register'
                ]);

                $this->user->setAttributes([
                    'email'    => $this->email,
                    'username' => 'VHP',
                    'password' => '1234567890',
                    'user_type' => 'Executive',
                ]);

                if(!$this->user->register()){
                    Yii::trace(VarDumper::dumpAsString($this->user->getErrors()),'vardump');
                    foreach($this->user->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg)."<br/>";
                }
                else
                    $this->user_id=$this->user->id;


                if(!empty($this->proTel->primary_landline)||!empty($this->proTel->primary_mobile)) {
                    if (!$this->proTel->save()) {
                        foreach($this->proTel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->proAdrs->telephone_id=$this->proTel->id;
                    }
                }
                if(!$this->proAdrs->save()){
                    foreach($this->proAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else {
                    $this->address_proof_id = $this->proAdrs->id;
                }


                /*begin: address*/
                if(!empty($this->preTel->primary_landline)||!empty($this->preTel->primary_mobile)) {
                    if (!$this->preTel->save()) {
                        foreach($this->preTel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $this->preAdrs->telephone_id=$this->preTel->id;
                    }
                }
                if(!$this->preAdrs->save()){
                    foreach($this->preAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else
                    $this->present_address_id=$this->preAdrs->id;
                /*end: address*/


               /* if(!$this->perAdrs->save()){
                    foreach($this->perAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
                else
                    $this->permanent_address_id=$this->perAdrs->id;
*/


            }else{
                // update address

                /* begin: address*/
                $proadrs=Address::findOne($this->address_proof_id);
                $proadrs->setAttributes(['adrs_line1'=>$this->proAdrs->adrs_line1,
                    'adrs_line2'=>$this->proAdrs->adrs_line2,
                    'city'=>$this->proAdrs->city,
                    'state'=>$this->proAdrs->state,
                    'country'=>$this->proAdrs->country,
                    'postal_code'=>$this->proAdrs->postal_code,
                    'proof_doc_type'=>$this->proAdrs->proof_doc_type,
                ]);

                $protel=$proadrs->getTelephone()->one();
                if($protel==null){
                    $protel=new Telephone();
                }
                $protel->setAttributes([
                    'primary_landline'=>$this->proTel->primary_landline,
                    'primary_mobile'=>$this->proTel->primary_mobile,
                    'secondary_landline'=>$this->proTel->secondary_landline,
                    'secondary_mobile'=>$this->proTel->secondary_mobile,
                ]);

                $this->proTel=$protel;
                    if (!$this->proTel->save()) {
                        foreach($this->proTel->getErrors() as $msg)
                            $errmsg.=join("<br/>",$msg);
                    }else{
                        $proadrs->telephone_id=$this->proTel->id;
                    }


                if(!$proadrs->save()){
                    foreach($this->proAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->proAdrs=$proadrs;

                /*end: address*/

                $preadrs=Address::findOne($this->present_address_id);
                $preadrs->setAttributes(['adrs_line1'=>$this->preAdrs->adrs_line1,
                    'adrs_line2'=>$this->preAdrs->adrs_line2,
                    'city'=>$this->preAdrs->city,
                    'state'=>$this->preAdrs->state,
                    'country'=>$this->preAdrs->country,
                    'postal_code'=>$this->preAdrs->postal_code,
                    'proof_doc_type'=>$this->preAdrs->proof_doc_type,
                ]);


                $pretel=$preadrs->getTelephone()->one();
                if($pretel==null){
                    $pretel=new Telephone();
                }
                $pretel->setAttributes([
                    'primary_landline'=>$this->preTel->primary_landline,
                    'primary_mobile'=>$this->preTel->primary_mobile,
                    'secondary_landline'=>$this->preTel->secondary_landline,
                    'secondary_mobile'=>$this->preTel->secondary_mobile,
                ]);

                $this->preTel=$pretel;
                if (!$this->preTel->save()) {
                    foreach($this->preTel->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else{
                    $preadrs->telephone_id=$this->preTel->id;
                }


                if(!$preadrs->save()){
                    foreach($this->preAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->preAdrs=$preadrs;

                /*
                $peradrs=Address::findOne($this->permanent_address_id);
                $peradrs->setAttributes(['adrs_line1'=>$this->perAdrs->adrs_line1,
                    'adrs_line2'=>$this->perAdrs->adrs_line2,
                    'city'=>$this->perAdrs->city,
                    'state'=>$this->perAdrs->state,
                    'country'=>$this->perAdrs->country,
                    'postal_code'=>$this->perAdrs->postal_code,
                ]);
                if(!$peradrs->save()){
                    foreach($this->perAdrs->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }else
                    $this->perAdrs=$peradrs;
                */


            }



            if(!$this->save()){
                Yii::trace(VarDumper::dumpAsString($this->getErrors()),'vardump');
                foreach($this->getErrors() as $key => $msg)
                        if($key!='user_id')
                        $errmsg.=join("<br/>",$msg);
            }else{

                foreach($this->attach as $adrsatch ){
                    if(!empty($adrsatch->file_path)) {
                        $adrsatch->exec_id = $this->id;
                        if (!$adrsatch->save()) {
                            foreach ($adrsatch->getErrors() as $msg)
                                $errmsg .= join("<br/>", $msg);
                        }
                    }
                }

               /* $adrsatch = $this->attach[0];
                if(!empty($adrsatch->file_path)) {

                    $adrsatch->exec_id = $this->id;
                    $adrsatch->attachment_type = 'Address Proof';
                    if (!$adrsatch->save()) {
                        foreach ($adrsatch->getErrors() as $msg)
                            $errmsg .= join("<br/>", $msg);
                    }
                }*/
            }

            Yii::trace(VarDumper::dumpAsString($errmsg),'vardump');
            $sql="delete  from executive_language where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();


            foreach($this->lang as $edu){
                $edu->exec_id=$this->id;
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg)."<br/>";
                }
            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
		    else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

    /**
     * save Qualification section of executive
     * @return boolean
     */
    public function saveQualification(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
             $sql="delete  from executive_education where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

            $sql="delete  from executive_computer_skill where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

            /*$sql="delete  from executive_language where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();*/

            $sql="delete  from executive_certification where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

            foreach($this->edu as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }

            Yii::trace(VarDumper::dumpAsString($this->attach),'vardump');

            foreach($this->attach as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }

            foreach($this->csk as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }

            /*foreach($this->lang as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }*/

            foreach($this->cert as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }


            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;

    }

    public function saveExpVech(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
             $sql="delete  from executive_experience where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

           /* $sql="delete  from executive_vehicle where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();*/


            foreach($this->exp as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $key=> $msg) {
                        //if($key=="exec_id") continue;
                        $errmsg .= $key. join("<br/>", $msg);
                    }
                }
            }

            /*foreach($this->vech as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }*/

            foreach($this->attach as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $key => $msg){
                        if($key=="exec_id") continue;
                        $errmsg.=join("<br/>",$msg);
                    }

                }
            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }

    public function saveVech(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {
           /* $sql="delete  from executive_experience where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();*/

            $sql="delete  from executive_vehicle where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

           /* foreach($this->exp as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }*/

            foreach($this->vech as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }

            foreach($this->attach as $edu){
                $edu->exec_id=$this->id;
                Yii::trace(VarDumper::dumpAsString($edu),'vardump');
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }

            if(!$this->save()){
                foreach($this->getErrors() as $msg)
                    $errmsg.=join("<br/>",$msg);
            }

            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;



    }


    public function saveReference(){
        $connection=\Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errmsg='';
        try {

            $sql="delete  from executive_reference where  exec_id=:id";
            $sth=$connection->createCommand($sql);
            $sth->bindValue(":id",$this->id);
            $sth->execute();

            foreach($this->reference as $edu){
                if(!$edu->save()){
                    foreach($edu->getErrors() as $msg)
                        $errmsg.=join("<br/>",$msg);
                }
            }




            \yii::trace("catch error ".$errmsg,'vardump');
            if($errmsg==''){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                throw new \yii\base\Exception($errmsg);
            }
        } catch ( \yii\base\Exception $e) {
            \yii::trace("catch error ".$e->getMessage(),'vardump');
            \yii::trace("catch error ".$errmsg,'vardump');

            $this->addError("errors",$errmsg);
            return false;
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(strtotime($this->joining_date)!==strtotime('01-01-1970')){
                $this->joining_date=date('Y-m-d',strtotime($this->joining_date));
            }else{
                $this->joining_date=null;
            }

            if(strtotime($this->license_expiry)!==strtotime('01-01-1970')){
                $this->license_expiry=date('Y-m-d',strtotime($this->license_expiry));
            }else{
                $this->license_expiry=null;
            }

            if(strtotime($this->dob)!==strtotime('01-01-1970')){
                $this->dob=date('Y-m-d',strtotime($this->dob));
            }else{
                $this->dob=null;
            }



            //if($this->isNewRecord){
               // $this->created_on=date('Y-m-d H:i:s');
            //}


            return true;
        } else {
            return false;
        }
    }


    public function afterFind ()
    {
        if(strtotime($this->joining_date)==strtotime('01-01-1970')){
            $this->joining_date=null;

        }else{
            $this->joining_date=date('d-m-Y',strtotime($this->joining_date));
        }

        if(strtotime($this->created_on)==strtotime('01-01-1970')){
            $this->created_on=null;

        }else{
            $this->created_on=date('d-m-Y H:i:s',strtotime($this->created_on));
        }
        if(strtotime($this->license_expiry)==strtotime('01-01-1970')){
            $this->license_expiry=null;

        }else{
            $this->license_expiry=date('d-m-Y',strtotime($this->license_expiry));
        }
        if(strtotime($this->dob)==strtotime('01-01-1970')){
            $this->dob=null;

        }else{
            $this->dob=date('d-m-Y',strtotime($this->dob));
        }



        /*$this->applicable_for_incentives=($this->applicable_for_incentives)?"Yes":"No";
        $this->applicable_for_food_allowance=($this->applicable_for_food_allowance)?"Yes":"No";
        $this->status=($this->status)?"Active":"Resigned";*/
        //$this->dpartment=$this->getDepartment()->one()->name;

        // $this->cash_balance=Yii::$app->formatter->asCurrency($this->cash_balance,'INR');
        parent::afterFind ();
    }

    public function getAfi(){
        return ($this->applicable_for_incentives)?"Yes":"No";
    }
    public  function getAffw(){
        return ($this->applicable_for_food_allowance)?"Yes":"No";
    }
    public  function getStat(){
        return ($this->status)?"Active":"Resigned";
    }

    public  function getfproadrs(){
        $adrs=$this->getAddressProof()->one();
        $tel=$adrs->getTelephone()->one();
        Yii::trace(VarDumper::dumpAsString($tel),'vardump');
        return "
         {$adrs->adrs_line1}
         {$adrs->adrs_line2}
         {$adrs->city}
         {$adrs->state}
         {$adrs->country}
         {$adrs->postal_code}
        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}
        ":"") ."

          Id Proof Document Type:
          {$adrs->proof_doc_type}
         " ;
    }

    public  function getfperadrs(){


        return "
         {$this->getPermanentAddress()->one()->adrs_line1}
         {$this->getPermanentAddress()->one()->adrs_line2}
         {$this->getPermanentAddress()->one()->city}
         {$this->getPermanentAddress()->one()->state}
         {$this->getPermanentAddress()->one()->country}
         {$this->getPermanentAddress()->one()->postal_code}

        ";
    }

    public  function getfpreadrs(){

        $adrs=$this->getPresentAddress()->one();
        $tel=$adrs->getTelephone()->one();
       // var_dump($adrs->telephone_id);

        return "
         {$this->getPresentAddress()->one()->adrs_line1}
         {$this->getPresentAddress()->one()->adrs_line2}
         {$this->getPresentAddress()->one()->city}
         {$this->getPresentAddress()->one()->state}
         {$this->getPresentAddress()->one()->country}
         {$this->getPresentAddress()->one()->postal_code}

        ".(($tel!=null)?"
        {$tel->primary_landline}
        {$tel->secondary_landline}
        {$tel->primary_mobile}
        {$tel->secondary_mobile}
        ":"")."

          Id Poof Document Type:
          {$adrs->proof_doc_type}
         ";
    }

    public  function getadrsProofpath(){
        $app=$this->getExecutiveAttachments()->filterWhere(["attachment_type"=>"Address Proof"])->one();
        if(!empty($app)){
            return $app->file_path;
        } else{
            return null;
        }

    }

    public  function getphotopath(){
        $app=$this->getExecutiveAttachments()->filterWhere(["attachment_type"=>"Photo"])->one();
        if(!empty($app)){
            return $app->file_path;
        } else{
            return null;
        }

    }

    public  function getresumepath(){
        $app=$this->getExecutiveAttachments()->filterWhere(["attachment_type"=>"Resume"])->one();
        if(!empty($app)){
            return $app->file_path;
        } else{
            return null;
        }

    }

    public  function getpreadrspath(){
        $app=$this->getExecutiveAttachments()->filterWhere(["attachment_type"=>"Present Address Proof"])->one();
        if(!empty($app)){
            return $app->file_path;
        } else{
            return null;
        }

    }


    public function getFullname(){
        return $this->fname." ".$this->lname;
    }

    public function getEmpid(){
        $empid='';
        $user=$this->getUser()->one();
        if($user){
            $empid=$user->username;
        }
        return $empid;
    }

    public function getContact(){
        $contact='';
        $adrs=$this->getPresentAddress()->one();
        if($adrs){
            $tel=$adrs->getTelephone()->one();
            if($tel){
                $contact=$tel->primary_mobile;
            }
        }
      return $contact;
    }

    public function getActivestatus(){
        return ($this->status)?"Active":"In-Active";
    }

    public function  savePassword($user){



        $success=true;
        if(!$this->validate(['old_password','new_password','cofirm_password'])){
            $success=false;
        }
        Yii::trace(VarDumper::dumpAsString(!Password::validate($this->old_password, $user->password_hash)),'vardump');
        if ($user === null || !Password::validate($this->old_password, $user->password_hash)) {
            $this->addError('old_password', \Yii::t('user', 'Invalid  password'));
            $success=false;
        }
        if($this->new_password!=$this->confirm_password){
            $this->addError('confirm_password', \Yii::t('user', 'Confirm  password doesn\'t match'  ));
            $success=false;
        }
        if($success){
          $success=  $user->resetPassword($this->new_password);
        }
            return $success;

    }


}
