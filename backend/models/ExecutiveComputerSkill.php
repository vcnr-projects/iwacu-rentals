<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_computer_skill".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $skill
 * @property string $expertise
 *
 * @property Executive $exec
 */
class ExecutiveComputerSkill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_computer_skill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'exec_id', 'skill'], 'required'],
            [['id', 'exec_id'], 'integer'],
            [['skill', 'expertise'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'skill' => 'Skill',
            'expertise' => 'Expertise',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['id' => 'exec_id']);
    }
}
