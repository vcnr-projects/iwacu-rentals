<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_activity_type".
 *
 * @property string $type
 * @property integer $is_active
 *
 * @property ExecutiveActivity[] $executiveActivities
 */
class ExecutiveActivityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['is_active'], 'integer'],
            [['type'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Executive Activity Type',
            'is_active' => 'is Active?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutiveActivities()
    {
        return $this->hasMany(ExecutiveActivity::className(), ['activity' => 'type']);
    }
}
