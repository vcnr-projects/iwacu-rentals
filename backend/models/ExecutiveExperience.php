<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "executive_experience".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $company
 * @property string $designation
 * @property string $from
 * @property string $to
 * @property string $remarks
 */
class ExecutiveExperience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'company', 'designation', 'from', 'to'], 'required'],
            [['exec_id'], 'integer'],
            [['from', 'to'], 'safe'],
            [['remarks'], 'string'],
            [['company'], 'string', 'max' => 200],
            [['designation'], 'string', 'max' => 100],
            [['from'], 'datelessthan', 'params' =>'to'],
            [['to'], 'datelessthan', 'params' =>'current date check']
        ];
    }

    public function datelessthan($attribute,$params) {

        if($params="current date check") {
            if (strtotime($this->{$attribute}) && strtotime(date('Y-m-d')) && strtotime($this->{$attribute}) > strtotime(date('Y-m-d'))) {
                $this->addError($attribute, ' Date greater than Today\'s Date');
            }
            return;
        }
        Yii::trace(VarDumper::dumpAsString($params),'vardump');
        if(strtotime($this->{$attribute})&&strtotime($this->{$params})&&strtotime($this->{$attribute})>strtotime($this->{$params})){
            $this->addError($attribute,'From Date greater than To Date');
        }
        /*if (!Yii::app()->user->isGuest and Elist::model()->exists('address=:address and owner_id=:owner_id',array(':address'=>$this->$attribute,':owner_id'=>Yii::app()->user->id)))
            $this->addError($attribute, 'Sorry, you\'re already using that name for another list.');
    */
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'company' => 'Company',
            'designation' => 'Designation',
            'from' => 'From',
            'to' => 'To',
            'remarks' => 'Remarks',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here

            if(strtotime($this->from)!=false){
                $this->from=date('Y-m-d',strtotime($this->from));
            }else{
                $this->addError('from','Enter a valid from date');
                return false;
            }
            Yii::trace(VarDumper::dumpAsString(strtotime($this->to)),'vardump');
            if(strtotime($this->to)!=false){
                $this->to=date('Y-m-d',strtotime($this->to));
            }else{
                $this->addError('from','Enter a valid To date');
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterFind(){
        if(strtotime($this->from)!=false){
            $this->from=date('d-m-Y',strtotime($this->from));
        }
        if(strtotime($this->to)!=false){
            $this->to=date('d-m-Y',strtotime($this->to));
        }
        parent::afterFind ();
    }

}
