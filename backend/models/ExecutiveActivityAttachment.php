<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_activity_attachment".
 *
 * @property string $id
 * @property string $name
 * @property string $file
 * @property string $remarks
 * @property string $activity_id
 */
class ExecutiveActivityAttachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_activity_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'file', 'activity_id'], 'required'],
            [['id', 'activity_id'], 'integer'],
            [['remarks'], 'string'],
            [['name', 'file'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'file' => 'File',
            'remarks' => 'Remarks',
            'activity_id' => 'Activity ID',
        ];
    }
}
