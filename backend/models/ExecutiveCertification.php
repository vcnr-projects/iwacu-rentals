<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "executive_certification".
 *
 * @property string $id
 * @property integer $exec_id
 * @property string $certificate
 * @property string $issued_date
 *
 * @property Executive $exec
 */
class ExecutiveCertification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'executive_certification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exec_id', 'certificate', 'issued_date'], 'required'],
            [['exec_id'], 'integer'],
            [['issued_date'], 'safe'],
            [['certificate'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exec_id' => 'Executive',
            'certificate' => 'Certificate',
            'issued_date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExec()
    {
        return $this->hasOne(Executive::className(), ['user_id' => 'exec_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here

            if(strtotime($this->issued_date)!=false){
                $this->issued_date=date('Y-m-d',strtotime($this->issued_date));
            }else{
                $this->addError('issued_date','Enter a valid from date');
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterFind(){
        if(strtotime($this->issued_date)!=false){
            $this->issued_date=date('d-m-Y',strtotime($this->issued_date));
        }

        parent::afterFind ();
    }
}
