<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExecutiveActivity;

/**
 * ExecutiveActivitySearch represents the model behind the search form about `app\models\ExecutiveActivity`.
 */
class ExecutiveActivitySearch extends ExecutiveActivity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exec_id', 'status', 'prop_id'], 'integer'],
            [['activity', 'created_on', 'start_date', 'end_date', 'remarks'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExecutiveActivity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exec_id' => $this->exec_id,
            'created_on' => $this->created_on,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'status' => $this->status,
            'prop_id' => $this->prop_id,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
