<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExecutiveProperties;

/**
 * ExecutivePropertiesSearch represents the model behind the search form about `app\models\ExecutiveProperties`.
 */
class ExecutivePropertiesSearch extends ExecutiveProperties
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exec_id', 'property', 'is_active'], 'integer'],
            [['date', 'expiry_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExecutiveProperties::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exec_id' => $this->exec_id,
            'property' => $this->property,
            'date' => $this->date,
            'expiry_date' => $this->expiry_date,
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
