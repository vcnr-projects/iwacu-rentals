(function( $ ) {
 
    $.fn.medgridc = function(nid,pluginactivate) {
		
		  return this.each(function(){
		
		var self=$(this);
        
        var plusspan=$("<span class='ui-icon ui-icon-plusthick glyphicon glyphicon-plus ' style='opacity:0.4' ></span>");
        var delspan=$("<span class='ui-icon ui-icon-trash glyphicon glyphicon-trash '  style='opacity:0.4' ></span>");
        var theadtd=	$("<th></th>");
        $(plusspan).on("click",function(){
			
	              addrow();
		});
		$(theadtd).append(plusspan);
		
		$(plusspan).on("mouseover",function(){
			$(this).css("opacity","1");
		
			});
		$(plusspan).on("mouseout",function(){
			$(this).css("opacity","0.4");
		
			});
			
			
			$(delspan).on("mouseover",function(){
			
			$(this).css("opacity","1");
		
		
			});
		$(delspan).on("mouseout",function(){
				$(this).css("opacity","0.4");
			});
				
		var deltd=$("<td></td>");
		$(delspan).on("click",function(event){
			delrow(event);
			});
		$(deltd).append(delspan);
		
		function addrow(){

			var cltr=self.find(">tbody > tr:first");
			//alert(self.find("tbody > tr:first").size());
			var cc=$(cltr) .clone();
			$("td:last",cc).replaceWith(deltd.clone(true,true));
			$(':input',cc).val('');
			$(cc).attr("id","");
			var tbody=self.children('tbody');
			//alert("body "+$(tbody).size());
			
			$(tbody).append(cc);

			resetslno();
            if(typeof pluginactivate !== 'undefined' && typeof  pluginactivate =='function')
			    pluginactivate(cc);

		}
		function delrow(event){
			
			if($(">tbody > tr",self).size()==1){
				return false;
			}
			var tr=$(event.target).closest('tr');
			$(tr).remove();
			
			resetslno();
            try {
                delrowcallback();
            }catch( e){}
		}
		function resetslno(){
		
			$(">tbody > tr ",self).each(function(i){
				
				$(">td:first",this).text(i+1);
			});
			$("#"+nid).val($(">tbody > tr",self).size());
			
		}
		
		$("#"+nid).on("change",function(event){
		     var nr=$(event.target).val();
			
		     if(isNaN(nr)||nr==""){
				alert("Please enter numbers");
				$("#"+nid).val($(">tbody > tr",self).size());
				return false;
			 }
			 if(nr<1){
				
				var ntr=$("tbody tr",self).size();
				
				$(event.target).val(ntr);
				return false;
			 }
			 var trsz=	$(">tbody > tr",self).size();
			 if(trsz>nr){
				 for(var i=0;i<trsz-nr;i++){
					 $(">tbody tr td:last span",self).trigger("click");
				 }
			 }
			 else{
				 for(var i=0;i<nr-trsz;i++){
					 addrow();
				 }
			 }	
		});
		

        $(this).children("thead").children('tr').append(theadtd);
        $(this).children("tbody").children('tr').each(function(){$(this).append(deltd.clone(true,true));});
	        if(typeof pluginactivate !== 'undefined' && typeof  pluginactivate =='function')
              pluginactivate();
       });
 
    };
 
}( jQuery ));
 
