<?php

namespace backend\controllers;

use app\models\ExecutiveAttachments;
use app\models\ExecutiveCertification;
use app\models\ExecutiveComputerSkill;
use app\models\ExecutiveEducation;
use app\models\ExecutiveExperience;
use app\models\ExecutiveInsurance;
use app\models\ExecutiveLanguage;
use app\models\ExecutiveReference;
use app\models\ExecutiveVehicle;
use backend\components\AccessRule;
use common\models\User;
use Yii;
use app\models\Executive;
use app\models\ExecutiveSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ExecutiveController implements the CRUD actions for Executive model.
 */
class ExecutiveController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \backend\components\AccessRule::className(),
                ],
                'only' => ['index','view','create','update','delete'
                    ,'createwiz_general'
                    ,'createwiz_office'
                    ,'createwiz_qualification'
                    ,'createwiz_experience'
                    ,'createwiz_vehicle'
                    ,'createwiz_reference'
                    ,'createwiz_insurance'

                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update'
                            ,'createwiz_general'
                            ,'createwiz_office'
                            ,'createwiz_qualification'
                            ,'createwiz_experience'
                            ,'createwiz_vehicle'
                            ,'createwiz_reference'
                            ,'createwiz_insurance'
                        ],
                        'roles' => ['Data Entry'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete'
                            ,'createwiz_general'
                            ,'createwiz_office'
                            ,'createwiz_qualification'
                            ,'createwiz_experience'
                            ,'createwiz_vehicle'
                            ,'createwiz_reference'
                            ,'createwiz_insurance'
                        ],
                        'roles' => ['Admin'],
                    ],
                    [
                        'allow' => false,

                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all Executive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExecutiveSearch();
        $params=Yii::$app->request->queryParams;
        if(!isset($params['ExecutiveSearch']['activestatus'])){
            $params['ExecutiveSearch']['activestatus']=1;
        }
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Executive model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Executive model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Executive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Executive model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Executive model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Executive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Executive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Executive::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Creates a new Executive General Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatewiz_general($id)
    {
        if(($id)==0) {
            $model = new Executive();
            $model->id=0;
        }
        else
        $model = $this->findModel($id);

        $wiz=(isset($_GET['wiz']))?true:false;

        \Yii::trace(VarDumper::dumpAsString($_POST),'vardump');

        if (isset($_POST['Executive'])) {
            $model->load(Yii::$app->request->post());

            $model->preAdrs->setAttributes($_POST['PreAddress']);
            $model->proAdrs->setAttributes($_POST['ProAddress']);
           // $model->perAdrs->setAttributes($_POST['PerAddress']);
           // $model->perAdrs->setAttributes($_POST['PerAddress']);
            $model->proTel->setAttributes($_POST['ProTelephone']);
            $model->preTel->setAttributes($_POST['PreTelephone']);

            \Yii::trace(VarDumper::dumpAsString($_FILES['ExecutiveAttachments']['name']['file_path']),'vardump');



            $model->attach=array();
            $attachments=$model->getExecutiveAttachments()->all();
            if(!empty($_FILES['ExecutiveAttachments']['name']['file_path'])){
                Yii::trace(VarDumper::dumpAsString(empty($attachments)),'vardump');

                foreach($attachments as $atch){
                    if($atch->attachment_type=="Address Proof"){
                        $model->attach[0]=$atch;
                        break;
                    }

                }

                if(empty($model->attach)){

                    $model->attach[0]=new ExecutiveAttachments();
                    Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');

                }

                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                Yii::trace(VarDumper::dumpAsString($model->attach[0]->file_path),'vardump');
                $model->attach[0]->attachment_type = 'Address Proof';
                $model->attach[0]->file_path = UploadedFile::getInstance($model->attach[0], 'file_path');
                $model->attach[0]->file_path->name=$model->attach[0]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[0]->file_path->extension;
                Yii::trace(VarDumper::dumpAsString($model->attach[0]),'vardump');

                $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->name );

                /*if ($model->validate()) {
                    $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->baseName . '.' . $model->attach[0]->file_path->extension);
                }*/
            }

            if(!empty($_FILES['idproofdoc']['name'])&&false){
                $found=false;
                foreach($attachments as $atch){
                    if($atch->attachment_type=="ID Proof"){
                        $model->attach[]=$atch;
                        $found=true;
                        break;
                    }
                }

                if(!$found){
                    $model->attach[]=new ExecutiveAttachments();
                }
                $aind=count($model->attach);
                $aind--;
                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                $model->attach[$aind]->attachment_type = 'ID Proof';
                $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("idproofdoc");
                $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );

                /*if ($model->validate()) {
                    $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->baseName . '.' . $model->attach[0]->file_path->extension);
                }*/
            }

            if(!empty($_FILES['photodoc']['name'])){
                $found=false;
                foreach($attachments as $atch){
                    if($atch->attachment_type=="Photo"){
                        $model->attach[]=$atch;
                        $found=true;
                        break;
                    }
                }

                if(!$found){
                    $model->attach[]=new ExecutiveAttachments();
                }
                $aind=count($model->attach);
                $aind--;
                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                $model->attach[$aind]->attachment_type = 'Photo';
                $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("photodoc");
                $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );

                /*if ($model->validate()) {
                    $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->baseName . '.' . $model->attach[0]->file_path->extension);
                }*/
            }


            if(!empty($_FILES['resumedoc']['name'])){
                $found=false;
                foreach($attachments as $atch){
                    if($atch->attachment_type=="Photo"){
                        $model->attach[]=$atch;
                        $found=true;
                        break;
                    }
                }

                if(!$found){
                    $model->attach[]=new ExecutiveAttachments();
                }
                $aind=count($model->attach);
                $aind--;
                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                $model->attach[$aind]->attachment_type = 'Resume';
                $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("resumedoc");
                $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );

                /*if ($model->validate()) {
                    $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->baseName . '.' . $model->attach[0]->file_path->extension);
                }*/
            }


            if(!empty($_FILES['presentadrsproof']['name'])){
                $found=false;
                foreach($attachments as $atch){
                    if($atch->attachment_type=="Present Address Proof"){
                        $model->attach[]=$atch;
                        $found=true;
                        break;
                    }
                }

                if(!$found){
                    $model->attach[]=new ExecutiveAttachments();
                }
                $aind=count($model->attach);
                $aind--;
                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                $model->attach[$aind]->attachment_type = 'Present Address Proof';
                $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("presentadrsproof");
                $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );

                /*if ($model->validate()) {
                    $model->attach[0]->file_path->saveAs('uploads/' . $model->attach[0]->file_path->baseName . '.' . $model->attach[0]->file_path->extension);
                }*/
            }


            $model->lang=array();
            for($i=0;$i<count($_POST['ExecutiveLanguage']['language']);$i++){
                $csk=new ExecutiveLanguage();
                $csk->language=$_POST['ExecutiveLanguage']['language'][$i];
                $csk->proficiency=$_POST['ExecutiveLanguage']['proficiency'][$i];
                $csk->exec_id=$model->id;

                $model->lang[]=$csk;
            }


            if($model->saveGeneral())
                if($wiz)
                    return $this->redirect(['createwiz_qualification', 'id' => $model->id,'wiz'=>1]);
                else
                   return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'gen']);
        }
        {
            return $this->render('createGeneral', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Creates a new Executive office Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatewiz_office($id)
    {
        $model = Executive::findOne($id);

        $wiz=(isset($_GET['wiz']))?true:false;

        \Yii::trace(VarDumper::dumpAsString($_POST),'vardump');

        if (isset($_POST['Executive']['department_id'])) {
            $model->setAttributes($_POST['Executive']);



            \Yii::trace(VarDumper::dumpAsString($model),'vardump');
            $success=false;

            if($model->designation=='Data Entry'||$model->designation=='Admin'){
                $user=User::findOne($model->user_id);
                if($user){
                    $user->user_type=$model->designation;
                    $user->scenario="updateType";
                    if($user->save()){
                        $success=true;
                    };
                    Yii::trace(VarDumper::dumpAsString($user->getErrors()),'vardump');

                }
            }else{
                $user=User::findOne($model->user_id);
                if($user){
                    $user->user_type="Executive";
                    $user->scenario="updateType";
                    if($user->save()){
                        $success=true;
                    };
                    Yii::trace(VarDumper::dumpAsString($user->getErrors()),'vardump');
                }
            }


            if($success&&$model->save())
                if($wiz)
                    return $this->redirect(['createwiz_insurance', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'off']);
                //return $this->redirect(['view', 'id' => $model->id]);

               /* if($wiz)
                    return $this->redirect(['createwiz_reference', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id]);*/
        }
        {
            return $this->render('createOffice', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Creates a new Executive qualification Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatewiz_qualification($id)
    {
        $model = Executive::findOne($id);

        \Yii::trace(VarDumper::dumpAsString($_POST),'vardump');
        $wiz=(isset($_GET['wiz']))?true:false;


        if (isset($_POST['ExecutiveEducation'])) {
           // $model->setAttributes($_POST['Executive']);

            $model->edu=array();

            $model->attach=array();
            $attachments=$model->getExecutiveAttachments()->all();

            for($i=0;$i<count($_POST['ExecutiveEducation']['qualification']);$i++){
                $edu=new ExecutiveEducation();
                $edu->qualification=$_POST['ExecutiveEducation']['qualification'][$i];
                $edu->college_university=$_POST['ExecutiveEducation']['college_university'][$i];
                $edu->yop=$_POST['ExecutiveEducation']['yop'][$i];
                $edu->percentage=$_POST['ExecutiveEducation']['percentage'][$i];
                $edu->exec_id=$model->id;

               Yii::trace(VarDumper::dumpAsString($_FILES['attachments'.$i]),'vardump');
               if(!empty($_FILES['attachments'.$i]['name'])){

                   $found=false;
                   foreach($attachments as $atch){
                       if($atch->attachment_type==$edu->qualification){
                           $model->attach[]=$atch;
                           $found=true;
                           break;
                       }
                   }

                   if(!$found){
                       $model->attach[]=new ExecutiveAttachments();
                   }
                   $aind=count($model->attach);
                   $aind--;
                   $model->attach[$aind]->exec_id =$id;
                   $model->attach[$aind]->attachment_type =$edu->qualification;
                   Yii::trace(VarDumper::dumpAsString(UploadedFile::getInstanceByName("attachments".$i)),'vardump');
                   $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("attachments".$i);
                   $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                   Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                   $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );
               }

               $model->edu[]=$edu;
           }
            Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');


            $model->csk=array();
            for($i=0;$i<count($_POST['ExecutiveComputerSkill']['skill']);$i++){
                $csk=new ExecutiveComputerSkill();
                $csk->skill=$_POST['ExecutiveComputerSkill']['skill'][$i];
                $csk->expertise=$_POST['ExecutiveComputerSkill']['expertise'][$i];
                $csk->exec_id=$model->id;

                $model->csk[]=$csk;
            }


            /*$model->lang=array();
            for($i=0;$i<count($_POST['ExecutiveLanguage']['language']);$i++){
                $csk=new ExecutiveLanguage();
                $csk->language=$_POST['ExecutiveLanguage']['language'][$i];
                $csk->proficiency=$_POST['ExecutiveLanguage']['proficiency'][$i];
                $csk->exec_id=$model->id;

                $model->lang[]=$csk;
            }*/



            $model->cert=array();
            for($i=0;$i<count($_POST['ExecutiveCertification']['certificate']);$i++){
                $edu=new ExecutiveCertification();
                $edu->certificate=$_POST['ExecutiveCertification']['certificate'][$i];
                $edu->issued_date=$_POST['ExecutiveCertification']['issued_date'][$i];
                $edu->exec_id=$id;

                /*$model->attach=array();
                $attachments=$model->getExecutiveAttachments()->all();*/

              //  Yii::trace(VarDumper::dumpAsString($_FILES['attachments'.$i]),'vardump');
                if(!empty($_FILES['attachmentsCert'.$i]['name'])){

                    $found=false;
                    foreach($attachments as $atch){
                        if($atch->attachment_type==$edu->certificate){
                            $model->attach[count($model->attach)]=$atch;
                            $found=true;
                            break;
                        }
                    }

                    if(!$found){
                        $model->attach[count($model->attach)]=new ExecutiveAttachments();
                    }
                    $aind=count($model->attach);
                    $aind--;
                    Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                    $model->attach[$aind]->exec_id =$id;
                    $model->attach[$aind]->attachment_type =$edu->certificate;
                    Yii::trace(VarDumper::dumpAsString(UploadedFile::getInstanceByName("attachmentsCert".$i)),'vardump');
                    $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("attachmentsCert".$i);
                    $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                    Yii::trace(VarDumper::dumpAsString($model->attach[$aind]),'vardump');
                    $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );
                }

                $model->cert[]=$edu;
            }




            \Yii::trace(VarDumper::dumpAsString($model),'vardump');



            if($model->saveQualification())
                if($wiz)
                    return $this->redirect(['createwiz_experience', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'qual']);
        }
        {
            return $this->render('createQualification', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Creates a new Executive qualification Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatewiz_experience($id)
    {
        $model = Executive::findOne($id);
        $wiz=(isset($_GET['wiz']))?true:false;


        \Yii::trace(VarDumper::dumpAsString("f1"),'vardump');
        \Yii::trace(VarDumper::dumpAsString(isset($_POST['ExecutiveExperience'])),'vardump');
        \Yii::trace(VarDumper::dumpAsString(isset($_POST['ExecutiveVehicle'])),'vardump');

        if(isset($_POST['fresher'])){
            $sql="delete from executive_experience where exec_id=:id";
            $con=Yii::$app->db;
            $cmd=$con->createCommand($sql);
            $cmd->bindValue(":id",$model->id);
            $cmd->execute();
            if($wiz)
                return $this->redirect(['createwiz_vehicle', 'id' => $model->id,'wiz'=>1]);
            return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'ecv']);

        }

        if (isset($_POST['ExecutiveExperience'])||isset($_POST['ExecutiveVehicle'])) {
            // $model->setAttributes($_POST['Executive']);

            \Yii::trace(VarDumper::dumpAsString($_POST),'vardump');
            \Yii::trace(VarDumper::dumpAsString(!empty($_POST['ExecutiveExperience']['designation'][0])),'vardump');


            $model->exp=array();
            if(!empty($_POST['ExecutiveExperience']['designation'][0]))
            for($i=0;$i<count($_POST['ExecutiveExperience']['designation']);$i++) {

                $csk = new ExecutiveExperience();
                $csk->company = $_POST['ExecutiveExperience']['company'][$i];
                $csk->designation = $_POST['ExecutiveExperience']['designation'][$i];
                $csk->from = $_POST['ExecutiveExperience']['from'][$i];
                $csk->to = $_POST['ExecutiveExperience']['to'][$i];
                $csk->exec_id = $model->id;

                $model->exp[] = $csk;

                $model->attach = array();
                $attachments = $model->getExecutiveAttachments()->all();

                Yii::trace(VarDumper::dumpAsString($_FILES['attachments' . $i]), 'vardump');
                if (!empty($_FILES['attachments' . $i]['name'])) {

                    $found = false;
                    foreach ($attachments as $atch) {
                        if ($atch->attachment_type == $csk->designation . $csk->company) {
                            $model->attach[] = $atch;
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $model->attach[] = new ExecutiveAttachments();
                    }
                    $aind = count($model->attach);
                    $aind--;
                    $model->attach[$aind]->exec_id = $id;
                    $model->attach[$aind]->attachment_type = $csk->designation . $csk->company;
                    $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("attachments" . $i);
                    $model->attach[$aind]->file_path->name = $model->attach[$aind]->file_path->baseName . rand(10000000, 999999999) . '.' . $model->attach[$aind]->file_path->extension;
                    $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name);

                }
            }

            $model->vech=array();
            if(!empty($_POST['ExecutiveVehicle']['vehicle_no'][0]))
            for($i=0;$i<count($_POST['ExecutiveVehicle']['vehicle_no']);$i++){
                $csk=new ExecutiveVehicle();
                $csk->vehicle_no=$_POST['ExecutiveVehicle']['vehicle_no'][$i];
                $csk->vehicle_type=$_POST['ExecutiveVehicle']['vehicle_type'][$i];
                $csk->make=$_POST['ExecutiveVehicle']['make'][$i];
                $csk->model=$_POST['ExecutiveVehicle']['model'][$i];
                $csk->exec_id=$model->id;

                $model->vech[]=$csk;
            }




            if($model->saveExpVech())
                if($wiz)
                    return $this->redirect(['createwiz_vehicle', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'ecv']);
               // return $this->redirect(['view', 'id' => $model->id]);
        }

        {
            \Yii::trace(VarDumper::dumpAsString("f5"),'vardump');

            return $this->render('createExperience', [
                'model' => $model,
            ]);
        }
    }



    public function actionCreatewiz_vehicle($id)
    {
        $model = Executive::findOne($id);

        $wiz=(isset($_GET['wiz']))?true:false;



        if (isset($_POST['ExecutiveVehicle'])) {
            //$model->setAttributes($_POST['Executive']);

            if($_POST['Executive']){
                $model->load(Yii::$app->request->post());
            }
            $model->vech=array();
            if(!empty($_POST['ExecutiveVehicle']['vehicle_no'][0]))
                for($i=0;$i<count($_POST['ExecutiveVehicle']['vehicle_no']);$i++){
                    $csk=new ExecutiveVehicle();
                    $csk->vehicle_no=$_POST['ExecutiveVehicle']['vehicle_no'][$i];
                    $csk->vehicle_type=$_POST['ExecutiveVehicle']['vehicle_type'][$i];
                    $csk->make=$_POST['ExecutiveVehicle']['make'][$i];
                    $csk->model=$_POST['ExecutiveVehicle']['model'][$i];
                    $csk->insurance=$_POST['ExecutiveVehicle']['insurance'][$i];
                    $csk->insurance_expiry_dt=$_POST['ExecutiveVehicle']['insurance_expiry_dt'][$i];
                    $csk->exec_id=$model->id;

                    $model->vech[]=$csk;
                }


            $model->attach=array();
            $attachments = $model->getExecutiveAttachments()->andWhere(["attachment_type"=>"ID Proof"])->all();

            if(!empty($_FILES['idproofdoc']['name'])){
                $found=false;

                foreach($attachments as $atch){
                    if($atch->attachment_type=="ID Proof"){
                        $model->attach[]=$atch;
                        $found=true;
                        break;
                    }
                }

                if(!$found){
                    $model->attach[]=new ExecutiveAttachments();
                }

                $aind=count($model->attach);
                $aind--;
                Yii::trace(VarDumper::dumpAsString($model->attach),'vardump');
                $model->attach[$aind]->attachment_type = 'ID Proof';
                $model->attach[$aind]->file_path = UploadedFile::getInstanceByName("idproofdoc");
                $model->attach[$aind]->file_path->name=$model->attach[$aind]->file_path->baseName .rand(10000000,999999999) .'.' . $model->attach[$aind]->file_path->extension;
                $model->attach[$aind]->file_path->saveAs('uploads/' . $model->attach[$aind]->file_path->name );
            }else{
                foreach($attachments as $atch){
                    if($atch->attachment_type=="ID Proof"){
                        $model->attach[]=$atch;

                    }
                }
            }

            if($model->saveVech())
                if($wiz)
                    return $this->redirect(['createwiz_reference', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'vech']);
               // return $this->redirect(['view', 'id' => $model->id]);

        }
        {
            \Yii::trace(VarDumper::dumpAsString("f5"),'vardump');
            return $this->render('createVehicle', [
                'model' => $model,
            ]);
        }
    }


    public function actionCreatewiz_reference($id)
    {
        $wiz=(isset($_GET['wiz']))?true:false;
        $model = Executive::findOne($id);
        $model->reference=ExecutiveReference::findAll(['exec_id'=>$id]);
        //$refletter=($model->reference)?$model->reference->reference_letter:null;
        if(!$model->reference){
            $model->reference[]=new ExecutiveReference();
        }


        if (isset($_POST['ExecutiveReference'])) {
            //$model->setAttributes($_POST['Executive']);
            $oldref=$model->reference;

            $model->reference=array();
            //ExecutiveReference::deleteAll(['exec_id'=>$model->id]);
            foreach($_POST['ExecutiveReference']['name'] as $i=>$name){
                if($name==''){
                    continue;
                }
                $reference=new ExecutiveReference();
                $reference->exec_id=$model->id;
                $reference->name=$name;
                $reference->company=$_POST['ExecutiveReference']['company'][$i];
                $reference->designation=$_POST['ExecutiveReference']['designation'][$i];
                $reference->phno=$_POST['ExecutiveReference']['phno'][$i];
                $reference->email=$_POST['ExecutiveReference']['email'][$i];
                $reference->remarks=$_POST['ExecutiveReference']['remarks'][$i];
                $reference->adrs=$_POST['ExecutiveReference']['adrs'][$i];
                //$reference->reference_letter=$_FILES['reference_letter']['tmp_name'][$i];

                $tmpFilePath = $_FILES['ExecutiveReference']['tmp_name']['reference_letter'][$i];
                if ($tmpFilePath != ""){
                    //Setup our new file path
                    $newFilePath = rand(10000000, 999999999). $_FILES['ExecutiveReference']['name']['reference_letter'][$i];
                    //Upload the file into the temp dir
                    if(move_uploaded_file($tmpFilePath, "uploads/".$newFilePath)) {
                        //Handle other code here
                        $reference->reference_letter=$newFilePath ;
                    }
                }else{
                    if(isset($oldref[$i])){
                        $reference->reference_letter=$oldref[$i]->reference_letter;
                    }
                }
                $model->reference[]=$reference;
            }

            if($model->saveReference())
                 if($wiz)
                    return $this->redirect(['createwiz_office', 'id' => $model->id,'wiz'=>1]);
                else
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'ref']);
               // return $this->redirect(['view', 'id' => $model->id]);

        }
        {
            \Yii::trace(VarDumper::dumpAsString("f5"),'vardump');
            return $this->render('_formreference', [
                'model' => $model->reference,
                'id'=>$model->id
                ,
            ]);
        }
    }


    public function actionCreatewiz_insurance($id)
    {
        $wiz=(isset($_GET['wiz']))?true:false;
        $model = Executive::findOne($id);
        $model->insurance=ExecutiveInsurance::findOne(['exec_id'=>$id]);
        if(!$model->insurance){
            $model->insurance=new ExecutiveInsurance();

        }
        $certificate=$model->insurance->certificate;

        if (isset($_POST['ExecutiveInsurance'])) {
            //$model->setAttributes($_POST['Executive']);

            ExecutiveInsurance::deleteAll(['exec_id'=>$model->id]);


            $model->insurance=new ExecutiveInsurance();
            $model->insurance->load(Yii::$app->request->post());
            $model->insurance->exec_id=$model->id;

            $tmpFilePath = $_FILES['ExecutiveInsurance']['tmp_name']['certificate'];
            if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = rand(10000000, 999999999). $_FILES['ExecutiveInsurance']['name']['certificate'];
                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, "uploads/".$newFilePath)) {
                    //Handle other code here
                    $model->insurance->certificate=$newFilePath ;
                }
            }else{
                $model->insurance->certificate=$certificate;

            }



            if($model->insurance->save())
               /* if($wiz)
                    return $this->redirect(['createwiz_office', 'id' => $model->id,'wiz'=>1]);
                else*/
                    return $this->redirect(['view', 'id' => $model->id,'viewtab'=>'ins']);
            // return $this->redirect(['view', 'id' => $model->id]);

        }
        {
            return $this->render('_forminsurance', [
                'model' => $model->insurance,
                'id'=>$model->id
                ,
            ]);
        }
    }


    public function actionUpdatePassword($id)
    {
        $model = $this->findModel($id);

        $user=\common\models\User::findOne($model->user_id);

        $loginuser=\common\models\User::findOne(Yii::$app->user->id);

        if(!$user){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if($user->id!=Yii::$app->user->id&&$loginuser->user_type!="Admin"){
            throw new HttpException(403,"Access Denied");
        }
        if ($model->load(Yii::$app->request->post()) && $model->savePassword($user)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update_password', [
                'model' => $model,
            ]);
        }

    }
    //end class
}
