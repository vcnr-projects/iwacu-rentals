<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action){
        //var_dump(get_class($action));
        $exception = Yii::$app->errorHandler->exception;
        /*var_dump($exception->getTrace()[0]['args'][0]->getPathInfo());
        var_dump(strpos($exception->getTrace()[0]['args'][0]->getPathInfo(),'uploads')!==false);
        return;*/
        //return parent::beforeAction($action);
        Yii::trace(VarDumper::dumpAsString(($exception)?"True":"False"),'vardump');

        if($exception) {
            if (!method_exists($exception->getTrace()[0]['args'][0],'getPathInfo')) {
                return parent::beforeAction($action);
            }

            if ($exception && strpos($exception->getTrace()[0]['args'][0]->getPathInfo(), 'uploads') !== false) {
                header('Content-type: image/png');
                echo file_get_contents('default-image.jpg');
                return;
            } else {
                return parent::beforeAction($action);
            }
        }else{
            return parent::beforeAction($action);
        }

        /*if(get_class($action)=="yii\web\ErrorAction"){


            return $exception->getTrace()[0]['args'][0]->getPathInfo();
            if(strrpos($exception->getTrace()[0]['args'][0]->getPathInfo(),'/uploads/')){
                return "4o4 uploads";
            }
        }else{
            return parent::beforeAction($action);
        }*/

    }

   /* public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return $exception->getTrace()[0]['args'][0]->getPathInfo();
        if(strrpos($exception->getTrace()[0]['args'][0]->getPathInfo(),'/uploads/')){
            return "4o4 uploads";
        }
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }*/



    public function actionIndex()
    {
        return $this->redirect(['/realty/property/index', 'builder' => "builder"]);
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {

            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
